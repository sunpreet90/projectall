/*--validtae signupform--*/
jQuery("#registerform").validate({
  errorClass   : "error-message",
  validClass   : "success-message",
  rules: {
    name: "required",
    phone: "required",
    terms: "required",
    email: {
      required: true,
      email: true
    },
    password : {
        required: true
    },
    confirm_password : {
    	required: true,
        equalTo : "#password"
    }
  }
});
/*--//validtae signupform--*/

/*--validate login form--*/
jQuery("#loginform").validate({
	errorClass   : "error-message",
	validClass   : "success-message",
	rules: {
	    email: {
	      required: true,
	      email: true
	    },
	    password : {
	        required: true
	    }
	}
});
/*--//validate login form--*/

/*--//validate register appointment form start here--*/

/*$('#stepy_form21312').validate({
  errorClass   : "error-message",
  validClass   : "success-message",
  rules: {
  	email: "required",
  	password: "required"
  	phone: "required",
  	accept_aggrement: "required",
  	current_address: "required",
  	first_consultation: "required",
  	estimate_date: "required",
  	skype_account: "required",
  	skype_id : "required",
  	advise_tool: "required",
  	immigration_type : "required",
  	applied_residence : "required",
  	living_canada : "required",
  	arrive_date : "required",
  	immigration_status : "required",
  	expiry_date : "required",
  	immigration_situation : "required",

  	referred_by : "required",
  	thank_referral : "required",
  	email: {
  		required: true,
  		email: true
  	}
  }
});	*/

/*--//validate register appointment form end here--*/

/* $(function() {
    $('#datetimepicker4').datetimepicker({
      pickTime: false
    });
  });

	$(function() {
	$('#datetimepicker3').datetimepicker({
	  pickDate: false
	});
	});*/



jQuery(document).ready(function(){

	/*--code for signup form start here--*/
		jQuery(".registerBTN").click(function(event){
			event.preventDefault();
			if(jQuery("#registerform").valid()){
				var name = jQuery(this).closest("form").find("input[name='name']").val();
				var email = jQuery(this).closest("form").find("input[name='email']").val();
				var phone = jQuery(this).closest("form").find("input[name='phone']").val();
				var password = jQuery(this).closest("form").find("input[name='password']").val();

					 jQuery.ajax({
				        type: 'post',
				        url: baseurl + 'website/register',
				        data: { name: name, email: email, phone: phone, password: password },
				        success: function ( result )
				        {
				        	if(result == '0'){
				        		jQuery(".custommessage").html('<div class="alert alert-warning"><strong>Warning!</strong> Email Already Exist.</div>');
				        	}else{
				        		jQuery(".custommessage").html('<div class="alert alert-success"><strong>Success!</strong> Successfully Register.</div>');
				        		setTimeout(function(){
				        			jQuery("#basicModal").modal('hide');
				        			jQuery('#registerform')[0].reset();
				        			jQuery(".custommessage").html('');
				        		}, 1000);
				        	}
				        },
				        error: function ( result )
				        {

				        }
				    });
			}
		});
	/*--//code for signup form end here--*/



	/*--code for register appointment start here--*/

	/*jQuery(document).on('click','.nxtbtn_extend',function(event){
		event.preventDefault();

		if($("#stepy_form").valid()){
			alert("valid");
				var firstname = jQuery(this).closest("form").find("input[name='firstname']").val();
				var lastname = jQuery(this).closest("form").find("input[name='lastname']").val();
				var phone = jQuery(this).closest("form").find("input[name='phone']").val();
				var email = jQuery(this).closest("form").find("input[name='email']").val();
				var accept_aggrement = jQuery(this).closest("form").find("input[name='accept_aggrement']:checked").val();
				var current_address = jQuery(this).closest("form").find("input[name='current_address']").val();
				var first_consultation = jQuery(this).closest("form").find("input[name='first_consultation']:checked").val();
				var estimate_date = jQuery(this).closest("form").find("input[name='estimate_date']").val();
				var skype_account = jQuery(this).closest("form").find("input[name='skype_account']:checked").val();
				var skype_id = jQuery(this).closest("form").find("input[name='skype_id']").val();
				var advise_tool = jQuery(this).closest("form").find("input[name='advise_tool']").val();
				var immigration_type = jQuery(this).closest("form").find("input[name='immigration_type']").val();
				var applied_residence = jQuery(this).closest("form").find("input[name='applied_residence']:checked").val();
				var living_canada = jQuery(this).closest("form").find("input[name='living_canada']:checked").val();
				var arrive_date = jQuery(this).closest("form").find("input[name='arrive_date']").val();
				var immigration_status = jQuery(this).closest("form").find("input[name='immigration_status']").val();
				var expiry_date = jQuery(this).closest("form").find("input[name='expiry_date']").val();
				var immigration_situation = jQuery(this).closest("form").find("input[name='immigration_situation']").val();
				var referred_by = jQuery(this).closest("form").find("input[name='referred_by']:checked").val();
				var thank_referral = jQuery(this).closest("form").find("input[name='thank_referral']").val();

					 $.ajax( {
				        type: 'post',
				        url: baseurl + 'website/booking_consultation',
data: { firstname: firstname, lastname: lastname, email: email, phone: phone, accept_aggrement: accept_aggrement, current_address: current_address,first_consultation: first_consultation, estimate_date: estimate_date, skype_account: skype_account, skype_id: skype_id, advise_tool: advise_tool, immigration_type: immigration_type, applied_residence: applied_residence, living_canada: living_canada, arrive_date: arrive_date, immigration_status: immigration_status, expiry_date: expiry_date, immigration_situation: immigration_situation, referred_by: referred_by, thank_referral: thank_referral   },
				        success: function ( result )
				        {
				        	if(result == '0'){
				        		$(".custommessage").html('<div class="alert alert-warning"><strong>Warning!</strong> Email Already Exist.</div>');
				        	}else{
				        		$(".custommessage").html('<div class="alert alert-success"><strong>Success!</strong> Successfully Register.</div>');
				        		setTimeout(function(){
				        			$("#basicModal").modal('hide');
				        			$('#registerform')[0].reset();
				        			$(".custommessage").html('');
				        		}, 1000);
				        	}
				        },
				        error: function ( result )
				        {

				        }
				    });
			}else{
				alert("Not Valid");
			}

	});*/

	/*jQuery(document).ready(function(){	
	
	});*/
	/*--code for register appointment end here--*/





	/*--code for login start here--*/
		jQuery(".loginBtn").click(function(event){
			event.preventDefault();
			if(jQuery("#loginform").valid()){
				var email = jQuery(this).closest("form").find("input[name='email']").val();
				var password = jQuery(this).closest("form").find("input[name='password']").val();
				jQuery.ajax({
			        type: 'post',
			        url: baseurl + 'website/login',
			        data: { email: email, password: password },
			        success: function ( result )
			        {
			        	console.log(result);
			        	if(result == '0'){
			        		jQuery(".customloginmessage").html('<div class="alert alert-warning"><strong>Warning!</strong> Email & Password Not Match.</div>');
			        	}else{
			        		jQuery(".customloginmessage").html('<div class="alert alert-success"><strong>Success!</strong> Login Successfully.</div>');
			        		setTimeout(function(){
			        			jQuery("#basicModal11").modal('hide');
			        			jQuery('#loginform')[0].reset();
			        			jQuery(".customloginmessage").html('');
			        			location.reload(true);
			        		}, 2000);
			        	}
			        },
			        error: function ( result )
			        {

			        }
			    });
			}
		});
	/*--//code for login end here--*/



	/*--code for forgot password start here--*/
	jQuery(".forgotpasswordbtn").click(function(event){
		event.preventDefault();
		jQuery("#basicModal11").modal('hide');
		jQuery('#loginform')[0].reset();
		jQuery("#basicModal22").modal('show');
		jQuery('#forgotpasswordform')[0].reset();
		jQuery(".forgotpassrequestbtn").click(function(event){
			event.preventDefault();
			alert("Processing");
		});
	});
	/*--//code for forgot password end here--*/


	/*--create tab for hire consultent--*/
	jQuery(".tab-content #Section1 .consultant").addClass("dshfkgksdhfk");
	jQuery(".tab-content #Section1 .consultant").each(function(){
		jQuery(this).click(function(event){
			event.preventDefault();
			seletedCns = jQuery(this);
			var conId = jQuery(this).attr("id");

			jQuery(".consultenttab li:nth-child(2)").addClass("active");
			jQuery(".consultenttab li:nth-child(1)").removeClass("active");

			jQuery.ajax({
		        type: 'post',
		        url: baseurl + 'website/selectconsultent',
		        data: { conId: conId },
		        success: function ( result )
		        {
		        	seletedCns.parent("div").removeClass("in active");
					seletedCns.parent("div").next("div").removeClass("fade");
					seletedCns.parent("div").next("div").addClass("in active");
		        	seletedCns.parent("div").next("div").html(result);
		        	

		        	jQuery(".previoustabcon, .consultenttab li:nth-child(1)").click(function(event){
		        		event.preventDefault();
		        		jQuery(".consultenttab li:nth-child(1)").addClass("active");
		        		jQuery(".consultenttab li:nth-child(2)").removeClass("active");
		        		jQuery("#Section2").removeClass("in active");
		        		jQuery("#Section2").addClass("fade");
		        		jQuery("#Section1").addClass("in active");
		        		jQuery("#Section1").removeClass("fade");
		        	});



		        	/*--submit appointment data start here--*/
		        		jQuery(document.body).on('click', '.nxtbtn_extend', function(event){
		        			//alert($('.selectedconsultent_sec').attr('id'));
		        			var conssId = jQuery('.selectedconsultent_sec').attr("id");
		        			jQuery("#formid").val(conssId);
		        			//alert(conssId);
						   var $form =jQuery('#stepy_form');
						       $form.validate({
						       	errorClass   : "error-message",
								validClass   : "success-message",
								rules: {
								  	firstname: "required",
								  	lastname: "required",
								  	phone: "required",
								  	accept_aggrement: "required",
								  	current_address: "required",
								  	first_consultation: "required",
								  	estimate_date: "required",
								  	skype_account: "required",
								  	skype_id : "required",
								  	advise_tool: "required",
								  	immigration_type : "required",
								  	applied_residence : "required",
								  	living_canada : "required",
								  	arrive_date : "required",
								  	immigration_status : "required",
								  	expiry_date : "required",
								  	immigration_situation : "required",

								  	referred_by : "required",
								  	thank_referral : "required",
								  	email: {
								  		required: true
								  		//email: true
								  	}
								}
						         /*submitHandler: function($form) {
						           $($form).ajaxSubmit({  //need to properly refer to jQuery object
						             target: "#result"
						           });
						         }*/
						       }).form();

						       event.preventDefault();

						       if($form.valid()){
						       	//alert("valid");

						       		var formid = jQuery(this).closest("form").find("input[name='formid']").val();
						       		var firstname = jQuery(this).closest("form").find("input[name='firstname']").val();
									var lastname = jQuery(this).closest("form").find("input[name='lastname']").val();
									var phone = jQuery(this).closest("form").find("input[name='phone']").val();
									var datetimepicker = jQuery(this).closest("form").find("input[name='datetimepicker']").val();
									var email = jQuery(this).closest("form").find("input[name='email']").val();
									var accept_aggrement = jQuery(this).closest("form").find("input[name='accept_aggrement']:checked").val();
									var current_address = jQuery(this).closest("form").find("#current_address").val();
									var first_consultation = jQuery(this).closest("form").find("input[name='first_consultation']:checked").val();
									var estimate_date = jQuery(this).closest("form").find("input[name='estimate_date']").val();
									var skype_account = jQuery(this).closest("form").find("input[name='skype_account']:checked").val();
									var skype_id = jQuery(this).closest("form").find("input[name='skype_id']").val();
									var advise_tool = jQuery(this).closest("form").find(".advise option:selected").val();
									var immigration_type = jQuery(this).closest("form").find(".immigration_type option:selected").val();
									var applied_residence = jQuery(this).closest("form").find("input[name='applied_residence']:checked").val();
									var living_canada = jQuery(this).closest("form").find("input[name='living_canada']:checked").val();
									var arrive_date = jQuery(this).closest("form").find("input[name='arrive_date']").val();
									var immigration_status = jQuery(this).closest("form").find(".immigration_status option:selected").val();
									var expiry_date = jQuery(this).closest("form").find("input[name='expiry_date']").val();
									var immigration_situation = jQuery(this).closest("form").find("#immigration_situation").val();
									var referred_by = jQuery(this).closest("form").find("input[name='referred_by']:checked").val();
									var thank_referral = jQuery(this).closest("form").find("input[name='thank_referral']").val();
									var consultation_document = jQuery(this).closest("form").find('.img1 img').attr('src');
									var consultation_document1 = jQuery(this).closest("form").find('.img2 img').attr('src');
									var consultation_document2 = jQuery(this).closest("form").find('.img3 img').attr('src');
									/*var thank_referral = jQuery(this).closest("form").find("input[name='consultation_document']").val();
									var thank_referral = jQuery(this).closest("form").find("input[name='consultation_document']").val();
									var thank_referral = jQuery(this).closest("form").find("input[name='consultation_document']").val();*/

						       	/*alert("valid"); $('.img1 img').attr('src');

						       		var firstname = jQuery(this).closest("form").find("input[name='firstname']").val();
									var lastname = jQuery(this).closest("form").find("input[name='lastname']").val();
									var phone = jQuery(this).closest("form").find("input[name='phone']").val();
									var email = jQuery(this).closest("form").find("input[name='email']").val();
									var accept_aggrement = jQuery(this).closest("form").find("input[name='accept_aggrement']:checked").val();
									var current_address = jQuery(this).closest("form").find("#current_address").val();
									var first_consultation = jQuery(this).closest("form").find("input[name='first_consultation']:checked").val();
									var estimate_date = jQuery(this).closest("form").find("input[name='estimate_date']").val();
									var skype_account = jQuery(this).closest("form").find("input[name='skype_account']:checked").val();
									var skype_id = jQuery(this).closest("form").find("input[name='skype_id']").val();
									var advise_tool = jQuery(this).closest("form").find(".advise option:selected").val();
									var immigration_type = jQuery(this).closest("form").find(".immigration_type option:selected").val();
									var applied_residence = jQuery(this).closest("form").find("input[name='applied_residence']:checked").val();
									var living_canada = jQuery(this).closest("form").find("input[name='living_canada']:checked").val();
									var arrive_date = jQuery(this).closest("form").find("input[name='arrive_date']").val();
									var immigration_status = jQuery(this).closest("form").find(".immigration_status option:selected").val();
									var expiry_date = jQuery(this).closest("form").find("input[name='expiry_date']").val();
									var immigration_situation = jQuery(this).closest("form").find("#immigration_situation").val();
									var referred_by = jQuery(this).closest("form").find("input[name='referred_by']:checked").val();
									var thank_referral = jQuery(this).closest("form").find("input[name='thank_referral']").val();
									$.ajax( {
							        type: 'post',
							        url: baseurl + 'website/booking_consultation',
data: { firstname: firstname, lastname: lastname, email: email, phone: phone, accept_aggrement: accept_aggrement, current_address: current_address,first_consultation: first_consultation, estimate_date: estimate_date, skype_account: skype_account, skype_id: skype_id, advise_tool: advise_tool, immigration_type: immigration_type, applied_residence: applied_residence, living_canada: living_canada, arrive_date: arrive_date, immigration_status: immigration_status, expiry_date: expiry_date, immigration_situation: immigration_situation, referred_by: referred_by, thank_referral: thank_referral   },
				        success: function ( result )
				        {
				        	if(result == '0'){

				        			


				        		$(".custommessage").html('<div class="alert alert-warning"><strong>Warning!</strong> Email Already Exist.</div>');
				        	}else{

				        			jQuery(".consultenttab li:nth-child(2)").removeClass("active");
						       		jQuery(".consultenttab li:nth-child(3)").addClass("active");
						       		jQuery("#Section2").removeClass("in active");
		        					jQuery("#Section2").addClass("fade");
		        					jQuery("#Section3").addClass("in active");
		        					jQuery("#Section3").removeClass("fade");

				        		$(".custommessage").html('<div class="alert alert-success"><strong>Success!</strong> Appointment Successfully Register.</div>');
				        		setTimeout(function(){
				        			//$("#basicModal").modal('hide');
				        			//$('#registerform')[0].reset();
				        			//$(".custommessage").html('');
				        		}, 1000);
				        	}
				        },
							        error: function ( result )
							        {

							        }
							    });*/


						       		jQuery(".consultenttab li:nth-child(2)").removeClass("active");
						       		jQuery(".consultenttab li:nth-child(3)").addClass("active");
						       		jQuery("#Section2").removeClass("in active");
		        					jQuery("#Section2").addClass("fade");
		        					jQuery("#Section3").addClass("in active");
		        					jQuery("#Section3").removeClass("fade");








		        					/*--payment section with validation start here--*/
		        					function cardFormValidate(){
									    var cardValid = 0;
									      
									    //Card validation
									    jQuery('#card_number').validateCreditCard(function(result) {
									        var cardType = (result.card_type == null)?'':result.card_type.name;
									        if(cardType == 'Visa'){
									            var backPosition = result.valid?'2px -163px, 260px -87px':'2px -163px, 260px -61px';
									        }else if(cardType == 'MasterCard'){
									            var backPosition = result.valid?'2px -247px, 260px -87px':'2px -247px, 260px -61px';
									        }else if(cardType == 'Maestro'){
									            var backPosition = result.valid?'2px -289px, 260px -87px':'2px -289px, 260px -61px';
									        }else if(cardType == 'Discover'){
									            var backPosition = result.valid?'2px -331px, 260px -87px':'2px -331px, 260px -61px';
									        }else if(cardType == 'Amex'){
									            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
									        }else{
									            var backPosition = result.valid?'2px -121px, 260px -87px':'2px -121px, 260px -61px';
									        }
									        jQuery('#card_number').css("background-position", backPosition);
									        if(result.valid){
									            jQuery("#card_type").val(cardType);
									            jQuery("#card_number").removeClass('required');
									            cardValid = 1;
									        }else{
									            jQuery("#card_type").val('');
									            jQuery("#card_number").addClass('required');
									            cardValid = 0;
									        }
									    });
									      
									    //Form validation
									    var cardName = jQuery("#name_on_card").val();
									    var expMonth = jQuery("#expiry_month").val();
									    var expYear = jQuery("#expiry_year").val();
									    var cvv = jQuery("#cvv").val();
									    var regName = /^[a-z ,.'-]+$/i;
									    var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
									    var regYear = /^2016|2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/;
									    var regCVV = /^[0-9]{3,3}$/;
									    if (cardValid == 0) {
									        jQuery("#card_number").addClass('required');
									        jQuery("#card_number").focus();
									        return false;
									    }else if (!regMonth.test(expMonth)) {
									        jQuery("#card_number").removeClass('required');
									        jQuery("#expiry_month").addClass('required');
									        jQuery("#expiry_month").focus();
									        return false;
									    }else if (!regYear.test(expYear)) {
									        jQuery("#card_number").removeClass('required');
									        jQuery("#expiry_month").removeClass('required');
									        jQuery("#expiry_year").addClass('required');
									        jQuery("#expiry_year").focus();
									        return false;
									    }else if (!regCVV.test(cvv)) {
									        jQuery("#card_number").removeClass('required');
									        jQuery("#expiry_month").removeClass('required');
									        jQuery("#expiry_year").removeClass('required');
									        jQuery("#cvv").addClass('required');
									        jQuery("#cvv").focus();
									        return false;
									    }else if (!regName.test(cardName)) {
									        jQuery("#card_number").removeClass('required');
									        jQuery("#expiry_month").removeClass('required');
									        jQuery("#expiry_year").removeClass('required');
									        jQuery("#cvv").removeClass('required');
									        jQuery("#name_on_card").addClass('required');
									        jQuery("#name_on_card").focus();
									        return false;
									    }else{
									        jQuery("#card_number").removeClass('required');
									        jQuery("#expiry_month").removeClass('required');
									        jQuery("#expiry_year").removeClass('required');
									        jQuery("#cvv").removeClass('required');
									        jQuery("#name_on_card").removeClass('required');
									        jQuery('#cardSubmitBtn').prop('disabled', false);  
									        return true;
									    }
									}
									    
									jQuery(document).ready(function() {
									    //Demo card numbers
									    jQuery('.card-payment .numbers li').wrapInner('<a href="javascript:void(0);"></a>').click(function(e) {
									        e.preventDefault();
									        jQuery('.card-payment .numbers').slideUp(100);
									        cardFormValidate()
									        return jQuery('#card_number').val(jQuery(this).text()).trigger('input');
									    });
									    jQuery('body').click(function() {
									        return jQuery('.card-payment .numbers').slideUp(100);
									    });
									    jQuery('#sample-numbers-trigger').click(function(e) {
									        e.preventDefault();
									        e.stopPropagation();
									        return jQuery('.card-payment .numbers').slideDown(100);
									    });
									    
									    //Card form validation on input fields
									    jQuery('#paymentForm input[type=text]').on('keyup',function(){
									        cardFormValidate();
									    });
									    
									    //Submit card form
									    jQuery("#cardSubmitBtn").on('click',function(){
									    	//alert("click");
									        if (cardFormValidate()) {
									            var formData = jQuery('#paymentForm').serialize();
									            jQuery.ajax({
									                type:'POST',
									                url: baseurl + 'website/paymentevent',
									                dataType: "json",
									                data:formData,
									                beforeSend: function(){  
									                    jQuery("#cardSubmitBtn").val('Processing....');
									                },
									                success:function(data){
									                	console.log(data.transactionID);
									                	var transactionID = data.transactionID;
									                	//alert(transactionID);
									                    if (data.status == 1) {

									 
									jQuery.ajax({
							        type: 'post',
							        url: baseurl + 'website/booking_consultation',
data: { formid: formid, firstname: firstname, lastname: lastname, email: email, phone: phone, datetimepicker:datetimepicker, accept_aggrement: accept_aggrement, current_address: current_address,first_consultation: first_consultation, estimate_date: estimate_date, skype_account: skype_account, skype_id: skype_id, advise_tool: advise_tool, immigration_type: immigration_type, applied_residence: applied_residence, living_canada: living_canada, arrive_date: arrive_date, immigration_status: immigration_status, expiry_date: expiry_date, immigration_situation: immigration_situation, referred_by: referred_by, thank_referral: thank_referral, transactionID: transactionID, consultation_document: consultation_document, consultation_document1: consultation_document1, consultation_document2: consultation_document2   },
				        success: function ( result )
				        {
				        	if(result == '0'){

				        			


				        		jQuery(".custommessage").html('<div class="alert alert-warning"><strong>Warning!</strong> Email Already Exist.</div>');
				        	}else{

				        			jQuery(".consultenttab li:nth-child(2)").removeClass("active");
						       		jQuery(".consultenttab li:nth-child(3)").addClass("active");
						       		jQuery("#Section2").removeClass("in active");
		        					jQuery("#Section2").addClass("fade");
		        					jQuery("#Section3").addClass("in active");
		        					jQuery("#Section3").removeClass("fade");

				        		jQuery(".custommessage").html('<div class="alert alert-success"><strong>Success!</strong> Appointment Successfully Register.</div>');
				        		setTimeout(function(){
				        			//$("#basicModal").modal('hide');
				        			//$('#registerform')[0].reset();
				        			//$(".custommessage").html('');
				        		}, 1000);
				        	}
				        },
							        error: function ( result )
							        {

							        }
							    });  	
				                    	//alert(data.transactionID);
				                        jQuery('#paymentSection').slideUp('slow');
				                        jQuery('#orderInfo').slideDown('slow');
				                        jQuery('#orderInfo').html('<p>Order <span>#'+data.transactionID+'</span> has been submitted successfully.</p>');
				                    }else{
				                        jQuery('#paymentSection').slideUp('slow');
				                        jQuery('#orderInfo').slideDown('slow');
				                        jQuery('#orderInfo').html('<p>Wrong card details given, please try again.</p>');
				                    }
				                }
				            });
						        }
						    });
						});
						/*--//payment section with validation end here--*/

						       }else{
						       	//alert("not valid");
						       }

						});



		        		/*jQuery(".nxtbtn_extend").click(function(event){
		        			event.preventDefault();
		        			alert("click btn");
		        			if(jQuery("#stepy_form21312").valid()){
		        				//jQuery("#stepy_form21312").submit();
		        				alert("valid form");
		        			}else{
		        				alert("Not Valid Yet");
		        			}
		        		});*/
		        	/*--//submit appointment data end here--*/














		        },
		        error: function ( result )
		        {

		        }
		    });

		});
	});
	/*--//create tab for hire consultent--*/




});