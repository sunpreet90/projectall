<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users_model extends CI_Model
{
	private $table_name			= 'users';			// user accounts
	private $profile_table_name	= 'user_profiles';	// user profiles
	private $locations_table_name	= 'ws_user_current_location';	// user profiles
	private $dog_walks_table_name	= 'ws_dog_walks';	// user profiles

	function __construct()
	{
		parent::__construct();

		$ci =& get_instance();
		$this->table_name			= $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
		$this->profile_table_name	= $ci->config->item('db_table_prefix', 'tank_auth').$this->profile_table_name;
	}

	/**
	 * Get user record by Id
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	function get_user_detail_by_id($user_id){

		$this->db->where('id', $user_id);

        $query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
	
	function get_user_by_id($user_id, $activated)
	{
		$this->db->where('id', $user_id);
		$this->db->where('activated', $activated ? 1 : 0);

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}
   
	/**
	 * Get user record by login (username or email)
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_login($login)
	{
		$this->db->where('LOWER(username)=', strtolower($login));
		$this->db->or_where('LOWER(email)=', strtolower($login));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by username
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_username($username)
	{
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by email
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_email($email)
	{
        if(!empty($email)){
		  $this->db->where('LOWER(email)=', strtolower($email));

			$query = $this->db->get($this->table_name);
			if ($query->num_rows() == 1)
            return $query->row();
	    }
		return NULL;
	}
	/**
      Get user by facebook id
	*/
	function get_user_by_facebook_id($facebook_id)
	{
		$this->db->where('facebook_id', $facebook_id);
      
		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Check if username available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Check if email available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->or_where('LOWER(new_email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Create new user record
	 *
	 * @param	array
	 * @param	bool
	 * @return	array
	 */
	function create_user($data, $activated = TRUE)
	{
		$data['created'] = date('Y-m-d H:i:s');
		$data['activated'] = $activated ? 1 : 0;

		if ($this->db->insert($this->table_name, $data)) {
			$user_id = $this->db->insert_id();
			if ($activated)	$this->create_profile($user_id);
			return array('user_id' => $user_id);
		}
		return NULL;
	}
	function create_facebook_user($data)
	{
		$data['created'] = date('Y-m-d H:i:s');
		
        if ($this->db->insert($this->table_name, $data)) {
			$user_id = $this->db->insert_id();

			$this->create_profile($user_id);
			return array('user_id' => $user_id);
		}
		return NULL;
	}

	/**
	 * Activate user if activation key is valid.
	 * Can be called for not activated users only.
	 *
	 * @param	int
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function activate_user($user_id, $activation_key)
	{
		//$this->db->select('1', FALSE);
		//$this->db->where('id', $user_id);
		//if ($activate_by_email) {
			//$this->db->where('new_email_key', $activation_key);
		/*} else {
			$this->db->where('new_password_key', $activation_key);
		}*/
		//$this->db->where('isVerified', 0);
		//$query = $this->db->get($this->table_name);

		//if ($query->num_rows() == 1) {

			$this->db->set('isVerified', 1);
			//$this->db->set('new_email_key', NULL);
			$this->db->where('id', $user_id);
	      	$data = $this->db->update($this->table_name);
            //print_r($data);die;
           
			//$this->create_profile($user_id);
			if($data){
				return TRUE;
			}
			
		//}
		return FALSE;
	}
	function update_email_key($user_id, $verifycode)
	{
		//$this->db->select('1', FALSE);
		// $this->db->where('id', $user_id);
		
		//$this->db->where('isVerified', 0);
		// $query = $this->db->get($this->table_name);

		  // if ($query->num_rows() == 1)
	 	  //echo "oko".$user_id.$verifycode;die;
            $this->db->set('new_email_key', $verifycode);
			// $this->db->where( array ('id' => $user_id ));
			// echo $this->table_name;
	      	$data= $this->db->where( 'id', $user_id )->update($this->table_name);
	      	//$this->db->last_query());die;
            //print_r($data);die;
           
			//$this->create_profile($user_id);
			///echo $data;
		   if($data>=0){
			  return TRUE;
		   }else{
		     return FALSE;
	       }

	}
/*
function activate_user($user_id, $activation_key)
	{
		$this->db->select('1', FALSE);
		$this->db->where('id', $user_id);
		if ($activate_by_email) {
			$this->db->where('new_email_key', $activation_key);
		} else {
			$this->db->where('new_password_key', $activation_key);
		}
		$this->db->where('activated', 0);
		$query = $this->db->get($this->table_name);

		if ($query->num_rows() == 1) {

			$this->db->set('isVerified', 1);
			$this->db->set('new_email_key', NULL);
			$this->db->where('id', $user_id);
			$this->db->update($this->table_name);

			$this->create_profile($user_id);
			return TRUE;
		}
		return FALSE;
	}
*/
	/**
	 * Purge table of non-activated users
	 *
	 * @param	int
	 * @return	void
	 */
	function purge_na($expire_period = 172800)
	{
		$this->db->where('activated', 0);
		$this->db->where('UNIX_TIMESTAMP(created) <', time() - $expire_period);
		$this->db->delete($this->table_name);
	}

	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->delete($this->table_name);
		if ($this->db->affected_rows() > 0) {
			$this->delete_profile($user_id);
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Set new password key for user.
	 * This key can be used for authentication when resetting user's password.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function set_password_key($user_id, $new_pass_key)
	{
		$this->db->set('new_password_key', $new_pass_key);
		$this->db->set('new_password_requested', date('Y-m-d H:i:s'));
		$this->db->where('id', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Check if given password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	int
	 * @return	void
	 */
	function can_reset_password($user_id, $new_pass_key, $expire_period = 900)
	{
		$this->db->select('1', FALSE);
		$this->db->where('id', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		//$this->db->where('UNIX_TIMESTAMP(new_password_requested) >', time() - $expire_period);
         

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 1;
	}

	/**
	 * Change user password if password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	int
	 * @return	bool
	 */
	function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900)
	{
		//echo $user_id."new pass:".$new_pass."new pass k:".$new_pass_key."ex pe:".$expire_period;die;
		//$data = $this->get_user_detail_by_id($user_id);
       
        
        $this->db->set('password', $new_pass);
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		//$this->db->where('new_password_requested >=', time() - $expire_period);
		// $this->db->where('DATE_FORMAT(new_password_requested, "%Y-%m-%d %H:%i:%s") >=', time() - $expire_period);

    
       
      
    
     /* echo strtotime($data->new_password_requested);
      echo "<br>";
      echo  time() - $expire_period;die;*/
     //$this->db->update('login_table',$data);

   $this->db->update($this->table_name); 
      // echo $this->db->last_query();die;
	return $this->db->affected_rows() > 0;
	}
	
	/**
	 * Change user password if password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	int
	 * @return	bool
	 */
	/* function verify_forgot_password($user_id,$verification_code, $expire_period = 15)
	{
	    $data = $this->get_user_detail_by_id($user_id);
        
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('new_password_key', $verification_code);

		
        if(strtotime($data->new_password_requested) >= time() - $expire_period){
            $this->db->update($this->table_name); 
	         
		   return $this->db->affected_rows() > 0;
        }
       return "linkexpire";
	} */
	
		
	function verify_forgot_password($user_id,$verification_code){
        $data = $this->get_user_detail_by_id($user_id);
        
        $this->db->set('new_password_key', NULL);
        $this->db->set('new_password_requested', NULL);
        $this->db->where('id', $user_id);
        $this->db->where('new_password_key', $verification_code);

           date_default_timezone_set("UTC");
           $currentDateTime = date( 'Y-m-d H:i:s');

          $diff_two_date = $this->get_date_diff($data->new_password_requested, $currentDateTime);
           
        if($diff_two_date <= 15){
               $this->db->update($this->table_name);
               return $this->db->affected_rows() > 0;
        }
       return "linkexpire";
    }
	/**
	 * Change user password
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function change_password($user_id, $new_pass,$org_pass)
	{
		$this->db->set('password', $new_pass);
		$this->db->set('org_password', $org_pass);
		$this->db->where('id', $user_id);
        
		$this->db->update($this->table_name);
		
		return $this->db->affected_rows() > 0;
	}
	/**
	 * Add or Change user location
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function add_user_location($user_id, $latitude,$longitude)
	{

        $this->db->where('user_id', $user_id);
        
        $query = $this->db->get($this->locations_table_name);
		if ($query->num_rows() == 1){
	            $this->db->set('latitude', $latitude);
			    $this->db->set('longitude', $longitude);
			    $this->db->where('user_id', $user_id);

                $this->db->update($this->locations_table_name);
		    return $this->db->affected_rows() > 0;
		}else{
			   $data =array();
			   $data = array('user_id'=>$user_id,'latitude'=>$latitude,'longitude'=>$longitude,'created'=>date('Y-m-d H:i:s'));
              
               if($this->db->insert($this->locations_table_name, $data)){
					 return true;
				}
            return false;
		} 
	}
	/**
	 * Add or Change user location
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function add_user_availability($user_id, $is_available)
	{

        $this->db->where('user_id', $user_id);
        
        $query = $this->db->get($this->locations_table_name);
		if ($query->num_rows() == 1){
	            $this->db->set('is_available', $is_available);
			    $this->db->where('user_id', $user_id);

                $this->db->update($this->locations_table_name);
		    return $this->db->affected_rows() > 0;
		}else{
			   $data =array();
			   $data = array('user_id'=>$user_id,'is_available'=>$is_available,'created'=>date('Y-m-d H:i:s'));
              
               if($this->db->insert($this->locations_table_name, $data)){
					 return true;
				}
        } 
		 return false;
	}
	/**
	 * Add or search user location
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function search_walkers_locations($fLat,$fLon)
	{
// 				,
/* ACOS( SIN( RADIANS( `latitude` ) ) * SIN( RADIANS( $fLat ) ) + COS( RADIANS( `latitude` ) )
 * COS( RADIANS( $fLat )) * COS( RADIANS( `longitude` ) - RADIANS( $fLon )) ) * 6380 AS `distance`*/
        	$this->db
			->select("ws_users.id, ws_users.name, ws_users.gender,ws_users.profile_pic,ws_users.fb_pic, ws_users.dob, ws_user_current_location.latitude, ws_user_current_location.longitude
			")
			->select_avg("ws_user_ratings.rating") 
			->from("ws_user_current_location")
			->join("ws_users", "ws_user_current_location.user_id = ws_users.id")
			->join("ws_user_ratings", "ws_user_ratings.rated_to = ws_users.id", 'left')
			->group_by("ws_users.id")
			->where(array("ACOS( SIN( RADIANS( `latitude` ) ) * SIN( RADIANS( $fLat ) ) + COS( RADIANS( `latitude` ) )
			* COS( RADIANS( $fLat )) * COS( RADIANS( `longitude` ) - RADIANS( $fLon )) ) * 6380 <" => 8,
			'ws_users.user_type' => 2,'is_available' => 1));

			$query = $this->db->get()->result_array();
			if($query > 0 && !empty($query)){

				  foreach($query as $Kwalker=>$Vwalker){
                   $this->db
					->select("ws_dog_walks.id,ws_dog_walks.walker_id")
					->from("ws_dog_walks")
					->where(array('walker_id'=> $Vwalker['id'],'walk_status'=>3));
                    $totalWalks = $this->db->get()->result_array();

					$walkerTotalwalks = count($totalWalks);
					$query[$Kwalker]['no_of_walks'] = $walkerTotalwalks;
					
                }
				return $query;
			}
			return $query;
            //print_r($this->db->last_query());
    }
    /**
    Function for get walker rating deatails
    */
    function get_walker_rating_details($user_id){
          $this->db
			->select("r.id, r.rated_by, u2.name, r.rating, r.rated_date, r.comment,
				")
			->from("ws_user_ratings r")
			->join("ws_users u", "r.rated_to = u.id")
			->join("ws_users u2", "u2.id = r.rated_by")
			->where('rated_to' , $user_id);
			
			$query = $this->db->get()->result_array();
			if($query > 0){
				return $query;
			}
			return false;
    }
    /**
    Function for add walk request
    */
    function add_walk_request($walk_data){

         $this->db->where(array('owner_id' => $walk_data['owner_id'],'walker_id'=> $walk_data['walker_id']));
         $this->db->limit(1, 0);
         $query = $this->db->order_by('id', 'DESC')
                           ->get($this->dog_walks_table_name)
                           ->result_array();
       
          if(!empty($query)){

               $created_time = $query[0]['created'];
             
			   $dateNow = strtotime(date('Y-m-d H:i:s'));
               date_default_timezone_set("UTC");
			   $currentDateTime = date( 'Y-m-d H:i:s', $dateNow);

               $diff_two_date = $this->get_date_diff($created_time, $currentDateTime);
			  
            if($diff_two_date >= 5){
                $this->db->set($walk_data);
	            $this->db->insert($this->dog_walks_table_name);

	             $this->db
					->select("d.id, d.owner_id, d.walker_id, d.start_time, d.end_time, d.pick_up_location, d.pick_up_lat,d.pick_up_long, d.no_of_dogs, d.instructions, d.created, u.name, u.user_type, u.profile_pic, u.fb_pic  
						")
					->from("ws_dog_walks d")
					->join("ws_users u", "d.walker_id = u.id")
					->order_by("created", "desc")
					->limit(1, 0)
					->where(array('owner_id' => $walk_data['owner_id'],'walker_id'=> $walk_data['walker_id']));
					
					$walkerDetail = $this->db->get()->result_array();
                    
                    if(!empty($walkerDetail)){
		            	return $walkerDetail[0]; 
	                }
                 return false;
	        
	        }else{
            	return false;
            }

		}else{ 
         	    $this->db->set($walk_data);
	            $this->db->insert($this->dog_walks_table_name);

	            $this->db
					->select("d.id, d.owner_id, d.walker_id, d.start_time, d.end_time, d.pick_up_location, d.pick_up_lat,d.pick_up_long, d.no_of_dogs, d.instructions, d.created, u.name, u.user_type, u.profile_pic, u.fb_pic  
						")
					->from("ws_dog_walks d")
					->join("ws_users u", "d.walker_id = u.id")
					->order_by("created", "desc")
					->limit(1, 0)
					->where(array('owner_id' => $walk_data['owner_id'],'walker_id'=> $walk_data['walker_id']));
					
					$walkerDetail = $this->db->get()->result_array();
                     
				
		            if(!empty($walkerDetail)){
		            	return $walkerDetail[0]; 
	                }
                 return false;
	    }
    }

function get_date_diff($date1, $date2)
{
 $old_date = strtotime($date1);
 $new_date = strtotime($date2);
	//$date2 = time();
$datediff = $new_date-$old_date; //strtotime($date2) - strtotime($date1);

$get_minutes = floor($datediff/60);
return ceil($get_minutes);
}

    /**
	 * Update user profile
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function update_user_profile($user_id, $dob,$gender,$profile_pic,$paypalAccount,$name,$phone,$user_type)
	{
         
		$this->db->set('dob', $dob);
		$this->db->set('gender', $gender);

		if($profile_pic != ""){
           $this->db->set('profile_pic', $profile_pic);
           $this->db->set('fb_pic', "");
		}
		$this->db->set('paypalAccount', $paypalAccount);
		$this->db->set('isProfileCompleted', 1);
		$this->db->set('name', $name);
		$this->db->set('phone', $phone);
		$this->db->set('user_type', $user_type);

		$this->db->where('id', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	 

	/**
	 * Set new email for user (may be activated or not).
	 * The new email cannot be used for login or notification before it is activated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function set_new_email($user_id, $new_email, $new_email_key, $activated)
	{
		$this->db->set($activated ? 'new_email' : 'email', $new_email);
		$this->db->set('new_email_key', $new_email_key);
		$this->db->where('id', $user_id);
		$this->db->where('activated', $activated ? 1 : 0);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Activate new email (replace old email with new one) if activation key is valid.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function activate_new_email($user_id, $new_email_key)
	{
		$this->db->set('email', 'new_email', FALSE);
		$this->db->set('new_email', NULL);
		$this->db->set('new_email_key', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('new_email_key', $new_email_key);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Update user login info, such as IP-address or login time, and
	 * clear previously generated (but not activated) passwords.
	 *
	 * @param	int
	 * @param	bool
	 * @param	bool
	 * @return	void
	 */
	function update_login_info($user_id, $record_ip, $record_time)
	{
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);
        
		if ($record_ip)		$this->db->set('last_ip', $this->input->ip_address());
		if ($record_time)	$this->db->set('last_login', date('Y-m-d H:i:s'));

		$this->db->where('id', $user_id);
		$this->db->update($this->table_name);
	}

	/**
	 * Ban user
	 *
	 * @param	int
	 * @param	string
	 * @return	void
	 */
	function ban_user($user_id, $reason = NULL)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 1,
			'ban_reason'	=> $reason,
		));
	}

	/**
	 * Unban user
	 *
	 * @param	int
	 * @return	void
	 */
	function unban_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 0,
			'ban_reason'	=> NULL,
		));
	}

	/**
	 * Create an empty profile for a new user
	 *
	 * @param	int
	 * @return	bool
	 */
	private function create_profile($user_id)
	{
		$this->db->set('user_id', $user_id);
		return $this->db->insert($this->profile_table_name);
	}

	/**
	 * Delete user profile
	 *
	 * @param	int
	 * @return	void
	 */
	private function delete_profile($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete($this->profile_table_name);
	}
}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */