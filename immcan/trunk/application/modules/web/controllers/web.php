<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//echo "hello"; die;

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class web extends MX_Controller {

	function __construct() {

		/**
		 * @author:	Harish Chauhan
		 *
		 * @desc: 	Parent constructors are not called implicitly if the child class defines a constructor.
		 * In order to run a parent constructor, a call to parent::__construct() within the child constructor is required.
		 * If the child does not define a constructor then it may be inherited from the parent class just like a normal class method 
		 * (if it was not declared as private).
		 */
		parent::__construct();
		//$this->load->module('user');


		$this->load->library('tank_auth');
		$this->load->helper('form');
		$this->load->helper('url');
		//$this->ci->load->config('tank_auth', TRUE);
		// $this->load->library('utils');
		/*if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}*/
		$this->load->library('form_validation');
		$this->load->model('web_model');
		//$this->load->model('consultant_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'register_appointment';


		
	}

	/**
	 * Loads th index page after login || The default login page
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function index() {
		   //echo "Inside Home Page";
		/*if (!$this->tank_auth->is_logged_in()) {
			        
			redirect('/auth/login/');
		} else {*/
			//$id = $_REQUEST['id'];
			//print_r($id); die;
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			//$data['consultants'] = $this->consultant_model->get_consultants_all();
			$data['consultants'] = $this->web_model->get_consultant_listing();
			//$data['consult'] = $this->web_model->result_view_ajax($id);
			//echo "<pre>"; print_r($data); die();
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('website')
			->build('web',isset($data) ? $data : NULL);
		//}
	}



	public function tabs(){
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('website')
			->build('web',isset($data) ? $data : NULL);

			


	}

	/* Display function to display the selected consultant*/
	public function get_view_ajax(){
			$id = $_REQUEST['id'];
			//$id = $this->uri->segment(4);
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$data['consult'] = $this->web_model->result_view_ajax($id);
			//echo "<pre>"; print_r($data['consult']); die;
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('website')
			->build('web',isset($data) ? $data : NULL);
			print_r($data);
	}
	/* Display function to display the selected consultant*/


	/* Display function to Add Book Consultation Form   */

	public function book_consultation(){
		//echo "Inside Function"; die;

		/*$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('website')
		->build('web',isset($data) ? $data : NULL);*/

		//echo "<pre>"; print_r($_POST);die;
		if ($this->input->post()) 
		{

			/*$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('firstname', 'firstname', 'required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			
            $this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('accept_agreement', 'Accept Agreement', 'required');
			$this->form_validation->set_rules('current_address', 'Current Address', 'required');
			$this->form_validation->set_rules('skype_id', 'Skype ID', 'required');
			$this->form_validation->set_rules('immigration_type', 'Immigration Type', 'required');
			$this->form_validation->set_rules('living_canada', 'Living Canada', 'required');
			$this->form_validation->set_rules('immigration_status', 'Immigration Status', 'required');


		   
            if ( $this->form_validation->run() == FALSE )
			{
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('website')
				->build('web',isset($data) ? $data : NULL);
			
			}else{*/

				$firstname = $this->input->post('firstname');
				$lastname = $this->input->post('lastname');
				$phone =$this->input->post('phone');
				$email =$this->input->post('email');
				$accept_agreement =$this->input->post('accept_agreement');
				$current_address =$this->input->post('current_address');
				$skype_id =$this->input->post('skype_id');
				$immigration_type =$this->input->post('immigration_type');
				$living_canada =$this->input->post('living_canada');
				$immigration_status =$this->input->post('immigration_status');
				$consultation_document1 = $this->do_upload();				
               
                $users_data=array(
                   'user_id' => '1',
                   'consultation_id' => 1,
                   'firstname'       	=> $firstname,
                   'lastname' 	=> $lastname,
                   'phone' 	=> $phone,
				   'email' => $email,
				   'accept_agreement' => $accept_agreement,
                   'current_address' 	=> $current_address,
                   'skype_id' 	=> $skype_id,
                   'immigration_type' 	=> $immigration_type,                   
                   'living_canada'	=> $living_canada,
                   'immigration_status'	=> $immigration_status,
                   'are_sponser' => 'yes',
                   'children_matter' => 'yes',
                   'sponsored_immigration' => 'no',
                   'sponsored_by_spouse' => 'no',
                   'estimate_date' => '23/12/2017',
                   'refused_application' => 'yes',
                   'notice_assesment' => 'no',
                   'immigration_situation' => 'not available',
                   'consultation_document1' => $consultation_document1,
                   'consultation_document2' => 'Still Not',
                   'referred_by' => 'admin',
                   'applied_residence' => 'yes',
                   'arrive_date' => '23/02/2018',
                   'skype_account' => 'yes',
                   'created_at' => $this->created
                   //'consultant_image'	=> $consultant_images,
                   
			    );
			    //echo "<pre>"; print_r($users_data); die;
				if ( $this->web_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Booking Consultation have been added successfully');
					$_POST = '';
					redirect('web');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('web');
				}
			}			
		//}
	

	}


	/**
	 * Prints hello message on page load
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function hello() {
                     
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('asd')
			->build('welcome_message',isset($data) ? $data : NULL);
		
		}
	}




function do_upload(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			'upload_path' => './uploads/documents',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultation_document1')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }



}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */