<!-- page heading start-->
<div class="page-heading">
    <h3>
        International Partner Agent Inquiry
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> International Partner Agent Inquiry </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                  International Partner Agent Inquiry
                </header>
                <div class="panel-body">
                    <form class="form-horizontal adminex-form" method="post" action="<?=base_url()?>partner/add" enctype='multipart/form-data'>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Name</label>
                            <div class="col-sm-4">
                                <input value="<?php echo set_value('partner_name'); ?>" type="text" name="partner_name" class="form-control">
                            </div>

                            <label class="col-sm-2 col-sm-2 control-label">Email</label>
                            <div class="col-sm-4">
                                <input value="<?php echo set_value('partner_email'); ?>" type="text" name="partner_email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Address</label>
                            <div class="col-sm-4">
                                <input value="<?php echo set_value('partner_address'); ?>" type="text" name="partner_address"  class="form-control" placeholder="">
                            </div>


                            <label class="col-sm-2 col-sm-2 control-label">Phone Number</label>
                            <div class="col-sm-4">
                                <input value="<?php echo set_value('partner_phone'); ?>" type="text" name="partner_phone"  class="form-control" placeholder="">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Education</label>
                            <div class="col-sm-4">
                                <input value="<?php echo set_value('partner_education'); ?>" type="text" name="partner_education"  class="form-control" placeholder="">
                            </div>

                            <label class="col-sm-2 col-sm-2 control-label">Profession</label>
                            <div class="col-sm-4">
                                <input value="<?php echo set_value('partner_profession'); ?>" type="text" name="partner_profession"  class="form-control" placeholder="">
                            </div>


                        </div>
                        
                
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Have you worked in Immigration matters before?</label>
                            <div class="col-sm-4">

                                <input type="radio" name="worked" value="yes"  checked> Yes
                                <input type="radio" name="worked" value="no" > No
                                
                            </div>

                            <label class="col-sm-2 col-sm-2 control-label">How did you hear about us?</label>
                            <div class="col-sm-4">
                                <input value="<?php echo set_value('hear_from'); ?>" type="text" name="hear_from"  class="form-control" placeholder="">
                            </div>


                        </div>



                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Captcha</label>
                            <div class="col-sm-6">
                                <div class="g-recaptcha" data-sitekey="6Le5mVkUAAAAAK141Xf876Kqka_gJBOktOluO-yM"></div>
                              <!--   <input id="num1" name="num1" readonly="readonly" class="sum" value="<?php echo rand(1,4) ?>" /> + 
                                <input id="num2" name="num2" readonly="readonly" class="sum" value="<?php echo rand(5,9) ?>" /> =
                                <input type="text" name="captcha" id="captcha" class="captcha" maxlength="2" />
                                <span id="spambot">(Are you human, or spambot?)</span> -->

                                <!-- <input value="<?php echo set_value('consultant_price'); ?>" type="text" name="consultant_price"  class="form-control" placeholder=""> -->
                            </div>

                            


                        </div>



                        <!-- <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control required_shop_image error file-upload" name="consultant_image" onchange="ValidateSingleInput(this);">
                            </div>
                        </div>
                       



                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Description</label>
                            <div class="col-sm-10">
                                <textarea name="consultant_description" value="<?php echo set_value('consultant_description'); ?>" style="width:350px; height:200px;"></textarea>
                                
                            </div>
                        </div> -->
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Add Partner Info</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->

<script>
var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];    
function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}
</script>