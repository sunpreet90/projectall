<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//require_once('phpass-0.1/PasswordHash.php');
//require_once('phpass-0.1/PasswordHash.php');
/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Partner extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tank_auth');
		$this->load->helper('form');
		$this->load->helper('url');
		//$this->ci->load->config('tank_auth', TRUE);
		// $this->load->library('utils');
		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}
		$this->load->library('form_validation');
		$this->load->model('partner_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'partner_agent';
	}

	function index(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$data['partners'] = $this->partner_model->get_partners_all();
		//echo "<pre>"; print_r($data['referrals']); die;

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('partners',isset($data) ? $data : NULL);
	}


	/* Use This function to display form on button Click*/
	function partner_add(){
		$data['user_id'] = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('partners_add',isset($data) ? $data : NULL);
	}



	function add()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('partners_add',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('partner_name', 'Partner Name', 'required');
			$this->form_validation->set_rules('partner_email', 'Partner Email', 'required');
			$this->form_validation->set_rules('partner_phone', 'Partner Phone', 'required');
			
            $this->form_validation->set_rules('partner_address', 'Partner Address', 'required');
			$this->form_validation->set_rules('partner_education', 'Partner Education', 'required');
			$this->form_validation->set_rules('partner_profession', 'Partner Profession', 'required');
			$this->form_validation->set_rules('worked', 'Worked', 'required');
			$this->form_validation->set_rules('hear_from', 'Hear About Us!', 'required');
			$this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');

			//$this->form_validation->set_rules('yes_details', 'Yes Details', 'required');


		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('partners_add',isset($data) ? $data : NULL);
			
			}else{

				$partner_name = $this->input->post('partner_name');
				$partner_email = $this->input->post('partner_email');
				$partner_phone = $this->input->post('partner_phone');
				$partner_address =$this->input->post('partner_address');
				$partner_education =$this->input->post('partner_education');
				$partner_profession =$this->input->post('partner_profession');
				$worked =$this->input->post('worked');
				$hear_from =$this->input->post('hear_from');
				$yes_details =$this->input->post('yes_details');	
				$captcha =$this->input->post('g-recaptcha-response');


				if(!$captcha){
		          echo '<h2>Please check the the captcha form.</h2>';
		          exit;
		        }
		        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Le5mVkUAAAAAATqUdSq_fOCsw2kn6YRbNdi1tWX&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
		        if($response['success'] == false)
		        {
		          echo '<h2>You are spammer ! Get the @$%K out</h2>';
		        }
		        else
		        {
		          echo '<h2>Thanks for posting comment.</h2>';
		        }			
				
				//$consultant_images = $this->do_upload();				
               
                $users_data=array(
                   'partner_name'       	=> $partner_name,
                   'user_id' => $data['user_id'],
                   'partner_email' 	=> $partner_email,
                   'partner_phone' 	=> $partner_phone,
				   //'age' => $age,
				   'partner_address' => $partner_address,
                   'partner_education' 	=> $partner_education,
                   'partner_profession' 	=> $partner_profession,
                   'worked' 	=> $worked,                   
                   'hear_from' 	=> $hear_from,                   
                   'yes_details' 	=> $yes_details,                   
                                    
                   'created_at'	=> $this->created,
                   //'consultant_image'	=> $consultant_images,
                   
			    );
			    //echo "<pre>"; print_r($users_data); die;
				if ( $this->partner_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'International Partner Agent Info have been added successfully');
					$_POST = '';
					redirect('partner');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('partner/add');
				}
			}			
		}
	}




	function do_upload(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			'upload_path' => './uploads',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultant_image')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }




    function userkey_exists($key,$user_id) {
	    $this->table->mail_exists($key,$user_id);
	}
	
	function delete()
	{
		$user_id = $this->uri->segment(4);
		if($this->partner_model->delete_partner($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url().'partner');
	}
	
	
	

}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */