<!-- page heading start-->
        <div class="page-heading">
            <h3>
                Page
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="#">Page</a>
                </li>
                <li class="active">Add Page </li>
            </ul>
        </div>
        <!-- page heading end-->

        <section class="panel">
            
            <header class="panel-heading">
                Create a new page
            </header>

            <div class="panel-body">

                <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>

                <div class="form">
                    <form class="form-horizontal adminex-form" method="post" action="<?php echo base_url('page/add')?>" novalidate="novalidate" enctype='multipart/form-data'>
                        <div class="form-group ">
                            <label for="title" class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_title" class=" form-control" id="post_title">
                            </div>
                        </div>
                         <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Content</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='post_content' class="wysihtml5 form-control" rows="9"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Status</label>
                            <div class="col-lg-10">
                               <select name="status" class="form-control m-bot15">
                                        <option value="published">Publish</option>
                                        <option value="pending">Pending</option>
                                        <option value="trash">Trash</option>
                                    </select>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Page Template</label>
                            <div class="col-lg-10">
                               <select name="page_template" class="form-control m-bot15">
                                        <option value="default">Default</option>
                                        <option value="contact">Contact</option>
                                        <option value="faq">Faq</option>
                                        <option value="about-us">About Us</option>
                                        <option value="service">Service</option>
                                    </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Featured Image:</label>
                            <div class="col-sm-10">
                                <input type="file" name="featured_image" class="form-control required_shop_image error file-upload" id="featured_image"  onchange="ValidateSingleInput(this);">                                                 
                            </div>
                        </div>

                        
                        <div class="form-group ">
                            <label for="post_button" class="control-label col-lg-2">Button Label</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_button_label" class=" form-control" id="post_button_label">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="post_link" class="control-label col-lg-2">Button Link</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_button_link" class=" form-control" id="post_button_link">
                            </div>
                        </div>

                        <input type="hidden" name="action" value="1">

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <button class="btn btn-default" type="button">Cancel</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>