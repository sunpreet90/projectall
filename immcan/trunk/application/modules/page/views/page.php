<div class="page-heading">
    <h3>Page Listing</h3>
    <ul class="breadcrumb">
        <li>
            <a href="<?= site_url('welcome')?>">Dashboard</a>
        </li>
        <li>
            <a href="#">Pages</a>
        </li>
        <li class="active"> View All Pages </li>
    </ul>
</div>
        <!-- page heading end-->

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">

                <section class="panel">
                    <header class="panel-heading">
                        Quick Access
                        <span class="tools pull-right">
                            <a class="fa fa-chevron-down" href="javascript:;"></a>
                            <a class="fa fa-times" href="javascript:;"></a>
                        </span>
                    </header>
                    <div class="panel-body">

                        <a href="<?= base_url('admin/home-page-settings')?>"><button class="btn btn-info" type="button">Home Page</button></a>
                        <a href="<?= base_url('admin/faq')?>"><button class="btn btn-info" type="button">Frequently Asked Questions's</button></a>

                        <!-- <a href="<?= base_url('admin/service')?>"><button class="btn btn-info" type="button">Service Page</button></a>
                        <a href="<?= base_url('#')?>"><button class="btn btn-info" type="button">About Us Page</button></a>
                        <a href="<?= base_url('admin/our_team')?>"><button class="btn btn-info" type="button">About You Page</button></a>
                        <a href="<?= base_url('contact/104')?>"><button class="btn btn-info" type="button">Contact Us</button></a> -->
                    
                    </div>
                </section>

        <section class="panel">
            
            <header class="panel-heading">
                Manage Pages
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                    
                </span>
            </header>

            <form id="filterform1" class="" action="#" method="post" role="">
                <div class="col-md-2 col-sm-3">
                    <label for="usr">Filter by status</label>
                    <select class="form-control category_select">
                        <option value="all">All</option>
                        <option value="published">Published</option>
                        <option value="pending">Pending</option>
                        <option value="trash">Trash</option>
                    </select>
            </div>
            
    
        <div class="clearfix"></div>
      </form>
        
        <header class="panel-heading">

            Pages Listing

            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                
             </span>
        </header>

        <div class="panel-body">
             <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
        <tr>
            <th>Title</th>
            <th>Status</th>
            <th>Created On</th>
            <th class="hidden-phone">Edit</th>
            <th class="hidden-phone">View</th>
            <th class="hidden-phone">Delete</th>
            
        </tr>
    </thead>
         <tbody>
             <?php 
                if ( !empty( $page )) 
                {
                    foreach ($page as $note) 
                    {

                        //pr($note);
                    ?>
                    <tr class="gradeX">
                        <td><?=$note['post_title']?></td>
                        <td class="text-capitalize"><?=$note['status']?></td>
                        <td><?=$note['created_on']?></td>
                        <!-- <td class="center hidden-phone"><a href="<?=base_url('page')?>/edit/id/<?=$note['id']?>">Edit</a></td>
                        <td class="center hidden-phone"><a target="_blank" href="<?=base_url('privacy-policy')?>">View</a></td> -->
                        <td class="center hidden-phone"><a href="<?=base_url('page')?>/edit/id/<?=$note['id']?>">Edit</a></td>
                        <td class="center hidden-phone"><a target="_blank" href="<?=base_url('page')?>/<?=$note['post_slug']?>/<?=$note['id']?>">View</a></td>
                        <!-- <td class="center hidden-phone"><a target="_blank" href="<?=base_url('privacy-policy')?>">View</a></td> -->
                        <td class="center hidden-phone"><a href="<?=base_url('page')?>/trash/id/<?=$note['id'];?>"><i class="fa fa-eraser"></i>
                        </a>
                       
                        <!-- <a href="#">
                            <i class="fa fa-eraser"></i>
                        </a> -->

                        </td>
                    </tr>
                    <?php }
                }
                ?>
            </tbody>

                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th class="hidden-phone">Edit</th>
                        <th class="hidden-phone">View</th>
                        <th class="hidden-phone">Delete</th>
                    </tr>
                    </tfoot>
        </table>
        </div>
        </div>
        </section>
        </div>
        </div>



<script type="text/javascript">

/*    jQuery.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var location = $(".category_select option:selected").val();
            var columnValue = data[1];

            if(location.toLowerCase() == 'all'){
              return true;
            }
            if(columnValue.toLowerCase().indexOf(location) >= 0){
              return true;
            }
            return false;
        }
    );
*/
</script>
<!--dynamic table initialization -->
<script>
/*jQuery(document).ready(function() {

   var table = jQuery('table.display').DataTable( {
            //"scrollX": true,
            "aaSorting": [[ 4, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend : 'excelHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8]
                    }
                },
                {
                    extend : 'csvHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8]
                    }
                },
                {
                    extend : 'pdfHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8]
                    }
                }]
        });

    jQuery( "#min_quantity,#max_quantity" ).on('keyup', function(){
    table.draw();
   });
   jQuery(".category_select").on('change', function(){
    alert('here');
    table.draw();
   }); 
 
 } );*/
</script>