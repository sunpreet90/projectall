<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Page extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('page_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 	= $this->page_model->get_page();

		//pr($data);

		$data['published_page'] 	= $this->page_model->get_page();
		$data['pending_page'] 	= $this->page_model->get_page();
		$data['trash_page'] 	= $this->page_model->get_page();

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('page',isset($data) ? $data : NULL);
	}
	

	/* Add Featured Image Start Here */

			function do_upload1(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			'upload_path' => './uploads/featured',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('featured_image')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }

			/* Add Featured Image End Here */	

	public function add($value='')
	{
		# code..

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		//$data['page'] 		= $this->page_model->get_notes();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$post_slug_title = trim( $this->input->post('post_title') );
      		$post_slug_simple = preg_replace("/[^ \w]+/", "", $post_slug_title);
      		$post_slug = str_replace(' ', '-', $post_slug_simple);
			$consultant_images = $this->do_upload1();
			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => strtolower($post_slug), 
								'post_content' => $this->input->post('post_content'),
								'featured_image' 	=> 'uploads/featured/'.$consultant_images,
								'status' => $this->input->post('status'),
								'page_template' => $this->input->post('page_template')
								/*'terms_and_conditions'=>'dummy',*/
							);
			
			$insert_id = $this->page_model->page_insert( $this->table, $insert_data );

			if( !$insert_id) 
			{
				die('error in page insert');
			}
			else
			{
				add_post_meta( $post_id = $insert_id, $meta_key = 'post_button_label', $meta_value = $this->input->post('post_button_label') );
				add_post_meta( $post_id = $insert_id, $meta_key = 'post_button_link', $meta_value = $this->input->post('post_button_link') );
				
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page created successfully');
				redirect('page');
			}
		}

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('page_add',isset($data) ? $data : NULL);
	}	

	public function edit($value='')
	{
		# code..
		
		$page_id = $this->uri->segment(4);

		//pr($this->input->post()); die();

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['page'] 		= $this->page_model->get_pages($page_id);

		$data['status']		= '';
		$data['message']	= '';

      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
          {
			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => '#', 
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status'),
								'page_template' => $this->input->post('page_template')
							);

               $this->db->where('id', $page_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('page');
			}
			else
			{
				add_post_meta( $post_id = $page_id, $meta_key = 'post_button_label', $meta_value = $this->input->post('post_button_label') );
				add_post_meta( $post_id = $page_id, $meta_key = 'post_button_link', $meta_value = $this->input->post('post_button_link') );
				
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page edited successfully');
				redirect('page');
			}
		}

		}

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
	
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('page_edit',isset($data) ? $data : NULL);

	}	

	/* Page.php END here */


	function add_z()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('notes')
		->build('notes_add',isset($data) ? $data : NULL);


		if ($this->input->post() ) 
		{	

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('note_title', 'Note title', 'required');
			$this->form_validation->set_rules('note_description', 'Note description', 'required');		
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('page')
				->build('notes_add',isset($data) ? $data : NULL);
			
			}else{

				$note_title =$this->input->post('note_title');
				$note_description = $this->input->post('note_description');

				$notes_data=array(
                   'note_title'       	=> $note_title,
                   'note_description' 	=> $note_description,
                   'created'	=> $this->created
			    );
				if ( $this->note_model->add(  $this->table, $notes_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Note added successfully');
					$_POST = '';
					redirect('notes/add');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('notes/add');
				}
			}			
		}
	}

	function edit_z()
	{
		$pages_id = $this->uri->segment(1);
		$pages='';
		$pages = $this->page_model->get_pages( $pages_id );

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['note']		= $note;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('page_edit',isset($data) ? $data : NULL);


		//echo $note_id;	die();
		if ($this->input->post() ) 
		{	
			//echo $note_id;	die();

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('post_title', 'post title', 'required');
			$this->form_validation->set_rules('post_content', 'post content', 'required');
			$this->form_validation->set_rules('status', 'status', 'required');			
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
				redirect('page/edit/id/'.$pages_id);		
			
			}else{

				$post_title =$this->input->post('post_title');
				$post_content = $this->input->post('post_content');
				$status = $this->input->post('status');



				$page_data=array(
                   'post_title'       	=> $post_title,
                   'post_content' 	=> $post_content,
                   'status'        => $status,
                   'created'	=> $this->created
			    );

			    $where_data=array( 'id' => $pages_id );

				if ( $this->page_model->updateWhere(  $this->table, $where_data, $page_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Note Updated successfully');
					$_POST = '';
					redirect('notes/edit/id/'.$pages_id);		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('page/edit/id/'.$pages_id);
				}
			}			
		}
	}

	public function delete()
	{
		$page_id = $this->uri->segment(4);
		

		$this->page_model->delete_page($page_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('page');
	}
}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */