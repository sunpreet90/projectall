<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Web_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'consultants';
	}
	public function mail_exists($email,$user_id)
	{
	    $this->db->where(array('email'=>$email,'id !='=>$user_id));
	    $query = $this->db->get($this->table);
	    print_r($query);die;
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }
	}

 	/*This function display all consultants result */

	function get_consultant_listing(){
		$this->db->select('*');
		$this->db->from('ws_consultants');
		//$this->db->where('id',$id);
		return $this->db->get()->result();
		
	}


	function result_view_ajax($id){
		$this->db->select('*');
		$this->db->from('ws_consultants');
		$this->db->where('id',$id);
		return $this->db->get()->result();
	}




	
	/*function result_view_ajax($id){

	$this->db->where('id', $id);

	$query = $this->db->get('ws_consultants');

	$options = array();

	if($query->result()){
	    $result = $query->result();

	    foreach($result as $row)
	    {
	        $options[$row->id] = $row->name;
	    }   
	    return $options;
	  } 
	}*/
	


	/* view consultant function end */


	public function add_user()
	{
		$password =$this->input->post('password');
        $hashed_password = $this->tank_auth->get_hash_password($password);
	    $data=array(
	      'name'=>$this->input->post('name'),
	      'email'=>$this->input->post('email'),
	      'phone'=>$this->input->post('phone'),
	      'password'=>$hashed_password,
	      'user_type'=> 2
	    );
	  	$this->db->insert('ws_users',$data);

	  	$new_user_id = $this->db->insert_id();

	  	$query = $this->db->get_where('ws_users', array('id' => $new_user_id), 1);
    	if ( $query->num_rows() > 0 )
	    {
	        $row = $query->row_array();
			$data["name"] = $row['name']." signed up as new user";
		
			$data = array(
		        'type' => 'new_user',
		        'user_id'  => $new_user_id,
		        'message'  => $data["name"]
			);

			$this->db->insert('ws_notifications', $data);
	    }


	  	return $this->db->insert_id();
	}

	public function email_check($email)
    {     
        $this->db->get_where('ws_users', array('email' => $email), 1);
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;      
    }

    public function login($email,$password){
    	$query = $this->db->get_where('ws_users', array('email' => $email), 1);
    	if ( $query->num_rows() > 0 )
	    {
	        $row = $query->row_array();
	        if (crypt($password, $row['password']) == $row['password']) 
			{
				$data["user_id"] = $row['id'];
				$data["username"] = $row['name'];
				$data["email"] = $row['email'];
				$this->session->set_userdata($data);
				return true;
			}else{
				return false;
			}
	    }
    }



}

/* End of file model.php */