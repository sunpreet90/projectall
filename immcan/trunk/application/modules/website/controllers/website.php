<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Website extends MX_Controller {

	function __construct() {

		/**
		 * @author:	Harish Chauhan
		 *
		 * @desc: 	Parent constructors are not called implicitly if the child class defines a constructor.
		 * In order to run a parent constructor, a call to parent::__construct() within the child constructor is required.
		 * If the child does not define a constructor then it may be inherited from the parent class just like a normal class method 
		 * (if it was not declared as private).
		 */
		parent::__construct();
		$this->load->library('tank_auth');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->library('email'); 
		$this->form_validation->CI =& $this;
		$this->load->model('web_model');
		$this->load->model('website_model');
		//$this->load->model('consultant_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'users';
		$this->table1 = $this->prefix.'partner_agent';
		$this->table2 = $this->prefix.'client_referral';
		$this->table3 = $this->prefix.'consultants';
		$this->table4 = $this->prefix.'register_appointment';

	}

	/**
	 * Loads th index page after login || The default login page
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function index() {

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['consultants'] = $this->website_model->get_consultant_listing();
		//echo "<pre>"; print_r($data['consultants']); die;	
		//$query = $this->db->query('SELECT name, title, email FROM my_table');
		$data['services'] = $this->db->query('SELECT p.id, p.post_title, p.post_content, p.post_excerpt, p.category_id FROM `ws_posts` AS p where p.category_id = 4')->result();
		//echo "<pre>"; print_r($data['services']); die;
		//$data['services'] = $this->website_model->get_post_data( $where_data = array( 'category_id' => 4, 'status' => 'published' ), $multi_record = TRUE );
		$data['sliders'] = $this->website_model->get_post_data( $where_data = array( 'category_id' => 3, 'status' => 'published' ), $multi_record = TRUE );
		
		$data['news'] = $this->website_model->get_post_data($where_data = array('category_id' =>6, 'status' => 'published' ), $multi_record = TRUE);

		$data['testimonial'] = $this->website_model->get_post_data($where = array('category_id' => 5, 'status' => 'published'), $multi_record = TRUE);

		$data['qualify'] = $this->db->query('SELECT p.id, p.post_title, p.post_content, p.category_id, pm.meta_id, pm.post_id, pm.meta_key, pm.meta_value FROM `ws_posts` AS p INNER JOIN `ws_postmeta` AS pm ON p.id = pm.post_id where p.category_id = 7')->result();
			//pr($data['services']); die();

		$data['agency'] = $this->website_model->get_post_data($where = array('category_id' => 9, 'status' => 'published'), $multi_record = TRUE);
		
		

			$this->load->module('layouts');
			$this->load->library('template');		   
			$this->template
			->set_layout('websites')
			->build('website',isset($data) ? $data : NULL);
		
	}
	
	public function tabs(){
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$data['consultants'] = $this->website_model->get_consultant_listing();
			$data['book_consult_text'] = $this->website_model->get_post_data($where = array('category_id' => 20, 'status' => 'published'), $multi_record = TRUE);
			//$data['consult_week'] = $this->db->query('SELECT week_off FROM `ws_consultants` ')->result();
			//echo "<pre>"; print_r($data['consult_week']); die;
			//echo "<pre>"; print_r($data['consultants']); die('hello');			
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('tabs',isset($data) ? $data : NULL);
	}

	public function datepicker(){
			
			//echo "<pre>"; print_r($data['consultants']); die('hello');			
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('datepicker')
			->build('datepicker');
	}


	public function referrals(){
		$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$data['referral'] = $this->website_model->get_post_data($where = array('category_id' => 22, 'status' => 'published'), $multi_record = TRUE);
			//$data['consultants'] = $this->website_model->get_consultant_listing();
			//echo "<pre>"; print_r($data['consultants']); die('hello');			
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('referral',isset($data) ? $data : NULL);
	}

	public function referral_affiliates()
	{
		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');

			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			/*$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('education_profession', 'Education Profession', 'required');
			$this->form_validation->set_rules('comments', 'Comment', 'required');*/
			$this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_validate_captcha');
		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
				$this->load->module('layouts');
				$this->load->library('template');	
				$this->template
				->set_layout('websites')
				->build('referral_affiliates', isset($data) ? $data : NULL);
			
			} else {

				$name = $this->input->post('name');
				$email =$this->input->post('email');
				$phone = $this->input->post('phone');
				$education_profession = $this->input->post('education_profession');
				$comments = $this->input->post('comments');
                
                $users_data=array(
                   'name'       	=> $name,
                   'email' 	=> $email,
                   'phone' 	=> $phone,                                    
                   'education_profession' 	=> $education_profession,                   
                   'comments' 	=> $comments,                   
                   'referral_type' 	=> 'affiliate',                   
                   'created_at'	=> $this->created
			    );
                

				if ( $this->website_model->add(  $this->table2, $users_data ) )  // success
				{
				  	//$admin_email = "info@nextstopcanadaimmigration.com";
				  	$admin_email = get_config_item('admin_email');

                    $message = "Hello Admin, </br>";
                    $message .= "Name: $name</br>";
                    $message .= " Email: $email</br>";
                    $message .= "Phone: $phone</br>";
                    $message .= "Education/Profession: $education_profession</br>";
                    $message .= "Comment: $comments</br>";
                    $message .= "Thank You!";

				 	$from_email = $email; 
		   			$this->email->from($from_email); 
			        $this->email->to($admin_email);
			        $this->email->subject('Next Stop Canada - Referral Affiliate'); 
			        $this->email->message($message);
		        	$this->email->send();

                    //Send a thank you and confirmation email to user 
                    $message = "Hi $name,<br>";
                    $message .= "Thank you for your referral!<br>";

		   			$this->email->from($admin_email); 
			        $this->email->to($email);
			        $this->email->subject('Next Stop Canada - Referral-Affiliate Confirmation'); 
			        $this->email->message($message);
		        	$this->email->send();

			    	$this->session->set_flashdata("email_sent","Email sent successfully.");						
					$this->session->set_flashdata('success', 'Referral Affiliates have been added successfully');
					$_POST = '';
					redirect('website/thankyou');
				} 
				else 
				{
					$this->session->set_flashdata('response_status', 'Error in adding database.');
					$_POST = '';
					redirect('website/referral_affiliates');
			    }
		    }			
		}
	


		$this->load->module('layouts');
		$this->load->library('template');	
		$this->template
		->set_layout('websites')
		->build('referral_affiliates', isset($data) ? $data : NULL);
	}


	public function pricing(){
		$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			//$data['pricing'] = $this->website_model->get_pricing_listing();
			$data['pricing'] = $this->website_model->get_post_data($where = array('category_id' => 17, 'status' => 'published'), $multi_record = TRUE);
			//echo "<pre>"; print_r($data['pricing']); die('hello');			
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('pricing',isset($data) ? $data : NULL);
	}


	public function agent_inquiry()
	{
		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');

			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			/*$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('education_profession', 'Education Profession', 'required');
			$this->form_validation->set_rules('comments', 'Comment', 'required');*/
			$this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_validate_captcha');
		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');	
				$this->template
				->set_layout('websites')
				->build('agent_inquiry', isset($data) ? $data : NULL);
			
			} else 
			{

				$name = $this->input->post('name');
				$email =$this->input->post('email');
				$phone = $this->input->post('phone');
				$education_profession = $this->input->post('education_profession');
				$immigration_experience = $this->input->post('immigration_experience');
				$any_support = $this->input->post('any_support');
				$comments = $this->input->post('comments');
				$captcha = $this->input->post('g-recaptcha-response');
				/*$consultant_images = $this->do_upload();*/				
               	/*if(!$captcha){
		          echo '<h2>Please check the the captcha form.</h2>';
		          exit;
		        }
		        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Le5mVkUAAAAAATqUdSq_fOCsw2kn6YRbNdi1tWX&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
		        if($response['success'] == false)
		        {
		          echo '<h2>You are spammer ! Get the @$%K out</h2>';
		        }
		        else
		        {
		          echo '<h2>Thanks for posting comment.</h2>';
		        }*/

                $users_data=array(
                   'name'       	=> $name,
                   'email' 	=> $email,
                   'phone' 	=> $phone,                                    
                   'education_profession' 	=> $education_profession,
                   'immigration_experience' =>$immigration_experience,
                   'any_support' => $any_support,                  
                   'comments' 	=> $comments,                   
                   'referral_type' 	=> 'agent_inquiry',                   
                   'created_at'	=> $this->created
                   /*'consultant_image'	=> $consultant_images,*/
                   
			    );
			    //echo "<pre>"; print_r($users_data); die;
				if ( $this->website_model->add(  $this->table2, $users_data ) )  // success
			
                if ( $this->website_model->add(  $this->table2, $users_data ) )  // success
				{
				  /*$admin_email = "info@nextstopcanadaimmigration.com";*/
                    $admin_email =get_config_item('admin_email');
                    $message = "Hello Admin, </br>";
                    $message .= "Name: $name</br>";
                    $message .= " Email: $email</br>";
                    $message .= "Phone: $phone</br>";
                    $message .= "Education/Profession: $education_profession</br>";
                    $message .= "Do You Have Experience with Immigration Matters?: $immigration_experience</br>";
                    $message .= "Are You Able to Provide Advertisement, Marketing or Office Support?: $any_support</br>";
                    $message .= "Comment: $comments</br>";
                    $message .= "Thank You!";

				 	$from_email = $email; 
		   			$this->email->from($from_email); 
			        $this->email->to($admin_email);
			        $this->email->subject('Next Stop Canada - Agent Inquiry'); 
			        $this->email->message($message);
		        	$this->email->send();

                    //Send a thank you and confirmation email to user 
                    $message = "Hi $name,<br>";
                    $message .= "Thank you for your Agent Inquiry!<br>";

		   			$this->email->from($admin_email); 
			        $this->email->to($email);
			        $this->email->subject('Next Stop Canada - Agent Inquiry Confirmation'); 
			        $this->email->message($message);
		        	$this->email->send();

			    	$this->session->set_flashdata("email_sent","Email sent successfully.");						
					$this->session->set_flashdata('success', 'Agent Inquiry have been added successfully');
					$_POST = '';
					redirect('website/thankyou');
				} 
				else 
				{
					$this->session->set_flashdata('response_status', 'Error in adding database.');
					$_POST = '';
					redirect('website/agent_inquiry');
			    }
		    }			
		}	
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('agent_inquiry',isset($data) ? $data : NULL);
	}


	public function aboutus(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

        $data['aboutus'] = $this->website_model->get_post_data($where = array('category_id' => 8, 'status' => 'published'), $multi_record = TRUE);
		
		$data['about_help'] = $this->website_model->get_post_data($where = array('category_id' => 15, 'status' => 'published'), $multi_record = TRUE);

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('aboutus',isset($data) ? $data : NULL);
	}
	
	public function calendly(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();


		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('calendly',isset($data) ? $data : NULL);
	}

	public function booking(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();


		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('booking',isset($data) ? $data : NULL);
	}

	public function aboutyou(){

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$data['about_you'] = $this->website_model->get_post_data($where = array('category_id' => 21, 'status' => 'published'), $multi_record = TRUE);
		/*$data['about_help'] = $this->website_model->get_post_data($where = array('category_id' => 15, 'status' => 'published'), $multi_record = TRUE);*/
		//pr($data['about_you']); die('123');

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('aboutyou',isset($data) ? $data : NULL);
	}




	function add_inquiry()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('inquiry',isset($data) ? $data : NULL);

		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('partner_name', 'Partner Name', 'required');
			$this->form_validation->set_rules('partner_email', 'Partner Email', 'required');
			$this->form_validation->set_rules('partner_phone', 'Partner Phone', 'required');
			
            $this->form_validation->set_rules('partner_address', 'Partner Address', 'required');
			$this->form_validation->set_rules('partner_education', 'Partner Education', 'required');
			$this->form_validation->set_rules('partner_profession', 'Partner Profession', 'required');
			$this->form_validation->set_rules('worked', 'Worked', 'required');
			$this->form_validation->set_rules('hear_from', 'Hear About Us!', 'required');
			$this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');
			//$this->form_validation->set_rules('yes_details', 'Yes Details', 'required');


		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('websites')
				->build('inquiry',isset($data) ? $data : NULL);
			
			}else{

				$partner_name = $this->input->post('partner_name');
				$partner_email = $this->input->post('partner_email');
				$partner_phone = $this->input->post('partner_phone');
				$partner_address =$this->input->post('partner_address');
				$partner_education =$this->input->post('partner_education');
				$partner_profession =$this->input->post('partner_profession');
				$worked =$this->input->post('worked');
				$hear_from =$this->input->post('hear_from');
				$yes_details =$this->input->post('yes_details');
				$captcha =$this->input->post('g-recaptcha-response');	
				

				if(!$captcha){
		          echo '<h2>Please check the the captcha form.</h2>';
		          exit;
		        }
		        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Le5mVkUAAAAAATqUdSq_fOCsw2kn6YRbNdi1tWX&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
		        if($response['success'] == false)
		        {
		          echo '<h2>You are spammer ! Get the @$%K out</h2>';
		        }
		        else
		        {
		          echo '<h2>Thanks for posting comment.</h2>';
		        }


				//$consultant_images = $this->do_upload();				
               
                $users_data=array(
                   'partner_name'       	=> $partner_name,
                   'user_id' => $data['user_id'],
                   'partner_email' 	=> $partner_email,
                   'partner_phone' 	=> $partner_phone,
				   //'age' => $age,
				   'partner_address' => $partner_address,
                   'partner_education' 	=> $partner_education,
                   'partner_profession' 	=> $partner_profession,
                   'worked' 	=> $worked,                   
                   'hear_from' 	=> $hear_from,                   
                   'yes_details' 	=> $yes_details,                   
                                    
                   'created_at'	=> $this->created,
                   //'consultant_image'	=> $consultant_images,
                   
			    );
			    //echo "<pre>"; print_r($users_data); die;
				if ( $this->website_model->add(  $this->table1, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'International Partner Agent Info have been added successfully');
					$_POST = '';
					redirect('website/inquiry');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('website/inquiry');
				}
			}			
		}
	}


	public function logout() {
		$newdata = array(
            'user_name'  =>'',
            'user_email' => '',
            'logged_in' => FALSE,
           );
	     $this->session->unset_userdata($newdata);
	     $this->session->sess_destroy();
	     redirect(base_url(),'refresh');
    }


		/*public function logout()
		{

			$this->session->sess_destroy();

			redirect(site_url());
		}*/
	

	public function register_form(){

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();			
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('register',isset($data) ? $data : NULL);

	}

	public function register_agent(){
		//echo "Hello"; die;

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();			
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('website/index',isset($data) ? $data : NULL);

	}


	/* Add Agent Via SignUp Form in Website Registration Popup */
	function add_agent()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('website',isset($data) ? $data : NULL);

		//echo "<pre>"; print_r($_POST); die;
		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'user name', 'required');
			$this->form_validation->set_rules('password', 'password', 'required');
			
            $this->form_validation->set_rules('phone', 'Phone', 'required|min_length[10]');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[password]');

			$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			

		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('websites')
				->build('website',isset($data) ? $data : NULL);
			
			}else{
				$three_digit_random_number = mt_rand(100, 999);
				$name =$this->input->post('name');
				$email = $this->input->post('email');
				$phone =$this->input->post('phone');

				$password =$this->input->post('password');

                $hashed_password = $this->tank_auth->get_hash_password($password);

                $last_four_digit = substr($phone, -4);
				$referal_code = $three_digit_random_number.'R'.$last_four_digit;
				//$referral_data = array('referal_code' => $referal_code );
				//$user_type = $this->input->post('user_type');
               
                $users_data=array(
                   'name'      => $name,
                   'username'  => $name,
                   'email' 	=> $email,
                   'phone' 	=> $phone,
                   'password' 	=> $hashed_password,
                   'user_type' 	=> '2',
                   'referal_code' => $referal_code,
                   'created'	=> $this->created
			    );
			    //echo "<pre>"; print_r($users_data); die;
				if ( $this->website_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'User have been added successfully');
					$_POST = '';
					redirect('website');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('website');
				}
			}			
		}
	}



	/* Forgot Password Start */

	public function doforget(){
		$data = array();

		$email = $this->input->post('email');

		$findemail = $this->website_model->ForgotPassword($email);
		//echo "<pre>"; print_r($findemail); die;

        if($findemail){
          $data['sendpassword'] = $this->website_model->sendpassword($findemail);
          //echo "<pre>"; print_r($data['sendpassword']); die;        
          $this->session->set_flashdata('success',' Email found!');
           }else{
          $this->session->set_flashdata('error',' Email not found!');
          $this->load->module('layouts');
		  $this->load->library('template');
		  $this->template
		  ->set_layout('websites')
		  ->build('website',isset($data) ? $data : NULL);
          redirect(base_url().'website','refresh');
        }

		//$data['email'] = $email;
		//$this->form_validation->set_rules('email','Email','required');

		/*if ( $this->form_validation->run() == FALSE ) {

		 	$this->session->set_flashdata('message','Please enter the phone number.');		
			$this->session->set_flashdata('message-class','danger');	

			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('websites')
			->build('website',isset($data) ? $data : NULL);
		
		} else {

			$q = $this->db->query("SELECT email FROM ws_users WHERE email ='" . $email . "'");
		 	$r = $q->row_array();
		 	//echo "<pre>"; print_r($r); die;
		 }*/

	}

	/* Forgot Password End */

	public function validate_captcha(){
	    if($this->input->post('captcha') != $this->input->post('confirm_captcha'))    {
	    	
	        $this->form_validation->set_message('validate_captcha', 'The CAPTCHA you entered did not match please try again.');
	        return false;
	    }else{
	        return true;
	    }
	}


	/* Add Referral Form Start*/

	function add_referral()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		//$this->load->library('email');
		$this->template
		->set_layout('websites')
		->build('referral',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('referral_name', 'Referral Name', 'required');
			
            /*$this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('referral_city', 'Referral City', 'required');*/
			$this->form_validation->set_rules('email', 'Email', 'required');
			/*$this->form_validation->set_rules('referral_country', 'Referral Country', 'required');*/
			/*$this->form_validation->set_rules('phone', 'Phone', 'required');*/
			$this->form_validation->set_rules('referral_email', 'Referral Email', 'required');
			$this->form_validation->set_rules('immigration_type', 'Immigration Type', 'required');
			$this->form_validation->set_rules('referral_country', 'Referral Country', 'required');
			//$this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');
			$this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_validate_captcha');

		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('websites')
				->build('referral',isset($data) ? $data : NULL);
			
			}else{

				$name = $this->input->post('name');
				$referral_name = $this->input->post('referral_name');
				$email =$this->input->post('email');
				$referral_country =$this->input->post('referral_country');
				$phone =$this->input->post('phone');
				$referral_email =$this->input->post('referral_email');
				$immigration_type =$this->input->post('immigration_type');
				$comments = $this->input->post('comments');

                $users_data=array(
                   'name'       	=> $name,
                   'user_id' => $data['user_id'],
                   'referral_name' 	=> $referral_name,
                   'email' 	=> $email,
                   'phone' 	=> $phone,                                     
                   'referral_email' 	=> $referral_email,                   
                   'immigration_type' 	=> $immigration_type,                                  
                   'referral_country' 	=> $referral_country, 
                   'comments'           => $comments,       
                   'created_at'	=> $this->created,
			    );

                /*if ( $this->website_model->add(  $this->table2, $users_data ) )  // success
				{
	                $this->email->from('info@nextstopcanadaimmigration.com'); 
			         //$this->email->from($from_email, 'info@nextstopcanadaimmigration.com'); 
			        $this->email->to($email);
			        $this->email->subject('Next Stop Canada'); 
			        $this->email->message("Test message test message test message");

			        if($this->email->send()){
				    	$this->session->set_flashdata("email_sent","Email sent successfully.");						
						$this->session->set_flashdata('success', 'Referral have been added successfully');
						//$_POST = '';
						redirect('website/thankyou');
						
					}else{
						$this->session->set_flashdata('response_status', 'Error in adding database.');
						//$_POST = '';
						redirect('website/referrals');
					}
				}*/	

				if ( $this->website_model->add(  $this->table2, $users_data ) )  // success
				{
                    //send an email to admin with complete referral form fields
                   /* $admin_email = "info@nextstopcanadaimmigration.com";*/ 
                    $admin_email = get_config_item('admin_email');

                    $message = "Hello Admin, </br></br>";
                    $message .= "Name: $name</br>";
                    $message .= "Referral Name: $referral_name</br>";
                    $message .= "Referral Email: $referral_email</br>";
                    $message .= "Referral Name: $referral_country</br>";
                    $message .= "Interested In: $immigration_type</br>";
                    $message .= "Phone: $phone</br>";
                    $message .= "Thank You!";

				 	$from_email = $email; 
		   			$this->email->from($from_email); 
			        $this->email->to($admin_email);
			        $this->email->subject('Next Stop Canada - Referral'); 
			        $this->email->message($message);
		        	$this->email->send();

                    //Send a thank you and confirmation email to user 
                    $message = "Hi $name,<br>";
                    $message .= "Thank you for your referral!<br>";

		   			$this->email->from($admin_email); 
			        $this->email->to($email);
			        $this->email->subject('Next Stop Canada - Referral Confirmation'); 
			        $this->email->message($message);
		        	$this->email->send();

			    	$this->session->set_flashdata("email_sent","Email sent successfully.");						
					$this->session->set_flashdata('success', 'Referral have been added successfully');
					$_POST = '';
					redirect('website/thankyou');
				} 
				else 
				{
					$this->session->set_flashdata('response_status', 'Error in adding database.');
					$_POST = '';
					redirect('website/referrals');
			    }
		    }
	    }
	}


	/* Add Referral Form End*/

	/* Thank You Page Start*/
		public function thankyou(){
		//echo "Hello"; die;

			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();			
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('website/thanks-referral',isset($data) ? $data : NULL);

	}

	/* Thank You Page End*/

	/* FAQ Page Start Here */

	public function faq(){

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$id = $this->uri->segment(2);

		$data['page_data'] = $this->db->select('*')->from('ws_posts')->where(array('id' => $id ))->get()->result();	
		$data['faq'] = $this->db->select('*')->from('ws_posts')->where(array('category_id' => 11 ))->get()->result();	
		
		$this->load->module('layouts');
		$this->load->library('template');	
	   
		$this->template
		->set_layout('websites')
		->build('website/faq',isset($data) ? $data : NULL);
	}

	/* FAQ Page End Here */


	/* Service Page Start Here */

	public function service(){
		$data['user_id']	= $this->tank_auth->get_user_id();

		$data['username']	= $this->tank_auth->get_username();			

		$data['username']	= $this->tank_auth->get_username();
		$id = $this->uri->segment(2);

		$this->db->from('ws_posts')->where(array('id' => $id ));
		$query = $this->db->get();
		
		$data['page_data'] =  $query->result();
		$data['id'] =  $id;			

		$data['username']	= $this->tank_auth->get_username();
		$id = $this->uri->segment(2);

		$this->db->from('ws_posts')->where(array('id' => $id ));
		$query = $this->db->get();
		
		$data['page_data'] =  $query->result();
		$data['id'] =  $id;	

		$this->db->from('ws_posts')->where(array('category_id' => 13 ));
		$query = $this->db->get();
		$data['services'] = $query->result();


		$data['range_services'] = $this->db->from('ws_posts')->where(array('category_id' => 14 ))->get()->result();
		
	/*	echo "<pre>"; print_r($data['range_services']); die;*/

		$this->load->module('layouts');
		$this->load->library('template');	
	   
		$this->template
		->set_layout('websites')
		->build('website/service',isset($data) ? $data : NULL);	
	}


	/* Service Page Start End */

	/* Get Started Page Start here */

		public function getstarted(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();			
		$this->load->module('layouts');
		$this->load->library('template');	
	   	$data['get_started'] = $this->db->from('ws_posts')->where(array('category_id' => 16 ))->get()->result();

		$this->template
		->set_layout('websites')
		->build('website/getstarted',isset($data) ? $data : NULL);	
	}

	/* Get Started Page End here */


	/* Download Link Function Start Old */

	/*function agreement(){
        //$yourFile = "Sample-CSV-Format.txt";
        $agreement = "referral-agreement.txt";
        $file = @fopen($agreement, "rb");

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        //header('Content-Disposition: attachment; filename=TheNameYouWant.txt');
        header('Content-Disposition: attachment; filename=referral-agreement.txt');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($agreement));
        while (!feof($file)) {
            print(@fread($file, 1024 * 8));
            ob_flush();
            flush();
        }
}*/
	
	/* Download Link Function End Old  */

	/* Download Link Function Start  */

	function agreement($filename = NULL) {
    // load download helder
    $this->load->helper('download');
    // read file contents
    $data = file_get_contents(base_url('/uploads/'.$filename));
    force_download($filename, $data);
	}


	/* Download Link Function End  */




	/* Display function to display the selected consultant*/
	public function get_view_ajax(){
			$id = $_REQUEST['id'];
			//echo "<pre>"; print_r($id); die;
			//$id = $this->uri->segment(4);
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$data['consult'] = $this->website_model->result_view_ajax($id);
			//echo "<pre>"; print_r($data['consult']); die;
			$this->load->module('layouts');
			$this->load->library('template');	
		   
			$this->template
			->set_layout('websites')
			->build('website',isset($data) ? $data : NULL);
			//print_r($data);
	}
	/* Display function to display the selected consultant*/


	/* Display function to Add Book Consultation Form   */

	public function book_consultation(){
		//echo "Inside Function"; die;

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('website',isset($data) ? $data : NULL);

		//echo "<pre>"; print_r($_POST);die;
		if ($this->input->post()) 
		{

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('firstname', 'firstname', 'required');
			$this->form_validation->set_rules('lastname', 'Last Name', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			
            $this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('accept_agreement', 'Accept Agreement', 'required');
			$this->form_validation->set_rules('current_address', 'Current Address', 'required');
			$this->form_validation->set_rules('skype_id', 'Skype ID', 'required');
			$this->form_validation->set_rules('immigration_type', 'Immigration Type', 'required');
			$this->form_validation->set_rules('living_canada', 'Living Canada', 'required');
			$this->form_validation->set_rules('immigration_status', 'Immigration Status', 'required');


		   
            if ( $this->form_validation->run() == FALSE )
			{
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('websites')
				->build('website',isset($data) ? $data : NULL);
			
			}else{

				
				$firstname = ($this->input->post('firstname'))?$this->input->post('firstname'):"";
				$lastname = ($this->input->post('lastname'))?$this->input->post('lastname'):"";
				$phone =($this->input->post('phone'))?$this->input->post('phone'):"";
				$email =($this->input->post('email'))?$this->input->post('email'):"";
				$accept_agreement =($this->input->post('accept_agreement'))?$this->input->post('accept_agreement'):"";
				$current_address =($this->input->post('current_address'))?$this->input->post('current_address'):"";
				$first_consultation =($this->input->post('first_consultation'))?$this->input->post('first_consultation'):"";
				$estimate_date =($this->input->post('estimate_date'))?$this->input->post('estimate_date'):"";
				$skype_account =($this->input->post('skype_account'))?$this->input->post('skype_account'):"";
				$skype_id =($this->input->post('skype_id'))?$this->input->post('skype_id'):"";
				$advise_tool =($this->input->post('advise_tool'))?$this->input->post('advise_tool'):"";
				$immigration_type =($this->input->post('immigration_type'))?$this->input->post('immigration_type'):"";
				$applied_residence =($this->input->post('applied_residence'))?$this->input->post('applied_residence'):"";
				$living_canada =($this->input->post('living_canada'))?$this->input->post('living_canada'):"";
				$arrive_date =($this->input->post('arrive_date'))?$this->input->post('arrive_date'):"";
				$immigration_status =($this->input->post('immigration_status'))?$this->input->post('immigration_status'):"";
				$expiry_date =($this->input->post('expiry_date'))?$this->input->post('expiry_date'):"";
				$immigration_situation =($this->input->post('immigration_situation'))?$this->input->post('immigration_situation'):"";
				$consultation_document = $this->dcs_upload_files('consultation_document');				
				$consultation_document1 = $this->dcs_upload_files('consultation_document1');				
				$consultation_document2 = $this->dcs_upload_files('consultation_document2');
				$referred_by =($this->input->post('referred_by'))?$this->input->post('referred_by'):"";
				$thank_referral =($this->input->post('thank_referral'))?$this->input->post('thank_referral'):"";				
               
                $users_data=array(
                   'user_id' => $data['user_id'],                   
                   'consultation_id' => 1,
                   'firstname'       	=> $firstname,
                   'lastname' 	=> $lastname,
                   'phone' 	=> $phone,
				   'email' => $email,
				   'accept_agreement' => $accept_agreement,
                   'current_address' 	=> $current_address,
                   'first_consultation' 	=> $first_consultation,
                   'estimate_date' => $estimate_date,
                   'skype_account' 	=> $skype_account,
                   'skype_id' 	=> $skype_id,
                   'advise_tool' 	=> $advise_tool,
                   'immigration_type' 	=> $immigration_type,                   
                   'applied_residence' 	=> $applied_residence,                   
                   'living_canada'	=> $living_canada,
                   'arrive_date'	=> $arrive_date,
                   'immigration_status'	=> $immigration_status,
                   'expiry_date'	=> $expiry_date,
                   'are_sponser' => 'yes',
                   'children_matter' => 'yes',
                   'sponsored_immigration' => 'no',
                   'sponsored_by_spouse' => 'no',
                   'refused_application' => 'yes',
                   'notice_assesment' => 'no',
                   'immigration_situation' => $immigration_situation,
                   'consultation_document' => $consultation_document,
                   'consultation_document1' => $consultation_document1,
                   'consultation_document2' => $consultation_document2,
                   'thank_referral' => $thank_referral,
                   'referred_by' => $referred_by,        
                   
                   
                   'created_at' => $this->created
                   //'consultant_image'	=> $consultant_images,
                   
			    );
			    //echo "<pre>"; print_r($users_data); die;
				if ( $this->website_model->add(  $this->table4, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Booking Consultation have been added successfully');
					$_POST = '';
					redirect('website/tabs');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('website');
				}
			}			
		}
	

	}



	/* Upload images or documents in folder Start*/

		public function dcs_upload_files($dcs_file_data='') {
			
			$config = array(
				'upload_path' => './uploads/settings',
				'allowed_types' => '*',
				'encrypt_name' => TRUE
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload($dcs_file_data)) {

				//$data=array('product_image' => $this->upload->data());
				$uploadData = $this->upload->data();
				//die('no error');
				
				return $picture1 = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				//echo "<pre>"; die($error); echo "</pre>"; die();

				return false;
			}
	    }


	/* Upload images or documents in folder End*/



	/**
	 * Prints hello message on page load
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function hello() {
                     
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('asd')
			->build('welcome_message',isset($data) ? $data : NULL);
		
		}
	}

/* Consultation Document Upload Start Here */

	function do_upload(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			//'upload_path' => './uploads/documents/cv',
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultation_document')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }


/* Consultation Document Upload End */

/* Consultation Document Upload1 Start Here*/	

function do_upload1(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			//'upload_path' => './uploads/documents',
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultation_document1')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }

/* Consulation Document Upload1 End Here*/





	
/* Cunsultation Document Upload1 Start Here*/	

function do_upload2(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			//'upload_path' => './uploads/documents',
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultation_document2')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }

/* Consulation Document Upload1 End Here*/





	     /* Booking Consultation Start Here */

	    public function booking_consultation(){
	    		//die("here");
               //print_r($_POST['formData']); die;
		    	$formid = $this->input->post('formid');
		    	$firstname = ($this->input->post('firstname'))?$this->input->post('firstname'):"";
				$lastname = ($this->input->post('lastname'))?$this->input->post('lastname'):"";
				$phone =($this->input->post('phone'))?$this->input->post('phone'):"";
				$datetimepicker =($this->input->post('datetimepicker'))?$this->input->post('datetimepicker'):"";
				$email =($this->input->post('email'))?$this->input->post('email'):"";
				$accept_agreement =($this->input->post('accept_agreement'))?$this->input->post('accept_agreement'):"";
				$current_address =($this->input->post('current_address'))?$this->input->post('current_address'):"";
				$first_consultation =($this->input->post('first_consultation'))?$this->input->post('first_consultation'):"";
				$estimate_date =($this->input->post('estimate_date'))?$this->input->post('estimate_date'):"";
				$skype_account =($this->input->post('skype_account'))?$this->input->post('skype_account'):"";
				$skype_id =($this->input->post('skype_id'))?$this->input->post('skype_id'):"";
				$advise_tool =($this->input->post('advise_tool'))?$this->input->post('advise_tool'):"";
				$immigration_type =($this->input->post('immigration_type'))?$this->input->post('immigration_type'):"";
				$applied_residence =($this->input->post('applied_residence'))?$this->input->post('applied_residence'):"";
				$living_canada =($this->input->post('living_canada'))?$this->input->post('living_canada'):"";
				$arrive_date =($this->input->post('arrive_date'))?$this->input->post('arrive_date'):"";
				$immigration_status =($this->input->post('immigration_status'))?$this->input->post('immigration_status'):"";
				$expiry_date =($this->input->post('expiry_date'))?$this->input->post('expiry_date'):"";
				$immigration_situation =($this->input->post('immigration_situation'))?$this->input->post('immigration_situation'):"";
				$consultation_document = $this->dcs_upload_files('consultation_document');				
				$consultation_document1 = $this->dcs_upload_files('consultation_document1');				
				$consultation_document2 = $this->dcs_upload_files('consultation_document2');

				$consultation_document = $this->input->post('consultation_document');				
				$consultation_document1 = $this->input->post('consultation_document1');				
				$consultation_document2 = $this->input->post('consultation_document2');
				$referred_by =($this->input->post('referred_by'))?$this->input->post('referred_by'):"";
				$thank_referral =($this->input->post('thank_referral'))?$this->input->post('thank_referral'):"";		
				
				$datetime = explode(" ",$datetimepicker);
 				$booking_date = $datetime[0];		
                $start_time = $datetime[1];
                $arr_slot_num = $this->db->query("SELECT `slot_num` FROM `ws_slots` WHERE `start_time` = '$start_time'")->result();                
                $slot_num =  $arr_slot_num[0]->slot_num;				
               
                $booking_data=array(
                   'user_id'	=> $this->tank_auth->get_user_id(),              
                   'consultation_id' => $formid,
                   'firstname'       	=> $firstname,
                   'lastname' 	=> $lastname,
                   'phone' 	=> $phone,
                   'booking_date' 	=> $booking_date,
                   'start_time' 	=> $start_time,
                   'slot_num' 	=> $slot_num,
				   'email' => $email,
				   'accept_agreement' => $accept_agreement,
                   'current_address' 	=> $current_address,
                   'first_consultation' 	=> $first_consultation,
                   'estimate_date' => $estimate_date,
                   'skype_account' 	=> $skype_account,
                   'skype_id' 	=> $skype_id,
                   'advise_tool' 	=> $advise_tool,
                   'immigration_type' 	=> $immigration_type,                   
                   'applied_residence' 	=> $applied_residence,                   
                   'living_canada'	=> $living_canada,
                   'arrive_date'	=> $arrive_date,
                   'immigration_status'	=> $immigration_status,
                   'expiry_date'	=> $expiry_date,
                   'are_sponser' => 'yes',
                   'children_matter' => 'yes',
                   'sponsored_immigration' => 'no',
                   'sponsored_by_spouse' => 'no',
                   'refused_application' => 'yes',
                   'notice_assesment' => 'no',
                   'immigration_situation' => $immigration_situation,
                   'consultation_document' => $consultation_document,
                   'consultation_document1' => $consultation_document1,
                   'consultation_document2' => $consultation_document2,
                   'thank_referral' => $thank_referral,
                   'referred_by' => $referred_by,        
                   'transactionID' => $_POST['transactionID'],
                   
                   'created_at' => $this->created
                   //'consultant_image'	=> $consultant_images,
                   
			    );

                echo "<pre>"; print_r($booking_data); //die;
	    	//$add_data = $this->website_model->add_booking($booking_data);
	    	$id = $this->website_model->add_booking($booking_data);
	    	//$id = $this->website_model->add($booking_data);
				    // set session here like how you will set on login
				    $data["user_id"] = $id; 
				    // other required data for session
				    $this->session->set_userdata($data);
				    //$this->thank();
				 	echo '1';

	    	/*$booking_data = array(
	    		'firstname' => $this->input->post('firstname');
				'lastname' => $this->input->post('lastname');
				'phone' => $this->input->post('phone');
				'email' => $this->input->post('email');
				'accept_agreement' => $this->input->post('accept_agreement');
				'current_address' => $this->input->post('current_address');
				'first_consultation' => $this->input->post('first_consultation');
				'estimate_date'=> $this->input->post('estimate_date');
				'skype_account' => $this->input->post('skype_account');
				'skype_id' => $this->input->post('skype_id');
				'advise_tool' => $this->input->post('advise_tool');
				'immigration_type' => $this->input->post('immigration_type');
				'applied_residence' => $this->input->post('applied_residence');
				'living_canada' => $this->input->post('living_canada');
				'arrive_date' => $this->input->post('arrive_date');
				'immigration_status' => $this->input->post('immigration_status');
				'expiry_date' => $this->input->post('expiry_date');
				'immigration_situation' => $this->input->post('immigration_situation');
				'consultation_document' => $this->dcs_upload_files('consultation_document');				
				'consultation_document1' => $this->dcs_upload_files('consultation_document1');				
				'consultation_document2' => $this->dcs_upload_files('consultation_document2');
				'referred_by' => $this->input->post('referred_by');
				'thank_referral' => $this->input->post('thank_referral');
	    	);*/
 	    }
	    /* Booking Consultation End Here	*/




	    /*--code for register user start here--*/
	    	public function register(){
	    		$password =$this->input->post('password');
                $hashed_password = $this->tank_auth->get_hash_password($password);

	    		$user=array(
			      'name'=>$this->input->post('name'),
			      'email'=>$this->input->post('email'),
			      'phone'=>$this->input->post('phone'),
			      'password'=>$hashed_password
			    );
			    //print_r($user);
			    //die("here");
				$email_check=$this->web_model->email_check($user['email']);
			 
				print_r($email_check);
				if($email_check){
					echo '0';
				}
				else{
				 	//$this->web_model->add_user($user);
				 	$id = $this->web_model->add_user($user);
				    // set session here like how you will set on login
				    $data["user_id"] = $id; 
				    // other required data for session
				    $this->session->set_userdata($data);
				    //$this->thank();
				 	echo '1';
				}
	    	}
	    /*--//code for register user end here--*/

	    /*--code for login user start here--*/
	    	public function login(){
	    		$email = $this->input->post('email');
	    		$password = $this->input->post('password');
	    		$check_login = $this->web_model->login($email,$password);
	    		if($check_login){
	    			echo '1';
	    		}else{
	    			echo '0';
	    		}
	    	}
	    /*--//code for login user end here--*/


	       /*--function to get selected consultent data--*/
	    	public function selectconsultent(){
	    		$data['formid'] = $consId = $_REQUEST['conId'];
	    		$data['consult'] = $this->website_model->result_view_ajax($consId);

	    		$conres = '<div class="consultant selectedconsultent_sec" id="'.$data['consult']['0']->id.'">';
	                $conres .= '<h5>A Consultation with '.$data['consult']['0']->consultant_name.' (Consultant)</h5><hr/>';
	                $conres .= '<div class="row">';
	                    $conres .= '<div class="col-lg-3 col-md-3">';
	                        $conres .= '<img src="'.base_url().'/'.$data['consult']['0']->consultant_image.'" alt="blank" class="img-responsive thumbnail" />';
	                    $conres .= '</div>';
	                    $conres .= '<div class="col-lg-9 col-md-9">';
	                        $conres .= '<h4>'.$data['consult']['0']->consultant_name.' ('.$data['consult']['0']->consultation_type_name.') via '.$data['consult']['0']->consultation_via.'</h4>';
	                        $conres .= '<p class="text-block">'.$data['consult']['0']->consultant_name.' '.$data['consult']['0']->consultant_duration.' @ '.$data['consult']['0']->consultation_price.'</p>';
	                        $conres .= '<p class="wrapper-text-block">'.$data['consult']['0']->consultant_description.'</p>';
	                    $conres .= '</div>';
	                $conres .= '</div>';
	            $conres .= '</div>';
	            $conres .= $this->load->view('consform', $data);
	    		echo $conres;
	    		//print_r($data['consult']);
	    	}
	    /*--//function to get selected consultent data--*/
		public function activation_user(){
					echo $this->uri->segment(3);
					$pass_segment = $this->uri->segment(3);
					$data['user_id']	= $this->tank_auth->get_user_id();
					$data['username']	= $this->tank_auth->get_username();

					$this->load->module('layouts');
					$this->load->library('template');
					$this->template
					->set_layout('websites')
					->build('change_password',isset($data) ? $data : NULL);	//echo "asdfs"; die;
		}	   


		public function changeForgetassword(){
			echo $this->uri->segment(3);
			echo "Inside Function Done"; die;
			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('websites')
			->build('change_password',isset($data) ? $data : NULL);
		}



		/*--function start for payment--*/
		public function paymentevent(){
			$this->load->library('paypal/PaypalPro');
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
			    $payableAmount = 10;
			    //$nameArray = explode(' ',$_POST['name_on_card']);
			    
			    //Buyer information
			    //$firstName = $nameArray[0];
			    //$lastName = $nameArray[1];
			    $firstName = 'tester';
			    $lastName = 'Sunpreet';
			    $city = 'Kolkata';
			    $zipcode = '700091';
			    $countryCode = 'IN';
			    
			    //Create an instance of PaypalPro class
			    $paypal = new PaypalPro;
				
				//Payment details
			    $paypalParams = array(
			        'paymentAction' => 'Sale',
			        'amount' => $payableAmount,
			        'currencyCode' => 'USD',
			        'creditCardType' => $_POST['card_type'],
			        'creditCardNumber' => trim(str_replace(" ","",$_POST['card_number'])),
			        'expMonth' => $_POST['expiry_month'],
			        'expYear' => $_POST['expiry_year'],
			        'cvv' => $_POST['cvv'],
			        'firstName' => $firstName,
			        'lastName' => $lastName,
			        'city' => $city,
			        'zip'	=> $zipcode,
			        'countryCode' => $countryCode,
			    );
			    $response = $paypal->paypalCall($paypalParams);
			    // print_r($response);
			    // die();
			    $paymentStatus = strtoupper($response["ACK"]);
			    if ($paymentStatus == "SUCCESS"){
			    	//print_r($response);
					$data['status'] = 1;
					
			        $data['transactionID'] = $response['TRANSACTIONID'];
			        //Update order table with tansaction data & return the OrderID
			        //SQL query goes here..........
					
			        //$data['orderID'] = $OrderID;
			    }else{
			         $data['status'] = 0;
			    }

			    echo json_encode($data);
			}
		}
		/*--//function end for payment--*/



}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */