<!--================Slider Area =================-->
        <section id="service-page" class="main_slider_area polo">
            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                <ul>
                    <li data-index="rs-2946" data-transition="slidevertical" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url() ?><?= get_config_item('about_us_header_image')?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            

                            <!-- LAYER NR. 10 -->
                     

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption Agency-CloseBtn rev-btn "
                            id="slide-2971-layer-20"
                            data-x="['center','center','center','center']" data-hoffset="['510','389','270','199']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-298','-229','-163','-118']"
                            data-width="50"
                            data-height="none"
                            data-whitespace="nowrap"

                            data-type="button"
                            data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2971-layer-15","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2971-layer-19","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2971-layer-20","delay":""}]'
                            data-responsive_offset="on"
                            data-responsive="off"
                            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:45deg;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":500,"to":"o:1;","delay":"bytrigger","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"to":"auto:auto;","ease":"nothing"},{"frame":"hover","speed":"500","ease":"Power1.easeInOut","to":"o:1;sX:1.1;sY:1.1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);"}]'
                            data-textAlign="['center','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            data-lasttriggerstate="reset"
                            style="z-index: 15; min-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;font-size: 24px;"><i class="fa fa-close"></i> </div>
							
                            <div class="tp-caption tp-resizeme secand_text"
                                data-x="['center','center','center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['0','0','0','0','0']"
                                data-fontsize="['48','48','48','28','28','22']"
                                data-lineheight="['60','60','60','36','36','30']"
                                data-width="100%"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text"
                                data-responsive_offset="on"
                                data-transform_idle="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['center','center','center','center','center','center']"

                                style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:700;color:#fff;text-transform:uppercase;">
								<p><?=get_config_item('about_us_header_title')?></p>
								</div>

                            <div class="tp-caption tp-resizeme slider_button"
                                data-x="['center','center','center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['130','130','130','100','100','100']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="text"
                                data-responsive_offset="on"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['center','center','center','center','center','center']">
                                
                                <p style="color:#fff"><?=get_config_item('about_us_header_content')?></p>

								<!--a class="bg_btn" href="<?php echo base_url('consultant-booking') ?>">Book Consultation</a-->
								<a class="about_btn" href="<?= base_url('get-started')?>" style="transition: none; text-align: inherit; line-height: 42px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;"> Let us help you!</a>
                            </div>
                        </div>
                    </li>
                    
                </ul>
            </div>
        </section>
        <!--================End Slider Area =================-->
    <!-- ================= Section About-us ======================= -->
    <section class="about-us-page">
       <div class="container-fluid">
            <div class="container">
                 <h2><?=get_config_item('about_us_page_title')?></h2>
                 <p><?=get_config_item('about_us_content')?> </p>
            </div>
      </div>
    </section>

    
    <!-- ==================  nd About Us Section ======================= -->
    <section class="images" id="images_team">
        <div class="container-fluid">
        	<div class="container">
              <h2><?=get_config_item('team_title')?></h2><br>
              <h3><?=get_config_item('team_tagline')?></h3>
              <div class="row text-center">
                <?php foreach ($aboutus as $note) { ?>
                    <div class="col-lg-4 col-md-4">
                 <div class="team_image">
                    <!-- <img src="../assets/images/aboutyou2.jpg" alt="team" width="390" height="300"> -->
                    <img src="<?= base_url() ?>/<?= $note->featured_image ?>" alt="team" class="img-responsive thumbnail"> </div>
                   <?= $note->post_title ?>
                    <p>
                       <?= $note->post_content ?>
                      </p>
                 
              </div>
                <?php } ?>
              </div>
        	</div>	
        </div>
    </section>
    <!-- =============  How we weork Section =================== -->
        
        <section class="how-we-work">
            <div class="container-fluid">
                <div class="container">
                    <h2><?=get_config_item('how_we_work_title')?></h2>
                    <div class="row how-it-row">
                        <div class="col-md-6">
                            <p><?=get_config_item('how_we_work_content')?></p>
                        </div>
                        <div class="col-md-6">
                            <img src="<?= base_url() ?><?=get_config_item('how_we_work_featured_img');?>" alt="team">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  ===========  End section How we work ====================-->

    <?php $our_about_footer_status = get_config_item('about_footer_status');
                if($our_about_footer_status == 1){

    ?>

    <footer class="footer_background" style="background-image:url('<?php echo base_url() ?><?= get_config_item('about_footer_image'); ?>');">
    <!-- <footer style="background-image:url('<?php echo base_url() ?>uploads/our-team/<?= get_config_item('about_footer_image'); ?>'); "> -->
    	<p><?= get_config_item('about_footer_title'); ?></p>
    		 <!--a class="bg_btn" href="<?php echo base_url('consultant-booking') ?>">Book Consultation</a-->
    		<a class="about_btn" href="<?= base_url('contact/104')?>" style="transition: none; text-align: inherit; line-height: 42px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;">CONTACT US</a>
    </footer>
    
<?php } ?>























