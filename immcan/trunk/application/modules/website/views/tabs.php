<style type="text/css">
.selectconsultent{
    padding: 96px 0 50px 0;
    margin-top: 20px;
}
.selectconsultent .tab .nav-tabs{
    border-bottom:0px;
}
.selectconsultent .tab .nav-tabs li{
    border-right:1px solid #ddd;
}
.selectconsultent .tab .nav-tabs li:last-child{
    border-right:0px solid #ddd;
}
.selectconsultent .tab .nav-tabs li:first-child p{
    border-left:1px solid #ddd;
}
.selectconsultent .tab .nav-tabs li p {
    color: #868686;
    background:#fff;
    border-radius:0;
    font-size:16px;
    /*margin-right:-1px;*/
    padding: 5.5px 25px;
    border-top:1px solid #d3d3d3;
    border-bottom: 1px solid #d3d3d3;
}
.selectconsultent .nav-tabs li:first-child p{
    border-radius: 5px 0 0 5px;
}
.selectconsultent .nav-tabs li:last-child p{
    border-radius: 0 5px 5px 0;
    border-right:1px solid #d3d3d3;
}
.selectconsultent .tab .nav-tabs li p:hover{
    background:#eee;
}
.selectconsultent .tab .nav-tabs li p:hover:before{
    border-left: 15px solid #eee;
}
.selectconsultent .tab .nav-tabs li.active p:after,
.selectconsultent .tab .nav-tabs li p:after{
    content: "";
    border-left: 17px solid #c0392b;
    border-top: 17px solid transparent;
    border-bottom: 19px solid transparent;
    position: absolute;
    top: 2px;
    right: -17px;
    z-index: 1;
}
.selectconsultent .tab .nav-tabs li p:after{
    border-left: 17px solid #d3d3d3;
}
.selectconsultent .tab .nav-tabs li.active p:before{
    border-left: 17px solid #c0392b;
}
.selectconsultent .tab .nav-tabs li p:before{
    border-bottom: 18px solid rgba(0, 0, 0, 0);
    border-left: 17px solid #fff;
    border-top: 17px solid rgba(0, 0, 0, 0);
    content: "";
    position: absolute;
    right: -15px;
    top: 2px;
    z-index: 2;
}
.selectconsultent .tab .nav-tabs li.active > p,
.selectconsultent .tab .nav-tabs > li.active > p:focus,
.selectconsultent .tab .nav-tabs > li.active > p:hover {
    border: none;
    color:#fff;
    background:#c0392b;
    border-top:1px solid #d3d3d3;
    border-bottom: 1px solid #d3d3d3;
}
.selectconsultent .tab .nav-tabs li:last-child.active p:after,
.selectconsultent .tab .nav-tabs li:last-child p:after{
    border: none;
}
.selectconsultent .tab .nav-tabs li:last-child p:after,
.selectconsultent .tab .nav-tabs li:last-child p:hover:before,
.selectconsultent .tab .nav-tabs li:last-child.active p:before,
.selectconsultent .tab .nav-tabs li:last-child p:before{
    border-left: none;
}
.selectconsultent .tab .tab-content{
    padding:12px;
    color:#5a5c5d;
    margin-top:2%;
    font-size: 14px;
    border: 1px solid #fff;
}
.consultant img{
    width: 100%;
    height: auto;
    object-fit: contain;
}
.consultant {
    padding: 10px;
    border: 1px solid #eeeeee;
    margin: 15px 0px 15px 0px;

}
.selectconsultent .consultant{
    cursor: pointer;
}
</style>


<div class="container selectconsultent">
    <div class="row">

        <div class="col-lg-3 col-md-3">
          <?php foreach ($book_consult_text as $text) { ?>
            
            <h1><?php echo $text->post_title ?></h1>
            <p><?php echo $text->post_content ?></p>
          <?php } ?>
        </div>

        <div class="col-lg-9 col-md-9">
            <div class="tab" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs consultenttab" role="tablist">
                    <li role="presentation" class="active col-lg-4 col-md-12" style="padding:0px;"><p href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Choose Appointment</p></li>
                    <li role="presentation" class="col-lg-4 col-md-12" style="padding:0px;"><p href="">Your Info</p></li>
                    <!-- <li role="presentation" class="col-lg-4 col-md-12" style="padding:0px;"><p href="">Confirmation</p></li> -->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    
                    <div role="tabpanel" class="tab-pane in active" id="Section1">
                        <?php
                            foreach ($consultants as $consultant) { ?>
                                <div class="consultant" id="<?= $consultant->id; ?>">
                                    
                                    <h5>
                                      <?= $consultant->consultant_name ?> <?php if($consultant->consultation_type_name != ''){ echo '('.$consultant->consultation_type_name.')'; }  ?>
                                      <?php if($consultant->consultation_via != ''){ echo '<i>via </i>'. $consultant->consultation_via; } ?>  
                                    </h5>
                                    <i><?= $consultant->consultant_duration ?> @ <?= $consultant->consultation_price ?></i>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3">
                                            <img src="<?= base_url() ?>/<?=$consultant->consultant_image?>" alt="blank" class="img-responsive thumbnail" />
                                        </div>
                                        <div class="col-lg-9 col-md-9">
                                            <p class="wrapper-text-block"><?= $consultant->consultant_description; ?></p>
                                            
                                        </div>
                                    </div>
                                </div>
                                        
                        <?php   }   ?>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="Section2">
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="Section3">
                        <!--payment form start here-->
                        <div class="card-payment">
                            <div id="paymentSection">
                            <form method="post" id="paymentForm">
                                  <h4>Payable amount: $10 USD</h4>
                                  <ul>
                                      <input type="hidden" name="card_type" id="card_type" value=""/>
                                      <li>
                                          <label for="card_number">Card number (<a href="javascript:void(0);" id="sample-numbers-trigger">try one of these</a>)</label>
                                          <div class="numbers" style="display: none;">
                                              <p>Try some of these numbers:</p>
                                      
                                              <ul class="list">
                                                  <li><a href="javascript:void(0);">4000 0000 0000 0002</a></li>
                                                  <li><a href="javascript:void(0);">5018 0000 0009</a></li>
                                                  <li><a href="javascript:void(0);">5100 0000 0000 0008</a></li>
                                                  <li><a href="javascript:void(0);">6011 0000 0000 0004</a></li>
                                              </ul>
                                          </div>
                                          <input type="text" placeholder="1234 5678 9012 3456" id="card_number" name="card_number" class="">
                          
                                          <small class="help">This demo supports Visa, American Express, Maestro, MasterCard and Discover.</small>              </li>
                          
                                      <li class="vertical">
                                          <ul>
                                              <li>
                                                  <label for="expiry_month">Expiry month</label>
                                                  <input type="text" placeholder="MM" maxlength="5" id="expiry_month" name="expiry_month">
                                              </li>
                                              <li>
                                                  <label for="expiry_year">Expiry year</label>
                                                  <input type="text" placeholder="YYYY" maxlength="5" id="expiry_year" name="expiry_year">
                                              </li>
                                              <li>
                                                  <label for="cvv">CVV</label>
                                                  <input type="text" placeholder="123" maxlength="3" id="cvv" name="cvv">
                                              </li>
                                          </ul>
                                      </li>
                                      <li>
                                          <label for="name_on_card">Name on card</label>
                                          <input type="text" placeholder="Card Holder Name" id="name_on_card" name="name_on_card">
                                      </li>
                                      <li><input type="button" name="card_submit" id="cardSubmitBtn" value="Proceed" class="payment-btn" disabled="true" ></li>
                                      <!--<p style="color:#EA0075;">Note that: This demo will working with PayPal sandbox accounts.</p>-->
                                  </ul>
                              </form>
                          </div>
                            <div id="orderInfo" style="display: none;"></div>
                        </div>
                        <!--//payment form end here-->




                        <button type="submit" class="previoustabcon btn btn-success" name="previous">Previous</button>
                        <!-- <input type="submit" name="submit" class="btn btn-success" value="Submit"> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>