<style>
.faqHeader {
    font-size: 27px;
    margin: 20px;
        font-weight: 700;
}
.card-header a{color:#000;}
.card-block {
    font-size: 16px;
    font-weight: 400;
}

.panel-heading [data-toggle="collapse"]:after {
    font-family: 'Glyphicons Halflings';
    content: "e072"; /* "play" icon */
    float: right;
    color: #F58723;
    font-size: 18px;
    line-height: 22px;
    /* rotate "play" icon from > (right arrow) to down arrow */
    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    -o-transform: rotate(-90deg);
    transform: rotate(-90deg);
}

.panel-heading [data-toggle="collapse"].collapsed:after {
    /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
    color: #454444;
}
</style>


<div class="container">

    <div class="faq-section" id="accordion">
        
            <?=$page_data[0]->post_content?>

            <div class="card ">
                 <?php if (!empty($faq)) {
                            $count = 0; 
                            foreach ($faq as $note) { ?>
                
                <div class="card-header">
                    <h4 class="card-header-inner">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne-<?=$count?>"><?=$note->post_title?></a>
                    </h4>
                </div>

                <div id="collapseOne-<?=$count?>" class="panel-collapse collapse in <?php if($count == 0) { echo 'show'; }?>">
                    <div class="card-block">
                        <?=$note->post_content?>
                    </div>
                </div>

                <?php $count++; }
                
                } ?>

        </div>
    </div>

</div>


