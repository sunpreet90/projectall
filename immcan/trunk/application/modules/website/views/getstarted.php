<!-- Start Banner Area -->
<section class="banner-area relative" style="background-image:url('<?php echo base_url() ?><?= get_config_item('get_started_header_image'); ?>'); background-size: cover; background-repeat: no-repeat;">
	<div class="overlay overlay-bg"></div>
	<div class="container">
		<div class="row fullscreen align-items-center justify-content-center" style="height:400px;">
			<div class="col-lg-12">
				<div class="banner-content">
					<h1 class="white-color"><?=get_config_item('get_started_page_title')?></h1>
					<p class="text-white"><?=get_config_item('get_started_tagline')?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Banner Area -->

<!-- Start Service Area -->
<section class="angle-bg p-t-b-40">
	<div class="container">
		<div class="row">
			
			<div id="demo" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ul class="carousel-indicators">
				<li data-target="#demo" data-slide-to="0" class="active"></li>
				<li data-target="#demo" data-slide-to="1"></li>
				<li data-target="#demo" data-slide-to="2"></li>
			  </ul>

			  <!-- The slideshow -->
			  <div class="carousel-inner">

			  	<?php 
			  	$active = 'active';
			  	$count = 1;
			  	foreach ($get_started as $note) { ?>
			  		

				<div class="carousel-item <?=$active ?>">
					<div class="col-xs-12 col-md-8 offset-md-2">
						<div class="caption-title" data-animation="animated fadeInUp">
							<h2>Step 0<?=$count ?> - <?= $note->post_title ?></h2>
						</div>
						<div class="caption-desc" data-animation="animated fadeInUp">
							<div class="tab-content pt-30">
								<!-- <h5>Contact</h5> -->
								<p class="lead"><?= $note->post_content ?></p>
								<!-- <p><?= $note->post_content ?></p> -->
								<a href="https://client.canadaconsultportal.ca/Login/Login?siteId=CCP2018135&qlauncher=true "> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								 Get Started 
								</button>
								</a>
								<a href="<?= base_url('consultant-booking') ?>"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								Book Consultation 
								</button>
								</a>
								<a href="<?= base_url('contact/104') ?>"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								Contact Us
								</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			  	<?php $count++; $active = ''; }  ?>
				 <!-- <div class="carousel-item">
					<div class="col-xs-12 col-md-8 offset-md-2">
						<div class="caption-title" data-animation="animated fadeInUp">
							<h2>Step 2 - Consult</h2>
						</div>
						<div class="caption-desc" data-animation="animated fadeInUp">
							<div class="tab-content pt-30">
								<h5>Contact</h5>
								<p class="lead">Get in touch and we will go over the basics and how to proceed.</p>
								<p class="mb-50">Our number 1 goal is to protect your future in Canada. <br><br>Don’t be a victim of confusing or bad advice from anonymous internet sources who don’t know YOUR individual circumstances! 
								<br><br>Contact us and within a few minutes we will be able to tell you if there may be Canadian immigration options available to you. <br>All it takes is an email or a phone call to get you started on the RIGHT path to success.</p>

								<a href="<?= base_url('contact-us') ?>"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								Contact Us 
								</button>
								</a>

								<a href="<?= base_url('consultant-booking') ?>"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								Book Consultation 
								</button>
								</a>
								<a href="https://client.canadaconsultportal.ca/Login/Login?siteId=CCP2018135&qlauncher=true"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
							    Get Started 
								</button>
								</a>
							</div>
						</div>
						div class="caption-button" data-animation="animated fadeInUp">
							<a href="#" class="button">Read more</a>
						</div>
					</div>
				
				<div class="carousel-item">
					<div class="col-xs-12 col-md-8 offset-md-2">
						<div class="caption-title" data-animation="animated fadeInUp">
							<h2>Step 3 - Success</h2>
						</div>
						<div class="caption-desc" data-animation="animated fadeInUp">
							<div class="tab-content pt-30">
								<h5>Contact</h5>
								<p class="lead">Get in touch and we will go over the basics and how to proceed.</p>
								<p class="mb-50">Our number 1 goal is to protect your future in Canada. <br><br>Don’t be a victim of confusing or bad advice from anonymous internet sources who don’t know YOUR individual circumstances! 
								<br><br>Contact us and within a few minutes we will be able to tell you if there may be Canadian immigration options available to you. <br>All it takes is an email or a phone call to get you started on the RIGHT path to success.</p>

								<a href="<?= base_url('contact-us') ?>"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								Contact Us 
								</button>
								</a>

								<a href="<?= base_url('consultant-booking') ?>"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								Book Consultation 
								</button>
								</a>

								<a href="https://client.canadaconsultportal.ca/Login/Login?siteId=CCP2018135&qlauncher=true"> 
								<button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
								Get Started
								</button>
								</a>
							</div>
						</div>						
					</div>
				</div> --> 
			  </div>

			  <!-- Left and right controls -->
			  <a class="carousel-control-prev" href="#demo" data-slide="prev">
				<span class="carousel-control-prev-icon"></span>
			  </a>
			  <a class="carousel-control-next" href="#demo" data-slide="next">
				<span class="carousel-control-next-icon"></span>
			  </a>
			</div>
			
		</div>
	</div>
</section>
<!-- End Service Area -->

