<!-- <div class="modal" id="password_modal">
    <div class="modal-header">
        <h3>Change Password <span class="extra-title muted"></span></h3>
    </div>
    <form name="changepass" method="post" id="passwordForm" action="<?php echo base_url().'website/changeForgetassword' ?>">
    <div class="modal-body form-horizontal">
        <div class="control-group">
            <label for="current_password" class="control-label">Current Password</label>
            <div class="controls">
                <input name="current_password" type="password">
            </div>
        </div>
        <div class="control-group">
            <label for="new_password" class="control-label">New Password</label>
            <div class="controls">
                <input name="new_password" type="password">
            </div>
        </div>
        <div class="control-group">
            <label for="confirm_password" class="control-label">Confirm Password</label>
            <div class="controls">
                <input name="confirm_password" type="password">
            </div>
        </div>      
    </div>
    <div class="modal-footer">
        <button href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button href="#" class="btn btn-primary" id="password_modal_save">Save changes</button>
    </div>
</form>
</div> -->

<div class="container">
<div class="row">
<div class="col-sm-12">
<h1>Change Password</h1>
</div>
</div>
<div class="row">
<div class="col-sm-6 col-sm-offset-3">
<p class="text-center">Use the form below to change your password. Your password cannot be the same as your username.</p>
<form name="changepass" method="post" id="passwordForm" action="<?php echo base_url().'website/changeForgetassword' ?>">
<input type="password" class="input-lg form-control" name="newpassword" id="newpassword" placeholder="New Password" autocomplete="off">

<input type="password" class="input-lg form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm Password" autocomplete="off">

<input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg" data-loading-text="Changing Password..." value="Change Password">
</form>
</div>
</div>
</div>