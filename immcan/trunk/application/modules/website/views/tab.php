<!-- <link href="<?php echo base_url('assets') ?>/css/style.css" rel="stylesheet"> -->
  <link href="<?php echo base_url('assets') ?>/css/jquery.stepy.css" rel="stylesheet">
  <link href="<?php echo base_url('assets') ?>/css/style-responsive.css" rel="stylesheet">
      <script src="http://code.jquery.com/jquery-1.10.2.js"></script>

        <script>
            function validate() {
                var output = true;
                $(".signup-error").html('');

                if ($("#personal-field").css('display') != 'none') {
                    if (!($("#name").val())) {
                        output = false;
                        $("#name-error").html("Name required!");
                    }

                    if (!($("#dob").val())) {
                        output = false;
                        $("#dob-error").html("Date of Birth required!");
                    }
                }

                if ($("#password-field").css('display') != 'none') {
                    if (!($("#user-password").val())) {
                        output = false;
                        $("#password-error").html("Password required!");
                    }

                    if (!($("#confirm-password").val())) {
                        output = false;
                        $("#confirm-password-error").html("Confirm password required!");
                    }

                    if ($("#user-password").val() != $("#confirm-password").val()) {
                        output = false;
                        $("#confirm-password-error").html("Password not matched!");
                    }
                }

                if ($("#contact-field").css('display') != 'none') {
                    if (!($("#phone").val())) {
                        output = false;
                        $("#phone-error").html("Phone required!");
                    }

                    if (!($("#email").val())) {
                        output = false;
                        $("#email-error").html("Email required!");
                    }

                    if (!$("#email").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
                        $("#email-error").html("Invalid Email!");
                        output = false;
                    }

                    if (!($("#address").val())) {
                        output = false;
                        $("#address-error").html("Address required!");
                    }
                }

                return output;
            }

            $(document).ready(function () {
                $("#next").click(function () {
                    var output = validate();
                    if (output === true) {
                        var current = $(".active");
                        var next = $(".active").next("li");
                        if (next.length > 0) {
                            $("#" + current.attr("id") + "-field").hide();
                            $("#" + next.attr("id") + "-field").show();
                            $("#back").show();
                            $("#finish").hide();
                            $(".active").removeClass("active");
                            next.addClass("active");
                            if ($(".active").attr("id") == $("li").last().attr("id")) {
                                $("#next").hide();
                                $("#finish").show();
                            }
                        }
                    }
                });


                $("#back").click(function () {
                    var current = $(".active");
                    var prev = $(".active").prev("li");
                    if (prev.length > 0) {
                        $("#" + current.attr("id") + "-field").hide();
                        $("#" + prev.attr("id") + "-field").show();
                        $("#next").show();
                        $("#finish").hide();
                        $(".active").removeClass("active");
                        prev.addClass("active");
                        if ($(".active").attr("id") == $("li").first().attr("id")) {
                            $("#back").hide();
                        }
                    }
                });

                $("input#finish").click(function (e) {
                    var output = validate();
                    var current = $(".active");

                    if (output === true) {
                        return true;
                    } else {
                        //prevent refresh
                        e.preventDefault();
                        $("#" + current.attr("id") + "-field").show();
                        $("#back").show();
                        $("#finish").show();
                    }
                });
            });
        </script>
  


<div class="mouse-icon hidden-xs">
                <div class="scroll"></div>
            </div>
    
    <section id="be-the-first" class="pad-xl">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2 text-center margin-30 wow fadeIn" data-wow-delay="0.6s">
            <h2>Canada is calling!</h2>
            <p class="lead">Let us help you in 3 easy steps!</p>
          </div>
        </div>
        
        <div class="iphone wow fadeInUp" data-wow-delay="1s">
            <img src="<?php echo base_url('assets') ?>/web/img/iphone.png">
        </div>
      </div>
    </section>



    
    <section id="main-info" class="pad-xl">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 wow fadeIn" data-wow-delay="0.4s">
                    <hr class="line purple">
                    <h3>READY?</h3>  
    
                    <p>Simply complete no-obligation <a href="https://www.abc-lang.com/free-assessments" style="text-decoration: underline; color: #5563c7;">Free Assessment </a> form or send us a request for more info! We will review YOUR situation and goals and will find the best options for YOU.</p>
                </div>
                <div class="col-sm-4 wow fadeIn" data-wow-delay="0.8s">
                    <hr  class="line blue">
                    <h3>SET.</h3>
                    <p>We will discuss each option in detail so that you can chose the best one.  Our advice will save you substantial time and money and will greatly increase your chances for success!</p>
                </div>
                <div class="col-sm-4 wow fadeIn" data-wow-delay="1.2s">
                    <hr  class="line yellow">
                    <h3>GO!</h3>
                    <p>When you make informed decision on the best option for you, we will guide and help you every step of the way. Our goal is to make your road to Canada safe and easy!</p>
                </div>
            </div>
        </div>
    </section>
        
        

    <!--Pricing-->
    <section id="pricing" class="pad-lg">
      <div class="container">
        <div class="row margin-40">
          <div class="col-sm-8 col-sm-offset-2 text-center">
            <h2 class="white">Pricing</h2>
            <p class="white">We offer different levels of service to fit your budget!</p>
          </div>
        </div>
        
        <div class="row margin-50">
          
          <div class="col-sm-4 pricing-container wow fadeInUp" data-wow-delay="1s">
            <br />
            <ul class="list-unstyled pricing-table text-center">
                        <li class="headline"><h5 class="white">Silver</h5></li>
                        <li class="price"><div class="amount">$<small></small></div></li>
                        <li class="info"><h3>If you like to be in the driver’s seat and need minimal help, this offer is for you!</h3></li>
                        <li class="features first">- &nbsp; Forms selections</li>
                        <li class="features">- &nbsp;Application review</li>
                        <li class="features">- &nbsp;Settlement/employment info</li>
                        <li class="features">- &nbsp;Email consultation</li>
                        <li class="features">- &nbsp;Phone/Skype consultation</li>
                        <li class="features">This option is best suited for people who like to complete all paperwork by themselves and have basic questions or want to make sure their application is complete and free of errors. Please note: If you chose this option, we will not submit the application on your behalf and act as your representative.</li>
                        <li class="features last btn btn-secondary btn-wide"><a href="#">Get Started</a></li>
                    </ul>
          </div>
          
          <div class="col-sm-4 pricing-container wow fadeInUp" data-wow-delay="0.4s">
            <ul class="list-unstyled pricing-table active text-center">
                        <li class="headline"><h5 class="white">Gold</h5></li>
                        <li class="price"><div class="amount">$$<small></small></div></li>
                        <li class="info"><h3>Application forms are often complex and immigration process can be very confusing. This option is best for people who like to be fully represented on their applications.</h3></li>
                        <li class="features first">- &nbsp;Full representation</li>
                        <li class="features">- &nbsp;Communication with CIC/other agencies</li>
                        <li class="features">- &nbsp;Information research, document translation</li>
                        <li class="features">- &nbsp;Interview preparation/coaching</li>
                        <li class="features">This option is best suited for people who like to complete all paperwork by themselves and have basic questions or want to make sure their application is complete and free of errors. Please note: If you chose this option, we will submit the application on your behalf and act as your representative throughout application process.</li>
                        <li class="features last btn btn-secondary btn-wide"><a href="#">Get Started</a></li>
                    </ul>
          </div>
          
           <div class="col-sm-4 pricing-container wow fadeInUp" data-wow-delay="1.3s">
            <br />
            <ul class="list-unstyled pricing-table text-center">
                        <li class="headline"><h5 class="white">Platinum</h5></li>
                        <li class="price"><div class="amount">$$$<small></small></div></li>
                        <li class="info features"><h3>This option is best for people with special needs and complex applications.</h3></li>
                        <li class="features first">- &nbsp;Criminal/Medical Inadmissibility</li>
                        <li class="features">- &nbsp;Criminal Rehabilitations</li>
                        <li class="features">- &nbsp;Labour Market Impact Assessments</li>
                        <li class="features">- &nbsp;NAFTA permits</li>
                        <li class="features">- &nbsp;Business/Investor/Self-Employed Applications</li>
                        <li class="features">We will ensure that your situation is presented to immigration officials in the most favourable light. Please note: If you chose this option, we will submit the application on your behalf and act as your representative during appeals and up to (excluding) Federal Court.</li>
                        <li class="features last btn btn-secondary btn-wide"><a href="#">Get Started</a></li>
                    </ul>
          </div> 
          
        </div>
        
      </div>
    </section>
    
 

    <section id="main-tabs">
        <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <h4 class="fw-title">Form Wizard with Validation</h4>
                    <div class="box-widget">
                        <div class="widget-head clearfix">
                            <div id="top_tabby" class="block-tabby pull-left">
                            </div>
                        </div>

                        <?php if($this->session->flashdata('success')): ?> 
                        <div class='alert alert-success alert-block fade in'>
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <h4>
                                <i class="icon-ok-sign">Success</i>
                                <p><?php echo $this->session->flashdata('success');?></p>
                            </h4>
                        </div>
                    <?php endif; ?>
                   <?php if ( strlen( validation_errors() ) > 0 ) : ?>
                        <div class='alert alert-block alert-danger fade in'>
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <h4>
                                <i class="icon-ok-sign">Errors</i>
                                <?php echo $this->session->flashdata('response_status');?>
                            </h4>
                            <p><?php echo validation_errors(); ?></p>
                        </div>
                    <?php endif; ?>

                        <div class="widget-container">
                            <div class="widget-block">
                                <div class="widget-content box-padding">
                                    <form id="stepy_form" class=" form-horizontal left-align form-well" name="book_consultation" method="POST" action="<?php echo base_url() ?>web/book_consultation" enctype='multipart/form-data'>
                                        <fieldset title="Step 1">
                                            <legend>Choose Appointment</legend>
                                            <p>I would like to schedule... </p><br/>
                                            <?php //echo "<pre>"; print_r($consultants);
                                            //foreach ($consultants as $consultant) { ?>                         

                                            <div class="well" id="<?php //echo $consultant->id; ?>">
                                            <p>A Consultation with <?php //echo $consultant->name; ?> (Consultant)</p>
                                              <div class="media">
                                                <a class="pull-left" href="#">
                                                   
                                        <img class="media-object" src="<?php //echo base_url()."uploads/".$consultant->consultant_image; ?>" width="150" height="120">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><?php //echo $consultant->name; ?> (Consultant) <?php //echo //$consultant->consultant_heading; ?> via <?php //echo $consultant->consultant_subheading; ?></h4>
                                                  <!-- <p class="text-right">By Anailuj</p> -->
                                                  <p><?php //echo $consultant->name; ?>1 hour @ $<?php //echo $consultant->consultant_price ?></p>
                                                  <p><?php //echo $consultant->consultant_description; ?></p>
                                                 
                                               </div>
                                            </div>
                                          </div>

                                          <?php //} ?>
                                          <?php //echo "<pre>"; print_r($consult);
                                            //foreach ($consultants as $consultant) { ?>  
                                          <div class="wells" id="<?php //echo $consultant->id; ?>" style="display: none;">
                                          <p>A Consultation with <?php //echo $consultant->name; ?> (Consultant)</p>
                                              <div class="media">
                                                <a class="pull-left" href="#">
                                                   
                                        <img class="media-object" src="<?php echo base_url()."uploads/".$consultant->consultant_image; ?>" width="150" height="120">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><?php //echo $consultant->name; ?> (Consultant) <?php echo $consultant->consultant_heading; ?> via <?php //echo $consultant->consultant_subheading; ?></h4>
                                                  <!-- <p class="text-right">By Anailuj</p> -->
                                                  <p><?php //echo $consultant->name; ?>1 hour @ $<?php //echo $consultant->consultant_price ?></p>
                                                  <p><?php //echo $consultant->consultant_description; ?></p>
                                                 
                                               </div>
                                            </div>
                                          </div>

                                          <?php //} ?>
                                          <!-- <div id="form1">
                                              <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Username</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input class="form-control" name="name" type="text"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Email Address</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input class="form-control" name="email" type="email"/>
                                                </div>
                                            </div>
                                            </div> --> 
                                        </fieldset>
                                        <fieldset title="Step 2">
                                            <legend>Your Info</legend>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">First Name</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input type="text" name="firstname" placeholder="First Name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Last Name</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input type="text" name="lastname" placeholder="Last Name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Phone</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input type="text" name="phone" placeholder="phone" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Email</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input type="text" name="email" placeholder="email" class="form-control">
                                                </div>
                                            </div>

                                            <h3>Eligibility Skype Consultation with Roxanne McInnis Jessome (Senior Consultant)</h3>
                                            <p>Initial Consultation Agreement</p>
                                            <p>This Initial Consultation Agreement sets forth the terms and conditions of an Initial Consultation meeting between Roxanne McInnis Jessome, of Join Canada, (“we” or “us”) and the undersigned Client (“you”):</p>
                                            <p>1. Purpose of Consultation. The purpose of the Initial Consultation is for us (a) to learn about you and your particular immigration situation based on the information you provide, which may include one or more aspects of the Canadian immigration process and related procedures; (b) to answer your questions to the best of our ability; (c) to identify your options and, to the extent possible, analyze the costs and benefits of those alternatives; (d) to help you determine your course of action, if any; (e) to determine the next steps in the process, as appropriate. All information and documents that you provide to us shall remain strictly confidential.</p>
                                            <p>2. No Legal Authority Granted. You agree and understand that this agreement does not constitute your engagement of Roxanne McInnis Jessome to act as your Authorized Representative in respect of any matters relating to Immigration, Refugees and Citizenship Canada, and/or the Canada Border Services Agency.</p>
                                            <p>3. Fee and Cancellation. In exchange for scheduling an Initial Consultation with Roxanne McInnis Jessome of up to 1 hour in duration, you agree to pay a non-refundable Consultation Fee of $175.00 CAD (GST Included) to Join Canada. Despite the foregoing, if your Initial Consultation appointment is cancelled with at least 24 hours’ advance notice provided to us, we will refund the Consultation Fee to the credit card you provided. Furthermore, you may re-schedule your Initial Consultation appointment with at least 24 hour’ advance notice provided to us without penalty. PLEASE NOTE: If additional time is required, additional fees MAY be applicable.</p>
                                            <p>4. Acceptance. By clicking the check box below to indicate your agreement of these terms, you will by that action agree to all of the terms and conditions set forth above in this Initial Consultation Agreement and acknowledge that any further engagement of Roxanne McInnis Jessome will require a further written agreement with different terms.</p>
                                            <p>These terms and conditions shall be governed by the laws in effect in the Province of British Columbia, Canada.</p>
                                            <p>Please be advised that Roxanne McInnis Jessome, RCIC is a member in good standing of the Immigration Consultants of Canada Regulatory Council and as such, is bound by its By-Laws, Code of Professional Ethics, and associated Regulations.</p>
                                            <label>I Understand and Agree to the Terms of the Initial Consultation Agreement *</label>
                                            <input name="accept_aggrement" id="" class="widget-single-checkbox" value="yes" type="checkbox">

                                            <p>Please enter your current address *</p>
                                            <textarea name="current_address" id="add_comment" cols="42" rows="6"></textarea><br/>
                                            <p>Is this your first consultation with Roxanne? 
                                            <input name="field" value="yes" type="radio"> yes
                                            <input name="field" value="no" type="radio"> no

                                           </p>
                                           <p>If you have had a previous consultation with Roxanne, please enter the date that consultation took place. If you do not know the date, please estimate the month and year it took place.</p>
                                           <input class="text form-control" name="estimate_date" value="" type="text">

                                           <p>Roxanne's Skype ID is RJESSOME Please add her to your Skype contacts. Do you have a Skype account? *</p>
                                           <input name="skype_account" value="yes" type="radio"> yes
                                            <input name="skype_account" value="no" type="radio"> no <br/>

                                            <p>What is your Skype ID? </p>
                                            <input class="text form-control" name="skype_id" value="" type="text">

                                            <p>Our preference is that you are using Skype on a computer but we can still conduct this consultation using smart phones and tablets. Please advise what tool you will be using to conduct this consultation. *</p>
                                            <select name="field" class="form-control inline-field">
                                            <option value="" selected=""></option>
                                            <option value="Computer or Laptop">Computer or Laptop</option>
                                            <option value="Tablet">Tablet</option>
                                            <option value="Smart Phone">Smart Phone</option>
                                            </select>

                                            <p>Do you know what type of immigration matter you would like to discuss? *

                                                <select name="immigration_type" class="form-control inline-field">
                                                    <option value="" selected=""></option>
                                                    <option value="Family Sponsorship">Family Sponsorship</option>
                                                    <option value="Express Entry Program (Federal Skilled Worker - Canadian Experience Class - Federal Skilled Trades)">Express Entry Program (Federal Skilled Worker - Canadian Experience Class - Federal Skilled Trades)</option>
                                                    <option value="Provincial Nominee">Provincial Nominee</option>
                                                    <option value="Extension of Status in Canada">Extension of Status in Canada</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </p>

                                            <p>Have you ever applied for permanent residence to Canada before? *
                                                <input name="applied_residence" value="yes" type="radio"> yes
                                            <input name="applied_residence" value="no" type="radio"> no
                                            </p>

                                            <p>Are you currently living in Canada? *
                                                <input name="living_canada" value="yes" type="radio"> yes
                                            <input name="living_canada" value="no" type="radio"> no
                                            </p>

                                            <p>If you are currently in Canada, what date did you arrive? </p>
                                            <input class="text form-control" name="arrive_date" value="" type="text">

                                            <p>If you are currently in Canada, what is your immigration status? 

                                                <select name="immigration_status" class="form-control inline-field">
                                                    <option value="" selected=""></option>
                                                    <option value="Citizen">Citizen</option>
                                                    <option value="Permanent Resident">Permanent Resident</option>
                                                    <option value="Foreign Worker">Foreign Worker</option>
                                                    <option value="Student">Student</option><option value="Visitor">Visitor</option>
                                                    <option value="I Don't Know My Status">I Don't Know My Status</option><option value="I'm Not In Canada">I'm Not In Canada</option>
                                                </select>

                                            </p>
                                            <!-- <p>If you are currently in Canada, what date does your current status expire? If your status has already expired, please enter the date of expiry.</p>
                                            <input class="text form-control" name="field" value="" type="text">
                                            <p>Briefly explain your immigration situation: (What would you like to discuss?) Please provide as much detail as possible. </p>
                                            <textarea cols="42" rows="6" name="field" class="form-control" maxlength=""></textarea>-->

                                            <p>You may upload documents you wish us to review during the consultation (Optional) </p>
                                            <input type="file" name="consultation_document1">

                                            <!-- <p>You upload documents you wish us to review during the consultation (Optional) </p>

                                                <input type="file" name="document"> -->

                                            <p>Were you referred to Roxanne by anyone? *
                                            <input name="referred_by" value="yes" type="radio"> yes
                                            <input name="referred_by" value="no" type="radio"> no
                                            </p>

                                            <p>If you were referred to Roxanne by another person, please enter that person's name. We <!--would like to thank them for the referral!</p>
                                            <input class="text form-control" name="field" value="" type="text">   -->  




                                        </fieldset>

                                        </fieldset>
                                        <!-- <fieldset title="Step 3">
                                            <legend>Confirmation</legend>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Text Input</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input type="text" placeholder="Text Input" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Checkbox</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <label class="checkbox">
                                                        <input type="checkbox" value="">
                                                        Option one is this and that—be sure to include why it's great </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Radio</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <label class="radio">
                                                        <input type="radio" name="optionsRadios" value="option1" checked>
                                                        Option one is this and that—be sure to include why it's great </label>
                                                </div>
                                            </div>
                                        </fieldset> -->
                                        <button type="submit" class="finish btn btn-info btn-extend"> Finish!</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


       
    <section id="invite" class="pad-lg light-gray-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2 text-center">
            <i class="fa fa-envelope-o margin-40"></i>
            <h2 class="black">Help me immigrate!</h2>
            <br />
            <p class="black">To get your <a href="https://www.abc-lang.com/free-assessments" style="text-decoration: underline; color: #5563c7;">Free Assessment </a> by email, please provide your email address below. We will send you your no-obligation Free Assessment right away!</p>
            <br />
            
            <div class="row">
              <?php if(!empty($statusMsg)){ ?>
                  <p class="statusMsg <?php echo !empty($msgClass)?$msgClass:''; ?>"><?php echo $statusMsg; ?></p>
              <?php } ?>
              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <!-- <form method="post" action=""> -->
                  <div class="form-group">
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter Email">
                  </div>
                  <input type="submit" class="btn btn-primary btn-lg" value="Help me immigrate!">   
                <!-- </form> -->
              </div>
            </div><!--End Form row-->

          </div>
        </div>
      </div>
    </section>
    
    
    <section id="carousel" class="carousel-facts">            
  <div class="container">
    <p class="text-center canada">Interesting facts about Canada</p>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
                <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
        <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
          <!-- Carousel indicators -->
          <ol class="carousel-indicators">
            <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="6"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="7"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="8"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="9"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="9"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="10"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="11"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="12"></li>
            <li data-target="#fade-quote-carousel" data-slide-to="13"></li>
            <!-- <li data-target="#fade-quote-carousel" data-slide-to="14"></li> -->
          </ol>
          <!-- Carousel items -->
          <div class="carousel-inner">
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(0,0,0,.2);"></div>
              <blockquote>
                <p>Canada is the second largest country in the world, right after Russia. However, Canada is the 8th least dense country in the world - Canada has fewer people than Tokyo’s metropolitan area!</p>
              </blockquote> 
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
              <blockquote>
                <p>The quality of Canada’s tap water is often better than bottled water.</p>
              </blockquote>
            </div>
            <div class="active item">
                        <div class="profile-circle" style="background-color: rgba(145,169,216,.2);"></div>
              <blockquote>
                <p>Canada is the World Most Educated Country: over half its residents have college degrees.</p>
              </blockquote>
            </div>
                    <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>Police Departments in Canada give out "positive tickets" when they see people doing something positive.</p>
              </blockquote>
            </div>
                    <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>No cows in Canada are given artificial hormones for milk production. (Which means no dairy products, like milk, cheese, or yogurt, produced in Canada contain hormones either!)</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>The quality of Canada’s tap water is often better than bottled water.</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>The U.S. / Canada Border is the longest international border in the world and it lacks military defense.</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>Canada has had no weapons of mass destruction since 1984</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>Canada has the third largest oil reserves of any country in the world after Saudi Arabia and Venezuela.</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                   <p>Canada has a strategic maple syrup reserve to ensure global supply in case of emergency.</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>People from Canada can order a portrait of Queen Elizabeth II and have it shipped to them for free.</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>It's estimated that 93,000 Canadians live in the US with expired visas - more than any other group of immigrants!</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>Canada is bigger than the entire European Union (33 times bigger than Italy and 15 times bigger than France), more than 30 per cent larger than Australia, five times as big as Mexico, three times as big as India and about the same size as 81,975 Walt Disney Worlds put together.</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>For one day in 1943, Ottawa designated a hospital room to be “exterritorial”  (international) ground so a Dutch princess could be born a full Dutch citizen. (A Dutch requirement for her to keep her princess title.) And every year the Netherlands sends Canada thousands of tulips to show their gratitude.</p>
              </blockquote>
            </div>
            <div class="item">
                        <div class="profile-circle" style="background-color: rgba(77,5,51,.2);"></div>
                <blockquote>
                <p>Canada’s national parks are bigger than some countries! Wood Buffalo National Park is Canada’s largest national park at 44 802 km². It straddles the border of Northern Alberta and southern Northwest Territories. It was created in 1922 to protect the world’s largest herd of roaming Wood Bison. This park is so large that it’s bigger than the country of Switzerland (41,285 km²), the Netherlands (41,850 km²) and Bhutan (38,394 km²), among many others.</p>
              </blockquote>
            </div>
           
          </div>
        </div>
      </div>              
    </div>
  </div>
</section>


<script type="text/javascript">
    $(document).ready(function(){
        $('#form1').hide();
    $('.well').click(function(){
        var id = $(this).attr('id');

        //alert(id);
        $.ajax({
       type:'POST',
       url:'<?php echo base_url(); ?>web/get_view_ajax',
       //url:'<?php echo base_url(); ?>web/index',
       data: "id="+id,
       success:function(data){
        console.log(data);
        $('.well').hide();
        $('.wells').show();
        $('.well').hide();
        //$('#div_result').append('<p>hello world</p>');
        alert("Success!");
        /*$.each(data.results, function(k, v) {
            $.each(v, function(key, value) {
                $('#div_result').append('<br/>' + key + ' : ' + value);
            })
        })*/
    }
       

      });

    });
    })
</script>
