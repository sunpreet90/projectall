<?php    
    //To get Consultant Data
    $arr_data = $this->db->query("SELECT * FROM `ws_consultants` WHERE `id` = $formid ")->result_array();    
?>
<style>
@media screen and (min-width: 768px) {
        .modal-dialog {
          min-width: 700px; /* New width for default modal */
        }
        .modal-sm {
          min-width: 350px; /* New width for small modal */
        }
    }
    @media screen and (min-width: 992px) {
        .modal-lg {
          min-width: 950px; /* New width for large modal */
        }
}
</style>
    <fieldset title="Step 2">
       <div class="row col-sm-12">
            <div class="custom-form" data-form-id="879523">
                <?= $arr_data[0]['consultant_long_description']; ?>
                
                <div class="form-group required-field">
                   <input name="accept_aggrement" id="accept_aggrement" class="widget-single-checkbox" value="yes" type="checkbox" onchange="isChecked(this, 'book')">

                    <label  class="control-label babel-ignore">I Understand and Agree to the <a href="#" data-toggle="modal" data-target="#termsModal">Terms</a> of the Initial Consultation Agreement <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                </div>              
            </div>
        </div>
        </fieldset>

        <div class="footerSection">
            <div class="row">
                <div class="col-lg-12 col-md-12" style="text-align: center;">
                    <a href="<?php echo "http://immcan.mobilytedev.com/booking?url=".base64_encode($arr_data[0]['profile_url']); ?>"><input type="button" name="submit" id="book" disabled="disabled" class="btn btn-success" value="BOOK NOW"></a>
                </div>
            </div>
        </div>


<script>
    function isChecked(checkbox, book) {
        document.getElementById(book).disabled = !checkbox.checked;
    }
</script>

<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">  
        <div class="modal-header">
            <h4>Terms of the Initial Consultation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
        </div>    
      <div class="modal-body">        
        <?php echo $arr_data[0]['consultant_terms']; ?>
        <?php //echo $arr_data[0]['consultant_long_description']; ?>
      </div>      
    </div>
  </div>
</div>

