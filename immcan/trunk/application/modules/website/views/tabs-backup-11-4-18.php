    <link href="<?php echo base_url('assets') ?>/css/jquery.stepy.css" rel="stylesheet">

    <style>
        .wrapper {
            margin-top: 100px;
            background-color: #f9f9f9;
        }
        .border-widget-block {
            padding: 19px;
            border-radius: 4px;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            margin-bottom: 75px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
        }
        .text-block {
            color: #a1a1a1;
            font-size: 16px;
        }
        .wrapper-text-block {
            color: #929496;
            font-size: 16px;
        }
        #stepy_form-buttons-0 {
            position: absolute;
            right: 0;
            padding: 43px 20px 0 0;
        }
        #stepy_form-buttons-1 {
            position: relative;
            right: 0;
            padding: 43px 20px 0 0;
        }
        .custom-form{padding:50px;}
        .radio-inline {
            display: inline-block !important;

        }
        .step select {

            margin-left: 7px;
            border-radius: 6px;

        }
        .choose {

            border: 1px solid #2dab88;
            color: #2dab88;

        }
        a#stepy_form-back-1 {
            position: absolute;
            top: 82px;
            left:0;
        }
        a#stepy_form-next-1 {
            position: absolute;
            top: 82px;
            right:0;
        }
        .consultant {
            border: 1px solid#ccc;
            border-radius: 4px;
            margin-bottom: 20px;
            padding: 20px;
        }
        .consultant img{max-width:87%;width:100%;}
        .main_menu_area {
            position: absolute;
            width: 100%;
            top: 0px;
            left: 0px;
            z-index: 30;
            padding: 0px 75px;
            border-bottom:0px !important;
            background-color: #bcafc9;
        }
    </style>

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h4 class="fw-title">Form Wizard with Validation</h4>
                    <div class="box-widget">
                            
                        <div class="widget-head clearfix">
                            <div id="top_tabby" class="block-tabby pull-left"></div>
                        </div>

                        <div class="widget-container">
                            <div class="widget-block border-widget-block">
                                <div class="widget-content box-padding">
                                    <form id="stepy_form" class="form-horizontal left-align form-well" method="POST" action="<?= base_url() ?>website/book_consultation" enctype='multipart/form-data'>
                                        <fieldset title="Step 2">
                                            <legend>description two</legend>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label for="usr">First Name:</label>
                                                    <input type="text" name="firstname" class="form-control" id="usr">
                                                </div>
                                                <div class="col-sm-9">
                                                    <label for="usr">Last Name:</label>
                                                        <input type="text" name="lastname" class="form-control" id="usr">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label for="usr">Phone:</label>
                                                    <input type="text" name="phone" class="form-control" id="usr">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label for="usr">E-mail</label>
                                                    <input type="text" name="email" class="form-control" id="usr">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="custom-form" data-form-id="879523">
                                                    <h2 class="babel-ignore">General Eligibility Skype Consultation with Ning Pagaling-Becada (Consultant) </h2>
                                                    <p class="babel-ignore form-description">Initial Consultation Agreement<br><br>
                                                    This Initial Consultation Agreement sets forth the terms and conditions of an Initial Consultation meeting between Ning Pagaling-Becada, of Join Canada, (“we” or “us”) and the undersigned Client (“you”):<br><br>
                                                    1. Purpose of Consultation. The purpose of the Initial Consultation is for us (a) to learn about you and your particular immigration situation based on the information you provide, which may include one or more aspects of the Canadian immigration process and related procedures; (b) to answer your questions to the best of our ability; (c) to identify your options and, to the extent possible, analyze the costs and benefits of those alternatives; (d) to help you determine your course of action, if any; (e) to determine the next steps in the process, as appropriate. All information and documents that you provide to us shall remain strictly confidential.<br>
                                                    <br>
                                                    2. No Legal Authority Granted. You agree and understand that this agreement does not constitute your engagement of Ning Pagaling-Becada to act as your Authorized Representative in respect of any matters relating to Immigration, Refugees and Citizenship Canada, and/or the Canada Border Services Agency.<br>
                                                    <br>
                                                    3. Fee and Cancellation. In exchange for scheduling an Initial Consultation with Ning Pagaling-Becada of up to 1 hour in duration, you agree to pay a non-refundable Consultation Fee of $165.00 CAD (GST Included) to Join Canada. Despite the foregoing, if your Initial Consultation appointment is cancelled with at least 24 hours’ advance notice provided to us, we will refund the Consultation Fee to the credit card you provided. Furthermore, you may re-schedule your Initial Consultation appointment with at least 24 hour’ advance notice provided to us without penalty.  PLEASE NOTE:  If additional time is required, additional fees MAY be applicable.<br>
                                                    <br>
                                                    4. Acceptance. By clicking the check box below to indicate your agreement of these terms, you will by that action agree to all of the terms and conditions set forth above in this Initial Consultation Agreement and acknowledge that any further engagement of Ning Pagaling-Becada will require a further written agreement with different terms.<br>
                                                    <br>
                                                    These terms and conditions shall be governed by the laws in effect in the Province of British Columbia, Canada.<br>
                                                    <br>
                                                    Please be advised that Ning Pagaling-Becada, RCIC is a member in good standing of the Immigration Consultants of Canada Regulatory Council and as such, is bound by its By-Laws, Code of Professional Ethics, and associated Regulations.<br>
                                                    </p>
                                                    
                                                    <div class="form-group required-field">
                                                       <input name="accept_aggrement" class="widget-single-checkbox" value="yes" type="checkbox">
                                                        <label  class="control-label babel-ignore">I Understand and Agree to the Terms of the Initial Consultation Agreement <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                                                    </div>

                                                    <div class="form-group required-field">
                                                        <label class="control-label babel-ignore">Please enter your current address including postal code <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                                                        <div>
                                                            <textarea cols="42" rows="6" name="current_address" class="form-control" maxlength="65535"></textarea>
                                                        </div>                                                                    
                                                    </div>

                                                    <div class="form-group required-field" data-field-id="4560892">
                                                        <label for="field:4560892" class="control-label babel-ignore">Is this your first consultation with Ning? 
                                                            <span class="error">*</span><span class="show-on-error">(required)</span>
                                                        </label>
                                                        <span class="widget-checkbox">
                                                            <label class="radio-inline"><input name="field" value="yes" type="radio"> yes</label>
                                                            <label class="radio-inline"><input name="field" value="no" type="radio"> no</label>
                                                        </span>            
                                                    </div>

                                                    <div class="form-group " data-field-id="4560895">
                                                        <label for="field:4560895" class="control-label babel-ignore">If you have had a previous consultation with Ning, please enter the date that consultation took place. If you do not know the date, please estimate the month and year it took place. </label>
                                                        <input class="text form-control" name="estimate_date" type="text">         
                                                    </div>

                                                    <div class="form-group required-field" data-field-id="4560898">
                                                        <label for="field:4560898" class="control-label babel-ignore">Ning's Skype ID is NING@JOINCANADA.COM.  Please add her to your Skype contacts.  Do you have a Skype account? <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                                                        <span class="widget-checkbox">
                                                            <label class="radio-inline">
                                                                <input name="skype_account" value="yes" type="radio"> yes
                                                            </label>
                                                            <label class="radio-inline"><input name="skype_account" value="no" type="radio"> no</label>
                                                        </span>        
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label babel-ignore">What is your Skype ID? </label>
                                                        <input class="text form-control" name="skype_id" value="" type="text">            
                                                    </div>

                                                    <div class="form-group required-field">
                                                        <label for="field:4560904" class="control-label babel-ignore">Our preference is that you are using Skype on a computer but we can still conduct this consultation using smart phones and tablets.  Please advise what tool you will be using to conduct this consultation. <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                                                        <select name="field" class="form-control inline-field">
                                                            <option value="" selected=""></option>
                                                            <option value="Computer or Laptop">Computer or Laptop</option>
                                                            <option value="Tablet">Tablet</option>
                                                            <option value="Smart Phone">Smart Phone</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group required-field" data-field-id="4560907">
                                                        <label class="control-label babel-ignore">Do you know what type of immigration matter you would like to discuss? <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                                                        <select name="immigration_type" class="form-control inline-field">
                                                            <option value="" selected=""></option>
                                                            <option value="Family Sponsorship">Family Sponsorship</option>
                                                            <option value="Express Entry Program (Federal Skilled Worker - Canadian Experience Class - Federal Skilled Trades)">Express Entry Program (Federal Skilled Worker - Canadian Experience Class - Federal Skilled Trades)</option><option value="Provincial Nominee">Provincial Nominee</option>
                                                            <option value="Citizenship">Citizenship</option>
                                                            <option value="Extension of Status in Canada">Extension of Status in Canada</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group required-field">
                                                        <label for="field:4560910" class="control-label babel-ignore">Have you ever applied for permanent residence to Canada before? <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                                                        
                                                        <span class="widget-checkbox">
                                                            <label class="radio-inline">
                                                                <input name="applied_residence" value="yes" type="radio"> yes
                                                            </label>
                                                            <label class="radio-inline"><input name="applied_residence" value="no" type="radio"> no</label>
                                                        </span>
                                                    </div>

                                                    <div class="form-group required-field" data-field-id="4560913">
                                                        <label for="field:4560913" class="control-label babel-ignore">Are you currently living in Canada? <span class="error">*</span> <span class="show-on-error">(required)</span></label>
                                                        <span class="widget-checkbox">
                                                            <label class="radio-inline">
                                                                <input name="living_canada" value="yes" type="radio"> yes
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input name="living_canada" value="no" type="radio"> no
                                                            </label>
                                                        </span>        
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label babel-ignore">If you are currently in Canada, what date did you arrive? </label>
                                                        <input class="text form-control" name="arrive_date" value="" type="text">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label babel-ignore">If you are currently in Canada, what is your immigration status? </label>
                                                        <select name="immigration_status" class="form-control inline-field">
                                                            <option value="" selected=""></option>
                                                            <option value="Citizen">Citizen</option>
                                                            <option value="Permanent Resident">Permanent Resident</option>
                                                            <option value="Foreign Worker">Foreign Worker</option>
                                                            <option value="Student">Student</option>
                                                            <option value="Visitor">Visitor</option>
                                                            <option value="I Don't Know My Status">I Don't Know My Status</option>
                                                            <option value="I'm Not In Canada">I'm Not In Canada</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group " data-field-id="4560922">
                                                        <label for="field:4560922" class="control-label babel-ignore">If you are currently in Canada, what date does your current status expire?  If your status has already expired, please enter the date of expiry. </label>
                                                        <input class="text form-control" name="field:4560922" value="" type="text">
                                                    </div>

                                                    <div class="form-group required-field" data-field-id="4560925">
                                                        <label for="field:4560925" class="control-label babel-ignore">Briefly explain your immigration situation: (What would you like to discuss?)  Please provide as much detail as possible. <span class="error">*</span><span class="show-on-error">(required)</span></label>
                                                        <div>
                                                            <textarea cols="42" rows="6" name="field:4560925" class="form-control" maxlength="65535"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group " data-field-id="4560943">
                                                        <label for="field:4560943" class="control-label babel-ignore">Providing a CV or resume can be helpful in determining eligibility in Economic immigration.  Please feel free to upload your CV/resume here. </label>

                                                        <div id="field-container:4560943">
                                                            <a class="btn btn-default choose" href="javascript:doFileUpload('4560943')">Choose File...</a> <span id="file-name:4560943"></span>
                                                            <input autocomplete="off" id="field:4560943" name="field:4560943" value="" type="hidden">
                                                        </div>
                                                    </div>

                                                    <div class="form-group " data-field-id="4560928">
                                                        <label for="field:4560928" class="control-label babel-ignore">You may upload documents you wish us to review during the consultation (Optional) </label>
                                                        <div id="field-container:4560928">
                                                            <a class="btn btn-default choose" href="javascript:doFileUpload('4560928')">Choose File...</a>
                                                            <span id="file-name:4560928"></span>
                                                            <input autocomplete="off" id="field:4560928" name="field:4560928" value="" type="hidden">
                                                        </div>
                                                    </div>

                                                    <div class="form-group " data-field-id="4560931">
                                                        <label for="field:4560931" class="control-label babel-ignore">You upload documents you wish us to review during the consultation (Optional) </label>
                                                        <div id="field-container:4560931">
                                                            <a class="btn btn-default choose" href="javascript:doFileUpload('4560931')">Choose File...</a>
                                                            <span id="file-name:4560931"></span>
                                                            <input autocomplete="off" id="field:4560931" name="field:4560931" value="" type="hidden">
                                                        </div>                                                    
                                                    </div>

                                                    <div class="form-group required-field" data-field-id="4560934">
                                                        <label for="field:4560934" class="control-label babel-ignore">Were you referred to Ning by anyone?
                                                        <span class="error">*</span>
                                                        <span class="show-on-error">(required)</span></label>
                                                        <span class="widget-checkbox">
                                                            <label class="radio-inline">
                                                            <input name="field:4560934" value="yes" type="radio"> yes</label><label class="radio-inline"><input name="field:4560934" value="no" type="radio"> no</label>
                                                        </span>            
                                                    </div>

                                                    <div class="form-group " data-field-id="4560937">
                                                        <label for="field:4560937" class="control-label babel-ignore">If you were referred to Ning by another person, please enter that person's name.  We would like to thank them for the referral! </label>
                                                        <input class="text form-control" name="field:4560937" value="" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <fieldset title="Step 3">
                                            <legend>description three</legend>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Text Input</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <input type="text" placeholder="Text Input" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Checkbox</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <label class="checkbox">
                                                        <input type="checkbox" value="">
                                                        Option one is this and that—be sure to include why it's great </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 col-sm-2 control-label">Radio</label>
                                                <div class="col-md-6 col-sm-6">
                                                    <label class="radio">
                                                        <input type="radio" name="optionsRadios" value="option1" checked>
                                                        Option one is this and that—be sure to include why it's great </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <button type="submit" class="finish btn btn-info btn-extend"> Finish!</button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> 
    <script type="text/javascript">
        $(document).ready(function(){
            
            $('.consultant').click(function(){

                var id = $(this).attr('id');
                    
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url(); ?>website/get_view_ajax',
                    data: "id="+id,
                    success:function(data){
                        console.log(data);
                        $('.consultant').hide();
                        $('.consultants').show();   
                    }
                });
            });
        })
    </script>