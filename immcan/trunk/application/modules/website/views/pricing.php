<style type="text/css">
	
/*---------------Pricing Tables-------------------*/
.white {
    color: #ffffff;
}
h2 {
    font-size: 40px;
    line-height: 40px;
    etter-spacing: -0.5px;
    color: #ffffff;
    font-weight: 700;
}

#pricing {
  background: #70cbce;
}

.pricing-container {
  padding-left: 0px;
  padding-right: 0px;
}

.pricing-table {
  background: transparent;
  margin-bottom: 50px;
  margin-top: 0px;
  -webkit-transition: all 0.25s ease-in-out;
  -moz-transition: all 0.25s ease-in-out;
  -o-transition: all 0.25s ease-in-out;
  transition: all 0.25s ease-in-out;
}
.pricing-table.active {
  box-shadow: 0px 0px 12px rgba(41,46,50,0.6); 
  position: absolute;
  margin: auto;
  z-index: 200;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
}

.pricing-table:hover {
  margin-top: -10px;
  -webkit-transition: all 0.25s ease-in-out;
  -moz-transition: all 0.25s ease-in-out;
  -o-transition: all 0.25s ease-in-out;
  transition: all 0.25s ease-in-out;
}

.pricing-table li {
  padding-left: 20px;
  padding-right: 20px;
}

.headline {
  background: #231f20;
  color: #FFFFFF;
  padding: 10px;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
}

.price {
    background: none repeat scroll 0 0 #231f20;
    color: #FFFFFF;
    font-size: 32px;
    font-weight: bold;
    padding-bottom: 20px;
    padding-top: 20px;
}


.pricing-table:hover li.price  {
  background: #303236;
  -webkit-transition: all 0.85s ease-in-out;
  -moz-transition: all 0.85s ease-in-out;
  -o-transition: all 0.85s ease-in-out;
  transition: all 0.85s ease-in-out;
}

.price small {
  font-weight: 300;
  color: #929496;
}

.info {
  padding-top: 20px;
  padding-bottom: 20px;
  font-weight: 300;
  font-size: 13px;
  color: #929496;
  background: #ffffff;
}

.features {
  color: #231f20;
  font-weight: bold;
  padding-top: 12px;
  padding-bottom: 15px;
  border-bottom: 1px dotted #E8EAEA;
  background: #ffffff
}

.features.first {
  border-top: 1px dotted #E8EAEA;
}

.features.last {
  padding-top: 17px;
  padding-bottom: 20px;
  width: 100%;
}

.features.last a {
	color: #70cbce;
	font-size: 14px;
	letter-spacing: 1px;
}

.pricing-container .btn {
    border-radius: 0;
}

h3.hclass {
    font-size: 16px;
    line-height: 30px;
    letter-spacing: 0px;
    color: #231f20;
    font-weight: 700;
}

.row.margin-50 {
    margin-top: 5%;
}

h2.white {
    margin-top: 2%;
}

.col-sm-4.pricing-container.wow.fadeInUp {
    border: 1px solid #70ccce;
}
</style>
<!-- Start Banner Area -->
<section class="banner-area relative" style="background-image:url('<?php echo base_url() ?><?= get_config_item('pricing_header_image'); ?>'); background-size: cover; background-repeat: no-repeat;">
	<div class="overlay overlay-bg"></div>
	<div class="container">
		<div class="row fullscreen align-items-center justify-content-center" style="height:400px;">
			<div class="col-lg-12">
				<div class="banner-content">
					<h1 class="white-color"><?=get_config_item('pricing_banner_title')?></h1>
					<p class="text-white"><?=get_config_item('pricing_banner_tagline')?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Banner Area -->



<section id="pricing" class="pad-lg">
      <div class="container">
        <div class="row margin-40">
          <div class="col-sm-12 col-sm-offset-2 text-center">
            <h2 class="white"><?=get_config_item('pricing_page_title')?></h2>
            <p class="white"><?=get_config_item('pricing_page_tagline')?></p>
          </div>
        </div>
        
        <div class="row margin-50">
          
          
            	<?php if (!empty($pricing)) {
            	$counter = 0;
            	$counters = 0;
            	$counters_type = 0; 
            	$counters_dollar = 0; 
            	foreach ($pricing as $note) { ?>
            		
            	<div class="col-sm-4 pricing-container wow fadeInUp" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: fadeInUp;">
		            <br>
		            <ul class="list-unstyled pricing-table text-center">
    					<li class="headline">
    						<h5 class="white"><?php if($counters_type == 0) { echo "Silver"; } if($counters_type == 1)
                                                { echo "Gold"; } if($counters_type == 2){ echo "Platinum"; }  ?></h5>
    					</li>
    					<li class="price">
    						<div class="amount"><?php if($counters_dollar == 0) { echo "$"; } if($counters_dollar == 1)
                                                { echo "$$"; } if($counters_dollar == 2){ echo "$$$"; }  ?><small></small>
    						</div>
    					</li>
    					<li class="info features"><h3 class="hclass"><?=$note->post_title?></h3></li>
    					<li class="features">- &nbsp; <?=$note->post_content?></li>
    					<!-- <li class="features">- &nbsp;Application review</li>
    					<li class="features">- &nbsp;Settlement/employment info</li>
    					<li class="features">- &nbsp;Email consultation</li>
    					<li class="features">- &nbsp;Phone/Skype consultation</li>
    					<li class="features">This option is best suited for people who like to complete all paperwork by themselves and have basic questions or want to make sure their application is complete and free of errors. Please note: If you chose this option, we will not submit the application on your behalf and act as your representative.</li> -->
    					<li class="features last btn-wide">
    						<!-- <a href="#">Get Started</a> -->
    					<a class="tp_btn test" href="<?php if($counter == 0) { echo base_url('get-started'); } if($counter == 1) { echo base_url('consultant-booking'); } if($counter == 2) { echo base_url('contact/104'); } ?>" style="transition: none; text-align: inherit; line-height: 40px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;"> 
    						<?php if($counters == 0) { echo "Get Started"; } if($counters == 1)
                                                { echo "Book Consultation"; } if($counters == 2){ echo "Contact Us "; }  ?>
                                                	
                                                </a>
    					</li>
    				</ul>
          </div>
    			<?php $counters++; $counter++; $counters_type++; $counters_dollar++; } } ?>
          
          <!-- <div class="col-sm-4 pricing-container wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
            <ul class="list-unstyled pricing-table active text-center">
    					<li class="headline"><h5 class="white">Gold</h5></li>
    					<li class="price"><div class="amount">$$<small></small></div></li>
    					<li class="info features"><h3 class="hclass">Application forms are often complex and immigration process can be very confusing. This option is best for people who like to be fully represented on their applications.</h3></li>
    					<li class="features first">- &nbsp;Full representation</li>
    					<li class="features">- &nbsp;Communication with CIC/other agencies</li>
    					<li class="features">- &nbsp;Information research, document translation</li>
    					<li class="features">- &nbsp;Interview preparation/coaching</li>
    					<li class="features">This option is best suited for people who like to complete all paperwork by themselves and have basic questions or want to make sure their application is complete and free of errors. Please note: If you chose this option, we will submit the application on your behalf and act as your representative throughout application process.</li>
    					<li class="features last btn btn-secondary btn-wide"><a href="#">Get Started</a></li>
    				</ul>
          </div>
          

           <div class="col-sm-4 pricing-container wow fadeInUp" data-wow-delay="1.3s" style="visibility: visible; animation-delay: 1.3s; animation-name: fadeInUp;">
            <br>
            <ul class="list-unstyled pricing-table text-center">
    					<li class="headline"><h5 class="white">Platinum</h5></li>
    					<li class="price"><div class="amount">$$$<small></small></div></li>
    					<li class="info features"><h3 class="hclass">This option is best for people with special needs and complex applications.</h3></li>
    					<li class="features first">- &nbsp;Criminal/Medical Inadmissibility</li>
    					<li class="features">- &nbsp;Criminal Rehabilitations</li>
    					<li class="features">- &nbsp;Labour Market Impact Assessments</li>
    					<li class="features">- &nbsp;NAFTA permits</li>
    					<li class="features">- &nbsp;Business/Investor/Self-Employed Applications</li>
    					<li class="features">We will ensure that your situation is presented to immigration officials in the most favourable light. Please note: If you chose this option, we will submit the application on your behalf and act as your representative during appeals and up to (excluding) Federal Court.</li>
    					<li class="features last btn btn-secondary btn-wide"><a href="#">Get Started</a></li>
    				</ul>
          </div>  -->
          
        </div>
        
      </div>
    </section>


<!-- Start Service Area -->

<!-- End Service Area -->

