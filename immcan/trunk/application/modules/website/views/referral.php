<?php
//Random Number Generation
$rand=substr(rand(),0,4); //only show 4 numbers
?>
<style type="text/css">
.captcha
{
width:60px; 
background-image:url('../uploads/media/cat.png'); 
font-size:20px; 
border: 1px solid;
}
.color
{
    color:#FF0000;
}
.refresh {
   height: 35px;
   width: 35px;
}
.placing {
    margin: 45px auto 0;
}
.fade {
    opacity: 1;
}
#errmsg
{
color: red;
}
</style>
<style>
 .referal {
    padding: 50px 0 50px 0;
    /*margin-top: 20px;*/
    background-color: #dadada;
}
.sub-button {
    margin-left: 20px;
    margin-top: 10px;
    padding: 6px 12px;
    background-color: #3578b6;
    border-radius: 6px;
    color: #fff;
}
.referal h3 {
    color: #3577b6;
    padding-bottom: 10px;
    font-weight: 400;
}
.referal p {
    font-size: 16px;
    color: #7d7b7b;
    padding: 0 0 20px 0;
}
.main_menu_area {
    position: absolute;
    width: 100%;
    top: 0px;
    left: 0px;
    z-index: 30;
    padding: 0px 75px;
    border-bottom:0px !important;
    background-color: #bcafc9;
}
.referal-inner {
    /*max-width: 50%;*/
    margin: 0 auto;
    background-color: #fff;
    border-radius: 6px;
    padding: 30px;
   /* border-bottom: 18px solid#3577b6;*/
}
.referal-inner img {
    max-width: 100%;
    height: auto;
}

.referal-inner label {
    font-weight: bold;
    color: #595959;
    font-size: 14px;
    margin-top: 10px;
}

.numberCircle {
    border-radius: 50%;
    width: 20px;
    height: 20px;
    padding: 3px;
    background: #f05a22;
    border: 2px solid #666;
    color: #fff;
    text-align: center;
    font: Arial, sans-serif;
}

.project_area {
   /* background: url("https://refer.ringcentral.com/RINGCENTRAL/_Asset/HERO2.png?version=1") no-repeat center center;*/
    background-size: cover;
    position: relative;
    z-index: 3;
    padding: 115px 0px;
    margin-top: 3%;
}

.affiliate-qa .qa {
    border: 1px solid #3576b640;
    margin-bottom: 5px;
    border-radius: 5px;
    padding: 5px 0px;
}
.affiliate-qa .qa label {
    margin: 0;
    color: #3576b6;
    font-weight: 500;
}

.affiliate-qa .qa select {
    padding: .3rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #3576b6;
    background-color: #fff;
    background-image: none;
    background-clip: padding-box;
    border: 1px solid #3576b661;
    border-radius: .25rem;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

</style>

<!--================Slider Area =================-->
<section class="project_area" style="background-image:url('<?php echo base_url() ?><?=get_config_item('banner_header_image'); ?>'); background-position: 50% 63px;">
            <div class="container">
                <div class="project_inner">
                    <div class="center_title">
                        <h2><?=get_config_item('banner_title')?></h2>                        
                    </div>                    
                </div>
            </div>
</section>

        <!--================End Slider Area =================-->
<section class="referal">
<div class="container">  
  <h1 style="text-align: center;"><?=get_config_item('banner_tagline')?></h1>     
       
  <div class="row" style="margin-top: 1%;">
    <div class="col-sm-12 referal-inner" style="">
        <?=get_config_item('banner_content')?>

       <!--Form Section-->
  
<div class="col-sm-12 referal-inner" style="background-color:#F0F8FC;">
<form name="referral" method="POST" action="<?=base_url()?>website/add_referral" novalidate="novalidate"> 
<?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>       
    <div class="container">
<div class="row">
<h3>CLIENT REFERRAL INTRODUCTION FORM</h3>
</div> 
<div class="row">
<!-- <P>Please complete the following form and submit it.</P> -->
</div>
<div class="row">
<div class="col-md-12">
<label for="email">Your Name:</label>
    <input value="<?php echo set_value('name'); ?>" type="text" name="name" class="form-control" id="name" placeholder="Enter Name*" required />
</div>
<div class="col-md-12">
<label for="email">Referral Name:</label>
    <input value="<?php echo set_value('referral_name'); ?>" type="text" name="referral_name" class="form-control" id="referral_name" placeholder="Name of the person you refer*" required />
</div>
</div>
<!-- <div class="row">
<div class="col-md-6">
<label for="email">Your Address:</label>
    <input value="<?php echo set_value('address'); ?>" type="text" name="address" class="form-control" id="address" placeholder="Enter address">
</div>
<div class="col-md-6">
<label for="email">Introducing City:</label>
    <input value="<?php echo set_value('referral_city'); ?>" type="text" name="referral_city" class="form-control" id="referral_city" placeholder="Enter City">
</div>
</div> -->
<div class="row">
<div class="col-md-12">
<label for="email">Your E-mail:</label>
    <input value="<?php echo set_value('email'); ?>" type="email" name="email" class="form-control" id="email" placeholder="Enter valid e-mail*" required />
</div>
<div class="col-md-12">
<label for="email">Referral Country:</label>
    <input value="<?php echo set_value('referral_country'); ?>" type="text" name="referral_country" class="form-control" id="email" placeholder="Indicate country of citizenship or legal status*" required />
</div>
</div>
<div class="row">
<div class="col-md-12">
<label for="email">Your Phone:</label>
    <input value="<?php echo set_value('phone'); ?>" type="text" name="phone" class="form-control" id="phone" placeholder="Indicate phone, incl. country code">
</div>
<div class="col-md-12">
<label for="email">Referral E-mail:</label>
    <input value="<?php echo set_value('referral_email'); ?>" type="email" name="referral_email" class="form-control" id="referral_email" placeholder="Enter valid e-mail of the person you refer*" required />
</div>
</div>
<div class="row">
<div class="col-md-12">
<label for="email">Intrested in (type of enquiry):</label>
    <input value="<?php echo set_value('immigration_type'); ?>" type="text" name="immigration_type" class="form-control" id="immigration_type" placeholder="Type of immigration program or inquiry*" required />
</div>
<div class="col-md-12">
<label for="email">Comments:</label>
    <textarea  name="referral_phone" class="form-control" id="referral_phone" placeholder="Instructions/recommendations, etc."><?php echo set_value('comments'); ?></textarea>  
</div>
</div>
<div class="row">
<div class="col-md-6">
<label for="email">Captcha:</label>
<input type="text" name="captcha" id="captcha" placeholder="Captcha required*" class="form-control" maxlength="4" required>
    <span id="errmsg"></span>
<!-- <div class="g-recaptcha" data-sitekey="6Le5mVkUAAAAAK141Xf876Kqka_gJBOktOluO-yM"></div>
 --><!-- <input class="form-control" id="num1" name="num1" readonly="readonly" class="sum" value="<?php echo rand(1,4) ?>" /> + 
    <input class="form-control" id="num2" name="num2" readonly="readonly" class="sum" value="<?php echo rand(5,9) ?>" /> =
    <input class="form-control" type="text" name="captcha" id="captcha" class="captcha" maxlength="2" />
    <span class="form-control" id="spambot">(Are you human, or spambot?)</span> -->
</div>
<div class="col-md-6">
    <div class="placing">
        <input type="text" value="<?=$rand?>" id="confirm_captcha" name="confirm_captcha" name readonly="readonly" class="captcha">        
        <img class="refresh" src="http://immcan.mobilytedev.com/uploads/media/refresh.png" onclick="captch()">
    </div>
</div>

    </div>
    <div class="row">
        <button class="sub-button" type="submit">Refer now!</button>
        <!-- <a href="#" class="sub-button">Submit</a> -->
    </div>
</div>
</form>   
    </div>
<!--End Form-->
<hr>
        <?=get_config_item('referral_content_page')?>
        <div class="affiliate-qa">
            <div class="row qa">
                <label class="col-md-9">Are you a hard-working person who wants to earn extra income?</label>
                <div class='col-md-3'>
                    <select class="">
                      <option>Yes</option>
                      <option>No</option>
                    </select>
                </div>

            </div>
            <div class="row qa">
                <label class="col-md-9">Do you have excellent communication skills in English and your own language?</label>
                <div class='col-md-3'>
                    <select class="">
                      <option>Yes</option>
                      <option>No</option>
                    </select>
                </div>
            </div>
            <div class="row qa">
            <label class="col-md-9">Do you know people who want to immigrate, work or study in Canada or would like to sponsor their loved ones to Canada?</label>
            <div class='col-md-3'>
                <select class="">
                 <option>Yes</option>
                 <option>No</option>
                </select>
            </div>
        </div>
            <div class="row qa">
              <label class="col-md-9">Do you have good business connections / know people who would like to invest or buy or start a business in Canada?</label>
                <div class='col-md-3'>
                    <select class="">
                      <option>Yes</option>
                      <option>No</option>
                    </select>
                </div>

            </div>

            <div class="row qa">
             <label class="col-md-9">Do you have a good network of contacts or strong connections in your country or ethnic or business community?</label>
                <div class='col-md-3'>
                    <select class="">
                      <option>Yes</option>
                      <option>No</option>
                    </select>
                </div>
            </div>

            <div class="row">
             <label class="col-md-7"></label>
                <div class='col-md-5 text-right'>
                    <a href="<?=base_url('website/referral_affiliates')?>"><button type="submit" class="btn btn-success">Become an Affiliate!</button></a>
                </div>
            </div>

        </div>
    </div>


  </div>
</div>
</section>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
/*$(document).ready(function(){
    $nums1 = $("#num1").val();
    $nums2 = $("#num2").val();
    $nums3 = parseInt($nums1) + parseInt($nums2);
    $(".sub-button").click(function () {
    //alert($nums3);
    if($nums3 == parseInt($nums1) + parseInt($nums2)){
        alert($nums3+"success");
    }else{
        alert("Check the value");
    }

    });
    
});*/
$('#sidebar_filter_city li').click(function(){   
    $('#sidebar_filter_areas').dropdown('toggle');
});

</script>
<script type="text/javascript">
//Javascript Referesh Random String
function captch() {
    var x = document.getElementById("confirm_captcha")
    x.value = Math.floor((Math.random() * 10000) + 1);
}
</script>
<script type="text/javascript">
    $(document).ready(function () {
  //called when key is pressed in textbox
  $("#captcha").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>