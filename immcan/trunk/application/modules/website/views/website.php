
<!--================Slider Area =================-->
        <section class="main_slider_area">
            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                <ul>

                    <?php 
                   /* echo "<pre>"; print_r($sliders); echo "</pre>"; die;*/
                    $count = 0;

                    foreach ($sliders as $slides) { /*if ($count == 0) {*/ ?>
                        
                        <li data-index="rs-294<?php echo $count; ?><?//=$count?>" data-transition="slidevertical" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                            <img src="<?php echo base_url() ?>/uploads/settings/<?= $slides->featured_image ?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                            <!-- LAYER NR. 1 -->
                            <div class="slider_text_box">
                                <div class="tp-caption tp-resizeme secand_text"
                                    data-x="['center','center','center','center','center','center']"
                                    data-hoffset="['0','80','80','0']"
                                    data-y="['middle','middle','middle','middle']"
                                    data-voffset="['0','0','0','0','0']"
                                    data-fontsize="['48','48','48','28','28','22']"
                                    data-lineheight="['60','60','60','36','36','30']"
                                    data-width="100%"
                                    data-height="none"
                                    data-whitespace="normal"
                                    data-type="text"
                                    data-responsive_offset="on"
                                    data-transform_idle="o:1;"
                                    data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                    data-textAlign="['center','center','center','center','center','center']"

                                    style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:700;color:#fff;">
                                    <?= $slides->post_content ?>
                                    <!-- Canadian Immigration & Citizenship<br />We’re experts in managing the details -->
                                </div>

                                <div class="tp-caption tp-resizeme slider_button"
                                    data-x="['center','center','center','center','center','center']"
                                    data-hoffset="['0','0','0','0']"
                                    data-y="['middle','middle','middle','middle']"
                                    data-voffset="['130','130','130','100','100','100']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-type="text"
                                    data-responsive_offset="on"
                                    data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                    data-textAlign="['center','center','center','center','center','center']">
                                    <a class="tp_btn" href="<?=base_url('get-started')?>" style="transition: none; text-align: inherit; line-height: 42px;border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;">Get Started</a>
                                  <a class="bg_btn" href="<?=base_url('consultant-booking')?>">Book Consultation</a>
                                  <a class="tp_btn" href="<?=base_url('contact/104')?>" style="transition: none; text-align: inherit; line-height: 42px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;">Contact Us</a>
                                </div>
                            </div>
                        </li>

                    <?php $count++; } ?>
                </ul>
            </div>
        </section>
        <!--================End Slider Area =================-->
        
		<?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>

        <!--================Creative Feature Area =================-->
        <section class="creative_feature_area">
            <div class="container">
                <?php $our_qualify_status = get_config_item('qualify_status');
                if($our_qualify_status == 1){

                 ?>
                <div class="c_feature_box">
				 <div class="top-cta" style="background: #232d37;
    padding: 20px;
    margin-top: -45px;
    color: white;
    margin-bottom: 20px;"> 
				 <div class="row">
                      <div class="col-lg-6">
                            <div class="c_box_item2">
                              <h3 style="padding:15px 10px; text-align: center;"><?=get_config_item('qualify_title')?></h3>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="c_box_item2" style=" text-align: center; padding: 5px 35px 0; width:100%;">
                            <a class="bg_btn box"  href="https://client.canadaconsultportal.ca/Login/Login?siteId=CCP2018135&qlauncher=true">Find Out Now</a>
                            </div>
                        </div>
                        </div>
                    </div>
                        
                    <div class="row">
                    <?php 
                    $counter = 0;
                    foreach ($qualify as $qualifi) { ?>
                        <div class="col-lg-4">
                            <div class="c_box_item">
                                <a href="<?php if($counter == 0) { echo "https://client.canadaconsultportal.ca/Login/Login?siteId=CCP2018135&qlauncher=true"; } if($counter == 1) { echo "#"; } if($counter == 2) { echo "#"; } ?>"><h4><i class="fa <?= $qualifi->meta_value ?>" aria-hidden="true"></i> <?= $qualifi->post_title ?></h4></a>
                                <p><?= $qualifi->post_content ?> </p>
                            </div>
                        </div>
                    <?php  $counter++;}
                    
                     ?>
                        <!-- <div class="col-lg-4">
                            <div class="c_box_item">
                                <a href="#"><h4><i class="fa fa-clock-o" aria-hidden="true"></i> Expertise</h4></a>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="c_box_item">
                                <a href="#"><h4><i class="fa fa-diamond" aria-hidden="true"></i> Quality</h4></a>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                            </div>
                        </div> -->
                    </div>
                </div>

                <?php } ?>


                 <?php $our_digital_agency_status = get_config_item('digital_agency_status');
                if($our_digital_agency_status == 1){

                 ?>
                <div class="digital_feature p_100">
                        
                    <div class="row">
                    <?php foreach ($agency as $note) { ?>
                        <div class="col-lg-6">
                            <div class="d_feature_text">
                                <div class="main_title">
                                    <!-- <h2>We Are A Creative <br /> <?= $note->post_title ?></h2> -->
                                    <h2><?= $note->post_title ?></h2>
                                </div>
                                <p>
                                    <?= $note->post_content ?>

                                    <?php
                                        $postid = $note->id;
                                        $pagelabel = get_page_metadata( $postid, 'agency_button_label' );
                                        $pagelink = get_page_metadata( $postid, 'agency_button_link' ); 
                                    ?>
                                </p>
                                <!-- <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia dese mollit anim id est laborum. Sed ut perspiciatis unde omnis iste.</p> -->
                                <a class="read_btn" href="<?php echo base_url($pagelink) ?>"><?= $pagelabel ?></a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="d_feature_img">
                                <img src="<?php echo base_url() ?>/<?= $note->featured_image ?>" alt="">
                            </div>
                        </div>
                   <?php  } ?>
                    </div>
                </div>

                <?php } ?>
            </div>
        </section>
        <!--================End Creative Feature Area =================-->

        <!--================Industries Area =================-->
        <?php $our_serve_industries_status = get_config_item('serve_industries_status');
                if($our_serve_industries_status == 1){

         ?>
        <section class="industries_area">
            <div class="left_indus">
                <div class="indus_img">
                <!--  <iframe width="560" height="315" src="https://www.youtube.com/embed/23bAomrDDBk?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
                <?=get_config_item('serve_industries_youtube_iframe')?>
                </div>
            </div>
            <div class="right_indus">
                <div class="indus_text">
                    <div class="main_title">
                        <h2><?=get_config_item('serve_industries_title')?></h2>
                        <p><?=get_config_item('serve_industries_content')?></p>
                    </div>
                    
                </div>
            </div>
        </section>
        <?php } ?>
        <!--================End Industries Area =================-->

        <!--================Our Service Area =================-->
        
        <?php $our_services_status = get_config_item('front_page_services_section_status');

            if ($our_services_status == 1) {
        ?>
                    <section class="service_area">
                        <div class="container">
                            <div class="center_title">
                                <h2><?=get_config_item('main_title')?></h2>
                                <p><?=get_config_item('tagline')?></p>
                            </div>
                            <div class="row service_item_inner">
                        
                                <?php if (!empty($services)) {
                                    
                                    $counter = 0;
                                    
                                    foreach ($services as $service) { ?>
                                        

                                        <div class="col-lg-4">
                                            <div class="service_item">
                                                <i class="ti-<?=get_page_metadata( $service->id, 'services_icon' )?>"></i>
                                                <h4><?=$service->post_title?></h4>
                                                <p><?=$service->post_content?></p>
                                                
                                                <a data-toggle="modal" data-target=".bd-example-modal-lg-read-more-<?=$counter?>" class="tp_btn test" style="transition: none; text-align: inherit; line-height: 40px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px; cursor: pointer;">READ MORE</a>
                                            </div>

                                        </div>

                                <?php $counter++; } } ?>

                            </div>
                        </div>
                    </section>

                <?php
            }
        ?>

        <?php if (!empty($services)) {
            
            //pr($services);
            $counter = 0;
            
            foreach ($services as $service) {  ?>

	        <div class="modal fade bd-example-modal-lg-read-more-<?=$counter?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	          <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	              <div class="modal-header">
	                <h5 class="modal-title" id="exampleModalLongTitle"><?=$service->post_title?></h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  <span aria-hidden="true">&times;</span>
	                </button>
	              </div>
	                
	              <div class="modal-body">

                    <p><?=$service->post_excerpt?></p>
	                 
                    <?php 
                        $read_more_button_label = get_page_metadata( $service->id, 'read_more_button_label' ); 
                        $read_more_button_link = get_page_metadata( $service->id, 'read_more_button_link' ); 
                    
                        if ($read_more_button_label) { ?>

                            <a href="<?=$read_more_button_link?>">
                                <button class="bkg-red-light bkg-hover-red-light color-white color-hover-white"><?=$read_more_button_label?></button>
                            </a>
                    
                    <?php } ?>

	              </div>
	            </div>
	          </div>
	        </div>
        
        <?php  $counter++; } } ?>

        <!--================End Our Service Area =================-->

        <!--================Testimonials Area =================-->
        <?php $our_canada_fun_facts_status = get_config_item('canada_fun_facts_status');
                if($our_canada_fun_facts_status == 1){

         ?>
        <section class="testimonials_area p_100">
            <div class="container">
                <div class="testimonials_slider owl-carousel">
                   <!--- Adding More Tesimonials -->

                    <?php foreach ($testimonial as $tests) { ?>
                       
                    <div class="item">
                        <div class="media">
                            <img class="d-flex rounded-circle" src="<?php echo base_url() ?>/uploads/settings/<?= $tests->featured_image ?>" style="width: 170px !important; height: 170px;" alt="">
                            <div class="media-body">
                                <img src="<?php echo base_url('assets') ?>/immcan/img/dotted-icon.png" alt="">
                                <p><?= $tests->post_content ?></p>
                                
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </section>
        <?php } ?>
        <!--================End Testimonials Area =================-->

        <!--================Project Area =================-->
        <?php $our_project_status = get_config_item('project_status');

            if ($our_project_status == 1) {
        ?>
        <section class="project_area">
            <div class="container">
                <div class="project_inner">
                    <div class="center_title">
                        <h2><?=get_config_item('project_title')?> </h2>
                        <p><?=get_config_item('project_tagline')?></p>
                    </div>
                    <a class="tp_btn" href="<?php echo base_url('get-started') ?>">GET STARTED</a>
                    <!-- <a class="tp_btn" href="<?php echo base_url('contact/104') ?>">CONTACT US</a> -->
                </div>
            </div>
        </section>
        <?php } ?>
        <!--================End Project Area =================-->

        <!--================Latest News Area =================-->
        <?php $our_news_check_status = get_config_item('news_check_status');

            if ($our_news_check_status == 1) {
        ?>
        <section class="latest_news_area p_100">
            <div class="container">
                <div class="b_center_title">
                    <h2><?=get_config_item('news_title')?></h2>
                    <p><?=get_config_item('news_tagline')?></p>
                </div>
                <?php //echo "<pre>"; print_r($news); echo "</pre>"; ?>

                <div class="l_news_inner">
                    <div class="row">

                        <?php $counter = 0; ?>

                        <?php foreach ($news as $latest_news) { ?>
                            
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url() ?>/uploads/settings/<?= $latest_news->featured_image ?>" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4><?= $latest_news->post_title ?></h4></a>
                                    <p><?= $latest_news->post_content ?></p>

                                  <!--   <a class="more_btn latest-news-read-more-" href="#">Learn More</a> -->

                                    <a data-toggle="modal" data-target=".latest-news-read-more-<?=$counter?>" class="tp_btn test" style="transition: none; text-align: center; line-height: 40px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px; cursor: pointer;">LEARN MORE</a>

                                </div>
                            </div>
                        </div>
                        <?php $counter++; } ?>
                        

        <?php if (!empty($news)) {
            
            //pr($services);
            $counter = 0;
            
            foreach ($news as $latest_news1) {  ?>

	        <div class="modal fade latest-news-read-more-<?=$counter?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	          <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	              <div class="modal-header">
	                <h5 class="modal-title" id="exampleModalLongTitle"><?=$latest_news1->post_title?></h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  <span aria-hidden="true">&times;</span>
	                </button>
	              </div>
	                
	              <div class="modal-body">

                    <p><?=$latest_news1->post_excerpt ?></p>

                    <?php 
                        $read_more_button_label = get_page_metadata( $latest_news1->id, 'read_more_button_label' ); 
                        $read_more_button_link = get_page_metadata( $latest_news1->id, 'read_more_button_link' ); 
                    
                        if ($read_more_button_label) { ?>

                            <a href="<?=$read_more_button_link?>">
                                <button class="bkg-red-light bkg-hover-red-light color-white color-hover-white"><?=$read_more_button_label?></button>
                            </a>
                    
                    <?php } ?>

	              </div>
	            </div>
	          </div>
	        </div>
        
        <?php  $counter++; } } ?>


                        <!--<div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url('assets') ?>/immcan/img/blog/l-news/l-news-2.jpg" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>Simple, Fast And Fun</h4></a>
                                    <p>The Fancy that recognize the talent and effort of the best web designers, develop-ers and agencies in the world.</p>
                                    <a class="more_btn" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="<?php echo base_url('assets') ?>/immcan/img/blog/l-news/l-news-3.jpg" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>Device Friendly</h4></a>
                                    <p>The Fancy that recognize the talent and effort of the best web designers, develop-ers and agencies in the world.</p>
                                    <a class="more_btn" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>-->

                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        <!--================End Latest News Area =================-->