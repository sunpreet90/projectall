<style>
 .referal {
    padding: 96px 0 50px 0;
    margin-top: 20px;
    background-color: #dadada;
}
.sub-button {
    margin-left: 20px;
    margin-top: 10px;
    padding: 6px 12px;
    background-color: #3578b6;
    border-radius: 6px;
    color: #fff;
}
.referal h3 {
    color: #3577b6;
    padding-bottom: 10px;
    font-weight: 400;
}
.referal p {
    font-size: 16px;
    color: #7d7b7b;
    padding: 0 0 20px 0;
}
.main_menu_area {
    position: absolute;
    width: 100%;
    top: 0px;
    left: 0px;
    z-index: 30;
    padding: 0px 75px;
    border-bottom:0px !important;
    background-color: #bcafc9;
}
.referal-inner {
    max-width: 70%;
    margin: 0 auto;
    background-color: #fff;
    border-radius: 6px;
    padding: 30px;
    border-bottom: 18px solid#3577b6;
}
.referal-inner label {
    font-weight: bold;
    color: #595959;
    font-size: 14px;
    margin-top: 10px;
}
</style>

<section class="referal">

    <div class="referal-inner">
        <form name="" method="post" action="<?=base_url('website/add_inquiry')?>" enctype='multipart/form-data'>
    <div class="container">
<div class="row">
<h3>International Partner Agent Enquiry</h3>
</div> 
<div class="row">
<P>Please complete the following form and submit it.</P>
</div>


<?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?> 

<div class="row">
<div class="col-md-6">
<label for="email">Your Name:</label>
    <input type="text" class="form-control" value="<?php echo set_value('partner_name'); ?>" name="partner_name">
</div>
<div class="col-md-6">
<label for="email">Your Email address:</label>
    <input type="text" class="form-control" value="<?php echo set_value('partner_email'); ?>" name="partner_email">
</div>
</div>
<div class="row">
<div class="col-md-6">
<label for="email">Your Address:</label>
    <input type="text" class="form-control" value="<?php echo set_value('partner_address'); ?>"  name="partner_address"">
</div>
<div class="col-md-6">
<label for="email">Phone Number:</label>
    <input class="form-control" value="<?php echo set_value('partner_phone'); ?>" type="text" name="partner_phone">
</div>
</div>
<div class="row">
<div class="col-md-6">
<label for="email">Education:</label>
    <input class="form-control" value="<?php echo set_value('partner_education'); ?>" type="text" name="partner_education">
</div>
<div class="col-md-6">
<label for="email">Profession:</label>
    <input class="form-control" value="<?php echo set_value('partner_profession'); ?>" type="text" name="partner_profession">
</div>
</div>
<div class="row">
<div class="col-md-6">
<label for="email">Have you worked in Immigration matters before?</label><br/>
    <input type="radio" name="worked" value="yes"  checked> Yes
    <input type="radio" name="worked" value="no" > No
</div>
<div class="col-md-6">
<label for="email">How did you hear about us:</label>
    <input class="form-control" value="<?php echo set_value('hear_from'); ?>" type="text" name="hear_from">
</div>
</div>
<div class="row">
<div class="col-md-6">
<label for="email">If Yes, Please Provide Details:</label>
    <input type="text" class="form-control" name="yes_details" placeholder="Enter enquiry type">
</div>
<div class="col-md-6">
<label for="email">Captcha:</label>
<div class="g-recaptcha" data-sitekey="6Le5mVkUAAAAAK141Xf876Kqka_gJBOktOluO-yM"></div>
    <!-- <input id="num1" name="num1" readonly="readonly" class="sum" value="<?php echo rand(1,4) ?>" /> + 
        <input id="num2" name="num2" readonly="readonly" class="sum" value="<?php echo rand(5,9) ?>" /> =
        <input type="text" name="captcha" id="captcha" class="captcha" maxlength="2" />
        <span id="spambot">(Are you human, or spambot?)</span> -->
</div>
</div>
<div class="row">
<div class="col-md-12">

</div>
    </div>
    <div class="row">
        <button class="sub-button" type="submit">Add Partner Info</button>
        <!-- <a href="#" class="sub-button">Submit</a> -->
    </div>
</div>
</form>
</div>
</section>

<script src='https://www.google.com/recaptcha/api.js'></script>
