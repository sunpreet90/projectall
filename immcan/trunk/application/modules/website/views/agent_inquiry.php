<?php
//Random Number Generation
$rand=substr(rand(),0,4); //only show 4 numbers
?>
<style type="text/css">
.captcha
{
width:60px; 
background-image:url('../uploads/media/cat.png'); 
font-size:20px; 
border: 1px solid;
}
.color
{
    color:#FF0000;
}
.refresh {
   height: 35px;
   width: 35px;
}
.placing {
    margin: 45px auto 0;
}
.fade {
    opacity: 1;
}
#errmsg
{
color: red;
}
</style>
<style>
 .referal {
    padding: 50px 0 50px 0;
    /*margin-top: 20px;*/
    background-color: #dadada;
}
.sub-button {
    margin-left: 20px;
    margin-top: 10px;
    padding: 6px 12px;
    background-color: #3578b6;
    border-radius: 6px;
    color: #fff;
}
.referal h3 {
    color: #3577b6;
    padding-bottom: 10px;
    font-weight: 400;
}
.referal p {
    font-size: 16px;
    color: #7d7b7b;
    padding: 0 0 20px 0;
}
.main_menu_area {
    position: absolute;
    width: 100%;
    top: 0px;
    left: 0px;
    z-index: 30;
    padding: 0px 75px;
    border-bottom:0px !important;
    background-color: #bcafc9;
}
.referal-inner {
    /*max-width: 50%;*/
    margin: 0 auto;
    background-color: #fff;
    border-radius: 6px;
    padding: 30px;
    border-bottom: 18px solid#3577b6;
}
.referal-inner img {
    max-width: 100%;
    height: auto;
}

.referal-inner label {
    font-weight: bold;
    color: #595959;
    font-size: 14px;
    margin-top: 10px;
}

.numberCircle {
    border-radius: 50%;
    width: 20px;
    height: 20px;
    padding: 3px;
    background: #f05a22;
    border: 2px solid #666;
    color: #fff;
    text-align: center;
    font: Arial, sans-serif;
}

.project_area {
   /* background: url("https://refer.ringcentral.com/RINGCENTRAL/_Asset/HERO2.png?version=1") no-repeat center center;*/
    background-size: cover;
    position: relative;
    z-index: 3;
    padding: 115px 0px;
    margin-top: 3%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;    
}
</style>

<!--================Slider Area =================-->
<section class="project_area" style="background-image:url('<?php echo base_url() ?><?=get_config_item('banner_header_image'); ?>'); background-position: 50% 63px;">
            <div class="container">
                <div class="project_inner">
                    <div class="center_title">
                        <h2></h2>                        
                    </div>                    
                </div>
            </div>
</section>

        <!--================End Slider Area =================-->
<section class="referal">
   <div class="container">  
     <h1 style="text-align: center;"><?=get_config_item('agent_inquiry_title')?></h1>     
       
    <div class="row" style="margin-top: 1%;">
        <div class="col-sm-6 referal-inner" style="">
            <?=get_config_item('agent_inquiry_content')?>
           <!-- <div style="color: #2F2F2F; font-size: 30px; font-weight: 300; font-family:'proxima-nova'; padding-top:0px;padding-bottom: 10px;line-height:31px;">Agent Inquiry</div>
            <div style="color: #2F2F2F; font-size: 18px; font-weight: 300; font-family:'proxima-nova'; line-height: 26px;">
              <p> The Agent and Partner Program is similar to Referral Affiliate, but it involves a higher level of commitment and participation, such as ongoing advertisement and marketing efforts, client  education, preselection, coordination, ability to provide front- and back-end customer and office support, etc.</p>
              <p> We provide strategic guidance, direction, support, education and training to qualified agents and partners. It is not required that you have experience in immigration matters/process, or that you become directly involved in our operations, although such experience and background will be beneficial as they will open additional opportunities. <hr><strong>If you are able to consistently generate a solid stream of referrals and/or clients, we would love to hear from you!  You can potentially make up to 50% of our professional fees, which alone can generate very significant additional income opportunities for you. In addition, you can potentially make money from various additional / supplementary value-added services.</strong></p>
              <p>The table below provides some typical examples of commission we pay to our agents and partners. However, actual amounts and percentage you earn will be based on the number and type of clients you refer to us and the level of your interest, experience, participation, etc.</p>
              <p>Please complete the form below, and also send us a quick message and briefly explain what you can offer – describe type of clients / connections / infrastructure  and business setup you have, programs your contacts are most likely to be interested in (business immigration, family sponsorship, skilled worker, student, etc). We will review your information, and if we think there’s a potential for our cooperation, we will contact you with additional details.</p>
              <p>To your success!</p>
            </div>
            <div class="row" style="margin-top: 1%;">
                    <table style="width:100%">
                  <tr>
                    <th>Visa Type</th>
                    <th colspan="3">Number of Clients Referred over 12 Months</th>
                  </tr>
                  <tr>
                    <td> </td>
                    <td>1-10</td>
                    <td>11-20</td>
                    <td>21+</td>
                  </tr>
                   <tr>
                    <td>Temporary </td>
                    <td>10%</td>
                    <td>20%</td>
                    <td>30%</td>
                  </tr>
                   <tr>
                    <td>Permanent Resident</td>
                    <td>20%</td>
                    <td>30%</td>
                    <td>40%</td>
                  </tr>
                   <tr>
                    <td>Business</td>
                    <td>30%</td>
                    <td>40%</td>
                    <td>50%</td>
                  </tr>
                </table>
            </div> -->
        </div>
<!-- </div> -->
<!-- Form Section-->
<div class="col-sm-6 referal-inner" style="background-color:#F0F8FC;">
        <form name="referral" method="POST" action="<?=base_url()?>website/agent_inquiry" novalidate="novalidate"> 
         
                     <?php if($this->session->flashdata('success')): ?> 
                        <div class='alert alert-success alert-block fade in'>
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <h4>
                                <i class="icon-ok-sign">Success</i>
                                <p><?php echo $this->session->flashdata('success');?></p>
                            </h4>
                        </div>
                    <?php endif; ?>
                   <?php if ( strlen( validation_errors() ) > 0 ) : ?>
                        <div class='alert alert-block alert-danger fade in'>
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <h4>
                                <i class="icon-ok-sign">Errors</i>
                                <?php echo $this->session->flashdata('response_status');?>
                            </h4>
                            <p><?php echo validation_errors(); ?></p>
                        </div>
                    <?php endif; ?>       
                <div class="container">
            <div class="row">
            <h3>AGENT INQUIRY FORM</h3>
            </div> 
            <div class="row">
           <!--  <P>Please complete the following form and submit it.</P> -->
            </div>
            <div class="row">
            <div class="col-md-12">
            <label for="email">Your Name:</label>
                <input value="<?php echo set_value('name'); ?>" type="text" name="name" class="form-control" id="name" placeholder="Enter Name*" required>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
            <label for="email">Your E-mail:</label>
                <input value="<?php echo set_value('email'); ?>" type="email" name="email" class="form-control" id="email" placeholder="Enter valid e-mail*" required>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
            <label for="email">Your Phone:</label>
                <input value="<?php echo set_value('phone'); ?>" type="text" name="phone" class="form-control" id="phone" placeholder="Indicate phone, incl. country code" required>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
            <label for="email">Your Education/Profession/Position/Business:</label>
                <textarea type="textarea" name="education_profession" class="form-control" id="education_profession" placeholder="Please describe your education and profession" maxlength="140" rows="4"><?php echo set_value('education_profession'); ?></textarea>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
            <label for="email">Do You Have Experience with Immigration Matters?</label>
                <textarea type="textarea" name="immigration_experience" class="form-control" id="immigration_experience" placeholder="Describe specific immigration experience" maxlength="140" rows="4"><?php echo set_value('immigration_experience'); ?></textarea>
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
            <label for="email">Are You Able to Provide Advertisement, Marketing or Office Support?</label>
                <textarea type="text" name="any_support" class="form-control" id="any_support" placeholder="Please provide details" maxlength="140" rows="5"><?php echo set_value('any_support'); ?></textarea>
            </div>
            </div>
            <div class="row">
             <div class="col-md-12">
            <label for="email">Comments:</label>
               <textarea type="textarea" name="comments" class="form-control" id="comments" placeholder="What can you bring to the table? Describe type and number of potential clients, imm. programs, etc." maxlength="140" rows="5"><?php echo set_value('comments'); ?></textarea>
            </div> 
            </div>
           <div class="row">
<div class="col-md-6">
<label for="email">Captcha:</label>
<input type="text" name="captcha" id="captcha" placeholder="Captcha required" class="form-control" maxlength="4" required>
    <span id="errmsg"></span>
</div>
<div class="col-md-6">
    <div class="placing">
        <input type="text" value="<?=$rand?>" id="confirm_captcha" name="confirm_captcha" name readonly="readonly" class="captcha">        
        <img class="refresh" src="http://immcan.mobilytedev.com/uploads/media/refresh.png" onclick="captch()">
    </div>
</div>

    </div>
                <div class="row">
                    <input type="hidden" name="affiliates_form" value="1">
                    <button class="sub-button" type="submit">Let's talk! </button>
                    <!-- <a href="#" class="sub-button">Submit</a> -->
                </div>
            </div>
        
        </form>  


    </div>
  </div>
</div>
</section>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
/*$(document).ready(function(){
    $nums1 = $("#num1").val();
    $nums2 = $("#num2").val();
    $nums3 = parseInt($nums1) + parseInt($nums2);
    $(".sub-button").click(function () {
    //alert($nums3);
    if($nums3 == parseInt($nums1) + parseInt($nums2)){
        alert($nums3+"success");
    }else{
        alert("Check the value");
    }

    });
    
});*/
$('#sidebar_filter_city li').click(function(){   
    $('#sidebar_filter_areas').dropdown('toggle');
});

</script>
<script type="text/javascript">
//Javascript Referesh Random String
function captch() {
    var x = document.getElementById("confirm_captcha")
    x.value = Math.floor((Math.random() * 10000) + 1);
}
</script>
<script type="text/javascript">
    $(document).ready(function () {
  //called when key is pressed in textbox
  $("#captcha").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>