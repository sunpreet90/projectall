<!--================Slider Area =================-->
        <section id="service-page" class="main_slider_area polo">
            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                <ul>
                    <li data-index="rs-2946" data-transition="slidevertical" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url() ?><?=get_config_item('about_header_image')?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            

                            <!-- LAYER NR. 10 -->
                     

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption Agency-CloseBtn rev-btn "
                            id="slide-2971-layer-20"
                            data-x="['center','center','center','center']" data-hoffset="['510','389','270','199']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-298','-229','-163','-118']"
                            data-width="50"
                            data-height="none"
                            data-whitespace="nowrap"

                            data-type="button"
                            data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2971-layer-15","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2971-layer-19","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2971-layer-20","delay":""}]'
                            data-responsive_offset="on"
                            data-responsive="off"
                            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:45deg;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":500,"to":"o:1;","delay":"bytrigger","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"to":"auto:auto;","ease":"nothing"},{"frame":"hover","speed":"500","ease":"Power1.easeInOut","to":"o:1;sX:1.1;sY:1.1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);"}]'
                            data-textAlign="['center','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            data-lasttriggerstate="reset"
                            style="z-index: 15; min-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;font-size: 24px;"><i class="fa fa-close"></i> </div>
							
                            <div class="tp-caption tp-resizeme secand_text"
                                data-x="['center','center','center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['0','0','0','0','0']"
                                data-fontsize="['48','48','48','28','28','22']"
                                data-lineheight="['60','60','60','36','36','30']"
                                data-width="100%"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text"
                                data-responsive_offset="on"
                                data-transform_idle="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['center','center','center','center','center','center']"

                                style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:700;color:#fff;text-transform:uppercase;">
								<p><?=get_config_item('about_you_page_title')?></p>
								</div>

                            <div class="tp-caption tp-resizeme slider_button"
                                data-x="['center','center','center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['130','130','130','100','100','100']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="text"
                                data-responsive_offset="on"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['center','center','center','center','center','center']">
                                <p class="about_you" style="color:#fff;"><?=get_config_item('about_you_content')?></p>
								<!--a class="bg_btn" href="<?php echo base_url('consultant-booking') ?>">Book Consultation</a-->
								<!-- <a class="about_btn" href="#" style="transition: none; text-align: inherit; line-height: 42px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;">About Us</a> -->
                            </div>
                        </div>
                    </li>
                    
                </ul>
            </div>
        </section>
        <!--================End Slider Area =================-->
<section class="how-we-help-page">
       <div class="container-fluid">
            <div class="container">
                 <h2><?=get_config_item('about_help_title')?></h2>
                 <p><?=get_config_item('about_help_tagline')?> </p>
            </div>
            
      </div>


    </section>

  <section class="how-we-work">

        <?php if (!empty($about_you)) {
            
            $count = 1;

            foreach ($about_you as $about) { 

                 if ($count % 2 != 0) {
        ?>
        <div class="container-fluid work">
                    <div class="container">
                        <div class="row how-we-row">
                          
                            <div class="col-md-6 help">
                                <img src="<?=$about->featured_image?>" alt="team">
                            </div>

                            <div class="col-md-6 help">
                                <h2><?=$about->post_title?></h2>
                                <p><?=$about->post_content?></p>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
                } else { ?>


                    <div class="container-fluid">
                        <div class="container">
                            <div class="row how-we-row">
                                <div class="col-md-6">
                                    <h2><?=$about->post_title?></h2>
                                    <p><?=$about->post_content?></p>
                                </div>
                                <div class="col-md-6">
                                    <img src="<?=$about->featured_image?>" alt="team">
                                </div>
                            </div>
                        </div>
                    </div>
        <?php
                }

                $count++;
            }

        } ?>

            <!-- <div class="container-fluid work">
                <div class="container">
                    <div class="row how-we-row">
                      
                        <div class="col-md-6 help">
                            <img src="../assets/immcan/img/aboutu@1.jpg" alt="team">
                        </div>

                        <div class="col-md-6 help">
                            <h2>CANDOR</h2>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="container">
                    <div class="row how-we-row">
                        <div class="col-md-6">
                            <h2>COMPETANCE</h2>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            </p>
                        </div>
                        <div class="col-md-6">
                            <img src="../assets/immcan/img/aboutu@2.jpg" alt="team">
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid work">
                <div class="container">
                    <div class="row how-we-row">
                        <div class="col-md-6 help">
                            <img src="../assets/immcan/img/aboutu@3.jpg" alt="team">
                        </div>
                        <div class="col-md-6 help">
                            <h2>ETHICS</h2>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="container">
                    <div class="row how-we-row">
                         <div class="col-md-6">
                            <h2>PROFESSIONALISM</h2>
                            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            </p>
                        </div>

                        <div class="col-md-6">
                            <img src="../assets/immcan/img/aboutu@4.jpg" alt="team">
                        </div>
                    </div>
                </div>
            </div> -->
        
        </section> 

 <footer class="footer_background" style="background-image:url('<?php echo base_url() ?><?=get_config_item('about_you_footer_image'); ?>');"> 
   <!--  <footer class="footer_background" style="background-image: url(../assets/immcan/img/aboutyoufooter.jpg);"> -->

	<p><?=get_config_item('about_you_footer_title')?></p>
		 <!-- <a class="bg_btn" href="<?php echo base_url('consultant-booking') ?>">Book Consultation</a> -->
		<a class="about_btn" href="<?= base_url('get-started')?>" style="transition: none; text-align: inherit; line-height: 42px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;">Get Started</a>
</footer>























