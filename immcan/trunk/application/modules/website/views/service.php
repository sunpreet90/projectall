<!--================Slider Area =================-->
        <section id="service-page" class="main_slider_area">
            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                <ul>
                    <li data-index="rs-2946" data-transition="slidevertical" data-slotamount="1" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="1000"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Intro" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                        <img src="<?=base_url().get_config_item('services_image') ?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <!-- <img src="<?php echo base_url('assets') ?>/immcan/img/home-slider/slider-1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina> -->
                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            

                            <!-- LAYER NR. 10 -->
                     

                            <!-- LAYER NR. 11 -->
                            <div class="tp-caption Agency-CloseBtn rev-btn "
                            id="slide-2971-layer-20"
                            data-x="['center','center','center','center']" data-hoffset="['510','389','270','199']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-298','-229','-163','-118']"
                            data-width="50"
                            data-height="none"
                            data-whitespace="nowrap"

                            data-type="button"
                            data-actions='[{"event":"click","action":"stoplayer","layer":"slide-2971-layer-15","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2971-layer-19","delay":""},{"event":"click","action":"stoplayer","layer":"slide-2971-layer-20","delay":""}]'
                            data-responsive_offset="on"
                            data-responsive="off"
                            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:45deg;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":500,"to":"o:1;","delay":"bytrigger","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"to":"auto:auto;","ease":"nothing"},{"frame":"hover","speed":"500","ease":"Power1.easeInOut","to":"o:1;sX:1.1;sY:1.1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);"}]'
                            data-textAlign="['center','center','center','center']"
                            data-paddingtop="[0,0,0,0]"
                            data-paddingright="[0,0,0,0]"
                            data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            data-lasttriggerstate="reset"
                            style="z-index: 15; min-width: 50px; max-width: 50px; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;font-size: 24px;"><i class="fa fa-close"></i> </div>
							
                            <div class="tp-caption tp-resizeme secand_text"
                                data-x="['center','center','center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['0','0','0','0','0']"
                                data-fontsize="['48','48','48','28','28','22']"
                                data-lineheight="['60','60','60','36','36','30']"
                                data-width="100%"
                                data-height="none"
                                data-whitespace="normal"
                                data-type="text"
                                data-responsive_offset="on"
                                data-transform_idle="o:1;"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['center','center','center','center','center','center']"

                                style="z-index: 8;font-family:'Poppins', sans-serif;font-weight:700;color:#fff;text-transform:uppercase;">
								<p><?=get_config_item('services_title')?></p>
								<?=get_config_item('services_tagline')?></div>

                            <div class="tp-caption tp-resizeme slider_button"
                                data-x="['center','center','center','center','center','center']"
                                data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']"
                                data-voffset="['130','130','130','100','100','100']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-type="text"
                                data-responsive_offset="on"
                                data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                data-textAlign="['center','center','center','center','center','center']">
                                <p style="color:#fff;"><?=get_config_item('services_content')?></p>
								<!--a class="bg_btn" href="<?php echo base_url('consultant-booking') ?>">Book Consultation</a-->
								<a class="tp_btn" href="<?=get_config_item('services_menu_page_link')?>" style="transition: none; text-align: inherit; line-height: 42px; border-width: 2px; margin: 0px; padding: 0px; letter-spacing: 0px; font-weight: 600; font-size: 13px;"><?=get_config_item('services_menu_page_title')?></a>
                            </div>
                        </div>
                    </li>
                    
                </ul>
            </div>
        </section>
        <!--================End Slider Area =================-->
<div class="container-fluid">
	<div class="row">
	
    <?php   

    $count = 1;

    foreach ($services as $note) { ?>
        
        <div class="col-xs-12 col-sm-4 panel p-t-b-40">
            <div class="panel-cont">
                <div class="feature-text">
                    <span class="text-large font-alt-1">0<?=$count?>.</span>
                    <h3 class="text-uppercase color-grey"><?= $note->post_title ?></h3>
                    <p class="mb-mobile-30"><?= $note->post_content ?> </p>
                </div>
            </div>
        </div>
        


    <?php $count++; } ?>

		<!-- <div class="col-xs-12 col-sm-4 panel bkg-grey-ultralight p-t-b-40">
			<div class="panel-cont">
				<div class="feature-text">
					<span class="text-large font-alt-1">01.</span>
					<h3 class="text-uppercase color-grey">Get In Touch</h3>
					<p class="mb-mobile-30">The easiest way to start is by simply getting in touch and booking a one hour consultation to discuss YOUR specific circumstances and goals. We will thoroughly assess your situation and will let you know your options and how to best proceed. </p>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4 panel p-t-b-40">
			<div class="panel-cont">
				<div class="feature-text">
					<span class="text-large font-alt-1">01.</span>
					<h3 class="text-uppercase color-grey">Get In Touch</h3>
					<p class="mb-mobile-30">The easiest way to start is by simply getting in touch and booking a one hour consultation to discuss YOUR specific circumstances and goals. We will thoroughly assess your situation and will let you know your options and how to best proceed. </p>
				</div>
			</div>
		</div> -->
		
	</div>
</div>		
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-6 panel" style="background-color:#c0392b">
			<div class="panel-cont">
				
				<div class="color-white">
					<h2 class="mb-40"><?=get_config_item('user_title')?></h2>
					<p class="lead"><?=get_config_item('user_tagline')?></p>
				</div>
				<p class="color-white transparent-element"><?=get_config_item('user_content')?>  </p>
				<a href="<?=get_config_item('user_menu_link')?>" data-offset="-60" class="button scroll-link medium border-white bkg-hover-charcoal color-white color-hover-white text-uppercase"><?=get_config_item('user_menu_title')?></a>					   
				
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 panel" style="background-color: yellow; padding:0;">
			<div>
				<img class="img-responsive" src="<?= base_url().get_config_item('user_image') ?>">
			</div>
		</div>
	</div>
</div>
<div id="fullrange" class="container-fluid">
	<div class="row">
		
		<div class="container">
     <div id="accordion p-t-b-40">
	<h3 class="mb-50"><?=get_config_item('range_services_title')?></h3>
	<p class="p-t-b"><?=get_config_item('range_services_tagline')?></p>
    <?php if (!empty($range_services)) {
              $count = 0; 
              foreach ($range_services as $note) { ?>
      <div class="card">
        
        <div class="card-header">
            <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#menuone-<?=$count?>"><?= $note->post_title ?></a>
        </div>
        
        <div id="menuone-<?=$count?>" class="collapse">
          <div class="card-body">
            <?= $note->post_content ?>
          </div>
        </div>

      </div>
       <?php $count++; }
              }?>

      <!-- <div class="card">
        <div class="card-header">
  <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#menutwo">Temporary Residence</a>
        </div>
        <div id="menutwo" class="collapse">
          <div class="card-body">
			<strong>Work Permit and Extensions including:</strong>
			<br><br>Express Entry 
			<br>(Federal Skilled Worker, Canadian Experience Class, Federal Skilled Trades Class)
			<br><br>Provincial Nominations
		   <br><br>Spouse, Common-law and Conjugal Sponsorships
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
  <a class="card-link" data-toggle="collapse" data-parent="#accordion" href="#menuthree">Inadmissibility</a>
        </div>
        <div id="menuthree" class="collapse">
          <div class="card-body">
Taj Mahal is built on the banks of river Yamuna and is surrounded by a beautiful garden. Mughal Emperor Shah Jahan constructed it for the commemoration of his wife Mumtaz Mahal. The construction was started in 1631 and in 1643, the construction of main building was completed. The construction of the whole complex was completed in 1653. Mumtaz Mahal is buried in Taj Mahal.
          </div>
        </div>
      </div> -->

	<a href="<?=base_url('get-started')?>"> <button class="bkg-red-light bkg-hover-red-light color-white color-hover-white">
                              Help Me Immigrate!</button></a>
   </div>
</div>
		
	</div>
</div>