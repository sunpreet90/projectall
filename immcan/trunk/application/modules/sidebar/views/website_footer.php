 <footer>
      <div class="container">
        
        <div class="row">
          <div class="col-sm-8 margin-20">
            <ul class="list-inline social">
              <li>Connect with us on</li>
              <li><a href="https://twitter.com/login?lang=en"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://www.instagram.com/accounts/login/"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
          
          <div class="col-sm-4 text-right">
            <p><small>Copyright &copy; 2018 by Next Stop Canada Immigration Services. All rights reserved. <br>
	           
          </div>
        </div>
        
      </div>
    </footer>
    
    
    <!-- Javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script>window.jQuery || document.write('<script src="<?php echo base_url('assets') ?>/web/js/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?php echo base_url('assets') ?>/web/js/wow.min.js"></script>
    <!-- <script src="<?php echo base_url('assets') ?>/web/js/bootstrap.min.js"></script> -->
    <script src="<?php echo base_url('assets') ?>/web/js/main.js"></script>


    <!-- <script src="js/jquery-1.10.2.min.js"></script> -->
<script src="<?php echo base_url('assets') ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery.stepy.js"></script>
<script src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/modernizr.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery.nicescroll.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>

<script>
    /*=====STEPY WIZARD====*/
    $(function() {
        $('#default').stepy({
            backLabel: 'Previous',
            block: true,
            nextLabel: 'Next',
            titleClick: true,
            titleTarget: '.stepy-tab'
        });
    });
    /*=====STEPY WIZARD WITH VALIDATION====*/
    $(function() {
        $('#stepy_form').stepy({
            backLabel: 'Back',
            nextLabel: 'Next',
            errorImage: true,
            block: true,
            description: true,
            legend: false,
            titleClick: true,
            titleTarget: '#top_tabby',
            validate: true
        });
        $('#stepy_form').validate({
            errorPlacement: function(error, element) {
                $('#stepy_form div.stepy-error').append(error);
            },
            rules: {
                'name': 'required',
                'email': 'required'
            },
            messages: {
                'name': {
                    required: 'Name field is required!'
                },
                'email': {
                    required: 'Email field is requerid!'
                }
            }
        });
    });
</script>
   
    </body>
</html>