<!--footer section start-->
        <footer>
          <?php echo $curYear = date('Y');?> &copy; Mobilyte Solutions
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src='https://www.google.com/recaptcha/api.js'></script> <!-- Captcha JS -->
<script src="<?=base_url('assets') ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url('assets') ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?=base_url('assets') ?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=base_url('assets') ?>/js/bootstrap.min.js"></script>
<script src="<?=base_url('assets') ?>/js/modernizr.min.js"></script>
<script src="<?=base_url('assets') ?>/js/jquery.nicescroll.js"></script>
<script type="text/javascript" src="<?=base_url('assets') ?>/js/ckeditor/ckeditor.js"></script>


<!--common scripts for all pages-->
<script src="<?=base_url('assets') ?>/js/scripts.js"></script>

<!--form validation-->
<script type="text/javascript" src="<?=base_url('assets') ?>/js/jquery.validate.min.js"></script>
<script src="<?=base_url('assets') ?>/js/validation-init.js"></script>


<?php if (isset($include_js)) {

	if ($include_js == 'editor') { ?>
	
		<script type="text/javascript" src="<?=base_url('assets') ?>/js/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
		<script type="text/javascript" src="<?=base_url('assets') ?>/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

		<script>
		    jQuery(document).ready(function(){
		         $('.wysihtml5').wysihtml5();
		    });
		</script>
	
	<?php }
	# code...
} ?>

<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=base_url('assets') ?>/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?=base_url('assets') ?>/js/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/jquery.dataTables.min.js"></script>
<!--dynamic table initialization -->
<script type="text/javascript"> src="<?=base_url('assets') ?>/js/dynamic_table_init.js"></script>
 <script type="text/javascript"> src="<?php echo base_url('assets') ?>/immcan/js/custom.js"></script>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets') ?>/js/jquery.isotope.js"></script>


<script type="text/javascript">
    $(function() {
        var $container = $('#gallery');
        $container.isotope({
            itemSelector: '.item',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        // filter items when filter link is clicked
        $('#filters a').click(function() {
            var selector = $(this).attr('data-filter');
            $container.isotope({filter: selector});
            return false;
        });
    });
</script>    


<!--gritter script-->
<script type="text/javascript" src="<?= base_url('assets') ?>/js/gritter/js/jquery.gritter.js"></script>
<script src="<?= base_url('assets') ?>/js/gritter/js/gritter-init.js" type="text/javascript"></script>        


<script type="text/javascript">
    
    var Gritter = function () {

        <?php if (isset($flash_status)) { ?>

                $.gritter.add({
                    
                    // (string | mandatory) the heading of the notification
                    title: '<?=$flash_title?>',
                    // (string | mandatory) the text inside the notification
                    text: '<?=$flash_message?>'
                });

        <?php } ?>

    }();

</script>


</body>
</html>
