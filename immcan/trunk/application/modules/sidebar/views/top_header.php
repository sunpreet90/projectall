 <link rel="stylesheet" href="<?=base_url()?>assets/js/toastr/toastr.css" type="text/css" cache="false" />
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <div class="header-section">

            <!--toggle button start-->
            <a class="toggle-btn"><i class="fa fa-bars"></i></a>
            <!--toggle button end-->

            <!--search start-->
           <!--  <form class="searchform" action="index.html" method="post">
                <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
            </form> -->
            <!--search end-->

            <!--notification menu start -->
            <div class="menu-right">
                <ul class="notification-menu">
                    <li>
                        <?php $data =  get_messages(); ?>
                            
                        <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge"> <?php echo $data['msgCount']; ?> </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title">You have <?php echo $data['msgCount']; ?> Mails </h5>
                            <ul class="dropdown-list normal-list">
                                
                                <?php 

                                if (!empty($data['messages'])) {
                                    # code...

                                    foreach($data['messages'] as $message)
                                    {
                                        ?>

                                        <li class="new">
                                            <a href="<?=base_url('admin/message')?>/view/<?= $message['id'] ?>">
                                                <span class="thumb"><i class="fa fa-envelope-o"></i></span>
                                                <span class="desc">
                                                  <span class="name"><?= $message['first_name']." ".$message['last_name'] ?> <span class="badge badge-success">new</span></span>
                                                  <span class="msg"><?=$message['message']?></span>
                                                </span>
                                            </a>
                                        </li>

                                        <?php
                                    } 
                                }

                                ?>

                                <li class="new"><a href="<?=base_url('admin/message')?>">Read All Mails</a></li>
                            </ul>
                        </div>
                    </li>                    
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="badge"><?=$notiCount;?></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-head pull-right">
                            <h5 class="title">Notifications</h5>
                            <ul class="dropdown-list normal-list">
                            <?php
                            //echo "<pre>"; print_r($notifications); echo "</pre>";
                            //get all notifications
                            foreach($notifications AS $get_notifications)
                            {
                            ?>
                                
                                <li class="new">
                                    <a href="<?= base_url('admin/notification')?>/view/<?= $get_notifications['id'] ?>">
                                        <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                        <span class="name"><?php echo $get_notifications['message']; ?></span>
                                        <em class="small">34 mins</em>
                                    </a>
                                </li>
                            <?php } ?>
                                
                                <li class="new"><a href="<?=base_url('admin/notification')?>">See All Notifications</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                             <img src="<?=base_url('assets') ?>/images/photos/demo.jpg" alt="" />
                            <?//=$username?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <li><a href="<?=base_url('profile')?>"><i class="fa fa-user"></i>  Profile</a></li>
                            <li><a href="<?=base_url('profile/settings')?>"><i class="fa fa-cog"></i>  Settings</a></li>
                            <li><a href="<?=base_url('auth/logout')?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!--notification menu end -->

        </div>
        <!-- header section end