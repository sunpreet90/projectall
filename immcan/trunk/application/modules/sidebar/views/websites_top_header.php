<!DOCTYPE html>
<?php $page_url = current_url(); ?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon"/>
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?=get_config_item('site_title')?></title>

        <!-- Icon css link -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/immcan/css/jquery.datetimepicker.css"/>
        <link href="<?php echo base_url('assets') ?>/immcan/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url('assets') ?>/immcan/vendors/elegant-icon/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets') ?>/immcan/vendors/themify-icon/themify-icons.css" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets') ?>/immcan/css/bootstrap.min.css" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="<?php echo base_url('assets') ?>/immcan/vendors/revolution/css/settings.css" rel="stylesheet">
        <link href="<?php echo base_url('assets') ?>/immcan/vendors/revolution/css/layers.css" rel="stylesheet">
        <link href="<?php echo base_url('assets') ?>/immcan/vendors/revolution/css/navigation.css" rel="stylesheet">
        <link href="<?php echo base_url('assets') ?>/immcan/vendors/animate-css/animate.css" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="<?php echo base_url('assets') ?>/immcan/vendors/owl-carousel/owl.carousel.min.css" rel="stylesheet">

        <link href="<?php echo base_url('assets') ?>/immcan/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url('assets') ?>/immcan/css/responsive.css" rel="stylesheet">
        <!-- <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
        <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet"> -->
        <link href="<?php echo base_url('assets') ?>/immcan/css/custom.css" rel="stylesheet">
        
        <script>var baseurl= '<?php echo base_url(); ?>'</script>
         <script src="<?php echo base_url('assets') ?>/immcan/js/jquery-3.2.1.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <?php

        if(!empty($active_menu)){
            $active = 'active';
        }else{
            $active = '';
            $active_menu = '';
        }


        if(!empty($actives_menu)){
            $active = 'nav-item';
        }else{
            $active = '';
            $actives_menu = '';
        }

        ?>
        <style type="text/css">
            .main_menu_area .navbar .navbar-nav li:nth-child(5) {
                margin-right: 20px;
            }

            .main_menu_area .navbar .navbar-nav li:nth-child(6) {
                margin-right: 20px;
            }
            .dropdown-item:focus, .dropdown-item:hover {
    color: #2bc0a4 !important; 
    text-decoration: none;
    /* background-color: transparent !important; */
}
        /*    .navbar-light .navbar-nav .nav-link:focus {
    color: #fff !important;
}*/

           /* .main_menu_area .navbar .navbar-nav li a:hover {
                color: #ccc !important;
            }*/
        </style>

        <!--================Search Area =================-->
        <section class="search_area">
            <div class="search_inner">
                <input type="text" placeholder="Enter Your Search...">
                <i class="ti-close"></i>
            </div>
        </section>
        <!--================End Search Area =================-->

        <!--================Header Menu Area =================-->
        <header class="main_menu_area">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><!-- <img src="<?php echo base_url('assets') ?>/immcan/img/logo.png" alt=""> -->
                
                    <?php if (get_config_item('website_logo') != '') { ?>
                        <img src="<?php echo site_url() ?>/uploads/settings/<?=get_config_item('website_logo')?>" style="width:200px;">
                    <?php } else { ?>

                        <img class="website_logo" src="<?php echo site_url() ?>">
                    <?php } ?>
                    </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <div class="navbar-collapse navbar-collapse" id="navbarSupportedContent" data-hover="dropdown" data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item dropdown <?=(base_url('website')==current_url())?'active':"" ?>">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo get_config_item('about_us_link') ?>"><?=get_config_item('about_menu_title')?></a>

                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li class="<?=(get_config_item('about_you_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('about_you_link') ?>"><?=get_config_item('about_you_title')?></a>
                                </li>
                              
                                <li class="<?=(get_config_item('about_us_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('about_us_link') ?>"><?=get_config_item('about_us_title')?></a>
                                </li>

                               <!--  <li class="<?=(get_config_item('our_team_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('our_team_link') ?>"><?=get_config_item('our_team_title')?></a>
                                </li> -->

                                <li class="<?=(get_config_item('why_us_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('why_us_link') ?>"><?=get_config_item('why_us_title')?></a>
                                </li>

                                <li class="<?=(get_config_item('can_help_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('can_help_link') ?>"><?=get_config_item('can_help_title')?></a>
                                </li>

                           </ul>
                        </li>

                        <li class="nav-item dropdown <?=(base_url('website')==current_url())?'active':"" ?>">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="<?php echo get_config_item('our_services_link') ?>"><?=get_config_item('services_menu_title')?></a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                                <li class="<?=(get_config_item('our_services_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('our_services_link') ?>"><?=get_config_item('our_services_title')?></a>
                                </li>

                                <li class="<?=(get_config_item('pricing_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('pricing_link') ?>"><?=get_config_item('pricing_title')?></a>
                                </li>

                                <!-- <li class="<?=(get_config_item('testimonials_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?php echo get_config_item('testimonials_link') ?>"><?=get_config_item('testimonials_title')?></a>
                                </li> -->

                                <!-- <li class="<?=(base_url('website/referrals')==current_url())?'active':"" ?>"><a class="dropdown-item" href="<?php echo base_url('website/referrals'); ?>">Referral</a></li>
                                <li class="<?=(base_url('website/inquiry')==current_url())?'active':"" ?>"><a class="dropdown-item" href="<?php echo base_url('website/inquiry'); ?>">Agent Inquiry</a></li> -->
                           </ul>
                        </li>

                        <li class="nav-item dropdown <?=(base_url('website')==current_url())?'active':"" ?>">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><?=get_config_item('main_menu_title')?></a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                                <li class="<?=(get_config_item('referral_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?=get_config_item('referral_link')?>"><?=get_config_item('referral_title')?></a></li>
                                
                                <li class="<?=(get_config_item('affiliates_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?=get_config_item('affiliates_link')?>"><?=get_config_item('affiliates_title')?></a></li>
                                    
                                <li class="<?=(get_config_item('agent_link')==current_url())?'active':"" ?>">
                                    <a class="dropdown-item" href="<?=get_config_item('agent_link')?>"><?=get_config_item('agent_title')?></a></li>
                            </ul>
                        </li>

                        <li class="<?=(get_config_item('faq_link')==current_url())?'active':"" ?>">
                        	<a class="nav-link" href="<?= get_config_item('faq_link') ?>" ><?=get_config_item('faq_title')?></a>
                        </li> 
                        <!-- <li class="<?=(base_url('website')==current_url())?'active':"" ?>">
                        	<a class="nav-link" href="https://client.canadaconsultportal.ca/Login/Login?siteId=CCP2018135&qlauncher=true" target="_blank">Start Assessment</a>
                        </li>  --> 
                        
                         
                        <?php if ($this->session->userdata('username') == TRUE) {?>
                        <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url('website/logout') ?>">Logout</a></li> -->
                        <?php } else{ ?>
                        <!-- <li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" data-target="#basicModal11">Login</a></li> -->
                        <?php } ?>
 						
                        
                        <li class="my-menu-button-li <?=(get_config_item('get_started_link')==current_url())?'active':"" ?>">
                        	<a class="my-menu-btn-a" href="<?=get_config_item('get_started_link')?>"><?=get_config_item('get_started_title')?></a>
                        </li> 
                        
                        <li class="my-menu-button-li <?=(get_config_item('book_now_link')==current_url())?'active':"" ?>">
                        	<a class="my-menu-btn-a" href="<?=get_config_item('book_now_link')?>"><?=get_config_item('book_now_title')?></a>
                        </li> 
                        
                        <li class="my-menu-button-li <?=(get_config_item('contact_us_link')==current_url())?'active':"" ?>">
                            <a class="my-menu-btn-a" href="<?=get_config_item('contact_us_link')?>"><?=get_config_item('contact_us_title')?></a>
                        	<!-- <a class="my-menu-btn-a" href="<?=base_url('contact-us')?>">Contact</a> -->
                        </li>

                    </ul>
                </div>
            </nav>
        </header>
        <!--================End Header Menu Area =================-->

		<!-- basic modal -->
		<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel">Register</h4>
		            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		              <span aria-hidden="true">&times;</span>
		            </button>
		        </div>
		        
		        <div class="custommessage"></div>

		        <form name="register_form" id="registerform" method="POST" action="<?php echo base_url('website/add_agent') ?>">
		            <div class="modal-body">
		                <div class="md-form">
		                    <i class="fa fa-user prefix grey-text"></i>
		                    <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
		                    <input type="text" name="name" id="orangeForm-name" class="form-control validate">
		                </div>
		                <div class="md-form">
		                    <i class="fa fa-envelope prefix grey-text"></i>
		                    <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
		                    <input type="email" name="email" id="orangeForm-email1" class="form-control validate">
		                </div>

		                <div class="md-form">
		                    <i class="fa fa-envelope prefix grey-text"></i>
		                    <label data-error="wrong" data-success="right" for="orangeForm-phone">Your Phone</label>
		                    <input type="text" name="phone" id="orangeForm-phone" class="form-control validate">
		                </div>

		                <div class="md-form">
		                    <i class="fa fa-lock prefix grey-text"></i>
		                    <label data-error="wrong" data-success="right" for="password">Your password</label>
		                    <input type="password" name="password" id="password" class="form-control validate">
		                </div>


		                <div class="md-form">
		                    <i class="fa fa-lock prefix grey-text"></i>
		                    <label data-error="wrong" data-success="right" for="confirm_password">Confirm password</label>
		                    <input type="password" name="confirm_password" id="confirm_password" class="form-control validate">
		                </div>


		                <div class="md-form text">
		                    <label data-error="wrong" data-success="right" for="confirm_password"></label>
		                    <input type="checkbox" name="terms" id="terms" class="validate"><a href="<?= base_url('pages/terms_and_conditions')?>" target="_blank">Terms and Conditions</a>
		                </div>

		            </div>

		          <div class="modal-footer">
		            <button type="submit" class="btn btn-default" data-dismiss="modal"  data-toggle="modal" data-target="#basicModal11"> Login </button>
		            <input type="submit" name="submit" class="btn btn-primary registerBTN" value="Submit"><!-- Save changes</button> -->
		          </div><br/>
		        </form>
		        </div>
		      </div>
		    </div>


			<!-- Login modal -->
			<div class="modal fade" id="basicModal11" tabindex="-1" role="dialog" aria-labelledby="basicModal11" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h4 class="modal-title" id="myModalLabel">Login</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="customloginmessage">
			      </div>
			      <form name="login_form" method="POST" id="loginform"  action="<?php echo base_url('website/add_agent') ?>">
			      <div class="modal-body">
			            <div class="md-form">
			                <i class="fa fa-envelope prefix grey-text"></i>
			                <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
			                <input type="email" name="email" id="orangeForm-email" class="form-control validate">
			            </div>
			            <div class="md-form">
			                <i class="fa fa-lock prefix grey-text"></i>
			                <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
			                <input type="password" name="password" id="orangeForm-pass"  class="form-control">
			            </div>
			        </div>
			        <!-- <button type="button" class="btn btn-default forgotpasswordbtn" data-dismiss="modal">Forgot Password</button> -->
			        <a href="#" class="nav-link" data-toggle="modal"  data-target="#basicModal22" data-dismiss="modal">Forget Password ?</a>
			       <!--  <button type="button" class="btn btn-default forgotpasswordbtn" data-toggle="modal" data-target="#basicModal22" data-dismiss="modal">Forgot Password</button> -->
			      <div class="modal-footer">
			        <button type="submit" class="btn btn-default" data-toggle="modal" data-dismiss="modal" data-target="#basicModal">SignUp</button>
			        <input type="submit" name="submit" class="btn btn-primary loginBtn" value="Login"><!-- Save changes</button> -->
			      </div>
			    </form>
			    </div>
			  </div>
			</div>

			<!-- Forgot Password modal -->
			<div class="modal fade" id="basicModal22" tabindex="-1" role="dialog" aria-labelledby="basicModal22" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="customloginmessage">
			      </div>
			      <form name="login_form" method="POST" id="forgotpasswordform"  action="<?php echo base_url().'website/doforget' ?>">
			      <div class="modal-body">
			            <div class="md-form">
			                <i class="fa fa-envelope prefix grey-text"></i>
			                <label data-error="wrong" data-success="right" for="orangeForm-emails">Your email</label>
			                <input type="email" name="email" id="orangeForm-emails" class="form-control validate">
			            </div>
			        </div>
			      <div class="modal-footer">
			        <button type="submit" class="btn btn-default" data-dismiss="modal"  data-toggle="modal" data-target="#basicModal11"> Login </button>
			        <input type="submit" name="submit" class="btn btn-primary forgotpassrequestbtn" value="Submit"><!-- Save changes</button> -->
			      </div>
			    </form>
			    </div>
			  </div>
			</div>

