<!DOCTYPE html>
<html lang="en">
<?php $page_url = current_url(); ?>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">

  <title>Next Stop Canada</title>

  <!--icheck-->
  <link href="<?=base_url('assets') ?>/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="<?=base_url('assets') ?>/js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="<?=base_url('assets') ?>/js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="<?=base_url('assets') ?>/js/iCheck/skins/square/blue.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets') ?>/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <!--dashboard calendar-->
  <link href="<?=base_url('assets') ?>/css/clndr.css" rel="stylesheet">

  <!--Morris Chart CSS -->
  <link rel="stylesheet" href="<?=base_url('assets') ?>/js/morris-chart/morris.css">

  <!--common-->
  <link href="<?=base_url('assets') ?>/css/style.css" rel="stylesheet">
  <link href="<?=base_url('assets') ?>/css/style-responsive.css" rel="stylesheet">
  <link href="<?=base_url('assets') ?>/immcan/css/admin-custom.css" rel="stylesheet">

  <!--dynamic table-->
  
  <link href="<?= base_url('assets') ?>/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="<?= base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?= base_url('assets') ?>/js/data-tables/DT_bootstrap.css" />
                
  <link href="<?= base_url('assets') ?>/css/style.css" rel="stylesheet">
  <link href="<?= base_url('assets') ?>/css/style-responsive.css" rel="stylesheet">

  <!-- Placed js at the end of the document so the pages load faster -->
    <script src="<?=base_url('assets') ?>/js/jquery-1.10.2.min.js"></script>
    <script src="<?=base_url('assets') ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="<?=base_url('assets') ?>/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?=base_url('assets') ?>/js/bootstrap.min.js"></script>
    <script src="<?=base_url('assets') ?>/js/modernizr.min.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?=base_url('assets') ?>/js/html5shiv.js"></script>
  <script src="<?=base_url('assets') ?>/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<?php

if(!empty($active_menu))
{
    $active = 'active';
}
else
{
    $active = '';
    $active_menu = '';
}

if(!empty($actives_menu))
{
    $active = 'menu-list';
}
else
{
    $active = '';
    $actives_menu = '';
} 

?>

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=site_url('welcome')?>">
              <img src="<?php echo base_url() ?>/assets/images/Next_Stop_Canada_Immigration.png" style="max-width: 150px; height: auto; background: white;"></a>
            <!-- <a href="<?=site_url()?>"><img src="http://immcan.mobilytedev.com/assets/immcan/img/logo.png"></a> -->
        </div>

        <div class="logo-icon text-center">
            <a href="<?= base_url('welcome') ?>">
                <img src="<?=base_url('assets') ?>/images/logo_icon.png" alt="">
            </a>
        </div>
        <!--logo and iconic logo end-->

        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?=base_url('assets') ?>/images/photos/user-avatar.png" class="media-object">
                    <div class="media-body">
                        <h4><a href="#">Hi,<?=$username?></a></h4>
                        <!-- <span>"Hello There..."</span> -->
                    </div>
                </div>

                <h5 class="left-nav-title">Account Information</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                  <li><a href="<?=base_url('profile')?>"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                  <li><a href="<?=base_url('profile/settings')?>"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                  <li><a href="<?=base_url('auth/logout')?>"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li class="">
                    <a href="<?php echo base_url() ?>" target="_blank"><i class="fa fa-laptop"></i> <span>Visit Website</span></a>
                </li>
                <li class="<?= (base_url('welcome')==current_url())?'active':""?>">
                    <a href="<?=base_url('welcome')?>"><i class="fa fa-home"></i> <span>Dashboard</span></a>
                </li>
                 <li class="<?= (base_url('admin/menu')==current_url())?'active':""?>">
                    <a href="<?=base_url('admin/menu')?>"><i class="fa fa-home"></i> <span>Main Menu</span></a>
                </li>

                <li class="menu-list <?= (base_url('admin/gallery')==current_url())?'active':""?>">
                    <a href=""><i class="fa fa-picture-o"></i> <span>Gallery</span></a>
                    <ul class="sub-menu-list">
                      <li class="<?=(base_url('admin/gallery')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/gallery')?>"> View All Gallery</a></li>
                        <li class="<?=(base_url('admin/gallery/add')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/gallery/add')?>"> Add Gallery</a></li>
                    </ul>
                </li>

                <li class="menu-list <?=(base_url('users')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-user"></i> <span>Users</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('users')==current_url())?'active':"" ?>"><a href="<?=base_url('users')?>"> All Users</a></li>
                    </ul>
                </li>

                <!-- <li class="menu-list <?=(base_url('users/consultation_types')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-user"></i> <span>Consultations</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('users/consultation_types')==current_url())?'active':"" ?>"><a href="<?=base_url('users/consultation_types')?>">Add Consultation</a></li>
                        <li class="<?=(base_url('users/all_types')==current_url())?'active':"" ?>"><a href="<?=base_url('users/all_types')?>">All Consultations</a></li>

                        <li class="<?=(base_url('consultant/booking_consultations')==current_url())?'active':"" ?>">
                        <a href="<?=base_url('consultant/booking_consultations')?>">All Bookings </a>
                      </li>
                    </ul>
                </li> -->

                <li class="menu-list <?=(base_url('consultant')==current_url() || base_url('consultant/add')==current_url() || base_url('users/consultation_types')==current_url() || base_url('users/all_types')==current_url()  )?'active':"" ?>">
                    <a href=""><i class="fa fa-tasks"></i> <span>Manage Consultants</span></a>
                    <ul class="sub-menu-list">
                      

                     <!--  <li class="<?=(base_url('users/consultation_types')==current_url())?'active':"" ?>">
                        <a href="<?=base_url('users/consultation_types')?>">Add Consultation</a>
                      </li> -->
                      <li class="<?=(base_url('consultant')==current_url())?'active':"" ?>">
                        <a href="<?=base_url('consultant')?>">All Consultants</a>
                      </li>
                      
                      <li class="<?=(base_url('users/all_types')==current_url())?'active':"" ?>">
                        <a href="<?=base_url('users/all_types')?>">All Consultation Type</a>
                      </li>

                      
                      
                      <!-- <li class="<?=(base_url('consultant/add')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('consultant/add')?>">Add Consultants</a>
                        </li> -->
                    </ul>
                </li>

                <!-- <li class="menu-list <?=(base_url('admin/gallery')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-picture-o"></i> <span>Media Gallery</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('admin/gallery')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/gallery')?>">Media Library</a></li>
                    </ul>
                </li> -->

                <li class="menu-list <?=(base_url('page')==current_url() || base_url('page/add')==current_url() || base_url('admin/our_services_page')==current_url() || base_url('admin/our_team')==current_url() || base_url('admin/pricing')==current_url()  )?'active':"" ?>">
                    <a href=""><i class="fa fa-file-text"></i> <span>Pages</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('page')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('page')?>"> View All Page</a>
                        </li>
                        
                        <li class="<?=(base_url('page/add')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('page/add')?>"> Page Add</a>
                        </li>

                        <li class="<?=(base_url('admin/our_services_page')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/our_service')?>">Our Services Page</a>
                       </li>

                       <li class="<?=(base_url('admin/our_team')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/our_team')?>">About Us</a>
                        </li>
                        <li class="<?=(base_url('admin/about_you')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/about_you')?>">About You</a>
                        </li>

                       <li class="<?=(base_url('admin/referral')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/referral')?>">Referral</a>
                        </li>
                        <li class="<?=(base_url('admin/affiliates')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/affiliates')?>">Referral Affiliates</a>
                        </li>
                        <li class="<?=(base_url('admin/agent_inquiry')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/agent_inquiry')?>">Agent Inquiry</a>
                        </li>

                        <li class="<?=(base_url('admin/pricing')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/pricing')?>">Pricing</a>
                        </li>
                   </ul>
                </li>

                <li class="menu-list <?=(base_url('admin/front_page')==current_url() || base_url('admin/slider')==current_url() || base_url('admin/agency')==current_url() || base_url('admin/services')==current_url() || base_url('admin/fun-facts')==current_url() || base_url('admin/news')==current_url() || base_url('admin/qualify')==current_url() )?'active':"" ?>">
                    <a href=""><i class="fa fa-file-text"></i> <span>Manage Home Page</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('admin/front_page')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/front_page')?>">Home page</a>
                        </li>
                        <li class="<?=(base_url('admin/slider')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/slider')?>">Manage Slider</a>
                        </li>
                        <li class="<?=(base_url('admin/qualify')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/qualify')?>"> Manage Qualify</a>
                        </li>
                        <li class="<?=(base_url('admin/agency')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/agency')?>">Digital Agency</a>
                        </li>
                        <li class="<?=(base_url('admin/services')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/services')?>"> Our Services</a>
                        </li>
                        <li class="<?=(base_url('admin/fun-facts')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/fun-facts')?>"> Fun Facts</a>
                        </li>
                        <li class="<?=(base_url('admin/news')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/news')?>"> Latest News</a>
                        </li>
                    </ul>
                </li>

                <li class="menu-list <?=(base_url('referral')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-bullhorn"></i> <span>Client Referral</span><span class="badge"> <?=get_referral_notification()?> </span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('referral')==current_url())?'active':"" ?>"><a href="<?=base_url('referral')?>"> Client Referral</a></li>
                         <li class="<?=(base_url('referral/referral_affiliates')==current_url())?'active':"" ?>"><a href="<?=base_url('referral/referral_affiliates')?>">Referal Affiliates</a></li> 
                         <li class="<?=(base_url('referral/agent_inquiry')==current_url())?'active':"" ?>"><a href="<?=base_url('referral/agent_inquiry')?>">Agent Inquiry List</a></li>
                        
                    </ul>
                </li>

                <li class="menu-list <?=(base_url('partner')==current_url() )?'active':"" ?>">
                    <a href=""><i class="fa fa-cogs"></i> <span>International Partner</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('partner')==current_url())?'active':"" ?>"><a href="<?=base_url('partner')?>"> International Partner</a></li>
                        <!-- <li class="<?=(base_url('partner/partner_add')==current_url())?'active':"" ?>"><a href="<?=base_url('partner/partner_add')?>"> Add Partner</a></li> -->
                       </ul>
                </li>
                
                 <li class="menu-list <?=(base_url('admin/setting')==current_url() || base_url('admin/faq')==current_url() || base_url('admin/get_started')==current_url() || base_url('admin/book_consultant')==current_url() )?'active':"" ?>">
                    <a href=""><i class="fa fa-cog"></i> <span>Settings</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('admin/setting')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/setting')?>"> Header/Footer</a>
                        </li>

                        <li class="<?=(base_url('admin/setting')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/setting')?>"> Social</a>
                        </li>

                        <!-- <li class="<?=(base_url('admin/our_team')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/our_team')?>">About Us</a>
                        </li> -->

                        <li class="<?=(base_url('admin/faq')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/faq')?>">FAQ</a>
                        </li>
                         
                        <li class="<?=(base_url('admin/get_started')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/get_started')?>">Get Started</a>
                        </li>
                        <li class="<?=(base_url('admin/book_consultant')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/book_consultant')?>">Book Consultant</a>
                        </li>
                        <!-- <li class="<?=(base_url('admin/pricing')==current_url())?'active':"" ?>">
                            <a href="<?=base_url('admin/pricing')?>">Pricing</a>
                        </li> -->
                    </ul>
                </li> 

                <!-- <li class="menu-list <?=(base_url('admin/services')==current_url() || base_url('admin/services/add')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-archive"></i> <span>All Services</span></a>
                    <ul class="sub-menu-list">
                    <li class="<?=(base_url('admin/services')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/services')?>"> View All</a></li>
                    <li class="<?=(base_url('admin/services/add')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/services/add')?>"> Add New</a></li>
                    </ul>
                </li>
                <li class="menu-list <?=(base_url('admin/news')==current_url() || base_url('admin/news/add')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-desktop"></i> <span>Latest News</span></a>
                    <ul class="sub-menu-list">
                    <li class="<?=(base_url('admin/news')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/news')?>"> View All</a></li>
                    <li class="<?=(base_url('admin/news/add')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/news/add')?>"> Add New</a></li>
                    </ul>
                </li>
                <li class="menu-list <?=(base_url('admin/fun_facts')==current_url() || base_url('admin/fun_facts/add')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-folder-open"></i> <span>Fun Facts</span></a>
                    <ul class="sub-menu-list">
                    <li class="<?=(base_url('admin/fun_facts')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/fun_facts')?>"> View All</a></li>
                    <li class="<?=(base_url('admin/fun_facts/add')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/fun_facts/add')?>"> Add New</a></li>
                    </ul>
                </li> -->

                <!--  <li class="menu-list <?=(base_url('admin/slider')==current_url() || base_url('admin/slider/add')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-bullhorn"></i> <span>Manage Slider</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('admin/slider')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/slider')?>"> View All Slides</a></li>
                        <li class="<?=(base_url('admin/slider/add')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/slider/add')?>"> Add New Slide</a></li>
                    </ul>
                </li> -->
                <!-- <li class="menu-list <?=(base_url('admin/qualify')==current_url() || base_url('admin/qualify/add')==current_url())?'active':"" ?>">
                    <a href=""><i class="fa fa-bullhorn"></i> <span>Manage Qualify</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?=(base_url('admin/qualify')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/qualify')?>"> View All Slides</a></li>
                        <li class="<?=(base_url('admin/qualify/add')==current_url())?'active':"" ?>"><a href="<?=base_url('admin/qualify/add')?>"> Add New Slide</a></li>
                    </ul>
                </li> -->
                
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
    <!-- left side end-->