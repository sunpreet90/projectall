<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8">
  <title>Next Stop Canada</title>
  <!-- <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  
  <meta property="og:title" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content=""> -->

  <!-- Styles -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="<?php echo base_url('assets') ?>/web/css/font-awesome.min.css"> 
  <link rel="stylesheet" href="<?php echo base_url('assets') ?>/web/css/animate.css">
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900|Montserrat:400,700' rel='stylesheet' type='text/css'>
  
  <!-- <link rel="stylesheet" href="<?php echo base_url('assets') ?>/web/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="<?php echo base_url('assets') ?>/web/css/main.css">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

  <style>
  section {
    padding-top: 100px;
      padding-bottom: 100px;
  }

  .quote {
      color: rgba(0,0,0,.1);
      text-align: center;
      margin-bottom: 30px;
  }
  .carousel-facts {
      padding-top: 0 !important;
  }
  p.text-center.canada {
      font-size: 30px;
      font-weight: 600;
  }

  /* carousel indicators css start */
  #fade-quote-carousel.carousel {
    padding-bottom: 60px;
  }
  #fade-quote-carousel.carousel .carousel-inner .item {
    opacity: 0;
    -webkit-transition-property: opacity;
        -ms-transition-property: opacity;
            transition-property: opacity;
  }
  #fade-quote-carousel.carousel .carousel-inner .active {
    opacity: 1;
    -webkit-transition-property: opacity;
        -ms-transition-property: opacity;
            transition-property: opacity;
  }
  #fade-quote-carousel.carousel .carousel-indicators {
    bottom: 10px;
  }
  #fade-quote-carousel.carousel .carousel-indicators > li {
    background-color: #e84a64;
    border: none;
  }
  
  /* carousel indicators css End */
  </style>

  <script src="<?php echo base_url('assets') ?>/web/js/modernizr-2.7.1.js"></script>
  
</head>

<body>
  
  <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="logo" href="index.php"><img src="https://www.abc-lang.com/landing-page/ImmigrateCanada/img/landing-logo.jpg" alt="Logo"></a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#pricing" class="scroll">Pricing</a></li>
              <li><a href="https://www.abc-lang.com/free-assessments" target="_blank">Free Assessment</a></li>
              
            </ul>
          </div><!--/.navbar-collapse -->
        </div>
      </div>
          
      <header>
        <div class="container">
          <div class="row">
            <div class="col-xs-6">
             <!--- <a href="index.html"><img src="https://www.abc-lang.com//resources/images/logo/logo.png" alt="Logo"></a>---->
            </div>
            <div class="col-xs-6 signin text-right navbar-nav">
              <a href="#pricing" class="scroll">Pricing</a>&nbsp; &nbsp;<a href="<?php echo base_url() ?>/auth/login">Sign in</a>
            </div>
          </div>
          
          <div class="row header-info">
            <div class="col-sm-10 col-sm-offset-1 text-center">
              <h1 class="wow fadeIn">Welcome to Canada!</h1>
              <br />
              <p class="lead wow fadeIn" data-wow-delay="0.5s">Do you want to live, work or study in Canada? <br>
             We can help your dream come true!</p>
              <br />
                
              <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                  <div class="row">
                    <div class="col-xs-6 text-right wow fadeInUp" data-wow-delay="1s">
                      <a href="#be-the-first" class="btn btn-secondary btn-lg scroll">Learn More</a>
                    </div>
                    <div class="col-xs-6 text-left wow fadeInUp" data-wow-delay="1.4s">
                      <a href="#invite" class="btn btn-primary btn-lg scroll">HELP ME IMMIGRATE!</a>
                    </div>
                  </div><!--End Button Row-->  
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </header>