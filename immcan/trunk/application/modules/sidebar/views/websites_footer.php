 <!--================Footer Area =================-->
        <footer class="footer_area">
            <div class="footer_widgets_area">
                <div class="container">
                    <div class="f_widgets_inner row">
                        <div class="col-lg-3 col-md-6">
                            <aside class="f_widget subscribe_widget">
                                <div class="f_w_title">
                                    <h3><?=get_config_item('social_title')?></h3>
                                </div>
                                <p><?=get_config_item('social_content')?> </p>
                               
                                <ul>
                                    <li><a href="<?=get_config_item('facebook_link')?>"><i class="fa fa-<?=get_config_item('facebook_icon')?>"></i></a></li>
                                    <li><a href="<?=get_config_item('twitter_link')?>"><i class="fa fa-<?=get_config_item('twitter_icon')?>"></i></a></li>
                                    <li><a href="<?=get_config_item('google_link')?>"><i class="fa fa-<?=get_config_item('google_icon')?>"></i></a></li>
                                    <li><a href="<?=get_config_item('instagram_link')?>"><i class="fa fa-<?=get_config_item('instagram_icon')?>"></i></a></li>
                                    <li><a href="<?=get_config_item('pinterest_link')?>"><i class="fa fa-<?=get_config_item('pinterest_icon')?>"></i></a></li>
                                </ul>
                            </aside>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <aside class="f_widget twitter_widget">
                                <div class="f_w_title">
                                   <h3><a href="<?=get_config_item('iccrc_link')?>" target="_blank"><?=get_config_item('twitter_title')?></a></h3>
                                    </div>
                                 <!-- <p><?=get_config_item('twitter_content')?></p> -->
                                <!--  <img src="<?php echo base_url() ?>/assets/images/imgpsh_fullsize.jpg" style="width: 185px; height: 130px; margin-top: 10px;">  -->
                                <img src="<?php echo base_url() ?>/uploads/settings/<?=get_config_item('footer_image')?>" style="width: 200px; height: 195px; margin-bottom: 5px;">
                                <p>
                               <?=get_config_item('twitter_content')?>
                                </p>
                                <!-- <div class="tweets_feed"></div> -->
                            </aside>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <aside class="f_widget categories_widget">
                                <div class="f_w_title">
                                    <h3><?=get_config_item('footer_widget_category_title')?></h3>
                                </div>
                                <?=get_config_item('footer_widget_category_content')?>

                                <!-- <ul>
                                    <li><a href="<?=base_url('website')?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Home</a></li>
                                    <li><a href="<?=base_url('about-us')?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>About</a></li>
                                    <li><a href="<?=base_url('website/service')?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Services</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Work</a></li>
                                    <li><a href="<?=base_url('privacy-policy')?>"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Privacy</a></li>
                                    <li><a href="https://client.canadaconsultportal.ca/Login/Login?siteId=CCP2018135&qlauncher=true" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Start Assessment</a></li>
                                </ul> -->
                            </aside>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <aside class="f_widget contact_widget">
                                <div class="f_w_title">
                                    <h3><?=get_config_item('footer_widget_contact_title')?></h3>
                                </div>
                                <?=get_config_item('footer_widget_contact_content')?>

                                <!-- <a href="#"><?=get_config_item('contact_content')?></a>
                                <a href="#"><?=get_config_item('phone_url')?></a>
                                <p><?=get_config_item('contact_url')?></p>
                                <h6><?=get_config_item('contact_tagline')?></h6> -->
                                
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copy_right_area">
                <div class="container">
                    <div class="float-md-left">
                        <h5><?=get_config_item('copyright_content')?></h5>
                       <!--  <h5>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </a></h5> -->
                    </div>
                    <div class="float-md-right">
                        <ul class="nav">
                           <!--  <li class="nav-item">
                                <a class="nav-link active" href="#">Disclaimer</a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('page/privacy-policy/132')?>">Privacy</a>
                            </li>
                           <!--  <li class="nav-item">
                                <a class="nav-link" href="#">Advertisement</a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('contact/104')?>">Contact us</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!--================End Footer Area =================-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--<script src="<?php echo base_url('assets') ?>/immcan/js/jquery.js"></script>-->
       
        <!-- Datetime Picker JS for consform Start here -->
        <!-- Datetime Picker for consform End here -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src='https://www.google.com/recaptcha/api.js'></script> <!-- Captcha JS -->
        <script src="<?php echo base_url('assets') ?>/immcan/js/popper.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/counterup/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/counterup/apear.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/counterup/countto.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/parallaxer/jquery.parallax-1.1.3.js"></script>
        <!--Tweets-->
        <!-- <script src="<?php echo base_url('assets') ?>/immcan/vendors/tweet/tweetie.min.js"></script> -->
        <script src="<?php echo base_url('assets') ?>/immcan/vendors/tweet/script.js"></script>

        <script src="<?php echo base_url('assets') ?>/immcan/js/theme.js"></script>

     <!-- <div class="modal hide" id="myModal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h3>Login to MyWebsite.com</h3>
          </div>
          <div class="modal-body">
            <form method="post" action='' name="login_form">
              <p><input type="text" class="span3" name="eid" id="email" placeholder="Email"></p>
              <p><input type="password" class="span3" name="passwd" placeholder="Password"></p>
              <p><button type="submit" class="btn btn-primary">Sign in</button>
                <a href="#">Forgot Password?</a>
              </p>
            </form>
          </div>
          <div class="modal-footer">
            New To MyWebsite.com?
            <a href="#" class="btn btn-primary">Register</a>
          </div>
        </div> -->

<!-- <script src="<?php echo base_url('assets') ?>/js/jquery-1.10.2.min.js"></script> -->
<script src="<?php echo base_url('assets') ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery.stepy.js"></script>
<!--<script src="<?php //echo base_url('assets') ?>/js/bootstrap.min.js"></script>-->
<script src="<?php echo base_url('assets') ?>/js/modernizr.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery.nicescroll.js"></script>

<!--common scripts for all pages-->
<script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>

<!-- <script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js"></script> -->
<!--js for custom changes-->
<script src="<?php echo base_url('assets') ?>/immcan/js/jquery.creditCardValidator.js"></script>
<script src="<?php echo base_url('assets') ?>/immcan/js/custom.js"></script>


<script src="<?php echo base_url('assets') ?>/immcan/js/jquery.datetimepicker.full.js"></script>


<!-- Onwechat script for chat with admin start here -->
<script type='text/javascript'>
var onWebChat={ar:[], set: function(a,b){if (typeof onWebChat_==='undefined'){this.ar.
push([a,b]);}else{onWebChat_.set(a,b);}},get:function(a){return(onWebChat_.get(a));},w
:(function(){ var ga=document.createElement('script'); ga.type = 'text/javascript';ga.
async=1;ga.src='//www.onwebchat.com/clientchat/71c8b9a9e628c8327be10392e0caf5ac';
var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})()}
</script>
<!-- Onwechat script for chat with admin end here -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<?php
/*
<script>
    //STEPY WIZARD
    $(function() {
        $('#default').stepy({
            backLabel: 'Previous',
            block: true,
            nextLabel: 'Next',
            titleClick: true,
            titleTarget: '.stepy-tab'
        });
    });
    //STEPY WIZARD WITH VALIDATION
    $(function() {
        $('#stepy_form').stepy({
            backLabel: 'Back',
            nextLabel: 'Next',
            errorImage: true,
            block: true,
            description: true,
            legend: false,
            titleClick: true,
            titleTarget: '#top_tabby',
            validate: true
        });
        $('#stepy_form').validate({
            errorPlacement: function(error, element) {
                $('#stepy_form div.stepy-error').append(error);
            },
            rules: {
                'name': 'required',
                'email': 'required'
            },
            messages: {
                'name': {
                    required: 'Name field is required!'
                },
                'email': {
                    required: 'Email field is requerid!'
                }
            }
        });
    });
</script>
*/
?>
    </body>
</html>




 