<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Sidebar extends MX_Controller {

	function __construct() {

			parent::__construct();
		}

		public function left_sidebar() {

			$this->load->view('left_sidebar',isset($data) ? $data : NULL);
		}

		public function footer() {

			$this->load->view('footer',isset($data) ? $data : NULL);
		}

		public function top_header() {
			$data['notifications'] = array();
			$this->db->select('*');
			$this->db->where('status', '1');
			$this->db->limit(5);
			$q = $this->db->get('ws_notifications');
			$data['notifications'] = $q->result_array();
			$data['notiCount'] = count($data['notifications']);
			/*print_r($data['notifications']);
die;*/
			$this->load->view('top_header',isset($data) ? $data : NULL);
		}

		public function immcan_left_sidebar() {

			$this->load->view('immcan_left_sidebar',isset($data) ? $data : NULL);
		}

		public function immcan_footer() {

			$this->load->view('immcan_footer',isset($data) ? $data : NULL);
		}

		public function immcan_top_header() {
			$data['notifications'] = array();
			$this->db->select('*');
			$this->db->where('status', '1');
			$this->db->limit(5);
			$q = $this->db->get('ws_notifications');
			$data['notifications'] = $q->result_array();
			$data['notiCount'] = count($data['notifications']);
			
			$this->load->view('immcan_top_header',isset($data) ? $data : NULL);
		}

		public function flash_msg() {

			$this->load->view('flash_msg',isset($data) ? $data : NULL);
		}


		public function website_left_sidebar(){
			$this->load->view('website_left_sidebar',isset($data) ? $data : NULL);
		}

		public function website_top_header(){
			$this->load->view('website_top_header',isset($data) ? $data : NULL);
		}

		public function website_footer(){
			$this->load->view('website_footer',isset($data) ? $data : NULL);
		}


		/* Main Theme layout name as Websites start*/

		public function websites_top_header(){
			$this->load->view('websites_top_header', isset($data) ? $data : NULL);
		}


		public function websites_footer(){
			$this->load->view('websites_footer', isset($data) ? $data : NULL);
		}

		public function datepicker_header(){
			$this->load->view('datepicker_header', isset($data) ? $data : NULL);
		}


		public function datepicker_footer(){
			$this->load->view('datepicker_footer', isset($data) ? $data : NULL);
		}

		/* Main Theme layout name as Websites end*/



}
/* End of file sidebar.php */