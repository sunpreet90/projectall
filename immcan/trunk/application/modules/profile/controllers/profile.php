<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Profile extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}
		$this->load->library('form_validation');
		$this->load->model('profile_model');
		$this->load->model('helper_model');
	}
	function index(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('edit_profile')
		->build('edit_profile',isset($data) ? $data : NULL);
	}

	function settings()
	{
		redirect('profile');
	}
	function change_email()
	{
		$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');	

		if ( $this->form_validation->run() == FALSE )
		{
			//echo "false";die;
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', 'Change password failed');
			$_POST = '';

			//$this->load->module('layouts');
			//$this->load->library('template');
			//$this->template
			//->set_layout('edit_profile')
			//->build('edit_profile',isset($data) ? $data : NULL);
		   redirect('profile');
		}else{
            //echo "true";die;
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			if ( $this->helper_model->check_user_password($password) )  // success
			{
				if ( $this->profile_model->change_email( $email ) ) // adding new email
				{
					$this->session->set_flashdata('response_status', 'success');
					$this->session->set_flashdata('message', 'Email changed successfully.');
					$_POST = '';
					redirect('profile');		
				}else{													// database error

					$this->session->set_flashdata('response_status', 'error');
					$this->session->set_flashdata('message', 'Error in adding to database.');
					$_POST = '';
					redirect('profile');
				}
			}else{			// Failed password not matched
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Password not correct.');
				$_POST = '';
				redirect('profile');
			}
		}	

	} //change_email

	function change_username()
	{

		$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
		$this->form_validation->set_rules('username', 'User name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');		
		if ( $this->form_validation->run() == FALSE )
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', 'Change username failed');
			$_POST = '';
			/*$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('edit_profile')
			->build('edit_profile',isset($data) ? $data : NULL);*/
			redirect('profile');
		}else{

			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ( $this->helper_model->check_user_password($password) )  // success
			{
				if ( $this->profile_model->change_username( $username ) ) // adding new email
				{
					$this->session->set_flashdata('response_status', 'success');
					$this->session->set_flashdata('message', 'User name changed successfully.');
					$_POST = '';
					redirect('profile');		
				}else{													// database error

					$this->session->set_flashdata('response_status', 'error');
					$this->session->set_flashdata('message', 'Error in adding User name to database.');
					$_POST = '';
					redirect('profile');
				}
			}else{			// Failed password not matched
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Password not correct.');
				$_POST = '';
				redirect('profile');
			}
		}	
		
	}

	function change_password()
	{		

		$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
		$this->form_validation->set_rules('old_password', 'Old password', 'required');
		$this->form_validation->set_rules('new_password', 'New password', 'required');		
		$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|matches[new_password]');		
		if ($this->form_validation->run() == FALSE)
		{
			//echo "ok run";die;
		 //echo validation_errors();die;
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', 'Change password failed');
		    
			$_POST = '';

			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('edit_profile')
			->build('edit_profile',isset($data) ? $data : NULL);
		
		}else{
           
			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');

			if ($this->tank_auth->change_password( $old_password, $confirm_password )) 
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Password changed successfully.');
				$_POST = '';

				redirect('profile');

			}else{
					
				$errors = $this->tank_auth->get_error_message();
				if ( $errors['old_password'] == 'auth_incorrect_password' ) 
				{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('response_status', 'auth_incorrect_password');
				$this->session->set_flashdata('message', 'Old password is not correct.');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('edit_profile')
				->build('edit_profile',isset($data) ? $data : NULL);
				}
			}
		}
		
	} // change_password
}

/* End of file profile.php */