<?php //pr($username); ?>
        <!-- page heading start-->
        <div class="col-md-12">
        <div class="row">
        <div class="page-heading">
            <h3>
                Profile Settings
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?php echo base_url();?>">Dashboard</a>
                </li>
                <li class="active">Profile</li>
            </ul>
      
        <div class="state-info col-md-5 ">
            <?php if ( $this->session->flashdata('response_status') == 'error' ) {
                
              ?>
                    <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <h4>
                                    <i class="icon-ok-sign"></i>
                                    <?php echo $this->session->flashdata('response_status');?>
                                </h4>
                                <p><?php echo  $this->session->flashdata('message');?></p>
                                <p><?php echo validation_errors(); ?></p>
                            </div>
                <?php } ?>
           
           <?php if ( $this->session->flashdata('response_status') == 'auth_incorrect_password' ) { ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"></i>
                            <?php // echo $this->session->flashdata('response_status');?>
                        </h4>
                        <p><?php echo $this->session->flashdata('message');?></p>
                        <p><?php echo validation_errors(); ?></p>
                    </div>
            <?php } ?>
            <?php if ( $this->session->flashdata('response_status') == 'success' ) { ?>
                    <div class="alert alert-success alert-block fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"></i>
                            <?php  echo $this->session->flashdata('response_status');?>
                        </h4>
                        <p><?php echo $this->session->flashdata('message');?></p>
                        <p><?php echo validation_errors(); ?></p>
                    </div>
            <?php } ?>
            </div>
            
             <!-- <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>-->
            
            </div>
            </div>
          </div>      
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
            <div class="col-md-6" >
                <section class="panel">
                    <header class="panel-heading">
                         Change password

                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!-- <a href="javascript:;" class="fa fa-times"></a>  -->
                        </span>
                    </header>
                    <div class="panel-body" style="display: block;">
                         <div class="col-md-8">
                            <form role="form" action="<?=base_url()?>profile/change_password" method="post">
                                <div class="form-group">
                                    <label for="">Old Password</label>
                                    <input type="password" name="old_password" placeholder="Password" id="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">New Password</label>
                                    <input type="password" name="new_password" placeholder="Password" id="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Confirm Password</label>
                                    <input type="password" name="confirm_password" placeholder="Password" id="" class="form-control">
                                </div>
                                <button class="btn btn-primary" type="submit">Change Password</button>
                            </form>
                        </div>
                    </div>
                </section>

                <section class="panel">
                    <header class="panel-heading">
                         Change Username
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!-- <a href="javascript:;" class="fa fa-times"></a>  -->
                        </span>
                    </header>
                    <div class="panel-body" style="display: none">
                         <div class="col-md-8">
                             <form role="form" action="<?=base_url()?>profile/change_username" method="post">
                                <div class="form-group">
                                    <label for="">User name</label>
                                    <input type="text" name="username" placeholder="Enter user name" id="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Password</label>
                                    <input type="password" placeholder="Password" name="password" id="" class="form-control">
                                </div>
                                <button class="btn btn-primary" type="submit">Change email</button>
                            </form>
                        </div>
                    </div>
                </section>

            </div>

            <div class="col-md-6" >
                <section class="panel">
                    <header class="panel-heading">
                         Change email
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <!-- <a href="javascript:;" class="fa fa-times"></a>  -->
                        </span>
                    </header>
                    <div class="panel-body" style="display: block;">
                         <div class="col-md-8">
                             <form role="form" action="<?=base_url()?>profile/change_email" method="post">
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" name="email" placeholder="Enter email" id="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Password</label>
                                    <input type="password" placeholder="Password" name="password" id="" class="form-control">
                                </div>
                                <button class="btn btn-primary" type="submit">Change email</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
                
        </div>
        <!--body wrapper end-->
