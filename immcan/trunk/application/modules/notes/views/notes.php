<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_page.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/data-tables/DT_bootstrap.css" rel="stylesheet">
<!-- page heading start-->
<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> My Dashboard </li>
    </ul>
</div>
<!-- page heading end-->
<?php // pr($notes); ?>

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                        <header class="panel-heading">
                            Notes
                            <!--
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                             </span>
                         -->
                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Note title</th>
                                        <th>Note Description</th>
                                        <th>Platform(s)</th>
                                        <th class="center">Edit</th>
                                        <th class="center">Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if ( !empty( $notes )) 
                                        {
                                            foreach ($notes as $note) 
                                            {
                                                //pr($note);
                                            ?>
                                            <tr class="gradeX">
                                                <td><?=$note['note_title']?></td>
                                                <td><?=$note['note_description']?></td>
                                                
                                                <td>Win 95+</td>
                                                <td class="center hidden-phone"><a href="<?=base_url('notes')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                                <td class="center hidden-phone">
                                                <a href="<?=base_url('notes')?>/delete/id/<?=$note['id'];?>">
                                                    <i class="fa fa-eraser"></i>
                                                </a>
                                                </td>
                                            </tr>
                                            <?php }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Note title</th>
                                            <th>Note Description</th>
                                            <th>Platform(s)</th>
                                            <th class="center">Edit</th>
                                            <th class="center">Delete</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </section>
        </div>
    </div>
</div>


<!--Data tables script-->




<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=base_url('assets')?>/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="<?=base_url('assets')?>/text/javascript" src="<?=base_url('assets')?>/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="<?=base_url('assets') ?>/js/dynamic_table_init.js"></script>

<!--common scripts for all pages-->
<script src="<?=base_url('assets') ?>/js/scripts.js"></script>





<!--body wrapper end-->