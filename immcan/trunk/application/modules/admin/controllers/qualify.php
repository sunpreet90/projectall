<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Qualify extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';

		$data['qualify'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 7 ) ); //category_id = 4 //services

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/qualify',isset($data) ? $data : NULL);
	}
	   
	public function add($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
				'post_title' 	=> $this->input->post('post_title'), 
				'post_slug' 	=> $post_slug, 
				'post_content' 	=> $this->input->post('post_content'),
				'status' 	=> $this->input->post('status'),
				'category_id' 	=> 7
			);
		
			$insert_id =$this->admin_model->qualify_insert( $this->table, $insert_data );

			if( !$insert_id )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				add_post_meta( $post_id = $insert_id, $meta_key = 'qualify_icon', $meta_value = $this->input->post('qualify_icon') );

				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Qualify created successfully');
				redirect('admin/qualify');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_qualify',isset($data) ? $data : NULL);
	}	



	public function edit($value='')
	{
		# code..
		
		$qualify_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $qualify_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';


		if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		

      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

      		//pr($_FILES); die();

      		if($_FILES['featured_image']['name'] == '') {

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		} else {

      			//pr( $this->do_upload() ); die();

      			$featured_image_name = $this->do_upload();

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,						
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		}

			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,								
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							);

			$this->db->where('id', $qualify_id);

			/*if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/qualify');
			}
			else
			{
				add_post_meta( $post_id = $qualify_id, $meta_key = 'qualify_icon', $meta_value = $this->input->post('qualify_icon'));
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page edited successfully');
				redirect('admin/qualify');
			}*/


			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/qualify');
			}
			else
			{
				add_post_meta( $post_id = $qualify_id, $meta_key = 'qualify_icon', $meta_value = $this->input->post('qualify_icon'));
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Qualify edited successfully');
				redirect('admin/qualify');
			}
		}
		

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_qualify',isset($data) ? $data : NULL);

	}






	/*public function edit($value='')
	{
		# code..
		
		$qualify_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $qualify_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';

      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							);

			$where_data  = array(
				'id' => $qualify_id,
				'category_id' => 7
			 );
				

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/qualify');
			}
			else
			{
				add_post_meta( $post_id = $qualify_id, $meta_key = 'qualify_icon', $meta_value = $this->input->post('qualify_icon'));
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page edited successfully');
				redirect('admin/qualify');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_qualify',isset($data) ? $data : NULL);

	}	*/
	/*public function delete()
			{
				$array = $this->uri->segment_array();
				$delete_id = end($array);
                $this->admin_model->delete_data_from_table('posts',$delete_id);
                redirect('admin/services', 'refresh');

		}*/

	/* Page.php END here */


	

	public function delete()
	{
		$qualify_id = $this->uri->segment(5);
		

		$this->admin_model->delete_qualify($qualify_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/qualify');
	}
}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */