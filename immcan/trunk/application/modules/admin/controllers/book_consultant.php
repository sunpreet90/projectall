
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Book_consultant extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';

		$data['book_consultant'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 20 ) );
		//echo "<pre>"; print_r($data['book_consultant']); die;
		 //category_id = 4 //services

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/book_consultant',isset($data) ? $data : NULL);
	}
	   
	public function add($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
				'post_title' 	=> $this->input->post('post_title'), 
				'post_slug' 	=> $post_slug, 
				'post_content' 	=> $this->input->post('post_content'),
				'post_excerpt' 	=> $this->input->post('post_excerpt'),
				'status' 	=> $this->input->post('status'),
				'category_id' 	=> 20
			);
		
			$insert_id =$this->admin_model->insert_book_consultant( $this->table, $insert_data );

			if( !$insert_id )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				/*add_post_meta( $post_id = $insert_id, $meta_key = 'services_icon', $meta_value = $this->input->post('services_icon') );
				add_post_meta( $post_id = $insert_id, $meta_key = 'read_more_button_label', $meta_value = $this->input->post('read_more_button_label') );
				add_post_meta( $post_id = $insert_id, $meta_key = 'read_more_button_link', $meta_value = $this->input->post('read_more_button_link') );*/

				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Book Consultant created successfully');
				redirect('admin/book_consultant');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_book_consultant',isset($data) ? $data : NULL);
	}
	
	public function edit($value='')
	{
		# code..
		
		$book_consultant_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $book_consultant_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';

      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'post_content' => $this->input->post('post_content'),
								'post_excerpt' 	=> $this->input->post('post_excerpt'),
								'status' => $this->input->post('status')
							);

			$this->db->where('id', $book_consultant_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/book_consultant');
			}
			else
			{
				/*add_post_meta( $post_id = $services_id, $meta_key = 'services_icon', $meta_value = $this->input->post('services_icon'));
				add_post_meta( $post_id = $services_id, $meta_key = 'read_more_button_label', $meta_value = $this->input->post('read_more_button_label') );
				add_post_meta( $post_id = $services_id, $meta_key = 'read_more_button_link', $meta_value = $this->input->post('read_more_button_link') );*/

				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Book Consultant edited successfully');
				redirect('admin/book_consultant');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_book_consultant',isset($data) ? $data : NULL);

	}	
	
	public function delete()
	{
		$book_consultant_id = $this->uri->segment(5);
		

		$this->admin_model->delete_book_consultant($book_consultant_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/book_consultant');
	}
	/*public function delete1()
	{
		$immigration_id = $this->uri->segment(5);
		

		$this->admin_model->delete_immigration($immigration_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/services');
	}*/
}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */