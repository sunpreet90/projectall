<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class message extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		//This will load 'admin_model' 
		$this->load->model('admin_model');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		
		$this->prefix = $this->config->item('db_table_prefix');
		$this->table = $this->prefix.'contactus';
	}

	public function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		//$data['page'] 	= $this->admin_model->get_page();

		//pr($data);

		//$data['published_page'] = $this->admin_model->get_page();
		//$data['pending_page'] 	= $this->admin_model->get_page();
		//$data['trash_page'] 	= $this->admin_model->get_page(); 

		$data['messages'] 	= $this->admin_model->getmessages( $this->table );

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('messages',isset($data) ? $data : NULL);
	}
	/*public function notification(){

		$getQuery = $this->admin_model->getrecords( $this->table );

		pr($getQuery);
		die;
	}*/

	// Function to view messages.
	public function view(){
         //echo "Inside Function"; die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('message_single',isset($data) ? $data : NULL);
	}
	// view Ends here..

	// Function to delete Messages
	public function delete(){
		$msgId = $this->uri->segment(5);
		//echo $msgId;die;
		$this->db->where('id', $msgId);
		if($this->db->delete('ws_contactus')){

		//if($this->partner_model->delete_partner($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		//}
		}
		header('Location: '.base_url().'admin/message');
	}
	// Delete Ends Here..
}

/* End of file message.php */
/* Location: ./application/modules/admin/controllers/message.php */