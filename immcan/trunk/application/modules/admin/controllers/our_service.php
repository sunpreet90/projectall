<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Our_service extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');

		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP

		$this->table = $this->prefix.'posts';
	}
	
	function index()
	{
        $data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		//$data['post_services']	= $this->admin_model->get_posts( $where_data = array( 'category_id' => 13, 'status' => 'publish' ), $multi_record = TRUE);
		$data['post_services'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 13 ) );
		$data['range_services'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 14 ) );
		//echo "<pre>"; print_r($data['post_services']); die;
        if ($this->input->post('action')) {
			# code...
			if ($this->input->post('action') == 'service') {

				set_config_item('services_title', $this->input->post('services_title'));
				set_config_item('services_tagline', $this->input->post('services_tagline'));
				set_config_item('services_content', $this->input->post('services_content'));

				if ($this->input->post('our_services_page_status')) {
					# code...
					set_config_item('our_services_page_status', 1);
				} else {
					set_config_item('our_services_page_status', 0);
				}

			  if($_FILES['services_image']['name'] != '') {

                	set_config_item('services_image', $this->do_upload());
				}
				set_config_item('services_menu_page_title', $this->input->post('services_menu_page_title'));
				set_config_item('services_menu_page_link', $this->input->post('services_menu_page_link'));
			}

			if ($this->input->post('action') == 'user_service') {

				set_config_item('user_title', $this->input->post('user_title'));
				set_config_item('user_tagline', $this->input->post('user_tagline'));
				set_config_item('user_content', $this->input->post('user_content'));

				if ($this->input->post('user_stories_status')) {
					# code...
					set_config_item('user_stories_status', 1);
				} else {
					set_config_item('user_stories_status', 0);
				}

			    if($_FILES['user_image']['name'] != '') {

                	set_config_item('user_image', $this->do_upload1());
				}
				set_config_item('user_menu_title', $this->input->post('user_menu_title'));
				set_config_item('user_menu_link', $this->input->post('user_menu_link'));
			}
			if ($this->input->post('action') == 'range_services') {

				set_config_item('range_services_title', $this->input->post('range_services_title'));
				set_config_item('range_services_tagline', $this->input->post('range_services_tagline'));
				set_config_item('range_services_menu_title', $this->input->post('range_services_menu_title'));
				set_config_item('range_services_link', $this->input->post('range_services_link'));
			}
		}

		/*($this->input->post('firstname'))?$this->input->post('firstname'):"";*/
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/our_services_page',isset($data) ? $data : NULL);
	}


	function do_upload(){

		$config = array(
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);
		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('services_image')) {

			$uploadData = $this->upload->data();
			return $picture = './uploads/settings/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }


	function do_upload1(){

		$config = array(
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);
		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('user_image')) {

			$uploadData = $this->upload->data();
			return $picture = './uploads/settings/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }

	public function add($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
				'post_title' 	=> $this->input->post('post_title'), 
				'post_slug' 	=> $post_slug, 
				'post_content' 	=> $this->input->post('post_content'),
				'status' 	=> $this->input->post('status'),
				'category_id' 	=> 13
			);
	
			//if( !$this->admin_model->insert_post_service( $this->table, $insert_data ) )
			if( !$this->db->insert( $this->table, $insert_data) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Service created successfully');
				redirect('admin/our_service');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_service_section',isset($data) ? $data : NULL);
	}

	public function edit($value='')
	{
		# code..
		
		$post_services_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $post_services_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';

      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							);

			$this->db->where('id', $post_services_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/our_service');
			}
			else
			{
				
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page edited successfully');
				redirect('admin/our_service');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_service_section',isset($data) ? $data : NULL);

	}
	public function delete()
	{
		$post_services_id = $this->uri->segment(5);
		$this->admin_model->delete_post_service($post_services_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/our_service');
	}
	public function add1($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
				'post_title' 	=> $this->input->post('post_title'), 
				'post_slug' 	=> $post_slug, 
				'post_content' 	=> $this->input->post('post_content'),
				'status' 	=> $this->input->post('status'),
				'category_id' 	=> 14
			);
	
			//if( !$this->admin_model->insert_post_service( $this->table, $insert_data ) )
			if( !$this->db->insert( $this->table, $insert_data) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Service created successfully');
				redirect('admin/our_service');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_service_section2',isset($data) ? $data : NULL);
	}
	public function edit1($value='')
	{
		# code..
		
		$range_services_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $range_services_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';

      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							);

			$this->db->where('id', $range_services_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/our_service');
			}
			else
			{
				
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page edited successfully');
				redirect('admin/our_service');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_service_section2',isset($data) ? $data : NULL);

	}
	public function delete1()
	{
		$range_services_id = $this->uri->segment(5);
		$this->admin_model->delete_range_service($range_services_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/our_service');
	}	
}
