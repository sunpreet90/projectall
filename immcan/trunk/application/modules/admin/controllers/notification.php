<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Notification extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		//This will load 'admin_model' 
		$this->load->model('admin_model');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		
		$this->prefix = $this->config->item('db_table_prefix');
		$this->table = $this->prefix.'notifications';
	}

	public function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		//$data['page'] 	= $this->admin_model->get_page();

		//pr($data);

		/*$data['published_page'] 	= $this->admin_model->get_page();
		$data['pending_page'] 	= $this->admin_model->get_page();
		$data['trash_page'] 	= $this->admin_model->get_page();*/

		$data['notifications'] 	= $this->admin_model->getrecords( $this->table );

		//print_r($data['notifications']);

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('notifications',isset($data) ? $data : NULL);
	}

	/*public function notification(){

		$getQuery = $this->admin_model->getrecords( $this->table );

		pr($getQuery);
		die;
	}*/


	public function view(){
         //echo "Inside Function"; die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('notification_single',isset($data) ? $data : NULL);
	}
}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */