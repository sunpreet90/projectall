<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Affiliates extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';


		if ($this->input->post('action') == 'referral_affiliates') {
			
			set_config_item('referral_affiliates_title', $this->input->post('referral_affiliates_title'));
			set_config_item('referral_affiliates_content', $this->input->post('referral_affiliates_content'));
			
			if($_FILES['referral_affiliates_header_image']['name'] != '') {

				//pr($_FILES); die();

            	set_config_item('referral_affiliates_header_image', $this->do_upload());
			}

			$data['flash_status'] = 'success';
			$data['flash_title'] = 'Success!';
			$data['flash_message'] = 'Client affiliates page updated successfully!';
		}
		

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/affiliates',isset($data) ? $data : NULL);
	}

    function do_upload(){

		$config = array(
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('referral_affiliates_header_image')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/settings/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			//$this->load->view('file_view', $error);
			return '#error';
			//pr($error); die();
		}
    }

}
