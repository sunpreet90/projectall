<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Get_started extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';

		$data['get_started'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 16 ) ); //category_id = 8 //our_team
		//$data['about_help'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 15 ) ); //category_id = 8 //our_team


		if ($this->input->post('action')) {
			# code...
			

			if ($this->input->post('action') == 'get_started') {

				set_config_item('get_started_page_title', $this->input->post('get_started_page_title'));				
				set_config_item('get_started_tagline', $this->input->post('get_started_tagline'));				

			    if($_FILES['get_started_header_image']['name'] != '') {

                	set_config_item('get_started_header_image', $this->do_upload1());
				}
			}
		}



		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/get_started',isset($data) ? $data : NULL);
	}

	public function add($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		

      		//$featured_image_name = $this->do_upload();

			
			/*if($_FILES['featured_image']['name'] == '') {

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 8
				);

			} else {*/
				/*pr( $featured_image_name); die();*/

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 16,
					//'featured_image' 	=> $featured_image_name
				);
			//}

			
			if( !$this->admin_model->insert_get_started( $this->table, $insert_data ) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Get Started created successfully');
				redirect('admin/get_started');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_get_started',isset($data) ? $data : NULL);
	}



	public function edit($value='')
	{
		# code..

		$get_started_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $get_started_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';


		if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		

      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		$insert_data = array();

      		//pr($_FILES); die();
      		//print_r($_FILES); die;
      		/*if(empty($_FILES['featured_image']['name'])) {

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		} else {*/

      			//pr( $this->do_upload() ); die();

      			//$featured_image_name = $this->do_upload();

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						//'featured_image' 	=> $featured_image_name,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		//}

			/*$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'featured_image' 	=> $featured_image_name,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							); */

			$this->db->where('id', $get_started_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/get_started');
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Get Started edited successfully');
				redirect('admin/get_started');
			}
		}
		
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_get_started',isset($data) ? $data : NULL);

	}




	public function delete()
	{
		$get_started_id = $this->uri->segment(5);
		

		$this->admin_model->delete_get_started($get_started_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/get_started');
	}




	function do_upload(){

		$config = array(
			'upload_path' => './uploads/our-team',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		$new_name = 'slider-'.time().$_FILES["featured_image"]['name'];
		$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('featured_image')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/our-team/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }
    function do_upload1(){

		$config = array(
			'upload_path' => './uploads/our-team',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		//$new_name = 'slider-'.time().$_FILES["about_footer_image"]['name'];
		//$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('get_started_header_image')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/our-team/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }



}
