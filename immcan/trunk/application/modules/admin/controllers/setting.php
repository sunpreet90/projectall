<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Setting extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');

		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP

		$this->table = $this->prefix.'posts';
	}

	function index()
	{
        $data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		if ($this->input->post('action')) {

			if ($this->input->post('action') == 'email') {
               set_config_item('admin_email', $this->input->post('admin_email'));
           }
			# code...
			if ($this->input->post('action') == 'header') {
				# code...

				//pr(); die();

				set_config_item('site_title', $this->input->post('site_title'));
				set_config_item('base_url', $this->input->post('base_url'));

				if($_FILES['website_logo']['name'] != '') {

                	set_config_item('website_logo', $this->do_upload());
				}
			}
			

			if ($this->input->post('action') == 'footer') {
				# code...
               set_config_item('copyright_content', $this->input->post('copyright_content'));
			}

            if ($this->input->post('action') == 'twitter_footer') {
				# code...
               set_config_item('twitter_title', $this->input->post('twitter_title'));
               set_config_item('twitter_content', $this->input->post('twitter_content'));
               set_config_item('iccrc_link', $this->input->post('iccrc_link'));
             
               if($_FILES['footer_image']['name'] != '') {

                	set_config_item('footer_image', $this->do_upload1());
				}
			}

			if ($this->input->post('action') == 'contact_footer') {
				# code...
               set_config_item('footer_widget_contact_title', $this->input->post('footer_widget_contact_title'));
               set_config_item('footer_widget_contact_content', $this->input->post('footer_widget_contact_content'));
               
			}
			if ($this->input->post('action') == 'link_footer') {
				# code...
               set_config_item('footer_widget_category_title', $this->input->post('footer_widget_category_title'));
               set_config_item('footer_widget_category_content', $this->input->post('footer_widget_category_content'));
              
			}

			if ($this->input->post('action') == 'social') {
				# code...
                set_config_item('social_title', $this->input->post('social_title'));
				set_config_item('social_content', $this->input->post('social_content'));

				set_config_item('facebook_link', $this->input->post('facebook_link'));
				set_config_item('facebook_icon', $this->input->post('facebook_icon'));


				set_config_item('instagram_link', $this->input->post('instagram_link'));
				set_config_item('instagram_icon', $this->input->post('instagram_icon'));
				

				set_config_item('twitter_link', $this->input->post('twitter_link'));
				set_config_item('twitter_icon', $this->input->post('twitter_icon'));
				
				set_config_item('google_link', $this->input->post('google_link'));
				set_config_item('google_icon', $this->input->post('google_icon'));
				
				set_config_item('pinterest_link', $this->input->post('pinterest_link'));
				set_config_item('pinterest_icon', $this->input->post('pinterest_icon'));
				
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('setting',isset($data) ? $data : NULL);
	}

	function do_upload(){

		$config = array(
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);
		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('website_logo')) {

			$uploadData = $this->upload->data();
			return $picture = $uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }



    function do_upload1(){

		$config = array(
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);
		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('footer_image')) {

			$uploadData = $this->upload->data();
			return $picture = $uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }



	public function header()
	{
		$this->db->select("*"); 
		  	$this->db->from('config');
		  	$this->db->where('id',8);
		  	$query = $this->db->get();
		  	$data['site_title'] = $query->result();
       
		$this->load->module('layouts');
		$this->load->library('template');
        $this->template
		->set_layout('immcan')
		->build('setting/header',isset($data) ? $data : NULL);
	}

	public function edit($value='')
	{
		# code..
		
		$page_id = $this->uri->segment(4);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['page'] 		= $this->admin_model->get_pages($page_id);
      	
      	if ( $this->input->post('action') == 1 )
      	{
			$insert_data = 	array(
								'post_title' => $this->input->post('value'), 
								'post_slug' => '#', 
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							);
			
			if( !$this->admin_model->page_insert( $this->table, $insert_data ) )
			{
				die('error in page insert');
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page edited successfully');
				redirect('page');
			}
		}

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('setting',isset($data) ? $data : NULL);

	}	

	/* Page.php END here */






	function add_z()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('notes')
		->build('notes_add',isset($data) ? $data : NULL);


		if ($this->input->post() ) 
		{	

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('note_title', 'Note title', 'required');
			$this->form_validation->set_rules('note_description', 'Note description', 'required');		
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('page')
				->build('notes_add',isset($data) ? $data : NULL);
			
			}else{

				$note_title =$this->input->post('note_title');
				$note_description = $this->input->post('note_description');

				$notes_data=array(
                   'note_title'       	=> $note_title,
                   'note_description' 	=> $note_description,
                   'created'	=> $this->created
			    );
				if ( $this->note_model->add(  $this->table, $notes_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Note added successfully');
					$_POST = '';
					redirect('notes/add');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('notes/add');
				}
			}			
		}
	}

	function edit_z()
	{
		$pages_id = $this->uri->segment(1);
		$pages='';
		$pages = $this->page_model->get_pages( $pages_id );

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['note']		= $note;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('page_edit',isset($data) ? $data : NULL);


		//echo $note_id;	die();
		if ($this->input->post() ) 
		{	
			//echo $note_id;	die();

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('post_title', 'post title', 'required');
			$this->form_validation->set_rules('post_content', 'post content', 'required');
			$this->form_validation->set_rules('status', 'status', 'required');			
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
				redirect('page/edit/id/'.$pages_id);		
			
			}else{

				$post_title =$this->input->post('post_title');
				$post_content = $this->input->post('post_content');
				$status = $this->input->post('status');



				$page_data=array(
                   'post_title'       	=> $post_title,
                   'post_content' 	=> $post_content,
                   'status'        => $status,
                   'created'	=> $this->created
			    );

			    $where_data=array( 'id' => $pages_id );

				if ( $this->page_model->updateWhere(  $this->table, $where_data, $page_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Note Updated successfully');
					$_POST = '';
					redirect('notes/edit/id/'.$pages_id);		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('page/edit/id/'.$pages_id);
				}
			}			
		}
	}

	public function delete()
	{
		$page_id = $this->uri->segment(4);

		$this->page_model->delete_page($page_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('page');
	}
}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */