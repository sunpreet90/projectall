<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Front_page extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');

		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP

		$this->table = $this->prefix.'posts';
	}
	
function index()
	{
        $data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

          if ($this->input->post('action')) {
			# code...
			if ($this->input->post('action') == 'our_services') {

				set_config_item('main_title', $this->input->post('main_title'));
				set_config_item('tagline', $this->input->post('tagline'));

				if ($this->input->post('our_services_status')) {
					# code...
					set_config_item('front_page_services_section_status', 1);
				} else {
					set_config_item('front_page_services_section_status', 0);
				}
			}
			if ($this->input->post('action') == 'qualify') {

				set_config_item('qualify_title', $this->input->post('qualify_title'));
				set_config_item('qualify_menu_page_title', $this->input->post('qualify_menu_page_title'));
				set_config_item('qualify_menu_page_link', $this->input->post('qualify_menu_page_link'));
				
				if ($this->input->post('qualify_status')) {
					# code...
					set_config_item('qualify_status', 1);
				} else {
					set_config_item('qualify_status', 0);
				}
			}

			if ($this->input->post('action') == 'latest_news') {
				set_config_item('news_title', $this->input->post('news_title'));
				set_config_item('news_tagline', $this->input->post('news_tagline'));
				set_config_item('news_check_status', ($this->input->post('news_check_status')) ? $this->input->post('news_check_status'):"" );
				if ($this->input->post('news_check_status')) {
					# code...
					set_config_item('news_check_status', 1);
				} else {
					set_config_item('news_check_status', 0);
				}
			}
			if ($this->input->post('action') == 'canada_fun_facts') {
				if ($this->input->post('canada_fun_facts_status')) {
					# code...
					set_config_item('canada_fun_facts_status', 1);
				} else {
					set_config_item('canada_fun_facts_status', 0);
				}

             }


             if ($this->input->post('action') == 'digital_agency') {		

				if ($this->input->post('digital_agency_status')) {
					# code...
					set_config_item('digital_agency_status', 1);
				} else {
					set_config_item('digital_agency_status', 0);
				}
			}


             if ($this->input->post('action') == 'project') {

				set_config_item('project_title', $this->input->post('project_title'));
				set_config_item('project_tagline', $this->input->post('project_tagline'));

				if ($this->input->post('project_status')) {
					# code...
					set_config_item('project_status', 1);
				} else {
					set_config_item('project_status', 0);
				}
			}
			 if ($this->input->post('action') == 'serve_industries') {

			 	/*pr($this->input->post()); die();*/

				set_config_item('serve_industries_title', $this->input->post('serve_industries_title'));
				set_config_item('serve_industries_content', $this->input->post('serve_industries_content'));

				set_config_item('serve_industries_youtube_iframe', $this->input->post('serve_industries_youtube_iframe'));

				if ($this->input->post('serve_industries_status')) {
					# code...
					set_config_item('serve_industries_status', 1);
				} else {
					set_config_item('serve_industries_status', 0);
				}
			}
		}
		/*($this->input->post('firstname'))?$this->input->post('firstname'):"";*/

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('front_page',isset($data) ? $data : NULL);
	}
}
