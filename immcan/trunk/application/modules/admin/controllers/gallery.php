<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Gallery extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		
		/*$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';*/

		$data['gallery'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 23 ) ); //category_id = 23 //gallery

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/gallery',isset($data) ? $data : NULL);
	}

	public function add($value='')
	{
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      /*	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
             $featured_image_name = $this->do_upload();
			$insert_data = 	array(
				'post_title' 	=> $this->input->post('post_title'), 
				'post_slug' 	=> $post_slug, 
				'post_content' 	=> $this->input->post('post_content'),
				'status' 	=> $this->input->post('status'),
				'category_id' 	=> 6,
				'featured_image' => $featured_image_name
			);
		
			if( !$this->admin_model->news_insert( $this->table, $insert_data ) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'News created successfully');
				redirect('admin/news');
			}
		}*/
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('admin/add_gallery',isset($data) ? $data : NULL);
	}

		/* Add Featured Image Start Here */

			function do_upload1(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$name = $_FILES['featured_image']['name'];
			$file_ext = pathinfo($name, PATHINFO_EXTENSION);			
			$image_name = basename($name,".".$file_ext);
			$new_name = cleanFunc($image_name).".".$file_ext;			

			$config = array(
			'upload_path' => './uploads/media',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'file_name' => $new_name,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);		
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('featured_image')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }

			/* Add Featured Image End Here */	

	public function add_image() {	

 	if ( $this->input->post('action') == 1 )
      	{      		

      		$post_slug_title = trim( $this->input->post('post_title') );
      		$post_slug_simple = preg_replace("/[^ \w]+/", "", $post_slug_title);
      		$post_slug = str_replace(' ', '-', $post_slug_simple);
			$media_images = $this->do_upload1();
			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => strtolower($post_slug), 
								'category_id' => 23,
								'featured_image' 	=> 'uploads/media/'.$media_images,
								'status' => $this->input->post('status'),
								'page_template' => $this->input->post('page_template')
								
							);
			
			if( !$this->admin_model->image_insert( $this->table, $insert_data ) )
			{
				die('error in image insert');
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Image inserted successfully');
				redirect('admin/gallery');
			}
		}

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('admin/add_gallery',isset($data) ? $data : NULL);			
		
	}

	/*public function edit($value='')
	{
		# code..
		
		$news_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $news_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';

      		if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		

      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

      		//pr($_FILES); die();

      		if($_FILES['featured_image']['name'] == '') {

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		} else {

      			//pr( $this->do_upload() ); die();

      			$featured_image_name = $this->do_upload();

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'featured_image' 	=> $featured_image_name,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		}

			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'featured_image' 	=> $featured_image_name,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							);

			$this->db->where('id', $news_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/news');
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'News edited successfully');
				redirect('admin/news');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_news',isset($data) ? $data : NULL);

	}
	
	function do_upload(){

		$config = array(
			'upload_path' => './uploads/settings',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		$new_name = 'slider-'.time().$_FILES["featured_image"]['name'];
		$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('featured_image')) {

			$uploadData = $this->upload->data();
			return $picture = $uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }			
}
*/
public function delete()
	{
		
		$post_id = $this->uri->segment(5);	
		$this->admin_model->delete_news($post_id);
		$this->session->set_flashdata('error', 'Error in adding database.');
		redirect('admin/gallery');
	}
}

function cleanFunc($string) {
   			$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   			$string = preg_replace('/[^A-Za-z0-9-]/', '', $string); // Removes special chars.
   			return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}