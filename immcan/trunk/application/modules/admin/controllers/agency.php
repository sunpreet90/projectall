<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Agency extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';

		$data['agency'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 9 ) ); //category_id = 8 //our_team

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/agency',isset($data) ? $data : NULL);
	}
	
	public function add($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		

      		$featured_image_name = $this->do_upload();

			
			if($_FILES['featured_image']['name'] == '') {

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 9
				);

			} else {
				/*pr( $featured_image_name); die();*/

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 9,
					'featured_image' 	=> $featured_image_name
				);
			}

			
			if( !$insert_id = $this->admin_model->insert_agency( $this->table, $insert_data ) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				add_post_meta( $post_id = $insert_id, $meta_key = 'agency_button_label', $meta_value = $this->input->post('agency_button_label') );
				add_post_meta( $post_id = $insert_id, $meta_key = 'agency_button_link', $meta_value = $this->input->post('agency_button_link') );

				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Agency created successfully');
				redirect('admin/agency');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_agency',isset($data) ? $data : NULL);
	}

public function edit($value='')
	{
		# code..

		$agency_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $agency_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';


		if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		

      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		$insert_data = array();

      		//pr($_FILES); die();
      		//print_r($_FILES); die;
      		if(empty($_FILES['featured_image']['name'])) {

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		} else {

      			//pr( $this->do_upload() ); die();

      			$featured_image_name = $this->do_upload();

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'featured_image' 	=> $featured_image_name,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		}

			/*$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'featured_image' 	=> $featured_image_name,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							); */

			$this->db->where('id', $agency_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/agency');
			}
			else
			{
				add_post_meta( $post_id = $agency_id, $meta_key = 'agency_button_label', $meta_value = $this->input->post('agency_button_label') );
				add_post_meta( $post_id = $agency_id, $meta_key = 'agency_button_link', $meta_value = $this->input->post('agency_button_link') );
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'News edited successfully');
				redirect('admin/agency');
			}
		}
		
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_agency',isset($data) ? $data : NULL);

	}
	
	public function delete()
	{
		$agency_id = $this->uri->segment(5);
		

		$this->admin_model->delete_our_team($agency_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/agency');
	}


	function do_upload(){

		$config = array(
			'upload_path' => './uploads/our-team',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		$new_name = 'slider-'.time().$_FILES["featured_image"]['name'];
		$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('featured_image')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/our-team/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }


}
