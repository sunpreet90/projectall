<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Menu extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');

		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP

		$this->table = $this->prefix.'posts';
	}
	
	function index()
	{
		/*die('asd2');*/

        $data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

          if ($this->input->post('action')) {
				# code...
				if ($this->input->post('action') == 'about') {

					set_config_item('about_menu_title', $this->input->post('about_menu_title'));
					set_config_item('about_menu_link', $this->input->post('about_menu_link'));

					set_config_item('about_you_title', $this->input->post('about_you_title'));
					set_config_item('about_you_link', $this->input->post('about_you_link'));

					set_config_item('about_us_title', $this->input->post('about_us_title'));
					set_config_item('about_us_link', $this->input->post('about_us_link'));
				
				    set_config_item('our_team_title', $this->input->post('our_team_title'));
					set_config_item('our_team_link', $this->input->post('our_team_link'));
				
				    set_config_item('why_us_title', $this->input->post('why_us_title'));
					set_config_item('why_us_link', $this->input->post('why_us_link'));
				
	                set_config_item('can_help_title', $this->input->post('can_help_title'));
					set_config_item('can_help_link', $this->input->post('can_help_link'));

					$data['flash_status'] = 'success';
					$data['flash_title'] = 'Success!';
					$data['flash_message'] = 'About menu updated successfully!';
	        }
	        if ($this->input->post('action') == 'our_services') {

					set_config_item('services_menu_title', $this->input->post('services_menu_title'));
					set_config_item('services_menu_link', $this->input->post('services_menu_link'));

					set_config_item('pricing_title', $this->input->post('pricing_title'));
					set_config_item('pricing_link', $this->input->post('pricing_link'));
				
				    set_config_item('testimonials_title', $this->input->post('testimonials_title'));
					set_config_item('testimonials_link', $this->input->post('testimonials_link'));
					
				    set_config_item('our_services_title', $this->input->post('our_services_title'));
					set_config_item('our_services_link', $this->input->post('our_services_link'));

					$data['flash_status'] = 'success';
					$data['flash_title'] = 'Success!';
					$data['flash_message'] = 'Services menu updated successfully!';
	        }
	        if ($this->input->post('action') == 'referral_agent') {

					set_config_item('main_menu_title', $this->input->post('main_menu_title'));
					set_config_item('main_menu_link', $this->input->post('main_menu_link'));

					set_config_item('referral_title', $this->input->post('referral_title'));
					set_config_item('referral_link', $this->input->post('referral_link'));

					set_config_item('affiliates_title', $this->input->post('affiliates_title'));
					set_config_item('affiliates_link', $this->input->post('affiliates_link'));
				
				    set_config_item('agent_title', $this->input->post('agent_title'));
					set_config_item('agent_link', $this->input->post('agent_link'));
					
				    
					$data['flash_status'] = 'success';
					$data['flash_title'] = 'Success!';
					$data['flash_message'] = 'Referral/Agent menu updated successfully!';
	        }
	        if ($this->input->post('action') == 'faq') {

					set_config_item('faq_title', $this->input->post('faq_title'));
					set_config_item('faq_link', $this->input->post('faq_link'));
	        }
	        
	        if ($this->input->post('action') == 'get_started') {

					set_config_item('get_started_title', $this->input->post('get_started_title'));
					set_config_item('get_started_link', $this->input->post('get_started_link'));
	        }

	        if ($this->input->post('action') == 'book_now') {

					set_config_item('book_now_title', $this->input->post('book_now_title'));
					set_config_item('book_now_link', $this->input->post('book_now_link'));
	        }

	        if ($this->input->post('action') == 'contact_us') {

					set_config_item('contact_us_title', $this->input->post('contact_us_title'));
					set_config_item('contact_us_link', $this->input->post('contact_us_link'));
	        }

	        if ($this->input->post('action') == 'main_menus') {

				set_config_item('faq_title', $this->input->post('faq_title'));
				set_config_item('faq_link', $this->input->post('faq_link'));

				set_config_item('get_started_title', $this->input->post('get_started_title'));
				set_config_item('get_started_link', $this->input->post('get_started_link'));

				set_config_item('book_now_title', $this->input->post('book_now_title'));
				set_config_item('book_now_link', $this->input->post('book_now_link'));

				set_config_item('contact_us_title', $this->input->post('contact_us_title'));
				set_config_item('contact_us_link', $this->input->post('contact_us_link'));

				$data['flash_status'] = 'success';
				$data['flash_title'] = 'Success!';
				$data['flash_message'] = 'Main menu updated successfully!';
	        }
	    }
		/*($this->input->post('firstname'))?$this->input->post('firstname'):"";*/

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('menu',isset($data) ? $data : NULL);
	}
}
