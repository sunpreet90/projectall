<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Faq extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');

		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';
		
		//ws_client_referral
		
		$data['faq'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 11 ) ); //category_id = 4 //news

		$data['referrals'] = $this->db->select('*')->from('ws_client_referral')->where('referral_type', 'referral')->get()->result_array();

		//pr($data_arr); die();

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/faq',isset($data) ? $data : NULL);
	}

public function add($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
				'post_title' 	=> $this->input->post('post_title'), 
				'post_slug' 	=> $post_slug, 
				'post_content' 	=> $this->input->post('post_content'),
				'status' 	=> $this->input->post('status'),
				'category_id' 	=> 11
			);
		
			/*$insert_id =$this->admin_model->insert_faq( $this->table, $insert_data );*/

			if( !$this->admin_model->insert_faq( $this->table, $insert_data ) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'FAQ created successfully');
				redirect('admin/faq');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_faq',isset($data) ? $data : NULL);
	}


public function edit($value='')
	{
		# code..

		$faq_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $faq_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['staus']		= '';
		$data['message']	= '';

      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);

			$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							);

			$this->db->where('id', $faq_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/faq');
			}
			else
			{
				/*add_post_meta( $post_id = $services_id, $meta_key = 'services_icon', $meta_value = $this->input->post('services_icon'));*/
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'FAQ edited successfully');
				redirect('admin/faq');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_faq',isset($data) ? $data : NULL);

	}		
public function delete()
	{
		$faq_id = $this->uri->segment(5);
		

		$this->admin_model->delete_faq($faq_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/faq');
	}

}
