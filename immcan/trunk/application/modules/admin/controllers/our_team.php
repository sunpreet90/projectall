<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Our_team extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		if (!$this->tank_auth->get_username())
		{
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('admin_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function index()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['staus']		= '';
		$data['message']	= '';

		$data['our_team'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 8 ) ); //category_id = 8 //our_team
		$data['about_us'] 	= $this->admin_model->get_posts( $where_data = array('category_id' => 19 ) ); //category_id = 8 //our_team


		if ($this->input->post('action')) {
			

			if ($this->input->post('action') == 'about_footer') {

				set_config_item('about_footer_title', $this->input->post('about_footer_title'));				

				if ($this->input->post('about_footer_status')) {
					# code...
					set_config_item('about_footer_status', 1);
				} else {
					set_config_item('about_footer_status', 0);
				}

			    if($_FILES['about_footer_image']['name'] != '') {

                	set_config_item('about_footer_image', $this->do_upload1());
				}
			}


			if ($this->input->post('action') == 'how_we_work') {

				set_config_item('how_we_work_title', $this->input->post('how_we_work_title'));				
				set_config_item('how_we_work_content', $this->input->post('how_we_work_content'));				

				if ($this->input->post('how_we_work_status')) {
					# code...
					set_config_item('how_we_work_status', 1);
				} else {
					set_config_item('how_we_work_status', 0);
				}

			    if($_FILES['how_we_work_featured_img']['name'] != '') {

                	set_config_item('how_we_work_featured_img', $this->do_upload2());
				}
			}
			
			if ($this->input->post('action') == 'team_section') {

				set_config_item('team_title', $this->input->post('team_title'));
				set_config_item('team_tagline', $this->input->post('team_tagline'));
			}

			if ($this->input->post('action') == 'about_us_page') {
				
				set_config_item('about_us_page_title', $this->input->post('about_us_page_title'));
				set_config_item('about_us_content', $this->input->post('about_us_content'));
			}
			if ($this->input->post('action') == 'about_us_header') {
				
				set_config_item('about_us_header_title', $this->input->post('about_us_header_title'));
				set_config_item('about_us_header_content', $this->input->post('about_us_header_content'));

				 if($_FILES['about_us_header_image']['name'] != '') {

                	set_config_item('about_us_header_image', $this->do_upload3());
				}
			}
		}

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('admin/our_team',isset($data) ? $data : NULL);
	}


	function add($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		

      		$featured_image_name = $this->do_upload();

			
			if($_FILES['featured_image']['name'] == '') {

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 8
				);

			} else {
				/*pr( $featured_image_name); die();*/

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 8,
					'featured_image' 	=> $featured_image_name
				);
			}

			
			if( !$this->admin_model->insert_our_team( $this->table, $insert_data ) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'News created successfully');
				redirect('admin/our_team');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_our_team',isset($data) ? $data : NULL);
	}

	function add_about($value='')
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
      	
      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		

      		$featured_image_name = $this->do_upload1();

			
			if($_FILES['featured_image']['name'] == '') {

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 19
				);

			} else {
				/*pr( $featured_image_name); die();*/

				$insert_data = 	array(
					'post_title' 	=> $this->input->post('post_title'), 
					'post_slug' 	=> $post_slug, 
					'post_content' 	=> $this->input->post('post_content'),
					'status' 	=> $this->input->post('status'),
					'category_id' 	=> 19,
					'featured_image' 	=> $featured_image_name
				);
			}

			
			if( !$this->admin_model->insert_about_us( $this->table, $insert_data ) )
			{
				$data['staus']		= 'error';
				$data['message']	= 'Error on inserting data.';
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'About Us created successfully');
				redirect('admin/our_team');
			}
		}
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('add_about_us',isset($data) ? $data : NULL);
	}

	public function edit($value='')
	{
		# code..

		$our_team_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $our_team_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['status']		= '';
		$data['message']	= '';


		if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		

      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		$insert_data = array();

      		//pr($_FILES); die();
      		//print_r($_FILES); die;
      		if(empty($_FILES['featured_image']['name'])) {

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		} else {

      			//pr( $this->do_upload() ); die();

      			$featured_image_name = $this->do_upload();

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'featured_image' 	=> $featured_image_name,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		}

			/*$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'featured_image' 	=> $featured_image_name,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							); */

			$this->db->where('id', $our_team_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/our_team');
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'News edited successfully');
				redirect('admin/our_team');
			}
		}
		
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_our_team',isset($data) ? $data : NULL);

	}


	public function edit_about($value='')
	{
		# code..

		$about_us_id 		= $this->uri->segment(5);

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['page'] 		= $this->admin_model->get_posts ($where_data = array('id' => $about_us_id), $multi_record = FALSE);
      	
      	//pr($data);

		$data['status']		= '';
		$data['message']	= '';


		if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);
      		

      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
      		$insert_data = array();

      		//pr($_FILES); die();
      		//print_r($_FILES); die;
      		if(empty($_FILES['featured_image']['name'])) {

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		} else {

      			//pr( $this->do_upload() ); die();

      			$featured_image_name = $this->do_upload1();

      			$insert_data = 	array(
						'post_title' => $this->input->post('post_title'), 
						'post_slug' => $post_slug,
						'featured_image' 	=> $featured_image_name,
						'post_content' => $this->input->post('post_content'),
						'status' => $this->input->post('status')
					);
      		}

			/*$insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => $post_slug,
								'featured_image' 	=> $featured_image_name,
								'post_content' => $this->input->post('post_content'),
								'status' => $this->input->post('status')
							); */

			$this->db->where('id', $about_us_id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('admin/our_team');
			}
			else
			{
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'News edited successfully');
				redirect('admin/our_team');
			}
		}
		
		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');

		$this->template
		->set_layout('immcan')
		->build('edit_about_us',isset($data) ? $data : NULL);
	}



	public function delete()
	{
		$our_team_id = $this->uri->segment(5);
		

		$this->admin_model->delete_our_team($our_team_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/our_team');
	}


	public function about_delete()
	{
		$about_us_id = $this->uri->segment(5);
		//print_r($about_help_id); die;

		$this->admin_model->delete_about_help($about_us_id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('admin/our_team');
	}


	function do_upload(){

		$config = array(
			'upload_path' => './uploads/our-team',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		$new_name = 'slider-'.time().$_FILES["featured_image"]['name'];
		$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('featured_image')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/our-team/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }

    function do_upload1(){

		$config = array(
			'upload_path' => './uploads/our-team',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		//$new_name = 'slider-'.time().$_FILES["about_footer_image"]['name'];
		//$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('about_footer_image')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/our-team/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }

    function do_upload2(){

		$config = array(
			'upload_path' => './uploads/our-team',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			//'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);

		//$new_name = 'slider-'.time().$_FILES["about_footer_image"]['name'];
		//$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('how_we_work_featured_img')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/our-team/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }
    function do_upload3(){

		$config = array(
			'upload_path' => './uploads/our-team',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			//'encrypt_name' => TRUE,
			//'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);
        $this->load->library('upload', $config);
		$data = $this->upload->initialize($config);

		if($this->upload->do_upload('about_us_header_image')) {

			$uploadData = $this->upload->data();

			return $picture = 'uploads/our-team/'.$uploadData['file_name'];
		
		} else {

			$error = array('error' => $this->upload->display_errors());
			$this->load->view('file_view', $error);
		}
    }
}
