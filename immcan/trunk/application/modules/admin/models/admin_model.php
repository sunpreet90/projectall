
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function get_posts( $where_data = array(), $multi_record = TRUE) {	
		//$where_data 	= array('category_id' => 20 );	
		//$data = $this->findWhere( $this->table, $where_data, $multi_record);
		//print_r($data);die;
		return $this->findWhere( $this->table, $where_data, $multi_record);
	}

	function delete_page($services_id)
	{
		$where_data 	= array('id' => $services_id );
		$post 	= array( 'status' => 'trash' );
		return $this->updateWhere($this->table, $where_data, $post);
	}


	function get_services($services_id)
	{
		$where_data = array('id' => $services_id );

		return $this->findWhere($this->table, $where_data, $multi_record = FALSE);
	}

	function delete_services($services_id){
	$where_data 	= array('id' => $services_id );
		$post 	= array( 'status' => 'trash' );
		$this->db->delete($this->table, $where_data, $post);
	}

	function services_insert($table_name, $insert_array)
	{
		// Inserting in Table(students) of Database(college)
		
		if ($this->db->insert($table_name, $insert_array) ){
			
			$insert_id = $this->db->insert_id();
			return $insert_id;

		} else {
			return false;
		}
	}
	public function getrecords($table ="", $cond =""){
            if(!empty($table)){
               $this->db->select("*");
               $this->db->from($table);
               if(!empty($cond)){
               	$this->db->where($cond);
               }
               $this->db->limit(5);
               $get_query = $this->db->get();
               return $get_query->result();
           
            }else{
            	return FALSE;
            }
	}
	public function getmessages($table ="", $cond =""){
            if(!empty($table)){
               $this->db->select("*");
               $this->db->from($table);
               if(!empty($cond)){
               	$this->db->where($cond);
                //$this->db->limit(5);
               }
               $get_query = $this->db->get();
               return $get_query->result();
           
            }else{
            	return FALSE;
            }
	}
	function news_insert($table_name, $insert_array)
	{
		// Inserting in Table(students) of Database(college)
		
		if ($this->db->insert($table_name, $insert_array) ){
			
			$insert_id = $this->db->insert_id();
			return $insert_id;

		} else {
			return false;
		}
	}

	function delete_news($news_id){
		$where_data 	= array('id' => $news_id );
		$post 	= array( 'status' => 'trash' );

		if( $this->db->delete($this->table, $where_data, $post) ){
			return true;
		} else {
			false;
		}
	}
	
	function insert_fun_facts($table_name, $insert_array)
	{
		if($this->db->insert($table_name, $insert_array)){
			$insert_id = $this->db->insert_id();
			return $insert_id;
		}
		else{
			return false;
		}
	}

	function delete_fun_facts($fun_facts_id){
		$where_data = array('id' => $fun_facts_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}

	function slider_insert($table_name, $insert_array)
	{
		if($this->db->insert($table_name, $insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;
		}else{
			return false;
		}
	}
	function delete_slider($slider_id){
		$where_data = array('id' => $slider_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}
	function qualify_insert($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
	function delete_qualify($qualify_id){
		$where_data = array('id' => $qualify_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}

	function insert_our_team($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
	function insert_get_started($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}


	function insert_book_consultant($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}



	function delete_our_team($our_team_id){
		$where_data = array('id' => $our_team_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table,$where_data,$post);
	}
	function delete_get_started($get_started_id){
		$where_data = array('id' => $get_started_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table,$where_data,$post);
	}

	function delete_book_consultant($book_consultant_id){
		$where_data = array('id' => $book_consultant_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table,$where_data,$post);
	}

	function delete_pricing($get_pricing_id){
		$where_data = array('id' => $get_pricing_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table,$where_data,$post);
	}

function insert_about_us($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}

	function about_delete($about_us_id){
		$where_data = array('id' => $about_us_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table,$where_data,$post);
	}


	

	/* Show all News Listing Using Category ID Start Here  */

		function get_news_listing(){
		$this->db->select('*');
		$this->db->from('ws_posts');
		$this->db->where('category_id',6);
		return $this->db->get()->result();
		
	}


	/* Show all News Listing Using Category ID End Here  */

	/* Show all Services Listing Using Category ID Start Here  */

		function get_services_listing(){
		$this->db->select('*');
		$this->db->from('ws_posts');
		$this->db->where('category_id',4);
		return $this->db->get()->result();
		
	}


	/* Show all Services Listing Using Category ID End Here  */
	/* Show all Fun_Facts Listing Using Category ID Start Here  */

		function get_fun_facts_listing(){
		$this->db->select('*');
		$this->db->from('ws_posts');
		$this->db->where('category_id',5);
		return $this->db->get()->result();
		
	}


	/* Show all Fun_Facts Listing Using Category ID End Here  */
	/* Show all Qualify Listing Using Category ID Start Here  */

		function get_qualify_listing(){
		$this->db->select('*');
		$this->db->from('ws_posts');
		$this->db->where('category_id',7);
		return $this->db->get()->result();
		
	}
/* Show all Qualify Listing Using Category ID End Here  */
/*Delete agency page*/
function delete_agency($agency_id){
		$where_data = array('id' => $agency_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}

/*Insert agency page*/
function insert_agency($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
	/*Insert serve industries page*/
function insert_serve_industries($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
/*Delete serve industries page*/
function delete_serve_industries($serve_industries_id){
		$where_data = array('id' => $serve_industries_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}

/*Insert FAQ page*/
function insert_faq($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
/*Delete FAQ page*/
function delete_faq($faq_id){
		$where_data = array('id' => $faq_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}

/*function insert_post_service($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}*/

/*Start Delete Services_section page*/
function delete_post_service($post_services_id){
		$where_data = array('id' => $post_services_id );
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}
/*End Delete Services_section page*/

/*Start Delete Range_Services_section page*/
function delete_range_service($range_services_id){
		$where_data = array('id' => $range_services_id );
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}
/*End Delete Range_Services_section page*/

/*Started Insert Immigration Application page*/
function insert_immigration($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
/*End Insert Immigration Application  page*/

/*Start Delete Immigration Application page*/
function delete_immigration($immigration_id){
		$where_data = array('id' => $immigration_id );
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}
/*End Delete Immigration Application page*/

/*Started Insert About You page*/
function insert_about_you($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
/*End Insert About You  page*/

/*Start Delete About You page*/
function delete_about_you($about_you_id){
		$where_data = array('id' => $about_you_id );
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}
/*End Delete About You page*/

/*Started Insert Referral page*/
function insert_referral($table_name,$insert_array)
	{
		if($this->db->insert($table_name,$insert_array))
		{
			$insert_id = $this->db->insert_id();
			return $insert_id;

		}else{
			return false;
		}
	}
/*End Insert Referral   page*/

/*Start Delete Referral  page*/
function delete_referral($referral_id){
		$where_data = array('id' => $referral_id);
		$post = array('status' => 'trash');
		$this->db->delete($this->table, $where_data, $post);
	}
/*End Delete Referral  page*/
/*Start Insert Gallery image*/
function image_insert($table_name, $insert_array)
	{
		// Inserting in Table(students) of Database(college)
		
		if ($this->db->insert($table_name, $insert_array) ){
			
			$insert_id = $this->db->insert_id();
			return $insert_id;

		} else {
			return false;
		}
	}
/*End Insert Gallery image*/

}