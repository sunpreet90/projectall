    <div class="page-heading">
        <h3>About You</h3>
        <ul class="breadcrumb">
            <li><a href="<?= site_url('welcome')?>">Dashboard</a></li>
            <li><a href="#">About You</a></li>
        </ul>
    </div>
    <!--Start About Us Section -->
    
    <div class="wrapper">

        <!-- About page title/desc -->
        <div class="row">
            <div class="col-sm-11">
                <section class="panel">
                
                    <header class="panel-heading">
                        About You Header Section
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                         </span>
                    </header>

                    <div style="display: block;" class="panel-body">
                        <div class="panel-body">
                            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/about_you') ?>" novalidate="novalidate" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Page Title:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control round-input" name="about_you_page_title" value="<?=get_config_item('about_you_page_title')?>">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-sm-3 control-label">Content:</label>
                                    <div class="col-sm-9">
                                        <textarea name='about_you_content' class="form-control ckeditor" rows="9"><?=get_config_item('about_you_content')?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Header Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="about_header_image" class="form-control round-input" >   
                            </div>
                        </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Header Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('about_header_image') != '') { ?>
                                    <img class="about_header_image" src="<?= site_url().get_config_item('about_header_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="action" value="about_you_page">
                                        <button class="btn btn-primary text1" type="submit">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>    
                    </div>
                
                </section>
            </div>
        </div>
        <!-- /About page title/desc -->


        <!-- Out team page section -->
        <div class="row">
            <div class="col-sm-11">
                <section class="panel">
                
                    <header class="panel-heading">
                       How We Help Section
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-up"></a>
                         </span>
                    </header>
                
                    <div style="display: none;" class="panel-body">
                        <div class="panel-body">
                            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/about_you') ?>">

                                 <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Page Title:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control round-input" name="about_help_title" value="<?=get_config_item('about_help_title')?>">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-sm-3 control-label">Content:</label>
                                    <div class="col-sm-9">
                                        <textarea name='about_help_tagline' class="form-control ckeditor" rows="9"><?=get_config_item('about_help_tagline')?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="action" value="about_help_section">
                                        <button class="btn btn-primary text1" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <hr>

                        <div class="panel-body">
                            <a href="<?=base_url('admin/about_you/add')?>"><button class="btn btn-primary" type="button">Add Our Team</button></a>
                        </div>

                        <div class="panel-body">
                             <?php if($this->session->flashdata('response_status')): ?> 
                                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <h4>
                                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                                            <p><?= $this->session->flashdata('message');?></p>
                                        </h4>
                                    </div>
                                <?php endif; ?>
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Created On</th>
                                        <th>About You Image</th>
                                        <th class="hidden-phone">Edit</th>
                                        <th class="hidden-phone">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php 
                                        if ( !empty( $about_you )) 
                                        {
                                            foreach ($about_you as $note) 
                                            {
                                            ?>
                                            <tr class="gradeX">
                                                <td><?=$note['post_title']?></td>
                                                <td class="text-capitalize"><?=$note['status']?></td>
                                                <td><?=$note['created_on']?></td>
                                                <td><?=$note['featured_image']?>
                                                <td class="center hidden-phone"><a href="<?=base_url('admin/about_you')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                                <td class="center hidden-phone">
                                                <a href="<?=base_url('admin/about_you')?>/delete/id/<?=$note['id'];?>">
                                                    <i class="fa fa-eraser"></i>
                                                </a>
                                                </td>
                                            </tr>
                                            <?php }
                                        }
                                        ?>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Created On</th>
                                        <th>About You Image</th>
                              
                                        <th class="hidden-phone">Edit</th>
                                     
                                        <th class="hidden-phone">Delete</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /our team Section -->



        <!--  -->
        <div class="row">
        <div class="col-sm-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           View Footer Section
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/about_you') ?>" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show About Footer:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('about_you_footer_status') == 1) { ?>

                                    <input type="checkbox" name="about_you_footer_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="about_you_footer_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="about_you_footer_title" value="<?=get_config_item('about_you_footer_title')?>">
                            </div>
                        </div>

                                                
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Footer Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="about_you_footer_image" class="form-control round-input" >   
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Footer Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('about_you_footer_image') != '') { ?>
                                    <img class="about_you_footer_image" src="<?= site_url().get_config_item('about_you_footer_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>

                       

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="about_you_footer">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

    </div>
</div>
        <!--  -->