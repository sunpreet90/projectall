<!-- page heading start-->
<div class="page-heading">
    <h3>Settings</h3>

    <ul class="breadcrumb">
        <li><a href="<?=base_url('welcome')?>"> Dashboard</a></li>
        <li class="active"> Settings </li>
    </ul>

</div>

<div class="container">
<!--Admin Email-->
 <div class="col-md-11">

        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
                Admin Email
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                </span>
            </header>
            
            <div style="display: block;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/setting') ?>" novalidate="novalidate" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Admin Email:</label>
                            <div class="col-sm-10">
                                <input type="text" name="admin_email" class="form-control round-input" value="<?=get_config_item('admin_email')?>">
                            </div>
                        </div>
                    
                        <input type="hidden" name="action" value="email">
                        <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </section>
        <!--pagination end-->
    </div>




<!--Admin Email-->


    <div class="col-md-11">

        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
                Header
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>
            
            <div style="display: none;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/setting') ?>" novalidate="novalidate" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Site Title:</label>
                            <div class="col-sm-10">
                                <input type="text" name="site_title" class="form-control round-input" value="<?=get_config_item('site_title')?>">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Base URL:</label>
                            <div class="col-sm-10">
                                <input type="text" name="base_url" value="<?=get_config_item('base_url')?>" class="form-control round-input">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Logo Image:</label>
                            <div class="col-sm-10">
                                <input type="file" name="website_logo" class="form-control round-input" >   
                            </div>
                        </div>


                        <?php if (get_config_item('website_logo') != '') { ?>
                            <img class="website_logo" src="<?php echo site_url() ?>/uploads/settings/<?=get_config_item('website_logo')?>">
                        <?php } else {
                            echo "<i>No logo selected...</i>";
                        } ?>


                        <input type="hidden" name="action" value="header">
                        <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                    </form>

                </div>
            </div>
        </section>
        <!--pagination end-->
    </div>
   
    <!-- <div class="col-md-11"> -->
    <div class="col-md-6">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
               Footer Widgets - Twitter
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/setting') ?>" novalidate="novalidate" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Twitter Title:</label>
                        <div class="col-sm-10">
                            <input type="text" name="twitter_title" value="<?=get_config_item('twitter_title')?>" class="form-control round-input" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">IRRCI Link:</label>
                        <div class="col-sm-10">
                            <input type="text" name="iccrc_link" value="<?=get_config_item('iccrc_link')?>" class="form-control round-input" >
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Twitter Content:</label>
                        <div class="col-sm-10">
                            <input type="text" name="twitter_content" value="<?=get_config_item('twitter_content')?>" class="form-control round-input" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Footer Image:</label>
                        <div class="col-sm-10">
                            <input type="file" name="footer_image" class="form-control round-input" >   
                        </div>
                    </div>


                    <?php if (get_config_item('footer_image') != '') { ?>
                        <img class="footer_image" src="<?php echo site_url() ?>/uploads/settings/<?=get_config_item('footer_image')?>">
                    <?php } else {
                        echo "<i>No image selected...</i>";
                    } ?>
                     <input type="hidden" name="action" value="twitter_footer">
                    <button type="submit" value="upload" class="btn btn-primary type">Submit</button>
                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>

    <div class="col-md-5">
        <!--pagination start-->

        <section class="panel">
            <header class="panel-heading">
               Footer Widgets - Contact Us
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>

            <div style="display: none;" class="panel-body">
                <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/setting') ?>" novalidate="novalidate" >
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Title:</label>
                        <div class="col-sm-10">
                            <input type="text" name="footer_widget_contact_title" value="<?=get_config_item('footer_widget_contact_title')?>" class="form-control round-input" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Content:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control ckeditor" name="footer_widget_contact_content" rows="6"><?=get_config_item('footer_widget_contact_content')?></textarea>
                        </div>
                    </div>
                    
                     <input type="hidden" name="action" value="contact_footer">
                    <button type="submit" value="upload" class="btn btn-primary type">Submit</button>
                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>

    <div class="col-md-11">        
        <section class="panel">
            <header class="panel-heading">
                Footer Widgets - Link Categories
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                </span>
            </header>
            <div style="display: none;" class="panel-body">
               <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>

                <div class="form">
                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/setting') ?>" novalidate="novalidate">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Link Title:</label>
                            <div class="col-sm-10">
                                <input type="text" name="footer_widget_category_title" value="<?=get_config_item('footer_widget_category_title')?>" class="form-control round-input" >
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Link Content:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control ckeditor" name="footer_widget_category_content" rows="6"><?=get_config_item('footer_widget_category_content')?></textarea>
                            </div>
                        </div>
                         <input type="hidden" name="action" value="link_footer">
                        <button type="submit" value="upload" class="btn btn-primary type">Submit</button>
                    </form>
                </div>
            </div>
        </section>
    </div>

    <!--start soical -->
    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            
            <header class="panel-heading">
                Footer Widgets - Social Settings
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>
            
            <div style="display: none;" class="panel-body">
                <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/setting') ?>" novalidate="novalidate" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Social Title:</label>
                        <div class="col-sm-10 text">
                            <input type="text" name="social_title" value="<?=get_config_item('social_title')?>" class="form-control round-input" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Social Content:</label>
                        <div class="col-sm-10 text">
                            <input type="text" name="social_content" value="<?=get_config_item('social_content')?>" class="form-control round-input" >
                        </div>
                    </div>
                    
                    <hr>

                    <!-- ########## -->

                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="facebook_link" class="form-control round-input" value="<?=get_config_item('facebook_link')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="facebook_icon" class="form-control round-input" value="<?=get_config_item('facebook_icon')?>">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="instagram_link" class="form-control round-input" value="<?=get_config_item('instagram_link')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="instagram_icon" class="form-control round-input" value="<?=get_config_item('instagram_icon')?>">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="twitter_link" class="form-control round-input" value="<?=get_config_item('twitter_link')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="twitter_icon" class="form-control round-input" value="<?=get_config_item('twitter_icon')?>">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="google_link" class="form-control round-input" value="<?=get_config_item('google_link')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="google_icon" class="form-control round-input" value="<?=get_config_item('google_icon')?>">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Name:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="pinterest_link" class="form-control round-input" value="<?=get_config_item('pinterest_link')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Social Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="pinterest_icon" class="form-control round-input" value="<?=get_config_item('pinterest_icon')?>">
                            </div>
                        </div>
                    </div>
                    <!-- ########## -->
                    <hr>
                    <div class="row text-right">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="action" value="social">
                            <button type="submit" value="upload" class="btn btn-primary type float-right">Save social settings</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>


    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
               Footer CopyRight
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/setting') ?>" novalidate="novalidate" >
                    <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">CopyRight Content:</label>
                        <div class="col-sm-10 text">
                            <input type="text" name="copyright_content" value="<?=get_config_item('copyright_content')?>" class="form-control round-input" >
                        </div>
                    </div>
                     <input type="hidden" name="action" value="footer">
                    <button type="submit" value="upload" class="btn btn-primary type">Submit</button>
                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>

</div>

<!-- page heading end