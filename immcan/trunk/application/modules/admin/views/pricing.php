<div class="page-heading">
            <h3>
               Details Listing
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?= site_url('welcome')?>">Dashboard</a>
                </li>
                <li>
                    <a href="#">Pricing</a>
                </li>
                <li class="active"> View All List </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">

        <div class="row">
        <div class="col-sm-12">
        <section class="panel">  
        <header class="panel-heading">
            Pricing Table


            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!-- <a href="javascript:;" class="fa fa-times"></a>
                <a href="javascript:;" class="fa fa-eye"></a> -->
             </span>
        </header>
        <!--Header Section-->
        <div class="panel-body">
                 <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/pricing') ?>" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="pricing_banner_title" value="<?=get_config_item('pricing_banner_title')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Tagline:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="pricing_banner_tagline" value="<?=get_config_item('pricing_banner_tagline')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Pricing Header Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="pricing_header_image" class="form-control round-input" >   
                            </div>
                        </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Get Started Header Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('pricing_header_image') != '') { ?>
                                    <img class="pricing_header_image" src="<?= site_url().get_config_item('pricing_header_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Pricing Page Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="pricing_page_title" value="<?=get_config_item('pricing_page_title')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Pricing Page Tagline:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="pricing_page_tagline" value="<?=get_config_item('pricing_page_tagline')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="pricing">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
        <!--End Header Section-->
        
        <div class="panel-body">
                <a href="<?=base_url('admin/pricing/add')?>"><button class="btn btn-primary" type="button">Add Pricing</button></a>
        </div>

        <div class="panel-body">
             <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Status</th>
            <th>Created On</th>
            <!-- <th>Our Team Image</th> -->
         <!--    <th>Services Icon</th> -->
            <th class="hidden-phone">Edit</th>
           <!--  <th class="hidden-phone">View</th> -->
            <th class="hidden-phone">Delete</th>
            
        </tr>
    </thead>
         <tbody>
             <?php 
                if ( !empty( $pricing )) 
                {
                    foreach ($pricing as $note) 
                    {

                    ?>
                    <tr class="gradeX">
                        <td><?=$note['post_title']?></td>
                        <td class="text-capitalize"><?=$note['status']?></td>
                        <td><?=$note['created_on']?></td>
                        <!-- <td><?=$note['featured_image']?> -->
                    <!--     <td><?=$note['services_icon']?></td> -->
                        <td class="center hidden-phone"><a href="<?=base_url('admin/pricing')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                        <!-- <td class="center hidden-phone"><a href="<?=base_url('admin/pricing')?>/view/id/<?=$note['id'];?>">View</a></td> -->
                        <td class="center hidden-phone">
                        <a href="<?=base_url('admin/pricing')?>/delete/id/<?=$note['id'];?>">
                            <i class="fa fa-eraser"></i>
                        </a>
                        </td>
                    </tr>
                    <?php }
                }
                ?>
            </tbody>

                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <!-- <th>Our Team Image</th> -->
                     <!--    <th>Services Icon </th> -->
                        <th class="hidden-phone">Edit</th>
                     <!--    <th class="hidden-phone">View</th> -->
                        <th class="hidden-phone">Delete</th>
                    </tr>
                    </tfoot>
        </table>
        </div>
        </div>
        </section>
        </div>
    </div>
</div>

