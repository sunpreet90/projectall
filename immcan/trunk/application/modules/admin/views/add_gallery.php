<!-- page heading start-->
        <div class="page-heading">
            <h3>
              Media Gallery
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="#">Media Gallery</a>
                </li>
                <li class="active">Add Media Gallery</li>
            </ul>
        </div>
        <!-- page heading end-->

        <section class="panel">
            
            <header class="panel-heading">
                Create a new page
            </header>

            <div class="panel-body">

                <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>

                <div class="form">
                    <form class="form-horizontal adminex-form" method="post" action="<?php echo base_url('admin/gallery/add_image')?>"  enctype='multipart/form-data'>
                        <!-- <div class="form-group ">
                            <label for="title" class="control-label col-lg-2">Name</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_name" class=" form-control" id="post_name">
                            </div>
                        </div> -->
                        <div class="form-group ">
                            <label for="title" class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_title" class=" form-control" id="post_title" required>
                            </div>
                        </div>
                         <!-- <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Content</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">                                        
                                        <textarea name='post_content' class="form-control ckeditor" rows="9"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Status</label>
                            <div class="col-lg-10">
                               <select name="status" class="form-control m-bot15">
                                        <option value="published">Publish</option>
                                        <option value="pending">Pending</option>
                                        <option value="trash">Trash</option>
                                    </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Media Image:</label>
                            <div class="col-sm-10">
                                <input type="file" name="featured_image" class="form-control" id="featured_image"  required>  
                                 
                            </div>
                        </div>
                       <!--  <div class="form-group ">
                            <label for="title" class="control-label col-lg-2">Services Icon</label>
                            <div class="col-lg-10">
                                <input type="text" name="services_icon" class=" form-control" id="services_icon">
                            </div>
                        </div> -->

                        <!-- <div class="form-group ">
                            <label for="featured_image" class="control-label col-lg-2">Featured Image</label>
                            <div class="col-lg-10">
                                <input class="form-control " id="featured_image" name="featured_image" type="file" onchange="ValidateSingleInput(this);">
                            </div>
                        </div> -->

                       <!-- <div class="form-group ">
                            <label for="confirm_password" class="control-label col-lg-2">Confirm Password</label>
                            <div class="col-lg-10">
                                <input class="form-control " id="confirm_password" name="confirm_password" type="password">
                            </div>
                        </div>
                      
                     
                        <div class="form-group ">
                            <label for="agree" class="control-label col-lg-2 col-sm-3">Agree to Our Policy</label>
                            <div class="col-lg-10 col-sm-9">
                                <input type="checkbox" style="width: 20px" class="checkbox form-control" id="agree" name="agree">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="newsletter" class="control-label col-lg-2 col-sm-3">Receive the Newsletter</label>
                            <div class="col-lg-10 col-sm-9">
                                <input type="checkbox" style="width: 20px" class="checkbox form-control" id="newsletter" name="newsletter">
                            </div>
                        </div> -->

                        <input type="hidden" name="action" value="1">

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Save Change</button>
                                <a href="<?=base_url('admin/gallery')?>"><button class="btn btn-default" type="button">Cancel</button></a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>