<div class="page-heading">
    <h3>Details Listing</h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?= site_url('welcome')?>">Dashboard</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li class="active"> View All List </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">

        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
            Services &nbsp;&nbsp; 
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
               
             </span>

            <a href="<?=base_url('admin/services/add')?>"><button class="btn btn-info" type="button">Add New</button></a>

            </header>

        <div class="panel-body">
             <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
        <tr>
            <th>Title</th>
            <th>Status</th>
            <th>Created On</th>
            <th>Services Icon</th>
            <th class="hidden-phone">Edit</th>
           <!--  <th class="hidden-phone">View</th> -->
            <th class="hidden-phone">Delete</th>
            
        </tr>
    </thead>
         <tbody>
             <?php 
                if ( !empty( $services )) 
                {
                    foreach ($services as $note) 
                    {

                    ?>
                    <tr class="gradeX">
                        <td><?=$note['post_title']?></td>
                        <td class="text-capitalize"><?=$note['status']?></td>
                        <td><?=$note['created_on']?></td>

                         <td><?=get_post_metadata( $note['id'], $meta_key='services_icon' )?></td> 

                        <td class="center hidden-phone"><a href="<?=base_url('admin/services')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                       <!--  <td class="center hidden-phone"><a href="<?=base_url('admin/services')?>/view/id/<?=$note['id'];?>">View</a></td> -->
                        <td class="center hidden-phone">
                        <a href="<?=base_url('admin/services')?>/delete/id/<?=$note['id'];?>">
                            <i class="fa fa-eraser"></i>
                        </a>
                        </td>
                    </tr>
                    <?php }
                }
                ?>
            </tbody>

                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th>Services Icon</th>
                        <th class="hidden-phone">Edit</th>
                       <!--  <th class="hidden-phone">View</th> -->
                        <th class="hidden-phone">Delete</th>
                    </tr>
                    </tfoot>
        </table>
        </div>
        </div>
        </section>
        </div>
    </div>
<!--Start Pop Services Immigrations Applications-->
