
        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Media Gallery
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li class="active"> Media Gallery </li>
            </ul>
        </div>
        <!-- page heading end-->

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">

                

        <section class="panel">
            
             <header class="panel-heading">
                            Media Manager
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>                                
                             </span>
                        </header>

            <form id="filterform1" class="hide" action="#" method="post" role="">
                <div class="col-md-2 col-sm-3">
                    <label for="usr">Filter by status</label>
                    <select class="form-control category_select">
                        <option value="all">All</option>
                        <option value="published">Published</option>
                        <option value="pending">Pending</option>
                        <option value="trash">Trash</option>
                    </select>
            </div>
            
    
        <div class="clearfix"></div>
      </form>
        
       

        <div class="panel-body">
            
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Created On</th>
                                            <th>URL</th>            
                                            <th class="hidden-phone">Delete</th>            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        
                                        if ( is_array( $gallery )) 
                                        {
                                            $srno = 1;
                                            foreach ($gallery as $media) 
                                            {
                                              
                                            ?>
                                            <tr class="gradeX">
                                                <td><?=$srno;?></td>
                                                <td><?=$media['post_title']?></td>
                                                <td><img src="<?=base_url($media['featured_image'])?>" alt=""  width="150px" height="150px"/></td>
                                                <td><?=$media['created_on']?></td>         
                                                <td><input type="hidden" value="<?=base_url($media['featured_image'])?>" id="p1"><button class="btn btn-success btn-sm" onclick="setClipboard('<?=base_url($media['featured_image'])?>')" type="button">COPY URL</button></td>             
                                                <td class="center hidden-phone"><a href="<?=base_url('admin/gallery/delete/id/')."/".$media['id']?>" onclick="if(!confirm('Are you sure want to delete this image?')){return false;}"><i class="fa fa-eraser"></i>
                                                </a></td>
                                            </tr>
                                            <?php $srno++;  }} ?>
                                    </tbody>
                                    
                                </table>
        </div>
        </div>
        </section>
        </div>
        </div>

<script type="text/javascript">
function setClipboard(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}
 </script>



