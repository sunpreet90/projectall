<div class="page-heading">
    <h3>FAQ's Listing</h3>
    <ul class="breadcrumb">
        <li>
            <a href="<?= site_url('welcome')?>">Dashboard</a>
        </li>
        <li>
            <a href="#">FAQ</a>
        </li>
        <li class="active"> View All List </li>
    </ul>
</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                   FAQ Table
                <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                       <!--  <a href="javascript:;" class="fa fa-times"></a>
                        <a href="javascript:;" class="fa fa-eye"></a> -->
                     </span>
                </header>
                <div class="panel-body">
                    <a href="<?=base_url('admin/faq/add')?>"><button class="btn btn-primary" type="button">Add FAQ</button></a>
                </div>

            <div class="panel-body">
                <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>

            <div class="adv-table">
            
                <table  class="display table table-bordered table-striped" id="dynamic-table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Created On</th>
                            <th>Edit</th>
                            <th>View</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
            
                    <tbody>
                     <?php 
                        if ( !empty( $faq )) 
                        {
                            foreach ($faq as $note) 
                            {

                            ?>
                            <tr class="gradeX">
                                <td><?=$note['post_title']?></td>
                                <td class="text-capitalize"><?=$note['status']?></td>
                                <td><?=$note['created_on']?></td>

                               <!--   <td><?=get_post_metadata( $note['id'], $meta_key='services_icon' )?></td>  -->

                                <td class="center hidden-phone"><a href="<?=base_url('admin/faq')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                <td class="center hidden-phone"><a href="<?=base_url('faq')?>/121">View</a></td> 
                                <td class="center hidden-phone">
                                    <a href="<?=base_url('admin/faq')?>/delete/id/<?=$note['id'];?>">
                                        <i class="fa fa-eraser"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php }
                        }
                        ?>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Created On</th>
                            <th class="hidden-phone">Edit</th>
                            <th class="hidden-phone">View</th>
                            <th class="hidden-phone">Delete</th>
                        </tr>
                    </tfoot>

                </table>

            </div>

            </div>
        </section>
    </div>
</div>
</div>