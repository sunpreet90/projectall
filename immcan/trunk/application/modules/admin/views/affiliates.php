    <div class="page-heading">
        <h3>Referral Affiliates</h3>
        <ul class="breadcrumb">
            <li><a href="<?= site_url('welcome')?>">Dashboard</a></li>
            <li><a href="#">Referral Affiliates</a></li>
        </ul>
    </div>
    <!--Start About Us Section -->
    
    <div class="wrapper">

        <!-- About page title/desc -->
        <div class="row">
            <div class="col-sm-11">
                <section class="panel">
                
                    <header class="panel-heading">
                        Referral Header Section
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                         </span>
                    </header>

                    <div style="display: block;" class="panel-body">
                        <div class="panel-body">
                            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/affiliates') ?>" novalidate="novalidate" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Referral Affiliates Title:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control round-input" name="referral_affiliates_title" value="<?=get_config_item('referral_affiliates_title')?>">
                                    </div>
                                </div>
                               
                                <div class="form-group ">
                                    <label class="col-sm-3 col-sm-3 control-label">Referral Affiliates Content:</label>
                                    <div class="col-sm-9">
                                        <textarea name='referral_affiliates_content' class="form-control ckeditor" rows="9"><?=get_config_item('referral_affiliates_content')?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Affiliates Header Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="referral_affiliates_header_image" class="form-control round-input">   
                            </div>
                        </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Affiliates Header Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('referral_affiliates_header_image') != '') { ?>
                                    <img class="referral_affiliates_header_image" src="<?= site_url().get_config_item('referral_affiliates_header_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="action" value="referral_affiliates">
                                        <button class="btn btn-primary text1" type="submit">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>    
                    </div>
                
                </section>
            </div>
        </div>
        <!-- /About page title/desc -->


        <!-- Out team page section -->
        <!-- <div class="row">
            <div class="col-sm-11">
                <section class="panel">
                
                    <header class="panel-heading">
                       Referral Sidebar Section
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-up"></a>
                         </span>
                    </header>
                
                    <div style="display: none;" class="panel-body">
                        <div class="panel-body">
                            <a href="<?=base_url('admin/referral/add')?>"><button class="btn btn-primary" type="button">Add Referral</button></a>
                        </div>

                        <div class="panel-body">
                             <?php if($this->session->flashdata('response_status')): ?> 
                                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <h4>
                                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                                            <p><?= $this->session->flashdata('message');?></p>
                                        </h4>
                                    </div> -->
                                <?php endif; ?>
                            <<!-- div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Created On</th>
                                        <th>Referral Image</th>
                                        <th class="hidden-phone">Edit</th>
                                        <th class="hidden-phone">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php 
                                        if ( !empty( $referral )) 
                                        {
                                            foreach ($referral as $note) 
                                            {
                                            ?>
                                            <tr class="gradeX">
                                                <td><?=$note['post_title']?></td>
                                                <td class="text-capitalize"><?=$note['status']?></td>
                                                <td><?=$note['created_on']?></td>
                                                <td><?=$note['featured_image']?>
                                                <td class="center hidden-phone"><a href="<?=base_url('admin/referral')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                                <td class="center hidden-phone">
                                                <a href="<?=base_url('admin/referral')?>/delete/id/<?=$note['id'];?>">
                                                    <i class="fa fa-eraser"></i>
                                                </a>
                                                </td>
                                            </tr>
                                            <?php }
                                        }
                                        ?>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Created On</th>
                                        <th>Referral Image</th>
                                        <th class="hidden-phone">Edit</th>
                                        <th class="hidden-phone">Delete</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div> -->
    <!-- /our team Section -->



    

    