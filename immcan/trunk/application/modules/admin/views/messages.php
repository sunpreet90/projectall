<div class="page-heading">
            <h3>
               Details Listing
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?= site_url()?>">Dashboard</a>
                </li>
                <li>
                    <a href="#">Pages</a>
                </li>
                <li class="active"> View All List </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">

        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
            
            <header class="panel-heading">
                Default Buttons
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                    <a class="fa fa-times" href="javascript:;"></a>
                </span>
            </header>
            <div class="panel-body">
                
                
                <button class="btn btn-primary" type="button">All Messages</button>
                <!--<button class="btn btn-success" type="button">Unread</button>
                <button class="btn btn-warning" type="button">Trash</button>
                <button class="btn btn-info" type="button">Pending</button> -->
                
            </div>
        
        <header class="panel-heading">
            Messages
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
                <a href="javascript:;" class="fa fa-eye"></a>
             </span>
        </header>

        <div class="panel-body">
             <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
        <tr>
            <th>Title</th>
            <th>Status</th>
            <th>Created On</th>
            <!-- <th class="hidden-phone">Edit</th> -->
            <th class="hidden-phone">View</th>
            <th class="hidden-phone">Delete</th>
            
        </tr>
    </thead>
         <tbody>

             <?php
             //echo "<pre>"; print_r($notifications); die; 
             /* View all notifications */
                if ( !empty( $messages )) 
                {
                    foreach ($messages as $message) 
                    {
                        //pr($message);
                        //pr($notification);
                        if($message->status == 1){
                            $status = "<b>Unread</b>";
                        }else{
                            $status = "Read";
                        }

                    ?>
                    <tr class="gradeX">
                        <td><?=$message->first_name ?></td>
                        <td class="text-capitalize"><?= $status ?></td>
                        <td><?=$message->created_at ?></td>
                        <!-- <td class="center hidden-phone"><a href="<?=base_url('page')?>/edit/id/<?= $notification->id ?>">Edit</a></td> -->
                        <td class="center hidden-phone"><a href="<?=base_url('admin/message')?>/view/<?= $message->id ?>">View</a></td>
                        <td class="center hidden-phone">
                        <a href="<?=base_url('admin/message')?>/delete/id/<?= $message->id ?>" onclick="if(!confirm('Are you sure to delete this record?')){return false;}">
                            <i class="fa fa-eraser"></i>
                        </a>
                        </td>
                    </tr>
                    <?php }
                }
                /* // View all notifications Ends here*/
                ?>
            </tbody>

                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <!-- <th class="hidden-phone">Edit</th> -->
                        <th class="hidden-phone">View</th>
                        <th class="hidden-phone">Delete</th>
                    </tr>
                    </tfoot>
        </table>
        </div>
        </div>
        </section>
        </div>
        </div>