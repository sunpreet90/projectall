 <!--ios7-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/js/ios-switch/switchery.css" />
<link href="<?php echo base_url() ?>assets/js/iCheck/skins/flat/blue.css" rel="stylesheet">

  

    <div class="page-heading">
    <h3>
      Home Page Setting
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="<?=base_url('welcome')?>">Dashboard</a>
        </li>
        <li class="active"> Home Page Setting </li>
    </ul>
 
</div>

<div class="container">
    <div class="col-md-11">

        <section class="panel">
            <header class="panel-heading">
           Manage home page slider
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: block;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="form-group">
                            <div class="col-sm-5">
                                <a href="<?=base_url('admin/slider')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage Slider</button></a>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           View All Services
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show all our services on front page:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('front_page_services_section_status') == 1) { ?>

                                    <input type="checkbox" name="our_services_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="our_services_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="main_title" value="<?=get_config_item('main_title')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Tagline</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="tagline" value="<?=get_config_item('tagline')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <a href="<?=base_url('admin/services')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage All Services</button></a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="our_services">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

    </div>
</div>

<div class="container">

    <div class="col-md-11">

    <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
        Qualify
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>

                    <!-- <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
                <div style="display: none;" class="panel-body">
                                
                    <div class="panel-body">
                        <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Show All Qualify:</label>

                                <div class="col-sm-7">

                                    <?php if (get_config_item('qualify_status') == 1) { ?>

                                        <input type="checkbox" name="qualify_status" class="js-switch" checked />
                                    
                                    <?php } else { ?>
                                        
                                        <input type="checkbox" name="qualify_status" class="js-switch"/>
                                    
                                    <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                 <label class="col-sm-4 col-sm-4 control-label">Main Title</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control round-input" name="qualify_title" value="<?=get_config_item('qualify_title')?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Name:</label>
                                <div class="col-md-7">
                                    <input type="text" name="qualify_menu_page_title" value="<?=get_config_item('qualify_menu_page_title')?>" class="form-control round-input" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Link:</label>
                                 <div class="col-md-7">
                                <input type="text" name="qualify_menu_page_link" class="form-control round-input" value="<?=get_config_item('qualify_menu_page_link')?>">
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label"></label>
                                <div class="col-sm-7">
                                    <a href="<?=base_url('admin/qualify')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage All Qualify</button></a>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="hidden" name="action" value="qualify">
                                    <button class="btn btn-primary text1" type="submit">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>


<div class="container">
<div class="col-md-11">

<!--pagination start-->
<section class="panel">
    <header class="panel-heading">
 Latest News
        <span class="tools pull-right">
            <a class="fa fa-chevron-up" href="javascript:;"></a>

           <!--  <a class="fa fa-times" href="javascript:;"></a> -->
        </span>
    </header>
        <div style="display: none;" class="panel-body">
                        
            <div class="panel-body">
            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">
                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show All Latest News:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('news_check_status') == 1) { ?>

                                    <input type="checkbox" name="news_check_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="news_check_status" class="js-switch"/>
                                
                                <?php } ?>
                            </div>
                        </div>

               

                <div class="form-group">
                     <label class="col-sm-4 col-sm-4 control-label">Main Title</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control round-input" name="news_title" value="<?=get_config_item('news_title')?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 col-sm-4 control-label">Tagline</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control round-input" name="news_tagline" value="<?=get_config_item('news_tagline')?>">
                    </div>
                </div>
                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <a href="<?=base_url('admin/news')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage All News</button></a>
                            </div>
                        </div>

                
                <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="latest_news">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

            </form>
        </div>
    </div>
</section>
</div>
</div>
<div class="container">
    <div class="col-md-11">

        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
         Fun Facts Of Canada
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
<!-- 
                    <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                <div class="panel-body">
                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show All Fun Facts Of Canada:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('canada_fun_facts_status') == 1) { ?>

                                    <input type="checkbox" name="canada_fun_facts_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="canada_fun_facts_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>

                        </div>
                         <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <a href="<?=base_url('admin/fun-facts')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage All Fun Facts</button></a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="canada_fun_facts">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </div>
</div>



<!--Digital Agency Start here-->

<div class="container">
    <div class="col-md-11">

        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
         Digital Agency
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
<!-- 
                    <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                <div class="panel-body">
                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show Digital Agency :</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('digital_agency_status') == 1) { ?>

                                    <input type="checkbox" name="digital_agency_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="digital_agency_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>

                        </div>
                         <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <a href="<?=base_url('admin/agency')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage Digital Agency</button></a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="digital_agency">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </div>
</div>
    


<!--Digital Agency End here-->










<div class="container">
    <div class="col-md-11">
<section class="panel">
    <header class="panel-heading">
Discuss Project 
        <span class="tools pull-right">
            <a class="fa fa-chevron-up" href="javascript:;"></a>

          <!--   <a class="fa fa-times" href="javascript:;"></a> -->
        </span>
    </header>
        <div style="display: none;" class="panel-body">
                        
            <div class="panel-body">
            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">
                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show Project:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('project_status') == 1) { ?>

                                    <input type="checkbox" name="project_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="project_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>
                        </div>

               

                <div class="form-group">
                     <label class="col-sm-4 col-sm-4 control-label">Main Title</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control round-input" name="project_title" value="<?=get_config_item('project_title')?>">
                    </div>
                </div>
                <div class="form-group">
                     <label class="col-sm-4 col-sm-4 control-label">Project Tagline</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control round-input" name="project_tagline" value="<?=get_config_item('project_tagline')?>">
                    </div>
                </div>

               
               <!--  <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <a href="<?=base_url('admin/qualify')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage All Qualify</button></a>
                            </div>
                        </div>
 -->
                
                <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="project">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

            </form>
        </div>
    </div>
</section>
</div>
</div>

    <div class="container">
        <div class="col-md-11">
            <section class="panel">
                <header class="panel-heading">Serve Industries 
                    <span class="tools pull-right">
                        <a class="fa fa-chevron-up" href="javascript:;"></a>
                    </span>
                </header>
                
                <div style="display: none;" class="panel-body">
                                    
                    <div class="panel-body">
                        <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/front_page') ?>" novalidate="novalidate" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Show Serve Industries:</label>

                                <div class="col-sm-7">

                                    <?php if (get_config_item('serve_industries_status') == 1) { ?>

                                        <input type="checkbox" name="serve_industries_status" class="js-switch" checked />
                                    
                                    <?php } else { ?>
                                        
                                        <input type="checkbox" name="serve_industries_status" class="js-switch"/>
                                    
                                    <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                 <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control round-input" name="serve_industries_title" value="<?=get_config_item('serve_industries_title')?>">
                                </div>
                            </div>
                            <div class="form-group">
                                 <label class="col-sm-4 col-sm-4 control-label">Serve_Industries_Content:</label>
                                <div class="col-sm-7">
                                     <textarea class="form-control" name="serve_industries_content"><?=get_config_item('serve_industries_content')?></textarea>
                                </div>
                            </div>
                             <div class="form-group">
                                 <label class="col-sm-4 col-sm-4 control-label">You Tube iframe:</label>
                                <div class="col-sm-7">
                                     <textarea class="form-control" name="serve_industries_youtube_iframe"><?=get_config_item('serve_industries_youtube_iframe')?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="hidden" name="action" value="serve_industries">
                                    <button class="btn btn-primary text1" type="submit">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>


<script src="<?php echo base_url() ?>assets/js/ios-switch/switchery.js" ></script>
<script src="<?php echo base_url() ?>assets/js/ios-switch/ios-init.js" ></script>