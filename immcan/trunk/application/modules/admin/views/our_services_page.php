
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/js/ios-switch/switchery.css" />
<link href="<?php echo base_url() ?>assets/js/iCheck/skins/flat/blue.css" rel="stylesheet">
    <div class="page-heading">
    <h3>
      Our Services Page Setting
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="<?=base_url('welcome')?>">Dashboard</a>
        </li>
        <li class="active">  Our Services Page Setting </li>
    </ul>
 
</div>

<div class="container">
    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           View Head Section Services
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: block;" class="panel-body">

                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_service') ?>" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show all our services on front page:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('our_services_page_status') == 1) { ?>

                                    <input type="checkbox" name="our_services_page_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="our_services_page_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="services_title" value="<?=get_config_item('services_title')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Tagline:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="services_tagline" value="<?=get_config_item('services_tagline')?>">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-4 col-sm-4 control-label">Content:</label>
                            <div class="col-lg-7">
                                        <!-- <textarea name='services_content' class="wysihtml5 form-control" rows="9"><?=get_config_item('services_content')?></textarea> -->
                                        <textarea name='services_content' class="form-control ckeditor" rows="9"><?=get_config_item('services_content')?></textarea>
                                    </div>
                                </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Service Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="services_image" class="form-control round-input" >   
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Service Image:</label>
                            <div class="col-sm-7">
                                <?php if (get_config_item('services_image') != '') { ?>
                                    <img class="service_image" src="<?= site_url().get_config_item('services_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>   
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Name:</label>
                                <div class="col-md-7">
                                    <input type="text" name="services_menu_page_title" value="<?=get_config_item('services_menu_page_title')?>" class="form-control round-input" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Link:</label>
                                 <div class="col-md-7">
                                <input type="text" name="services_menu_page_link" class="form-control round-input" value="<?=get_config_item('services_menu_page_link')?>">
                                </div>
                            </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <a href="<?=base_url('admin/our_services_page')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage All Services Page</button></a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="service">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

    </div>
</div>
<!--Start 2nd Section Of Services-->
<div class="container">
    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           View Post Services
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
        <div style="display: none;" class="panel-body">
            <div class="page-heading">
                <h3>
                   Details Listing
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('welcome')?>">Dashboard</a>
                    </li>
                    <li>
                        <a href="#">Post Services</a>
                    </li>
                    <li class="active"> View All List </li>
                </ul>
            </div>
             <div class="panel-body">
            <a href="<?=base_url('admin/our_service/add')?>"><button class="btn btn-primary" type="button">Add Post Service</button></a>
        </div>

          <!-- <div class="panel-body"> -->
               <!-- <div class="form-group">
                    <label class="col-sm-4 col-sm-4 control-label">Post Services:</label>

                    <div class="col-sm-7">

                        <?php if (get_config_item('post_status') == 1) { ?>

                            <input type="checkbox" name="post_status" class="js-switch" checked />
                        
                        <?php } else { ?>
                            
                            <input type="checkbox" name="post_status" class="js-switch"/>
                        
                        <?php } ?>

                    </div>
                </div> -->
            <div class="panel-body">
                 <?php if($this->session->flashdata('response_status')): ?> 
                        <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <h4>
                                <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                                <p><?= $this->session->flashdata('message');?></p>
                            </h4>
                        </div>
                    <?php endif; ?>


                    <!--  -->
                    <table class="table  table-hover general-table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th class="hidden-phone">Content</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php 
                            if ( !empty( $post_services )) 
                            {
                                foreach ($post_services as $note) 
                                {

                                ?>
                                <tr>
                                    <td>
                                        <a href="#"><?=$note['post_title']?></a>
                                    </td>
                                    <td class="hidden-phone"><?=$note['post_content']?>
                                    <td><span class="label label-warning label-mini"><?=$note['status']?></span></td>
                                    <td><a href="<?=base_url('admin/our_service')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                    <td><a href="<?=base_url('admin/our_service')?>/delete/id/<?=$note['id'];?>">
                                        <i class="fa fa-eraser"></i>
                                    </a></td>
                                </tr>

                                <?php }
                            }
                            ?>

                        </tbody>
                    </table>
                    <!--  -->


            <!-- <div class="adv-table">
             <table  class="display table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Content</th>
                    <th class="hidden-phone">Edit</th>
                    <th class="hidden-phone">Delete</th>
                </tr>
                </thead>
                 <tbody>
                     <?php 
                        if ( !empty( $post_services )) 
                        {
                            foreach ($post_services as $note) 
                            {

                            ?>
                            <tr class="gradeX">
                                <td><?=$note['post_title']?></td>
                                <td class="text-capitalize"><?=$note['status']?></td>
                                <td><?=$note['post_content']?></td><td class="center hidden-phone"><a href="<?=base_url('admin/our_service')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                <td class="center hidden-phone">
                                <a href="<?=base_url('admin/our_service')?>/delete/id/<?=$note['id'];?>">
                                    <i class="fa fa-eraser"></i>
                                </a>
                                </td>
                            </tr>
                            <?php }
                        }
                        ?>
                    </tbody>

                            <tfoot>
                            <tr>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Content</th>
                                <th class="hidden-phone">Edit</th>
                                <th class="hidden-phone">Delete</th>
                            </tr>
                            </tfoot>
                </table>
            </div> -->

            </div>
        <!-- </div> -->
     </div>
  </section>
 </div>
</div>

<!--End  2nd Section Of Services -->

<div class="container">
    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           View User Stories Services
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_service') ?>" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show User Stories Services:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('user_stories_status') == 1) { ?>

                                    <input type="checkbox" name="user_stories_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="user_stories_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="user_title" value="<?=get_config_item('user_title')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Tagline:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="user_tagline" value="<?=get_config_item('user_tagline')?>">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-4 col-sm-4 control-label">Content:</label>
                            <div class="col-lg-7">
                                        <!-- <textarea name='user_content' class="wysihtml5 form-control" rows="9"><?=get_config_item('user_content')?></textarea> -->
                                        <textarea name='user_content' class="form-control ckeditor" rows="9"><?=get_config_item('user_content')?></textarea>
                                    </div>
                                </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">User Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="user_image" class="form-control round-input" >   
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">User Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('user_image') != '') { ?>
                                    <img class="user_image" src="<?= site_url().get_config_item('user_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Name:</label>
                                <div class="col-md-7">
                                    <input type="text" name="user_menu_title" value="<?=get_config_item('user_menu_title')?>" class="form-control round-input" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Link:</label>
                                 <div class="col-md-7">
                                <input type="text" name="user_menu_link" class="form-control round-input" value="<?=get_config_item('user_menu_link')?>">
                                </div>
                            </div>
                     

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label"></label>
                            <div class="col-sm-7">
                                <a href="<?=base_url('admin/our_services_page')?>"><button class="btn btn-success btn-sm btn-block" type="button">Manage All Services Page</button></a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="user_service">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

    </div>
</div>

<!--Start 4th Section Of Services-->
<div class="container">
    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           View Range of Services
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
        <div style="display: none;" class="panel-body">
            <div class="page-heading">
                <h3>
                   Details Listing
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <a href="<?= site_url('welcome')?>">Dashboard</a>
                    </li>
                    <li>
                        <a href="#">Range of Services</a>
                    </li>
                    <li class="active"> View All List </li>
                </ul>
            </div>
            <div class="panel-body">
                 <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_service') ?>">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="range_services_title" value="<?=get_config_item('range_services_title')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Tagline:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="range_services_tagline" value="<?=get_config_item('range_services_tagline')?>">
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Name:</label>
                                <div class="col-md-7">
                                    <input type="text" name="range_services_menu_title" value="<?=get_config_item('range_services_menu_title')?>" class="form-control round-input" >
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-4 col-sm-4 control-label">Button Link:</label>
                                 <div class="col-md-7">
                                <input type="text" name="range_services_link" class="form-control round-input" value="<?=get_config_item('range_services_link')?>">
                                </div>
                            </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="range_services">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
             <div class="panel-body">
            <a href="<?=base_url('admin/our_service/add1')?>"><button class="btn btn-primary" type="button">Add Range of Services</button></a>
        </div>

            <div class="panel-body">
                 <?php if($this->session->flashdata('response_status')): ?> 
                        <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                            <button data-dismiss="alert" class="close close-sm" type="button">
                                <i class="fa fa-times"></i>
                            </button>
                            <h4>
                                <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                                <p><?= $this->session->flashdata('message');?></p>
                            </h4>
                        </div>
                    <?php endif; ?>

                    <!-- -->
                     <table class="table  table-hover general-table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th class="hidden-phone">Content</th>
                                <th>Status</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php 
                            if ( !empty( $range_services )) 
                            {
                                foreach ($range_services as $note) 
                                {

                                ?>
                                <tr>
                                    <td>
                                        <a href="#"><?=$note['post_title']?></a>
                                    </td>
                                    <td class="hidden-phone"><?=$note['post_content']?>
                                    <td><span class="label label-warning label-mini"><?=$note['status']?></span></td>
                                    <td><a href="<?=base_url('admin/our_service')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                    <td><a href="<?=base_url('admin/our_service')?>/delete/id/<?=$note['id'];?>">
                                        <i class="fa fa-eraser"></i>
                                    </a></td>
                                </tr>

                                <?php }
                            }
                            ?>

                        </tbody>
                    </table>
             </div>
     </div>
  </section>
 </div>
</div>
<!--End  4th Section Of Services -->




<script src="<?php echo base_url() ?>assets/js/ios-switch/switchery.js" ></script>
<script src="<?php echo base_url() ?>assets/js/ios-switch/ios-init.js" ></script>