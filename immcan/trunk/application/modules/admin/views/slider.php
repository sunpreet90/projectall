<div class="page-heading">
            <h3>
               Details Listing
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?= site_url('welcome')?>">Dashboard</a>
                </li>
                <li>
                    <a href="#">Slider</a>
                </li>
                <li class="active"> View All List </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">

        <div class="row">
        <div class="col-sm-12">
        <section class="panel">        
        <header class="panel-heading">
            Slider Table


            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
             </span>
        </header>
         <div class="panel-body">
            <a href="<?=base_url('admin/slider/add')?>"><button class="btn btn-primary" type="button">Add Slider</button></a>
        </div>

        <div class="panel-body">
             <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
        <tr>
            <th>Title</th>
             <th>Slider Image</th>
            <th>Status</th>
            <th>Created On</th>
         <!--    <th>Services Icon</th> -->
            <th class="hidden-phone">Edit</th>
            <!-- <th class="hidden-phone">View</th> -->
            <th class="hidden-phone">Delete</th>
            
        </tr>
    </thead>
         <tbody>
             <?php 
                if ( !empty( $slider )) 
                {
                    foreach ($slider as $note) 
                    {

                    ?>
                    <tr class="gradeX">
                        <td><?=$note['post_title']?></td>
                           <td><?=$note['featured_image']?>
                        <td class="text-capitalize"><?=$note['status']?></td>
                        <td><?=$note['created_on']?></td>
                    <!--     <td><?=$note['services_icon']?></td> -->


                        <td class="center hidden-phone"><a href="<?=base_url('admin/slider')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                        <!-- <td class="center hidden-phone"><a href="<?=base_url('admin/slider')?>/view/id/<?=$note['id'];?>">View</a></td> -->
                        <td class="center hidden-phone">
                        <a href="<?=base_url('admin/slider')?>/delete/id/<?=$note['id'];?>">
                            <i class="fa fa-eraser"></i>
                        </a>
                        </td>
                    </tr>
                    <?php }
                }
                ?>
            </tbody>

                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Slider Image</th>
                        <th>Status</th>
                        <th>Created On</th>
                     <!--    <th>Services Icon</th> -->
                        <th class="hidden-phone">Edit</th>
                        <!-- <th class="hidden-phone">View</th> -->
                        <th class="hidden-phone">Delete</th>
                    </tr>
                    </tfoot>
        </table>
        </div>
        </div>
        </section>
        </div>
    </div>