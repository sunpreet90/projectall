<div class="page-heading">
            <h3>
               Details Listing
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?= site_url()?>">Dashboard</a>
                </li>
                <li>
                    <a href="#">Pages</a>
                </li>
                <li class="active"> View All List </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">

        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
            Message  <button onclick="goBack()">Go Back</button>
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
                <a href="javascript:;" class="fa fa-eye"></a>
             </span>
        </header>
        <script>
          function goBack() {
              window.location = "<?=base_url('admin/message') ?>";
          }
          </script>
        <div class="panel-body">
             <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>
            <div class="adv-table">
                    <?php 
                      $id = $this->uri->segment(4); 
                      //echo $id;
                      if(!empty($id))
                      {
                       $this->db->select("*");
                       $this->db->from('ws_contactus');
                       $this->db->where('id', $id);
                       $get_query = $this->db->get();
                       $result = $get_query->result();
                       //pr($result);
                       foreach ($result as $index => $value) {
                           # code...
                        echo "<p style='border:1px solid #ccc;padding:10px;'>";
                        echo "<b>From:</b> ".$value->first_name." ".$value->last_name."</br>";
                        echo "<b>Email Id:</b> ".$value->email_id."</br>";
                        echo "<b>Address:</b> ".$value->residence;
                        echo "</p>";
                        echo "<p style='border:1px solid #ccc;padding:10px;'>";
                        echo "<b>Message:</b> </br>";
                        echo $value->message;
                        echo "</p>";
                       }
                       $data = array(
                              'status' => '0'
                      );

                      $this->db->where('id', $id);
                      $this->db->update('ws_contactus', $data);
                       
                      }else{
                        return FALSE;
                        }
                    ?>      
            </div>
        </div>
        </section>
        </div>
        </div>