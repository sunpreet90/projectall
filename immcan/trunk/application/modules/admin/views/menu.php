 <!--ios7-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/js/ios-switch/switchery.css" />
<link href="<?php echo base_url() ?>assets/js/iCheck/skins/flat/blue.css" rel="stylesheet">

    <div class="page-heading">
        <h3>Menu Setting</h3>
        <ul class="breadcrumb">
            <li><a href="<?=base_url('welcome')?>">Dashboard </a></li>
            <li class="active">Menu Setting </li>
        </ul>
    </div>

    <div class="container">

        <div class="col-md-11">
            <!--pagination start-->
            <section class="panel">
                <header class="panel-heading">
                   Main Menu - About You 
                    <span class="tools pull-right">
                        <a class="fa fa-chevron-up" href="javascript:;"></a>
                    </span>
                </header>
                
                <div style="display: block;" class="panel-body">
                   <form class="form-horizontal immcan-form my-form-nsc" method="post" action="<?= base_url('admin/menu') ?>" novalidate="novalidate" >
                        
                        <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                                
                                <div class="col-md-7 col-sm-7 col-xs-6 text">
                                    <input type="text" name="about_menu_title" value="<?=get_config_item('about_menu_title')?>" class="form-control round-input" >
                                </div>
                            
                            </div>

                            <div class="col-md-7 col-sm-7 col-xs-6">
                                <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                                <div class="col-md-8 col-sm-8 col-xs-6 text">
                                    <input type="text" name="about_menu_link" class="form-control round-input" value="<?=get_config_item('about_menu_link')?>">
                                </div>
                            </div>
                        </div>
                        
                        <hr>

                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                                <div class="col-md-7 col-sm-7 col-xs-6 text">
                                    <input type="text" name="about_you_title" value="<?=get_config_item('about_you_title')?>" class="form-control round-input" >
                                </div>
                            </div>

                            <div class="col-md-7 col-sm-7 col-xs-6">
                                <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                                <div class="col-md-8 col-sm-8 col-xs-6 text">
                                    <input type="text" name="about_you_link" class="form-control round-input" value="<?=get_config_item('about_you_link')?>">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                                <div class="col-md-7 col-sm-7 col-xs-6 text">
                                    <input type="text" name="about_us_title" class="form-control round-input" value="<?=get_config_item('about_us_title')?>">
                                </div>
                            </div>

                            <div class="col-md-7 col-sm-7 col-xs-6">
                                <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                                <div class="col-md-8 col-sm-8 col-xs-6 text">
                                    <input type="text" name="about_us_link" class="form-control round-input" value="<?=get_config_item('about_us_link')?>">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                                <div class="col-md-7 col-sm-7 col-xs-6 text">
                                    <input type="text" name="our_team_title" class="form-control round-input" value="<?=get_config_item('our_team_title')?>">
                                </div>
                            </div>

                            <div class="col-md-7 col-sm-7 col-xs-6">
                                <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                                <div class="col-md-8 col-sm-8 col-xs-6 text">
                                    <input type="text" name="our_team_link" class="form-control round-input" value="<?=get_config_item('our_team_link')?>">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                                <div class="col-md-7 col-sm-7 col-xs-6 text">
                                    <input type="text" name="why_us_title" class="form-control round-input" value="<?=get_config_item('why_us_title')?>">
                                </div>
                            </div>

                            <div class="col-md-7 col-sm-7 col-xs-6">
                                <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                                <div class="col-md-8 col-sm-8 col-xs-6 text">
                                    <input type="text" name="why_us_link" class="form-control round-input" value="<?=get_config_item('why_us_link')?>">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                                <div class="col-md-7 col-sm-7 col-xs-6 text">
                                    <input type="text" name="can_help_title" class="form-control round-input" value="<?=get_config_item('can_help_title')?>">
                                </div>
                            </div>

                            <div class="col-md-7 col-sm-7 col-xs-6">
                                <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                                <div class="col-md-8 col-sm-8 col-xs-6 text">
                                    <input type="text" name="can_help_link" class="form-control round-input" value="<?=get_config_item('can_help_link')?>">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row text-right">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="hidden" name="action" value="about">
                                <button type="submit" value="upload" class="btn btn-primary type">Save menus</button>
                            </div>
                        </div>
                
                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>
 
    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
               Header Widgets - Services
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>
            
            <div style="display: none;" class="panel-body">
               <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/menu') ?>" novalidate="novalidate" >
                    
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="services_menu_title" value="<?=get_config_item('services_menu_title')?>" class="form-control round-input" >
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="services_menu_link" class="form-control round-input" value="<?=get_config_item('services_menu_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="our_services_title" value="<?=get_config_item('our_services_title')?>" class="form-control round-input">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="our_services_link" class="form-control round-input" value="<?=get_config_item('our_services_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="pricing_title" class="form-control round-input" value="<?=get_config_item('pricing_title')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="pricing_link" class="form-control round-input" value="<?=get_config_item('pricing_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="testimonials_title" class="form-control round-input" value="<?=get_config_item('testimonials_title')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="testimonials_link" class="form-control round-input" value="<?=get_config_item('testimonials_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row text-right">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="action" value="our_services">
                            <button type="submit" value="upload" class="btn btn-primary type float-right">Save services menus</button>
                        </div>
                    </div>

                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>

    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
               Header Widgets - Referral And Agent
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>
            
            <div style="display: none;" class="panel-body">
               <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/menu') ?>" novalidate="novalidate" >
                    
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="main_menu_title" value="<?=get_config_item('main_menu_title')?>" class="form-control round-input" >
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="main_menu_link" class="form-control round-input" value="<?=get_config_item('main_menu_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="referral_title" value="<?=get_config_item('referral_title')?>" class="form-control round-input">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="referral_link" class="form-control round-input" value="<?=get_config_item('referral_link')?>">
                            </div>
                        </div>
                    </div>
                     <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="affiliates_title" value="<?=get_config_item('affiliates_title')?>" class="form-control round-input">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="affiliates_link" class="form-control round-input" value="<?=get_config_item('affiliates_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="agent_title" class="form-control round-input" value="<?=get_config_item('agent_title')?>">
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="agent_link" class="form-control round-input" value="<?=get_config_item('agent_link')?>">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row text-right">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="action" value="referral_agent">
                            <button type="submit" value="upload" class="btn btn-primary type float-right">Save referral/agent menus</button>
                        </div>
                    </div>

                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>

    <div class="col-md-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
               Main Menus
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                </span>
            </header>
            <div style="display: none;" class="panel-body">
               <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/menu') ?>" novalidate="novalidate" >
                    
                    <!-- ##### -->
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="faq_title" value="<?=get_config_item('faq_title')?>" class="form-control round-input" >
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="faq_link" class="form-control round-input" value="<?=get_config_item('faq_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="get_started_title" value="<?=get_config_item('get_started_title')?>" class="form-control round-input" >
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="get_started_link" class="form-control round-input" value="<?=get_config_item('get_started_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="book_now_title" value="<?=get_config_item('book_now_title')?>" class="form-control round-input" >
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="book_now_link" class="form-control round-input" value="<?=get_config_item('book_now_link')?>">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <label class="col-md-5 col-sm-5 col-xs-6 control-label">Menu Name:</label>
                            <div class="col-md-7 col-sm-7 col-xs-6 text">
                                <input type="text" name="contact_us_title" value="<?=get_config_item('contact_us_title')?>" class="form-control round-input" >
                            </div>
                        </div>

                        <div class="col-md-7 col-sm-7 col-xs-6">
                            <label class="col-md-4 col-sm-4 col-xs-6 control-label">Menu Link:</label>
                            <div class="col-md-8 col-sm-8 col-xs-6 text">
                                <input type="text" name="contact_us_link" class="form-control round-input" value="<?=get_config_item('contact_us_link')?>">
                            </div>
                        </div>
                    </div>
                    <!-- ##### -->
                        
                    <hr>
                    <div class="row text-right">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="action" value="main_menus">
                            <button type="submit" value="upload" class="btn btn-success type float-right">Save main menus</button>
                        </div>
                    </div>
                
                </form>
            </div>
        </section>
        <!--pagination end-->
    </div>
    
</div>