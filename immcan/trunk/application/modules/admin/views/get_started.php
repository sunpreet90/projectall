<div class="page-heading">
            <h3>
               Details Listing
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?= site_url('welcome')?>">Dashboard</a>
                </li>
                <li>
                    <a href="#">Get Started</a>
                </li>
                <li class="active"> View All List </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">

        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
            
            <!-- <header class="panel-heading">
                Default Buttons
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                    <a class="fa fa-times" href="javascript:;"></a>
                </span>
            </header>
            <div class="panel-body">
                
                
                <button class="btn btn-primary" type="button">All Pages</button>
                <button class="btn btn-success" type="button">Published</button>
                <button class="btn btn-info" type="button">Pending</button>
                <button class="btn btn-warning" type="button">Trash</button>
                
            </div> -->
        
        <header class="panel-heading">
            Get Started Table


            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <!-- <a href="javascript:;" class="fa fa-times"></a>
                <a href="javascript:;" class="fa fa-eye"></a> -->
             </span>
        </header>
        <!--Header Section-->
        <div class="panel-body">
                 <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/get_started') ?>" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="get_started_page_title" value="<?=get_config_item('get_started_page_title')?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Tagline:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="get_started_tagline" value="<?=get_config_item('get_started_tagline')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Get Started Header Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="get_started_header_image" class="form-control round-input" >   
                            </div>
                        </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Get Started Header Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('get_started_header_image') != '') { ?>
                                    <img class="get_started_header_image" src="<?= site_url().get_config_item('get_started_header_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="get_started">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
        <!--End Header Section-->        
        <div class="panel-body">
                <a href="<?=base_url('admin/get_started/add')?>"><button class="btn btn-primary" type="button">Add Get Started</button></a>
        </div>

        <div class="panel-body">
             <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>
        <div class="adv-table">
        <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Status</th>
            <th>Created On</th>
            <!-- <th>Our Team Image</th> -->
         <!--    <th>Services Icon</th> -->
            <th class="hidden-phone">Edit</th>
           <!--  <th class="hidden-phone">View</th> -->
            <th class="hidden-phone">Delete</th>
            
        </tr>
    </thead>
         <tbody>
             <?php 
                if ( !empty( $get_started )) 
                {
                    foreach ($get_started as $note) 
                    {

                    ?>
                    <tr class="gradeX">
                        <td><?=$note['post_title']?></td>
                        <td class="text-capitalize"><?=$note['status']?></td>
                        <td><?=$note['created_on']?></td>
                        <!-- <td><?=$note['featured_image']?> -->
                    <!--     <td><?=$note['services_icon']?></td> -->
                        <td class="center hidden-phone"><a href="<?=base_url('admin/get_started')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                        <!-- <td class="center hidden-phone"><a href="<?=base_url('admin/get_started')?>/view/id/<?=$note['id'];?>">View</a></td> -->
                        <td class="center hidden-phone">
                        <a href="<?=base_url('admin/get_started')?>/delete/id/<?=$note['id'];?>">
                            <i class="fa fa-eraser"></i>
                        </a>
                        </td>
                    </tr>
                    <?php }
                }
                ?>
            </tbody>

                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <!-- <th>Our Team Image</th> -->
                     <!--    <th>Services Icon </th> -->
                        <th class="hidden-phone">Edit</th>
                     <!--    <th class="hidden-phone">View</th> -->
                        <th class="hidden-phone">Delete</th>
                    </tr>
                    </tfoot>
        </table>
        </div>
        </div>
        </section>
        </div>
    </div>
</div>

