    <div class="page-heading">
        <h3>About Us</h3>
        <ul class="breadcrumb">
            <li><a href="<?= site_url('welcome')?>">Dashboard</a></li>
            <li><a href="#">About Us</a></li>
        </ul>
    </div>
    <!--Start About Us Section -->
    
    <div class="wrapper">
<!--START ABOUT US HEADER SECTION-->
 <div class="row">
            <div class="col-sm-11">
                <section class="panel">
                
                    <header class="panel-heading">
                        About You Header Section
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                         </span>
                    </header>

                    <div style="display: block;" class="panel-body">
                        <div class="panel-body">
                            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_team') ?>" novalidate="novalidate" enctype="multipart/form-data">

                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Page Title:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control round-input" name="about_us_header_title" value="<?=get_config_item('about_us_header_title')?>">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-sm-3 control-label">Content:</label>
                                    <div class="col-sm-9">
                                        <textarea name='about_us_header_content' class="form-control ckeditor" rows="9"><?=get_config_item('about_us_header_content')?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Us Header Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="about_us_header_image" class="form-control round-input" >   
                            </div>
                        </div>
                                <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Header Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('about_us_header_image') != '') { ?>
                                    <img class="about_us_header_image" src="<?= site_url().get_config_item('about_us_header_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="action" value="about_us_header">
                                        <button class="btn btn-primary text1" type="submit">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>    
                    </div>
                
                </section>
            </div>
        </div>
<!--END ABOUT US HEADER SECTION-->

        <!-- About page title/desc -->
        <div class="row">
            <div class="col-sm-11">
                <section class="panel">
                
                    <header class="panel-heading">
                        About Us Section
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-up"></a>
                         </span>
                    </header>

                    <div style="display: none;" class="panel-body">
                        <div class="panel-body">
                            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_team') ?>">

                                <div class="form-group">
                                    <label class="col-sm-3 col-sm-3 control-label">Page Title:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control round-input" name="about_us_page_title" value="<?=get_config_item('about_us_page_title')?>">
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-sm-3 col-sm-3 control-label">Content:</label>
                                    <div class="col-sm-9">
                                        <textarea name='about_us_content' class="form-control ckeditor" rows="9"><?=get_config_item('about_us_content')?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="action" value="about_us_page">
                                        <button class="btn btn-primary text1" type="submit">Update</button>
                                    </div>
                                </div>

                            </form>
                        </div>    
                    </div>
                
                </section>
            </div>
        </div>
        <!-- /About page title/desc -->


        <!-- Out team page section -->
        <div class="row">
            <div class="col-sm-11">
                <section class="panel">
                
                    <header class="panel-heading">
                       Our Team Section
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-up"></a>
                         </span>
                    </header>
                
                    <div style="display: none;" class="panel-body">
                        <div class="panel-body">
                            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_team') ?>">

                                <div class="form-group">
                                    <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control round-input" name="team_title" value="<?=get_config_item('team_title')?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 col-sm-4 control-label">Tagline:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control round-input" name="team_tagline" value="<?=get_config_item('team_tagline')?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="action" value="team_section">
                                        <button class="btn btn-primary text1" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <hr>

                        <div class="panel-body">
                            <a href="<?=base_url('admin/our_team/add')?>"><button class="btn btn-primary" type="button">Add Our Team</button></a>
                        </div>

                        <div class="panel-body">
                             <?php if($this->session->flashdata('response_status')): ?> 
                                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                                        <button data-dismiss="alert" class="close close-sm" type="button">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <h4>
                                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                                            <p><?= $this->session->flashdata('message');?></p>
                                        </h4>
                                    </div>
                                <?php endif; ?>
                                

                                 <table class="table  table-hover general-table">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                             <th>Our Team Image</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 
                                        if ( !empty( $our_team )) 
                                        {
                                            foreach ($our_team as $note) 
                                            {

                                            ?>
                                            <tr>
                                                <td>
                                                    <a href="#"><?=$note['post_title']?></a>
                                                </td>
                                                <td><?=$note['featured_image']?></td>
                                                <td><span class="label label-warning label-mini"><?=$note['status']?></span></td>
                                                <td><a href="<?=base_url('admin/our_service')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                                <td><a href="<?=base_url('admin/our_service')?>/delete/id/<?=$note['id'];?>">
                                                    <i class="fa fa-eraser"></i>
                                                </a></td>
                                            </tr>

                                            <?php }
                                        }
                                        ?>

                                    </tbody>
                                </table> 
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /our team Section -->

      <!-- HOW WE WORK SECTION -->
        <div class="row">
        <div class="col-sm-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           How we work
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_team') ?>" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show how we work:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('how_we_work_status') == 1) { ?>

                                    <input type="checkbox" name="how_we_work_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="how_we_work_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Section Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="how_we_work_title" value="<?=get_config_item('how_we_work_title')?>">
                            </div>
                        </div>

                        <div class="form-group ">
                            <label class="col-sm-3 col-sm-3 control-label">Content:</label>
                            <div class="col-sm-9">
                                <textarea name='how_we_work_content' class="form-control ckeditor" rows="9"><?=get_config_item('how_we_work_content')?></textarea>
                            </div>
                        </div>

                                                
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Featured Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="how_we_work_featured_img" class="form-control round-input" >   
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">featured_img:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('how_we_work_featured_img') != '') { ?>
                                    <img class="user_image" src="<?= site_url().get_config_item('how_we_work_featured_img')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>

                       

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="how_we_work">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

    </div>
</div>
<!--END HOW WE WORK SECTION -->



        <!-- START FOOTER  -->
        <div class="row">
        <div class="col-sm-11">
        <!--pagination start-->
        <section class="panel">
            <header class="panel-heading">
           View User Stories Services
                <span class="tools pull-right">
                    <a class="fa fa-chevron-up" href="javascript:;"></a>
                   <!--  <a class="fa fa-times" href="javascript:;"></a> -->
                </span>
            </header>
            <div style="display: none;" class="panel-body">
                 <div class="panel-body">

                    <form class="form-horizontal immcan-form" method="post" action="<?= base_url('admin/our_team') ?>" novalidate="novalidate" enctype="multipart/form-data">

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Show About Footer:</label>

                            <div class="col-sm-7">

                                <?php if (get_config_item('about_footer_status') == 1) { ?>

                                    <input type="checkbox" name="about_footer_status" class="js-switch" checked />
                                
                                <?php } else { ?>
                                    
                                    <input type="checkbox" name="about_footer_status" class="js-switch"/>
                                
                                <?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">Main Title:</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control round-input" name="about_footer_title" value="<?=get_config_item('about_footer_title')?>">
                            </div>
                        </div>

                                                
                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Footer Image:</label>
                            <div class="col-sm-7">
                                <input type="file" name="about_footer_image" class="form-control round-input" >   
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 col-sm-4 control-label">About Footer Image:</label>
                            <div class="col-sm-7">
                                
                                <?php if (get_config_item('about_footer_image') != '') { ?>
                                    <img class="user_image" src="<?= site_url().get_config_item('about_footer_image')?>">
                                <?php } else {
                                    echo "<i>No image selected...</i>";
                                } ?>

                            </div>
                        </div>

                       

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <input type="hidden" name="action" value="about_footer">
                                <button class="btn btn-primary text1" type="submit">Submit</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </section>

    </div>
</div>
        <!-- END FOOTER -->


      