

<!-- page heading start-->
        <div class="page-heading">
            <h3>
                Latest Book Consultant
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="#"> Latest Book Consultant</a>
                </li>
                <li class="active">Edit  Book Consultant </li>
            </ul>
        </div>
        <!-- page heading end-->

        <section class="panel">
            
            <header class="panel-heading">
                Edit News
            </header>

            <div class="panel-body">

                <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>

                <div class="form">

                    <form class="form-horizontal adminex-form" method="post" action="<?=base_url()?>admin/book_consultant/edit/id/<?= ( !empty($page) ? $page['id'] : 0); ?>" novalidate="novalidate" enctype='multipart/form-data'>

                        <div class="form-group ">
                            <label for="title" class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_title" class=" form-control" id="post_title" value="<?= ( !empty($page) ? $page['post_title'] : 0); ?>">
                            </div>
                        </div>
                         <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Conent</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <!-- <textarea name='post_content' class="wysihtml5 form-control" rows="9"><?= ( !empty($page) ? $page['post_content'] : 0); ?>"</textarea> -->
                                        <textarea name='post_content' class="form-control ckeditor" rows="9"><?= ( !empty($page) ? $page['post_content'] : 0); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Excerpt</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='post_excerpt' class="form-control ckeditor" rows="9"><?= ( !empty($page) ? $page['post_excerpt'] : 0); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2" value="<?=$page['post_title']?>">Status</label>
                            <div class="col-lg-10">
                               <select name="status" class="form-control m-bot15">
                                    <option value="published" <?php if($page['status']== 'published'){echo "Selected";} ?>>Publish</option>
                                    <option value="pending" <?php if($page['status']== 'pending'){echo "Selected";} ?>>Pending</option>
                                    <option value="trash" <?php if($page['status']== 'trash'){echo "Selected";} ?>>Trash</option>
                                </select>
                            </div>
                        </div>
                       >

                        <input type="hidden" name="action" value="1">

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <a href="<?=base_url('admin/news')?>"><button class="btn btn-default" type="button">Cancel</button></a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>