<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	function get_page()
	{
		$where_data = array('category_id' => 1);
		return $this->findWhere( $this->table, $where_data, $multi_record = TRUE );
	}
	function get_pages($id)
	{
		$where_data = array('id' => $id );
		return $this->findWhere($this->table, $where_data, $multi_record = FALSE);

	}
	function delete_page($page_id)
	{
		$where_data 	= array('id' => $page_id );
		$post 	= array( 'status' => 'trash' );
		 $this->db->delete($this->table, $where_data, $post);
	}
	/*function delete_data_from_table($table,$id){
		$this->db->delete($table, array('id' => $id));
	}*/

	function trash_page($id){
		$status = $this->db->select('status')->from('ws_posts')->where('id',$id)->get()->result_array();

		//echo "<pre>"; print_r($status); echo "</pre>"; die;
		if($status[0]['status'] == 'pending' || $status[0]['status'] == 'published'){
			// /echo "here"; die;
		//if(($status == 'pending')  && ($status =='published')){
		$trash_data = 	array(
			'status' => 'trash'
		);

        $this->db->where('id', $id);
		$this->db->update($this->table,$trash_data);
	}else{
		//echo "hello"; die;
		$where_data 	= array('id' => $id );
		//$post 	= array( 'status' => 'trash' );
		 $this->db->delete($this->table, $where_data);
	}
		//$where_data = array('id' => $id);
		//$post = array('status' => 'trash');
		//$this->db->update($this->table,$where_data,array('status' => 'trash'));
	}




	function page_insert($table_name, $insert_array)
	{
		// Inserting in Table(students) of Database(college)
		
		if ($this->db->insert($table_name, $insert_array) ){
			
			$insert_id = $this->db->insert_id();
			return $insert_id;

		} else {
			return false;
		}
	}
	/*function get_row_from_table($table,$id){
		$this->db->where('id', $id);
		return $this->db->get($table)->row();
	}*/


}

/* End of file model.php */