<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Pages extends MX_Controller {

	function __construct() {

		parent::__construct();

		$this->load->library('tank_auth');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model('pages_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'posts';
	}

	public function terms_and_conditions() {

		$this->db->from('ws_posts');

		$this->db->where('id',5);

		$query = $this->db->get();
		  
		$data['page_data'] =  $query->result();

		$this->load->module('layouts');
		$this->load->library('template');		   
		$this->template
		->set_layout('websites')
		->build('terms_and_conditions',isset($data) ? $data : NULL);
	}

	public function page_by_slug_and_id() {

		$page_name = $this->uri->segment(1);
		$slug = $this->uri->segment(2);
		$id = $this->uri->segment(3);

		if($slug == '#')
		{
			redirect('/');
		}
		else
		{
			
			$this->db->from('ws_posts')->where(array('id' => $id, 'post_slug' => $slug ));
			$query = $this->db->get();
			$data['page_data'] =  $query->result();
            //pr($data); die;

			if( $data['page_data'][0]->page_template == 'contact')
			{
               redirect(base_url()."contact/".$id);
			}
			elseif ( $data['page_data'][0]->page_template == 'faq' ) {
				# code...
				redirect(base_url()."faq/".$id);
			}
			elseif ( $data['page_data'][0]->page_template == 'service') {
				# code...
				redirect(base_url()."website/service/".$id);
			}
			else
			{

				$this->load->module('layouts');
				$this->load->library('template');		   
				$this->template
				->set_layout('websites')
				->build('terms_and_conditions',isset($data) ? $data : NULL);

			}
			//if page_template = contact then redirecr to contact controller
			//redirect(base_url()."contact/contact/index/".$id);
		}
	}


	/* Edit Featured Image Start Here */

			function do_upload1(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			'upload_path' => './uploads/featured',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('featured_image')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }

			/* Edit Featured Image End Here */	

	public function edit_page_by_slug_and_id( $slug, $id ) {

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['page'] 		= $this->pages_model->get_pages($id);

		$data['status']		= '';
		$data['message']	= '';

      	if ( $this->input->post('action') == 1 )
      	{
      		$data['form_data']	= $this->input->post();

      		$post_slug_title 	= trim( $this->input->post('post_title') );
      		$post_slug_simple 	= preg_replace("/[^ \w]+/", "", $post_slug_title);      		
      		$post_slug 			= str_replace(' ', '-', $post_slug_simple);
          	$consultant_images = $this->do_upload1();

          	if(empty($_FILES['featured_image']['name'])) {

                $insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => strtolower($post_slug),
								'post_content' => $this->input->post('post_content'),
								//'featured_image' 	=> 'uploads/featured/'.$consultant_images,
								'status' => $this->input->post('status'),
								'page_template' => $this->input->post('page_template')
							);

            }else{

            	 $insert_data = 	array(
								'post_title' => $this->input->post('post_title'), 
								'post_slug' => strtolower($post_slug),
								'post_content' => $this->input->post('post_content'),
								'featured_image' 	=> 'uploads/featured/'.$consultant_images,
								'status' => $this->input->post('status'),
								'page_template' => $this->input->post('page_template')
							);


            }

			

           $this->db->where('id', $id);

			if( !$this->db->update($this->table, $insert_data) )
			{
				$this->session->set_flashdata('response_status', 'error');
				$this->session->set_flashdata('message', 'Error on this function');
				redirect('page');
			}
			else
			{
				add_post_meta( $post_id = $id, $meta_key = 'post_button_label', $meta_value = $this->input->post('post_button_label') );
				add_post_meta( $post_id = $id, $meta_key = 'post_button_link', $meta_value = $this->input->post('post_button_link') );
				
				$this->session->set_flashdata('response_status', 'success');
				$this->session->set_flashdata('message', 'Page edited successfully');
				redirect('page');
			}
		}

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		
		$this->load->module('layouts');
		$this->load->library('template');		   
		$this->template
		->set_layout('immcan')
		->build('page_edit',isset($data) ? $data : NULL);

			
		//echo "$slug $id";

		//die('edit_page_by_slug_and_id');
	}

	public function trash_page_by_slug_and_id( $slug, $id ) {


		//$id = $this->uri->segment(4);
		

		$this->pages_model->trash_page($id);
		$this->session->set_flashdata('error', 'Error in adding database.');

		redirect('page');
			
		/*echo "$slug $id";

		die('trash_page_by_slug_and_id');*/
	}

	public function page_by_status() {

		die('page_by_status');

	}
}

/* End of file pages.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */