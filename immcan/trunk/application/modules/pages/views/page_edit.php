
<!-- page heading start-->
        <div class="page-heading">
            <h3>Page</h3>
            <ul class="breadcrumb">
                <li><a href="#">Page</a></li>
                <li class="active">Edit Page </li>
            </ul>
        </div>
        <!-- page heading end-->

        <section class="panel">
            
            <header class="panel-heading">Edit page <a target="_blank" href="<?=base_url()?>page/<?= $page['post_slug']?>/<?= $page['id']?>"><button class="btn btn-success btn-xs" type="button">VIEW PAGE</button></a></header>

            <div class="panel-body">

                <?php if($this->session->flashdata('response_status')): ?> 
                    <div class='alert alert-<?= $this->session->flashdata('response_status') ?> alert-block fade in'>
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <h4>
                            <i class="icon-ok-sign"><?= $this->session->flashdata('response_status') ?></i>
                            <p><?= $this->session->flashdata('message');?></p>
                        </h4>
                    </div>
                <?php endif; ?>

                <div class="form">

                    <form class="form-horizontal adminex-form" method="post" action="<?=base_url()?>page/edit/id/<?= ( !empty($page) ? $page['id'] : 0); ?>" novalidate="novalidate" enctype='multipart/form-data'>

                        <div class="form-group ">
                            <label for="title" class="control-label col-lg-2">Title</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_title" class=" form-control" id="post_title" value="<?= ( !empty($page) ? $page['post_title'] : 0); ?>">
                            </div>
                        </div>
                         <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Conent</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='post_content' class="wysihtml5 form-control" rows="9"><?= ( !empty($page) ? $page['post_content'] : 0); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2" value="<?=$page['post_title']?>">Status</label>
                            <div class="col-lg-10">
                               <select name="status" class="form-control m-bot15">
                                    <option value="published" <?php if($page['status']== 'published'){echo "Selected";} ?>>Publish</option>
                                    <option value="pending" <?php if($page['status']== 'pending'){echo "Selected";} ?>>Pending</option>
                                    <option value="trash" <?php if($page['status']== 'trash'){echo "Selected";} ?>>Trash</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2" value="<?=$page['post_title']?>">Page Template</label>
                            <div class="col-lg-10">
                               <select name="page_template" class="form-control m-bot15">
                                        <option value="default" <?php if($page['page_template']== 'default'){echo "Selected";} ?>>Default</option>
                                        <option value="contact" <?php if($page['page_template']== 'contact'){echo "Selected";} ?>>Contact</option>
                                        <option value="faq" <?php if($page['page_template']== 'faq'){echo "Selected";} ?>>Faq</option>
                                        <option value="about-us" <?php if($page['page_template']== 'about-us'){echo "Selected";} ?>>About Us</option>
                                        <option value="service" <?php if($page['page_template']== 'service'){echo "Selected";} ?>>Service</option>
                                    </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Edit Featured Image:</label>
                            <div class="col-sm-10">
                                <input type="file" name="featured_image" class="form-control required_shop_image error file-upload" id="featured_image"  onchange="ValidateSingleInput(this);">
                                <img class="featured_image" src="<?=base_url()?>/<?php echo $page['featured_image']; ?>" style="width: 20%;">                 
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="post_button" class="col-sm-2 col-sm-2 control-label">Button Label:</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_button_label" class=" form-control" id="post_button_label" value="<?= get_page_metadata( $page['id'], $meta_key ='post_button_label') ?>">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="post_link" class="col-sm-2 col-sm-2 control-label">Button Link:</label>
                            <div class="col-lg-10">
                                <input type="text" name="post_button_link" class=" form-control" id="post_button_link" value="<?= get_page_metadata( $page['id'], $meta_key ='post_button_link') ?>">
                            </div>
                        </div>
                        
                        <?php if($page['page_template']== 'faq'){ ?>
                        
                        
                            <div class="form-group">
                                
                                <div class="col-lg-2"></div>
                                <div class="col-lg-10">
                                    <a href="<?=base_url('admin/faq')?>">
                                        <button class="btn btn-info" type="button">Manage FAQ's</button>
                                    </a>
                                </div>
                                
                            </div>
                        
                        <?php } ?>

                        <input type="hidden" name="action" value="1">

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <a href="<?php echo base_url('page') ?>" class="btn btn-default" type="button">Cancel</a>
                                <!-- <button class="btn btn-default" type="button">Cancel</button> -->
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>