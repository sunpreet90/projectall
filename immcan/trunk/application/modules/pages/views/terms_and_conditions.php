<style type="text/css">
    .main_menu_area {
    position: absolute;
    width: 100%;
    top: 0px;
    left: 0px;
    z-index: 30;
    padding: 0px 75px;
    border-bottom: 1px solid #626571;
    background-color: #bcafc9;
}
ul.checkapp {
    list-style: none;
    margin: 0 80px 1em 15px;
    padding: 0;
}
#section1
{
    padding-top: 100px;
}
h1 {
    text-align: center;
    margin-bottom: 12px;
    margin-top: 0;
}
ul{
    line-height: 30px;
}
</style>

<?php
foreach($page_data as $get_page_data)
{
       /* echo "<pre>";
        print_r($get_page_data);echo "</pre>";*/
?>
<?php if($get_page_data->featured_image != NULL){ ?>
<section class="">
        <img src="<?= base_url().$get_page_data->featured_image; ?>" alt="<?= $get_page_data->post_title; ?>" width="100%" height="350px" style="margin-top: 8%;">
</section>
<?php } ?>
    <section id="section1" class="single-page">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1><?= $get_page_data->post_title; ?></h1>
                       <p> <?= htmlspecialchars_decode(@$get_page_data->post_content); ?></p>
                       <?php 
                        $post_button_label = get_page_metadata( $get_page_data->id, 'post_button_label' ); 
                        $post_button_link = get_page_metadata( $get_page_data->id, 'post_button_link' ); 
                    
                        if ($post_button_label) { ?>

                            <a href="<?=$post_button_link?>">
                                <button type="button" class="btn btn-primary"><?=$post_button_label?></button>
                            </a>
                    
                    <?php } ?>

                    </div>

                </div>
            </div>
    </section>

<?php
}
?>
