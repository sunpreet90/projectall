<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php'); // including rest api library

/* Braintree Library start */
require(APPPATH.'libraries/lib/Braintree.php'); // including Braintree library

Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('nxv4y769mbyv7ph7'); // Merchant Id
Braintree_Configuration::publicKey('vzt2fx8rgn54fn6v'); // public key
Braintree_Configuration::privateKey('08df8a72bcc6e6aef36f16876ba3aa34'); // private key  

/* Braintree Library End */

class App extends REST_Controller {

	function __construct() {

		parent::__construct();
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->baseurl = $this->config->item('base_url');	// App base URL
		$this->load->model('common_model');					// Adding your model to use for api
    ini_set('max_execution_time', 300); 
		// Limiting request per hour by using limit feature
		// Set $config['rest_enable_limits'] = true; and create limits tables as described in rest.php application/config/rest.php
		$this->methods['test_post']['limit'] = 5000; //500 requests per hour per user/key
	}

	/**
	 ***********************************************************
	 *	Function Name :	test								
	 *	Functionality : Created for testing purpose.
	 *					checks if app contoller is properly integrated with rest architecture.
	 *	@access 		public 									
	 *	@param 		  : input 				
	 *	@return 	  : input data
	 ***********************************************************
	 **/
	 
	 public function test_post()
	{
		$input = $this->input->post('input');
		echo $input;
	}
	 
	
	/**
	 ***********************************************************
	 *	Function Name :	braintree_client_token								
	 *	Functionality : Created for generate client token.
	 *	@return 	  : token data
	 ***********************************************************
	 **/
	
	 public function braintree_client_token_post()
    {
        $clientToken = \Braintree_ClientToken::generate();
        if($clientToken){
            $data = ['status' => 200, 'message' => 'Client token generated successfully','token' => $clientToken];
        }
        else{
            $data = ['status' => 301, 'message' => 'no token found'];
        }
         $this->response($data, 200);
    }
	
	/**
     ***********************************************************
     *  Function Name : braintree_nonce_save
     *  Functionality : save the braintree nonce.
     *  @access         public
     *  @param        : user_id,account_type,nonce_token.
     *  @return       : if generate successfully gives status 1 otherwise 0";
     *  Author        : Sunpreet
     ***********************************************************
     **/
	public function braintree_nonce_save_post(){
		$user_id = $this->input->post("user_id"); // User Id
		$ticket_id = $this->input->post("ticket_id"); // Ticket ID
		$nonce_token = $this->input->post("nonce_token"); // Nonce token
		$is_saved = $this->input->post("is_saved"); // Is saved
		$total_amount = $this->input->post("total_amount"); // Is saved
		
		  $where_data = array('id' => $user_id);
            if (empty( $this->db->where( $where_data)->get('ws_users')->row() )) {
	            $data = array( 'status' => 301, 'message' => "User id not exists.");
			  	$this->response($data, 301);
            } 
	
		$this->check_empty($user_id, "Please Add user_id");
		$this->check_empty($ticket_id, 'Enter ticket id');			
		$this->check_empty($nonce_token, 'Enter Nonce Token');		
		$this->check_empty($is_saved, 'Save 1 or No 0');	
		$this->check_empty($total_amount, 'Enter total amount');	
		
			
	$userCheck =	$this->db->select('*')->from('ws_users')->where('id',$user_id)->get()->row_array();
       
		 if ($userCheck) {

            //check user payment method
         $userPaymentCheck = $this->db->get_where('bp_payment_accounts' , array('user_id' => $user_id ))->row_array();
           if(!$userPaymentCheck) {
			 
                //create user on braintree and add card
				
				/* $result = Braintree_Transaction::sale([
					  'amount' => '10.00',
					  'paymentMethodNonce' => $nonce_token,
					  
					  'options' => [
						'storeInVaultOnSuccess' => false,
					  ]
					]); */
				
				//$customer = \Braintree_Customer::find($user_id);
				//echo "<pre>"; print_r($customer); die;
				 $result = \Braintree_Customer::create([
                    'id' => $user_id,
                    'firstName' => $userCheck['name'],
                    'email' => $userCheck['email'],
                    'paymentMethodNonce' => $nonce_token,
                    'creditCard' => [
                        'options' => [
                            // 'failOnDuplicatePaymentMethod' => true,
                            'verifyCard' => true
                        ]
                    ]

                ]);
                
					 
				//var_dump($result);//die;
				//return $result->success; die;
					  
					  
				  if($result->success){
                    //var_dump($result);die;
						$data_arr = array(
								'user_id' => $user_id,
								//'account_type' => $result->customer->creditCards[0]->cardType,
								'card_type' => $result->customer->creditCards[0]->cardType,
								'card_identifier' => $result->customer->creditCards[0]->uniqueNumberIdentifier,
								'masked_number' => $result->customer->creditCards[0]->maskedNumber,
								'payment_token' => $result->customer->creditCards[0]->token,
								'expiration_date' => $result->customer->creditCards[0]->expirationDate,
								'image_url' => $result->customer->creditCards[0]->imageUrl,
								'card_last_4' => $result->customer->creditCards[0]->last4,
								'expired' => $result->customer->creditCards[0]->expired,
								'payroll' => $result->customer->creditCards[0]->payroll,
								'updated_at' => date('Y-m-d H:i:s')
						); 
						$this->db->insert('bp_payment_accounts', $data_arr);
						$data = array('status' => 200, 'message' => 'Payment method saved successfully');
                  }
                else{
					$data = array('status' => 301, 'message' => 'Payment Method not saved','message_braintree' => $result->message);
                    //$data = ['status' => 0, 'message' => 'Payment Method not saved', 'message_braintree' => $result->message];
                } 
				
			}else{
				$result = \Braintree_PaymentMethod::create([
                    'customerId' => $user_id,
                    'paymentMethodNonce' => $nonce_token,
                    'options' => [
                        //'failOnDuplicatePaymentMethod' =>true,
                        'verifyCard' => true
                    ]

                ]);
				
				if($result->success){

                    					
					$data_arr = array(
							'user_id' => $user_id,
                            //'account_type' => $account_type,
                            'card_type' => $result->paymentMethod->cardType,
                            'card_identifier' => $result->paymentMethod->uniqueNumberIdentifier,
                            'masked_number' => $result->paymentMethod->maskedNumber,
                            'payment_token' => $result->paymentMethod->token,
                            'expiration_date' => $result->paymentMethod->expirationDate,
                            'image_url' => $result->paymentMethod->imageUrl,
                            'card_last_4' => $result->paymentMethod->last4,
                            'expired' => $result->paymentMethod->expired,
                            'payroll' => $result->paymentMethod->payroll,
                            //'is_default' => $result->paymentMethod->default,
                            'is_default' => 1,
                            'updated_at' => date('Y-m-d H:i:s')
						);
					
                    
						$this->db->insert('bp_payment_accounts', $data_arr);
						$data = array('status' => 200, 'message' => 'Payment method saved successfully');

                }else{
					$data = array('status' => 301, 'message' => 'Payment Method not saved','message_braintree' => $result->message);
                }							
			}
			
		 }else{
            $data = array('status' => 301, 'message' => 'No user found');
        }
		$this->response($data, 200);
        //return response()->json($data, 200);	 
		 
		
	}
	
	
	
		/**
     ***********************************************************
     *  Function Name : braintree_card_detail
     *  Functionality : Fetch card details by braintree.
     *  @access         public
     *  @param        : user_id,account_type,nonce_token.
     *  @return       : if generate successfully gives status 1 otherwise 0";
     *  Author        : Sunpreet
     ***********************************************************
     **/
	
	public function braintree_card_detail_post(){
		
		$user_id = $this->input->post("user_id");
		$this->check_empty($user_id, "please enter user id");
		
		$valid_user = $this->db->get_where('ws_users' , array('id' => $user_id ))->row_array();
		
		//print_r($valid_user); die;
		
        if($valid_user)
        {
			//$userPaymentMethodsCards = $this->db->select('id, name')->from('bp_payment_accounts')->where('user_id',$user_id)->get();

			$userPaymentMethodsCards =	$this->db->select('id, user_id, masked_number,image_url,payment_token')->from('bp_payment_accounts')->where('user_id',$user_id)->get()->row_array();
			//$userPaymentMethods['cards'] = $userPaymentMethodsCards;
			//print_r($userPaymentMethodsCards); die;
			$data = array('status' => 200, 'payment_methods' => $userPaymentMethodsCards);
			
		}else{
			
			$data = array('status' => 301, 'message' => 'No user found');
           
        }

        $this->response($data, 200);	
		
	}
	
	
	
	
	
    /**
	 *********************************************************************
	 *	Function Name :	check_empty() .
	 *	Functionality : check data validation
	 *********************************************************************
	 **/
	public function check_empty( $data, $message = '', $numeric = false )
	{
       
		$message = ( !empty( $message ) ) ? $message : 'Invalid data';
		if( empty( $data ) )
	    {
	   	
	    	$data = array( 'status' => 301, 'message' => $message );
	    	$this->response($data, 200);
         
	    }
	    if( $numeric )
	    {
	    	if( !numeric( $data ) )
		    {
		    	$data = array( 'status' => 301, 'message' => 'Invalid data' );
	    		$this->response($data, 200);
		    }
	    }
	}

     /*
      API for user login
     */
	public function login_post()
    {
        /* $isSocial = $this->input->post('isSocial');
       
        if($isSocial == ""){
           $this->check_empty($isSocial, 'Please add isSocial');
        }else{

		        if($isSocial == 0){ */
			        $email = $this->input->post('email');
			        $this->check_empty($email, 'Please add email');
			        $password   = $this->input->post('password');
			        
			        $this->check_empty($password, 'Please add password');

			        $data = $this->tank_auth->login_app( $email, $password, 'get_user_by_email', $login_by_email = true );            
			        $this->response($data, 200);
					
					
			    /* }else if($isSocial== 1){
                       //echo "ook is 1";die;
			    	$name = $this->input->post('name');

						$email = $this->input->post('email');
						$phone = $this->input->post('phone');
						$gender = $this->input->post('gender');
						$profile_pic = $this->input->post('profile_pic');
						

						$facebook_id = $this->input->post('facebook_id');

                      $this->check_empty($facebook_id, 'Please add facebook_id');

			          $data = $this->tank_auth->login_with_facebook_app($facebook_id,$email, $phone, $name,$profile_pic,$gender,'get_user_by_email', $login_by_email = true );

                      <!--$data = $this->tank_auth->login_with_facebook_app( $facebook_id,$email,'get_user_by_email', $login_by_email = true );
                      $this->response($data, 200);-->
                      
			    	  $this->response($data, 200);

                }else{ */
			    	$data = array( 'status' => 301, 'message' => 'Invalid login data' );
			    	$this->response($data, 200);
			    //}
	    //}

    }
    /*
     API for user register
    */
  function register_post()
	{

		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		
		$this->check_empty($name, 'Please add name');
    	$this->check_empty($email, 'Please add email');
    	$this->check_empty($password, 'Please add password');
    	
       
    	if(!$this->users->is_email_available($email)){

    		$data = array( 'status' => 301, 'message' => 'Email already exits' );
	    	$this->response($data, 200);
    	}

    	$email_activation = $this->config->item('email_activation', 'tank_auth');
				if (!is_null($data = $this->tank_auth->create_app_user(
						$email,
						$password,
						$name,
						$email_activation))) { // success
          // $data['site_name'] = $this->config->item('website_name', 'tank_auth');
					// $data['data'] = $data;

					if ($email_activation) {									// send "activate" email
						//$data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

						 

						unset($data['password']); // Clear password (just for any case)
						$success_message = "User registered successfully";

						$data['status'] = 200;
						$data['message'] = $success_message;
                       
                         $where_data1 = array('id'=>$data['data']['user_id']);
					 //echo $where_data1;die;
						  $data1= get_object_vars($this->db->where( $where_data1)->get('ws_users')->row());

				          //print_r($this->db->last_query());die;
				        //print_r($user_data);die;
				        $this->_send_email_app('resend_code',$data1['email'] ,$data1, "Register");
                         //Send verify code on email 
						$this->response($data, 200);
                        
                        

                       

					} else {
						$success_message = "User registered successfully";
						// $this->_show_message($this->lang->line('auth_message_registration_completed_2').' '.anchor('/auth/login/', 'Login'));
					}
	    			$data['status'] = 200;
					$data['message'] = $success_message;
                    
                    //$this->_send_email('activate', $data['email'], $data);

	    			$this->response($data, 200);

				} else {
					$errors = $this->tank_auth->get_error_message();
					foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);

					$data = array( 'status' => 301, 'message' => $errors );
	    			$this->response($data, 200);
				}

        }
         /*
     API for user email verify
    */
  function verification_email_post()
	{

          $user_id = $this->input->post('user_id');
          $verification_code = $this->input->post('verification_code');
		
		  $this->check_empty($user_id, 'Please add user_id');
		  $this->check_empty($verification_code, 'Please add verification_code');

          //echo $user_id.$verification_code;die;
           $where_data1 = array('id' => $user_id);
           $where_data = array('id' => $user_id, 'isVerified' =>1);
           $where_data2 = array('id' => $user_id, 'new_email_key' => $verification_code);
			
          if (!empty( $this->db->where( $where_data)->get('ws_users')->row() )) {
            $data = array( 'status' => 301, 'message' => "Email already verified" );
		  	$this->response($data, 200);
          }
          if(empty( $this->db->where( $where_data1)->get('ws_users')->row() )){
          	$data = array( 'status' => 301, 'message' => "user_id does not exist" );
		  	$this->response($data, 200);
          }
          if (empty( $this->db->where( $where_data2)->get('ws_users')->row() )) {
            $data = array( 'status' => 301, 'message' => "Invalid verification code." );
		  	$this->response($data, 200);
          }

          $data = $this->tank_auth->code_verify_app( $user_id, $verification_code);
		  $this->response($data, 200);
    }
   function resend_verify_code_post()
   {
          $user_id = $this->input->post('user_id');
          $this->check_empty($user_id, 'Please add user_id');
     
          $where_data1 = array('id' => $user_id);
          $where_data = array('id' => $user_id, 'isVerified' =>1);
          //pr($this->db->where( $where_data)->get('ws_users')->row_array());
          // if (empty($this->findWhere( 'users', $where_data))) {

         

          if(empty( $this->db->where( $where_data1)->get('ws_users')->row() )){
          	$data = array( 'status' => 301, 'message' => "user_id does not exist" );
		  	$this->response($data, 200);
          }
          if (!empty( $this->db->where( $where_data)->get('ws_users')->row() )) {
            $data = array( 'status' => 200, 'message' => "Email already verified" );
		  	$this->response($data, 200);
          }

          if(!empty( $user_info = $this->db->where( $where_data1)->get('ws_users')->row() )){
            if($user_info){
                // print_r($user_info);
                 if($user_info->attempts_limit_exceed >3){
                 	   $data = array( 'status' => 301, 'message' => "Verification code attempts limit exceeds" );
		  	           $this->response($data, 200);
                 }else{
                 	        $limit_add= $user_info->attempts_limit_exceed +1;
				            $this->db->set('attempts_limit_exceed', $limit_add);

							$data= $this->db->where( 'id', $user_id )->update('ws_users');

                 }
            }

          	
          }
           //$data['attempts_limit_exceed'] =  1;

          $data1 = get_object_vars($this->tank_auth->resend_verify_code_app($user_id));

         

          if(!empty($data1)){
          	 

          	$this->_send_email_app('resend_code',$data1['email'] ,$data1, 'Verify Code');
             $attempt_left = 4-$data1['attempts_limit_exceed'];
          	$data = array( 'status' => 200, 'message' => "Check your email for the verification code" );
            $data['data'] = array('attempt_left'=> $attempt_left );
		  	   $this->response($data, 200);

          }else{
          	$data = array( 'status' => 301, 'message' => "Invalid user" );
		  	$this->response($data, 200);
          	
          } 


          
         
    }
    /*
     API for forgot password
    */
    function forgot_password_post(){
             $email = $this->input->post('email');
             $this->check_empty($email, 'Please add email');

            $where_data = array('email' => $email);

            if (empty( $this->db->where( $where_data)->get('ws_users')->row() )) {
	            $data = array( 'status' => 301, 'message' => "Email id not exists.");
			  	$this->response($data, 200);
            }

          if (!is_null($data = $this->tank_auth->forgot_password_app($email))){
                $data['site_name'] = $this->config->item('website_name', 'tank_auth');
                 $this->_send_email_app('forgot_user_password',$data['email'] ,$data, 'Forgot Password');
               
                 $data1 = array( 'status' => 200, 'message' => "Verification code has been sent to your email. Please follow the steps to reset your password.");
                 $data1['data'] =array("user_id"=>$data['user_id']);
			  	
          }else{
              $data1 = array( 'status' => 200, 'message' => "Something went wrong");
          }
          $this->response($data1, 200); 
    }
       /*
     API for verify code for reset passwod 
    */
  function verification_forgot_password_post()
  {
          $user_id = $this->input->post('user_id');
          $verification_code = $this->input->post('verification_code');
    
      $this->check_empty($user_id, 'Please add user_id');
      $this->check_empty($verification_code, 'Please add verification_code');

          //echo $user_id.$verification_code;die;
           $where_data1 = array('id' => $user_id);
           
          if(empty( $this->db->where( $where_data1)->get('ws_users')->row() )){
             $data = array( 'status' => 301, 'message' => "User id does not exist" );
             $this->response($data, 200);
          }
          

          $data = $this->tank_auth->code_verify_forgot_password_app( $user_id, $verification_code);
      $this->response($data, 200);
    }
        /*
     API for reset passwod 
    */
  function reset_password_post()
  {
          $user_id = $this->input->post('user_id');
          $new_password = $this->input->post('new_password');
    
      $this->check_empty($user_id, 'Please add user_id');
      $this->check_empty($new_password, 'Please add new_password');

          $where_data1 = array('id' => $user_id);
           
          if(empty( $this->db->where( $where_data1)->get('ws_users')->row() )){
             $data = array( 'status' => 301, 'message' => "User id does not exist" );
             $this->response($data, 200);
          }
           
            $data = $this->tank_auth->reset_password_app($user_id, $new_password);
             if($data == true){
                   $data = array( 'status' => 200, 'message' => "Password have been changed successfully.");
             }else{
                $data = array( 'status' => 301, 'message' => "Password not save");
             } 
      $this->response($data, 200);
    }
    /*
     API for forgot password
    */
    function change_password_post(){
             $user_id = $this->input->post('user_id');
             $oldPassword = $this->input->post('oldPassword');
             $newPassword = $this->input->post('newPassword');

             $this->check_empty($user_id, 'Please add user_id');
             $this->check_empty($oldPassword, 'Please add oldPassword');
             $this->check_empty($newPassword, 'Please add newPassword');

            $where_data = array('id' => $user_id);

            if (empty( $user= $this->db->where( $where_data)->get('ws_users')->row() )) {

               $data = array( 'status' => 301, 'message' => "User id not exists.");
                 $this->response($data, 200);
            }
           
            //echo $oldPassword."new".$newPassword;die;
            $data = $this->tank_auth->change_password_app($user_id,$oldPassword,$newPassword);
             if($data == true){
                   $data = array( 'status' => 200, 'message' => "Password changed successfully.");
             }else{
                $data = array( 'status' => 301, 'message' => "Old Password Does not Match");
             }       
            $this->response($data, 200);

    }
	
	/*
	API for Fiter Events 
	*/
	/* function search_events_post(){
		$event = $this->input->post("event");
		$location = $this->input->post("location");
		$query = $this->db->get('ws_event_management')->result_array();
		//echo $this->db->last_query();
		//print_r($query); die;
		if($event !='' AND $location == ''){
		//if($query){ //print_r($query); die;
			foreach($query as $events){
				//print_r($events); die;
				$res['event_name'] = (!empty($events['event_name']) ? ($events['event_name']) : '');
                $res['event_address'] = (!empty($events['event_address']) ? ($events['event_address']) : '');
                $res['zipcode'] = (!empty($events['zipcode']) ? ($events['zipcode']) : '');
                $res['event_id'] = $events['id'];                
                $res['city'] = $events['city'];
                $res['state'] = $events['state'];
				//$this->db->from('ws_event_management', array('id'=> $res['event_id']));
                $data = $this->db->get_where('ws_event_management', array('city'=> $res['city']))->result_array();
				//print_r($data); die;
			}
		}
		else {
            $data = array('status' => 0, 'message' => 'no results');
        }
        $this->response($data, 200); 
	//} 
	}*/
	
	function search_events_post(){
		$event_name = $this->input->post("event_name");
		//$this->check_empty($event,'Please enter event');
		$location = $this->input->post("location");
		
				       if(!empty($location) || !empty($event_name)){
						   
					     
						  //if(!empty($location))
						  //{
						  $this->db->from('ws_event_management em');
							$this->db->where('(em.city="'.$location.'" AND em.state = "'.$location.'")');
							$this->db->like('em.city', $location, 'after');
							$this->db->or_like('em.state', $location, 'after');
						  //}
						  //if(!empty($event_name))
						  //{
							//$this->db->from('ws_event_management em');
							$this->db->like('em.event_name', $event_name, 'after');
						 // }
						  
						  
				 $searchData = $this->db->get()->result_array();
				// echo "<pre>";
				 //print_r($searchData); die;
				 if($searchData){
			
					
					 $data = array('status' => 200, 'message' => 'Search event found successfully', 'data' => $searchData);
				 }
				else {
            $data = array('status' => 301, 'message' => 'No search result found');
			}
					   
		}else{
			$data = array('status' => 301, 'message' => 'Not able find location');
			 }
			
			$this->response($data, 200);
						
	}
	
	function event_details_post(){
		$eventid = $this->input->post('eventid');
		$this->check_empty($eventid,'Please enter event id');
		$eventdata = $this->db->get_where('ws_event_management', array('id' => $eventid))->row_array();
		
			if(!empty($eventdata['day']) ? $eventdata['day'] : ''){
				$data['Type'] = 'day';
				//(!empty($user_detail['user_id']) ? $user_detail['user_id'] : '');
				$data['ticket_price'] = (!empty($eventdata['day']) ? $eventdata['day'] : '') ;
				$eventdata['duration'][] = $data;
			}
			if(!empty($eventdata['week']) ? $eventdata['week'] : ''){
				$data['Type'] = 'week';
				$data['ticket_price'] = (!empty($eventdata['week']) ? $eventdata['week'] : '');
				$eventdata['duration'][] = $data;
			}
			if(!empty($eventdata['month']) ? $eventdata['month'] : '' ){
				$data['Type'] = 'month';
				$data['ticket_price'] = (!empty($eventdata['month']) ? $eventdata['month'] : '');
				$eventdata['duration'][] = $data;
			}
			if(!empty($eventdata['season'])? $eventdata['season'] : ''){
				$data['Type'] = 'season';
				$data['ticket_price'] = (!empty($eventdata['season']) ? $eventdata['season'] : '');
				$eventdata['duration'][] = $data;
			}
			
		
		//echo "<pre>"; print_r($eventdata); die;
		
		if($eventdata){
			$this->response(array('status' => 200, 'message' => 'Event detail fetch successfully', 'data' => $eventdata));
		}else{
			$this->response(array('status' => 301, 'message' => 'Eventid does not exist'));
		}
	}
	
	
	/* Add tickets and Management  */ 
	
	 public function add_tickets_management_post() 
    {
        $user_id = $this->input->post('user_id');
        $this->check_empty($user_id, 'Please enter user_id');
		
		$event_id = $this->input->post("event_id");
		$this->check_empty($event_id, 'Please enter event id');
		
        $valid_user = $this->db->get_where('ws_users' , array('id' => $user_id ))->row_array();
		
        if($valid_user)
        {
            
			$event_id = $this->input->post("event_id");
			$attendies = $this->input->post("attendies");
			$ticket_type = $this->input->post("ticket_type");
			
			$this->check_empty($event_id, 'Please enter event id'); 
			$this->check_empty($attendies, 'Please enter Attendies');
			$this->check_empty($ticket_type, 'Please enter ticket type');
			
			$where_data = array('id' => $event_id);
            if (empty( $this->db->where( $where_data)->get('ws_event_management')->row() )) {
	            $data = array( 'status' => 200, 'message' => "Event id not exists.");
			  	$this->response($data, 200);
            }
			$eventdata = $this->db->get_where('ws_event_management', array('id' => $event_id))->row_array();
			//pr($eventdata); die;
			if($ticket_type == "day"){
				
				$price	= $eventdata['day'];
			}else if($ticket_type == "week"){
				$price	= $eventdata['week'];
			}else if($ticket_type == "month"){
				$price	= $eventdata['month'];
			}else{
			$price	= $eventdata['season'];
			}
					
					
			$totalprice = ($attendies * $price);
			//echo "<pre>";
			//print_r($totalprice);
			//die;
			
			 $post_data = array(
			'event_id' => $event_id,
			'user_id'   => $user_id,
			'attendees'   => $attendies,
			'price' => $price,
			'total_price'   => $totalprice,
			'ticket_type'   => $ticket_type,
			'created_at'   =>    date('Y-m-d H:i:s', time())
			); 
			
			if($last_id = $this->common_model->add_tickets( $table = 'ws_tickets_management', $post_data ))
			{
				$post_data['ticket_id']=$last_id;
				$res_data = array('status' =>200, 'message' => 'Ticket store successfully please proceed for payment', 'data' => $post_data );
			}
			else
			{
				$res_data = array( 'status' => 301, 'message' => 'Error' );
			}
        }
         else
        {
            $res_data = array( 'status' => 301, 'message' => ' user id does not exist');
        }     
		
	    $this->response($res_data, 200);
    }
	
	
	
	
	
	/* function search_events_post1(){
		$event = $this->input->post("event");
		$location = $this->input->post("location");
		if ($event && $location) { 
		$this->db->select('em.event_name','em.city')
				         ->from('ws_event_management em')
						 ->where('(em.city="'.$location.'" or em.state = "'.$location.'")')
						 ->like('em.event_name', $event);
				 $searchData = $this->db->get()->result_array();
		
		} elseif ($event == '' && $location == ''){
			
		}
	
	} */
	
    /**
     API for get profile
    */
    function get_profile_post(){
             $user_id = $this->input->post('user_id');
             $this->check_empty($user_id, 'Please add user_id');

            $where_data = array('id' => $user_id);

            if (empty( $this->db->where( $where_data)->get('ws_users')->row() )) {
	            $data = array( 'status' => 200, 'message' => "User id not exists.");
			  	$this->response($data, 200);
            }
            $data = $this->tank_auth->get_profile_app($user_id);

            if($data){
                   if($data['data']['fb_pic'] == ""){ 

                      if($data['data']['profile_pic'] != ""){
                             $data['data']['profile_pic'] = $this->baseurl.'assets/uploads/profile_pic/'.$data['data']['profile_pic'];
                      }else{
                             $data['data']['profile_pic'] = $data['data']['profile_pic'];
                      } 

                   }else{
                       $data['data']['profile_pic'] = $data['data']['fb_pic'];
                   }
                unset($data['data']['fb_pic']);

                $this->response($data, 200);
            }else{
              $data['status']=301;
              $data['message']="Inccorrect user";
              $this->response($data, 200);
            }
    }
   /* function download_fb_pic_post(){
        $url = $this->input->post('url');

        //$url = 'http://example.com/image.jpg';
        // $img = $_SERVER["DOCUMENT_ROOT"].'doggystroll/assets/uploads/profile_pic/';
        $img = $this->baseurl."assets/uploads/profile_pic/";
        $data = file_get_contents($url);
        file_put_contents($img, $data);
        die("aaa");
        die("okoko");
    }*/
    
    /**
     API for update profile
    */
    function update_profile_post(){

             $user_id = $this->input->post('user_id');
             $dob = $this->input->post('dob');
             $gender = $this->input->post('gender');
             $paypalAccount = $this->input->post('paypalAccount');
             $name = $this->input->post('name');
             $phone = $this->input->post('phone');
             $user_type = $this->input->post('user_type');
            
             $this->check_empty($user_id, 'Please add user_id');
             $this->check_empty($dob, 'Please add dob');
             $this->check_empty($gender, 'Please add gender');
             //$this->check_empty($profile_pic, 'Please add profile_pic');
             $this->check_empty($paypalAccount, 'Please add paypalAccount');
             $this->check_empty($name, 'Please add name');
             $this->check_empty($phone, 'Please add phone');
             $this->check_empty($user_type, 'Please add user_type');
             if(isset($_FILES['profile_pic'])){
                  $profile_pic = $_FILES['profile_pic'];
             }else{
                $profile_pic="";
             }
            

           $where_data = array('id' => $user_id);

            if (empty( $this->db->where( $where_data)->get('ws_users')->row() )) {
	            $data = array( 'status' => 200, 'message' => "User id not exists.");
			  	    $this->response($data, 200);
            }

         
        if(!empty($profile_pic)){
  
          $profilepic = $this->upload_profile_pic($profile_pic);

          if($profilepic['status'] == 200){

            $profile_pic = $profilepic['profile_pic'];
               $paypal_verify = $this->validate_paypal($user_id,$paypalAccount);
                ////////////////////////////
              if($paypal_verify['status'] == 200){
              $data = $this->tank_auth->update_profile_app($user_id, $dob,$gender,$profile_pic,$paypalAccount,$name,$phone,$user_type);
               //print_r($data);die;
                    if($data){
                            if($data['data']['fb_pic'] == ""){ 

                              if($data['data']['profile_pic'] != ""){
                                     $data['data']['profile_pic'] = $this->baseurl.'assets/uploads/profile_pic/'.$data['data']['profile_pic'];
                              }else{
                                     $data['data']['profile_pic'] = $data['data']['profile_pic'];
                              } 

                           }else{
                               $data['data']['profile_pic'] = $data['data']['fb_pic'];
                           }
                        unset($data['data']['fb_pic']);
                      

                        $this->response($data, 200);
                    }else{
                      $data['status']=301;
                      $data['message']="Inccorrect user";

                      $this->response($data, 200);
                    }
                }else{
                    $this->response($paypal_verify, 200);
                }   
                ////////////////////////////
             
          }else{
             $this->response($profilepic, 200); 
          }
        }else{   
        
        $paypal_verify = $this->validate_paypal($user_id,$paypalAccount);
        if($paypal_verify['status'] == 200){
            $data = $this->tank_auth->update_profile_app($user_id, $dob,$gender,$profile_pic,$paypalAccount,$name,$phone,$user_type);
            //print_r($data);die;
            if($data){
                    if($data['data']['fb_pic'] == ""){ 

                      if($data['data']['profile_pic'] != ""){
                             $data['data']['profile_pic'] = $this->baseurl.'assets/uploads/profile_pic/'.$data['data']['profile_pic'];
                      }else{
                             $data['data']['profile_pic'] = $data['data']['profile_pic'];
                      } 

                   }else{
                       $data['data']['profile_pic'] = $data['data']['fb_pic'];
                   }
                unset($data['data']['fb_pic']);
              

                $this->response($data, 200);
            }else{
              $data['status']=301;
              $data['message']="Inccorrect user";

              $this->response($data, 200);
            }
        }else{
            $this->response($paypal_verify, 200);
        }   
      }

    }
    
  private function validate_paypal($user_id,$paypal_email)
  {
         
              
            /*********************************************************/
            /**************  Configuration ******************/
            if(!session_id()) session_start();
            /** 
            * Sandbox Mode - TRUE/FALSE
            */
            $host_split = explode('.',$_SERVER['HTTP_HOST']);
            $sandbox = $host_split[0] == 'sandbox' && $host_split[1] == 'domain' ? TRUE : FALSE;
            $domain = $sandbox ? 'http://sandbox.domain.com/' : 'http://www.domain.com/';

            $sandbox = true;

            /**
            * Enable error reporting if running in sandbox mode.
            */
            if($sandbox)
            {
                error_reporting(E_ALL|E_STRICT);
                ini_set('display_errors', '1'); 
            }
              $api_version = '119.0'; // Released 11.05.2014
              $application_id = $sandbox ? 'APP-80W284485P519543T' : '';
              $developer_account_email = 'pay1_biz@payapi.com';

              $api_username = $sandbox ? 'pay1_biz_api1.payapi.com' : 'LIVE_API_USERNAME';
              $api_password = $sandbox ? 'ETL4QUDTB5WPKQ6M' : 'LIVE_API_PASSWORD';
              $api_signature = $sandbox ? 'AXQluwqnVWQ7M4V49HnBKsJmXrNzA1EQIunpsC9Rj0PWRAr94X6TAgw-' : 'LIVE_API_SIGNATURE';

              $api_subject = ''; // If making calls on behalf a third party, their PayPal email address or account ID goes here.
              $device_id = '';
              $device_ip_address = $_SERVER['REMOTE_ADDR'];
              $print_headers = false;
              $log_results = false;
              $log_path = $_SERVER['DOCUMENT_ROOT'].'/logs/';



            $PayPalConfig = array(
               'Sandbox' => $sandbox,
               'DeveloperAccountEmail' => $developer_account_email,
               'ApplicationID' => $application_id,
               'DeviceID' => $device_id,
               'IPAddress' => $_SERVER['REMOTE_ADDR'],
               'APIUsername' => $api_username,
               'APIPassword' => $api_password,
               'APISignature' => $api_signature,
               'APISubject' => $api_subject,
                           'PrintHeaders' => $print_headers, 
               'LogResults' => $log_results, 
               'LogPath' => $log_path,
            );
            $this->load->library('verify_paypal/verify/adaptive', $PayPalConfig );
            // Prepare request arrays
            $GetVerifiedStatusFields = array(
              'EmailAddress' => $paypal_email, // Required.  The email address of the PayPal account holder.
              'FirstName' => '', // The first name of the PayPal account holder.  Required if MatchCriteria is NAME
              'LastName' => '', // The last name of the PayPal account holder.  Required if MatchCriteria is NAME
              'MatchCriteria' => 'NONE' // Required.  The criteria must be matched in addition to EmailAddress.  Currently, only NAME is supported.  Values:  NAME, NONE   To use NONE you have to be granted advanced permissions
            );

            $PayPalRequestData = array('GetVerifiedStatusFields' => $GetVerifiedStatusFields);

            $verifyResponse = $this->adaptive->GetVerifiedStatus( $PayPalRequestData );

            if ( empty( $verifyResponse['Errors'] ) ) 
            {
                $data = array(
                    'status' => 200,
                    'message' => 'Paypal account is verified'
                  );
            }else{
                $data = array(
                  'status' => 301,
                  'message' => $verifyResponse['Errors'][0]['Message']
                );
            }
            return $data;
  }
    function upload_profile_pic($profilepic){
        $_FILES['profile_pic'] = $profilepic;
        
        $profile_path = '';
                   
         //$uploaddir = $_SERVER["DOCUMENT_ROOT"].'/assets/uploads/profile_pic/';
         $uploaddir = $_SERVER["DOCUMENT_ROOT"].'/assets/uploads/profile_pic/';
        // $uploaddir = str_replace('trunk/', '', $uploaddir);
        if (isset($_FILES['profile_pic']['name'])) {

            //provide config values
            $file_name = $_FILES['profile_pic']['name'];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
          
            $config['upload_path'] = $uploaddir;
            $config['allowed_types'] = 'gif|jpg|png|PNG|JPEG|jpeg|JPG';
            $config['max_size'] = '8192';
            $config['max_width'] = '524000';
            $config['max_height'] = '576800';
            $config['file_name'] = 'profile' . rand() . '.' . $ext;
           
            $this->load->library('upload', $config);
           
            $data = array('upload_data' => $this->upload->data());

            //if the profile pic could not be uploaded
            if (!$this->upload->do_upload('profile_pic')) {
                //print_r($this->upload->display_errors());
                $data = array(
                    'status' => 301,
                    'message' => $this->upload->display_errors()
                );
            }else{
                $data = array(
                    'status' => 200,
                    'message' => "Upload profile successfully",
                    'profile_pic' => $config['file_name']
                );
            }
            //echo $this->baseurl."assets/uploads/profile_pic/".$config['file_name'];die;
           return $data;
       }
  }
  
 
  /**
  Function for check user id exist 
  */
  function check_user_id($user_id){
           $where_data = array('id' => $user_id);
         if(empty( $user= $this->db->where( $where_data)->get('ws_users')->row() )) {

               $data = array( 'status' => 301, 'message' => "User id not exists.");
               $this->response($data, 200);
          }
  } 
/**
   * Send email message of given type (activate, forgot_password, etc.)
   *
   * @param string
   * @param string
   * @param array
   * @return  void
   */
  /* function _send_email($type, $email, &$data)
  {
    // var_dump($data);
    // die('sss');
     // echo "okoko email";die;
    $this->load->library('email', $this->config->item('smtp'));
    $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
    $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
    $this->email->to($email);
	 $this->email->subject("Beach Pass");
    //$this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
    $this->email->message($this->load->view('email/'.$type.'-html', $data, TRUE));
    $this->email->set_alt_message($this->load->view('email/'.$type.'-html', $data, TRUE));
    if (!$this->email->send()) {
      echo $this->email->print_debugger();
    }else{
      // die('ddddddd');
      return TRUE;
    }
  } */
  
   function _send_email_app($type, $email, &$data, $subject){
    // var_dump($data);
    // die('sss');
     // echo "okoko email";die;
    $this->load->library('email', $this->config->item('smtp'));
    $this->email->from($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
    $this->email->reply_to($this->config->item('webmaster_email', 'tank_auth'), $this->config->item('website_name', 'tank_auth'));
    $this->email->to($email);
	 $this->email->subject($subject);
    //$this->email->subject(sprintf($this->lang->line('auth_subject_'.$type), $this->config->item('website_name', 'tank_auth')));
    $this->email->message($this->load->view('email/'.$type.'-html', $data, TRUE));
    $this->email->set_alt_message($this->load->view('email/'.$type.'-html', $data, TRUE));
    if (!$this->email->send()) {
      echo $this->email->print_debugger();
    }else{
      // die('ddddddd');
      return TRUE;
    }
  }
  /*-------notification function start here --------*/

  function curl_hit($post){
      $apiKey = 'AIzaSyDgWuSg302HRjV17bojVMCRfY96kkzLAwg';

      $url = 'https://fcm.googleapis.com/fcm/send';

      $headers = array(
          'Authorization: key=' . $apiKey,
          'Content-Type: application/json'
      );
      // Initialize curl handle
      $ch = curl_init();

      // Set URL to GCM endpoint
      curl_setopt($ch, CURLOPT_URL, $url);

      // Set request method to POST
      curl_setopt($ch, CURLOPT_POST, true);

      // Set our custom headers
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      // Get the response back as string instead of printing it
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      // Set JSON post data
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));

      // Actually send the push
      $result = curl_exec($ch);

      // Close curl handle
      curl_close($ch);
      $result_de = json_decode($result);
      return (array)$result_de;
  }
  function firebaseCloudMessage($params,$message)
  {
            
          switch ($params['type']){
            case 'newWalk':
                $notification_data = array( //when application open then post field 'data' parameter work so 'message' and 'body' key should have same text or value
                    'message'           => $message,
                    'meta_data'           => $params//array('topic' => 'doggystroll'.$params['notifyWalkerId'])
                  
                );
                break;
          }
    
        $notification = array(       //// when application close then post field 'notification' parameter work
          'body'  => $message,
          'sound' => 'default',
          'color'=> '#202720'
      );

      /*---for android---*/
      $topic = "'eventpass".$params['notifyWalkerId']."' in topics";
      $post = array(
          'condition'         => $topic,
          'notification'      => $notification,
          "content_available" => true,
          'priority'          => 'high',
          'data'              => $notification_data
      );
         // print_r($post);die;

      $notifyResult = $this->curl_hit($post);
      //print_r($notifyResult);die;
      return array($notifyResult);
  }
      
}
/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */