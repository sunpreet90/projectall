<!-- page heading start-->
<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="<?=base_url('welcome')?>">Dashboard</a>
        </li>
        <li class="active"> My Dashboard </li>
    </ul>
    <div class="state-info">
        <section class="panel">
            <div class="panel-body">
                <div class="summary">
                    <span>yearly expense</span>
                    <h3 class="red-txt">$ 45,600</h3>
                </div>
                <div id="income" class="chart-bar"></div>
            </div>
        </section>
        <section class="panel">
            <div class="panel-body">
                <div class="summary">
                    <span>yearly  income</span>
                    <h3 class="green-txt">$ 45,600</h3>
                </div>
                <div id="expense" class="chart-bar"></div>
            </div>
        </section>
    </div>
</div>

<div class="container">
    <div class="col-md-12">
                            <!--pagination start-->
                            <section class="panel">
                                <header class="panel-heading">
                                    Initail Collaps bar
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>

                                        <a class="fa fa-times" href="javascript:;"></a>
                                    </span>
                                </header>
                                <div style="display: block;" class="panel-body">
                                    contents goes here
                                </div>
                            </section>
                            <!--pagination end-->
                        </div>
</div>

<!-- page heading end-->