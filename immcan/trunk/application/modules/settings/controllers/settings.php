<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		
		/*if ($this->tank_auth->user_role($this->tank_auth->get_role_id()) != 'admin') {
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('access_denied'));
			redirect('');
		}*/
		//$this->load->model('settings_model');
		$this->load->model('users/user_model');
		$this->load->model('consultant/consultant_model');
		$this->load->model('referral/referral_model');
		$this->load->model('partner/partner_model');


	}

	public function index() {
		            
		if (!$this->tank_auth->is_logged_in()) {
			        
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');
			//$users 	= $this->user_model->get_users_all(); // Total Users
			$users 	= $this->user_model->get_activated_users(); // Total Users
			//echo "<pre>"; print_r($users); die;
           $data['users_count'] = count($users); // count total users
		   $consultant_listing 	= $this->consultant_model->get_consultant_listing(); // Consultant Listing
           $data['consultant_listing_count'] = count($consultant_listing); // count Consultant Listing

           $referral_listing 	= $this->referral_model->get_client_referrels(); // Client Referrels
           $data['referral_listing_count'] = count($referral_listing); // count client Referrels

           $international_partner 	= $this->partner_model->get_international_partner(); // Client Referrels
           $data['international_partner_count'] = count($international_partner); // count client Referrels
		   
			$this->template->set_layout('dashboard')->build('header',isset($data) ? $data : NULL);       
		
		}
	}


	function footer()
	{
		echo "working";
	}

	function update()
	{
		echo "1212";
		/*if ($this->input->post()) {
			if ($this->uri->segment(3) == 'general') {
				$this->_update_general_settings('general');
			}elseif($this->uri->segment(3) == 'system'){
				$this->_update_system_settings('system');
			}elseif($this->uri->segment(3) == 'email'){
				$this->_update_email_settings('email');
			}elseif($this->uri->segment(3) == 'event'){
				$this->_update_event_settings('event');
			}else{
				$this->_update_contract_settings('contract');
			}

		}else{
			$this->load->module('layouts');
			$this->load->library('template');
			$data['form'] = TRUE;
			$this->template->title(lang('settings').' - '.$this->config->item('customer_name'). ' '. $this->config->item('version'));			
			if($this->uri->segment(3) == 'general'){
				$data['page'] = lang('general_settings');
				$setting = 'general';
			}elseif ($this->uri->segment(3) == 'system') {
				$data['page'] = lang('system_settings');
				$setting = 'system';
			}elseif($this->uri->segment(3) == 'email'){
				$data['page'] = lang('email_settings');
				$setting = 'email';
			}elseif($this->uri->segment(3) == 'event'){
				$data['page'] = 'event_settings';
				$setting = 'event';
			}else{
				$data['page'] = 'contract_settings';
				$setting = 'contract';
			}

			$this->template
			->set_layout('users')
			->build($setting,isset($data) ? $data : NULL);
		}*/
	}
	/*private function _update_general_settings($setting){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('customer_name', 'Customer Name', 'required');
		$this->form_validation->set_rules('customer_address', 'Customer Address', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			redirect('settings/update/'.$setting);
		}else{
			foreach ($_POST as $key => $value) {
				$data = array('value' => $value); 
				$this->db->where('config_key', $key)->update('config', $data); 
			}
			$this->session->set_flashdata('response_status', 'success');
			$this->session->set_flashdata('message', lang('settings_updated_successfully'));
			redirect('settings/update/'.$setting);
		}
		
	}
	private function _update_system_settings($setting){
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
		$this->form_validation->set_rules('base_url', 'Base URL', 'required');
		$this->form_validation->set_rules('file_max_size', 'File Max Size', 'required');		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			$_POST = '';
			$this->update('system');
		}else{
		foreach ($_POST as $key => $value) {
				$data = array('value' => $value); 
				$this->db->where('config_key', $key)->update('config', $data); 
			}

			$this->session->set_flashdata('response_status', 'success');
			$this->session->set_flashdata('message', lang('settings_updated_successfully'));
			redirect('settings/update/'.$setting);
		}
		
	}
	private function _update_email_settings($setting){
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
		$this->form_validation->set_rules('customer_email', 'Customer Email', 'required');
		$this->form_validation->set_rules('protocol', 'Email Protocol', 'required');		
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			$_POST = '';
			$this->update('email');
		}else{
			$this->load->library('encrypt');
			$raw_smtp_pass =  $this->input->post('smtp_pass');
			$smtp_pass = $this->encrypt->encode($raw_smtp_pass);
			foreach ($_POST as $key => $value) {
				$data = array('value' => $value); 
				$this->db->where('config_key', $key)->update('config', $data); 
			}
		$data = array('value' => $smtp_pass); $this->db->where('config_key', 'smtp_pass')->update('config', $data); 
		

			$this->session->set_flashdata('response_status', 'success');
			$this->session->set_flashdata('message', lang('settings_updated_successfully'));
			redirect('settings/update/'.$setting);
		}
		
	}
	private function _update_event_settings($setting){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('start_time', 'Start Time', 'required');
		$this->form_validation->set_rules('end_time', 'End Time', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			redirect('settings/update/'.$setting);
		}else{
			foreach ($_POST as $key => $value) {
				$data = array('value' => $value); 
				$this->db->where('config_key', $key)->update('config', $data); 
			}
			$this->session->set_flashdata('response_status', 'success');
			$this->session->set_flashdata('message', lang('settings_updated_successfully'));
			redirect('settings/update/'.$setting);
		}
		
	}
	private function _update_contract_settings($setting){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('contract', 'Contract', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			redirect('settings/update/'.$setting);
		}else{
			foreach ($_POST as $key => $value) {
				$data = array('value' => $value); 
				$this->db->where('config_key', $key)->update('config', $data); 
			}
			$this->session->set_flashdata('response_status', 'success');
			$this->session->set_flashdata('message', lang('settings_updated_successfully'));
			redirect('settings/update/'.$setting);
		}
		
	}
	function update_payment_settings(){
		if ($this->input->post()) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
		$this->form_validation->set_rules('default_currency', 'Default Currency', 'required');
		$this->form_validation->set_rules('default_currency_symbol', 'Default Currency Symbol', 'required');	
		//$this->form_validation->set_rules('paypal_email', 'Paypal Email', 'required');		
		//$this->form_validation->set_rules('paypal_cancel_url', 'Paypal Cancel', 'required');	
		//$this->form_validation->set_rules('paypal_ipn_url', 'Paypal IPN', 'required');	
		//$this->form_validation->set_rules('paypal_success_url', 'Paypal Success', 'required');	
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			$_POST = '';
			$this->update('general');
		}else{
			foreach ($_POST as $key => $value) {
				$data = array('value' => $value); 
				$this->db->where('config_key', $key)->update('config', $data); 
			}


			$this->session->set_flashdata('response_status', 'success');
			$this->session->set_flashdata('message', lang('settings_updated_successfully'));
			redirect('settings/update/general');
		}
	}else{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			redirect('settings/update/general');
	}
		
	}

	function update_email_templates(){
		if ($this->input->post()) {
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
		$this->form_validation->set_rules('email_estimate_message', 'Estimate Message', 'required');
		$this->form_validation->set_rules('email_invoice_message', 'Invoice Message', 'required');	
		$this->form_validation->set_rules('reminder_message', 'Reminder Message', 'required');	
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			$_POST = '';
			$this->update('email');
		}else{
			foreach ($_POST as $key => $value) {
				$data = array('value' => $value); 
				$this->db->where('config_key', $key)->update('config', $data); 
			}

			$this->session->set_flashdata('response_status', 'success');
			$this->session->set_flashdata('message', lang('settings_updated_successfully'));
			redirect('settings/update/email');
		}
	}else{
			$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', lang('settings_update_failed'));
			redirect('settings/update/email');
	}
		
	}
	function upload_logo(){
		if ($_FILES['userfile'] != "") {
				$config['upload_path']   = './resource/images/';
            			$config['allowed_types'] = 'jpg|jpeg|png';
            			$config['max_width']  = '300';
            			$config['max_height']  = '300';
            			$config['remove_spaces'] = TRUE;
            			$config['file_name']  = 'logo';
            			$config['overwrite']  = TRUE;
            			$config['max_size']      = '300';
            			$this->load->library('upload', $config);
						if ($this->upload->do_upload())
						{
							$data = $this->upload->data();
							$file_name = $data['file_name'];
							$data = array(
								'value' => $file_name);
							$this->db->where('config_key', 'customer_logo')->update('config', $data); 
							$this->session->set_flashdata('response_status', 'success');
							$this->session->set_flashdata('message', lang('logo_changed'));
							redirect('settings/update/general');
						}else{
							$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', lang('logo_upload_error'));
							redirect('settings/update/general');
						}
			}else{
							$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', lang('file_upload_failed'));
							redirect('settings/update/general');
			}
	}
	function invoice_logo(){
		if ($_FILES['userfile'] != "") {
				$config['upload_path']   = './resource/images/logos/';
            			$config['allowed_types'] = 'jpg|jpeg|png';
            			$config['remove_spaces'] = TRUE;
            			$config['file_name']  = 'invoice_logo';
            			$config['overwrite']  = TRUE;
            			$this->load->library('upload', $config);
						if ($this->upload->do_upload())
						{
							$data = $this->upload->data();
							$file_name = $data['file_name'];
							$data = array(
								'value' => $file_name);
							$this->db->where('config_key', 'invoice_logo')->update('config', $data); 
							$this->session->set_flashdata('response_status', 'success');
							$this->session->set_flashdata('message', lang('logo_changed'));
							redirect('settings/update/general');
						}else{
							$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', lang('logo_upload_error'));
							redirect('settings/update/general');
						}
			}else{
							$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', lang('file_upload_failed'));
							redirect('settings/update/general');
			}
	}
	function database()
	{
		$this->load->dbutil();
		$prefs = array(
                'format'      => 'txt',             // gzip, zip, txt
                'filename'    => 'latest-freelancerbackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );
			$backup =& $this->dbutil->backup($prefs);
			$this->load->helper('file');
			write_file('resource/database.backup/latest-freelancerbackup.sql', $backup); 
			$this->load->helper('download');
			force_download('latest-freelancerbackup.sql', $backup);
	}*/
	
}

/* End of file settings.php */
