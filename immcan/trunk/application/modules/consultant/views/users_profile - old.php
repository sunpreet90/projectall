<!-- page heading start-->
<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active">User Event History </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                        <header class="panel-heading">
                            Event History
                            <!--
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                             </span>
                         -->
                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Sr.No.</th>
                                        <th>Event Id</th>
                                        <th>Name</th>
                                        <th>Location</th>
                                        <th>Duration</th>
                                        <th>Tickets Purcahsed</th>
                                        <th>Payment Method</th>
                                        <th>Total Amount Spent</th>
                                        <th>Staus</th>
                                        <!--<th class="center">Action</th>--> 
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        /* if ( is_array( $bookings )) 
                                        {
                                            $srno = 1;
                                            foreach ($bookings as $booking) 
                                            { */
                                            ?>
                                            <tr class="gradeX">
                                                <td><?php// echo $srno; ?></td>
                                                <td><?//=$booking['walker_id']?></td>
                                                <td><?//=$booking['owner_id']?></td>
                                                <td><?//=$booking['pick_up_location']?></td>
                                                <td><?//=$booking['created']?></td>
                                                <td><?//=$booking['start_time']?></td>
                                                <td><?//=$booking['end_time']?></td>
                                                <td>
                                                        <?php /* switch ($booking['walk_status']) {
                                                            case 0:
                                                                echo "Upcoming";
                                                                break;
                                                            case 1:
                                                                echo "Ongoing";
                                                                break;
                                                            case 2:
                                                                echo "Completed";
                                                                break;
                                                             case 3:
                                                                echo "Concelled";
                                                                break;    
                                                        } */ ?>
                                                </td>
                                                <td class="center hidden-phone">
                                                        <!--<a title="Edit" href="<?//=base_url('Bookings')?>/edit/id/<?//=$booking['id'];?>">
                                                             <i class="fa fa-pencil" aria-hidden="true"></i>
                                                        </a>-->
                                                        <!--<a title="Delete" href="<?//=base_url('bookings')?>/delete/id/<?//=$booking['id'];?>" onclick="if(!confirm('Are you sure to delete this record?')){return false;}">
                                                           <i class="fa fa-eraser"></i>
                                                        </a>-->
                                                </td> 
                                            </tr>
                                          <?php 
                                             /*     $srno++;
                                             }
                                        } */
                                        ?>
                                    </tbody>
                                    <!--<tfoot>
                                        <tr>
                                             <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>booking Type</th>
                                            <th class="center">Edit</th>
                                            <th class="center">Delete</th>
                                        </tr>
                                    </tfoot>-->
                                </table>
                            </div>
                        </div>
                    </section>
        </div>
    </div>
</div>
<!--body wrapper end-->

