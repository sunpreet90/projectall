    <div class="page-heading">
        <h3>Details Listing</h3>
            
        <ul class="breadcrumb">
            <li><a href="<?= site_url()?>">Dashboard</a></li>
            <li>   <a href="#">Pages</a>
            </li>
            <li class="active"> View All List </li>
        </ul>
    </div>
    <!-- page heading end-->

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    
                    <header class="panel-heading">
                        Consultants &nbsp;&nbsp; <a href="<?=base_url('consultant/add')?>"><button class="btn btn-info" type="button">Add New</button></a>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>

                    <div style="display: block;" class="panel-body">
                         <div class="panel-body">

                            <form class="form-horizontal immcan-form" method="post" action="<?= base_url('consultant') ?>" novalidate="novalidate" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Booking Title</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="booking_title" class="form-control round-input" value="<?=get_config_item('booking_title')?>">
                                    </div>
                                </div>
                                  <div class="form-group ">
                                     <label class="col-sm-2 col-sm-2 control-label">Booking Content:</label>
                                    <div class="col-sm-10">
                                    <textarea name='booking_content' class="form-control ckeditor" rows="9"><?=get_config_item('booking_content')?></textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="booking_consultant_content">
                                <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                            </form>

                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>type</th>
                                        <th>Via</th>
                                        <th>Duration</th>
                                        <th>Price</th>
                                        <th class="hidden-phone">Edit</th>
                                        <th class="hidden-phone">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <?php 
                                if ( !empty( $consultants )) 
                                {
                                    foreach ($consultants as $note) 
                                    {
                                    ?>
                                        <tr class="gradeX">
                                            <td><?=$note['consultant_name']?></td>
                                            <td><?=$note['consultation_type_name']?></td>
                                            <td><?=$note['consultation_via']?></td>
                                            <td><?=$note['consultant_duration']?></td>
                                            <td><?=$note['consultation_price']?></td>
                                            <td class="center hidden-phone"><a href="<?=base_url('consultant')?>/edit/id/<?=$note['id'];?>">Edit</a></td>
                                            <td class="center hidden-phone">
                                                <a href="<?=base_url('consultant')?>/delete_consultant/id/<?=$note['id'];?>"><i class="fa fa-eraser"></i></a>
                                            </td>
                                        </tr>
                            <?php   }
                                }
                            ?>
                                </tbody>

                                <tfoot>
                                        <tr>
                                        <th>Name</th>
                                        <th>type</th>
                                        <th>Via</th>
                                        <th>Duration</th>
                                        <th>Price</th>
                                        <th class="hidden-phone">Edit</th>
                                        <th class="hidden-phone">Delete</th>
                                        </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>