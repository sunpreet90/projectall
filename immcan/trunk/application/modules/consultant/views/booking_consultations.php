    <div class="page-heading">
        <h3>Booking Consultation Details Listing</h3>
            
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url()?>">Dashboard</a>
            </li>
            <li>
                <a href="#">Pages</a>
            </li>
            <li class="active"> View All List </li>
        </ul>
         <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>
    </div>
    <!-- page heading end-->

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Booking Consultation Details Listing
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Transaction ID</th>
                                        <th>Immigration Type</th>
                                        <th>Immigration Status</th>
                                        <!-- <th class="hidden-phone">Edit</th> -->
                                        <th class="hidden-phone">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <?php 
                                if ( !empty( $booked_consultant )) 
                                {
                                    foreach ($booked_consultant as $booked) 
                                    {
                                    ?>
                                        <tr class="gradeX">
                                            <td><?=$booked['firstname']?></td>
                                            <td><?=$booked['lastname']?></td>
                                            <td><?=$booked['email']?></td>
                                            <td><?=$booked['phone']?></td>
                                            <td>$<?=$booked['transactionID']?></td>
                                            <td>$<?=$booked['immigration_type']?></td>
                                            <td>$<?=$booked['immigration_status']?></td>
                                            <!-- <td class="center hidden-phone"><a href="#">Edit</a></td> -->
                                            <td class="center hidden-phone">
                                                <a href="<?=base_url('consultant')?>/deletes_booked/id/<?=$booked['id'];?>"><i class="fa fa-eraser"></i></a>
                                            </td>
                                        </tr>
                            <?php   }
                                }
                            ?>
                                </tbody>

                                <tfoot>
                                        <tr>
                                        <th>Name</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Transaction ID</th>
                                        <th>Immigration Type</th>
                                        <th>Immigration Status</th>
                                        <!-- <th class="hidden-phone">Edit</th> -->
                                        <th class="hidden-phone">Delete</th>
                                        </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>