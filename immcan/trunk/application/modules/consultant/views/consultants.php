<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_page.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/data-tables/DT_bootstrap.css" rel="stylesheet">
<!-- page heading start-->
<div class="page-heading">
    <h3>
        Consultants Listing
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href=" <?= site_url()?>">Dashboard</a>

        </li>
        <li class="active"> Consultants Listing </li>
    </ul>
    <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>
</div>
<!-- page heading end-->
<?php  //pr($users); die; ?>

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                  Consultants Listing &nbsp; &nbsp;
                   <a href="<?=base_url('consultant/add')?>"><button class="btn btn-info" type="button">Add New</button></a>
              </header>

                <div class="panel-body">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                            <thead>
                            <tr>
                                <th>Sr.No.</th>
                                <th>Name</th>
                                <th>Heading</th>
                                <th>Sub Heading</th>
                                <th>Timing</th>
                                <th>Price</th>
                                <th>Image</th>
                                <th class="center">Action</th> 
                            </tr>
                            </thead>
                            <tbody>
                                <?php 
                                //echo "<pre>"; print_r($consultants); die;
                               
                                if ( is_array( $consultants )) 
                                {
                                    $srno = 1;
                                    foreach ($consultants as $consultant) 
                                    {
                                      //if(!empty($user['name']) && !empty($user['email'])){
                                    ?>
                                <tr class="gradeX">
                                    <td><?php echo $srno; ?></td>
                                    <td><?=$consultant['consultant_name']?></td>
                                    <td><?=$consultant['consultant_heading']?></td>
                                    <td><?=$consultant['consultant_subheading']?></td>
                                    <td><?=$consultant['consultant_timing']?></td>
                                    <td><?=$consultant['consultant_price']?></td>
                                    <!-- <td><?=$consultant['consultant_description']?></td> -->
                                    <td>
                         <?php if($consultant['consultant_image'] != ''){
                                 //echo $event['event_image'];
                                 if($consultant['consultant_image'] == '0'){ ?>
                                      <img src='<?php echo base_url()."uploads/default_photo.png" ?>' width="150" height="120" >
                                <?php }else{ ?>
                                      <img src='<?php echo base_url()."uploads/".$consultant['consultant_image'] ?>' width="150" height="120" >
                                 <?php }
                                 ?>
                        
                         <?php } else { ?>
                          <img src='<?php echo base_url()."uploads/default_photo.png" ?>' width="150" height="120" >
                        
                         <?php } ?>
                         </td>
                                     <td class="center hidden-phone">
                                                <a title="Edit" href="<?=base_url('consultant')?>/view/id/<?=$consultant['id'];?>">
                                                     <i class="fa fa-eye" aria-hidden="true"></i>
                                                </a>
                                                <a title="Edit" href="<?=base_url('consultant')?>/edit/id/<?=$consultant['id'];?>">
                                                     <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                                <a title="Delete" href="<?=base_url('consultant')?>/delete/id/<?=$consultant['id'];?>" onclick="if(!confirm('Are you sure to delete this record?')){return false;}">
                                                   <i class="fa fa-eraser"></i>
                                                </a>
                                        </td> 
                                </tr>
                                <?php $srno++;  }
                                   }
                                //}
                                ?>
                               
                            </tbody>
                         
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<!--Data tables script-->




<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=base_url('assets')?>/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="<?=base_url('assets')?>/text/javascript" src="<?=base_url('assets')?>/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="<?=base_url('assets') ?>/js/dynamic_table_init.js"></script>

<!--common scripts for all pages-->
<script src="<?=base_url('assets') ?>/js/scripts.js"></script>

 <script type="text/javascript">
         <!--
            function Warn() {
               
              var retVal = confirm("Warning!You want to delete this user.");
               if( retVal == true ){
                  return true;
               }
               else{
                  return false;
               }
               
            }
         //-->
      </script>



<!--body wrapper end-->