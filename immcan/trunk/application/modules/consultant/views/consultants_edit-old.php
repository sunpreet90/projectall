<?php //echo "<pre>";print_r(validation_errors());?>
<!-- page heading start-->

<?php //echo validation_errors(); ?>
<div class="page-heading">
    <h3>
        Consultant Edit
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> Consultant Edit </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
               
                <header class="panel-heading">
                    Consultant Edit
                </header>
                <div class="panel-body">
                    <form class="form-horizontal adminex-form" method="post" action="<?=base_url()?>consultant/edit/id/<?=$user['id']?>" >
                        <?php //echo "<pre>"; print_r($user); ?>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Name</label>
                            <div class="col-sm-10">
                                <input  type="text" name="name" value="<?=$user['name']?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Heading</label>
                            <div class="col-sm-10">
                                <input  type="text" name="consultant_heading" value="<?=$user['consultant_heading']?>"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Subheading</label>
                            <div class="col-sm-10">
                                <input  type="text" name="consultant_subheading" value="<?=$user['consultant_subheading']?>"  class="form-control" placeholder="">
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Timimg</label>
                            <div class="col-sm-10">
                                <input  type="text" name="consultant_timing" value="<?=$user['consultant_timing']?>" class="form-control" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Price</label>
                            <div class="col-sm-10">
                                <input  type="text" name="consultant_price" value="<?=$user['consultant_price']?>" class="form-control" placeholder="">
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control required_shop_image error file-upload" name="consultant_image" value="<?=$user['consultant_image']?>" onchange="ValidateSingleInput(this);" /><?=$user['consultant_image']?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Description</label>
                            <div class="col-sm-10">
                                <textarea name="consultant_description" class="my-textarea" value="<?= $user['consultant_description']; ?>"><?= $user['consultant_description']; ?></textarea>
                                <!-- <input value="<?php echo set_value('consultant_description'); ?>" type="text" name="consultant_description"  class="form-control" placeholder=""> -->
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" value="<?=$user['name']?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" name="email" value="<?=$user['email']?>"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input type="text" name="phone" value="<?=$user['phone']?>"  class="form-control" placeholder="">
                            </div>
                        </div> -->
                        
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Save Consultant</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->

