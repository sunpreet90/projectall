<?php //echo "<pre>";print_r(validation_errors());?>
<!-- page heading start-->
<style type="text/css">
.searchable-container{margin:20px 0 0 0}
.searchable-container label.btn-default.active{background-color:#007ba7;color:#FFF}
.searchable-container label.btn-default{width:90%;border:1px solid #efefef;margin:5px; box-shadow:5px 8px 8px 0 #ccc;}
.searchable-container label .bizcontent{width:100%;}
.searchable-container .btn-group{width:100%}
.searchable-container .btn span.glyphicon{
    opacity: 0;
}
.searchable-container .btn.active span.glyphicon {
    opacity: 1;
}
</style>
<!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
<script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

<!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!--Font Awesome (added because you use icons in your prepend/append)-->
<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
<!-- Inline CSS based on choices in "Settings" tab -->
<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>
<?php //echo validation_errors(); ?>
<div class="page-heading">
    <h3>
        Consultant Edit
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> Consultant Edit </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
               
                <header class="panel-heading">
                    Consultant Edit
                </header>
                <div class="panel-body">
                    <form class="form-horizontal adminex-form" id="#consult_form" method="post" action="<?=base_url()?>consultant/edit/id/<?=$user['id']?>" enctype='multipart/form-data'>
                        <?php //echo "<pre>"; print_r($user); die; ?>

                           
                       <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Edit Consultant Name</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_name" value="<?=$user['consultant_name']?>" class="form-control" id="consultant_name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Edit Consultant Type</label>
                            <div class="col-lg-10">
                                <?php 
                                $categories = $this->db->query("SELECT * FROM `ws_consultation_types` ")->result_array();
                                ?>

                                <select name="consultation_type_name" class="form-control m-bot15">
                                          <option value="<?=$user['consultation_type_name']?>"><?=$user['consultation_type_name']?></option>
                                          
                                        <?php foreach ($categories as $rows): ?>
                                          <option value="<?php echo $rows['consultation_type']; ?>"><?php echo $rows['consultation_type']; ?></option>
                                          <?php endforeach; ?>
                                        
                                 </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Edit Consultant Via</label>
                            <div class="col-lg-10">

                                 <select name="consultation_via" class="form-control m-bot15"> 
                                        <option value="Email" <?php if($user['consultation_via']== 'Email'){echo "Selected";} ?>>Email</option>
                                        <option value="Skype" <?php if($user['consultation_via']== 'Skype'){echo "Selected";} ?>>Skype</option>
                                        <option value="Phone" <?php if($user['consultation_via']== 'Phone'){echo "Selected";} ?>>Phone</option>
                                 </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Edit Consultant Duration</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_duration" value="<?=$user['consultant_duration']?>" class="form-control" id="consultant_duration">
                            </div>
                        </div>      
                        

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Edit Consultant Price</label>
                            <div class="col-lg-10">
                                <!-- onkeypress="return isNumberKey(event)" -->
                                <input type="text" name="consultant_price" value="<?=$user['consultation_price']?>" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Edit Consultant Image:</label>
                            <div class="col-sm-10">
                                <input type="file" name="consultant_image" class="form-control required_shop_image error file-upload" id="featured_image"  onchange="ValidateSingleInput(this);">
                                <img class="featured_image" src="<?=base_url()?>/<?php echo $user['consultant_image']; ?>" style="width: 20%;">  
                                 
                            </div>
                        </div>


                        <!-- <div class="form-group ">
                            <label for="title" class="control-label col-lg-2">Image</label>
                            <div class="col-lg-10">
                                <input type="file" name="featured_image" class=" form-control" value="<?php echo $page['featured_image']; ?>" id="featured_image">
                                <img class="featured_image" src="<?=base_url()?>/<?php echo $page['featured_image']; ?>">
                            </div>
                        </div> -->




                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Consultant description</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='consultant_description' class="wysihtml5 form-control" rows="9"><?=$user['consultant_description']?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Consultant long description</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='consultant_long_description' class="wysihtml5 form-control" rows="9"><?= ( !empty($user) ? $user['consultant_long_description'] : 0); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Consultant Terms</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='post_terms' class="wysihtml5 form-control" rows="9"><?= ( !empty($user) ? $user['consultant_terms'] : 0); ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Edit Profile URL</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_url" value="<?=$user['profile_url']?>" class="form-control" id="" placeholder="for e.g. https://calendly.com/sachin-gupta-1">
                            </div>
                        </div>

                         <!-- <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Week Off</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <?php $WeekOff = explode(',',$user['week_off']); 
                                            //print_r($WeekOff);
                                            //print_r($user['selected_slots'][0]);
                                        ?>
                                        <span><input type="checkbox" name="day[]" value="1" <?php if(in_array(1, $WeekOff)){echo "checked";} ?>/>Monday</span>
                                        <span><input type="checkbox" name="day[]" value="2" <?php if(in_array(2, $WeekOff)){echo "checked";} ?>/>Tueday</span>
                                        <span><input type="checkbox" name="day[]" value="3" <?php if(in_array(3, $WeekOff)){echo "checked";} ?>/>Wednesday</span>
                                        <span><input type="checkbox" name="day[]" value="4" <?php if(in_array(4, $WeekOff)){echo "checked";} ?>/>Thursday</span>
                                        <span><input type="checkbox" name="day[]" value="5" <?php if(in_array(5, $WeekOff)){echo "checked";} ?>/>Friday</span>
                                        <span><input type="checkbox" name="day[]" value="6" <?php if(in_array(6, $WeekOff)){echo "checked";} ?>/>Saturday</span>
                                        <span><input type="checkbox" name="day[]" value="0" <?php if(in_array(0, $WeekOff)){echo "checked";} ?>/>Sunday</span>
                                    </div>
                                </div>
                            </div>
                        </div>                      



                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Not Available Dates</label>
                            <div class="col-lg-3">
                           <div class="input-group">
                            <div class="input-group-addon">
                             <i class="fa fa-calendar">
                             </i>
                            </div>
                            <input class="form-control" id="date" name="datetimepicker" placeholder="MM/DD/YYYY" type="text"/>
                           </div>
                         </div>
                        </div> -->

                        <!-- <div class="form-group" id="available-date">
                            <label for="title" class="control-label col-lg-2">Not Available Dates</label>
                            <div class="col-lg-10">
                                  <input id="datepicker" width="270" onclick="$('#datepicker').datepicker();$('#dp').datepicker('show');" /> 
                                 <input name="datetimepicker" id="datetimepicker" width="270" />
                            </div>
                        </div> -->
                       

                        <!-- <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Not Available Slot Date</label>
                            <div class="col-lg-3">                                 
                                 <input type="date" name="slot_date" value="<?php echo date("Y-m-d");?>" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Choose Slots for disable</label>
                            <div class="col-lg-10">                                 
                                 <?php foreach($user['slots'] as $slot){ 
                                        $selected_slots = explode(',',$user['selected_slots'][0]['slot_num']);
                                    ?>
                                    <div class="searchable-container">
                                        <div class="items col-xs-5 col-sm-5 col-md-2 col-lg-2">
                                            <div class="info-block block-info clearfix">                                                 
                                                <div data-toggle="buttons" class="btn-group bizmoduleselect">
                                                    <label class="btn btn-default <?php if(in_array($slot['slot_num'], $selected_slots)){echo "active";} ?>">
                                                        <div class="bizcontent">
                                                            <input type="checkbox" name="slots[]" autocomplete="off" value="<?php echo $slot['slot_num']; ?>" <?php if(in_array($slot['slot_num'], $selected_slots)){echo "checked";} ?>>
                                                            <h5><?php echo $slot['start_time']." to ".$slot['end_time']; ?> </h5>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>                                
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div id="appendTimeslots">
                        <div class='form-group event_timeslot' id='from_div' style="display: none;">
                            <label for='title' class="control-label col-lg-2">From</label>
                           
                            <div class='col-lg-2'>
                                <select name='from-hour[]' class='from-hour' id='from-hour'>
                                    <option value='00'>00</option>
                                    <option value='01'>01</option>
                                    <option value='02'>02</option>
                                    <option value='03'>03</option>
                                    <option value='04'>04</option>
                                    <option value='05'>05</option>
                                    <option value='06'>06</option>
                                    <option value='07'>07</option>
                                    <option value='08'>08</option>
                                    <option value='09'>09</option>
                                    <option value='10'>10</option>
                                    <option value='11'>11</option>
                                    <option value='12'>12</option>                                    
                                    </select>

                                    <select name='from-minute[]' class='from-minute' id='from-minute'>
                                    <option value='00'>00</option>                                    
                                    <option value='05'>05</option>                                    
                                    <option value='10'>10</option>                                    
                                    <option value='15'>15</option>                                   
                                    <option value='20'>20</option>                                    
                                    <option value='25'>25</option>                                    
                                    <option value='30'>30</option>                                   
                                    <option value='35'>35</option>                                    
                                    <option value='40'>40</option>                                    
                                    <option value='45'>45</option>                                    
                                    <option value='50'>50</option>                                   
                                    <option value='55'>55</option>
                                    </select>

                                    <select name='from-daystatus[]' class='from-daystatus' id='from-daystatus'>
                                    <option value='am'>AM</option>
                                    <option value='pm'>PM</option>
                                                                        
                                    </select>
                            </div>
                       

                            <label for="title" class="control-label col-lg-2">To</label>
                            <div class="col-lg-2">
                                <select name='to-hour[]' class='to-hour' id='to-hour'>
                                    <option value='00'>00</option>
                                    <option value='01'>01</option>
                                    <option value='02'>02</option>
                                    <option value='03'>03</option>
                                    <option value='04'>04</option>
                                    <option value='05'>05</option>
                                    <option value='06'>06</option>
                                    <option value='07'>07</option>
                                    <option value='08'>08</option>
                                    <option value='09'>09</option>
                                    <option value='10'>10</option>
                                    <option value='11'>11</option>
                                    <option value='12'>12</option>                                    
                                    </select>



                                    <select name='to-minute[]' class='to-minute' id='to-minute'>
                                    <option value='00'>00</option>                                    
                                    <option value='05'>05</option>                                    
                                    <option value='10'>10</option>                                    
                                    <option value='15'>15</option>                                   
                                    <option value='20'>20</option>                                    
                                    <option value='25'>25</option>                                    
                                    <option value='30'>30</option>                                   
                                    <option value='35'>35</option>                                    
                                    <option value='40'>40</option>                                    
                                    <option value='45'>45</option>                                    
                                    <option value='50'>50</option>                                   
                                    <option value='55'>55</option>                                    
                                    </select>

                                    <select name='to-daystatus[]' class='to-daystatus' id='to-daystatus'>
                                    <option value='am'>AM</option>
                                    <option value='pm'>PM</option>                                                                        
                                    </select>
                            </div>


                        <div class="col-lg-2"><button type="button" id="btnAddControl" name="">+</button>          
                        </div>             
                        </div> 
</div> -->
                       
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary editconsevent" type="submit">Save Consultant</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->

<!-- <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js" type="text/javascript"></script> -->



<script type="text/javascript">

var $startDate = jQuery('#datetimepicker');

var dateToday = new Date();
var holydays = ['05/17/2018', '05/18/2018', '05/19/2018'];
//$("#datetimepicker").datetimepicker({
$startDate.datetimepicker({
    inline:true,
    sideBySide: true,
    //datepicker:true,
    selectMultiple:true,
    timepicker:false,
    //minDate: dateToday,
    //maxDate: "+2Y",
    //defaultDate: "+1w",
    //dateFormat: 'mm/dd/yy',
    numberOfMonths: 1,
    minDate: "0",
    disabledDates:['2018/04/26','2018/04/28','2018/04/29'],
    //startDate:  '2018/04/23',
    startDate:  dateToday,
    allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
    value:'',
    step:10,
    beforeShowDay: function(date) {
    var show = true;
    if(date.getDay()==0||date.getDay()==6) show=false
        return [show];
    }
    
});

$startDate.on('change', function() {
  var selectedDate = jQuery(this).val();
  jQuery(".xdsoft_time").addClass('.xdsoft_current');
  //alert(selectedDate);
  $('#from_div').show();
  $('#available-date').hide();
});


function highlightDays(date) {
    for (var i = 0; i < holydays.length; i++) {
        if (new Date(holydays[i]).toString() == date.toString()) {
            return [true, 'highlight'];
        }
    }
    return [true, ''];

}

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
        //alert('Only numbers allowed.');
        return false;
    } else {
    return true;
    }
}
</script>


<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery("#btnAddControl").click(function () { 
        //alert("yes");
        /*jQuery("#from_div").append("<div class='form-group' id='from_div'><label for='title' class='control-label col-lg-2'>From</label><div class='col-lg-2'><select name='from-hour' id='from-hour'>=<option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><select name='from-minute' id='from-minute'><option value='00'>00</option><option value='05'>05</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option><option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option><option value='45'>45</option><option value='50'>50</option><option value='55'>55</option></select><select name='from-daystatus' id='from-daystatus'><option value='am'>AM</option><option value='pm'>PM</option></select></div><label for='title' class='control-label col-lg-2'>To</label><div class='col-lg-2'><select name='to-hour' id='to-hour'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><select name='to-minute' id='to-minute'><option value='00'>00</option><option value='05'>05</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option><option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option><option value='45'>45</option><option value='50'>50</option><option value='55'>55</option></select><select name='to-daystatus' id='to-daystatus'><option value='am'>AM</option><option value='pm'>PM</option></select></div><div class='col-lg-2'><button type='button' id='' name="">+</button><button type='button' name=''>-</button></div></div>");*/
        jQuery("#appendTimeslots").append("<div class='form-group event_timeslot'><label for='title' class='control-label col-lg-2'>From</label><div class='col-lg-2'><select name='from-hour[]' class='from-hour' id='from-hour'>=<option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><select name='from-minute[]' class='from-minute' id='from-minute'><option value='00'>00</option><option value='05'>05</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option><option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option><option value='45'>45</option><option value='50'>50</option><option value='55'>55</option></select><select name='from-daystatus[]' class='from-daystatus' id='from-daystatus'><option value='am'>AM</option><option value='pm'>PM</option></select></div><label for='title' class='control-label col-lg-2'>To</label><div class='col-lg-2'><select name='to-hour[]' class='to-hour' id='to-hour'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option></select><select name='to-minute[]' class='to-minute' id='to-minute'><option value='00'>00</option><option value='05'>05</option><option value='10'>10</option><option value='15'>15</option><option value='20'>20</option><option value='25'>25</option><option value='30'>30</option><option value='35'>35</option><option value='40'>40</option><option value='45'>45</option><option value='50'>50</option><option value='55'>55</option></select><select name='to-daystatus[]' class='to-daystatus' id='to-daystatus'><option value='am'>AM</option><option value='pm'>PM</option></select></div><a href='#' class='remove_field'>Remove</a></div>");
        });
        jQuery('#appendTimeslots').on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); jQuery(this).parent('div').remove(); x--;
            });








        /*--add event date on edit consultenent event start--*/
            /*jQuery(".editconsevent").on("click",function(event){
                event.preventDefault();
                jQuery(".event_timeslot").each(function(){
                    var from_hour = jQuery(this).find(".from-hour option:selected").val();
                    var from_minute = jQuery(this).find(".from-minute option:selected").val();
                    var from_daystatus = jQuery(this).find(".from-daystatus option:selected").val();
                    

                    var to_hour = jQuery(this).find(".to-hour option:selected").val();
                    var to_minute = jQuery(this).find(".to-minute option:selected").val();
                    var to_daystatus = jQuery(this).find(".to-daystatus option:selected").val();
                    alert(from_hour);
                    $.ajax( {
                        type: 'post',
                        url: baseurl + 'consultant/edit',
                        data: { from_hour: from_hour, from_minute: from_minute, from_daystatus: from_daystatus, to_hour: to_hour, to_minute: to_minute, to_daystatus: to_daystatus },
                        success: function ( result )
                        {
                            if(result == '0'){
                                jQuery(".custommessage").html('<div class="alert alert-warning"><strong>Warning!</strong> Email Already Exist.</div>');
                            }else{
                                jQuery(".custommessage").html('<div class="alert alert-success"><strong>Success!</strong> Successfully Register.</div>');
                                setTimeout(function(){
                                   
                                }, 1000);
                            }
                        },
                        error: function ( result )
                        {

                        }
                    });


                });
            });*/
        /*--//add event date on edit consultenent event end--*/
});
    
</script>

<script type="text/javascript">
    $(function() {
    $('#search').on('keyup', function() {
        var pattern = $(this).val();
        $('.searchable-container .items').hide();
        $('.searchable-container .items').filter(function() {
            return $(this).text().match(new RegExp(pattern, 'i'));
        }).show();
    });
});
</script>


<!-- <script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script> -->
<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script type="text/javascript">
    /*$(document).ready(function () {        
        $('.default-date-picker').datepicker({
            multidate: true,
            format: 'Y/m/d'
        //format: 'mm-dd-yyyy'
    });
        //$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

    });*/
/*    $(document).ready(function () {
    $('#datepicker').datepicker({         
        onSelect: function() {
          var date = $(this).val();
          console.log(date);
      }
      //uiLibrary: 'bootstrap'
    });
});*/

$(document).ready(function(){
        var date_input=$('input[name="datetimepicker"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
        multidate: true,
            format: 'yyyy/mm/dd',
            container: container,
            todayHighlight: true,
            autoclose: false,
        })
    })
</script>
