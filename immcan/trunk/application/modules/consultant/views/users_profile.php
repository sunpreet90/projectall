<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">
<!--<link rel="stylesheet" type="text/css" href="<?//=base_url('assets') ?>/css/extra.css">-->
<style>
.jumbotron .col-sm-4 {
    background: #fff none repeat scroll 0 0;
    margin: 6px;
    padding: 20px;
    width: 32%;
}
.jumbotron {
    padding-bottom: 48px;
    padding-top: 48px;
    width: 100%;
}
.shop-cls-main .existing-cls-div .message-display-cls {
    display: table-cell;
    vertical-align: middle;
}
.jumbotron .col-sm-3 {
  background: #fff none repeat scroll 0 0;
  display: table;
  height: 189px;
  margin: 1%;
  padding: 10px;
  vertical-align: middle;
  width: 23%;
}
</style>
<!-- page heading start-->
<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active">User Event History </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper buyer-view">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-10">
                <header>
                    <h1>User Event History</h1>
                </header>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-2">
            	<?php //if($edit_allowed == 1){?>
            	<?php //if($buyer_data['status'] == 'suspended' || $buyer_data['status'] == 'pending'){ ?>
            		<!--<a href="#" data-user-id="<?//= $buyer_data['user_id'];?>" data-action="active" class="statusBtn btn btn-success">Activate</a>-->
            	<?php// }else{ ?>
            		<a href="#" data-user-id="<?//= $buyer_data['user_id'];?>" data-action="suspended" class="statusBtn btn btn-success">Suspend User</a>
            	<?php //}} ?>
            </div>
        </div>
        <div class="row">
            <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php }else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php }else if($this->session->flashdata('warning')){  ?>
        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
        </div>
    <?php }else if($this->session->flashdata('info')){  ?>
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
        </div>
    <?php } ?>
    </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="jumbotron">
                        <div class="col-sm-15 text-align-center">
						<img src="<?= base_url().'uploads/default_photo.png';?>" class="img-rounded" width="150" height="150">
                        	
                          <div class="row">
                                <label>Name : </label>
                                <span><?= $buyer_data['name']; ?></span>
                            </div>
                            <div class="row">
                                <label>Email : </label>
                                <span><?= $buyer_data['email']; ?></span>
                            </div>
                            
                            <div class="row">
                                <label>Last Location : </label>
                                <span><?//= $buyer_data['address']; ?></span>
                            </div>
                        </div>
                        <div class="col-sm-15">
                            <div class="row">
                                <label>ID : </label>
                                <span><?=$buyer_data['id']?></span>
                            </div>
                            <div class="row">
                                <label>Join Date : </label>
                                <span><?=date('Y-m-d', strtotime($buyer_data['created']));?></span>
                            </div>
                            <div class="row">
                                <label>Last Login : </label>
                                <!--<span><?//=date('Y-m-d', strtotime($buyer_data['last_login']));?></span>-->
                                <span><?=date('Y-m-d', strtotime($buyer_data['modified']));?></span>
                            </div>
                           
                        </div>
                           
            </div>
            </div>
        </div>
<!--=====================  order_details table start ================ -->
    <div class="col-lg-12">
        <section class="panel">
			 <header class="panel-heading center-text">
                Filter Events Details
            </header>
			
            <div class="filter-users">
				
			<form id="filterform" class="" action="#" method="post" role="">
                <!--<div class="col-md-3 col-sm-3">
					<div class="form-group label-floating is-empty">
						<label class="control-label">Order Date Range</label>
						<div class="date"><input type="text" class="datepicker" id="orderDetailsDateMin"><i class="fa fa-calendar-o" aria-hidden="true"></i> </div>
						<span> To </span>
						<div class="date"><input type="text" class="datepicker" id="orderDetailsDateMax"> <i class="fa fa-calendar-o" aria-hidden="true"></i></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3">
					<div class="form-group label-floating is-empty">
					<label class="control-label">Quantity</label>
					<div class="date"><input type="text" id="orderDetailsQuantityMin" /></div>
					<span> To </span>
					<div class="date"><input type="text" id="orderDetailsQuantityMax" /></div>
					</div>
				</div>-->
							
				
				<div class="col-md-2 col-sm-3">
				 <label for="usr">Filter Duration</label>
				<select class="form-control" id="category_select">
				<option value="all">All</option>
				<option value="day">Day</option>
				<option value="week">Week</option>
				<option value="month">Month</option>
				<option value="season">Season</option>				
				</select>
				</div>
				
				
				<div class="col-md-3 col-sm-3">
					<div class="form-group label-floating is-empty">
					<label class="control-label">Status</label>
						<select class="form-control category_select">
							<option value="all">All</option>
							<option value="inactive">Inactive</option>
							<option value="active">Active</option>
							<option value="expired">Expired</option>
						</select>
					</div>
				</div>
				<!--<div class="col-md-3 col-sm-3">
					<div class="form-group label-floating is-empty">
					<label class="control-label">Location</label>
						<input type="text" id="orderDetailsLocation" /></div>
				</div>-->
				<!-- <div class="col-md-3 col-sm-3">					
					<div id="custom-search-input">
						<div class="input-group col-md-12 genrate-report">
							<input type="text" placeholder="Search" class=" search-query form-control">
							<span class="input-group-btn">
								<button type="button" class="btn btn-danger">
									<span class=" glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</div>	
				</div> -->	
			</form>	
            </div>
		
            <div class="panel-body">
                <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="dynamic-table1">
                        <thead>
							<tr>
								<th>S.No</th>
								<th>Event ID</th>
								<th>User Id</th>
								<th>Name</th>
								<th>Location</th>
								<th>Duration</th>
								<th>Tickets Purchased</th>
								<th>Payment Method</th>
								<th>Total Amount Spent</th>
								<th>Status</th>
								
							</tr>
						</thead>
                        <tbody>
							
                            <!--<tr class="gradeX">
	                                <td> 1 <?//= $countOrd; ?></td>
	                                <td>1</td>
	                                <td>11</td>
	                                <td>Shaan</td>
	                                <td>chandigarh</td>
	                                <td>week</td>
	                                <td>12</td>
	                                <td>1452</td>
	                                <td>3080</td>
	                                <td>Expired</td>
	                            </tr>
								
								<tr class="gradeX">
	                                <td>2 <?//= $countOrd; ?></td>
	                                <td>2</td>
	                                <td>12</td>
	                                <td>Aman</td>
	                                <td>punjab</td>
	                                <td>Month</td>
	                                <td>10</td>
	                                <td>1152</td>
	                                <td>2880</td>
	                                <td>Inactive</td>
	                            </tr>-->
                        
                        </tbody>
                    </table>
                </div>
				<div class="clearfix"></div>
            </div> 
        </section>
    </div>
<!--===================== order_details table end ================ -->		
    </div>
<!--body wrapper end-->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
 <!--<link href="jquery-ui.css" rel="stylesheet">-->
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	  
	   <script>
         $(function() {
            $( ".datepicker" ).datepicker();       
         });
      </script>
      <!-- Javascript -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js">
</script>
<!--Add custom filter methods-->
<script type="text/javascript">
   	<!-- Sort BY Event Status start -->
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var location = $(".category_select option:selected").val();
			//console.log("location");
            var columnValue = data[9];

            if(location.toLowerCase() == 'all'){
            	return true;
            }
            if(columnValue.toLowerCase().indexOf(location) >= 0){
              return true;
            }
            return false;
        }
    );
	
		<!-- Sort BY Event Status start End-->
	
	<!-- Sort BY Event Duration start -->
	$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var location = $("#category_select option:selected").val();
			//console.log("location");
            var columnValue = data[5];

            if(location.toLowerCase() == 'all'){
            	return true;
            }
            if(columnValue.toLowerCase().indexOf(location) >= 0){
              return true;
            }
            return false;
        }
    );
	<!-- Sort BY Event Duration End-->

	

</script>
<!--dynamic table initialization -->
<script>
$(document).ready(function() {
   //var table = $('table.display').DataTable({});
   var table = $('table.display').DataTable( {
            "scrollX": true,
            "aaSorting": [[ 4, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend : 'excelHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8,9]
                    }
                },
                {
                    extend : 'csvHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8,9]
                    }
                },
                {
                    extend : 'pdfHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8,9]
                    }
                }]
        });
   
   $(".category_select").on('change', function(){
	   //alert("hello");
   	table.draw();
   });
   $("#category_select").on('change', function(){
	   //alert("hello");
   	table.draw();
   });
   
 } );
</script> 
<style>
.date input {
  background: transparent none repeat scroll 0 0 !important;
  border: 1px solid;
  position: relative;
  width: 100%;
  z-index: 1;
}
.date .fa.fa-calendar-o {
  font-size: 14px;
  position: absolute;
  right: 5px;
  top: 8px;
  z-index: 0;
}
.date {
  display: inline-block;
  position: relative;

}

.adv-table .dataTables_filter label {
  line-height: 2;
  vertical-align: middle;
  width: 100%;
}
.adv-table .dataTables_filter label input {
  float: right;
  height: 25px;
  width: auto;
}
</style>