  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets') ?>/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />

  <link href="<?php echo base_url('assets') ?>/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url('assets') ?>/css/style-responsive.css" rel="stylesheet">
<div class="page-heading">
    <h3>
        Add Consultant
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> Add Consultant </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?> 



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                  Add Consultant
                </header>
                <div class="panel-body">
                    <form class="form-horizontal adminex-form" method="post" action="<?=base_url()?>consultant/add" enctype='multipart/form-data'>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Name</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('name'); ?>" type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Heading</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('consultant_heading'); ?>" type="text" name="consultant_heading"  class="form-control" placeholder="">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Subheading</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('consultant_subheading'); ?>" type="text" name="consultant_subheading"  class="form-control" placeholder="">
                            </div>
                        </div>
						
						
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Timimg</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('consultant_timing'); ?>" type="text" name="consultant_timing"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Price</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('consultant_price'); ?>" type="text" name="consultant_price"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Image</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control required_shop_image error file-upload" name="consultant_image" onchange="ValidateSingleInput(this);">
                            </div>
                        </div>


                       <!--  <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Available Date</label>
                            <div class="col-sm-6">
                                <div class='input-group date datetimepicker1' id='datetimepicker1'>
                                    <div id="field">
                                        <input autocomplete="off" class="input" id="field1" name="available_date[]" type="text" placeholder="Type something" data-items="8"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        <button id="b1" class="btn add-more" type="button">+</button></div>

                                           
                                </div>
                            </div>
                        </div> -->
                       


                        <div class="form-group">
                                    <label class="col-sm-2 col-sm-2 control-label">Advance Datetimepicker</label>
                                    <div class="col-md-4">
                                        <div class="input-group date form_datetime-adv datetimepicker1" id="datetimepicker1">
                                            <input type="text" class="form-control form-control-inline input-medium default-date-picker" name="available_date" readonly="" size="16">
                                            <!-- <input type="text" class="form-control" name="available_date[]" readonly="" size="16"> -->
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-primary date-reset"><i class="fa fa-times"></i></button>
                                                <button type="button" class="btn btn-success date-set"><i class="fa fa-calendar"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <!-- <div class="container">
                            <div class="row">
                                <div class='col-sm-6'>
                                    <div class="form-group">
                                        <div class='input-group date datetimepicker1' id='datetimepicker1'>
                                            <input type='text' class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>


                        <div class="form-group">
                                <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        </div> -->



                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Description</label>
                            <div class="col-sm-10">
                                <textarea name="consultant_description" value="<?php echo set_value('consultant_description'); ?>" style="width:350px; height:200px;"></textarea>
                                <!-- <input value="<?php echo set_value('consultant_description'); ?>" type="text" name="consultant_description"  class="form-control" placeholder=""> -->
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Add Consultant</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <!-- <link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" /> -->

<script src="<?php echo base_url('assets') ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/modernizr.min.js"></script>
<script src="<?php echo base_url('assets') ?>/js/jquery.nicescroll.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets') ?>/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--pickers initialization-->
<script src="<?php echo base_url('assets') ?>/js/pickers-init.js"></script>


<!--common scripts for all pages-->
<script src="<?php echo base_url('assets') ?>/js/scripts.js"></script>


  <style type="text/css">
      * {
  .border-radius(0) !important;
}

#field {
    margin-bottom:20px;
}
  </style>


  <!-- Create Date Time picker for add in form  -->
<script type="text/javascript">
            $(document).ready(function () {
                //alert('yes');
                $('.datetimepicker1').datetimepicker({
                    //alert('die');
                    multidate: true,
                    
                });
            });
        </script>
<script>
var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];    
function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}
</script>


<script type="text/javascript">
    
$(document).ready(function(){
    var next = 1;
    $(".add-more").click(function(e){
        e.preventDefault();
        var addto = "#field" + next;
        var addRemove = "#field" + (next);
        next = next + 1;
        var newIn = '<input autocomplete="off" class="input form-control test" id="field' + next + '" name="available_date[]' + next + '" type="text"><span class="input-group-addon"><span class="input-group-addon"><span></span class="glyphicon glyphicon-calendar"></span></span>';
        var newInput = $(newIn);
        var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="field">';
        var removeButton = $(removeBtn);
        $(addto).after(newInput);
        $(addRemove).after(removeButton);
        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
        $("#count").val(next);  
        
            $('.remove-me').click(function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).remove();
                $(fieldID).remove();
            });
    });
 

    
});

</script>


<!-- <div class="container">
    <div class="row">
        <div class='col-sm-6'>
            <div class="form-group">
                <div class='input-group date datetimepicker1' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker();
            });
        </script>
    </div>
</div>