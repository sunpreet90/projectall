<!-- page heading start-->

        <div class="page-heading">
            <h3>Consultant Add</h3>
            <ul class="breadcrumb">
                <li><a href="#">Consultant</a></li>
                <li class="active">Add Consultant </li>
            </ul>
        </div>
        <!-- page heading end-->

        <section class="panel">
            
            <header class="panel-heading">Add New Consultant</header>

            <div class="panel-body">

                <div class="form">
                    
                    <form class="form-horizontal adminex-form" id="consultant_form" method="post" action="<?= base_url('consultant/add')?>" novalidate="novalidate" enctype='multipart/form-data'>
                       
                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Name</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_name" class="form-control" id="consultant_name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Type</label>
                            <div class="col-lg-10">
                                <?php 
                                $categories = $this->db->query("SELECT * FROM `ws_consultation_types` ")->result_array();
                                ?>

                                <select name="consultation_type_name" class="form-control m-bot15">
                                         
                                    <?php foreach ($categories as $rows): ?>
                                      <option value="<?= $rows['consultation_type']; ?>"><?= $rows['consultation_type']; ?></option>
                                    <?php endforeach; ?>
                                        
                                 </select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Via</label>
                            <div class="col-lg-10">

                                <select name="consultation_via" class="form-control m-bot15">

                                    <option value="Skype">Skype</option>
                                    <option value="Email">Email</option>
                                    <option value="Phone">Phone</option>
                                        
                                 </select>
                                <!-- <input type="text" name="consultation_via" class="form-control" id="consultation_via"> -->
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Duration</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_duration" class="form-control" id="consultant_duration" placeholder="e.g 1 Hour">
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Heading</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_heading" class="form-control" id="consultant_heading">
                            </div>
                        </div> -->


                        <!-- <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Subheading</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_subheading" class="form-control" id="consultant_subheading">
                            </div>
                        </div> -->

                       <!-- <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Timimg</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_timing" class="form-control" id="consultant_timing">
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Consultant Price</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_price" class="form-control" id="consultant_price">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="control-label col-lg-2">Status</label>
                            <div class="col-lg-10">
                               <select name="status" class="form-control m-bot15">
                                        <option value="published">Publish</option>
                                        <option value="pending">Pending</option>
                                        <option value="trash">Trash</option>
                                    </select>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Consultant Image:</label>
                            <div class="col-sm-10">
                                <input type="file" name="consultant_image" class="form-control required_shop_image error file-upload" id="featured_image"  onchange="ValidateSingleInput(this);">  
                                 
                            </div>
                        </div>

                        
                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Consultant Description</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='consultant_description' class="wysihtml5 form-control" rows="9"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Consultant long description</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='consultant_long_description' class="wysihtml5 form-control" rows="9"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="form-group ">
                            <label for="status" class="control-label col-lg-2">Consultant Terms</label>
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <textarea name='consultant_terms' class="wysihtml5 form-control" rows="9"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="control-label col-lg-2">Edit Profile URL</label>
                            <div class="col-lg-10">
                                <input type="text" name="consultant_url" class="form-control" id="" placeholder="for e.g. https://calendly.com/sachin-gupta-1">
                            </div>
                        </div>


                        <input type="hidden" name="action" value="1">

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Save</button>
                                <a href="<?php echo base_url('consultant') ?>"><button class="btn btn-default" type="button">Cancel</button></a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>

        <link rel="stylesheet" type="text/css" href="http://immcan.mobilytedev.com/assets/immcan/css/jquery.datetimepicker.css"/>        
        <script src="http://immcan.mobilytedev.com/assets/immcan/js/jquery.js"></script>
        <script src="http://immcan.mobilytedev.com/assets/immcan/js/jquery.datetimepicker.full.js"></script>
        
        <script type="text/javascript">
            
            $(document).ready(function() {
                $('form[id="consultant_form"]').validate({
                    rules: {
                      consultant_name: 'required',
                      consultation_via: {
                        required: true
                      }
                    },
                    submitHandler: function(form) {
                      form.submit();
                    }
                });
            });

        </script>

        <script>
        var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];    
        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                 if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                     
                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
        </script>