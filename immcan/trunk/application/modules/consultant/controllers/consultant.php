<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//require_once('phpass-0.1/PasswordHash.php');
//require_once('phpass-0.1/PasswordHash.php');
/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Consultant extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tank_auth');
		$this->load->helper('form');
		$this->load->helper('url');

		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}

		$this->load->library('form_validation');
		$this->load->model('consultant_model');

		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'consultants';
		$this->table1 = $this->prefix.'consultant_dates';
	}

	function index(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$data['consultants'] = $this->consultant_model->get_consultants_all();
		//echo "<pre>"; print_r($data['consultants']); die;

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('index',isset($data) ? $data : NULL);
	}


	function add()
	{
		//echo "hello"; die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('immcan')
		->build('consultants_add',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('consultant_name', 'name', 'required');
			$this->form_validation->set_rules('consultation_type_name', 'Consultant Type', 'required');
			
            $this->form_validation->set_rules('consultation_via', 'Consultant Via', 'required');
			$this->form_validation->set_rules('consultant_timing', 'Consultant Timing', 'required');
			$this->form_validation->set_rules('consultant_price', 'Consultant Price', 'required');
			$this->form_validation->set_rules('post_content', 'Consultant Description', 'required');


			$consultant_name = $this->input->post('consultant_name');
			$consultation_type_name = $this->input->post('consultation_type_name');
			$consultation_via = $this->input->post('consultation_via');

			$consultant_duration = $this->input->post('consultant_duration');				
			//$consultant_timing =$this->input->post('consultant_timing');

			$consultant_price =$this->input->post('consultant_price');
			$consultant_description =$this->input->post('consultant_description');
			
			$consultant_url =$this->input->post('consultant_url');
			$consultant_terms =$this->input->post('consultant_terms');
			$consultant_long_description =$this->input->post('consultant_long_description');
			
			$consultant_images = $this->do_upload();				

            $users_data=array(
               'consultant_name'       	=> $consultant_name,
               'consultation_type_name' 	=> $consultation_type_name,
               'consultation_via' 	=> $consultation_via,
               'consultant_duration' 	=> $consultant_duration,
               'consultation_price' 	=> $consultant_price,
               'consultant_description' 	=> $consultant_description,
               'consultant_long_description' 	=> $consultant_long_description,    
               'consultant_terms' 		=> $consultant_terms,
               'profile_url' 			=> $consultant_url,             
               'created_on'	=> $this->created,
               'consultant_image'	=> 'uploads/consultants/'.$consultant_images,
               
		    );
			
			if ( $this->consultant_model->add(  $this->table, $users_data ) )  // success
			{
				$this->session->set_flashdata('success', 'Consultant have been added successfully');
				$_POST = '';
				redirect('consultant');		
			
			} else {

				$this->session->set_flashdata('error', 'Error in adding database.');
				$_POST = '';
				redirect('consultant/add');
			}
		}
	}

	function do_upload(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			'upload_path' => './uploads/consultants',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultant_image')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }



			/* Edit Consultant Image Start Here */

			function do_upload1(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			'upload_path' => './uploads/consultants',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultant_image')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }

			/* Edit Consultant Image End Here */	    







    function userkey_exists($key,$user_id) {
	    $this->table->mail_exists($key,$user_id);
	}

	function edit()
	{
		$user_id = $this->uri->segment(4);
		
		$user='';
		$user = $this->consultant_model->get_user($user_id);
		$user['slots'] = $this->consultant_model->get_slots_all();
		$current_date = NOW();
		$user['selected_slots'] = $this->consultant_model->get_selected_slots($current_date);
		//echo "<pre>"; print_r($user); die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['user']		= $user;

		$data['include_js'] = 'editor';
		$data['include_css'] = 'editor';

		$this->load->module('layouts');
		$this->load->library('template');
        
        $this->template
		->set_layout('immcan')
		->build('consultants_edit',isset($data) ? $data : NULL);

        if ($this->input->post() ) 
		{
			
            $this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('consultant_name', 'user name', 'required');
			//$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			
			$this->form_validation->set_rules('consultation_type_name', 'Consultant Heading', 'required');
			
            $this->form_validation->set_rules('consultation_via', 'Consultant Subheading', 'required');
			$this->form_validation->set_rules('consultant_duration', 'Consultant Timing', 'required');
			$this->form_validation->set_rules('consultant_price', 'Consultant Price', 'required');

			
			if ( $this->form_validation->run() == FALSE )
			{
				
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';				

          	    $this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('consultants_edit',isset($data) ? $data : NULL);
			}else{
				// echo 'good';die;
				//echo "<pre>"; print_r($_POST());
				$consultant_name =$this->input->post('consultant_name');
				$consultation_type_name = $this->input->post('consultation_type_name');
				$consultation_via =$this->input->post('consultation_via');
				$consultant_duration = $this->input->post('consultant_duration');
				$consultant_price = $this->input->post('consultant_price');
				$consultant_description = $this->input->post('consultant_description');
				$consultant_long_description = $this->input->post('consultant_long_description');
				$consultant_terms = $this->input->post('post_terms');
				$consultant_url = $this->input->post('consultant_url');
            	$consultant_images = $this->do_upload1();
				$datetimepicker = "'".$this->input->post('datetimepicker')."'";				
				$from_hour = $this->input->post('from-hour');
				$from_minute = $this->input->post('from-minute');
				$from_daystatus = $this->input->post('from-daystatus');
				$to_hour = $this->input->post('to-hour');
				$to_minute = $this->input->post('to-minute');
				$to_daystatus = $this->input->post('to-daystatus');

				// Make a string for all the from time from hour
				$from_count = count($this->input->post('from-hour'));
				$from_time = array();
				for($i=0; $i<$from_count; $i++){
					$from_time[] =  $from_hour[$i].':'.$from_minute[$i].' '.$from_daystatus[$i];
				}
				$final_from_time  = implode(",", $from_time);	

 				// Make a string for all the to time
				$to_count = count($this->input->post('to-hour'));
				$to_time = array();
				for($i=0; $i<$to_count; $i++){
					$to_time[] =  $to_hour[$i].':'.$to_minute[$i].' '.$to_daystatus[$i];
				}				
				$final_to_time  = implode(",", $to_time);	           

	            $arr_not_availvale_days = $this->input->post('day');
	            $not_availvale_days  = implode(",", $arr_not_availvale_days);
	            
	            
	            $arr_not_availvale_slots = $this->input->post('slots');	            
	            $not_availvale_slots = (!empty($arr_not_availvale_slots) ? implode(",", $arr_not_availvale_slots) : '');


	          if(empty($_FILES['consultant_image']['name'])) {

                $users_data=array(
                   'consultant_name'       	=> $consultant_name,
                   'consultation_type_name' 	=> $consultation_type_name,
                   'consultation_via' 	=> $consultation_via,
                   //'available_date' 	=> $datetimepicker,
                   'consultant_description' 	=> $consultant_description,
                   'consultant_duration' 	=> $consultant_duration,
                   'consultant_long_description' 	=> $consultant_long_description,
                   'consultant_terms' 	=> $consultant_terms,
                   'profile_url' 	=> $consultant_url,
                   'consultation_price' 	=> $consultant_price,
                   //'consultant_image' 	=> 'uploads/consultants/'.$consultant_images,
                   'created_on'	=> $this->created
			    );

            }else{

            	 $users_data=array(
                   'consultant_name'       	=> $consultant_name,
                   'consultation_type_name' 	=> $consultation_type_name,
                   'consultation_via' 	=> $consultation_via,
                   //'available_date' 	=> $datetimepicker,
                   'consultant_description' 	=> $consultant_description,
                   'consultant_duration' 	=> $consultant_duration,
                   'consultant_long_description' 	=> $consultant_long_description,
                   'consultant_terms' 	=> $consultant_terms,
                   'profile_url' 	=> $consultant_url,
                   'consultation_price' 	=> $consultant_price,
                   'consultant_image' 	=> 'uploads/consultants/'.$consultant_images,
                   'created_on'	=> $this->created
			    );


            }
                  //echo "<pre>"; print_r($users_data); echo "</pre>"; die;
			    $where_data=array( 'id' => $user_id );
               
				if ( $this->consultant_model->updateWhere(  $this->table, $where_data, $users_data ) )  // success
				{

					$datetime = strtotime($this->input->post('slot_date'));
 					$date = date('Y/m/d', $datetime);
 					$where_data 	= array('consult_id' => $user_id,'available_date' => $date);		
					$this->db->delete($this->table1,$where_data);
					$consult_dates=array(
						'consult_id'  => $user_id,						
						'available_date' 	=> $date,
						'slot_num' 	=> $not_availvale_slots,
						'from_date'   => $final_from_time,
	                    'to_date'   => $final_to_time,
	                    'created_on'	=> $this->created 
					);
					
					
					$this->consultant_model->add($this->table1, $consult_dates);
					//echo "<pre>"; print_r($insert_id); die;

					$this->session->set_flashdata('success', 'Consultant Updated successfully');

					$_POST = '';
					redirect('consultant/edit/id/'.$user_id);	

				} else {	// Failed password not matched					
					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';

					redirect('consultant/edit/id/'.$user_id);
				}
			}			
		}
	}

	function delete_consultant()
	{
		$user_id = $this->uri->segment(4);
		
		if($this->consultant_model->delete_consultant($user_id))
		{
            $this->session->set_flashdata('response_status', 'success');
		    //$this->session->set_flashdata('response_status', 'error');
			$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url().'consultant');
	}

	public function view()
	{
		$user_id = $this->uri->segment(4);
		
		$user='';
		$user = $this->consultant_model->get_consultant($user_id);
		
		//echo "<pre>"; print_r($user); die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['consultantviews']		= $user;
		
		//print_r($data['user']); die;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('view_consultant',isset($data) ? $data : NULL);
	}

	public function paypal_add(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('view_consultant',isset($data) ? $data : NULL);
	}

	

	public function booking_consultations(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['booked_consultant'] = $this->consultant_model->get_booked_consultant();
		//echo "<pre>"; print_r($data['booked_consultant']); die;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('booking_consultations',isset($data) ? $data : NULL);	
	}


	public function deletes_booked(){
		$booked_id = $this->uri->segment(4);
		if($this->consultant_model->deletes_booked($booked_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url().'consultant/booking_consultations');
	}



	/* public function viewproduct($id) {
        //echo $id;
        if ($id) {
            $this->db->where('product_id', $id);
            $this->db->set('view_count', '`view_count`+ 1', FALSE);
            $this->db->update('ws_products');
            $data['arts']           = $this->ProductModel->GetProduct($id);
            $data['ws_shop_images'] = $this->InitModel->showshopimages();
            
            //echo "<pre>", print_r($data) ;die();
        }

        $this->template->title('View Product', 'The product detail page.');

        $data['include_js'] = 'view-product';
        $data['include_css'] = 'view-product';
        $data['active_menu'] = 'my-products';

        $this->template
        ->set_layout('default') // application/views/layouts/two_col.php
        ->build('view-product', $data); // views/welcome_message
    } */
	
	
	

}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */