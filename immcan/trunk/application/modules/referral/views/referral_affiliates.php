<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_page.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/data-tables/DT_bootstrap.css" rel="stylesheet">
<!-- page heading start-->
<div class="page-heading">
    <h3>
        Referral Affiliates Listing
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href=" <?= site_url()?>">Dashboard</a>

        </li>
        <li class="active"> Referral Affiliates Listing </li>
       
    </ul>
    <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>
</div>
<!-- page heading end-->
<?php  //pr($users); die; ?>

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                        <header class="panel-heading">
                            Referral Affiliates Listing &nbsp; 

                                <!-- <a href="<?=base_url('referral/add')?>"><button class="btn btn-primary" type="button">Add Referral</button></a> -->
                           
                            <!-- <a href="<?php //echo base_url('referral/referral_add'); ?>" class="btn btn-info" style="float:right;">Client Referral</a> -->
                            <!--
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                             </span>
                         -->
                         <!-- <a href="<?php echo base_url('consultant/add') ?>" class="btn btn-info" style="float:right">Add Consultant</a> -->
                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Sr.No.</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Referral/Professional</th>
                                        <th>Comments</th>
                        
                                       
                                        <th class="center">Action</th> 
                                    </tr>
                                    </thead>




                                    <tbody>
                                        <?php 
                                        //echo "<pre>"; print_r($referrals); die;
                                       
                                        if ( is_array( $referrals )) 
                                        {
                                            $srno = 1;
                                            foreach ($referrals as $referral) 
                                            {
                                              
                                              //if(!empty($user['name']) && !empty($user['email'])){
                                            ?>
                                        <tr class="gradeX">
                                            <td><?php echo $srno; ?></td>
                                            <td><?=$referral['name']?></td>
                                            
                                            <td><?=$referral['email']?></td>
                                            <td><?=$referral['phone']?></td>
                                            <td><?=$referral['education_profession']?></td>
                                            <td><?=$referral['comments']?></td>
                                            <td class="center hidden-phone">
                                                        <!-- a title="Edit" href="<?=base_url('consultant')?>/view/id/<?=$consultant['id'];?>">
                                                             <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                        <a title="Edit" href="<?=base_url('consultant')?>/edit/id/<?=$consultant['id'];?>">
                                                             <i class="fa fa-pencil" aria-hidden="true"></i>
                                                        </a>-->
                                                        <a title="Delete" href="<?=base_url('referral')?>/delete1/id/<?=$referral['id'];?>" onclick="if(!confirm('Are you sure to delete this record?')){return false;}">
                                                           <i class="fa fa-eraser"></i>
                                                        </a>
                                                </td> 
                                        </tr>
                                        <?php $srno++;  }
                                          // }
                                        }
                                        ?>
                                       
                                    </tbody>
                                 
                                </table>
                            </div>
                        </div>
                    </section>
        </div>
    </div>
</div>


<!--Data tables script-->




<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=base_url('assets')?>/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="<?=base_url('assets')?>/text/javascript" src="<?=base_url('assets')?>/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="<?=base_url('assets') ?>/js/dynamic_table_init.js"></script>

<!--common scripts for all pages-->
<script src="<?=base_url('assets') ?>/js/scripts.js"></script>

 <script type="text/javascript">
         <!--
            function Warn() {
               
              var retVal = confirm("Warning!You want to delete this user.");
               if( retVal == true ){
                  return true;
               }
               else{
                  return false;
               }
               
            }
         //-->
      </script>

 <script type="text/javascript">
    $(document).ready(function(){    
        $("#dynamic-table_length").addClass('col-md-3');    
    });
 </script>

<!--body wrapper end-->