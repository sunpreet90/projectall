<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Referral_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'consultants';
	}
	public function mail_exists($email,$user_id)
	{
	    $this->db->where(array('email'=>$email,'id !='=>$user_id));
	    $query = $this->db->get($this->table);
	    print_r($query);die;
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }
	}


	function get_user($user_id)
	{
		$where_data = array('id' => $user_id );
		return $this->findWhere($this->table, $where_data, $multi_record = FALSE);
	}


	
	function get_users()
	{
		$where_data = array('user_type !=' => 0); 
		//echo "<pre>";print_r($this->findWhere( $this->table, $where_data, $multi_record = TRUE ));die;
		return $this->findWhere( $this->table, $where_data, $multi_record = TRUE );
	}
	
	function get_consultants_all()
	{
		$this->db->select('*');
		$this->db->from('ws_consultants');
		//$this->db->where('id',$id);
		return $this->db->get()->result_array();
	}
	
	
	
	function delete_consultant($user_id)
	{
     //echo $user_id;die;
		/*$where_data 	= array('id' => $user_id );
		//$post 	= array('status' => 0 );
		return $this->updateWhere($this->table, $where_data, $post);*/

        $where_data 	= array('id' => $user_id );
		//$this->db->where('activated', 0);
		return $this->db->delete($this->table,$where_data);
	}


}

/* End of file model.php */