<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Referral_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'client_referral';
	}
	public function mail_exists($email,$user_id)
	{
	    $this->db->where(array('email'=>$email,'id !='=>$user_id));
	    $query = $this->db->get($this->table);
	    print_r($query);die;
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }
	}

	
	
	function get_referrals_all( $referral_type )
	{
		$this->db->select('*');
		$this->db->from('ws_client_referral');
		$this->db->where('referral_type', $referral_type);
		return $this->db->get()->result_array();
	}

	function get_client_referrels()
	{
		$this->db->select('*');
		$this->db->from('ws_client_referral');
		//$this->db->where('id',$id);
		return $this->db->get()->result();
	}
	
	
	
	function delete_referral($user_id)
	{
     //echo $user_id;die;
		/*$where_data 	= array('id' => $user_id );
		//$post 	= array('status' => 0 );
		return $this->updateWhere($this->table, $where_data, $post);*/

        $where_data 	= array('id' => $user_id );
		//$this->db->where('activated', 0);
		return $this->db->delete($this->table,$where_data);
	}
	function delete_agent($user_id)
	{
     //echo $user_id;die;
		/*$where_data 	= array('id' => $user_id );
		//$post 	= array('status' => 0 );
		return $this->updateWhere($this->table, $where_data, $post);*/

        $where_data 	= array('id' => $user_id );
		//$this->db->where('activated', 0);
		return $this->db->delete($this->table,$where_data);
	}
	function delete_affiliates($user_id)
	{
     //echo $user_id;die;
		/*$where_data 	= array('id' => $user_id );
		//$post 	= array('status' => 0 );
		return $this->updateWhere($this->table, $where_data, $post);*/

        $where_data 	= array('id' => $user_id );
		//$this->db->where('activated', 0);
		return $this->db->delete($this->table,$where_data);
	}


}

/* End of file model.php */