<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class index extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tank_auth');
		// $this->load->library('utils');
		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}
		$this->load->library('form_validation');
		$this->load->model('user_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'users';
	}

	function index(){
		echo 'we are here';die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['users'] 	= $this->user_model->get_users();
		//pr($data['users'], true);
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('users',isset($data) ? $data : NULL);
	}
	function add()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('users_add',isset($data) ? $data : NULL);


		if ($this->input->post() ) 
		{	

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('user_title', 'user title', 'required');
			$this->form_validation->set_rules('user_description', 'user description', 'required');		
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('users_add',isset($data) ? $data : NULL);
			
			}else{

				$user_title =$this->input->post('user_title');
				$user_description = $this->input->post('user_description');

				$users_data=array(
                   'user_title'       	=> $user_title,
                   'user_description' 	=> $user_description,
                   'created'	=> $this->created
			    );
				if ( $this->user_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'user added successfully');
					$_POST = '';
					redirect('users/add');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('users/add');
				}
			}			
		}
	}

	function edit()
	{
		$user_id = $this->uri->segment(4);
		$user='';
		$user = $this->user_model->get_user( $user_id );

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['user']		= $user;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('users_edit',isset($data) ? $data : NULL);


		//echo $user_id;	die();
		if ($this->input->post() ) 
		{	
			//echo $user_id;	die();

			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('user_title', 'user title', 'required');
			$this->form_validation->set_rules('user_description', 'user description', 'required');		
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
				redirect('users/edit/id/'.$user_id);		
			
			}else{

				$user_title =$this->input->post('user_title');
				$user_description = $this->input->post('user_description');



				$users_data=array(
                   'user_title'       	=> $user_title,
                   'user_description' 	=> $user_description,
                   'created'	=> $this->created
			    );

			    $where_data=array( 'id' => $user_id );

				if ( $this->user_model->updateWhere(  $this->table, $where_data, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'user Updated successfully');
					$_POST = '';
					redirect('users/edit/id/'.$user_id);		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('users/edit/id/'.$user_id);
				}
			}			
		}
	}
	function delete()
	{
		$user_id = $this->uri->segment(4);
		$this->user_model->delete_user($user_id);
		header('Location: '.base_url().'users');
	}



}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */