<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//require_once('phpass-0.1/PasswordHash.php');
//require_once('phpass-0.1/PasswordHash.php');
/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Referral extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tank_auth');
		$this->load->helper('form');
		$this->load->helper('url');
		//$this->ci->load->config('tank_auth', TRUE);
		// $this->load->library('utils');
		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}
		$this->load->library('form_validation');
		$this->load->model('referral_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'client_referral';
	}

	function index(){

		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		
		$data['referrals'] = $this->referral_model->get_referrals_all( $referral_type = 'referral' );
		//echo "<pre>"; print_r($data['referrals']); die;

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('referrals',isset($data) ? $data : NULL);
	}


	/* Use This function to display form on button Click*/
	function referral_add(){
		$data['user_id'] = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('referrals_add',isset($data) ? $data : NULL);
	}
    function referral_affiliates()
    {
		$data['user_id'] = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$data['referrals'] = $this->referral_model->get_referrals_all( $referral_type = 'affiliate' );
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('referral_affiliates',isset($data) ? $data : NULL);
	}
	function agent_inquiry()
    {
		$data['user_id'] = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$data['referrals'] = $this->referral_model->get_referrals_all( $referral_type = 'agent_inquiry' );
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('agent_inquiry',isset($data) ? $data : NULL);
	}



	function add()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('referrals_add',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('referral_name', 'Referral Name', 'required');
			
            $this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('referral_city', 'Referral City', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('referral_country', 'Referral Country', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('referral_email', 'Referral Email', 'required');
			$this->form_validation->set_rules('immigration_type', 'Immigration Type', 'required');
			$this->form_validation->set_rules('referral_phone', 'Referral Phone', 'required');
			$this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');



		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('referrals_add',isset($data) ? $data : NULL);
			
			}else{

				$name = $this->input->post('name');
				$referral_name = $this->input->post('referral_name');
				$address =$this->input->post('address');
				$referral_city =$this->input->post('referral_city');
				$email =$this->input->post('email');
				$referral_country =$this->input->post('referral_country');
				$phone =$this->input->post('phone');
				$referral_email =$this->input->post('referral_email');
				$immigration_type =$this->input->post('immigration_type');
				$referral_phone =$this->input->post('referral_phone');
				$captcha =$this->input->post('g-recaptcha-response');

				if(!$captcha){
		          echo '<h2>Please check the the captcha form.</h2>';
		          exit;
		        }
		        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Le5mVkUAAAAAATqUdSq_fOCsw2kn6YRbNdi1tWX&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
		        if($response['success'] == false)
		        {
		          echo '<h2>You are spammer ! Get the @$%K out</h2>';
		        }
		        else
		        {
		          echo '<h2>Thanks for posting comment.</h2>';
		        }
				//$consultant_images = $this->do_upload();				
               
                $users_data=array(
                   'name'       	=> $name,
                   'user_id' => $data['user_id'],
                   'referral_name' 	=> $referral_name,
				   //'age' => $age,
				   'address' => $address,
                   'referral_city' 	=> $referral_city,
                   'email' 	=> $email,
                   'phone' 	=> $phone,                   
                   'referral_country' 	=> $referral_country,                   
                   'referral_email' 	=> $referral_email,                   
                   'immigration_type' 	=> $immigration_type,                   
                   'referral_phone' 	=> $referral_phone,                   
                   'referral_country' 	=> $referral_country,                   
                   'created_at'	=> $this->created,
                   //'consultant_image'	=> $consultant_images,
                   
			    );
			    //echo "<pre>"; print_r($users_data); die;
				if ( $this->referral_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Consultant have been added successfully');
					$_POST = '';
					redirect('referral');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('referral/add');
				}
			}			
		}
	}




	function do_upload(){
			//echo base_url().'uploads'; die;
			//echo realpath(base_url() . 'uploads'); die;
			$config = array(
			'upload_path' => './uploads',
			'allowed_types' => 'gif|jpg|png|jpeg',
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			//'max_height' => "768",
			//'max_width' => "1024"
			);
			//echo "<pre>"; print_r($config); die;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if($this->upload->do_upload('consultant_image')) {

				$uploadData = $this->upload->data();
				return $picture = $uploadData['file_name'];
			
			} else {

				$error = array('error' => $this->upload->display_errors());
				
				return false;
				//echo "not uploaded";
				//print_r($error);
				//return false;
				//die('event_image');
			}
	    }




    function userkey_exists($key,$user_id) {
	    $this->table->mail_exists($key,$user_id);
	}
	function edit()
	{
		$user_id = $this->uri->segment(4);
		
		$user='';
		$user = $this->referral_model->get_user($user_id);
		
		//echo "<pre>"; print_r($user); die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['user']		= $user;

		$this->load->module('layouts');
		$this->load->library('template');
        
        $this->template
		->set_layout('users')
		->build('consultants_edit',isset($data) ? $data : NULL);

        if ($this->input->post() ) 
		{	
            
			
            $this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'user name', 'required');
			//$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			
			$this->form_validation->set_rules('consultant_heading', 'Consultant Heading', 'required');
			
            $this->form_validation->set_rules('consultant_subheading', 'Consultant Subheading', 'required');
			$this->form_validation->set_rules('consultant_timing', 'Consultant Timing', 'required');
			$this->form_validation->set_rules('consultant_price', 'Consultant Price', 'required');
			$this->form_validation->set_rules('consultant_description', 'Consultant Description', 'required');

			
			
			if ( $this->form_validation->run() == FALSE )
			{
				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
          	    $this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('consultants_edit',isset($data) ? $data : NULL);
			}else{
				// echo 'good';die;
				$name =$this->input->post('name');
				$consultant_heading = $this->input->post('consultant_heading');
				$consultant_subheading =$this->input->post('consultant_subheading');
				$consultant_timing = $this->input->post('consultant_timing');
				$consultant_price = $this->input->post('consultant_price');
				$consultant_description = $this->input->post('consultant_description');
               
                $users_data=array(
                   'name'       	=> $name,
                   'consultant_heading' 	=> $consultant_heading,
                   'consultant_subheading' 	=> $consultant_subheading,
                   'consultant_timing' 	=> $consultant_timing,
                   'consultant_price' 	=> $consultant_price,
                   'consultant_description' 	=> $consultant_description,
                   'created'	=> $this->created
			    );
                  
			    $where_data=array( 'id' => $user_id );

				if ( $this->referral_model->updateWhere(  $this->table, $where_data, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Consultant Updated successfully');
					$_POST = '';
					redirect('consultant/edit/id/'.$user_id);		
				}else{	// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('consultant/edit/id/'.$user_id);
				}
			}			
		}
	}
	function delete()
	{
		$user_id = $this->uri->segment(4);
		if($this->referral_model->delete_referral($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url().'referral');
	}

	function delete1()
	{
	 $user_id = $this->uri->segment(4);

		if($this->referral_model->delete_affiliates($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url('referral').'/referral_affiliates');
	}
	function delete2()
	{
	 $user_id = $this->uri->segment(4);

		if($this->referral_model->delete_agent($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url('referral').'/agent_inquiry');
	}
	
	

}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */