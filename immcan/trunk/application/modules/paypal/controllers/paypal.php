<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */

class Paypal extends MX_Controller 
{

	function __construct() {

		parent::__construct();
		$this->created = date('Y-m-d H:i:s', time());
	}

	function index(){
		
		$this->load->view('paypal');

	}
	

	function paypal_success(){
		$this->load->view('paypal_success');

	}

	function paypal_error(){
     $this->load->view('paypal_error');

	}
}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */