<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//echo "hello"; die;

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Welcome extends MX_Controller {

	function __construct() {

		/**
		 * @author:	Harish Chauhan
		 *
		 * @desc: 	Parent constructors are not called implicitly if the child class defines a constructor.
		 * In order to run a parent constructor, a call to parent::__construct() within the child constructor is required.
		 * If the child does not define a constructor then it may be inherited from the parent class just like a normal class method 
		 * (if it was not declared as private).
		 */
		parent::__construct();
		//$this->load->module('user');
		$this->load->model('users/user_model');
		$this->load->model('consultant/consultant_model');
		$this->load->model('referral/referral_model');
		$this->load->model('partner/partner_model');
		$this->load->model('admin/admin_model');
	}

	/**
	 * Loads th index page after login || The default login page
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function index() {
		            
		if (!$this->tank_auth->is_logged_in()) {
			        
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');
			//$users 	= $this->user_model->get_users_all(); // Total Users
			$users 	= $this->user_model->get_activated_users(); // Total Users
			//echo "<pre>"; print_r($users); die;
           $data['users_count'] = count($users); // count total users
		   $consultant_listing 	= $this->consultant_model->get_consultant_listing(); // Consultant Listing
           $data['consultant_listing_count'] = count($consultant_listing); // count Consultant Listing

           $referral_listing 	= $this->referral_model->get_client_referrels(); // Client Referrels
           $data['referral_listing_count'] = count($referral_listing); // count client Referrels

           $international_partner 	= $this->partner_model->get_international_partner(); // Client Referrels
           $data['international_partner_count'] = count($international_partner); // count client Referrels


           $latest_news 	= $this->admin_model->get_news_listing(); // Latest news
           $data['latest_news_count'] = count($latest_news); // count latest news

            $our_services 	= $this->admin_model->get_services_listing(); // Our Services
           $data['our_services_count'] = count( $our_services); // count our services

           $fun_facts 	= $this->admin_model->get_fun_facts_listing(); // Fun Facts
           $data['fun_facts_count'] = count( $fun_facts); // count fun facts

           $qualify 	= $this->admin_model->get_qualify_listing(); // Qualify
           $data['qualify_count'] = count( $qualify); // count qualify
           


		   
		   //$total_events 	= $this->event_model->get_total_events(); // Total Events
           //$data['events_count'] = count($total_events); // count total events
		   
			$this->template
			->set_layout('dashboard')
			->build('welcome',isset($data) ? $data : NULL);

			       
		
		}
	}

	/**
	 * Prints hello message on page load
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function hello() {
                     
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} else {
			$data['user_id']	= $this->tank_auth->get_user_id();
			$data['username']	= $this->tank_auth->get_username();

			$this->load->module('layouts');
			$this->load->library('template');
			$this->template
			->set_layout('asd')
			->build('welcome_message',isset($data) ? $data : NULL);
		
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */