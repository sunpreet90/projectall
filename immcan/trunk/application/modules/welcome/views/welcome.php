
        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Dashboard
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?=base_url('welcome')?>">Dashboard</a>
                </li>
                <li class="active"> My Dashboard </li>
            </ul>
           <!--  <div class="state-info">
                <section class="panel">
                    <div class="panel-body">
                        <div class="summary">
                            <span>yearly expense</span>
                            <h3 class="red-txt">$ 45,600</h3>
                        </div>
                        <div id="income" class="chart-bar"></div>
                    </div>
                </section>
                <section class="panel">
                    <div class="panel-body">
                        <div class="summary">
                            <span>yearly  income</span>
                            <h3 class="green-txt">$ 45,600</h3>
                        </div>
                        <div id="expense" class="chart-bar"></div>
                    </div>
                </section>
            </div> -->
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--statistics start-->
                    <div class="row state-overview">
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $users_count;?></div>
                                    <div class="title"><a href="<?=base_url('users')?>">Users</a></div>
                                </div>
                            </div>
                        </div>
						
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-gavel"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $consultant_listing_count;?></div>
                                    <div class="title"><a href="<?=base_url('consultant')?>">Consultants</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel red">
                                <div class="symbol">
                                    <i class="fa fa-cogs"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $our_services_count;?></div>
                                    <div class="title"> <a href="<?=base_url('admin/services')?>">All Services</a></div>

                                </div>
                            </div>
                        </div> 
						<div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel red">
                                <div class="symbol">
                                    <i class="fa fa-book"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $latest_news_count;?></div>
                                    <div class="title"><a href="<?=base_url('admin/news')?>">Latest News</a></div>
                                </div>
                            </div>
                        </div>
                       <!--  <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $referral_listing_count;?></div>
                                    <div class="title"><a href="#">Media Gallery</a></div>
                                </div>
                            </div>
                        </div>  -->
<!-- 
                        <div class="col-md-4 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $international_partner_count;?></div>
                                    <div class="title"><a href="#">Partners</a></div>
                                </div>
                            </div>
                        </div> --> 


                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $fun_facts_count;?></div>
                                    <div class="title"><a href="<a href="<?=base_url('admin/fun_facts')?>">Fun Facts</a></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12 col-sm-6">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-tasks"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php echo $qualify_count;?></div>
                                    <div class="title"><a href="<a href="<?=base_url('admin/qualify')?>">Manage Qualify</a></div>
                                </div>
                            </div>
                        </div>


                    </div>
                     
                    <!--statistics end-->
                </div>

            </div6
        </div>
        <!--body wrapper end-->


    <!--easy pie chart-->
    <script src="<?=base_url('assets') ?>/js/easypiechart/jquery.easypiechart.js"></script>
    <script src="<?=base_url('assets') ?>/js/easypiechart/easypiechart-init.js"></script>

    <!--Sparkline Chart-->
    <script src="<?=base_url('assets') ?>/js/sparkline/jquery.sparkline.js"></script>
    <script src="<?=base_url('assets') ?>/js/sparkline/sparkline-init.js"></script>

    <!--icheck -->
    <script src="<?=base_url('assets') ?>/js/iCheck/jquery.icheck.js"></script>
    <script src="<?=base_url('assets') ?>/js/icheck-init.js"></script>

    <!-- jQuery Flot Chart-->
    <script src="<?=base_url('assets') ?>/js/flot-chart/jquery.flot.js"></script>
    <script src="<?=base_url('assets') ?>/js/flot-chart/jquery.flot.tooltip.js"></script>
    <script src="<?=base_url('assets') ?>/js/flot-chart/jquery.flot.resize.js"></script>


    <!--Morris Chart-->
    <script src="<?=base_url('assets') ?>/js/morris-chart/morris.js"></script>
    <script src="<?=base_url('assets') ?>/js/morris-chart/raphael-min.js"></script>

    <!--Calendar-->
    <script src="<?=base_url('assets') ?>/js/calendar/clndr.js"></script>
    <script src="<?=base_url('assets') ?>/js/calendar/evnt.calendar.init.js"></script>
    <script src="<?=base_url('assets') ?>/js/calendar/moment-2.2.1.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>

    <!--common scripts for all pages-->
    <script src="<?=base_url('assets') ?>/js/scripts.js"></script>

    <!--Dashboard Charts-->
    <script src="<?=base_url('assets') ?>/js/dashboard-chart-init.js"></script>

    <!--gritter script-->
    <script type="text/javascript" src="<?=base_url('assets') ?>/js/gritter/js/jquery.gritter.js"></script>
    <script src="<?=base_url('assets') ?>/js/gritter/js/gritter-init.js" type="text/javascript"></script>

