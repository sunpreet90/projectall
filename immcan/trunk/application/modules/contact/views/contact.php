<style type="text/css">
.contact { margin-top: 100px; }
.lead a {
    padding: 12px 22px;
    margin-top: 10px;
}
.main_menu_area {
    position: absolute;
    width: 100%;
    top: 0px;
    left: 0px;
    z-index: 30;
    padding: 0px 75px;
    border-bottom:0px !important;
    background-color: #bcafc9;
}
.red{ color:red;}
.form-area {
    background-color: #FAFAFA;
    padding: 10px 40px 60px;
    margin: 10px 0px 60px;
}
label.error {
    color: #ff5722 !important;
}
.error::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: red !important;
    opacity: 1; /* Firefox */
}

.error::-ms-input-placeholder { /* Internet Explorer 10-11 */
    color: red !important;
}

.error::-ms-input-placeholder { /* Microsoft Edge */
    color: red !important;
}

</style>
    <!--================Banner Area =================-->
    <section class="banner_area">
        <div class="container">
            <div class="banner_text_inner">
                <h4><?=get_page_metadata( $page_id = $page_data[0]->id, $key = 'banner_title' )?></h4>
                <h5><?=get_page_metadata( $page_id = $page_data[0]->id, $key = 'banner_subheading' )?></h5>
            </div>
        </div>
    </section>
    <!--================End Banner Area =================-->

    <!--================Contact Us Area =================-->
    <section class="contact_us_area1">
        <div class="container">
            <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>
            <div class="contact_details_inner">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="contact_text">
                            <div class="main_title">
                                
                                <h2><?=$page_data[0]->post_title?></h2>

                                <?=$page_data[0]->post_content?>

                            </div>

                            <div class="static_social">
                                <div class="main_title">
                                    <h2>Follow Us:</h2>
                                </div>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact_form">
                            <div class="main_title">
                                <h2>Get In Touch With Us!</h2>
                                <p>We will be always happy to help you the best we can!</p>
                            </div>
                           

                            <div class="form-area">  
                                <!-- <form role="form" class="contact_us_form"  id="contactForm"  method="POST" action="<?php echo base_url('contact/add_contact'); ?>"> -->
                                    <form role="form" class="contact_us_form"  id="contactForm"  method="POST" action="#" novalidate="novalidate">
                                    <div class="form-group">
                                    <label for="email">First Name:</label>
                                    <input type="text" class="form-control" id="name" name="firstname" placeholder="Name*">
                                  </div>

                                  <div class="form-group">
                                    <label for="email">Last Name:</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name*">
                                  </div>

                                  <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email*">
                                  </div>
                                  <div class="form-group">
                                    <label for="email">Country:</label>
                                    <input type="text" class="form-control" id="residence" name="residence" placeholder="Country of Citizenship/Legal Status*">
                                  </div>
                                 
                                  <div class="form-group">
                                    <label for="email">Message:</label>
                                     <textarea class="form-control" type="textarea" id="message" name="message" placeholder="Message*" maxlength="140" rows="7"></textarea>     
                                            </div>
                                    
                                <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Get in touch!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Contact Us Area =================-->

    <!--================Footer Area =================-->

    <script type="text/javascript">
        
        jQuery(document).ready(function(){ 
            //alert('inside function');
        jQuery('#characterLeft').text('140 characters left');
        jQuery('#message').keydown(function () {
            var max = 140;
            var len = $(this).val().length;
            if (len >= max) {
                jQuery('#characterLeft').text('You have reached the limit');
                jQuery('#characterLeft').addClass('red');
                jQuery('#btnSubmit').addClass('disabled');            
            } 
            else {
                var ch = max - len;
                jQuery('#characterLeft').text(ch + ' characters left');
                jQuery('#btnSubmit').removeClass('disabled');
                jQuery('#characterLeft').removeClass('red');            
            }
        });    
    });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
        $('form[id="contactForm"]').validate({
            rules: {
              firstname: 'required',
              lastname: 'required',
              email: {
                required: true,
                email: true,
              },
              residence: {
                    required: true,
                    //minlength: 5
                },
              message: {
                required: true,
                //minlength: 8,
              }
            },
            messages: {
              firstname: '',
              lastname: '',
              email: '',
              message: {
                required: ''
              }, 
              residence: {
                required : ''
                //minlength: 'Residence must be at least 5 characters long'
              }
            },
            submitHandler: function(form) {
              form.submit();
            }
          });

        });

    </script>

    <script type="text/javascript">
        
        //$(document).ready(function(){
    
    /*(function($) {
        "use strict";

    
    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");*/

    // validate contactForm form
    /*$(function() {
        $('#contactForm').validate({
            rules: {
                firstname: {
                    required: true,
                    minlength: 2
                },
                lastname: {
                    required: true,
                    minlength: 4
                },
                residence: {
                    required: true,
                    minlength: 5
                },
                email: {
                    required: true,
                    email: true
                },
                message: {
                    required: true,
                    minlength: 20
                }
            }*//*,
            messages: {
                firstname: {
                    required: "Please add name here",
                    minlength: "Your name must consist of at least 2 characters"
                },
                lastname: {
                    required: "Please add last name here",
                    minlength: "Your subject must consist of at least 4 characters"
                },
                residence: {
                    required: "Please add Country of Citizenship/Legal Status",
                    minlength: "Your Number must consist of at least 5 characters"
                },
                email: {
                    required: "Please add your email address"
                },
                message: {
                    required: "Please add your message Here.."
                    //minlength: "thats all? really?"
                }
            }*/
        //}).form();
    //})
        
 //})(jQuery)
//})

    </script>