<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//echo "hello"; die;

/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */

class Contact extends MX_Controller {

	function __construct() {

		parent::__construct();

		$this->load->library('tank_auth');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model('contact_model');
		//$this->load->model('consultant_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'contactus';
	}

	/**
	 * Loads th index page after login || The default login page
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	public function index() {
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$id = $this->uri->segment(2);

		$this->db->from('ws_posts')->where(array('id' => $id ));
		$query = $this->db->get();
		
		$data['page_data'] =  $query->result();
		$data['id'] =  $id;
		//pr($data['page_data'][0]->id); die;

		if ($this->input->post()) 
		{	
			//die("here");
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('firstname', 'user name', 'required');
			$this->form_validation->set_rules('lastname', 'last name');
			$this->form_validation->set_rules('email', 'user email', 'required');	
			
            $this->form_validation->set_rules('residence', 'Current Residence');
            $this->form_validation->set_rules('message', 'Message', 'required');

			

		   //echo validation_errors(); die;
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('websites')
				->build('contact/'.$data['page_data'][0]->id,isset($data) ? $data : NULL);
			
			}else{
				//$three_digit_random_number = mt_rand(100, 999);
				$firstname =$this->input->post('firstname');
				$lastname =$this->input->post('lastname');
				$email_id = $this->input->post('email');

				$residence =$this->input->post('residence');
				$message =$this->input->post('message');

				 $from_email = "info@nextstopcanadaimmigration.com"; 
		         //$to_email = $this->input->post('email'); 
		   
                $users_data=array(
                	'user_id' => $data['user_id'],
                   'first_name'      => $firstname,
                   'last_name '  => $lastname,                   
                   'email_id' 	=> $email_id,
                   'residence' 	=> $residence,                   
                   'message' => $message,
                   'created_at'	=> $this->created
			    );

		         $this->load->library('email'); 
		   
		         $this->email->from($from_email); 
		         //$this->email->from($from_email, 'info@nextstopcanadaimmigration.com'); 
		         $this->email->to($email_id);
		         $this->email->subject('Next Stop Canada'); 
		         $this->email->message($message);

		        if($this->email->send()){
			    	$this->session->set_flashdata("email_sent","Email sent successfully.");
				if ( $this->contact_model->add(  $this->table, $users_data ) )  // success
					{
						$this->session->set_flashdata('success', 'Contact have been added successfully');
						$_POST = '';
						redirect('website/thankyou');
						//redirect('contact/104');		
					}else{			// Failed password not matched					
						$this->session->set_flashdata('response_status', 'Error in adding database.');
						$_POST = '';
						redirect('contact/'.$data['page_data'][0]->id);
					}
				}else{
					$this->session->set_flashdata('response_status', 'Error in adding database.');
						$_POST = '';
						redirect('contact/'.$data['page_data'][0]->id);
					//$this->session->set_flashdata("email_sent","Error in sending Email.");
				}



			    /*if($this->email->send()){
			    	$this->session->set_flashdata("email_sent","Email sent successfully.");
				if ( $this->contact_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Contact have been added successfully');
					$_POST = '';
					redirect('website/thankyou');
					//redirect('contact/104');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('response_status', 'Error in adding database.');
					$_POST = '';
					redirect('contact/'.$data['page_data'][0]->id);
				}
			}else{
				$this->session->set_flashdata("email_sent","Error in sending Email.");
			}*/

			}			
		}



		$this->load->module('layouts');
		$this->load->library('template');		   
		$this->template
		->set_layout('websites')
		->build('contact',isset($data) ? $data : NULL);
	}

	function add_contact()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('websites')
		->build('contact',isset($data) ? $data : NULL);

		//echo "<pre>"; print_r($_POST); die;
		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('firstname', 'user name', 'required');
			$this->form_validation->set_rules('lastname', 'last name');
			$this->form_validation->set_rules('email', 'user email', 'required');	
			
            $this->form_validation->set_rules('residence', 'Current Residence');
            $this->form_validation->set_rules('message', 'Message', 'required');

			

		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('websites')
				->build('contact',isset($data) ? $data : NULL);
			
			}else{
				//$three_digit_random_number = mt_rand(100, 999);
				$firstname =$this->input->post('firstname');
				$lastname =$this->input->post('lastname');
				$email_id = $this->input->post('email');

				$residence =$this->input->post('residence');
				$message =$this->input->post('message');

				 $from_email = "sunpreet@mobilyte.com"; 
		         //$to_email = $this->input->post('email'); 
		   
                $users_data=array(
                	'user_id' => $data['user_id'],
                   'first_name'      => $firstname,
                   'last_name '  => $lastname,                   
                   'email_id' 	=> $email_id,
                   'residence' 	=> $residence,                   
                   'message' => $message,
                   'created_at'	=> $this->created
			    );

		         $this->load->library('email'); 
		   
		         $this->email->from($from_email, 'Sun'); 
		         $this->email->to($email_id);
		         $this->email->subject('Email Test'); 
		         $this->email->message($message);

			    if($this->email->send()){
			    	$this->session->set_flashdata("email_sent","Email sent successfully.");
				if ( $this->contact_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Contact have been added successfully');
					$_POST = '';
					redirect('contact');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('contact');
				}
			}else{
				$this->session->set_flashdata("email_sent","Error in sending Email.");
			}

			}			
		}
	}
}
	
/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */