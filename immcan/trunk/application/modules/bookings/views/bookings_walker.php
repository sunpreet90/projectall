<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_page.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/data-tables/DT_bootstrap.css" rel="stylesheet">
<!-- page heading start-->
<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href=" <?= site_url()?>">Dashboard</a>

        </li>
        <li class="active"> Walker Bookings </li>
    </ul>
</div>
<!-- page heading end-->
<?php // pr($notes); ?>

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                        <header class="panel-heading">
                            Bookings
                            <!--
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                             </span>
                         -->
                        </header>
                        <div class="panel-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" id="dynamic-table">
                                    <thead>
                                    <tr>
                                        <th>Sr.No.</th>
                                        <th>Walker Id</th>
                                        <th>Owner Id</th>
                                        <th>Pick Location</th>
                                        <th>Requested Time</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Walk Status</th>
                                        <th class="center">Action</th> 
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if ( is_array( $bookings )) 
                                        {
                                            $srno = 1;
                                            foreach ($bookings as $booking) 
                                            {
                                            ?>
                                            <tr class="gradeX">
                                                <td><?php echo $srno; ?></td>
                                                <td><?=$booking['walker_id']?></td>
                                                <td><?=$booking['owner_id']?></td>
                                                <td><?=$booking['pick_up_location']?></td>
                                                <td><?=$booking['created']?></td>
                                                <td><?=$booking['start_time']?></td>
                                                <td><?=$booking['end_time']?></td>
                                                <td>
                                                        <?php switch ($booking['walk_status']) {
                                                            case 0:
                                                                echo "Upcoming";
                                                                break;
                                                            case 1:
                                                                echo "Ongoing";
                                                                break;
                                                            case 2:
                                                                echo "Completed";
                                                                break;
                                                             case 3:
                                                                echo "Concelled";
                                                                break;    
                                                        } ?>
                                                </td>
                                                <td class="center hidden-phone">
                                                        <!--<a title="Edit" href="<?=base_url('Bookings')?>/edit/id/<?=$booking['id'];?>">
                                                             <i class="fa fa-pencil" aria-hidden="true"></i>
                                                        </a>-->
                                                        <a title="Delete" href="<?=base_url('bookings')?>/delete/id/<?=$booking['id'];?>" onclick="if(!confirm('Are you sure to delete this record?')){return false;}">
                                                           <i class="fa fa-eraser"></i>
                                                        </a>
                                                </td> 
                                            </tr>
                                          <?php 
                                                 $srno++;
                                             }
                                        }
                                        ?>
                                    </tbody>
                                    <!--<tfoot>
                                        <tr>
                                             <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>booking Type</th>
                                            <th class="center">Edit</th>
                                            <th class="center">Delete</th>
                                        </tr>
                                    </tfoot>-->
                                </table>
                            </div>
                        </div>
                    </section>
        </div>
    </div>
</div>
<!--Data tables script-->
<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=base_url('assets')?>/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="<?=base_url('assets')?>/text/javascript" src="<?=base_url('assets')?>/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="<?=base_url('assets') ?>/js/dynamic_table_init.js"></script>

<!--common scripts for all pages-->
<script src="<?=base_url('assets') ?>/js/scripts.js"></script>
<!--body wrapper end-->