<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'event_management';
	}
	
	function get_events()
	{
		//$where_data = array('user_type !=' => '0'); 
		//echo "<pre>";print_r($this->findWhere( $this->table, $where_data, $multi_record = TRUE ));die;
		//return $this->findWhere( $this->table, $multi_record = TRUE );
		return $this->db->get($this->table)->result_array();
	}
	
	function get_total_events(){
		$where_data = array('status !=' => 0);
		//echo "<pre>"; print_r($this->findWhere($this->table, $where_data, $multi_record = TRUE)); die;
		return $this->findWhere($this->table, $where_data, $multi_record = TRUE);
	}
	
	
	function get_bookings_walker($walker_id)
	{
	             $this->db
					->select("d.id, d.owner_id, d.walker_id, d.start_time, d.end_time, d.pick_up_location, d.pick_up_lat,d.pick_up_long, d.no_of_dogs,d.walk_status, d.instructions, d.created, u.name, u.user_type, u.profile_pic, u.fb_pic  
						")
					->from("ws_dog_walks d")
					->join("ws_users u", "d.walker_id = u.id")
					->order_by("created", "desc")
					->where(array("walker_id" => $walker_id));
					
					return $this->db->get()->result_array();
					
    }
    function get_bookings_owner($owner_id)
	{
			      $this->db
					->select("d.id, d.owner_id, d.walker_id, d.start_time, d.end_time, d.pick_up_location, d.pick_up_lat,d.pick_up_long, d.no_of_dogs,d.walk_status, d.instructions, d.created, u.name, u.user_type, u.profile_pic, u.fb_pic  
						")
					->from("ws_dog_walks d")
					->join("ws_users u", "d.owner_id = u.id")
					->order_by("created", "desc")
					->where(array("owner_id" => $owner_id));
					
					return $this->db->get()->result_array();
					
    }
	function get_booking($user_id)
	{
		$where_data = array('id' => $user_id );

		return $this->findWhere($this->table, $where_data, $multi_record = FALSE);

	}
	function delete_booking($user_id)
	{
     //echo $user_id;die;
		/*$where_data 	= array('id' => $user_id );
		//$post 	= array('status' => 0 );
		return $this->updateWhere($this->table, $where_data, $post);*/

        $where_data 	= array('id' => $user_id );
		//$this->db->where('activated', 0);
		return $this->db->delete($this->table,$where_data);
	}


}

/* End of file model.php */