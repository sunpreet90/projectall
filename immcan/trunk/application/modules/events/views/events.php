<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css">

<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active">Event Management </li>              
                
          <a href="<?=base_url('events/add'); ?>"><button class="btn btn-primary" style="float:right"><i class="fa fa-plus" aria-hidden="true"></i>Create Event</button>  </a>
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>

<div class="wrapper buyer-view order-view shop-cls-main pr-main-cls">
<!--<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <header>
                    <h2>Events</h2>
			    </header>
            </div>
        </div>
		<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 existing-cls-div">
            <div class="jumbotron container">
                   <form>
                        <div class="col-sm-3">
					   <div class="message-display-cls"><?//= $total_products; ?> <span>Total Events</span></div>
                        </div>
						   <div class="col-sm-3">
						<div class="message-display-cls"><?//= $new_products; ?><span>New Events</span></div>
                        </div>
					   <div class="col-sm-3">
						<div class="message-display-cls"><?//= $rejected_products; ?> <span>Rejected  Events</span></div>
                        </div>
						 <div class="col-sm-3">
						<h5>Best selling Product </h5>
						<p>Product Name : <?//= $best_product['product_name'];?></p>
						<p>Category : <?//= $best_product['name'];?></p>
						 <p>Price : <?//= $best_product['product_price'];?></p>
						<p>Net income: <?//= $best_product['total']; ?></p>
                        </div>
                    </form>
            </div>
            </div>
        </div>
    </div>-->
<!--=====================  table-1 start ================ -->
    <div class="col-lg-12 filter-input-cls">
        <section class="panel">
			 <header class="panel-heading center-text">
            Event Management
            </header>
            <div class="filter-users">
			<form id="filterform1" class="" action="#" method="post" role="">
			<div class="col-md-2 col-sm-3">
			   <label for="usr">Event Name </label>
				<input type="text" id="event_names">
				</div>
				<!--<div class="col-md-3 col-sm-3 input-filter-cls">
					<div class="form-group label-floating is-empty">
					<label class="control-label">Available quantity</label>
					<div class="date col-sm-5"><input type="text" id="min_quantity"></div>
					<span class="col-sm-1 text-center"> To </span>
					<div class="date col-sm-5"><input type="text" id="max_quantity"></div>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 input-filter-cls">
					<div class="form-group label-floating is-empty">
					<label class="control-label">Price</label>
					<div class="date col-sm-5"><input type="text" id="min_price"></div>
					<span class="col-sm-1 text-center"> To </span>
					<div class="date col-sm-5"><input type="text" id="max_price"></div>
					</div>
				</div>-->
				<div class="col-md-2 col-sm-3">
				 <label for="usr">Filter Status</label>
				<select class="form-control category_select">
					<option value="all">All</option>
				<option value="upcoming">Upcoming</option>
				<option value="ongoing">Ongoing</option>
				<option value="past">Past</option>
				<!--<option value="housing">Housing</option>
				<option value="retail">Retail</option>-->
				</select>
				</div>
				
				<div class="col-md-2 col-sm-3">
				 <label for="usr">Filter Duration</label>
				<select class="form-control" id="category_select">
					<option value="all">All</option>
				<option value="day">Day</option>
				<option value="week">Week</option>
				<option value="month">Month</option>
				<option value="season">Season</option>
				<!--<option value="housing">Housing</option>
				<option value="retail">Retail</option>-->
				</select>
				</div>
				
				
				
				<!--<div class="col-md-2 col-sm-3">
				 <label for="usr">Sort By</label>
				<select class="form-control" id="sort_select">
					<option value="all">All</option>
				<option value="eventasc">Event ID - Ascending</option>
				<option value="eventdsc">Event ID - Descending</option>
				<option value="ticketsasc">Tickets Sold- Ascending</option>
				<option value="ticketsdsc">Tickets Sold- Descending</option>
				<option value="ticketasc">Tickets Available- Ascending</option>
				<option value="ticketdsc">Tickets Available- Descending</option>
				<option value="amountasc">Ticket Amount - Ascending</option>
				<option value="amountdsc">Ticket Amount - Descending</option>
				</select>
				</div>-->
				
				
				</div>	
				<div class="clearfix"></div>
			</form>
            <div class="panel-body">
                <div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="dynamic-table1">
                        <thead>
							<tr>
								<th>S.No</th>
								<th>Event ID</th>
								<th>Event Name</th>
								<th>Location</th>
								<!-- <th>Shop Name</th> -->
								<th>Duration</th>
								<th>Tickets Sold</th>
								<th>Available Tickets</th>
								<th>Total Amount Earned</th>
								<th>State</th>
								<th>City</th>
								<th>Zipcode</th>
								<th>StartDate</th>
								<th>EndDate</th>
								<th>Status</th>
								<!-- <th>Total Amount</th> -->
								
								
							</tr>
						</thead>
                        <tbody>
							<?php
                        $count = 1;
						
                        foreach ($events as $event) { ?>
                            <tr class="gradeX">
                            	<td><?= $count; ?></td>
                               	<td><?=$event['id']?></td>
								<td><?=$event['event_name']?></td>
								<td><?=$event['address1'] ?>,<?=$event['address2']?></td>
								<td>Week</td>
								<td>12</td>
								<td>8</td>
								<td>25000</td>
								<td><?=$event['state']?></td>
								<td><?=$event['city']?></td>
								<td><?=$event['zipcode']?></td>
								<td><?=$event['startdate']?></td>
								<td><?=$event['enddate']?></td>
								<td>Upcoming</td>
								
                            </tr>
                        <?php $count++; } ?>
                        </tbody>
                    </table>
                </div>
				<div class="clearfix"></div>
            </div>   </div>
        </section>
    </div>
<!--===================== table-1 end ================ -->		
		 </div>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
 <!--<link href="jquery-ui.css" rel="stylesheet">-->
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
      <!-- Javascript -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js">
</script>
<!--Add custom filter methods-->
<script type="text/javascript">
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = parseInt($("#min_price").val(),10);
            var max = parseInt($("#max_price").val(),10);
            var columnValue = parseFloat(data[7] || 0);

            if((isNaN(min) && isNaN(max)) || (isNaN(min) && columnValue <= max) || (min <= columnValue && isNaN(max)) || (min <= columnValue && columnValue <= max)){
                return true;
            }
            return false;
        }
    );

    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var min = parseInt($("#min_quantity").val(),10);
            var max = parseInt($("#max_quantity").val(),10);
            var columnValue = parseFloat(data[6] || 0);

            if((isNaN(min) && isNaN(max)) || (isNaN(min) && columnValue <= max) || (min <= columnValue && isNaN(max)) || (min <= columnValue && columnValue <= max)){
                return true;
            }
            return false;
        }
    );
	
		<!-- Sort BY Event Status start -->
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var location = $(".category_select option:selected").val();
			//console.log("location");
            var columnValue = data[13];

            if(location.toLowerCase() == 'all'){
            	return true;
            }
            if(columnValue.toLowerCase().indexOf(location) >= 0){
              return true;
            }
            return false;
        }
    );
	
		<!-- Sort BY Event Status start End-->
	
	<!-- Sort BY Event Duration start -->
	$.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var location = $("#category_select option:selected").val();
			//console.log("location");
            var columnValue = data[4];

            if(location.toLowerCase() == 'all'){
            	return true;
            }
            if(columnValue.toLowerCase().indexOf(location) >= 0){
              return true;
            }
            return false;
        }
    );
	<!-- Sort BY Event Duration End-->
	
	<!-- Sort BY Event Id Start-->
	/* $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var location = $("#sort_select option:selected").val();
			//console.log("location");
            var columnValue = data[1];

            if(location.toLowerCase() == 'all'){
            	return true;
            }
            if(columnValue.toLowerCase().indexOf(location) >= 0){
              return true;
            }
            return false;
        }
    ); */
	<!-- Sort By Event ID End-->
	
<!-- Search BY Event Name -->
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            var location = $("#event_names").val();
            var columnValue = data[2];
            if(columnValue.toLowerCase().indexOf(location) >=0){
              return true;
            }
            return false;
        }
    );
	<!-- Search BY Event Name End -->
</script>
<!--dynamic table initialization -->
<script>
$(document).ready(function() {
   //var table = $('table.display').DataTable({});
   var table = $('table.display').DataTable( {
            "scrollX": true,
            "aaSorting": [[ 4, "desc" ]],
            dom: 'Bfrtip',
            buttons: [
                {
                    extend : 'excelHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
                    }
                },
                {
                    extend : 'csvHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
                    }
                },
                {
                    extend : 'pdfHtml5',
                    exportOptions : {
                        columns : [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
                    }
                }]
        });
   $("#min_price,#max_price").on('keyup',function(){
      table.draw();
   });
   $("#event_names").on('keyup', function(){
   	table.draw();
   });
   $( "#min_quantity,#max_quantity" ).on('keyup', function(){
   	table.draw();
   });
   $(".category_select").on('change', function(){
	   //alert("hello");
   	table.draw();
   });
   $("#category_select").on('change', function(){
	   //alert("hello");
   	table.draw();
   });
   $("#sort_select").on('change', function(){
   	table.draw();
   });
 } );
</script> 
<style>
.date input {
  background: transparent none repeat scroll 0 0 !important;
  border: 1px solid;
  position: relative;
  width: 100%;
  z-index: 1;
}
.date .fa.fa-calendar-o {
  font-size: 14px;
  position: absolute;
  right: 5px;
  top: 8px;
  z-index: 0;
}
.date {
  display: inline-block;
  position: relative;

}

.adv-table .dataTables_filter label {
  line-height: 2;
  vertical-align: middle;
  width: 100%;
}
.adv-table .dataTables_filter label input {
  float: right;
  height: 25px;
  width: auto;
}
</style>