<!-- page heading start-->
<link rel="stylesheet" type="text/css" href="<?=base_url('assets') ?>/js/bootstrap-datepicker/css/datepicker-custom.css" />


  <link rel="stylesheet" type="text/css" href="<?=base_url('assets') ?>/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets') ?>/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />

<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> Events Add </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                  Events Add
                </header>
                <div class="panel-body">
                    <form class="form-horizontal adminex-form" method="post" action="<?=base_url()?>events/add" >
                        <div class="form-group">
                            <label class="col-sm-1 col-sm-1 control-label">Event Name</label>
                            <div class="col-sm-5">
                                <input value="<?php echo set_value('event_name'); ?>" type="text" name="event_name" class="form-control">
                            </div>
							
							<label class="col-sm-1 col-sm-1 control-label">Zip Code</label>
                            <div class="col-sm-5">
                                <input value="<?php echo set_value('zipcode'); ?>" type="text" name="zipcode" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 col-sm-1 control-label">State</label>
                            <div class="col-sm-5">
                                <input value="<?php echo set_value('state'); ?>" type="text" name="state"  class="placepicker form-control" placeholder="">
                            </div>
							
							
							<label class="col-sm-1 col-sm-1 control-label">City</label>
                            <div class="col-sm-5">
                                <input value="<?php echo set_value('city'); ?>" type="text" name="city"  class="placepicker form-control" placeholder="">
                            </div>							
							
                        </div>
						
						<div class="form-group">
                            <label class="col-sm-1 col-sm-1 control-label">Enter Location</label>
                            <div class="col-sm-5">
                                <input value="<?php echo set_value('address1'); ?>" type="text" name="address1"  class="form-control" placeholder="Address1">
                            </div>
							
							 <label class="col-sm-1 col-sm-1 control-label"></label>
                            <div class="col-sm-5">
                                <input value="<?php echo set_value('address2'); ?>" type="text" name="address2"  class="form-control" placeholder="Address2">
                            </div>
							
                        </div>
						
						
						<div class="form-group">
                            <label class="col-sm-1 col-sm-1 control-label">Start Date</label>
                            <div class="col-sm-5"> 	

                                <input value="<?php echo set_value('startdate'); ?>" type="text" name="startdate" class="form-control dpd1" placeholder="">
                          	</div>
							
							 <label class="col-sm-1 col-sm-1 control-label"></label>
                            <div class="col-sm-5">
                                <input value="<?php echo set_value('enddate'); ?>" type="text" name="enddate"  class="form-control dpd2" placeholder="">
                            </div>
							
                        </div>
						
						
						<div class="form-group" id="price_div">
                            <label class="col-sm-1 col-sm-1 control-label">Price</label>
                            <div class="col-sm-5" id="day" style="display:none;">							
                                <input value="<?php echo set_value('p_day'); ?>" type="text" name="p_day"  class="form-control" placeholder="Price for Day">
                           	</div>
							
							 <label class="col-sm-1 col-sm-1 control-label"></label>
                            <div class="col-sm-5" id="week" style="display:none;">
                                <input value="<?php echo set_value('p_week'); ?>" type="text" name="p_week"  class="form-control" placeholder="Price for Week">
                            </div>
							
                        </div>
						
						<div class="form-group">
                            <label class="col-sm-1 col-sm-1 control-label"></label>
                            <div class="col-sm-5" id="month" style="display:none;">							
                                <input value="<?php echo set_value('p_month'); ?>" type="text" name="p_month"  class="form-control" placeholder="Price for Month">
                           	</div>
							
							 <label class="col-sm-1 col-sm-1 control-label"></label>
                            <div class="col-sm-5" id="season" style="display:none;">
                                <input value="<?php echo set_value('p_season'); ?>" type="text" name="p_season"  class="form-control" placeholder="Price for Season">
                            </div>
							
                        </div>
						
						
						
                        
                        
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
							
                                <button class="btn btn-primary" type="submit">Add Event</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url('assets') ?>/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=base_url('assets') ?>/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?=base_url('assets') ?>/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?=base_url('assets') ?>/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<!--pickers initialization-->
<script src="<?=base_url('assets') ?>/js/pickers-init.js"></script>


<!--common scripts for all pages-->
<script src="<?=base_url('assets') ?>/js/scripts.js"></script>
<script>
$(document).ready(function () {
	$('#price_div').hide();

$(".dpd2").on( "blur", function(){
	$(document).focus();
	var start_date =new Date($(".dpd1").val());
var end_date =new Date($(".dpd2").val());
var diff = end_date - start_date;
days = (diff/1000/60/60/24) + 1;
console.log(days);
if(days <= 6){	
$('#price_div').show();
$('#day').show();
$('#week').hide();
$('#month').hide();
$('#season').hide();
}else if(days <= 29){
	$('#price_div').show();
	$('#day').show();
	$('#week').show();
	$('#month').hide();
	$('#season').hide();
}else if(days > 30){
	$('#price_div').show();
	$('#day').show();
	$('#week').show();
	$('#month').show();
	$('#season').show();
}	
});
});

</script>
<!-- Placepicker -->

	<!--<script src="http://maps.google.com/maps/api/js?key=AIzaSyCr2_8BS0NmK7Ped-DOe_Z2dkDt0eYn93k&sensor=true&libraries=places"></script>-->
	<script src="http://maps.google.com/maps/api/js?key=AIzaSyC5X9-BJd9ZG6FVL-XoB-4ogt-RuVWbZVw&sensor=true&libraries=places"></script>

    <script src="<?=base_url('assets') ?>/jquery.placepicker.js"></script>

    <script>

      $(document).ready(function() {
        $(".placepicker").placepicker();
      }); 

    </script>
	
<!-- placepicker End -->



<!-- day wee month show/hide start phase2-->

<!--<script>
$(document).ready(function () {

    var d = new Date();
    var dayOfWeek = d.getDay();
    var hour = d.getHours();
    
    // open hours Monday - Friday 9am - 5:pm = open
    if (dayOfWeek === 6 || dayOfWeek === 0 || hour <= 9 || hour >= 17) {
        $('.hours').hide();
    }
    // closed any other time than above * working from 0am -9am but none other
    if (dayOfWeek === 6 || dayOfWeek === 0 || hour <= 0 || hour >= 9) {
        $('.closed').hide();
    }
     
});
</script>-->
<!-- day wee month show/hide end phase2 -->