<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//require_once('phpass-0.1/PasswordHash.php');
//require_once('phpass-0.1/PasswordHash.php');
/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Events extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tank_auth');
		//$this->ci->load->config('tank_auth', TRUE);
		// $this->load->library('utils');
		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}
		$this->load->library('form_validation');
		$this->load->model('event_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'event_management';
	}

	function index(){
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$data['events'] 	= $this->event_model->get_events();
		//echo "<pre>";print_r($data);die;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('events',isset($data) ? $data : NULL);
	}
	function bookings_walker(){
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$walker_id = $this->uri->segment(4);
		
        $data['bookings'] 	= $this->bookings_model->get_bookings_walker($walker_id);
		
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('bookings_walker',isset($data) ? $data : NULL);
	}
	function bookings_owner(){
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$owner_id = $this->uri->segment(4);
		
        $data['bookings'] 	= $this->bookings_model->get_bookings_owner($owner_id);
		
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('bookings_owner',isset($data) ? $data : NULL);
	}
	function add()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('events_add',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('event_name', 'event name', 'required');
			$this->form_validation->set_rules('zipcode', 'zipcode', 'required');
			
            $this->form_validation->set_rules('address1', 'address1', 'required');
            $this->form_validation->set_rules('address2', 'address2', 'required');
            $this->form_validation->set_rules('state', 'state', 'required');
            $this->form_validation->set_rules('city', 'city', 'required');
            $this->form_validation->set_rules('p_day', 'Day', 'required');
            $this->form_validation->set_rules('p_week', 'Week', 'required');
            $this->form_validation->set_rules('p_month', 'Month', 'required');
            $this->form_validation->set_rules('p_season', 'Season', 'required');
            

			//$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			//$this->form_validation->set_rules('phone', 'user phone', 'required|numeric');
			//$this->form_validation->set_rules('user_type', 'user type', 'required');

		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('events_add',isset($data) ? $data : NULL);
			
			}else{

				$eventname =$this->input->post('event_name');
				$zipcode = $this->input->post('zipcode');
				$state =$this->input->post('state');
				$city =$this->input->post('city');
				$address1 =$this->input->post('address1');
				$address2 =$this->input->post('address2');
				$startdate =$this->input->post('startdate');
				$enddate =$this->input->post('enddate');
				$p_day =$this->input->post('p_day');
				$p_week =$this->input->post('p_week');
				$p_month =$this->input->post('p_month');
				$p_season =$this->input->post('p_season');

				//$password =$this->input->post('password');

                //$hashed_password = $this->tank_auth->get_hash_password($password);

				//$user_type = $this->input->post('user_type');
               
                $events_data=array(
                   'event_name'       	=> $eventname,
                   'zipcode' 	=> $zipcode,
                   'state' 	=> $state,
                   'city' 	=> $city,
                   'address1' 	=> $address1,
                   'address2' 	=> $address2,
                   'startdate' 	=> $startdate,
                   'enddate' 	=> $enddate,
                   'day' 	=> $p_day,
                   'week' 	=> $p_week,
                   'month' 	=> $p_month,
                   'season' => $p_season,
                   'created'	=> $this->created
			    );
				if ( $this->event_model->add(  $this->table, $events_data ) )  // success
				{
					$this->session->set_flashdata('success', 'Event added successfully');
					$_POST = '';
					redirect('events');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('events/add');
				}
			}			
		}
	}
    function userkey_exists($key,$user_id) {
	    $this->table->mail_exists($key,$user_id);
	}
	function edit()
	{
		$user_id = $this->uri->segment(4);
		
		$user='';
		$user = $this->user_model->get_user($user_id);
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['user']		= $user;

		$this->load->module('layouts');
		$this->load->library('template');
        
        $this->template
		->set_layout('users')
		->build('users_edit',isset($data) ? $data : NULL);

        if ($this->input->post() ) 
		{	
            
			
            $this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'user name', 'required');
			//$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			if($this->input->post('email') != $user['email']) {
				$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
              //$is_unique =  '|is_unique['.$this->table.'.email]';
            } 
			
			$this->form_validation->set_rules('phone', 'user phone', 'required|numeric');
			$this->form_validation->set_rules('user_type', 'User type', 'required');
			
			if ( $this->form_validation->run() == FALSE )
			{
				// die("AAA");
				// echo validation_errors();
				// echo 'bad';die;
				/*$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'There is some error in fields');
				// print_r($this->session->set_flashdata);die;
				$_POST = '';
				// redirect('users/edit/id/'.$user_id);	
		        //$this->template
				// ->set_layout('users')
				// ->build('users_edit'.$user_id,isset($data) ? $data : NULL);*/

				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
          	    $this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('users_edit',isset($data) ? $data : NULL);
			}else{
				// echo 'good';die;
				$name =$this->input->post('name');
				$email = $this->input->post('email');
				$phone =$this->input->post('phone');
				$user_type = $this->input->post('user_type');
               
                $users_data=array(
                   'name'       	=> $name,
                   'email' 	=> $email,
                   'phone' 	=> $phone,
                   'user_type' 	=> $user_type,
                   'created'	=> $this->created
			    );
                  
			    $where_data=array( 'id' => $user_id );

				if ( $this->user_model->updateWhere(  $this->table, $where_data, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'User Updated successfully');
					$_POST = '';
					redirect('users/edit/id/'.$user_id);		
				}else{	// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('users/edit/id/'.$user_id);
				}
			}			
		}
	}
	function delete()
	{
		$user_id = $this->uri->segment(4);
		if($this->user_model->delete_user($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url().'users');
	}



}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */