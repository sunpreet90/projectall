<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_page.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/advanced-datatable/css/demo_table.css" rel="stylesheet">
<link href="<?=base_url('assets') ?>/js/data-tables/DT_bootstrap.css" rel="stylesheet">
<!-- page heading start-->
<div class="page-heading">
    <h3>Users Listing</h3>
    <ul class="breadcrumb">
        <li>
            <a href=" <?= site_url()?>">Dashboard</a>
        </li>
        <li class="active"> Users Listing </li>
    </ul>
</div>
<!-- page heading end-->
<?php  //pr($users); die; ?>

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Users Listing
                    <!--
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                     </span>
                 -->
                </header>
                <div class="panel-body">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                            <thead>
                            <tr>
                                <th>Sr.No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <!--<th>Age</th>
                                <th>Gender</th>-->
                                <th>Phone Number</th>
                                <!-- <th>Total Amount</th> -->
                                <!--<th>Paypal Account</th>
                                <th>Phone</th>-->
                                <!--<th>User Type</th>-->
                               <!--<th class="center">Edit</th>-->
                                <th class="center">Action</th> 
                            </tr>
                            </thead>
                            <tbody>
                                <?php 
								$tickets = 15;
								$ticket_price = 150;
                                if ( is_array( $users )) 
                                {
                                    $srno = 1;
                                    foreach ($users as $user) 
                                    {
                                      //if(!empty($user['name']) && !empty($user['email'])){
                                    ?>
                                    <tr class="gradeX">
                                        <td><?php echo $srno; ?></td>
                                        <td><?=$user['name']?></td>
                                        <td><?=$user['email']?></td>
                                        <!--<td><?//=$user['age']?></td>
                                        <td><?//=$user['gender']?></td>-->
                                       <!--  <td><?php echo '10' ?></td>
                                        <td><?php echo ($tickets * $ticket_price); ?></td> -->
                                        <!--<td><?//=$user['paypalAccount'] != ""? $user['paypalAccount']:"N/A"; ?></td>-->
                                        <td><?php echo $user['phone'] != ""? $user['phone']:"N/A"; ?></td>
                                        
                                        <!--<td><?//= $user['user_type'] == '1'?'User' :($user['user_type']=='2'?'Owner':"NA");  ?></td>-->
                                        <!--<td class="center hidden-phone"><a href="<?=base_url('users')?>/edit/id/<?=$user['id'];?>">Edit</a></td>-->
                                        <td class="center hidden-phone">
                                       
                                                <!--<a title="Edit" href="<?//=base_url('users')?>/edit/id/<?//=$user['id'];?>">
                                                     <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>-->
                                                <!--<a title="Delete" href="<?//=base_url('users')?>/delete/id/<?//=$user['id'];?>" onclick="if(!confirm('Are you sure to delete this record?')){return false;}">
                                                   <i class="fa fa-eraser"></i>
                                                </a>-->
                                                <?php //if($user['user_type'] == 1 || $user['user_type'] == 2){ ?>
							<a title="View" href="<?=base_url('users') ?>/user_profile/<?=$user['id']; ?>"><i class="fa fa-eye"></i>
                                                </a>
                                                <!--<a title="View" href="<?//=base_url('bookings')?>/bookings_<?php //echo $user['user_type'] == 1? "owner":"walker"; ?>/id/<?=$user['id'];?>">-->
                                                   
                                               <?php// } ?>
                                                <!--<a title="Change Password" onclick="Warn();" href="<?=base_url('users')?>/delete/id/<?=$user['id'];?>">
                                                    <i class="fa fa-key" aria-hidden="true"></i>
                                                </a>-->
                                           
                                            

                                        </td> 
                                    </tr>
                                    <?php $srno++;  }
                                   }
                                //}
                                ?>
                            </tbody>
                            <!--<tfoot>
                                <tr>
                                     <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>User Type</th>
                                    <th class="center">Edit</th>
                                    <th class="center">Delete</th>
                                </tr>
                            </tfoot>-->
                        </table>
                    </div>
                </div>
            </section>
            
        </div>
    </div>
</div>


<!--Data tables script-->




<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=base_url('assets')?>/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="<?=base_url('assets')?>/text/javascript" src="<?=base_url('assets')?>/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="<?=base_url('assets') ?>/js/dynamic_table_init.js"></script>

<!--common scripts for all pages-->
<script src="<?=base_url('assets') ?>/js/scripts.js"></script>

 <script type="text/javascript">
         <!--
            function Warn() {
               
              var retVal = confirm("Warning!You want to delete this user.");
               if( retVal == true ){
                  return true;
               }
               else{
                  return false;
               }
               
            }
         //-->
      </script>



<!--body wrapper end-->