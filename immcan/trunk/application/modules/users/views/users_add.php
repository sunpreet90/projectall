<!-- page heading start-->
<div class="page-heading">
    <h3>
        Dashboard
    </h3>
    <ul class="breadcrumb">
        <li>
            <a href="#">Dashboard</a>
        </li>
        <li class="active"> Add User </li>              
                
            
    </ul>
        <?php if($this->session->flashdata('success')): ?> 
            <div class='alert alert-success alert-block fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Success</i>
                    <p><?php echo $this->session->flashdata('success');?></p>
                </h4>
            </div>
        <?php endif; ?>
       <?php if ( strlen( validation_errors() ) > 0 ) : ?>
            <div class='alert alert-block alert-danger fade in'>
                <button data-dismiss="alert" class="close close-sm" type="button">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="icon-ok-sign">Errors</i>
                    <?php echo $this->session->flashdata('response_status');?>
                </h4>
                <p><?php echo validation_errors(); ?></p>
            </div>
        <?php endif; ?>



</div>
<!-- page heading end-->

<!--body wrapper start-->
<div class="wrapper">
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                  Add User
                </header>
                <div class="panel-body">
                    <form class="form-horizontal adminex-form" method="post" action="<?=base_url()?>users/add" >
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('name'); ?>" type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('email'); ?>" type="text" name="email"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('phone'); ?>" type="text" name="phone"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">User Type</label>
                            <div class="col-sm-10">
                              <!-- <input type="text" name="email"  class="form-control" placeholder=""> -->
                                 <select name="user_type"  class="form-control" >
                                      <option value="">Select User</option>
                                      <option value="1">User</option>
                                      <option value="2">Agent</option>
                                      
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('password'); ?>" type="password" name="password"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Comfirm-Password</label>
                            <div class="col-sm-10">
                                <input value="<?php echo set_value('confirm_password'); ?>" type="password" name="confirm_password"  class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-primary" type="submit">Add user</button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</div>
<!--body wrapper end-->

