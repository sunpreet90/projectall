    <div class="page-heading">
        <h3>Consultation All Listing</h3>
            
        <ul class="breadcrumb">
            <li>
                <a href="<?= site_url()?>">Dashboard</a>
            </li>
            <li>
                <a href="#">Consulation</a>
            </li>
            <li class="active"> View All List </li>
        </ul>
    </div>
    <!-- page heading end-->

    <!--body wrapper start-->
    <div class="wrapper">

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Consultant Types Listing &nbsp; &nbsp;
                        <!-- <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span> -->
                         <a href="<?=base_url('users/consultation_types')?>"><button class="btn btn-info" type="button">Add New</button></a>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        
                                        <th>Type</th>
                                        <th>Date</th>
                                       
                                       <!--  <th class="hidden-phone">Edit</th> -->
                                        <th class="hidden-phone">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <?php 


                                //echo "<pre>"; print_r($consultation_type); echo "</pre>"; die;

                                if ( !empty( $consultation_type )) 
                                {
                                    foreach ($consultation_type as $alltype) 
                                    {
                                    ?>
                                        <tr class="gradeX">
                                            <td><?=$alltype->consultation_type ?></td>
                                            <td><?=$alltype->created?></td>
                                            <!-- <td class="center hidden-phone"><a href="#">Edit</a></td> -->
                                            <td class="center hidden-phone">
                                                <a title="Delete" href="<?=base_url('users')?>/typedelete/id/<?=$alltype->id ?>" onclick="if(!confirm('Are you sure to delete this record?')){return false;}">
                                                           <i class="fa fa-eraser"></i>
                                                        </a>
                                            </td>
                                        </tr>
                            <?php   }
                                }
                            ?>
                                </tbody>

                                <tfoot>
                                        <tr>
                                        <th>Type</th>
                                        <th>Date</th>
                                        
                                       
                                        <th class="hidden-phone">Delete</th>
                                        </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>