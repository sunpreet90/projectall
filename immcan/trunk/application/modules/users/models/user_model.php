<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model
{	
	function __construct() {
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'users';
		$this->table1 = $this->prefix.'consultation_types';
	}
	public function mail_exists($email,$user_id)
	{
	    $this->db->where(array('email'=>$email,'id !='=>$user_id));
	    $query = $this->db->get($this->table);
	    print_r($query);die;
	    if ($query->num_rows() > 0){
	        return true;
	    }
	    else{
	        return false;
	    }
	}
	function get_users()
	{
		$where_data = array('user_type !=' => 0); 
		//echo "<pre>";print_r($this->findWhere( $this->table, $where_data, $multi_record = TRUE ));die;
		return $this->findWhere( $this->table, $where_data, $multi_record = TRUE );
	}
	
	function get_users_all()
	{
		$this->db->select('*');
		$this->db->from('ws_users');
		//$this->db->where('id',$id);
		return $this->db->get()->result();
	}
	

	function consultation_alltypes(){
		$this->db->select('*');
		$this->db->from('ws_consultation_types');
		return $this->db->get()->result();
	}
	
	function get_activated_users(){
		$where_data = array('user_type !=' => 0); 
		//$where_data = array('isVerified !=' => 0);
		//$where_data = array('username !=' => 'admin');
		//echo "<pre>"; print_r($this->findWhere($this->table, $where_data,$multi_record = TRUE)); die;
		return $this->findWhere($this->table, $where_data,$multi_record = TRUE);
	}
	
	
	
	function get_user($user_id)
	{
		$where_data = array('id' => $user_id );
		return $this->findWhere($this->table, $where_data, $multi_record = FALSE);

	}
	function delete_user($user_id)
	{
     //echo $user_id;die;
		/*$where_data 	= array('id' => $user_id );
		//$post 	= array('status' => 0 );
		return $this->updateWhere($this->table, $where_data, $post);*/

        $where_data 	= array('id' => $user_id );
		//$this->db->where('activated', 0);
		return $this->db->delete($this->table,$where_data);
	}


	function delete_types($user_id){
		$where_data = array('id' => $user_id);
		return $this->db->delete($this->table1,$where_data);
	}


}

/* End of file model.php */