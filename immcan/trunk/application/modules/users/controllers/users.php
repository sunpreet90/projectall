<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//require_once('phpass-0.1/PasswordHash.php');
//require_once('phpass-0.1/PasswordHash.php');
/**
 * MX_Controller
 *
 * @package	Package Name
 * @subpackage	Subpackage
 * @category	Category
 * @author	Harish Chauhan
 * @link	https://google.com
 */
class Users extends MX_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('tank_auth');
		//$this->ci->load->config('tank_auth', TRUE);
		// $this->load->library('utils');
		if (!$this->tank_auth->get_username()) {
			$this->session->set_flashdata('message', 'You are not allowed to access this page. Please contact the system admin for assistance.');
			redirect('');
		}
		$this->load->library('form_validation');
		$this->load->model('user_model');
		$this->prefix = $this->config->item('db_table_prefix');
		$this->created = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->last_login = date('Y-m-d H:i:s', time());		// TIME STAMP
		$this->table = $this->prefix.'users';
		$this->table1 = $this->prefix.'consultation_types';
	}

	function index(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		//$data['username']	= $this->tank_auth->get_username();

		$data['users'] 	= $this->user_model->get_activated_users();
		//echo "<pre>"; print_r($data['users']); die;
		
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('users',isset($data) ? $data : NULL);
	}


	function add()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('users_add',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'user name', 'required');
			$this->form_validation->set_rules('password', 'password', 'required');
			
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[password]');

			$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			$this->form_validation->set_rules('phone', 'user phone', 'required|numeric');
			$this->form_validation->set_rules('user_type', 'user type', 'required');

		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('users_add',isset($data) ? $data : NULL);
			
			}else{

				$name =$this->input->post('name');
				$email = $this->input->post('email');
				$phone =$this->input->post('phone');

				$password =$this->input->post('password');

                $hashed_password = $this->tank_auth->get_hash_password($password);

				$user_type = $this->input->post('user_type');
               
                $users_data=array(
                   'name'       	=> $name,
                   'username'       	=> $name,
                   'email' 	=> $email,
                   'phone' 	=> $phone,
                   'password' 	=> $hashed_password,
                   'user_type' 	=> $user_type,
                   'created'	=> $this->created
			    );
				if ( $this->user_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'User have been added successfully');
					$_POST = '';
					redirect('users/add');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('users/add');
				}
			}			
		}
	}


	/*function add()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('users_add',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'user name', 'required');
			$this->form_validation->set_rules('password', 'password', 'required');
			
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[password]');

			$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			$this->form_validation->set_rules('age', 'age', 'required');
			$this->form_validation->set_rules('phone', 'user phone', 'required|numeric');
			$this->form_validation->set_rules('user_type', 'user type', 'required');

		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('users_add',isset($data) ? $data : NULL);
			
			}else{

				$name =$this->input->post('name');
				$email = $this->input->post('email');
				$age =$this->input->post('age');
				$gender =$this->input->post('gender');
				$phone =$this->input->post('phone');
				$password =$this->input->post('password');

                $hashed_password = $this->tank_auth->get_hash_password($password);

				$user_type = $this->input->post('user_type');
               
                $users_data=array(
                   'name'       	=> $name,
                   'email' 	=> $email,
				   'age' => $age,
				   'gender' => $gender,
                   'phone' 	=> $phone,
                   'password' 	=> $hashed_password,
                   'user_type' 	=> $user_type,
                   'created'	=> $this->created
			    );
				if ( $this->user_model->add(  $this->table, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'User have been added successfully');
					$_POST = '';
					redirect('users/add');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('users/add');
				}
			}			
		}
	}*/
    function userkey_exists($key,$user_id) {
	    $this->table->mail_exists($key,$user_id);
	}
	function edit()
	{
		$user_id = $this->uri->segment(4);
		
		$user='';
		$user = $this->user_model->get_user($user_id);
		
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['user']		= $user;

		$this->load->module('layouts');
		$this->load->library('template');
        
        $this->template
		->set_layout('users')
		->build('users_edit',isset($data) ? $data : NULL);

        if ($this->input->post() ) 
		{	
            
			
            $this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'user name', 'required');
			//$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
			if($this->input->post('email') != $user['email']) {
				$this->form_validation->set_rules('email', 'user email', 'valid_email|required|is_unique['.$this->table.'.email]');	
              //$is_unique =  '|is_unique['.$this->table.'.email]';
            } 
			
			$this->form_validation->set_rules('phone', 'user phone', 'required|numeric');
			$this->form_validation->set_rules('user_type', 'User type', 'required');
			
			if ( $this->form_validation->run() == FALSE )
			{
				// die("AAA");
				// echo validation_errors();
				// echo 'bad';die;
				/*$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'There is some error in fields');
				// print_r($this->session->set_flashdata);die;
				$_POST = '';
				// redirect('users/edit/id/'.$user_id);	
		        //$this->template
				// ->set_layout('users')
				// ->build('users_edit'.$user_id,isset($data) ? $data : NULL);*/

				$data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';
          	    $this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('users_edit',isset($data) ? $data : NULL);
			}else{
				// echo 'good';die;
				$name =$this->input->post('name');
				$email = $this->input->post('email');
				$phone =$this->input->post('phone');
				$user_type = $this->input->post('user_type');
               
                $users_data=array(
                   'name'       	=> $name,
                   'email' 	=> $email,
                   'phone' 	=> $phone,
                   'user_type' 	=> $user_type,
                   'created'	=> $this->created
			    );
                  
			    $where_data=array( 'id' => $user_id );

				if ( $this->user_model->updateWhere(  $this->table, $where_data, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'User Updated successfully');
					$_POST = '';
					redirect('users/edit/id/'.$user_id);		
				}else{	// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('users/edit/id/'.$user_id);
				}
			}			
		}
	}
	function delete()
	{
		$user_id = $this->uri->segment(4);
		if($this->user_model->delete_user($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url().'users');
	}
	
	
	function user_profile($id){
		//echo $id; die;
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['buyer_data'] = $this->db->get_where('ws_users',array('id' => $id))->row_array();
		//print_r($data['buyer_data']); die;
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('users_profile',isset($data) ? $data : NULL);


		
	}



	function consultation_types(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();

		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('consultation_types',isset($data) ? $data : NULL);
	}




	function consultations()
	{
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('consultation_types',isset($data) ? $data : NULL);


		if ($this->input->post()) 
		{	
			$this->form_validation->set_error_delimiters('<span style="color:red">', '</span><br>');
			$this->form_validation->set_rules('name', 'user name', 'required');	

		   
            if ( $this->form_validation->run() == FALSE )
			{
                $data['user_id']	= $this->tank_auth->get_user_id();
				$data['username']	= $this->tank_auth->get_username();
				$this->session->set_flashdata('error', 'Please add required fieds');
				$_POST = '';

				$this->load->module('layouts');
				$this->load->library('template');
				$this->template
				->set_layout('users')
				->build('consultation_types',isset($data) ? $data : NULL);
			
			}else{

				$name =$this->input->post('name');				
               
                $users_data=array(
                   'consultation_type'    => $name,                   
                   'created'	=> $this->created
			    );
				if ( $this->user_model->add(  $this->table1, $users_data ) )  // success
				{
					$this->session->set_flashdata('success', 'consultation type have been added successfully');
					$_POST = '';
					redirect('users/all_types');		
				}else{			// Failed password not matched					
					$this->session->set_flashdata('error', 'Error in adding database.');
					$_POST = '';
					redirect('users/consultations');
				}
			}			
		}
	}


	function all_types(){
		$data['user_id']	= $this->tank_auth->get_user_id();
		$data['username']	= $this->tank_auth->get_username();
		$data['consultation_type'] = $this->user_model->consultation_alltypes();
		//echo "<pre>"; print_r($data['consultation_type']); echo "</pre>";
		//$data['consultation_type'] = $this->db->get_where('consultation_types',array('id' => $id))->row_array();
		$this->load->module('layouts');
		$this->load->library('template');
		$this->template
		->set_layout('users')
		->build('all_types',isset($data) ? $data : NULL);
	}


	function typedelete()
	{
		$user_id = $this->uri->segment(4);
		if($this->user_model->delete_types($user_id)){
			                $this->session->set_flashdata('response_status', 'success');
						    //$this->session->set_flashdata('response_status', 'error');
							$this->session->set_flashdata('message', 'Record have been deleted successfully');
		}

		header('Location: '.base_url().'users/all_types');
	}
	
	/* public function viewproduct($id) {
        //echo $id;
        if ($id) {
            $this->db->where('product_id', $id);
            $this->db->set('view_count', '`view_count`+ 1', FALSE);
            $this->db->update('ws_products');
            $data['arts']           = $this->ProductModel->GetProduct($id);
            $data['ws_shop_images'] = $this->InitModel->showshopimages();
            
            //echo "<pre>", print_r($data) ;die();
        }

        $this->template->title('View Product', 'The product detail page.');

        $data['include_js'] = 'view-product';
        $data['include_css'] = 'view-product';
        $data['active_menu'] = 'my-products';

        $this->template
        ->set_layout('default') // application/views/layouts/two_col.php
        ->build('view-product', $data); // views/welcome_message
    } */
	
	
	

}

/* End of file welcome.php */
/* Location: ./application/modules/welcome/controllers/welcome.php */