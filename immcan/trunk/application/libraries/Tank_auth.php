<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('phpass-0.1/PasswordHash.php');

define('STATUS_ACTIVATED', '1');
define('STATUS_NOT_ACTIVATED', '0');

/**
 * Tank_auth
 *
 * Authentication library for Code Igniter.
 *
 * @package		Tank_auth
 * @author		Ilya Konyukhov (http://konyukhov.com/soft/)
 * @version		1.0.9
 * @based on	DX Auth by Dexcell (http://dexcell.shinsengumiteam.com/dx_auth)
 * @license		MIT License Copyright (c) 2008 Erick Hartanto
 */
class Tank_auth
{
	private $error = array();

	function __construct()
	{
		$this->ci =& get_instance();

		$this->ci->load->config('tank_auth', TRUE);

		$this->ci->load->library('session');
		$this->ci->load->database();
		$this->ci->load->model('tank_auth/users_model','users');
		
        // Try to autologin
		$this->autologin();
	}

	/**
	 * Login user on the site. Return TRUE if login is successful
	 * (user exists and activated, password is correct), otherwise FALSE.
	 *
	 * @param	string	(username or email or both depending on settings in config file)
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function login($login, $password, $remember, $login_by_username, $login_by_email)
	{
		if ((strlen($login) > 0) AND (strlen($password) > 0)) {

			// Which function to use to login (based on config)
			if ($login_by_username AND $login_by_email) {
				$get_user_func = 'get_user_by_login';
			} else if ($login_by_username) {
				$get_user_func = 'get_user_by_username';
			} else {
				$get_user_func = 'get_user_by_email';
			}

			if (!is_null($user = $this->ci->users->$get_user_func($login))) {	// login ok

				// Does password match hash in database?
				$hasher = new PasswordHash(
						$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
						$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
				if ($hasher->CheckPassword($password, $user->password)) {		// password ok

					if ($user->banned == 1) {									// fail - banned
						$this->error = array('banned' => $user->ban_reason);

					} else {
						$this->ci->session->set_userdata(array(
								'user_id'	=> $user->id,
								'username'	=> $user->username,
								'status'	=> ($user->activated == 1) ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED,
						));

						if ($user->activated == 0) {							// fail - not activated
							$this->error = array('not_activated' => '');

						} else {												// success
							if ($remember) {
								$this->create_autologin($user->id);
							}

							$this->clear_login_attempts($login);

							$this->ci->users->update_login_info(
									$user->id,
									$this->ci->config->item('login_record_ip', 'tank_auth'),
									$this->ci->config->item('login_record_time', 'tank_auth'));
							return TRUE;
						}
					}
				} else {														// fail - wrong password
					$this->increase_login_attempt($login);
					$this->error = array('password' => 'auth_incorrect_password');
				}
			} else {															// fail - wrong login
				$this->increase_login_attempt($login);
				$this->error = array('login' => 'auth_incorrect_login');
			}
		}
		return FALSE;
	}

	/**
	 * Logout user from the site
	 *
	 * @return	void
	 */
	function logout()
	{
		$this->delete_autologin();

		// See http://codeigniter.com/forums/viewreply/662369/ as the reason for the next line
		$this->ci->session->set_userdata(array('user_id' => '', 'username' => '', 'status' => ''));

		$this->ci->session->sess_destroy();
	}

	/**
	 * Check if user logged in. Also test if user is activated or not.
	 *
	 * @param	bool
	 * @return	bool
	 */
	function is_logged_in($activated = TRUE)
	{
		return $this->ci->session->userdata('status') === ($activated ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED);
	}

	/**
	 * Get user_id
	 *
	 * @return	string
	 */
	function get_user_id()
	{
		return $this->ci->session->userdata('user_id');
	}

	/**
	 * Get username
	 *
	 * @return	string
	 */
	function get_username()
	{
		return $this->ci->session->userdata('username');
	}
	function get_hash_password($password){
                $hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			    $hashed_password = $hasher->HashPassword($password);
			    return $hashed_password;
	}
	/**
	 * Create new user on the site and return some data about it:
	 * user_id, username, password, email, new_email_key (if any).
	 *
	 * @param	string
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	array
	 */
	function create_user($username, $email, $password, $email_activation)
	{
		if ((strlen($username) > 0) AND !$this->ci->users->is_username_available($username)) {
			$this->error = array('username' => 'auth_username_in_use');

		} elseif (!$this->ci->users->is_email_available($email)) {
			$this->error = array('email' => 'auth_email_in_use');

		} else {
			// Hash password using phpass
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			$hashed_password = $hasher->HashPassword($password);

			$data = array(
				'username'	=> $username,
				'password'	=> $hashed_password,
				'email'		=> $email,
				'last_ip'	=> $this->ci->input->ip_address(),
			);

			if ($email_activation) {
				$data['new_email_key'] = md5(rand().microtime());
			}
			if (!is_null($res = $this->ci->users->create_user($data, !$email_activation))) {
				$data['user_id'] = $res['user_id'];
				$data['password'] = $password;
				unset($data['last_ip']);
				return $data;
			}
		}
		return NULL;
	}

	/**
	 * Check if username available for registering.
	 * Can be called for instant form validation.
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username)
	{
		return ((strlen($username) > 0) AND $this->ci->users->is_username_available($username));
	}

	/**
	 * Check if email available for registering.
	 * Can be called for instant form validation.
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email)
	{
		return ((strlen($email) > 0) AND $this->ci->users->is_email_available($email));
	}

	/**
	 * Change email for activation and return some data about user:
	 * user_id, username, email, new_email_key.
	 * Can be called for not activated users only.
	 *
	 * @param	string
	 * @return	array
	 */
	function change_email($email)
	{
		$user_id = $this->ci->session->userdata('user_id');

		if (!is_null($user = $this->ci->users->get_user_by_id($user_id, FALSE))) {

			$data = array(
				'user_id'	=> $user_id,
				'username'	=> $user->username,
				'email'		=> $email,
			);
			if (strtolower($user->email) == strtolower($email)) {		// leave activation key as is
				$data['new_email_key'] = $user->new_email_key;
				return $data;

			} elseif ($this->ci->users->is_email_available($email)) {
				$data['new_email_key'] = md5(rand().microtime());
				$this->ci->users->set_new_email($user_id, $email, $data['new_email_key'], FALSE);
				return $data;

			} else {
				$this->error = array('email' => 'auth_email_in_use');
			}
		}
		return NULL;
	}

	/**
	 * Activate user using given key
	 *
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function activate_user($user_id, $activation_key, $activate_by_email = TRUE)
	{
		$this->ci->users->purge_na($this->ci->config->item('email_activation_expire', 'tank_auth'));

		if ((strlen($user_id) > 0) AND (strlen($activation_key) > 0)) {
			return $this->ci->users->activate_user($user_id, $activation_key, $activate_by_email);
		}
		return FALSE;
	}

	/**
	 * Set new password key for user and return some data about user:
	 * user_id, username, email, new_pass_key.
	 * The password key can be used to verify user when resetting his/her password.
	 *
	 * @param	string
	 * @return	array
	 */
	function forgot_password($login)
	{
	    //print_r($login);die;
	    $new_email_key  = mt_rand(100000, 999999);
		if (strlen($login) > 0) {
			if (!is_null($user = $this->ci->users->get_user_by_login($login))) {

				$data = array(
					'user_id'		=> $user->id,
					'username'		=> $user->username,
					'email'			=> $user->email,
					'new_pass_key'	=> md5(rand().microtime()),
				);

				$this->ci->users->set_password_key($user->id, $data['new_pass_key']);
				return $data;

			} else {
				$this->error = array('login' => 'auth_incorrect_email_or_username');
			}
		}
		return NULL;
	}

	/**
	 * Check if given password key is valid and user is authenticated.
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function can_reset_password($user_id, $new_pass_key)
	{
          
		if ((strlen($user_id) > 0) AND (strlen($new_pass_key) > 0)) {
			return $this->ci->users->can_reset_password(
				$user_id,
				$new_pass_key,
				$this->ci->config->item('forgot_password_expire', 'tank_auth'));
		}
		return FALSE;
	}

	/**
	 * Replace user password (forgotten) with a new one (set by user)
	 * and return some data about it: user_id, username, new_password, email.
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function reset_password($user_id, $new_pass_key, $new_password)
	{
		//echo "user:".$user_id."newpaskey:".$new_pass_key."newpass".$new_password;die;

           if ((strlen($user_id) > 0) AND (strlen($new_pass_key) > 0) AND (strlen($new_password) > 0)) {
        
			if (!is_null($user = $this->ci->users->get_user_detail_by_id($user_id))) {
			//if (!is_null($user = $this->ci->users->get_user_by_id($user_id, TRUE))) {
				//echo "in tank auth";die;
                // Hash password using phpass
				$hasher = new PasswordHash(
						$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
						$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
				$hashed_password = $hasher->HashPassword($new_password);

				if ($this->ci->users->reset_password(
						$user_id,
						$hashed_password,
						$new_pass_key,
						$this->ci->config->item('forgot_password_expire', 'tank_auth'))) {	// success

					// Clear all user's autologins
					//$this->ci->load->model('tank_auth/user_autologin');
					//$this->ci->user_autologin->clear($user->id);

					return array(
						'user_id'		=> $user_id,
						'username'		=> $user->username,
						'email'			=> $user->email,
						'new_password'	=> $new_password,
					);
                   

				}
			}
		}
		return NULL;
	}

	/**
	 * Change user password (only when user is logged in)
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function change_password($old_pass, $new_pass)
	{
		$user_id = $this->ci->session->userdata('user_id');



		if (!is_null($user = $this->ci->users->get_user_detail_by_id($user_id, TRUE))) 
		{

			// Check if old password correct
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			if ($hasher->CheckPassword($old_pass, $user->password)) {			// success

				// Hash new password using phpass
				$hashed_password = $hasher->HashPassword($new_pass);

				// Replace old password with new one
				$this->ci->users->change_password($user_id, $hashed_password);
				return TRUE;

			}else{	
			    														// fail
				$this->error = array('old_password' => 'auth_incorrect_password');
			}
		}
		return FALSE;
	}

	/**
	 * Change user email (only when user is logged in) and return some data about user:
	 * user_id, username, new_email, new_email_key.
	 * The new email cannot be used for login or notification before it is activated.
	 *
	 * @param	string
	 * @param	string
	 * @return	array
	 */
	function set_new_email($new_email, $password)
	{
		$user_id = $this->ci->session->userdata('user_id');

		if (!is_null($user = $this->ci->users->get_user_by_id($user_id, TRUE))) {

			// Check if password correct
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			if ($hasher->CheckPassword($password, $user->password)) {			// success

				$data = array(
					'user_id'	=> $user_id,
					'username'	=> $user->username,
					'new_email'	=> $new_email,
				);

				if ($user->email == $new_email) {
					$this->error = array('email' => 'auth_current_email');

				} elseif ($user->new_email == $new_email) {		// leave email key as is
					$data['new_email_key'] = $user->new_email_key;
					return $data;

				} elseif ($this->ci->users->is_email_available($new_email)) {
					$data['new_email_key'] = md5(rand().microtime());
					$this->ci->users->set_new_email($user_id, $new_email, $data['new_email_key'], TRUE);
					return $data;

				} else {
					$this->error = array('email' => 'auth_email_in_use');
				}
			} else {															// fail
				$this->error = array('password' => 'auth_incorrect_password');
			}
		}
		return NULL;
	}

	/**
	 * Activate new email, if email activation key is valid.
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function activate_new_email($user_id, $new_email_key)
	{
		if ((strlen($user_id) > 0) AND (strlen($new_email_key) > 0)) {
			return $this->ci->users->activate_new_email(
					$user_id,
					$new_email_key);
		}
		return FALSE;
	}

	/**
	 * Delete user from the site (only when user is logged in)
	 *
	 * @param	string
	 * @return	bool
	 */
	function delete_user($password)
	{
		$user_id = $this->ci->session->userdata('user_id');

		if (!is_null($user = $this->ci->users->get_user_by_id($user_id, TRUE))) {

			// Check if password correct
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			if ($hasher->CheckPassword($password, $user->password)) {			// success

				$this->ci->users->delete_user($user_id);
				$this->logout();
				return TRUE;

			} else {															// fail
				$this->error = array('password' => 'auth_incorrect_password');
			}
		}
		return FALSE;
	}

	/**
	 * Get error message.
	 * Can be invoked after any failed operation such as login or register.
	 *
	 * @return	string
	 */
	function get_error_message()
	{
		return $this->error;
	}

	/**
	 * Save data for user's autologin
	 *
	 * @param	int
	 * @return	bool
	 */
	private function create_autologin($user_id)
	{
		$this->ci->load->helper('cookie');
		$key = substr(md5(uniqid(rand().get_cookie($this->ci->config->item('sess_cookie_name')))), 0, 16);

		$this->ci->load->model('tank_auth/user_autologin');
		$this->ci->user_autologin->purge($user_id);

		if ($this->ci->user_autologin->set($user_id, md5($key))) {
			set_cookie(array(
					'name' 		=> $this->ci->config->item('autologin_cookie_name', 'tank_auth'),
					'value'		=> serialize(array('user_id' => $user_id, 'key' => $key)),
					'expire'	=> $this->ci->config->item('autologin_cookie_life', 'tank_auth'),
			));
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Clear user's autologin data
	 *
	 * @return	void
	 */
	private function delete_autologin()
	{
		$this->ci->load->helper('cookie');
		if ($cookie = get_cookie($this->ci->config->item('autologin_cookie_name', 'tank_auth'), TRUE)) {

			$data = unserialize($cookie);

			$this->ci->load->model('tank_auth/user_autologin');
			$this->ci->user_autologin->delete($data['user_id'], md5($data['key']));

			delete_cookie($this->ci->config->item('autologin_cookie_name', 'tank_auth'));
		}
	}

	/**
	 * Login user automatically if he/she provides correct autologin verification
	 *
	 * @return	void
	 */
	private function autologin()
	{
		if (!$this->is_logged_in() AND !$this->is_logged_in(FALSE)) {			// not logged in (as any user)

			$this->ci->load->helper('cookie');
			if ($cookie = get_cookie($this->ci->config->item('autologin_cookie_name', 'tank_auth'), TRUE)) {

				$data = unserialize($cookie);

				if (isset($data['key']) AND isset($data['user_id'])) {

					$this->ci->load->model('tank_auth/user_autologin');
					if (!is_null($user = $this->ci->user_autologin->get($data['user_id'], md5($data['key'])))) {

						// Login user
						$this->ci->session->set_userdata(array(
								'user_id'	=> $user->id,
								'username'	=> $user->username,
								'status'	=> STATUS_ACTIVATED,
						));

						// Renew users cookie to prevent it from expiring
						set_cookie(array(
								'name' 		=> $this->ci->config->item('autologin_cookie_name', 'tank_auth'),
								'value'		=> $cookie,
								'expire'	=> $this->ci->config->item('autologin_cookie_life', 'tank_auth'),
						));

						$this->ci->users->update_login_info(
								$user->id,
								$this->ci->config->item('login_record_ip', 'tank_auth'),
								$this->ci->config->item('login_record_time', 'tank_auth'));
						return TRUE;
					}
				}
			}
		}
		return FALSE;
	}

	/**
	 * Check if login attempts exceeded max login attempts (specified in config)
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_max_login_attempts_exceeded($login)
	{
		if ($this->ci->config->item('login_count_attempts', 'tank_auth')) {
			$this->ci->load->model('tank_auth/login_attempts');
			return $this->ci->login_attempts->get_attempts_num($this->ci->input->ip_address(), $login)
					>= $this->ci->config->item('login_max_attempts', 'tank_auth');
		}
		return FALSE;
	}

	/**
	 * Increase number of attempts for given IP-address and login
	 * (if attempts to login is being counted)
	 *
	 * @param	string
	 * @return	void
	 */
	private function increase_login_attempt($login)
	{
		if ($this->ci->config->item('login_count_attempts', 'tank_auth')) {
			if (!$this->is_max_login_attempts_exceeded($login)) {
				$this->ci->load->model('tank_auth/login_attempts');
				$this->ci->login_attempts->increase_attempt($this->ci->input->ip_address(), $login);
			}
		}
	}

	/**
	 * Clear all attempt records for given IP-address and login
	 * (if attempts to login is being counted)
	 *
	 * @param	string
	 * @return	void
	 */
	private function clear_login_attempts($login)
	{
		if ($this->ci->config->item('login_count_attempts', 'tank_auth')) {
			$this->ci->load->model('tank_auth/login_attempts');
			$this->ci->login_attempts->clear_attempts(
					$this->ci->input->ip_address(),
					$login,
					$this->ci->config->item('login_attempt_expire', 'tank_auth'));
		}
	}


function login_app($login, $password, $login_by_username, $login_by_email, $ip_address = '', $user_agent = '')
	{
	if ((strlen($login) > 0) AND (strlen($password) > 0))
		{
		if (!is_null($user = $this->ci->users->get_user_by_email($login)))
			{ // login ok

			// Does password match hash in database?

			$hasher = new PasswordHash($this->ci->config->item('phpass_hash_strength', 'tank_auth') , $this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			if ($hasher->CheckPassword($password, $user->password))

			// if ( $password == $user->password ) // password ok

				{ // password ok

				// pr($user);

				if ($user->banned == 1)
					{

					// fail - banned

					$data['status'] = 301;
					$data['message'] = $user->ban_reason;
					}
				elseif ($user->activated != 1)
					{
					
					      foreach($user as $key => $value)
							{
							if (is_null($value))
								{
								$user->$key = "";
								}
							}

						   $data['status'] = 200;
                           $data["message"] = 'Login successfully';

                            $data['data']["user_id"] = $user->id;
							//$data['data']['user_type'] = $user->user_type;
							//$data['data']['isSocial'] = $user->isSocial;
	                        $data['data']['isVerified'] = $user->isVerified;
	                        //$data['data']['isProfileCompleted'] = $user->isProfileCompleted;

					}
				
				elseif($user->isVerified == 0 ){
					$data['status']; 
				}	
					
				  else
					{ //
					if ($user->activated == 0)
						{

						// fail - not activated

						$data['status'] = 301;
						$data['message'] = 'Account not activated';
						}
					  else
						{ // success
						$this->clear_login_attempts($login);
						$user->user_id = $user->id;
						$this->ci->users->update_login_info($user->user_id, $this->ci->config->item('login_record_ip', 'tank_auth') , $this->ci->config->item('login_record_time', 'tank_auth'));

						// return TRUE;

						foreach($user as $key => $value)
							{
							if (is_null($value))
								{
								$user->$key = "";
								}
							}

						$data['status'] = 200;

						// pr($user);
						// $user['user_id'] = $user['id'];

						$data['data'] = $user;
						}
					}
				}
			  else
				{

				// fail - wrong password

				$this->increase_login_attempt($login);
				$data['status'] = 301;
				$data['message'] = 'Incorrect password';
				}
			}
		  else
			{

			// fail - wrong login

			$this->increase_login_attempt($login);
			$data['status'] = 301;

			// $data['data'] = FALSE;

			$data['message'] = 'Email id does not exist.';
			}
		}
	return $data;

	}
	//Login with facebook

    function login_with_facebook_app($facebook_id,$login,$phone, $name,$profile_pic,$gender,$login_by_username, $login_by_email, $ip_address = '', $user_agent = '')
	{

      //echo $facebook_id."<br>".$login."<br>".$login_by_username."<br>".$login_by_email;die;
	 if($facebook_id != ""){
	            if (!is_null($user = $this->ci->users->get_user_by_facebook_id($facebook_id)))
				{ // login ok
				           foreach($user as $key => $value)
								{
								if (is_null($value))
									{
									$user->$key = "";
									}
								}

							$data['status'] = 200;
	                        $data["message"] = 'Login with facebook';
							
	                        $data['data']["user_id"] = $user->id;
							$data['data']['user_type'] = $user->user_type;
							$data['data']['isSocial'] = $user->isSocial;
	                        $data['data']['isVerified'] = $user->isVerified;
	                        $data['data']['isProfileCompleted'] = $user->isProfileCompleted;
	                      
				}else{
					//fail - wrong login register with facebook

						 if (!is_null($user = $this->ci->users->get_user_by_email($login)))
					     { // login ok
							 $data['status'] = 301;
                             $data["message"] = 'Email allready exist';
			             }else{
			            	$data = $this->create_app_facebook_user($login,$phone, $name,$facebook_id,$profile_pic,$gender);
			                if(!is_null($user = $this->ci->users->get_user_by_facebook_id($facebook_id))){
		                       foreach($user as $key => $value)
										{
										if (is_null($value))
											{
											$user->$key = "";
											}
										}

									$data['status'] = 200;
			                        $data["message"] = 'Register with facebook';

				                    $data['data']["user_id"] = $user->id;
									$data['data']['user_type'] = $user->user_type;
									$data['data']['isSocial'] = $user->isSocial;
			                        $data['data']['isVerified'] = $user->isVerified;
			                        $data['data']['isProfileCompleted'] = $user->isProfileCompleted;
									
			                        
			               }else{
				               	    $this->increase_login_attempt($login);
									$data['status'] = 301;
			                        $data['message'] = 'Incorrect login';
			               }
			            }

	                }
        }else{
			                    $this->increase_login_attempt($login);
								$data['status'] = 301;
		                        $data['message'] = 'Incorrect login';
	    }
	return $data;

	}
////////////////////////


	/**
	 * Create new user on the site and return some data about it:
	 * user_id, username, password, email, new_email_key (if any).
	 *
	 * @param	string
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	array
	 */

	

    function create_app_user($email, $password,$name, $email_activation)
	{
		
			//Hash password using phpass
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			$hashed_password = $hasher->HashPassword($password);

			$data = array(
				'email'	=> $email,
				'password'	=> $hashed_password,
				'name'	=> $name
			);

			if ($email_activation) {
			    $data['new_email_key'] = mt_rand(100000, 999999);//generateRandomString(6,TURE);
				$data['attempts_limit_exceed'] =  1;
			}
			if (!is_null($res = $this->ci->users->create_user($data, !$email_activation))) {
				$data['user_id'] = $res['user_id'];
				$data['password'] = $password;
               
                if (!is_null($user = $this->ci->users->get_user_by_id($data['user_id'], 0)))
				{ 

                        foreach($user as $key => $value)
							{
							if (is_null($value))
								{
								$user->$key = "";
								}
							}

                        $data1['data']["user_id"] = $user->id;
						/* $data1['data']['user_type'] = $user->user_type;
						$data1['data']['isSocial'] = $user->isSocial;
                        $data1['data']['isVerified'] = $user->isVerified;
                        $data1['data']['isProfileCompleted'] = $user->isProfileCompleted */;


				}
				
	            return $data1;
			}
		return NULL;
	}
    
    function create_app_facebook_user($email, $phone, $name, $facebook_id,$profile_pic,$gender)
	{
			$data = array(
				'email'	=> $email,
				'phone'	=> $phone,
				'name'	=> $name,
				'isSocial'=>1,
				'isVerified'=>1,
				'fb_pic'=>$profile_pic,
				'facebook_id'	=> $facebook_id,
				'gender'=>$gender
			);

			if (!is_null($res = $this->ci->users->create_facebook_user($data))) {
				$data['user_id'] = $res['user_id'];

               if (!is_null($user = $this->ci->users->get_user_by_id($data['user_id'], 1)))
				{ 

                        foreach($user as $key => $value)
							{
							if (is_null($value))
								{
								$user->$key = "";
								}
							}

                        $data1['data']["user_id"] = $user->id;
						$data1['data']['user_type'] = $user->user_type;
						$data1['data']['isSocial'] = $user->isSocial;
                        $data1['data']['isVerified'] = $user->isVerified;
                        $data1['data']['isProfileCompleted'] = $user->isProfileCompleted;


				}
	            return $data1;
			}
		return NULL;
	}
	function code_verify_app($user_id, $verification_code){
		//echo $user_id;die; 
	    if (!is_null($user = $this->ci->users->activate_user($user_id, $verification_code)))
		{ 
                 $data['status'] = 200;
	             $data['message'] = "User verification successful.";

        }else{
                $data['status'] = 301;
	            $data['message'] = "Invalid verification code.";
		}
	   return $data;
	}
	function resend_verify_code_app($user_id){
		    //$data['new_email_key'] =  generateRandomString(6,TURE);
		    $new_email_key  = mt_rand(100000, 999999);
		   
		    $data1 =$this->ci->users->update_email_key($user_id, $new_email_key);
            $data = $this->ci->users->get_user_detail_by_id($user_id);
           return $data;	
    }
    function forgot_password_app($email){
           if (!is_null($user = $this->ci->users->get_user_by_email($email))) {

              $new_key  = mt_rand(100000, 999999);
               $data = array(
					'user_id'		=> $user->id,
					'username'		=> $user->username,
					'email'			=> $user->email,
					'new_pass_key'	=> $new_key //md5(rand().microtime()),
					
				);
               
				$this->ci->users->set_password_key($user->id, $data['new_pass_key']);
				//print_r($data);die;
				return $data;

			} else {
				return false;
           }
	 }
	 function code_verify_forgot_password_app($user_id, $verification_code){
	        $user = $this->ci->users->get_user_detail_by_id($user_id);
              $result = $this->ci->users->verify_forgot_password($user->id, $verification_code);
              	
            if($result == 1){
                 $data = array( 'status' => 200, 'message' => "Your code have been verified" );  
                 $data['data']['user_id'] = $user->id;
            }else if($result == "linkexpire"){
                $data = array( 'status' => 301, 'message' => "Your verification code have been expired,Please try another." );
            }else{
            	 $data = array( 'status' => 301, 'message' => "Code not verified" );
            }
       return $data;
    }
    function reset_password_app($user_id, $new_pass){

	          	$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
				// success
                // Hash new password using phpass
				$hashed_password = $hasher->HashPassword($new_pass);

              if($this->ci->users->change_password($user_id, $hashed_password,$new_pass)){
                  return true;
              }else{
              	return null;
              }


     }
     function change_password_app($user_id,$old_pass,$new_pass){
        if (!is_null($user = $this->ci->users->get_user_detail_by_id($user_id))) 
		{
            
			// Check if old password correct
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'tank_auth'),
					$this->ci->config->item('phpass_hash_portable', 'tank_auth'));
			if ($hasher->CheckPassword($old_pass, $user->password)) {			// success

				// Hash new password using phpass
				$hashed_password = $hasher->HashPassword($new_pass);

				// Replace old password with new one
				$this->ci->users->change_password($user_id, $hashed_password,$new_pass);

				return TRUE;

			}else{	
			    	return FALSE;													
			}
		}
		return FALSE;
     }
      function add_user_location_app($user_id,$latitude,$longitude){
          if($this->ci->users->add_user_location($user_id, $latitude,$longitude)){
            return true; 
          }else{
          	return null;
          }
     }
     function add_user_availability_app($user_id,$is_available){
          if($this->ci->users->add_user_availability($user_id, $is_available)){
            return true; 
          }else{
          	return null;
          }
     }
     function search_walkers_app($latitude,$longitude){
          $walkers =$this->ci->users->search_walkers_locations($latitude,$longitude);
          if(!empty($walkers)){
          	                /*foreach($walkers as $key => $value)
							{
								
								 foreach($value as $innerKey=>$innerValue)
								{
									if (is_null($innerValue))
									{
									   $walkers[$key][$innerKey]= "";
									}
								}
							}*/
             
          	  $data = array( 'status' => 200, 'message' => "Search walkers locations successfully.");
          	  
          	  //print_r($walkers);die;
              $data['data'] = $walkers;
             
                                                                                                                                                                                                                                                            return $data; 
            }else{
               $data = array( 'status' => 200, 'message' => "Walkers not found on this location");
               $data['data'] = array();
            }
          return $data;
     }
     
     function walker_rating_details_app($user_id){
     	   $walkers =$this->ci->users->get_walker_rating_details($user_id);
     	   if(!empty($walkers)){
          	                /*foreach($walkers as $key => $value)
							{
								
								 foreach($value as $innerKey=>$innerValue)
								{
									if (is_null($innerValue))
									{
									   $walkers[$key][$innerKey]= "";
									}
								}
							}*/
              
          	  $data = array( 'status' => 200, 'message' => "Feedback details.");
              $data['data'] = $walkers;
              //echo "<pre>";print_r($data);die;

            return $data; 
          }else{
              $data = array( 'status' => 200, 'message' => "Feedback details not found");
              $data['data'] = array();
          }
          return $data;
     }
     function add_walk_request_app($owner_id,$walker_id,$start_time,$end_time,$pick_up_location,$pick_up_lat,$pick_up_long,$no_of_dogs,$instructions){
                
		         $start_time = strtotime($start_time);
		         $end_time = strtotime($end_time);
                 $created_time = strtotime(date('Y-m-d H:i:s'));

			     date_default_timezone_set("UTC");
				$start_time = date("Y-m-d H:i:s", $start_time);
				  $end_time = date("Y-m-d H:i:s", $end_time);
                $created_time = date( 'Y-m-d H:i:s', $created_time);
		
		            $walk_data =array(
					   'owner_id' => $owner_id,
					   'walker_id' => $walker_id,
					   'start_time' => $start_time,
					   'end_time' => $end_time,
					   'pick_up_location' => $pick_up_location,
					   'pick_up_lat' => $pick_up_lat,
					   'pick_up_long' => $pick_up_long,
					   'no_of_dogs' => $no_of_dogs,
					   'instructions' => $instructions,
					   'created'=>$created_time
			        );

     	  $addRequest =$this->ci->users->add_walk_request($walk_data);

     	  if(!empty($addRequest)){
             $data = array( 'status' => 200, 'message' => "Dog walks request have been sent successfully");
             $data['data'] = $addRequest;
     	  }else{
              $data = array( 'status' => 301, 'message' => "Please wait 5 minutes,You have allready sent request.");

     	  }
     	  return $data;


     }
    function RandomStringGenerator($length = 10)
	{              
	  $string = "";
	  $pattern = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for($i=0; $i<$length; $i++)
		{
			$string .= $pattern{rand(0,61)};
		}
		return $string;
	}
	function get_profile_app($user_id){
          if (!is_null( $user = $this->ci->users->get_user_detail_by_id($user_id))){
          	       foreach($user as $key => $value)
							{
							if (is_null($value))
								{
								$user->$key = "";
								}
							}

						$data["status"] = 200;
						$data['message'] = "Profile info retrieved successfully";	

                        $data['data']["user_id"] = $user->id;
						$data['data']['name'] = $user->name;
						$data['data']['phone'] = $user->phone;
                        $data['data']['dob'] = "$user->dob";
                        $data['data']['gender'] = $user->gender;
                        $data['data']['user_type'] = $user->user_type;
                        $data['data']['paypalAccount'] = $user->paypalAccount;
                        $data['data']['profile_pic'] = $user->profile_pic;
                        $data['data']['fb_pic'] = $user->fb_pic;

                        $data['data']['email'] = $user->email;
                        $data['data']['isSocial'] = $user->isSocial;
                        $data['data']['isVerified'] = $user->isVerified;
                        $data['data']['isProfileCompleted'] = $user->isProfileCompleted;
                       

                return $data;
          }
          return false;

	}
	function update_profile_app($user_id, $dob,$gender,$profile_pic,$paypalAccount,$name,$phone,$user_type){

        if (!is_null( $userProfile = $this->ci->users->update_user_profile($user_id, $dob,$gender,$profile_pic,$paypalAccount,$name,$phone,$user_type)))
		{
         //echo "Userid:".$user_id."dob:".$dob."gen:".$gender."pro:".$profile_pic."pay:".$paypalAccount."name:".$name."phone:".$phone;die;

            if (!is_null( $user = $this->ci->users->get_user_detail_by_id($user_id))){
          	       foreach($user as $key => $value)
							{
							if (is_null($value))
								{
								$user->$key = "";
								}
							}

						$data["status"] = 200;
						$data['message'] = "Profile updated successfully";	

                        $data['data']["user_id"] = $user->id;
						$data['data']['name'] = $user->name;
						$data['data']['phone'] = $user->phone;
                        $data['data']['dob'] = "$user->dob";
                        $data['data']['gender'] = $user->gender;
                        $data['data']['user_type'] = $user->user_type;
                        $data['data']['paypalAccount'] = $user->paypalAccount;

                        $data['data']['profile_pic'] = $user->profile_pic;
                        $data['data']['fb_pic'] = $user->fb_pic;

                        $data['data']['user_type'] = $user->user_type;
                        $data['data']['email'] = $user->email;
                        $data['data']['isSocial'] = $user->isSocial;
                        $data['data']['isVerified'] = $user->isVerified;
                        $data['data']['isProfileCompleted'] = $user->isProfileCompleted;

                return $data;
            }
        }
          return false;
    }

}

/* End of file Tank_auth.php */
/* Location: ./application/libraries/Tank_auth.php */