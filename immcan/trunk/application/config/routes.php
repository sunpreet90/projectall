<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "web";
$route['default_controller'] = "website";
//$route['default_controller'] = "welcome";
$route['404_override'] = '';

/*Admin Routes*/
$route['admin'] = 'welcome';
$route['dashboard'] = 'welcome';
$route['admin/home-page-settings'] = 'admin/front_page';
$route['admin/fun-facts'] = 'admin/fun_facts';

/*Frontend Routes*/
$route['consultant-booking'] = 'website/tabs';
$route['about-us'] = 'website/aboutus';
$route['about-you'] = 'website/aboutyou';

$route['contact-us'] = 'contact/contact';
$route['contact/(:num)'] = 'contact/contact/index/$1';

$route['privacy-policy'] = 'pages/terms_and_conditions';
$route['terms-and-conditions'] = 'pages/terms_and_conditions';

$route['page/edit/(:any)/(:num)'] = 'pages/edit_page_by_slug_and_id/$1/$2';
$route['page/trash/(:any)/(:num)'] = 'pages/trash_page_by_slug_and_id/$1/$2';

$route['page/status/(:any)'] = 'pages/page_by_status/$1';

$route['page/(:any)/(:num)'] = 'pages/page_by_slug_and_id/$1/$2';

$route['get-started'] = 'website/getstarted';

$route['faq/(:num)'] = 'website/faq/$1';
$route['service/(:num)'] = 'website/service/$1';

$route['calendly'] = 'website/calendly';

$route['booking'] = 'website/booking';
/*$route['about'] = 'web/about';
$route['contact'] = 'web/contact';*/



/* End of file routes.php */
/* Location: ./application/config/routes.php */