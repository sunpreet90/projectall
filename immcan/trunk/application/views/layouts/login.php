<!DOCTYPE html>
<html lang="en" class="bg-dark">
	<head>
		<meta charset="utf-8" />
		<link rel="shortcut icon" href="<?=base_url()?>resource/images/favicon.ico">

    	<title><?php  echo $template['title'];?></title>
		<meta name="description" content="<?=$this->config->item('site_desc')?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<link rel="stylesheet" href="<?=base_url()?>resource/css/app.v2.css" type="text/css" />
		<link rel="stylesheet" href="<?=base_url()?>resource/css/font.css" type="text/css" cache="false" />
		<link rel="stylesheet" href="<?=base_url()?>assets/js/toastr/toastr.css" type="text/css" cache="false" />
		<!--[if lt IE 9]>
		<script src="js/ie/html5shiv.js" cache="false">
		</script>
		<script src="js/ie/respond.min.js" cache="false">
		</script>
		<script src="js/ie/excanvas.js" cache="false">
		</script> <![endif]-->
	</head>
	<body> 
	
	<!--main content start-->
      <?php  echo $template['body'];?>
      <!--main content end-->


	<!-- footer --> <footer id="footer">
	<div class="text-center padder">
		<p> <small>All rights reserved <?=$this->config->item('customer_name')?>
		<br>&copy; <?=date('Y')?> Developed with all the love in the World by <a href="<?=$this->config->item('company')?>" target="_blank"><?=$this->config->item('customer_name')?></a> </small> </p>
	</div> </footer>
	<!-- / footer -->
	<script src="<?=base_url()?>resource/js/app.v2.js">
	</script>
	<!-- Bootstrap -->
	<!-- App -->
<script type="text/javascript" src="<?=base_url()?>assets/js/toastr/toastr.js"></script>
<?php
if($this->session->flashdata('message')){ 
$message = $this->session->flashdata('message');
$alert = $this->session->flashdata('response_status');
?>
<script type="text/javascript">

$(document).ready(function(){

toastr.<?=$alert?>("<?=$message?>", "Response Status");
toastr.options = {
 "closeButton": true,
 "debug": false,
 "positionClass": "toast-bottom-right",
 "onclick": null,
 "showDuration": "300",
 "hideDuration": "1000",
 "timeOut": "5000",
 "extendedTimeOut": "1000",
 "showEasing": "swing",
 "hideEasing": "linear",
 "showMethod": "fadeIn",
 "hideMethod": "fadeOut"
}
});
</script>
<?php } ?>

</body>
</html>
