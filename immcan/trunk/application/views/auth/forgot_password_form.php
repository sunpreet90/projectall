<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
    'class' => 'form-control',
    'placeholder' => 'User Id or Email',
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = 'Email or login';
} else {
	$login_label = 'Email';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Forgot Password</title>

    <link href="<?=base_url('assets') ?>/css/style.css" rel="stylesheet">
    <link href="<?=base_url('assets') ?>/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?=base_url('assets') ?>/js/html5shiv.js"></script>
    <script src="<?=base_url('assets') ?>/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <?php 
        $form_attributes = array( 'class' => 'form-signin' );
        echo form_open( $this->uri->uri_string(), $form_attributes ); 
    ?>
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Forgot Password</h1>
            <!-- DoggyStroll -->
            <!-- <img src="<?=base_url('assets') ?>/images/login-logo.png" alt=""/> -->
        </div>
        <div class="login-wrap">

            <?php echo form_input($login); ?>
            <?php echo form_error( $login['name'] ); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?>

            <button class="btn btn-lg btn-login btn-block" type="submit">
                <i class="fa fa-check"></i>
            </button>

            <div class="registration">
                Login 
                <a class="" href="<?=base_url()?>auth/login">
                    Here
                </a>
            </div>
            <!-- <div class="registration">
                Not a member yet?
                <a class="" href="<?=base_url()?>auth/register">
                    Signup
                </a>
            </div> -->

        </div>
    <?php echo form_close(); ?>

</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?=base_url('assets') ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url('assets') ?>/js/bootstrap.min.js"></script>
<script src="<?=base_url('assets') ?>/js/modernizr.min.js"></script>

</body>
</html>
