<?php
$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'class' => 'form-control input-lg'
);
$confirm_new_password = array(
	'name'	=> 'confirm_new_password',
	'id'	=> 'confirm_new_password',
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size' 	=> 30,
	'class' => 'form-control input-lg'
);
?>
<?php  //echo modules::run('sidebar/flash_msg');?>  

<?php //die("AAA"); ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>Forgot Password</title>

    <link href="<?=base_url('assets') ?>/css/style.css" rel="stylesheet">
    <link href="<?=base_url('assets') ?>/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?=base_url('assets') ?>/js/html5shiv.js"></script>
    <script src="<?=base_url('assets') ?>/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
        <a class="navbar-brand block" href="<?=base_url()?>"><?=$this->config->item('customer_name')?></a> 
		 <section class="panel panel-default bg-white m-t-lg">
		 	<h1 class="sign-title"> <?php echo $this->config->item('website_name', 'tank_auth'); ?></h1>
		<div class="panel-heading text-center"> <strong>Change Password <?=$this->config->item('customer_name')?></strong> </div>

		<?php 
		$attributes = array('class' => 'panel-body wrapper-lg');
		echo form_open($this->uri->uri_string(),$attributes); ?>

			<div class="form-group">
				<label class="control-label"><?php echo "New Password";//=lang('new_password')?></label>
				<?php echo form_password($new_password); ?>
				<span style="color: red;">
				<?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></span>
			</div>
			<div class="form-group">
				<label class="control-label"><?php echo "Confirm Password"; //=lang('confirm_password')?> </label>
				<?php echo form_password($confirm_new_password); ?>
				<span style="color: red;"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?></span>
			</div>


			<button name="reset" type="submit" class="btn btn-primary">Change Password</button>
			<div class="line line-dashed">
			</div> 
			<!--<?php if ($this->config->item('allow_registration', 'tank_auth')){ ?>
			<p class="text-muted text-center"><small>Do not have an account?</small></p> 
			<?php } ?>
			<a href="<?=base_url()?>auth/register/" class="btn btn-success btn-block">Get Your Account</a>-->
<?php echo form_close(); ?>

 </section>




	</div> 
	</div> 
	</div> 
	





<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?=base_url('assets') ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?=base_url('assets') ?>/js/bootstrap.min.js"></script>
<script src="<?=base_url('assets') ?>/js/modernizr.min.js"></script>

</body>
</html>
<!--
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">

		<div class="container aside-xxl">
		 <a class="navbar-brand block" href="<?=base_url()?>"><?=$this->config->item('customer_name')?></a> 
		 <section class="panel panel-default bg-white m-t-lg">
		<header class="panel-heading text-center"> <strong>Change Password <?=$this->config->item('customer_name')?></strong> </header>

		<?php 
		$attributes = array('class' => 'panel-body wrapper-lg');
		echo form_open($this->uri->uri_string(),$attributes); ?>
			<div class="form-group">
				<label class="control-label"><?php echo "New Password";//=lang('new_password')?></label>
				<?php echo form_password($new_password); ?>
				<span style="color: red;">
				<?php echo form_error($new_password['name']); ?><?php echo isset($errors[$new_password['name']])?$errors[$new_password['name']]:''; ?></span>
			</div>
			<div class="form-group">
				<label class="control-label"><?php echo "Confirm Password"; //=lang('confirm_password')?> </label>
				<?php echo form_password($confirm_new_password); ?>
				<span style="color: red;"><?php echo form_error($confirm_new_password['name']); ?><?php echo isset($errors[$confirm_new_password['name']])?$errors[$confirm_new_password['name']]:''; ?></span>
			</div>


			<button type="submit" class="btn btn-primary">Change Password</button>
			<div class="line line-dashed">
			</div> 
			<?php if ($this->config->item('allow_registration', 'tank_auth')){ ?>
			<p class="text-muted text-center"><small>Do not have an account?</small></p> 
			<?php } ?>
			<a href="<?=base_url()?>auth/register/" class="btn btn-success btn-block">Get Your Account</a>
<?php echo form_close(); ?>

 </section>
	</div> 
	</section>-->