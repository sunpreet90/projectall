<?php
function pr($data, $die='')
{
  if ($die == true) 
  {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    die();
  }else{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
  }
}

function get_referral_notification() {

    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();

    //select the required fields from the database
    $ci->db->select('id');

    //tell the db class the criteria
    $ci->db->where( array('status' => 1 ) );

    //supply the table name and get the data
    $result = $ci->db->get('ws_client_referral')->result();

    //echo "<pre>"; print_r($result); echo "<pre>";

    if (!empty($result)) {
        # code...

        return count($result);
        //get the full name by concatinating the first and last names
        
    } else {
        
        return 0;
    }
}

function get_page_metadata( $page_id, $key ) {

    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();

    //select the required fields from the database
    $ci->db->select('meta_value');

    //tell the db class the criteria
    $ci->db->where( array('post_id' => $page_id, 'meta_key' => $key ) );

    //supply the table name and get the data
    $row = $ci->db->get('ws_postmeta')->row();

    //echo "<pre>"; print_r($row); echo "<pre>";

    if (!empty($row)) {
        # code...

        //get the full name by concatinating the first and last names
        return $meta_value = $row->meta_value;
    } else {
        return false;
    }
}

function get_post_metadata( $post_id, $meta_key ) {

    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();

    //select the required fields from the database
    $ci->db->select('meta_value');

    //tell the db class the criteria
    $ci->db->where( array('post_id' => $post_id, 'meta_key' => $meta_key ) );

    //supply the table name and get the data
    $row = $ci->db->get('ws_postmeta')->row();

    if (!empty($row)) {

        //get the full name by concatinating the first and last names
        $meta_value = $row->meta_value;

        // return the full name;
        return $meta_value;
    
    } else { return false; }
}

function add_post_meta( $post_id, $meta_key, $meta_value ) {

    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();


    // If exist select the required fields from the database
    $ci->db->select('meta_value');

    //tell the db class the criteria
    $ci->db->where( array('post_id' => $post_id, 'meta_key' => $meta_key ) );

    //supply the table name and get the data
    $row = $ci->db->get('ws_postmeta')->row();

    if ( !empty($row) ) {
        # code...

        //select the required fields from the database
        $ci->db->set('meta_value', $meta_value);

        //tell the db class the criteria
        $ci->db->where( array('meta_key' => $meta_key, 'post_id' => $post_id ) );

        //supply the table name and get the data
        $ci->db->update('ws_postmeta');

        // return the full name;
        return true;
    }
    else
    {
        $insert_array = array('post_id' => $post_id, 'meta_key' => $meta_key, 'meta_value' => $meta_value);

        $ci->db->insert('ws_postmeta', $insert_array);

        // return the full name;
        return true;
    }
}

function get_config_item($fix_key ) {

    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();

    //select the required fields from the database
    $ci->db->select('value');

    //tell the db class the criteria
    $ci->db->where( array('fix_key' => $fix_key ) );

    //supply the table name and get the data
    $row = $ci->db->get('config')->row();

    if (!empty($row)) {
        # code...

        //get the full name by concatinating the first and last names
        $value = $row->value;
    } else {
        $value = '';
    }

    // return the full name;
    return $value;
}

function set_config_item($fix_key, $update_value ) {

    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();

    //select the required fields from the database
    $ci->db->set('value', $update_value);

    //tell the db class the criteria
    $ci->db->where( array('fix_key' => $fix_key ) );

    //supply the table name and get the data
    $ci->db->update('config');

    // return the full name;
    return true;
}


function get_messages() {

    //the database functions can not be called from within the helper
    //so we have to explicitly load the functions we need in to an object
    //that I will call ci. then we use that to access the regular stuff.
    $ci=& get_instance();
    $ci->load->database();

    //select the required fields from the database
    $ci->db->select('*');

    //tell the db class the criteria
    $ci->db->where('status', '1');
    $ci->db->limit(5);
    //supply the table name and get the data
    $row = $ci->db->get('ws_contactus');

    //get the full data
    $data['messages'] = $row->result_array();
    $data['msgCount'] = count($data['messages']);
    //print_r($data);
    // return the data;
    return $data;
}