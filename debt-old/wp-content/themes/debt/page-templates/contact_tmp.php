<?php 
// Template Name: Contact Page Template
get_header();
?>

<div class="breadcrumbs-container">
	<div class="container-fluid">
		<div id="breadcrumbs"><span><span><a href="https://www.debt.com/"></a> <span class="breadcrumb_last">Contact Us</span></span></span></div> 
 </div>
</div>
<div id="content" class="site-content">
	<div class="contact container">
		<div class="row">
			<div class="col-lg-1"></div>
			<div class="title col-lg-5 col-xl-5">
				<h1>Contact</h1>
				<div class="contact-content">
					<p>See something on our site that doesn’t work?  Want to explore business opportunities?  Fill out the form below and we’ll respond promptly.  Got a question about a news article? Want to tell us how much you liked it?  How much you hated it? Got a story idea? Want to write for us?  <!-- Email <a href="#" target="_self">editor@demo.com</a> -->.</p>
				</div>
				<div class="social-icons">
					<ul>
						<li> 
							<a class="sm-icon fb" href="#" target="_blank" property="sameAs"> <span class="icon fb"><i aria-hidden="true" class="fab fa-facebook" title="Facebook Icon"></i></span> <span class="sr-only">Facebook Icon linking to Facebook Page</span> </a>
						</li>
						<li> 
							<a class="sm-icon twitter" href="#" target="_blank" property="sameAs"> <span class="icon twitter"><i aria-hidden="true" class="fab fa-twitter" title="Twitter Icon"></i></span> <span class="sr-only">Twitter Icon linking to Twitter Page</span> </a>
						</li>
						<li> 
							<a class="sm-icon youtube" href="https://www.youtube.com/c/DebtDotCom" target="_blank" property="sameAs"> <span class="icon youtube"><i aria-hidden="true" class="fab fa-youtube" title="Youtube Icon"></i></span> <span class="sr-only">Youtube Icon linking to  Youtube Page</span> </a>
						</li>
						<li> 
							<a class="sm-icon google" href="#" target="_blank" property="sameAs"> <span class="icon google"><i aria-hidden="true" class="fab fa-google-plus" title="Google Plus Icon"></i></span> <span class="sr-only">Google Plus Icon linking to Google Plus Page</span> </a>
						</li>
						<li> 
							<a class="sm-icon flipboard" href="#" target="_blank" property="sameAs"> <span class="icon flipboard"><img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/icons/flipboard-icon-white.jpg" alt="Flipboard Icon"></span> <span class="sr-only">Flipboard Icon linking to Flipboard Page</span> </a>
						</li>
						<li>
						 <a href="#" target="_blank" property="sameAs" class="sm-icon in"> <span class="icon in"><i aria-hidden="true" title="Instagram Icon" class="fab fa-instagram"></i></span> <span class="sr-only">Instagram Icon linking to Instagram Page</span> </a>
						</li>
						<li>
						 <a class="sm-icon pinterest" href="#" target="_blank" property="sameAs"> <span class="icon pinterest"><i aria-hidden="true" class="fab fa-pinterest" title="Pinterest Icon"></i></span> <span class="sr-only">Pinterest Icon linking to Pinterest Page</span> </a>
						</li>
						<li> 
							<a class="sm-icon rss" href="/feed/" target="_blank" property="sameAs"> <span class="icon rss"><i aria-hidden="true" class="fas fa-rss" title="RSS Icon"></i></span> <span class="sr-only">RSS Icon linking to RSS Feed</span> </a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-5 col-xl-5">
				<div id="contact-form" class="contact-form" data-form="main-div"> 
					<span class="success-message" style="display: none;">Thank you for contacting. We'll get back to you as soon as possible!</span>
					<form id="contact-vtform1" class="dc_forms" novalidate="novalidate">
						<div class="form-row">
							<div class="col">
								<div class="form-group"> 
									<label for="first_name">First Name</label> 
									<input class="form-control" name="first_name" data-var-name="first_name" id="first_name" required="" minlength="2" data-ng-pattern="/^[A-Za-z]+$/" maxlength="50" aria-required="true" type="text">
								</div>
							</div>
							<div class="col">
								<div class="form-group"> 
									<label for="last_name">Last Name</label>
									 <input class="form-control" name="last_name" data-var-name="last_name" id="last_name" required="" minlength="2" aria-required="true" type="text">
									</div>
								</div>
							</div>
							<div class="form-group">
							 <label for="email">Email Address</label>
							  <input class="form-control" data-var-name="email" id="email" placeholder="email@example.com" required="" aria-required="true" type="email"> 
							  <input name="send_email" data-var-name="send_email" value="qa@debt.com" type="hidden">
							   <input name="subject" data-var-name="subject" value="Contact form submission" type="hidden">
							</div>
							<div class="form-group"> 
								<label for="message">Message</label>
								<textarea class="form-control" data-var-name="message" id="message" name="message" required="" rows="3" aria-required="true"></textarea>
							</div>
								<div class="form-check"> <label class="form-check-label"> <input class="form-check-input" type="checkbox"> I request to be contacted at the email or by any other information provided in the comment box. </label>
								</div>
								 <button id="contact-button1" class="btn btn-primary">Submit<span class="ng-hide" style="display: none;">&nbsp;<img src="https://www.debt.com/ang/images/ajax-loader.gif" alt="ajax loader">
								 </span>
								 </button>
								 <input name="xxTrustedFormToken" id="xxTrustedFormToken_1" value="https://cert.trustedform.com/d54cf6348ffb8fd92eefb651a1700347c0ae39cd" type="hidden">
								 <input name="xxTrustedFormCertUrl" id="xxTrustedFormCertUrl_1" value="https://cert.trustedform.com/d54cf6348ffb8fd92eefb651a1700347c0ae39cd" type="hidden">
								</form>
							</div>
						</div>
						<div class="col-lg-1"></div>
					</div>
				</div>
				  </div>

<div class="footer-disclaimer container-fluid">
	<p>This website is intended for informational purposes and as a reference tool to match consumers with companies that may be able to assist them. <a href="#">View our Advertising Disclosures here</a>.</p>
</div>

<?php get_footer(); ?>