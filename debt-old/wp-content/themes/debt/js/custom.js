jQuery(document).ready(function() {
    jQuery.fn.ForceNumericOnly = function() {
        return this.each(function() {
            jQuery(this).keydown(function(e) {
                var t = e.charCode || e.keyCode || 0;
                return e.shiftKey ? !1 : 8 == t || 9 == t || 13 == t || 46 == t || 110 == t || 190 == t || t >= 35 && 40 >= t || t >= 48 && 57 >= t || t >= 96 && 105 >= t
            })
        })
    }, jQuery.fn.ForceNumbersOnly = function() {
        return this.each(function() {
            jQuery(this).keydown(function(e) {
                var t = e.charCode || e.keyCode || 0;
                return e.shiftKey ? !1 : 8 == t || 9 == t || 13 == t || 46 == t || t >= 35 && 40 >= t || t >= 48 && 57 >= t || t >= 96 && 105 >= t
            })
        })
    }, jQuery.validator.addMethod("currencywithcommas", function(e, t) {
        var r = /^(\d+|\d{1,3}(,\d{3})*)+(\.\d{2})?$/;
        return this.optional(t) || e.match(r)
    }, "Must be in US currency format 9.99"), jQuery.validator.addMethod("currencywithcommasSP", function(e, t) {
        var r = /^(\d+|\d{1,3}(,\d{3})*)+(\.\d{2})?$/;
        return this.optional(t) || e.match(r)
    }, "Debe estar en formato de moneda 9.99"), jQuery.validator.addMethod("NotEnough", function(e, t) {
        var r = getNumber($("#home_balance3").val()),
            a = getNumber($("#home_apr3").val()),
            n = a / 100 / 12,
            u = r * n;
        return e > u
    }, "Minimum payment must be greater than interest against balance"), jQuery.validator.addMethod("NotEnoughcc", function(e, t) {
        var r = getNumber($("#cc_balance1").val()),
            a = getNumber($("#cc_apr1").val()),
            n = a / 100 / 12,
            u = r * n;
        return e > u
    }, "Minimum payment must be greater than interest against balance"), jQuery.validator.addMethod("NotEnoughcc2", function(e, t) {
        var r = getNumber($("#cc_balance2").val()),
            a = getNumber($("#cc_apr2").val()),
            n = a / 100 / 12,
            u = r * n;
        return e > u
    }, "Minimum payment must be greater than interest against balance")
});
var version = "1.0";
var currentScriptName = 'EmailForm.js';
var div_select = null;
var emailformToSend = {};

function show_ThankYou(url) {
    jQuery("#" + div_select + " form").remove();
    jQuery("#" + div_select + " .success-message").css("display", "block");
}

function get_Form_Values(divName) {
    jQuery("#" + divName + " *").each(function() {
        if (jQuery(this).attr("data-var-name") != "undefined") {
            if (jQuery(this).is("input")) {
                if ((jQuery(this).attr("type") == "text" || jQuery(this).attr("type") == "email" || jQuery(this).attr("type") == "tel" || jQuery(this).attr("type") == "number") && jQuery(this).val() != "" && jQuery(this).val() != null) {
                    formToSend[jQuery(this).attr("data-var-name")] = jQuery(this).val().replace(/ /g, "+");
                } else if ((jQuery(this).attr("type") == "hidden") && jQuery(this).val() != "" && jQuery(this).val() != null) {
                    if (jQuery(this).attr("data-var-name") == "pid") {
                        formToSend["ckm_subid"] = jQuery(this).val().replace(/ /g, "+");
                        formToSend["pid"] = jQuery(this).val().replace(/ /g, "+");
                    } else if (jQuery(this).attr("data-var-name") == "send_email" || jQuery(this).attr("data-var-name") == "subject") {
                        emailformToSend[jQuery(this).attr("data-var-name")] = jQuery(this).val().replace(/ /g, "+");
                    } else {
                        formToSend[jQuery(this).attr("name")] = jQuery(this).val().replace(/ /g, "+");
                    }
                }
                if (jQuery(this).attr("type") == "checkbox" && jQuery(this).is(":checked")) {
                    formToSend[jQuery(this).attr("data-var-name")] = 1;
                }
                if (jQuery(this).attr("type") == "file" && jQuery(this).val() != "" && jQuery(this).val() != null) {
                    var ACCESS_TOKEN = 'l39ZSs1QS5AAAAAAAAAAiKs8sWkoKh04xTG8nfLdgxpScQo8XjnsCX3Rs5vJamyF';
                    var dbx = new Dropbox.Dropbox({
                        accessToken: ACCESS_TOKEN
                    });
                    var fileInput = document.getElementById('file-upload');
                    $.each(fileInput.files, function(index, file) {
                        dbx.filesUpload({
                            path: '/' + formToSend["first_name"] + '-' + formToSend["last_name"] + '-' + file.name,
                            contents: file
                        }).then(function(response) {
                            console.log(response);
                        }).catch(function(error) {
                            console.error(error);
                        });
                    });
                }
            } else if (jQuery(this).is("textarea")) {
                if (jQuery(this).val() != "" && jQuery(this).val() != null) {
                    formToSend[jQuery(this).attr("data-var-name")] = jQuery(this).val().replace(/ /g, "+");
                }
            } else if (jQuery(this).is("select")) {
                var var_name = jQuery(this).attr("data-var-name")
                if (jQuery(this).children().is(":selected")) {
                    var value = jQuery(this).val();
                    if (value != null && value != "")
                        formToSend[var_name] = value.replace(/ /g, "+");
                }
            }
        }
    });
    if ((divName == 'newsletter-signup') || (divName == 'sidebar-newsletter-signup')) {
        getClientIpAddress();
        formToSend["ip_address"] = _ip_address;
        formToSend["apikey"] = _apikey;
        formToSend["source"] = _pagesource.replace(/ /g, "+");
        formToSend["opt_in"] = _opt_in;
        if (ckm_subid_2 != null && ckm_subid_2 != '')
            formToSend["ckm_subid_2"] = ckm_subid_2;
        if (ckm_subid_3 != null && ckm_subid_3 != '')
            formToSend["ckm_subid_3"] = ckm_subid_3;
        if (ckm_subid_4 != null && ckm_subid_4 != '')
            formToSend["ckm_subid_4"] = ckm_subid_4;
        if (ckm_subid_5 != null && ckm_subid_5 != '')
            formToSend["ckm_subid_5"] = ckm_subid_5;
    }
}

function submit_Form_To_email(btnName) {
    jQuery("#" + btnName + " span").show();
    var resultUrl = _thankYouURL;
    var jsonString = JSON.stringify(formToSend);
    jQuery.post(MyAjax.ajaxurl, {
        'action': 'send_message',
        'data': jsonString,
        'name': formToSend["first_name"],
        'email': formToSend["email"],
        'send_email': emailformToSend["send_email"],
        'subject': emailformToSend["subject"]
    }, function(response) {});
    show_ThankYou(resultUrl);
}

function submit_Form_To_Marketing(btnName) {
    jQuery("#" + btnName + " span").show();
    var resultUrl = _thankYouURL_Unsold;
    var form = formToSend;
    var data = {};
    var _data = "";
    for (var prop in form) {
        if (form.hasOwnProperty(prop)) {
            data[prop] = form[prop];
            _data = _data + "&" + prop + "=" + form[prop];
        }
    }
    if (_data != "")
        _data = "?" + _data.substring(1, _data.length);
    var _url = "https://leadapi.debt.com/createlead.aspx";
    jQuery.ajax({
        url: _url + _data,
        type: "GET",
        jsonp: 'callback',
        jsonpCallback: "jsonpcallback",
        dataType: "jsonp",
        contentType: "application/json; charset=utf-8",
        success: function(json) {
            var data = JSON.parse(json);
            if (data.Status === "Success") {
                var success = JSON.parse(data.Cake);
                var buyerId = success.BuyerId;
                var buyerContractId = success.BuyerContractId;
                var mapi_lead_id = data.LeadId;
                var cake_lead_id = success.CakeLeadId;
                resultUrl = _thankYouURL;
                if (typeof PassBuyerInfo != 'undefined') {
                    if (PassBuyerInfo) {
                        if (resultUrl.indexOf('?') == -1) {
                            resultUrl = resultUrl + "?BuyerId=" + buyerId + "&BuyerContractId=" + buyerContractId + "&LeadId=" + mapi_lead_id + "&CakeId=" + cake_lead_id;
                        } else {
                            resultUrl = resultUrl + "&BuyerId=" + buyerId + "&BuyerContractId=" + buyerContractId + "&LeadId=" + mapi_lead_id + "&CakeId=" + cake_lead_id;
                        }
                    }
                }
            }
            show_ThankYou(resultUrl);
        },
        error: function(xhr, status, error) {
            show_ThankYou(resultUrl);
            console.warn("Error saving the data:");
            console.warn(error);
        }
    });
}
jQuery(document).ready(function($) {
    if (typeof _getLangCode != 'undefined' && _getLangCode != "es") {
        primary_phone_text_error = "Your 10 digit phone number is required";
        first_name_text_error = "Your first name is required";
        last_name_text_error = "Your last name is required";
        email_text_error = "Your email address is required";
        email_text_error_missing = "Please use this email format: name@domain.com";
        minlength_error = "Please enter at least 2 characters";
    } else {
        primary_phone_text_error = "Su nÃºmero de telÃ©fono debe tener 10 dÃ­gitos";
        first_name_text_error = "Su nombre es obligatorio";
        last_name_text_error = "Su apellido es obligatorio";
        email_text_error = "Su correo electrÃ³nico es obligatorio";
        email_text_error_missing = "Por favor use este formato de correo electrÃ³nico: nombre@dominio.com";
        minlength_error = "Por favor, no escribas menos de 2 caracteres.";
    }
    $(".dc_forms").each(function() {
        $(this).validate({
            rules: {
                primary_phone: {
                    minlength: 14
                },
                terms: {
                    required: true
                },
                message: {
                    required: true
                },
                question: {
                    required: true
                }
            },
            messages: {
                primary_phone: primary_phone_text_error
            },
            errorClass: 'error',
            errorPlacement: function(error, element) {
                if (element.parent('.form-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element.parent());
                }
                if (element.attr('name') == 'number[]') {
                    error.insertAfter('#checkboxGroup');
                } else {
                    error.appendTo(element.parent());
                }
            },
            submitHandler: function(form) {}
        });
    });
    $(".success-message").hide();
    $(".ng-hide").hide();
    var buttonpressed;
    var buttonelement;
    $('.dc_forms .btn-primary').click(function() {
        buttonpressed = $(this).attr('id');
        buttonelement = $(this);
    })
    $('.dc_forms').submit(function(event) {
        if ($(this).valid() === true) {
            div_select = buttonelement.closest("div[data-form='main-div']").attr("id");
            var btn_select = buttonpressed;
            get_Form_Values(div_select);
            if ((buttonpressed == 'email-button') || (buttonpressed == 'sidebar-email-button')) {
                submit_Form_To_Marketing(btn_select);
                return false;
            } else if ((buttonpressed == 'contact-button') || (buttonpressed == 'partner-button') || (buttonpressed == 'ate-button') || (buttonpressed == 'scholarships-button')) {
                submit_Form_To_email(btn_select);
                return false;
            }
        }
    });
    VariableManagement();
    if (typeof defaultVid != 'undefined' && defaultVid != "") {
        $("#vt-form-debt-type").val(defaultVid);
        $("#vt-form-debt-type").trigger("change");
    }
});
jQuery(function($) {
    $('.misha_loadmore').click(function() {
        var button = $(this),
            data = {
                'action': 'loadmore',
                'query': misha_loadmore_params.posts,
                'page': misha_loadmore_params.current_page,
                'lang': _getLangCode
            };
        $.ajax({
            url: misha_loadmore_params.ajaxurl,
            data: data,
            type: 'POST',
            beforeSend: function(xhr) {
                if (_getLangCode != 'es') {
                    button.text('Loading...');
                } else {
                    button.text('Cargando...');
                }
            },
            success: function(data) {
                if (data) {
                    $('.story-block-small').find('article:last-of-type').after(data);
                    if (_getLangCode != 'es') {
                        button.text('More posts')
                    } else {
                        button.text('MÃ¡s publicaciones')
                    }
                    misha_loadmore_params.current_page++;
                    if (misha_loadmore_params.current_page == misha_loadmore_params.max_page)
                        button.remove();
                } else {
                    button.remove();
                }
            }
        });
    });
});
jQuery(function($) {
    $('.ate_cat').click(function() {
        var catId = $(this).attr('id');
        var catName = $(this).html();
        $('.sub-cat-bar-ate .menu li').removeClass('cool-style');
        $(this).addClass('cool-style');
        var button = $(this),
            data = {
                'action': 'ateload',
                'query': ateload_params.posts,
                'cat_id': catId,
                'page': -1,
                'lang': _getLangCode
            };
        $.ajax({
            url: ateload_params.ajaxurl,
            data: data,
            type: 'POST',
            beforeSend: function(xhr) {
                if (_getLangCode != 'es') {
                    button.html('<a>Loading...</a>');
                } else {
                    button.html('<a>Cargando...</a>');
                }
            },
            success: function(data) {
                if (data) {
                    $('.ate-block').empty();
                    $('.ate-block').append(data);
                    if (_getLangCode != 'es') {
                        button.html(catName)
                    }
                    ateload_params.current_page++;
                    if (ateload_params.current_page == ateload_params.max_page)
                        button.remove();
                } else {
                    button.remove();
                }
            }
        });
    });
});
jQuery(function($) {
    $('.ate_loadmore').click(function() {
        var button = $(this),
            data = {
                'action': 'ateloadmore',
                'query': ateloadmore_params.posts,
                'page': ateloadmore_params.current_page,
                'lang': _getLangCode
            };
        $.ajax({
            url: ateloadmore_params.ajaxurl,
            data: data,
            type: 'POST',
            beforeSend: function(xhr) {
                if (_getLangCode != 'es') {
                    button.text('Loading...');
                } else {
                    button.text('Cargando...');
                }
            },
            success: function(data) {
                if (data) {
                    $('.ate-block').find('article:last-of-type').after(data);
                    if (_getLangCode != 'es') {
                        button.text('More posts')
                    } else {
                        button.text('MÃ¡s publicaciones')
                    }
                    ateloadmore_params.current_page++;
                    if (ateloadmore_params.current_page == ateloadmore_params.max_page)
                        button.remove();
                } else {
                    button.remove();
                }
            }
        });
    });
});
(function(e, t) {
    'object' == typeof exports && 'undefined' != typeof module ? module.exports = t() : 'function' == typeof define && define.amd ? define(t) : e.Popper = t()
})(this, function() {
    'use strict';

    function e(e) {
        return e && '[object Function]' === {}.toString.call(e)
    }

    function t(e, t) {
        if (1 !== e.nodeType) return [];
        var o = window.getComputedStyle(e, null);
        return t ? o[t] : o
    }

    function o(e) {
        return 'HTML' === e.nodeName ? e : e.parentNode || e.host
    }

    function n(e) {
        if (!e || -1 !== ['HTML', 'BODY', '#document'].indexOf(e.nodeName)) return window.document.body;
        var i = t(e),
            r = i.overflow,
            p = i.overflowX,
            s = i.overflowY;
        return /(auto|scroll)/.test(r + s + p) ? e : n(o(e))
    }

    function r(e) {
        var o = e && e.offsetParent,
            i = o && o.nodeName;
        return i && 'BODY' !== i && 'HTML' !== i ? -1 !== ['TD', 'TABLE'].indexOf(o.nodeName) && 'static' === t(o, 'position') ? r(o) : o : window.document.documentElement
    }

    function p(e) {
        var t = e.nodeName;
        return 'BODY' !== t && ('HTML' === t || r(e.firstElementChild) === e)
    }

    function s(e) {
        return null === e.parentNode ? e : s(e.parentNode)
    }

    function d(e, t) {
        if (!e || !e.nodeType || !t || !t.nodeType) return window.document.documentElement;
        var o = e.compareDocumentPosition(t) & Node.DOCUMENT_POSITION_FOLLOWING,
            i = o ? e : t,
            n = o ? t : e,
            a = document.createRange();
        a.setStart(i, 0), a.setEnd(n, 0);
        var f = a.commonAncestorContainer;
        if (e !== f && t !== f || i.contains(n)) return p(f) ? f : r(f);
        var l = s(e);
        return l.host ? d(l.host, t) : d(e, s(t).host)
    }

    function a(e) {
        var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 'top',
            o = 'top' === t ? 'scrollTop' : 'scrollLeft',
            i = e.nodeName;
        if ('BODY' === i || 'HTML' === i) {
            var n = window.document.documentElement,
                r = window.document.scrollingElement || n;
            return r[o]
        }
        return e[o]
    }

    function f(e, t) {
        var o = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
            i = a(t, 'top'),
            n = a(t, 'left'),
            r = o ? -1 : 1;
        return e.top += i * r, e.bottom += i * r, e.left += n * r, e.right += n * r, e
    }

    function l(e, t) {
        var o = 'x' === t ? 'Left' : 'Top',
            i = 'Left' == o ? 'Right' : 'Bottom';
        return +e['border' + o + 'Width'].split('px')[0] + +e['border' + i + 'Width'].split('px')[0]
    }

    function m(e, t, o, i) {
        return _(t['offset' + e], o['client' + e], o['offset' + e], ie() ? o['offset' + e] + i['margin' + ('Height' === e ? 'Top' : 'Left')] + i['margin' + ('Height' === e ? 'Bottom' : 'Right')] : 0)
    }

    function h() {
        var e = window.document.body,
            t = window.document.documentElement,
            o = ie() && window.getComputedStyle(t);
        return {
            height: m('Height', e, t, o),
            width: m('Width', e, t, o)
        }
    }

    function c(e) {
        return se({}, e, {
            right: e.left + e.width,
            bottom: e.top + e.height
        })
    }

    function g(e) {
        var o = {};
        if (ie()) try {
            o = e.getBoundingClientRect();
            var i = a(e, 'top'),
                n = a(e, 'left');
            o.top += i, o.left += n, o.bottom += i, o.right += n
        } catch (e) {} else o = e.getBoundingClientRect();
        var r = {
                left: o.left,
                top: o.top,
                width: o.right - o.left,
                height: o.bottom - o.top
            },
            p = 'HTML' === e.nodeName ? h() : {},
            s = p.width || e.clientWidth || r.right - r.left,
            d = p.height || e.clientHeight || r.bottom - r.top,
            f = e.offsetWidth - s,
            m = e.offsetHeight - d;
        if (f || m) {
            var g = t(e);
            f -= l(g, 'x'), m -= l(g, 'y'), r.width -= f, r.height -= m
        }
        return c(r)
    }

    function u(e, o) {
        var i = ie(),
            r = 'HTML' === o.nodeName,
            p = g(e),
            s = g(o),
            d = n(e),
            a = t(o),
            l = +a.borderTopWidth.split('px')[0],
            m = +a.borderLeftWidth.split('px')[0],
            h = c({
                top: p.top - s.top - l,
                left: p.left - s.left - m,
                width: p.width,
                height: p.height
            });
        if (h.marginTop = 0, h.marginLeft = 0, !i && r) {
            var u = +a.marginTop.split('px')[0],
                b = +a.marginLeft.split('px')[0];
            h.top -= l - u, h.bottom -= l - u, h.left -= m - b, h.right -= m - b, h.marginTop = u, h.marginLeft = b
        }
        return (i ? o.contains(d) : o === d && 'BODY' !== d.nodeName) && (h = f(h, o)), h
    }

    function b(e) {
        var t = window.document.documentElement,
            o = u(e, t),
            i = _(t.clientWidth, window.innerWidth || 0),
            n = _(t.clientHeight, window.innerHeight || 0),
            r = a(t),
            p = a(t, 'left'),
            s = {
                top: r - o.top + o.marginTop,
                left: p - o.left + o.marginLeft,
                width: i,
                height: n
            };
        return c(s)
    }

    function y(e) {
        var i = e.nodeName;
        return 'BODY' === i || 'HTML' === i ? !1 : 'fixed' === t(e, 'position') || y(o(e))
    }

    function w(e, t, i, r) {
        var p = {
                top: 0,
                left: 0
            },
            s = d(e, t);
        if ('viewport' === r) p = b(s);
        else {
            var a;
            'scrollParent' === r ? (a = n(o(e)), 'BODY' === a.nodeName && (a = window.document.documentElement)) : 'window' === r ? a = window.document.documentElement : a = r;
            var f = u(a, s);
            if ('HTML' === a.nodeName && !y(s)) {
                var l = h(),
                    m = l.height,
                    c = l.width;
                p.top += f.top - f.marginTop, p.bottom = m + f.top, p.left += f.left - f.marginLeft, p.right = c + f.left
            } else p = f
        }
        return p.left += i, p.top += i, p.right -= i, p.bottom -= i, p
    }

    function v(e) {
        var t = e.width,
            o = e.height;
        return t * o
    }

    function E(e, t, o, i, n) {
        var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
        if (-1 === e.indexOf('auto')) return e;
        var p = w(o, i, r, n),
            s = {
                top: {
                    width: p.width,
                    height: t.top - p.top
                },
                right: {
                    width: p.right - t.right,
                    height: p.height
                },
                bottom: {
                    width: p.width,
                    height: p.bottom - t.bottom
                },
                left: {
                    width: t.left - p.left,
                    height: p.height
                }
            },
            d = Object.keys(s).map(function(e) {
                return se({
                    key: e
                }, s[e], {
                    area: v(s[e])
                })
            }).sort(function(e, t) {
                return t.area - e.area
            }),
            a = d.filter(function(e) {
                var t = e.width,
                    i = e.height;
                return t >= o.clientWidth && i >= o.clientHeight
            }),
            f = 0 < a.length ? a[0].key : d[0].key,
            l = e.split('-')[1];
        return f + (l ? '-' + l : '')
    }

    function x(e, t, o) {
        var i = d(t, o);
        return u(o, i)
    }

    function O(e) {
        var t = window.getComputedStyle(e),
            o = parseFloat(t.marginTop) + parseFloat(t.marginBottom),
            i = parseFloat(t.marginLeft) + parseFloat(t.marginRight),
            n = {
                width: e.offsetWidth + i,
                height: e.offsetHeight + o
            };
        return n
    }

    function L(e) {
        var t = {
            left: 'right',
            right: 'left',
            bottom: 'top',
            top: 'bottom'
        };
        return e.replace(/left|right|bottom|top/g, function(e) {
            return t[e]
        })
    }

    function S(e, t, o) {
        o = o.split('-')[0];
        var i = O(e),
            n = {
                width: i.width,
                height: i.height
            },
            r = -1 !== ['right', 'left'].indexOf(o),
            p = r ? 'top' : 'left',
            s = r ? 'left' : 'top',
            d = r ? 'height' : 'width',
            a = r ? 'width' : 'height';
        return n[p] = t[p] + t[d] / 2 - i[d] / 2, n[s] = o === s ? t[s] - i[a] : t[L(s)], n
    }

    function T(e, t) {
        return Array.prototype.find ? e.find(t) : e.filter(t)[0]
    }

    function C(e, t, o) {
        if (Array.prototype.findIndex) return e.findIndex(function(e) {
            return e[t] === o
        });
        var i = T(e, function(e) {
            return e[t] === o
        });
        return e.indexOf(i)
    }

    function N(t, o, i) {
        var n = void 0 === i ? t : t.slice(0, C(t, 'name', i));
        return n.forEach(function(t) {
            t.function && console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
            var i = t.function || t.fn;
            t.enabled && e(i) && (o.offsets.popper = c(o.offsets.popper), o.offsets.reference = c(o.offsets.reference), o = i(o, t))
        }), o
    }

    function k() {
        if (!this.state.isDestroyed) {
            var e = {
                instance: this,
                styles: {},
                attributes: {},
                flipped: !1,
                offsets: {}
            };
            e.offsets.reference = x(this.state, this.popper, this.reference), e.placement = E(this.options.placement, e.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), e.originalPlacement = e.placement, e.offsets.popper = S(this.popper, e.offsets.reference, e.placement), e.offsets.popper.position = 'absolute', e = N(this.modifiers, e), this.state.isCreated ? this.options.onUpdate(e) : (this.state.isCreated = !0, this.options.onCreate(e))
        }
    }

    function W(e, t) {
        return e.some(function(e) {
            var o = e.name,
                i = e.enabled;
            return i && o === t
        })
    }

    function B(e) {
        for (var t = [!1, 'ms', 'Webkit', 'Moz', 'O'], o = e.charAt(0).toUpperCase() + e.slice(1), n = 0; n < t.length - 1; n++) {
            var i = t[n],
                r = i ? '' + i + o : e;
            if ('undefined' != typeof window.document.body.style[r]) return r
        }
        return null
    }

    function D() {
        return this.state.isDestroyed = !0, W(this.modifiers, 'applyStyle') && (this.popper.removeAttribute('x-placement'), this.popper.style.left = '', this.popper.style.position = '', this.popper.style.top = '', this.popper.style[B('transform')] = ''), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this
    }

    function H(e, t, o, i) {
        var r = 'BODY' === e.nodeName,
            p = r ? window : e;
        p.addEventListener(t, o, {
            passive: !0
        }), r || H(n(p.parentNode), t, o, i), i.push(p)
    }

    function P(e, t, o, i) {
        o.updateBound = i, window.addEventListener('resize', o.updateBound, {
            passive: !0
        });
        var r = n(e);
        return H(r, 'scroll', o.updateBound, o.scrollParents), o.scrollElement = r, o.eventsEnabled = !0, o
    }

    function A() {
        this.state.eventsEnabled || (this.state = P(this.reference, this.options, this.state, this.scheduleUpdate))
    }

    function M(e, t) {
        return window.removeEventListener('resize', t.updateBound), t.scrollParents.forEach(function(e) {
            e.removeEventListener('scroll', t.updateBound)
        }), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t
    }

    function I() {
        this.state.eventsEnabled && (window.cancelAnimationFrame(this.scheduleUpdate), this.state = M(this.reference, this.state))
    }

    function R(e) {
        return '' !== e && !isNaN(parseFloat(e)) && isFinite(e)
    }

    function U(e, t) {
        Object.keys(t).forEach(function(o) {
            var i = ''; - 1 !== ['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(o) && R(t[o]) && (i = 'px'), e.style[o] = t[o] + i
        })
    }

    function Y(e, t) {
        Object.keys(t).forEach(function(o) {
            var i = t[o];
            !1 === i ? e.removeAttribute(o) : e.setAttribute(o, t[o])
        })
    }

    function F(e, t, o) {
        var i = T(e, function(e) {
                var o = e.name;
                return o === t
            }),
            n = !!i && e.some(function(e) {
                return e.name === o && e.enabled && e.order < i.order
            });
        if (!n) {
            var r = '`' + t + '`';
            console.warn('`' + o + '`' + ' modifier is required by ' + r + ' modifier in order to work, be sure to include it before ' + r + '!')
        }
        return n
    }

    function j(e) {
        return 'end' === e ? 'start' : 'start' === e ? 'end' : e
    }

    function K(e) {
        var t = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
            o = ae.indexOf(e),
            i = ae.slice(o + 1).concat(ae.slice(0, o));
        return t ? i.reverse() : i
    }

    function q(e, t, o, i) {
        var n = e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
            r = +n[1],
            p = n[2];
        if (!r) return e;
        if (0 === p.indexOf('%')) {
            var s;
            switch (p) {
                case '%p':
                    s = o;
                    break;
                case '%':
                case '%r':
                default:
                    s = i;
            }
            var d = c(s);
            return d[t] / 100 * r
        }
        if ('vh' === p || 'vw' === p) {
            var a;
            return a = 'vh' === p ? _(document.documentElement.clientHeight, window.innerHeight || 0) : _(document.documentElement.clientWidth, window.innerWidth || 0), a / 100 * r
        }
        return r
    }

    function G(e, t, o, i) {
        var n = [0, 0],
            r = -1 !== ['right', 'left'].indexOf(i),
            p = e.split(/(\+|\-)/).map(function(e) {
                return e.trim()
            }),
            s = p.indexOf(T(p, function(e) {
                return -1 !== e.search(/,|\s/)
            }));
        p[s] && -1 === p[s].indexOf(',') && console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
        var d = /\s*,\s*|\s+/,
            a = -1 === s ? [p] : [p.slice(0, s).concat([p[s].split(d)[0]]), [p[s].split(d)[1]].concat(p.slice(s + 1))];
        return a = a.map(function(e, i) {
            var n = (1 === i ? !r : r) ? 'height' : 'width',
                p = !1;
            return e.reduce(function(e, t) {
                return '' === e[e.length - 1] && -1 !== ['+', '-'].indexOf(t) ? (e[e.length - 1] = t, p = !0, e) : p ? (e[e.length - 1] += t, p = !1, e) : e.concat(t)
            }, []).map(function(e) {
                return q(e, n, t, o)
            })
        }), a.forEach(function(e, t) {
            e.forEach(function(o, i) {
                R(o) && (n[t] += o * ('-' === e[i - 1] ? -1 : 1))
            })
        }), n
    }
    for (var z = Math.min, V = Math.floor, _ = Math.max, X = ['native code', '[object MutationObserverConstructor]'], Q = function(e) {
            return X.some(function(t) {
                return -1 < (e || '').toString().indexOf(t)
            })
        }, J = 'undefined' != typeof window, Z = ['Edge', 'Trident', 'Firefox'], $ = 0, ee = 0; ee < Z.length; ee += 1)
        if (J && 0 <= navigator.userAgent.indexOf(Z[ee])) {
            $ = 1;
            break
        }
    var i, te = J && Q(window.MutationObserver),
        oe = te ? function(e) {
            var t = !1,
                o = 0,
                i = document.createElement('span'),
                n = new MutationObserver(function() {
                    e(), t = !1
                });
            return n.observe(i, {
                    attributes: !0
                }),
                function() {
                    t || (t = !0, i.setAttribute('x-index', o), ++o)
                }
        } : function(e) {
            var t = !1;
            return function() {
                t || (t = !0, setTimeout(function() {
                    t = !1, e()
                }, $))
            }
        },
        ie = function() {
            return void 0 == i && (i = -1 !== navigator.appVersion.indexOf('MSIE 10')), i
        },
        ne = function(e, t) {
            if (!(e instanceof t)) throw new TypeError('Cannot call a class as a function')
        },
        re = function() {
            function e(e, t) {
                for (var o, n = 0; n < t.length; n++) o = t[n], o.enumerable = o.enumerable || !1, o.configurable = !0, 'value' in o && (o.writable = !0), Object.defineProperty(e, o.key, o)
            }
            return function(t, o, i) {
                return o && e(t.prototype, o), i && e(t, i), t
            }
        }(),
        pe = function(e, t, o) {
            return t in e ? Object.defineProperty(e, t, {
                value: o,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = o, e
        },
        se = Object.assign || function(e) {
            for (var t, o = 1; o < arguments.length; o++)
                for (var i in t = arguments[o], t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
            return e
        },
        de = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'],
        ae = de.slice(3),
        fe = {
            FLIP: 'flip',
            CLOCKWISE: 'clockwise',
            COUNTERCLOCKWISE: 'counterclockwise'
        },
        le = function() {
            function t(o, i) {
                var n = this,
                    r = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
                ne(this, t), this.scheduleUpdate = function() {
                    return requestAnimationFrame(n.update)
                }, this.update = oe(this.update.bind(this)), this.options = se({}, t.Defaults, r), this.state = {
                    isDestroyed: !1,
                    isCreated: !1,
                    scrollParents: []
                }, this.reference = o.jquery ? o[0] : o, this.popper = i.jquery ? i[0] : i, this.options.modifiers = {}, Object.keys(se({}, t.Defaults.modifiers, r.modifiers)).forEach(function(e) {
                    n.options.modifiers[e] = se({}, t.Defaults.modifiers[e] || {}, r.modifiers ? r.modifiers[e] : {})
                }), this.modifiers = Object.keys(this.options.modifiers).map(function(e) {
                    return se({
                        name: e
                    }, n.options.modifiers[e])
                }).sort(function(e, t) {
                    return e.order - t.order
                }), this.modifiers.forEach(function(t) {
                    t.enabled && e(t.onLoad) && t.onLoad(n.reference, n.popper, n.options, t, n.state)
                }), this.update();
                var p = this.options.eventsEnabled;
                p && this.enableEventListeners(), this.state.eventsEnabled = p
            }
            return re(t, [{
                key: 'update',
                value: function() {
                    return k.call(this)
                }
            }, {
                key: 'destroy',
                value: function() {
                    return D.call(this)
                }
            }, {
                key: 'enableEventListeners',
                value: function() {
                    return A.call(this)
                }
            }, {
                key: 'disableEventListeners',
                value: function() {
                    return I.call(this)
                }
            }]), t
        }();
    return le.Utils = ('undefined' == typeof window ? global : window).PopperUtils, le.placements = de, le.Defaults = {
        placement: 'bottom',
        eventsEnabled: !0,
        removeOnDestroy: !1,
        onCreate: function() {},
        onUpdate: function() {},
        modifiers: {
            shift: {
                order: 100,
                enabled: !0,
                fn: function(e) {
                    var t = e.placement,
                        o = t.split('-')[0],
                        i = t.split('-')[1];
                    if (i) {
                        var n = e.offsets,
                            r = n.reference,
                            p = n.popper,
                            s = -1 !== ['bottom', 'top'].indexOf(o),
                            d = s ? 'left' : 'top',
                            a = s ? 'width' : 'height',
                            f = {
                                start: pe({}, d, r[d]),
                                end: pe({}, d, r[d] + r[a] - p[a])
                            };
                        e.offsets.popper = se({}, p, f[i])
                    }
                    return e
                }
            },
            offset: {
                order: 200,
                enabled: !0,
                fn: function(e, t) {
                    var o, i = t.offset,
                        n = e.placement,
                        r = e.offsets,
                        p = r.popper,
                        s = r.reference,
                        d = n.split('-')[0];
                    return o = R(+i) ? [+i, 0] : G(i, p, s, d), 'left' === d ? (p.top += o[0], p.left -= o[1]) : 'right' === d ? (p.top += o[0], p.left += o[1]) : 'top' === d ? (p.left += o[0], p.top -= o[1]) : 'bottom' === d && (p.left += o[0], p.top += o[1]), e.popper = p, e
                },
                offset: 0
            },
            preventOverflow: {
                order: 300,
                enabled: !0,
                fn: function(e, t) {
                    var o = t.boundariesElement || r(e.instance.popper);
                    e.instance.reference === o && (o = r(o));
                    var i = w(e.instance.popper, e.instance.reference, t.padding, o);
                    t.boundaries = i;
                    var n = t.priority,
                        p = e.offsets.popper,
                        s = {
                            primary: function(e) {
                                var o = p[e];
                                return p[e] < i[e] && !t.escapeWithReference && (o = _(p[e], i[e])), pe({}, e, o)
                            },
                            secondary: function(e) {
                                var o = 'right' === e ? 'left' : 'top',
                                    n = p[o];
                                return p[e] > i[e] && !t.escapeWithReference && (n = z(p[o], i[e] - ('right' === e ? p.width : p.height))), pe({}, o, n)
                            }
                        };
                    return n.forEach(function(e) {
                        var t = -1 === ['left', 'top'].indexOf(e) ? 'secondary' : 'primary';
                        p = se({}, p, s[t](e))
                    }), e.offsets.popper = p, e
                },
                priority: ['left', 'right', 'top', 'bottom'],
                padding: 5,
                boundariesElement: 'scrollParent'
            },
            keepTogether: {
                order: 400,
                enabled: !0,
                fn: function(e) {
                    var t = e.offsets,
                        o = t.popper,
                        i = t.reference,
                        n = e.placement.split('-')[0],
                        r = V,
                        p = -1 !== ['top', 'bottom'].indexOf(n),
                        s = p ? 'right' : 'bottom',
                        d = p ? 'left' : 'top',
                        a = p ? 'width' : 'height';
                    return o[s] < r(i[d]) && (e.offsets.popper[d] = r(i[d]) - o[a]), o[d] > r(i[s]) && (e.offsets.popper[d] = r(i[s])), e
                }
            },
            arrow: {
                order: 500,
                enabled: !0,
                fn: function(e, t) {
                    if (!F(e.instance.modifiers, 'arrow', 'keepTogether')) return e;
                    var o = t.element;
                    if ('string' == typeof o) {
                        if (o = e.instance.popper.querySelector(o), !o) return e;
                    } else if (!e.instance.popper.contains(o)) return console.warn('WARNING: `arrow.element` must be child of its popper element!'), e;
                    var i = e.placement.split('-')[0],
                        n = e.offsets,
                        r = n.popper,
                        p = n.reference,
                        s = -1 !== ['left', 'right'].indexOf(i),
                        d = s ? 'height' : 'width',
                        a = s ? 'top' : 'left',
                        f = s ? 'left' : 'top',
                        l = s ? 'bottom' : 'right',
                        m = O(o)[d];
                    p[l] - m < r[a] && (e.offsets.popper[a] -= r[a] - (p[l] - m)), p[a] + m > r[l] && (e.offsets.popper[a] += p[a] + m - r[l]);
                    var h = p[a] + p[d] / 2 - m / 2,
                        g = h - c(e.offsets.popper)[a];
                    return g = _(z(r[d] - m, g), 0), e.arrowElement = o, e.offsets.arrow = {}, e.offsets.arrow[a] = Math.round(g), e.offsets.arrow[f] = '', e
                },
                element: '[x-arrow]'
            },
            flip: {
                order: 600,
                enabled: !0,
                fn: function(e, t) {
                    if (W(e.instance.modifiers, 'inner')) return e;
                    if (e.flipped && e.placement === e.originalPlacement) return e;
                    var o = w(e.instance.popper, e.instance.reference, t.padding, t.boundariesElement),
                        i = e.placement.split('-')[0],
                        n = L(i),
                        r = e.placement.split('-')[1] || '',
                        p = [];
                    switch (t.behavior) {
                        case fe.FLIP:
                            p = [i, n];
                            break;
                        case fe.CLOCKWISE:
                            p = K(i);
                            break;
                        case fe.COUNTERCLOCKWISE:
                            p = K(i, !0);
                            break;
                        default:
                            p = t.behavior;
                    }
                    return p.forEach(function(s, d) {
                        if (i !== s || p.length === d + 1) return e;
                        i = e.placement.split('-')[0], n = L(i);
                        var a = e.offsets.popper,
                            f = e.offsets.reference,
                            l = V,
                            m = 'left' === i && l(a.right) > l(f.left) || 'right' === i && l(a.left) < l(f.right) || 'top' === i && l(a.bottom) > l(f.top) || 'bottom' === i && l(a.top) < l(f.bottom),
                            h = l(a.left) < l(o.left),
                            c = l(a.right) > l(o.right),
                            g = l(a.top) < l(o.top),
                            u = l(a.bottom) > l(o.bottom),
                            b = 'left' === i && h || 'right' === i && c || 'top' === i && g || 'bottom' === i && u,
                            y = -1 !== ['top', 'bottom'].indexOf(i),
                            w = !!t.flipVariations && (y && 'start' === r && h || y && 'end' === r && c || !y && 'start' === r && g || !y && 'end' === r && u);
                        (m || b || w) && (e.flipped = !0, (m || b) && (i = p[d + 1]), w && (r = j(r)), e.placement = i + (r ? '-' + r : ''), e.offsets.popper = se({}, e.offsets.popper, S(e.instance.popper, e.offsets.reference, e.placement)), e = N(e.instance.modifiers, e, 'flip'))
                    }), e
                },
                behavior: 'flip',
                padding: 5,
                boundariesElement: 'viewport'
            },
            inner: {
                order: 700,
                enabled: !1,
                fn: function(e) {
                    var t = e.placement,
                        o = t.split('-')[0],
                        i = e.offsets,
                        n = i.popper,
                        r = i.reference,
                        p = -1 !== ['left', 'right'].indexOf(o),
                        s = -1 === ['top', 'left'].indexOf(o);
                    return n[p ? 'left' : 'top'] = r[t] - (s ? n[p ? 'width' : 'height'] : 0), e.placement = L(t), e.offsets.popper = c(n), e
                }
            },
            hide: {
                order: 800,
                enabled: !0,
                fn: function(e) {
                    if (!F(e.instance.modifiers, 'hide', 'preventOverflow')) return e;
                    var t = e.offsets.reference,
                        o = T(e.instance.modifiers, function(e) {
                            return 'preventOverflow' === e.name
                        }).boundaries;
                    if (t.bottom < o.top || t.left > o.right || t.top > o.bottom || t.right < o.left) {
                        if (!0 === e.hide) return e;
                        e.hide = !0, e.attributes['x-out-of-boundaries'] = ''
                    } else {
                        if (!1 === e.hide) return e;
                        e.hide = !1, e.attributes['x-out-of-boundaries'] = !1
                    }
                    return e
                }
            },
            computeStyle: {
                order: 850,
                enabled: !0,
                fn: function(e, t) {
                    var o = t.x,
                        i = t.y,
                        n = e.offsets.popper,
                        p = T(e.instance.modifiers, function(e) {
                            return 'applyStyle' === e.name
                        }).gpuAcceleration;
                    void 0 !== p && console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
                    var s, d, a = void 0 === p ? t.gpuAcceleration : p,
                        f = r(e.instance.popper),
                        l = g(f),
                        m = {
                            position: n.position
                        },
                        h = {
                            left: V(n.left),
                            top: V(n.top),
                            bottom: V(n.bottom),
                            right: V(n.right)
                        },
                        c = 'bottom' === o ? 'top' : 'bottom',
                        u = 'right' === i ? 'left' : 'right',
                        b = B('transform');
                    if (d = 'bottom' == c ? -l.height + h.bottom : h.top, s = 'right' == u ? -l.width + h.right : h.left, a && b) m[b] = 'translate3d(' + s + 'px, ' + d + 'px, 0)', m[c] = 0, m[u] = 0, m.willChange = 'transform';
                    else {
                        var y = 'bottom' == c ? -1 : 1,
                            w = 'right' == u ? -1 : 1;
                        m[c] = d * y, m[u] = s * w, m.willChange = c + ', ' + u
                    }
                    var v = {
                        "x-placement": e.placement
                    };
                    return e.attributes = se({}, v, e.attributes), e.styles = se({}, m, e.styles), e
                },
                gpuAcceleration: !0,
                x: 'bottom',
                y: 'right'
            },
            applyStyle: {
                order: 900,
                enabled: !0,
                fn: function(e) {
                    return U(e.instance.popper, e.styles), Y(e.instance.popper, e.attributes), e.offsets.arrow && U(e.arrowElement, e.offsets.arrow), e
                },
                onLoad: function(e, t, o, i, n) {
                    var r = x(n, t, e),
                        p = E(o.placement, r, t, e, o.modifiers.flip.boundariesElement, o.modifiers.flip.padding);
                    return t.setAttribute('x-placement', p), U(t, {
                        position: 'absolute'
                    }), o
                },
                gpuAcceleration: void 0
            }
        }
    }, le
});;
! function(t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e(require, exports, module) : t.Tether = e()
}(this, function(t, e, o) {
    "use strict";

    function i(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
    }

    function n(t) {
        var e = t.getBoundingClientRect(),
            o = {};
        for (var i in e) o[i] = e[i];
        if (t.ownerDocument !== document) {
            var r = t.ownerDocument.defaultView.frameElement;
            if (r) {
                var s = n(r);
                o.top += s.top, o.bottom += s.top, o.left += s.left, o.right += s.left
            }
        }
        return o
    }

    function r(t) {
        var e = getComputedStyle(t) || {},
            o = e.position,
            i = [];
        if ("fixed" === o) return [t];
        for (var n = t;
            (n = n.parentNode) && n && 1 === n.nodeType;) {
            var r = void 0;
            try {
                r = getComputedStyle(n)
            } catch (s) {}
            if ("undefined" == typeof r || null === r) return i.push(n), i;
            var a = r,
                f = a.overflow,
                l = a.overflowX,
                h = a.overflowY;
            /(auto|scroll)/.test(f + h + l) && ("absolute" !== o || ["relative", "absolute", "fixed"].indexOf(r.position) >= 0) && i.push(n)
        }
        return i.push(t.ownerDocument.body), t.ownerDocument !== document && i.push(t.ownerDocument.defaultView), i
    }

    function s() {
        A && document.body.removeChild(A), A = null
    }

    function a(t) {
        var e = void 0;
        t === document ? (e = document, t = document.documentElement) : e = t.ownerDocument;
        var o = e.documentElement,
            i = n(t),
            r = P();
        return i.top -= r.top, i.left -= r.left, "undefined" == typeof i.width && (i.width = document.body.scrollWidth - i.left - i.right), "undefined" == typeof i.height && (i.height = document.body.scrollHeight - i.top - i.bottom), i.top = i.top - o.clientTop, i.left = i.left - o.clientLeft, i.right = e.body.clientWidth - i.width - i.left, i.bottom = e.body.clientHeight - i.height - i.top, i
    }

    function f(t) {
        return t.offsetParent || document.documentElement
    }

    function l() {
        if (M) return M;
        var t = document.createElement("div");
        t.style.width = "100%", t.style.height = "200px";
        var e = document.createElement("div");
        h(e.style, {
            position: "absolute",
            top: 0,
            left: 0,
            pointerEvents: "none",
            visibility: "hidden",
            width: "200px",
            height: "150px",
            overflow: "hidden"
        }), e.appendChild(t), document.body.appendChild(e);
        var o = t.offsetWidth;
        e.style.overflow = "scroll";
        var i = t.offsetWidth;
        o === i && (i = e.clientWidth), document.body.removeChild(e);
        var n = o - i;
        return M = {
            width: n,
            height: n
        }
    }

    function h() {
        var t = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0],
            e = [];
        return Array.prototype.push.apply(e, arguments), e.slice(1).forEach(function(e) {
            if (e)
                for (var o in e)({}).hasOwnProperty.call(e, o) && (t[o] = e[o])
        }), t
    }

    function d(t, e) {
        if ("undefined" != typeof t.classList) e.split(" ").forEach(function(e) {
            e.trim() && t.classList.remove(e)
        });
        else {
            var o = new RegExp("(^| )" + e.split(" ").join("|") + "( |$)", "gi"),
                i = c(t).replace(o, " ");
            g(t, i)
        }
    }

    function p(t, e) {
        if ("undefined" != typeof t.classList) e.split(" ").forEach(function(e) {
            e.trim() && t.classList.add(e)
        });
        else {
            d(t, e);
            var o = c(t) + (" " + e);
            g(t, o)
        }
    }

    function u(t, e) {
        if ("undefined" != typeof t.classList) return t.classList.contains(e);
        var o = c(t);
        return new RegExp("(^| )" + e + "( |$)", "gi").test(o)
    }

    function c(t) {
        return t.className instanceof t.ownerDocument.defaultView.SVGAnimatedString ? t.className.baseVal : t.className
    }

    function g(t, e) {
        t.setAttribute("class", e)
    }

    function m(t, e, o) {
        o.forEach(function(o) {
            e.indexOf(o) === -1 && u(t, o) && d(t, o)
        }), e.forEach(function(e) {
            u(t, e) || p(t, e)
        })
    }

    function i(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
    }

    function v(t, e) {
        if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
        t.prototype = Object.create(e && e.prototype, {
            constructor: {
                value: t,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
    }

    function y(t, e) {
        var o = arguments.length <= 2 || void 0 === arguments[2] ? 1 : arguments[2];
        return t + o >= e && e >= t - o
    }

    function b() {
        return "undefined" != typeof performance && "undefined" != typeof performance.now ? performance.now() : +new Date
    }

    function w() {
        for (var t = {
                top: 0,
                left: 0
            }, e = arguments.length, o = Array(e), i = 0; i < e; i++) o[i] = arguments[i];
        return o.forEach(function(e) {
            var o = e.top,
                i = e.left;
            "string" == typeof o && (o = parseFloat(o, 10)), "string" == typeof i && (i = parseFloat(i, 10)), t.top += o, t.left += i
        }), t
    }

    function C(t, e) {
        return "string" == typeof t.left && t.left.indexOf("%") !== -1 && (t.left = parseFloat(t.left, 10) / 100 * e.width), "string" == typeof t.top && t.top.indexOf("%") !== -1 && (t.top = parseFloat(t.top, 10) / 100 * e.height), t
    }

    function O(t, e) {
        return "scrollParent" === e ? e = t.scrollParents[0] : "window" === e && (e = [pageXOffset, pageYOffset, innerWidth + pageXOffset, innerHeight + pageYOffset]), e === document && (e = e.documentElement), "undefined" != typeof e.nodeType && ! function() {
            var t = e,
                o = a(e),
                i = o,
                n = getComputedStyle(e);
            if (e = [i.left, i.top, o.width + i.left, o.height + i.top], t.ownerDocument !== document) {
                var r = t.ownerDocument.defaultView;
                e[0] += r.pageXOffset, e[1] += r.pageYOffset, e[2] += r.pageXOffset, e[3] += r.pageYOffset
            }
            G.forEach(function(t, o) {
                t = t[0].toUpperCase() + t.substr(1), "Top" === t || "Left" === t ? e[o] += parseFloat(n["border" + t + "Width"]) : e[o] -= parseFloat(n["border" + t + "Width"])
            })
        }(), e
    }
    var E = function() {
            function t(t, e) {
                for (var o = 0; o < e.length; o++) {
                    var i = e[o];
                    i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
                }
            }
            return function(e, o, i) {
                return o && t(e.prototype, o), i && t(e, i), e
            }
        }(),
        x = void 0;
    "undefined" == typeof x && (x = {
        modules: []
    });
    var A = null,
        T = function() {
            var t = 0;
            return function() {
                return ++t
            }
        }(),
        S = {},
        P = function() {
            var t = A;
            t && document.body.contains(t) || (t = document.createElement("div"), t.setAttribute("data-tether-id", T()), h(t.style, {
                top: 0,
                left: 0,
                position: "absolute"
            }), document.body.appendChild(t), A = t);
            var e = t.getAttribute("data-tether-id");
            return "undefined" == typeof S[e] && (S[e] = n(t), k(function() {
                delete S[e]
            })), S[e]
        },
        M = null,
        W = [],
        k = function(t) {
            W.push(t)
        },
        _ = function() {
            for (var t = void 0; t = W.pop();) t()
        },
        B = function() {
            function t() {
                i(this, t)
            }
            return E(t, [{
                key: "on",
                value: function(t, e, o) {
                    var i = !(arguments.length <= 3 || void 0 === arguments[3]) && arguments[3];
                    "undefined" == typeof this.bindings && (this.bindings = {}), "undefined" == typeof this.bindings[t] && (this.bindings[t] = []), this.bindings[t].push({
                        handler: e,
                        ctx: o,
                        once: i
                    })
                }
            }, {
                key: "once",
                value: function(t, e, o) {
                    this.on(t, e, o, !0)
                }
            }, {
                key: "off",
                value: function(t, e) {
                    if ("undefined" != typeof this.bindings && "undefined" != typeof this.bindings[t])
                        if ("undefined" == typeof e) delete this.bindings[t];
                        else
                            for (var o = 0; o < this.bindings[t].length;) this.bindings[t][o].handler === e ? this.bindings[t].splice(o, 1) : ++o
                }
            }, {
                key: "trigger",
                value: function(t) {
                    if ("undefined" != typeof this.bindings && this.bindings[t]) {
                        for (var e = 0, o = arguments.length, i = Array(o > 1 ? o - 1 : 0), n = 1; n < o; n++) i[n - 1] = arguments[n];
                        for (; e < this.bindings[t].length;) {
                            var r = this.bindings[t][e],
                                s = r.handler,
                                a = r.ctx,
                                f = r.once,
                                l = a;
                            "undefined" == typeof l && (l = this), s.apply(l, i), f ? this.bindings[t].splice(e, 1) : ++e
                        }
                    }
                }
            }]), t
        }();
    x.Utils = {
        getActualBoundingClientRect: n,
        getScrollParents: r,
        getBounds: a,
        getOffsetParent: f,
        extend: h,
        addClass: p,
        removeClass: d,
        hasClass: u,
        updateClasses: m,
        defer: k,
        flush: _,
        uniqueId: T,
        Evented: B,
        getScrollBarSize: l,
        removeUtilElements: s
    };
    var z = function() {
            function t(t, e) {
                var o = [],
                    i = !0,
                    n = !1,
                    r = void 0;
                try {
                    for (var s, a = t[Symbol.iterator](); !(i = (s = a.next()).done) && (o.push(s.value), !e || o.length !== e); i = !0);
                } catch (f) {
                    n = !0, r = f
                } finally {
                    try {
                        !i && a["return"] && a["return"]()
                    } finally {
                        if (n) throw r
                    }
                }
                return o
            }
            return function(e, o) {
                if (Array.isArray(e)) return e;
                if (Symbol.iterator in Object(e)) return t(e, o);
                throw new TypeError("Invalid attempt to destructure non-iterable instance")
            }
        }(),
        E = function() {
            function t(t, e) {
                for (var o = 0; o < e.length; o++) {
                    var i = e[o];
                    i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i)
                }
            }
            return function(e, o, i) {
                return o && t(e.prototype, o), i && t(e, i), e
            }
        }(),
        j = function(t, e, o) {
            for (var i = !0; i;) {
                var n = t,
                    r = e,
                    s = o;
                i = !1, null === n && (n = Function.prototype);
                var a = Object.getOwnPropertyDescriptor(n, r);
                if (void 0 !== a) {
                    if ("value" in a) return a.value;
                    var f = a.get;
                    if (void 0 === f) return;
                    return f.call(s)
                }
                var l = Object.getPrototypeOf(n);
                if (null === l) return;
                t = l, e = r, o = s, i = !0, a = l = void 0
            }
        };
    if ("undefined" == typeof x) throw new Error("You must include the utils.js file before tether.js");
    var Y = x.Utils,
        r = Y.getScrollParents,
        a = Y.getBounds,
        f = Y.getOffsetParent,
        h = Y.extend,
        p = Y.addClass,
        d = Y.removeClass,
        m = Y.updateClasses,
        k = Y.defer,
        _ = Y.flush,
        l = Y.getScrollBarSize,
        s = Y.removeUtilElements,
        L = function() {
            if ("undefined" == typeof document) return "";
            for (var t = document.createElement("div"), e = ["transform", "WebkitTransform", "OTransform", "MozTransform", "msTransform"], o = 0; o < e.length; ++o) {
                var i = e[o];
                if (void 0 !== t.style[i]) return i
            }
        }(),
        D = [],
        X = function() {
            D.forEach(function(t) {
                t.position(!1)
            }), _()
        };
    ! function() {
        var t = null,
            e = null,
            o = null,
            i = function n() {
                return "undefined" != typeof e && e > 16 ? (e = Math.min(e - 16, 250), void(o = setTimeout(n, 250))) : void("undefined" != typeof t && b() - t < 10 || (null != o && (clearTimeout(o), o = null), t = b(), X(), e = b() - t))
            };
        "undefined" != typeof window && "undefined" != typeof window.addEventListener && ["resize", "scroll", "touchmove"].forEach(function(t) {
            window.addEventListener(t, i)
        })
    }();
    var F = {
            center: "center",
            left: "right",
            right: "left"
        },
        H = {
            middle: "middle",
            top: "bottom",
            bottom: "top"
        },
        N = {
            top: 0,
            left: 0,
            middle: "50%",
            center: "50%",
            bottom: "100%",
            right: "100%"
        },
        U = function(t, e) {
            var o = t.left,
                i = t.top;
            return "auto" === o && (o = F[e.left]), "auto" === i && (i = H[e.top]), {
                left: o,
                top: i
            }
        },
        V = function(t) {
            var e = t.left,
                o = t.top;
            return "undefined" != typeof N[t.left] && (e = N[t.left]), "undefined" != typeof N[t.top] && (o = N[t.top]), {
                left: e,
                top: o
            }
        },
        R = function(t) {
            var e = t.split(" "),
                o = z(e, 2),
                i = o[0],
                n = o[1];
            return {
                top: i,
                left: n
            }
        },
        q = R,
        I = function(t) {
            function e(t) {
                var o = this;
                i(this, e), j(Object.getPrototypeOf(e.prototype), "constructor", this).call(this), this.position = this.position.bind(this), D.push(this), this.history = [], this.setOptions(t, !1), x.modules.forEach(function(t) {
                    "undefined" != typeof t.initialize && t.initialize.call(o)
                }), this.position()
            }
            return v(e, t), E(e, [{
                key: "getClass",
                value: function() {
                    var t = arguments.length <= 0 || void 0 === arguments[0] ? "" : arguments[0],
                        e = this.options.classes;
                    return "undefined" != typeof e && e[t] ? this.options.classes[t] : this.options.classPrefix ? this.options.classPrefix + "-" + t : t
                }
            }, {
                key: "setOptions",
                value: function(t) {
                    var e = this,
                        o = arguments.length <= 1 || void 0 === arguments[1] || arguments[1],
                        i = {
                            offset: "0 0",
                            targetOffset: "0 0",
                            targetAttachment: "auto auto",
                            classPrefix: "tether"
                        };
                    this.options = h(i, t);
                    var n = this.options,
                        s = n.element,
                        a = n.target,
                        f = n.targetModifier;
                    if (this.element = s, this.target = a, this.targetModifier = f, "viewport" === this.target ? (this.target = document.body, this.targetModifier = "visible") : "scroll-handle" === this.target && (this.target = document.body, this.targetModifier = "scroll-handle"), ["element", "target"].forEach(function(t) {
                            if ("undefined" == typeof e[t]) throw new Error("Tether Error: Both element and target must be defined");
                            "undefined" != typeof e[t].jquery ? e[t] = e[t][0] : "string" == typeof e[t] && (e[t] = document.querySelector(e[t]))
                        }), p(this.element, this.getClass("element")), this.options.addTargetClasses !== !1 && p(this.target, this.getClass("target")), !this.options.attachment) throw new Error("Tether Error: You must provide an attachment");
                    this.targetAttachment = q(this.options.targetAttachment), this.attachment = q(this.options.attachment), this.offset = R(this.options.offset), this.targetOffset = R(this.options.targetOffset), "undefined" != typeof this.scrollParents && this.disable(), "scroll-handle" === this.targetModifier ? this.scrollParents = [this.target] : this.scrollParents = r(this.target), this.options.enabled !== !1 && this.enable(o)
                }
            }, {
                key: "getTargetBounds",
                value: function() {
                    if ("undefined" == typeof this.targetModifier) return a(this.target);
                    if ("visible" === this.targetModifier) {
                        if (this.target === document.body) return {
                            top: pageYOffset,
                            left: pageXOffset,
                            height: innerHeight,
                            width: innerWidth
                        };
                        var t = a(this.target),
                            e = {
                                height: t.height,
                                width: t.width,
                                top: t.top,
                                left: t.left
                            };
                        return e.height = Math.min(e.height, t.height - (pageYOffset - t.top)), e.height = Math.min(e.height, t.height - (t.top + t.height - (pageYOffset + innerHeight))), e.height = Math.min(innerHeight, e.height), e.height -= 2, e.width = Math.min(e.width, t.width - (pageXOffset - t.left)), e.width = Math.min(e.width, t.width - (t.left + t.width - (pageXOffset + innerWidth))), e.width = Math.min(innerWidth, e.width), e.width -= 2, e.top < pageYOffset && (e.top = pageYOffset), e.left < pageXOffset && (e.left = pageXOffset), e
                    }
                    if ("scroll-handle" === this.targetModifier) {
                        var t = void 0,
                            o = this.target;
                        o === document.body ? (o = document.documentElement, t = {
                            left: pageXOffset,
                            top: pageYOffset,
                            height: innerHeight,
                            width: innerWidth
                        }) : t = a(o);
                        var i = getComputedStyle(o),
                            n = o.scrollWidth > o.clientWidth || [i.overflow, i.overflowX].indexOf("scroll") >= 0 || this.target !== document.body,
                            r = 0;
                        n && (r = 15);
                        var s = t.height - parseFloat(i.borderTopWidth) - parseFloat(i.borderBottomWidth) - r,
                            e = {
                                width: 15,
                                height: .975 * s * (s / o.scrollHeight),
                                left: t.left + t.width - parseFloat(i.borderLeftWidth) - 15
                            },
                            f = 0;
                        s < 408 && this.target === document.body && (f = -11e-5 * Math.pow(s, 2) - .00727 * s + 22.58), this.target !== document.body && (e.height = Math.max(e.height, 24));
                        var l = this.target.scrollTop / (o.scrollHeight - s);
                        return e.top = l * (s - e.height - f) + t.top + parseFloat(i.borderTopWidth), this.target === document.body && (e.height = Math.max(e.height, 24)), e
                    }
                }
            }, {
                key: "clearCache",
                value: function() {
                    this._cache = {}
                }
            }, {
                key: "cache",
                value: function(t, e) {
                    return "undefined" == typeof this._cache && (this._cache = {}), "undefined" == typeof this._cache[t] && (this._cache[t] = e.call(this)), this._cache[t]
                }
            }, {
                key: "enable",
                value: function() {
                    var t = this,
                        e = arguments.length <= 0 || void 0 === arguments[0] || arguments[0];
                    this.options.addTargetClasses !== !1 && p(this.target, this.getClass("enabled")), p(this.element, this.getClass("enabled")), this.enabled = !0, this.scrollParents.forEach(function(e) {
                        e !== t.target.ownerDocument && e.addEventListener("scroll", t.position)
                    }), e && this.position()
                }
            }, {
                key: "disable",
                value: function() {
                    var t = this;
                    d(this.target, this.getClass("enabled")), d(this.element, this.getClass("enabled")), this.enabled = !1, "undefined" != typeof this.scrollParents && this.scrollParents.forEach(function(e) {
                        e.removeEventListener("scroll", t.position)
                    })
                }
            }, {
                key: "destroy",
                value: function() {
                    var t = this;
                    this.disable(), D.forEach(function(e, o) {
                        e === t && D.splice(o, 1)
                    }), 0 === D.length && s()
                }
            }, {
                key: "updateAttachClasses",
                value: function(t, e) {
                    var o = this;
                    t = t || this.attachment, e = e || this.targetAttachment;
                    var i = ["left", "top", "bottom", "right", "middle", "center"];
                    "undefined" != typeof this._addAttachClasses && this._addAttachClasses.length && this._addAttachClasses.splice(0, this._addAttachClasses.length), "undefined" == typeof this._addAttachClasses && (this._addAttachClasses = []);
                    var n = this._addAttachClasses;
                    t.top && n.push(this.getClass("element-attached") + "-" + t.top), t.left && n.push(this.getClass("element-attached") + "-" + t.left), e.top && n.push(this.getClass("target-attached") + "-" + e.top), e.left && n.push(this.getClass("target-attached") + "-" + e.left);
                    var r = [];
                    i.forEach(function(t) {
                        r.push(o.getClass("element-attached") + "-" + t), r.push(o.getClass("target-attached") + "-" + t)
                    }), k(function() {
                        "undefined" != typeof o._addAttachClasses && (m(o.element, o._addAttachClasses, r), o.options.addTargetClasses !== !1 && m(o.target, o._addAttachClasses, r), delete o._addAttachClasses)
                    })
                }
            }, {
                key: "position",
                value: function() {
                    var t = this,
                        e = arguments.length <= 0 || void 0 === arguments[0] || arguments[0];
                    if (this.enabled) {
                        this.clearCache();
                        var o = U(this.targetAttachment, this.attachment);
                        this.updateAttachClasses(this.attachment, o);
                        var i = this.cache("element-bounds", function() {
                                return a(t.element)
                            }),
                            n = i.width,
                            r = i.height;
                        if (0 === n && 0 === r && "undefined" != typeof this.lastSize) {
                            var s = this.lastSize;
                            n = s.width, r = s.height
                        } else this.lastSize = {
                            width: n,
                            height: r
                        };
                        var h = this.cache("target-bounds", function() {
                                return t.getTargetBounds()
                            }),
                            d = h,
                            p = C(V(this.attachment), {
                                width: n,
                                height: r
                            }),
                            u = C(V(o), d),
                            c = C(this.offset, {
                                width: n,
                                height: r
                            }),
                            g = C(this.targetOffset, d);
                        p = w(p, c), u = w(u, g);
                        for (var m = h.left + u.left - p.left, v = h.top + u.top - p.top, y = 0; y < x.modules.length; ++y) {
                            var b = x.modules[y],
                                O = b.position.call(this, {
                                    left: m,
                                    top: v,
                                    targetAttachment: o,
                                    targetPos: h,
                                    elementPos: i,
                                    offset: p,
                                    targetOffset: u,
                                    manualOffset: c,
                                    manualTargetOffset: g,
                                    scrollbarSize: S,
                                    attachment: this.attachment
                                });
                            if (O === !1) return !1;
                            "undefined" != typeof O && "object" == typeof O && (v = O.top, m = O.left)
                        }
                        var E = {
                                page: {
                                    top: v,
                                    left: m
                                },
                                viewport: {
                                    top: v - pageYOffset,
                                    bottom: pageYOffset - v - r + innerHeight,
                                    left: m - pageXOffset,
                                    right: pageXOffset - m - n + innerWidth
                                }
                            },
                            A = this.target.ownerDocument,
                            T = A.defaultView,
                            S = void 0;
                        return T.innerHeight > A.documentElement.clientHeight && (S = this.cache("scrollbar-size", l), E.viewport.bottom -= S.height), T.innerWidth > A.documentElement.clientWidth && (S = this.cache("scrollbar-size", l), E.viewport.right -= S.width), ["", "static"].indexOf(A.body.style.position) !== -1 && ["", "static"].indexOf(A.body.parentElement.style.position) !== -1 || (E.page.bottom = A.body.scrollHeight - v - r, E.page.right = A.body.scrollWidth - m - n), "undefined" != typeof this.options.optimizations && this.options.optimizations.moveElement !== !1 && "undefined" == typeof this.targetModifier && ! function() {
                            var e = t.cache("target-offsetparent", function() {
                                    return f(t.target)
                                }),
                                o = t.cache("target-offsetparent-bounds", function() {
                                    return a(e)
                                }),
                                i = getComputedStyle(e),
                                n = o,
                                r = {};
                            if (["Top", "Left", "Bottom", "Right"].forEach(function(t) {
                                    r[t.toLowerCase()] = parseFloat(i["border" + t + "Width"])
                                }), o.right = A.body.scrollWidth - o.left - n.width + r.right, o.bottom = A.body.scrollHeight - o.top - n.height + r.bottom, E.page.top >= o.top + r.top && E.page.bottom >= o.bottom && E.page.left >= o.left + r.left && E.page.right >= o.right) {
                                var s = e.scrollTop,
                                    l = e.scrollLeft;
                                E.offset = {
                                    top: E.page.top - o.top + s - r.top,
                                    left: E.page.left - o.left + l - r.left
                                }
                            }
                        }(), this.move(E), this.history.unshift(E), this.history.length > 3 && this.history.pop(), e && _(), !0
                    }
                }
            }, {
                key: "move",
                value: function(t) {
                    var e = this;
                    if ("undefined" != typeof this.element.parentNode) {
                        var o = {};
                        for (var i in t) {
                            o[i] = {};
                            for (var n in t[i]) {
                                for (var r = !1, s = 0; s < this.history.length; ++s) {
                                    var a = this.history[s];
                                    if ("undefined" != typeof a[i] && !y(a[i][n], t[i][n])) {
                                        r = !0;
                                        break
                                    }
                                }
                                r || (o[i][n] = !0)
                            }
                        }
                        var l = {
                                top: "",
                                left: "",
                                right: "",
                                bottom: ""
                            },
                            d = function(t, o) {
                                var i = "undefined" != typeof e.options.optimizations,
                                    n = i ? e.options.optimizations.gpu : null;
                                if (n !== !1) {
                                    var r = void 0,
                                        s = void 0;
                                    if (t.top ? (l.top = 0, r = o.top) : (l.bottom = 0, r = -o.bottom), t.left ? (l.left = 0, s = o.left) : (l.right = 0, s = -o.right), window.matchMedia) {
                                        var a = window.matchMedia("only screen and (min-resolution: 1.3dppx)").matches || window.matchMedia("only screen and (-webkit-min-device-pixel-ratio: 1.3)").matches;
                                        a || (s = Math.round(s), r = Math.round(r))
                                    }
                                    l[L] = "translateX(" + s + "px) translateY(" + r + "px)", "msTransform" !== L && (l[L] += " translateZ(0)")
                                } else t.top ? l.top = o.top + "px" : l.bottom = o.bottom + "px", t.left ? l.left = o.left + "px" : l.right = o.right + "px"
                            },
                            p = !1;
                        if ((o.page.top || o.page.bottom) && (o.page.left || o.page.right) ? (l.position = "absolute", d(o.page, t.page)) : (o.viewport.top || o.viewport.bottom) && (o.viewport.left || o.viewport.right) ? (l.position = "fixed", d(o.viewport, t.viewport)) : "undefined" != typeof o.offset && o.offset.top && o.offset.left ? ! function() {
                                l.position = "absolute";
                                var i = e.cache("target-offsetparent", function() {
                                    return f(e.target)
                                });
                                f(e.element) !== i && k(function() {
                                    e.element.parentNode.removeChild(e.element), i.appendChild(e.element)
                                }), d(o.offset, t.offset), p = !0
                            }() : (l.position = "absolute", d({
                                top: !0,
                                left: !0
                            }, t.page)), !p)
                            if (this.options.bodyElement) this.options.bodyElement.appendChild(this.element);
                            else {
                                for (var u = !0, c = this.element.parentNode; c && 1 === c.nodeType && "BODY" !== c.tagName;) {
                                    if ("static" !== getComputedStyle(c).position) {
                                        u = !1;
                                        break
                                    }
                                    c = c.parentNode
                                }
                                u || (this.element.parentNode.removeChild(this.element), this.element.ownerDocument.body.appendChild(this.element))
                            }
                        var g = {},
                            m = !1;
                        for (var n in l) {
                            var v = l[n],
                                b = this.element.style[n];
                            b !== v && (m = !0, g[n] = v)
                        }
                        m && k(function() {
                            h(e.element.style, g), e.trigger("repositioned")
                        })
                    }
                }
            }]), e
        }(B);
    I.modules = [], x.position = X;
    var $ = h(I, x),
        z = function() {
            function t(t, e) {
                var o = [],
                    i = !0,
                    n = !1,
                    r = void 0;
                try {
                    for (var s, a = t[Symbol.iterator](); !(i = (s = a.next()).done) && (o.push(s.value), !e || o.length !== e); i = !0);
                } catch (f) {
                    n = !0, r = f
                } finally {
                    try {
                        !i && a["return"] && a["return"]()
                    } finally {
                        if (n) throw r
                    }
                }
                return o
            }
            return function(e, o) {
                if (Array.isArray(e)) return e;
                if (Symbol.iterator in Object(e)) return t(e, o);
                throw new TypeError("Invalid attempt to destructure non-iterable instance")
            }
        }(),
        Y = x.Utils,
        a = Y.getBounds,
        h = Y.extend,
        m = Y.updateClasses,
        k = Y.defer,
        G = ["left", "top", "right", "bottom"];
    x.modules.push({
        position: function(t) {
            var e = this,
                o = t.top,
                i = t.left,
                n = t.targetAttachment;
            if (!this.options.constraints) return !0;
            var r = this.cache("element-bounds", function() {
                    return a(e.element)
                }),
                s = r.height,
                f = r.width;
            if (0 === f && 0 === s && "undefined" != typeof this.lastSize) {
                var l = this.lastSize;
                f = l.width, s = l.height
            }
            var d = this.cache("target-bounds", function() {
                    return e.getTargetBounds()
                }),
                p = d.height,
                u = d.width,
                c = [this.getClass("pinned"), this.getClass("out-of-bounds")];
            this.options.constraints.forEach(function(t) {
                var e = t.outOfBoundsClass,
                    o = t.pinnedClass;
                e && c.push(e), o && c.push(o)
            }), c.forEach(function(t) {
                ["left", "top", "right", "bottom"].forEach(function(e) {
                    c.push(t + "-" + e)
                })
            });
            var g = [],
                v = h({}, n),
                y = h({}, this.attachment);
            return this.options.constraints.forEach(function(t) {
                var r = t.to,
                    a = t.attachment,
                    l = t.pin;
                "undefined" == typeof a && (a = "");
                var h = void 0,
                    d = void 0;
                if (a.indexOf(" ") >= 0) {
                    var c = a.split(" "),
                        m = z(c, 2);
                    d = m[0], h = m[1]
                } else h = d = a;
                var b = O(e, r);
                "target" !== d && "both" !== d || (o < b[1] && "top" === v.top && (o += p, v.top = "bottom"), o + s > b[3] && "bottom" === v.top && (o -= p, v.top = "top")), "together" === d && ("top" === v.top && ("bottom" === y.top && o < b[1] ? (o += p, v.top = "bottom", o += s, y.top = "top") : "top" === y.top && o + s > b[3] && o - (s - p) >= b[1] && (o -= s - p, v.top = "bottom", y.top = "bottom")), "bottom" === v.top && ("top" === y.top && o + s > b[3] ? (o -= p, v.top = "top", o -= s, y.top = "bottom") : "bottom" === y.top && o < b[1] && o + (2 * s - p) <= b[3] && (o += s - p, v.top = "top", y.top = "top")), "middle" === v.top && (o + s > b[3] && "top" === y.top ? (o -= s, y.top = "bottom") : o < b[1] && "bottom" === y.top && (o += s, y.top = "top"))), "target" !== h && "both" !== h || (i < b[0] && "left" === v.left && (i += u, v.left = "right"), i + f > b[2] && "right" === v.left && (i -= u, v.left = "left")), "together" === h && (i < b[0] && "left" === v.left ? "right" === y.left ? (i += u, v.left = "right", i += f, y.left = "left") : "left" === y.left && (i += u, v.left = "right", i -= f, y.left = "right") : i + f > b[2] && "right" === v.left ? "left" === y.left ? (i -= u, v.left = "left", i -= f, y.left = "right") : "right" === y.left && (i -= u, v.left = "left", i += f, y.left = "left") : "center" === v.left && (i + f > b[2] && "left" === y.left ? (i -= f, y.left = "right") : i < b[0] && "right" === y.left && (i += f, y.left = "left"))), "element" !== d && "both" !== d || (o < b[1] && "bottom" === y.top && (o += s, y.top = "top"), o + s > b[3] && "top" === y.top && (o -= s, y.top = "bottom")), "element" !== h && "both" !== h || (i < b[0] && ("right" === y.left ? (i += f, y.left = "left") : "center" === y.left && (i += f / 2, y.left = "left")), i + f > b[2] && ("left" === y.left ? (i -= f, y.left = "right") : "center" === y.left && (i -= f / 2, y.left = "right"))), "string" == typeof l ? l = l.split(",").map(function(t) {
                    return t.trim()
                }) : l === !0 && (l = ["top", "left", "right", "bottom"]), l = l || [];
                var w = [],
                    C = [];
                o < b[1] && (l.indexOf("top") >= 0 ? (o = b[1], w.push("top")) : C.push("top")), o + s > b[3] && (l.indexOf("bottom") >= 0 ? (o = b[3] - s, w.push("bottom")) : C.push("bottom")), i < b[0] && (l.indexOf("left") >= 0 ? (i = b[0], w.push("left")) : C.push("left")), i + f > b[2] && (l.indexOf("right") >= 0 ? (i = b[2] - f, w.push("right")) : C.push("right")), w.length && ! function() {
                    var t = void 0;
                    t = "undefined" != typeof e.options.pinnedClass ? e.options.pinnedClass : e.getClass("pinned"), g.push(t), w.forEach(function(e) {
                        g.push(t + "-" + e)
                    })
                }(), C.length && ! function() {
                    var t = void 0;
                    t = "undefined" != typeof e.options.outOfBoundsClass ? e.options.outOfBoundsClass : e.getClass("out-of-bounds"), g.push(t), C.forEach(function(e) {
                        g.push(t + "-" + e)
                    })
                }(), (w.indexOf("left") >= 0 || w.indexOf("right") >= 0) && (y.left = v.left = !1), (w.indexOf("top") >= 0 || w.indexOf("bottom") >= 0) && (y.top = v.top = !1), v.top === n.top && v.left === n.left && y.top === e.attachment.top && y.left === e.attachment.left || (e.updateAttachClasses(y, v), e.trigger("update", {
                    attachment: y,
                    targetAttachment: v
                }))
            }), k(function() {
                e.options.addTargetClasses !== !1 && m(e.target, g, c), m(e.element, g, c)
            }), {
                top: o,
                left: i
            }
        }
    });
    var Y = x.Utils,
        a = Y.getBounds,
        m = Y.updateClasses,
        k = Y.defer;
    x.modules.push({
        position: function(t) {
            var e = this,
                o = t.top,
                i = t.left,
                n = this.cache("element-bounds", function() {
                    return a(e.element)
                }),
                r = n.height,
                s = n.width,
                f = this.getTargetBounds(),
                l = o + r,
                h = i + s,
                d = [];
            o <= f.bottom && l >= f.top && ["left", "right"].forEach(function(t) {
                var e = f[t];
                e !== i && e !== h || d.push(t)
            }), i <= f.right && h >= f.left && ["top", "bottom"].forEach(function(t) {
                var e = f[t];
                e !== o && e !== l || d.push(t)
            });
            var p = [],
                u = [],
                c = ["left", "top", "right", "bottom"];
            return p.push(this.getClass("abutted")), c.forEach(function(t) {
                p.push(e.getClass("abutted") + "-" + t)
            }), d.length && u.push(this.getClass("abutted")), d.forEach(function(t) {
                u.push(e.getClass("abutted") + "-" + t)
            }), k(function() {
                e.options.addTargetClasses !== !1 && m(e.target, u, p), m(e.element, u, p)
            }), !0
        }
    });
    var z = function() {
        function t(t, e) {
            var o = [],
                i = !0,
                n = !1,
                r = void 0;
            try {
                for (var s, a = t[Symbol.iterator](); !(i = (s = a.next()).done) && (o.push(s.value), !e || o.length !== e); i = !0);
            } catch (f) {
                n = !0, r = f
            } finally {
                try {
                    !i && a["return"] && a["return"]()
                } finally {
                    if (n) throw r
                }
            }
            return o
        }
        return function(e, o) {
            if (Array.isArray(e)) return e;
            if (Symbol.iterator in Object(e)) return t(e, o);
            throw new TypeError("Invalid attempt to destructure non-iterable instance")
        }
    }();
    return x.modules.push({
        position: function(t) {
            var e = t.top,
                o = t.left;
            if (this.options.shift) {
                var i = this.options.shift;
                "function" == typeof this.options.shift && (i = this.options.shift.call(this, {
                    top: e,
                    left: o
                }));
                var n = void 0,
                    r = void 0;
                if ("string" == typeof i) {
                    i = i.split(" "), i[1] = i[1] || i[0];
                    var s = i,
                        a = z(s, 2);
                    n = a[0], r = a[1], n = parseFloat(n, 10), r = parseFloat(r, 10)
                } else n = i.top, r = i.left;
                return e += n, o += r, {
                    top: e,
                    left: o
                }
            }
        }
    }), $
});
(function() {
    var container, button, menu, links, subMenus;
    container = document.getElementById('site-navigation');
    if (!container) {
        return;
    }
    button = container.getElementsByTagName('button')[0];
    if ('undefined' === typeof button) {
        return;
    }
    menu = container.getElementsByTagName('ul')[0];
    if ('undefined' === typeof menu) {
        button.style.display = 'none';
        return;
    }
    menu.setAttribute('aria-expanded', 'false');
    if (-1 === menu.className.indexOf('nav-menu')) {
        menu.className += ' nav-menu';
    }
    button.onclick = function() {
        if (-1 !== container.className.indexOf('toggled')) {
            container.className = container.className.replace(' toggled', '');
            button.setAttribute('aria-expanded', 'false');
            menu.setAttribute('aria-expanded', 'false');
        } else {
            container.className += ' toggled';
            button.setAttribute('aria-expanded', 'true');
            menu.setAttribute('aria-expanded', 'true');
        }
    };
    links = menu.getElementsByTagName('a');
    subMenus = menu.getElementsByTagName('ul');
    for (var i = 0, len = subMenus.length; i < len; i++) {
        subMenus[i].parentNode.setAttribute('aria-haspopup', 'true');
    }
    for (i = 0, len = links.length; i < len; i++) {
        links[i].addEventListener('focus', toggleFocus, true);
        links[i].addEventListener('blur', toggleFocus, true);
    }

    function toggleFocus() {
        var self = this;
        while (-1 === self.className.indexOf('nav-menu')) {
            if ('li' === self.tagName.toLowerCase()) {
                if (-1 !== self.className.indexOf('focus')) {
                    self.className = self.className.replace(' focus', '');
                } else {
                    self.className += ' focus';
                }
            }
            self = self.parentElement;
        }
    }
})();
(function() {
    var is_webkit = navigator.userAgent.toLowerCase().indexOf('webkit') > -1,
        is_opera = navigator.userAgent.toLowerCase().indexOf('opera') > -1,
        is_ie = navigator.userAgent.toLowerCase().indexOf('msie') > -1;
    if ((is_webkit || is_opera || is_ie) && document.getElementById && window.addEventListener) {
        window.addEventListener('hashchange', function() {
            var id = location.hash.substring(1),
                element;
            if (!(/^[A-z0-9_-]+$/.test(id))) {
                return;
            }
            element = document.getElementById(id);
            if (element) {
                if (!(/^(?:a|select|input|button|textarea)$/i.test(element.tagName))) {
                    element.tabIndex = -1;
                }
                element.focus();
            }
        }, false);
    }
})();
jQuery(document).ready(function($) {
    $('[data-toggle="tooltip"]').tooltip()
});
/*!
 * Masonry PACKAGED v4.1.1
 * Cascading grid layout library
 * http://masonry.desandro.com
 * MIT License
 * by David DeSandro
 */
! function(t, e) {
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function(i) {
        return e(t, i)
    }) : "object" == typeof module && module.exports ? module.exports = e(t, require("jquery")) : t.jQueryBridget = e(t, t.jQuery)
}(window, function(t, e) {
    "use strict";

    function i(i, r, a) {
        function h(t, e, n) {
            var o, r = "$()." + i + '("' + e + '")';
            return t.each(function(t, h) {
                var u = a.data(h, i);
                if (!u) return void s(i + " not initialized. Cannot call methods, i.e. " + r);
                var d = u[e];
                if (!d || "_" == e.charAt(0)) return void s(r + " is not a valid method");
                var l = d.apply(u, n);
                o = void 0 === o ? l : o
            }), void 0 !== o ? o : t
        }

        function u(t, e) {
            t.each(function(t, n) {
                var o = a.data(n, i);
                o ? (o.option(e), o._init()) : (o = new r(n, e), a.data(n, i, o))
            })
        }
        a = a || e || t.jQuery, a && (r.prototype.option || (r.prototype.option = function(t) {
            a.isPlainObject(t) && (this.options = a.extend(!0, this.options, t))
        }), a.fn[i] = function(t) {
            if ("string" == typeof t) {
                var e = o.call(arguments, 1);
                return h(this, t, e)
            }
            return u(this, t), this
        }, n(a))
    }

    function n(t) {
        !t || t && t.bridget || (t.bridget = i)
    }
    var o = Array.prototype.slice,
        r = t.console,
        s = "undefined" == typeof r ? function() {} : function(t) {
            r.error(t)
        };
    return n(e || t.jQuery), i
}),
function(t, e) {
    "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}("undefined" != typeof window ? window : this, function() {
    function t() {}
    var e = t.prototype;
    return e.on = function(t, e) {
        if (t && e) {
            var i = this._events = this._events || {},
                n = i[t] = i[t] || [];
            return -1 == n.indexOf(e) && n.push(e), this
        }
    }, e.once = function(t, e) {
        if (t && e) {
            this.on(t, e);
            var i = this._onceEvents = this._onceEvents || {},
                n = i[t] = i[t] || {};
            return n[e] = !0, this
        }
    }, e.off = function(t, e) {
        var i = this._events && this._events[t];
        if (i && i.length) {
            var n = i.indexOf(e);
            return -1 != n && i.splice(n, 1), this
        }
    }, e.emitEvent = function(t, e) {
        var i = this._events && this._events[t];
        if (i && i.length) {
            var n = 0,
                o = i[n];
            e = e || [];
            for (var r = this._onceEvents && this._onceEvents[t]; o;) {
                var s = r && r[o];
                s && (this.off(t, o), delete r[o]), o.apply(this, e), n += s ? 0 : 1, o = i[n]
            }
            return this
        }
    }, t
}),
function(t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define("get-size/get-size", [], function() {
        return e()
    }) : "object" == typeof module && module.exports ? module.exports = e() : t.getSize = e()
}(window, function() {
    "use strict";

    function t(t) {
        var e = parseFloat(t),
            i = -1 == t.indexOf("%") && !isNaN(e);
        return i && e
    }

    function e() {}

    function i() {
        for (var t = {
                width: 0,
                height: 0,
                innerWidth: 0,
                innerHeight: 0,
                outerWidth: 0,
                outerHeight: 0
            }, e = 0; u > e; e++) {
            var i = h[e];
            t[i] = 0
        }
        return t
    }

    function n(t) {
        var e = getComputedStyle(t);
        return e || a("Style returned " + e + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), e
    }

    function o() {
        if (!d) {
            d = !0;
            var e = document.createElement("div");
            e.style.width = "200px", e.style.padding = "1px 2px 3px 4px", e.style.borderStyle = "solid", e.style.borderWidth = "1px 2px 3px 4px", e.style.boxSizing = "border-box";
            var i = document.body || document.documentElement;
            i.appendChild(e);
            var o = n(e);
            r.isBoxSizeOuter = s = 200 == t(o.width), i.removeChild(e)
        }
    }

    function r(e) {
        if (o(), "string" == typeof e && (e = document.querySelector(e)), e && "object" == typeof e && e.nodeType) {
            var r = n(e);
            if ("none" == r.display) return i();
            var a = {};
            a.width = e.offsetWidth, a.height = e.offsetHeight;
            for (var d = a.isBorderBox = "border-box" == r.boxSizing, l = 0; u > l; l++) {
                var c = h[l],
                    f = r[c],
                    m = parseFloat(f);
                a[c] = isNaN(m) ? 0 : m
            }
            var p = a.paddingLeft + a.paddingRight,
                g = a.paddingTop + a.paddingBottom,
                y = a.marginLeft + a.marginRight,
                v = a.marginTop + a.marginBottom,
                _ = a.borderLeftWidth + a.borderRightWidth,
                E = a.borderTopWidth + a.borderBottomWidth,
                z = d && s,
                b = t(r.width);
            b !== !1 && (a.width = b + (z ? 0 : p + _));
            var x = t(r.height);
            return x !== !1 && (a.height = x + (z ? 0 : g + E)), a.innerWidth = a.width - (p + _), a.innerHeight = a.height - (g + E), a.outerWidth = a.width + y, a.outerHeight = a.height + v, a
        }
    }
    var s, a = "undefined" == typeof console ? e : function(t) {
            console.error(t)
        },
        h = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
        u = h.length,
        d = !1;
    return r
}),
function(t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", e) : "object" == typeof module && module.exports ? module.exports = e() : t.matchesSelector = e()
}(window, function() {
    "use strict";
    var t = function() {
        var t = Element.prototype;
        if (t.matches) return "matches";
        if (t.matchesSelector) return "matchesSelector";
        for (var e = ["webkit", "moz", "ms", "o"], i = 0; i < e.length; i++) {
            var n = e[i],
                o = n + "MatchesSelector";
            if (t[o]) return o
        }
    }();
    return function(e, i) {
        return e[t](i)
    }
}),
function(t, e) {
    "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function(i) {
        return e(t, i)
    }) : "object" == typeof module && module.exports ? module.exports = e(t, require("desandro-matches-selector")) : t.fizzyUIUtils = e(t, t.matchesSelector)
}(window, function(t, e) {
    var i = {};
    i.extend = function(t, e) {
        for (var i in e) t[i] = e[i];
        return t
    }, i.modulo = function(t, e) {
        return (t % e + e) % e
    }, i.makeArray = function(t) {
        var e = [];
        if (Array.isArray(t)) e = t;
        else if (t && "number" == typeof t.length)
            for (var i = 0; i < t.length; i++) e.push(t[i]);
        else e.push(t);
        return e
    }, i.removeFrom = function(t, e) {
        var i = t.indexOf(e); - 1 != i && t.splice(i, 1)
    }, i.getParent = function(t, i) {
        for (; t != document.body;)
            if (t = t.parentNode, e(t, i)) return t
    }, i.getQueryElement = function(t) {
        return "string" == typeof t ? document.querySelector(t) : t
    }, i.handleEvent = function(t) {
        var e = "on" + t.type;
        this[e] && this[e](t)
    }, i.filterFindElements = function(t, n) {
        t = i.makeArray(t);
        var o = [];
        return t.forEach(function(t) {
            if (t instanceof HTMLElement) {
                if (!n) return void o.push(t);
                e(t, n) && o.push(t);
                for (var i = t.querySelectorAll(n), r = 0; r < i.length; r++) o.push(i[r])
            }
        }), o
    }, i.debounceMethod = function(t, e, i) {
        var n = t.prototype[e],
            o = e + "Timeout";
        t.prototype[e] = function() {
            var t = this[o];
            t && clearTimeout(t);
            var e = arguments,
                r = this;
            this[o] = setTimeout(function() {
                n.apply(r, e), delete r[o]
            }, i || 100)
        }
    }, i.docReady = function(t) {
        var e = document.readyState;
        "complete" == e || "interactive" == e ? t() : document.addEventListener("DOMContentLoaded", t)
    }, i.toDashed = function(t) {
        return t.replace(/(.)([A-Z])/g, function(t, e, i) {
            return e + "-" + i
        }).toLowerCase()
    };
    var n = t.console;
    return i.htmlInit = function(e, o) {
        i.docReady(function() {
            var r = i.toDashed(o),
                s = "data-" + r,
                a = document.querySelectorAll("[" + s + "]"),
                h = document.querySelectorAll(".js-" + r),
                u = i.makeArray(a).concat(i.makeArray(h)),
                d = s + "-options",
                l = t.jQuery;
            u.forEach(function(t) {
                var i, r = t.getAttribute(s) || t.getAttribute(d);
                try {
                    i = r && JSON.parse(r)
                } catch (a) {
                    return void(n && n.error("Error parsing " + s + " on " + t.className + ": " + a))
                }
                var h = new e(t, i);
                l && l.data(t, o, h)
            })
        })
    }, i
}),
function(t, e) {
    "function" == typeof define && define.amd ? define("outlayer/item", ["ev-emitter/ev-emitter", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("ev-emitter"), require("get-size")) : (t.Outlayer = {}, t.Outlayer.Item = e(t.EvEmitter, t.getSize))
}(window, function(t, e) {
    "use strict";

    function i(t) {
        for (var e in t) return !1;
        return e = null, !0
    }

    function n(t, e) {
        t && (this.element = t, this.layout = e, this.position = {
            x: 0,
            y: 0
        }, this._create())
    }

    function o(t) {
        return t.replace(/([A-Z])/g, function(t) {
            return "-" + t.toLowerCase()
        })
    }
    var r = document.documentElement.style,
        s = "string" == typeof r.transition ? "transition" : "WebkitTransition",
        a = "string" == typeof r.transform ? "transform" : "WebkitTransform",
        h = {
            WebkitTransition: "webkitTransitionEnd",
            transition: "transitionend"
        }[s],
        u = {
            transform: a,
            transition: s,
            transitionDuration: s + "Duration",
            transitionProperty: s + "Property",
            transitionDelay: s + "Delay"
        },
        d = n.prototype = Object.create(t.prototype);
    d.constructor = n, d._create = function() {
        this._transn = {
            ingProperties: {},
            clean: {},
            onEnd: {}
        }, this.css({
            position: "absolute"
        })
    }, d.handleEvent = function(t) {
        var e = "on" + t.type;
        this[e] && this[e](t)
    }, d.getSize = function() {
        this.size = e(this.element)
    }, d.css = function(t) {
        var e = this.element.style;
        for (var i in t) {
            var n = u[i] || i;
            e[n] = t[i]
        }
    }, d.getPosition = function() {
        var t = getComputedStyle(this.element),
            e = this.layout._getOption("originLeft"),
            i = this.layout._getOption("originTop"),
            n = t[e ? "left" : "right"],
            o = t[i ? "top" : "bottom"],
            r = this.layout.size,
            s = -1 != n.indexOf("%") ? parseFloat(n) / 100 * r.width : parseInt(n, 10),
            a = -1 != o.indexOf("%") ? parseFloat(o) / 100 * r.height : parseInt(o, 10);
        s = isNaN(s) ? 0 : s, a = isNaN(a) ? 0 : a, s -= e ? r.paddingLeft : r.paddingRight, a -= i ? r.paddingTop : r.paddingBottom, this.position.x = s, this.position.y = a
    }, d.layoutPosition = function() {
        var t = this.layout.size,
            e = {},
            i = this.layout._getOption("originLeft"),
            n = this.layout._getOption("originTop"),
            o = i ? "paddingLeft" : "paddingRight",
            r = i ? "left" : "right",
            s = i ? "right" : "left",
            a = this.position.x + t[o];
        e[r] = this.getXValue(a), e[s] = "";
        var h = n ? "paddingTop" : "paddingBottom",
            u = n ? "top" : "bottom",
            d = n ? "bottom" : "top",
            l = this.position.y + t[h];
        e[u] = this.getYValue(l), e[d] = "", this.css(e), this.emitEvent("layout", [this])
    }, d.getXValue = function(t) {
        var e = this.layout._getOption("horizontal");
        return this.layout.options.percentPosition && !e ? t / this.layout.size.width * 100 + "%" : t + "px"
    }, d.getYValue = function(t) {
        var e = this.layout._getOption("horizontal");
        return this.layout.options.percentPosition && e ? t / this.layout.size.height * 100 + "%" : t + "px"
    }, d._transitionTo = function(t, e) {
        this.getPosition();
        var i = this.position.x,
            n = this.position.y,
            o = parseInt(t, 10),
            r = parseInt(e, 10),
            s = o === this.position.x && r === this.position.y;
        if (this.setPosition(t, e), s && !this.isTransitioning) return void this.layoutPosition();
        var a = t - i,
            h = e - n,
            u = {};
        u.transform = this.getTranslate(a, h), this.transition({
            to: u,
            onTransitionEnd: {
                transform: this.layoutPosition
            },
            isCleaning: !0
        })
    }, d.getTranslate = function(t, e) {
        var i = this.layout._getOption("originLeft"),
            n = this.layout._getOption("originTop");
        return t = i ? t : -t, e = n ? e : -e, "translate3d(" + t + "px, " + e + "px, 0)"
    }, d.goTo = function(t, e) {
        this.setPosition(t, e), this.layoutPosition()
    }, d.moveTo = d._transitionTo, d.setPosition = function(t, e) {
        this.position.x = parseInt(t, 10), this.position.y = parseInt(e, 10)
    }, d._nonTransition = function(t) {
        this.css(t.to), t.isCleaning && this._removeStyles(t.to);
        for (var e in t.onTransitionEnd) t.onTransitionEnd[e].call(this)
    }, d.transition = function(t) {
        if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(t);
        var e = this._transn;
        for (var i in t.onTransitionEnd) e.onEnd[i] = t.onTransitionEnd[i];
        for (i in t.to) e.ingProperties[i] = !0, t.isCleaning && (e.clean[i] = !0);
        if (t.from) {
            this.css(t.from);
            var n = this.element.offsetHeight;
            n = null
        }
        this.enableTransition(t.to), this.css(t.to), this.isTransitioning = !0
    };
    var l = "opacity," + o(a);
    d.enableTransition = function() {
        if (!this.isTransitioning) {
            var t = this.layout.options.transitionDuration;
            t = "number" == typeof t ? t + "ms" : t, this.css({
                transitionProperty: l,
                transitionDuration: t,
                transitionDelay: this.staggerDelay || 0
            }), this.element.addEventListener(h, this, !1)
        }
    }, d.onwebkitTransitionEnd = function(t) {
        this.ontransitionend(t)
    }, d.onotransitionend = function(t) {
        this.ontransitionend(t)
    };
    var c = {
        "-webkit-transform": "transform"
    };
    d.ontransitionend = function(t) {
        if (t.target === this.element) {
            var e = this._transn,
                n = c[t.propertyName] || t.propertyName;
            if (delete e.ingProperties[n], i(e.ingProperties) && this.disableTransition(), n in e.clean && (this.element.style[t.propertyName] = "", delete e.clean[n]), n in e.onEnd) {
                var o = e.onEnd[n];
                o.call(this), delete e.onEnd[n]
            }
            this.emitEvent("transitionEnd", [this])
        }
    }, d.disableTransition = function() {
        this.removeTransitionStyles(), this.element.removeEventListener(h, this, !1), this.isTransitioning = !1
    }, d._removeStyles = function(t) {
        var e = {};
        for (var i in t) e[i] = "";
        this.css(e)
    };
    var f = {
        transitionProperty: "",
        transitionDuration: "",
        transitionDelay: ""
    };
    return d.removeTransitionStyles = function() {
        this.css(f)
    }, d.stagger = function(t) {
        t = isNaN(t) ? 0 : t, this.staggerDelay = t + "ms"
    }, d.removeElem = function() {
        this.element.parentNode.removeChild(this.element), this.css({
            display: ""
        }), this.emitEvent("remove", [this])
    }, d.remove = function() {
        return s && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function() {
            this.removeElem()
        }), void this.hide()) : void this.removeElem()
    }, d.reveal = function() {
        delete this.isHidden, this.css({
            display: ""
        });
        var t = this.layout.options,
            e = {},
            i = this.getHideRevealTransitionEndProperty("visibleStyle");
        e[i] = this.onRevealTransitionEnd, this.transition({
            from: t.hiddenStyle,
            to: t.visibleStyle,
            isCleaning: !0,
            onTransitionEnd: e
        })
    }, d.onRevealTransitionEnd = function() {
        this.isHidden || this.emitEvent("reveal")
    }, d.getHideRevealTransitionEndProperty = function(t) {
        var e = this.layout.options[t];
        if (e.opacity) return "opacity";
        for (var i in e) return i
    }, d.hide = function() {
        this.isHidden = !0, this.css({
            display: ""
        });
        var t = this.layout.options,
            e = {},
            i = this.getHideRevealTransitionEndProperty("hiddenStyle");
        e[i] = this.onHideTransitionEnd, this.transition({
            from: t.visibleStyle,
            to: t.hiddenStyle,
            isCleaning: !0,
            onTransitionEnd: e
        })
    }, d.onHideTransitionEnd = function() {
        this.isHidden && (this.css({
            display: "none"
        }), this.emitEvent("hide"))
    }, d.destroy = function() {
        this.css({
            position: "",
            left: "",
            right: "",
            top: "",
            bottom: "",
            transition: "",
            transform: ""
        })
    }, n
}),
function(t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define("outlayer/outlayer", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function(i, n, o, r) {
        return e(t, i, n, o, r)
    }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : t.Outlayer = e(t, t.EvEmitter, t.getSize, t.fizzyUIUtils, t.Outlayer.Item)
}(window, function(t, e, i, n, o) {
    "use strict";

    function r(t, e) {
        var i = n.getQueryElement(t);
        if (!i) return void(h && h.error("Bad element for " + this.constructor.namespace + ": " + (i || t)));
        this.element = i, u && (this.$element = u(this.element)), this.options = n.extend({}, this.constructor.defaults), this.option(e);
        var o = ++l;
        this.element.outlayerGUID = o, c[o] = this, this._create();
        var r = this._getOption("initLayout");
        r && this.layout()
    }

    function s(t) {
        function e() {
            t.apply(this, arguments)
        }
        return e.prototype = Object.create(t.prototype), e.prototype.constructor = e, e
    }

    function a(t) {
        if ("number" == typeof t) return t;
        var e = t.match(/(^\d*\.?\d*)(\w*)/),
            i = e && e[1],
            n = e && e[2];
        if (!i.length) return 0;
        i = parseFloat(i);
        var o = m[n] || 1;
        return i * o
    }
    var h = t.console,
        u = t.jQuery,
        d = function() {},
        l = 0,
        c = {};
    r.namespace = "outlayer", r.Item = o, r.defaults = {
        containerStyle: {
            position: "relative"
        },
        initLayout: !0,
        originLeft: !0,
        originTop: !0,
        resize: !0,
        resizeContainer: !0,
        transitionDuration: "0.4s",
        hiddenStyle: {
            opacity: 0,
            transform: "scale(0.001)"
        },
        visibleStyle: {
            opacity: 1,
            transform: "scale(1)"
        }
    };
    var f = r.prototype;
    n.extend(f, e.prototype), f.option = function(t) {
        n.extend(this.options, t)
    }, f._getOption = function(t) {
        var e = this.constructor.compatOptions[t];
        return e && void 0 !== this.options[e] ? this.options[e] : this.options[t]
    }, r.compatOptions = {
        initLayout: "isInitLayout",
        horizontal: "isHorizontal",
        layoutInstant: "isLayoutInstant",
        originLeft: "isOriginLeft",
        originTop: "isOriginTop",
        resize: "isResizeBound",
        resizeContainer: "isResizingContainer"
    }, f._create = function() {
        this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), n.extend(this.element.style, this.options.containerStyle);
        var t = this._getOption("resize");
        t && this.bindResize()
    }, f.reloadItems = function() {
        this.items = this._itemize(this.element.children)
    }, f._itemize = function(t) {
        for (var e = this._filterFindItemElements(t), i = this.constructor.Item, n = [], o = 0; o < e.length; o++) {
            var r = e[o],
                s = new i(r, this);
            n.push(s)
        }
        return n
    }, f._filterFindItemElements = function(t) {
        return n.filterFindElements(t, this.options.itemSelector)
    }, f.getItemElements = function() {
        return this.items.map(function(t) {
            return t.element
        })
    }, f.layout = function() {
        this._resetLayout(), this._manageStamps();
        var t = this._getOption("layoutInstant"),
            e = void 0 !== t ? t : !this._isLayoutInited;
        this.layoutItems(this.items, e), this._isLayoutInited = !0
    }, f._init = f.layout, f._resetLayout = function() {
        this.getSize()
    }, f.getSize = function() {
        this.size = i(this.element)
    }, f._getMeasurement = function(t, e) {
        var n, o = this.options[t];
        o ? ("string" == typeof o ? n = this.element.querySelector(o) : o instanceof HTMLElement && (n = o), this[t] = n ? i(n)[e] : o) : this[t] = 0
    }, f.layoutItems = function(t, e) {
        t = this._getItemsForLayout(t), this._layoutItems(t, e), this._postLayout()
    }, f._getItemsForLayout = function(t) {
        return t.filter(function(t) {
            return !t.isIgnored
        })
    }, f._layoutItems = function(t, e) {
        if (this._emitCompleteOnItems("layout", t), t && t.length) {
            var i = [];
            t.forEach(function(t) {
                var n = this._getItemLayoutPosition(t);
                n.item = t, n.isInstant = e || t.isLayoutInstant, i.push(n)
            }, this), this._processLayoutQueue(i)
        }
    }, f._getItemLayoutPosition = function() {
        return {
            x: 0,
            y: 0
        }
    }, f._processLayoutQueue = function(t) {
        this.updateStagger(), t.forEach(function(t, e) {
            this._positionItem(t.item, t.x, t.y, t.isInstant, e)
        }, this)
    }, f.updateStagger = function() {
        var t = this.options.stagger;
        return null === t || void 0 === t ? void(this.stagger = 0) : (this.stagger = a(t), this.stagger)
    }, f._positionItem = function(t, e, i, n, o) {
        n ? t.goTo(e, i) : (t.stagger(o * this.stagger), t.moveTo(e, i))
    }, f._postLayout = function() {
        this.resizeContainer()
    }, f.resizeContainer = function() {
        var t = this._getOption("resizeContainer");
        if (t) {
            var e = this._getContainerSize();
            e && (this._setContainerMeasure(e.width, !0), this._setContainerMeasure(e.height, !1))
        }
    }, f._getContainerSize = d, f._setContainerMeasure = function(t, e) {
        if (void 0 !== t) {
            var i = this.size;
            i.isBorderBox && (t += e ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth), t = Math.max(t, 0), this.element.style[e ? "width" : "height"] = t + "px"
        }
    }, f._emitCompleteOnItems = function(t, e) {
        function i() {
            o.dispatchEvent(t + "Complete", null, [e])
        }

        function n() {
            s++, s == r && i()
        }
        var o = this,
            r = e.length;
        if (!e || !r) return void i();
        var s = 0;
        e.forEach(function(e) {
            e.once(t, n)
        })
    }, f.dispatchEvent = function(t, e, i) {
        var n = e ? [e].concat(i) : i;
        if (this.emitEvent(t, n), u)
            if (this.$element = this.$element || u(this.element), e) {
                var o = u.Event(e);
                o.type = t, this.$element.trigger(o, i)
            } else this.$element.trigger(t, i)
    }, f.ignore = function(t) {
        var e = this.getItem(t);
        e && (e.isIgnored = !0)
    }, f.unignore = function(t) {
        var e = this.getItem(t);
        e && delete e.isIgnored
    }, f.stamp = function(t) {
        t = this._find(t), t && (this.stamps = this.stamps.concat(t), t.forEach(this.ignore, this))
    }, f.unstamp = function(t) {
        t = this._find(t), t && t.forEach(function(t) {
            n.removeFrom(this.stamps, t), this.unignore(t)
        }, this)
    }, f._find = function(t) {
        return t ? ("string" == typeof t && (t = this.element.querySelectorAll(t)), t = n.makeArray(t)) : void 0
    }, f._manageStamps = function() {
        this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
    }, f._getBoundingRect = function() {
        var t = this.element.getBoundingClientRect(),
            e = this.size;
        this._boundingRect = {
            left: t.left + e.paddingLeft + e.borderLeftWidth,
            top: t.top + e.paddingTop + e.borderTopWidth,
            right: t.right - (e.paddingRight + e.borderRightWidth),
            bottom: t.bottom - (e.paddingBottom + e.borderBottomWidth)
        }
    }, f._manageStamp = d, f._getElementOffset = function(t) {
        var e = t.getBoundingClientRect(),
            n = this._boundingRect,
            o = i(t),
            r = {
                left: e.left - n.left - o.marginLeft,
                top: e.top - n.top - o.marginTop,
                right: n.right - e.right - o.marginRight,
                bottom: n.bottom - e.bottom - o.marginBottom
            };
        return r
    }, f.handleEvent = n.handleEvent, f.bindResize = function() {
        t.addEventListener("resize", this), this.isResizeBound = !0
    }, f.unbindResize = function() {
        t.removeEventListener("resize", this), this.isResizeBound = !1
    }, f.onresize = function() {
        this.resize()
    }, n.debounceMethod(r, "onresize", 100), f.resize = function() {
        this.isResizeBound && this.needsResizeLayout() && this.layout()
    }, f.needsResizeLayout = function() {
        var t = i(this.element),
            e = this.size && t;
        return e && t.innerWidth !== this.size.innerWidth
    }, f.addItems = function(t) {
        var e = this._itemize(t);
        return e.length && (this.items = this.items.concat(e)), e
    }, f.appended = function(t) {
        var e = this.addItems(t);
        e.length && (this.layoutItems(e, !0), this.reveal(e))
    }, f.prepended = function(t) {
        var e = this._itemize(t);
        if (e.length) {
            var i = this.items.slice(0);
            this.items = e.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(e, !0), this.reveal(e), this.layoutItems(i)
        }
    }, f.reveal = function(t) {
        if (this._emitCompleteOnItems("reveal", t), t && t.length) {
            var e = this.updateStagger();
            t.forEach(function(t, i) {
                t.stagger(i * e), t.reveal()
            })
        }
    }, f.hide = function(t) {
        if (this._emitCompleteOnItems("hide", t), t && t.length) {
            var e = this.updateStagger();
            t.forEach(function(t, i) {
                t.stagger(i * e), t.hide()
            })
        }
    }, f.revealItemElements = function(t) {
        var e = this.getItems(t);
        this.reveal(e)
    }, f.hideItemElements = function(t) {
        var e = this.getItems(t);
        this.hide(e)
    }, f.getItem = function(t) {
        for (var e = 0; e < this.items.length; e++) {
            var i = this.items[e];
            if (i.element == t) return i
        }
    }, f.getItems = function(t) {
        t = n.makeArray(t);
        var e = [];
        return t.forEach(function(t) {
            var i = this.getItem(t);
            i && e.push(i)
        }, this), e
    }, f.remove = function(t) {
        var e = this.getItems(t);
        this._emitCompleteOnItems("remove", e), e && e.length && e.forEach(function(t) {
            t.remove(), n.removeFrom(this.items, t)
        }, this)
    }, f.destroy = function() {
        var t = this.element.style;
        t.height = "", t.position = "", t.width = "", this.items.forEach(function(t) {
            t.destroy()
        }), this.unbindResize();
        var e = this.element.outlayerGUID;
        delete c[e], delete this.element.outlayerGUID, u && u.removeData(this.element, this.constructor.namespace)
    }, r.data = function(t) {
        t = n.getQueryElement(t);
        var e = t && t.outlayerGUID;
        return e && c[e]
    }, r.create = function(t, e) {
        var i = s(r);
        return i.defaults = n.extend({}, r.defaults), n.extend(i.defaults, e), i.compatOptions = n.extend({}, r.compatOptions), i.namespace = t, i.data = r.data, i.Item = s(o), n.htmlInit(i, t), u && u.bridget && u.bridget(t, i), i
    };
    var m = {
        ms: 1,
        s: 1e3
    };
    return r.Item = o, r
}),
function(t, e) {
    "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size"], e) : "object" == typeof module && module.exports ? module.exports = e(require("outlayer"), require("get-size")) : t.Masonry = e(t.Outlayer, t.getSize)
}(window, function(t, e) {
    var i = t.create("masonry");
    return i.compatOptions.fitWidth = "isFitWidth", i.prototype._resetLayout = function() {
        this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];
        for (var t = 0; t < this.cols; t++) this.colYs.push(0);
        this.maxY = 0
    }, i.prototype.measureColumns = function() {
        if (this.getContainerWidth(), !this.columnWidth) {
            var t = this.items[0],
                i = t && t.element;
            this.columnWidth = i && e(i).outerWidth || this.containerWidth
        }
        var n = this.columnWidth += this.gutter,
            o = this.containerWidth + this.gutter,
            r = o / n,
            s = n - o % n,
            a = s && 1 > s ? "round" : "floor";
        r = Math[a](r), this.cols = Math.max(r, 1)
    }, i.prototype.getContainerWidth = function() {
        var t = this._getOption("fitWidth"),
            i = t ? this.element.parentNode : this.element,
            n = e(i);
        this.containerWidth = n && n.innerWidth
    }, i.prototype._getItemLayoutPosition = function(t) {
        t.getSize();
        var e = t.size.outerWidth % this.columnWidth,
            i = e && 1 > e ? "round" : "ceil",
            n = Math[i](t.size.outerWidth / this.columnWidth);
        n = Math.min(n, this.cols);
        for (var o = this._getColGroup(n), r = Math.min.apply(Math, o), s = o.indexOf(r), a = {
                x: this.columnWidth * s,
                y: r
            }, h = r + t.size.outerHeight, u = this.cols + 1 - o.length, d = 0; u > d; d++) this.colYs[s + d] = h;
        return a
    }, i.prototype._getColGroup = function(t) {
        if (2 > t) return this.colYs;
        for (var e = [], i = this.cols + 1 - t, n = 0; i > n; n++) {
            var o = this.colYs.slice(n, n + t);
            e[n] = Math.max.apply(Math, o)
        }
        return e
    }, i.prototype._manageStamp = function(t) {
        var i = e(t),
            n = this._getElementOffset(t),
            o = this._getOption("originLeft"),
            r = o ? n.left : n.right,
            s = r + i.outerWidth,
            a = Math.floor(r / this.columnWidth);
        a = Math.max(0, a);
        var h = Math.floor(s / this.columnWidth);
        h -= s % this.columnWidth ? 0 : 1, h = Math.min(this.cols - 1, h);
        for (var u = this._getOption("originTop"), d = (u ? n.top : n.bottom) + i.outerHeight, l = a; h >= l; l++) this.colYs[l] = Math.max(d, this.colYs[l])
    }, i.prototype._getContainerSize = function() {
        this.maxY = Math.max.apply(Math, this.colYs);
        var t = {
            height: this.maxY
        };
        return this._getOption("fitWidth") && (t.width = this._getContainerFitWidth()), t
    }, i.prototype._getContainerFitWidth = function() {
        for (var t = 0, e = this.cols; --e && 0 === this.colYs[e];) t++;
        return (this.cols - t) * this.columnWidth - this.gutter
    }, i.prototype.needsResizeLayout = function() {
        var t = this.containerWidth;
        return this.getContainerWidth(), t != this.containerWidth
    }, i
});
/*!
 * imagesLoaded PACKAGED v4.1.1
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */
! function(t, e) {
    "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", e) : "object" == typeof module && module.exports ? module.exports = e() : t.EvEmitter = e()
}("undefined" != typeof window ? window : this, function() {
    function t() {}
    var e = t.prototype;
    return e.on = function(t, e) {
        if (t && e) {
            var i = this._events = this._events || {},
                n = i[t] = i[t] || [];
            return -1 == n.indexOf(e) && n.push(e), this
        }
    }, e.once = function(t, e) {
        if (t && e) {
            this.on(t, e);
            var i = this._onceEvents = this._onceEvents || {},
                n = i[t] = i[t] || {};
            return n[e] = !0, this
        }
    }, e.off = function(t, e) {
        var i = this._events && this._events[t];
        if (i && i.length) {
            var n = i.indexOf(e);
            return -1 != n && i.splice(n, 1), this
        }
    }, e.emitEvent = function(t, e) {
        var i = this._events && this._events[t];
        if (i && i.length) {
            var n = 0,
                o = i[n];
            e = e || [];
            for (var r = this._onceEvents && this._onceEvents[t]; o;) {
                var s = r && r[o];
                s && (this.off(t, o), delete r[o]), o.apply(this, e), n += s ? 0 : 1, o = i[n]
            }
            return this
        }
    }, t
}),
function(t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["ev-emitter/ev-emitter"], function(i) {
        return e(t, i)
    }) : "object" == typeof module && module.exports ? module.exports = e(t, require("ev-emitter")) : t.imagesLoaded = e(t, t.EvEmitter)
}(window, function(t, e) {
    function i(t, e) {
        for (var i in e) t[i] = e[i];
        return t
    }

    function n(t) {
        var e = [];
        if (Array.isArray(t)) e = t;
        else if ("number" == typeof t.length)
            for (var i = 0; i < t.length; i++) e.push(t[i]);
        else e.push(t);
        return e
    }

    function o(t, e, r) {
        return this instanceof o ? ("string" == typeof t && (t = document.querySelectorAll(t)), this.elements = n(t), this.options = i({}, this.options), "function" == typeof e ? r = e : i(this.options, e), r && this.on("always", r), this.getImages(), h && (this.jqDeferred = new h.Deferred), void setTimeout(function() {
            this.check()
        }.bind(this))) : new o(t, e, r)
    }

    function r(t) {
        this.img = t
    }

    function s(t, e) {
        this.url = t, this.element = e, this.img = new Image
    }
    var h = t.jQuery,
        a = t.console;
    o.prototype = Object.create(e.prototype), o.prototype.options = {}, o.prototype.getImages = function() {
        this.images = [], this.elements.forEach(this.addElementImages, this)
    }, o.prototype.addElementImages = function(t) {
        "IMG" == t.nodeName && this.addImage(t), this.options.background === !0 && this.addElementBackgroundImages(t);
        var e = t.nodeType;
        if (e && d[e]) {
            for (var i = t.querySelectorAll("img"), n = 0; n < i.length; n++) {
                var o = i[n];
                this.addImage(o)
            }
            if ("string" == typeof this.options.background) {
                var r = t.querySelectorAll(this.options.background);
                for (n = 0; n < r.length; n++) {
                    var s = r[n];
                    this.addElementBackgroundImages(s)
                }
            }
        }
    };
    var d = {
        1: !0,
        9: !0,
        11: !0
    };
    return o.prototype.addElementBackgroundImages = function(t) {
        var e = getComputedStyle(t);
        if (e)
            for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(e.backgroundImage); null !== n;) {
                var o = n && n[2];
                o && this.addBackground(o, t), n = i.exec(e.backgroundImage)
            }
    }, o.prototype.addImage = function(t) {
        var e = new r(t);
        this.images.push(e)
    }, o.prototype.addBackground = function(t, e) {
        var i = new s(t, e);
        this.images.push(i)
    }, o.prototype.check = function() {
        function t(t, i, n) {
            setTimeout(function() {
                e.progress(t, i, n)
            })
        }
        var e = this;
        return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function(e) {
            e.once("progress", t), e.check()
        }) : void this.complete()
    }, o.prototype.progress = function(t, e, i) {
        this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !t.isLoaded, this.emitEvent("progress", [this, t, e]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, t), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + i, t, e)
    }, o.prototype.complete = function() {
        var t = this.hasAnyBroken ? "fail" : "done";
        if (this.isComplete = !0, this.emitEvent(t, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
            var e = this.hasAnyBroken ? "reject" : "resolve";
            this.jqDeferred[e](this)
        }
    }, r.prototype = Object.create(e.prototype), r.prototype.check = function() {
        var t = this.getIsImageComplete();
        return t ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void(this.proxyImage.src = this.img.src))
    }, r.prototype.getIsImageComplete = function() {
        return this.img.complete && void 0 !== this.img.naturalWidth
    }, r.prototype.confirm = function(t, e) {
        this.isLoaded = t, this.emitEvent("progress", [this, this.img, e])
    }, r.prototype.handleEvent = function(t) {
        var e = "on" + t.type;
        this[e] && this[e](t)
    }, r.prototype.onload = function() {
        this.confirm(!0, "onload"), this.unbindEvents()
    }, r.prototype.onerror = function() {
        this.confirm(!1, "onerror"), this.unbindEvents()
    }, r.prototype.unbindEvents = function() {
        this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
    }, s.prototype = Object.create(r.prototype), s.prototype.check = function() {
        this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;
        var t = this.getIsImageComplete();
        t && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
    }, s.prototype.unbindEvents = function() {
        this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
    }, s.prototype.confirm = function(t, e) {
        this.isLoaded = t, this.emitEvent("progress", [this, this.element, e])
    }, o.makeJQueryPlugin = function(e) {
        e = e || t.jQuery, e && (h = e, h.fn.imagesLoaded = function(t, e) {
            var i = new o(this, t, e);
            return i.jqDeferred.promise(h(this))
        })
    }, o.makeJQueryPlugin(), o
});
window.ga = window.ga || function() {
    (ga.q = ga.q || []).push(arguments)
};
jQuery(document).ready(function($) {
    $.fn.visible = function(partial) {
        var viewTop = $(window).scrollTop(),
            viewBottom = viewTop + $(window).height(),
            _top = $(this).offset().top,
            _bottom = _top + $(this).height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;
        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
    $(".x-pullquote").each(function(i, el) {
        var el = $(el);
        if (el.visible(true)) {
            el.addClass("already-visible");
        }
    });
    $('#mobile-menu > span').click(function() {
        var mobile_menu = $('#toggle-view-menu');
        if (mobile_menu.is(':hidden')) {
            mobile_menu.slideDown('300');
            $(this).children('span').html('-');
        } else {
            mobile_menu.slideUp('300');
            $(this).children('span').html('+');
        }
        $(this).toggleClass('active');
    });
    $('#toggle-view-menu li').click(function() {
        var text = $(this).children('div.menu-panel');
        if (text.is(':hidden')) {
            text.slideDown('300');
            jQuery(this).children('span').html('-');
        } else {
            text.slideUp('300');
            $(this).children('span').html('+');
        }
    });
    $('.flash-card .back .a').css({
        marginTop: -($('.flash-card .back .a').height() / 2 + 10) + 'px'
    })
    $('.flash-card .btn').click(function() {
        var main_div = $(this).closest(".flash-card");
        $(main_div).find(".back").css({
            height: ($(main_div).find(".front").height() + 20) + 'px'
        });
        if ($(main_div).hasClass('flip')) {
            $(main_div).removeClass('flip')
        } else {
            $(main_div).addClass('flip')
        }
        return false;
    });
    $('.flash-card2 .btn').click(function() {
        $('.flash-card2 .back').css({
            height: ($('.flash-card2 .front').height() + 20) + 'px'
        });
        if ($('.flash-card2').hasClass('flip')) {
            $('.flash-card2').removeClass('flip');
        } else {
            $('.flash-card2').addClass('flip');
        }
        return false;
    });
    var $grid = $('.grid').imagesLoaded(function() {
        $('.grid').masonry({
            columnWidth: '.grid-sizer',
            itemSelector: '.grid-item',
            percentPosition: true,
            gutter: 15
        });
    });
    var expr_date = new Date();
    expr_date.setTime(expr_date.getTime() + (30 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + expr_date.toUTCString() + ";";
    var delete_cookie = "expires=Thu, 01 Jan 1970 00:00:01 GMT;"
    if (location.search) {
        var qry_strs = location.search.slice(1).split('&');
        var qry_hash = {};
        qry_strs.forEach(function(qry_str) {
            qry_str = qry_str.split('=');
            qry_hash[qry_str[0]] = decodeURIComponent(qry_str[1] || '');
        });
    } else {
        var qry_hash = {};
    }
    var s_values = ['s2', 's3', 's4', 's5', 'b'];
    var overRideValues = false;
    if (Object.keys(qry_hash).length > 0) {
        s_values.forEach(function(s_val) {
            overRideValues = overRideValues || s_val in qry_hash;
        });
    }
    if (overRideValues) {
        s_values.forEach(function(s_val) {
            if (s_val in qry_hash) {
                document.cookie = s_val + '=' + qry_hash[s_val] + ';' + expires + 'path=/;domain=' + location.hostname.replace('www.', '');
            } else {
                document.cookie = s_val + '=;' + delete_cookie + 'path=/;domain=' + location.hostname.replace('www.', '');
            }
        });
    } else {
        var cookies = document.cookie.split('; ');
        var cookie_hash = {};
        cookies.forEach(function(cookie) {
            cookie = cookie.split('=');
            cookie_hash[cookie[0]] = decodeURIComponent(cookie[1] || '');
        });
        var cookies_s_values = {};
        if ('s2' in cookie_hash) {
            cookies_s_values['s2'] = cookie_hash['s2'];
            if (s2 === 'undefined') {
                var s2 = cookie_hash['s2'];
            } else {
                s2 = cookie_hash['s2'];
            }
        }
        if ('s3' in cookie_hash) {
            cookies_s_values['s3'] = cookie_hash['s3'];
            if (s3 === 'undefined') {
                var s3 = cookie_hash['s3'];
            } else {
                s3 = cookie_hash['s3'];
            }
        }
        if ('s4' in cookie_hash) {
            cookies_s_values['s4'] = cookie_hash['s4'];
            if (s4 === 'undefined') {
                var s4 = cookie_hash['s4'];
            } else {
                s4 = cookie_hash['s4'];
            }
        }
        if ('s5' in cookie_hash) {
            cookies_s_values['s5'] = cookie_hash['s5'];
            if (s5 === 'undefined') {
                var s5 = cookie_hash['s5'];
            } else {
                s5 = cookie_hash['s5'];
            }
        }
        if (Object.keys(cookies_s_values).length !== 0) {
            window.retQ = window.retQ || {
                q: {},
                tags: function(args) {
                    for (var tag in args) {
                        this.q[tag] = args[tag];
                    }
                },
                empty: function() {
                    return Object.keys(this.q).length === 0;
                },
                to_tags: function() {
                    var ret = this.q;
                    this.q = {};
                    return ret;
                }
            };
            retQ.tags(cookies_s_values);
        }
    }
    $('#header-bottom .expandContent').click(function() {
        if ($('#show-searchbox').is(":visible")) {
            $('.header-top2').hide();
            $('#show-searchbox').hide();
        }
        $('.header-top2').animate({
            height: 'toggle'
        }, 350);
        $('#header-bottom #social-row4').animate({
            height: 'toggle'
        }, 350);
        $('#social-row4').show();
        $('#show-socialicons').show();
    });
    $('.header-bottom #searchboxopen').click(function() {
        if ($('.header-bottom #social-row4').is(":visible")) {
            $('.header-top2').hide();
            $('.header-bottom #social-row4').hide();
        }
        if ($('#searchboxopen').hasClass('active')) {
            $('#searchboxopen').removeClass('active');
        } else {
            $('#searchboxopen').addClass('active');
        }
        $('.header-top2').animate({
            height: 'toggle'
        }, 350);
        $('#show-socialicons').hide();
        $('#social-row4').hide();
    });
    $('.search-submit').click(function() {
        $('.header-top2').animate(slideUp, 350);
        $('.searchboxopen').show();
    });
    $('#header-bottom #social-row4').hide();
    $('#show-socialicons').hide();
    $('.header-top2').hide();

    function showstuff(boxid) {
        $(boxid).css('display', 'inline');
        $(boxid).css('visibility', 'visible');
    }

    function hidestuff(boxid) {
        $(boxid).css('visibility', 'hidden');
        $(boxid).css('display', 'none');
    }
    $('#scroll-origin').click(function() {
        $('html, body').animate({
            scrollTop: $('#scroll-target').offset().top
        }, 1000);
        return false;
    });
    $(window).scroll(function(event) {
        if ($(this).scrollTop() > 1) {
            $('header.avod').addClass("sticky");
            $('header.parallax').addClass("sticky");
            $('.debt-sticky-header').addClass("sticky");
        } else {
            $('header.avod').removeClass("sticky");
            $('header.parallax').removeClass("sticky");
            $('.debt-sticky-header').removeClass("sticky");
        }
    });
    $(".toggleRightMenu").click(function() {
        $("#showRight").toggleClass("active");
        $("#cbp-spmenu-s2").toggleClass("cbp-spmenu-open");
    });
    $('.youtube-defer').each(function() {
        var yt_el = $(this);
        yt_el.prop('src', yt_el.data('src'));
        yt_el.data('src', null);
        yt_el.removeAttr('data-src');
    });
    $('.youtube-delayed').click(function() {
        wrapper = $(this);
        var video_url = wrapper.data("videourl");
        wrapper.empty();
        wrapper.append('<div class="youtube-embed-wrapper"><iframe src="' + video_url + '"></iframe></div>');
    });
    $('.youtube-delayed').each(function() {
        var video_url = $(this).data("videourl");
        $("head").append('<link rel="prefetch" href="' + video_url + '" />');
        $("head").append('<link rel="prerender" href="' + video_url + '" />');
    });

    function onYouTubeIframeAPIReady() {}
    $("#search-expander").click(function() {
        $("#search-wrapper").toggleClass("search-expanded");
    });
    $('.push-menu-container li.menu-item-has-children > a').click(function(e) {
        e.preventDefault();
    });
    $(".push-menu-container li.menu-item-has-children > a").attr("href", "#");
    $(".push-menu-container a").click(function() {
        var link = $(this);
        var closest_ul = link.closest("ul");
        var parallel_active_links = closest_ul.find(".active")
        var closest_li = link.closest("li");
        var link_status = closest_li.hasClass("active");
        var count = 0;
        closest_ul.find("ul").slideUp(function() {
            if (++count == closest_ul.find("ul").length)
                parallel_active_links.removeClass("active");
        });
        if (!link_status) {
            closest_li.children("ul").slideDown();
            closest_li.addClass("active");
        }
    });

    function NextFunction(self) {
        var parent_fieldset = self.parents('fieldset');
        var data_next = 1;
        $(parent_fieldset).find('div').each(function(index, value) {
            var attr = $(this).attr('data-next');
            if ($(this).css("display") != "none" && typeof attr !== typeof undefined && attr !== false) {
                data_next = $(this).attr("data-next");
                return false;
            }
        });
        parent_fieldset.fadeOut(400, function() {
            parent_fieldset.siblings().eq(parent_fieldset.index() + Number(data_next) - 1).fadeIn();
            if ($('.lead-form .processing-step').css('display') == 'block') {
                $('.lead-form .processing-step').delay(3500).fadeOut(400, function() {
                    $('.lead-form .step-5').fadeIn();
                });
            }
        });
    }
    $('.lead-form #zip').keypress(function(e) {
        var key = e.which;
        if (key == 13 && !$('.zip-error').is(':visible')) {
            e.preventDefault();
            NextFunction($(this));
        }
    });
    $('.hp-no-bt input:radio').change(function() {
        NextFunction($(this));
    });
    $('#msform fieldset:first-child').fadeIn('slow');
    $('#msform-no-next-btn fieldset:first-child').fadeIn('slow');
    $('.lead-form .next').on('click', function() {
        var next_step = true;
        var radButton = $(this).parents('fieldset').find('input[name=radio]:checked');
        var radButtondebt = $(this).parents('fieldset').find('input[name=radiodebt]:checked').val();
        var zipcode = $(this).parents('fieldset').find('input[name=zip]').val();
        if ($(this).parents('fieldset').hasClass('required')) {
            $('.showerror').show();
        } else {
            NextFunction($(this));
        }
    });
    $('.lead-form .reset').on('click', function() {
        $(this).parents('fieldset').fadeOut(400, function() {
            $(this).siblings().eq(0).fadeIn();
            $('.lead-form .type-of-debt').attr('class', "step-2 type-of-debt required");
            $('.lead-form .qualifying-questions-1').attr('class', "step-3 qualifying-questions-1 required");
            $('.lead-form .qualifying-questions-2').attr('class', "step-3 qualifying-questions-2 required");
            $('.lead-form .zip-code').attr('class', "step-4 zip-code required");
        });
    });
    $('.lead-form .previous').on('click', function() {
        $(this).parents('fieldset').fadeOut(400, function() {
            $(this).prev().fadeIn();
        });
    });
    $('.type-of-debt input:radio').change(function() {
        $('.showerror').hide();
        $('.type-of-debt').removeClass("required");
    });
    $('.amount-owed input:checkbox').change(function() {
        $('.showerror').hide();
        $('.amount-owed').removeClass("required");
    });
    $('.qualifying-questions-1 input:radio').change(function() {
        $('.showerror').hide();
        $('.qualifying-questions-1').removeClass("required");
    });
    $('.qualifying-questions-2 input:radio').change(function() {
        $('.showerror').hide();
        $('.qualifying-questions-2').removeClass("required");
    });
    $('.debt-types input:radio[name=radio]').change(function() {
        console.log('here');
        if (this.value != '108') {
            $(".how-much-you-owe").show();
            $(".credit-repair-question").hide();
            $('.amount-owed').attr('class', "step-1 amount-owed");
            $(".form-disclaimer .general-disclaimer").show();
            $(".form-disclaimer .credit-correction-disclaimer").hide();
            $(".cc-debt-questions").hide();
            $(".tax-debt-questions").hide();
            $(".student-loan-questions").hide();
            $(".step-5 .thank-you-variable").hide();
            if (this.value == '106') {
                $(".cc-debt-questions").show();
                $('.qualifying-questions-1').addClass("cc-debt");
                $(".step-5 .credit-card").show();
                $(".success-message .js-vertical").text("Credit Card Debt Solutions");
                $(".success-message .js-vertical-es").text("Deudas de Tarjeta de CrÃ©dito");
            } else if (this.value == '110') {
                $(".tax-debt-questions").show();
                $(".tax-debt-questions-2").show();
                $(".student-loan-questions").hide();
                $(".student-loan-questions-2").hide();
                $(".step-5 .tax-debt").show();
                $(".success-message .js-vertical").text("Tax Debt Resolution");
            } else if (this.value == '109') {
                $(".student-loan-questions").show();
                $(".student-loan-questions-2").show();
                $(".tax-debt-questions").hide();
                $(".tax-debt-questions-2").hide();
                $(".step-5 .student-loans").show();
                $(".success-message .js-vertical").text("Student Loan Debt Solutions");
            }
        } else {
            $(".how-much-you-owe").hide();
            $(".credit-repair-question").show();
            $('.amount-owed').addClass("creditrq");
            $(".form-disclaimer .general-disclaimer").hide();
            $(".form-disclaimer .credit-correction-disclaimer").show();
            $(".step-5 .thank-you-variable").hide();
            $(".step-5 .credit-repair").show();
            $('.amount-owed').addClass("required");
            $(".success-message .js-vertical").text("Credit Repair");
            $(".success-message .js-vertical-es").text("ReparaciÃ³n de CrÃ©dito");
        }
    });
    adsbygoogle = window.adsbygoogle || [];
    if ($(window).width() > 768) {
        $('.adsense-mob').remove();
    } else {
        $('.adsense-desk').remove();
    }
    $('ins.adsbygoogle').each(function(i) {
        adsbygoogle.push({});
    });
    $("#content .vt_avod article p, #content div.post-content article p").not($(".flash-card p, .QandA p, .content p")).eq(5).delay(3000).after(function() {
        if ($(this).next().is('p')) {
            return '<div class="adsense-ad"><ins class="adsbygoogle" style="display:block;margin-bottom:20px;text-align:center;" data-ad-layout="in-article" data-ad-client="ca-pub-6075192633676074" data-ad-slot="7324610117" data-ad-format="fluid"></ins></div>';
        } else {
            return false;
        }
    });
    $("#content .vt_avod article p, #content div.post-content article p").not($(".flash-card p, .QandA p, .content p")).eq(10).delay(3000).after(function() {
        if ($(this).next().is('p')) {
            return '<div class="adsense-ad"><ins class="adsbygoogle" style="display:block;margin-bottom:20px;text-align:center;" data-ad-layout="in-article" data-ad-client="ca-pub-6075192633676074" data-ad-slot="9830542859" data-ad-format="fluid"></ins></div>';
        } else {
            return false;
        }
    });
    $(".debt-slider").asRange({
        range: false,
        limit: false,
        format: function format(value) {
            if (value == 100) {
                return "$" + value + ",000+";
            } else {
                return "$" + value + ",000";
            }
        }
    });
    $('#slideshow-container .view-list').click(function() {
        $('#slideshow-container').addClass("list-view");
    })
    $('#slideshow-container .view-slideshow').click(function() {
        $('#slideshow-container').removeClass("list-view");
    })
    var checkitem = function() {
        var $this;
        $this = $("#content-slideshow");
        if ($("#content-slideshow .carousel-inner .carousel-item:first").hasClass("active")) {
            $this.find(".carousel-control-prev").hide();
            $this.find(".carousel-control-next").show();
        } else if ($("#content-slideshow .carousel-inner .carousel-item:last").hasClass("active")) {
            $this.find(".carousel-control-prev").show();
            $this.find(".carousel-control-next").hide();
        } else {
            $this.find('a[class^="carousel-control"]').show();
        }
    };
    checkitem();
    $("#content-slideshow").on("slid.bs.carousel", "", checkitem);
    autoPlayYouTubeModal();

    function autoPlayYouTubeModal() {
        var trigger = $("body").find('[data-target="#video-modal"]');
        trigger.click(function() {
            var theModal = $(this).data("target"),
                videoSRC = $(this).attr("data-theVideo"),
                videoSRCauto = videoSRC + "?autoplay=1";
            $(theModal + ' iframe').attr('src', videoSRCauto);
            $(theModal + ' button.close').click(function() {
                $(theModal + ' iframe').attr('src', videoSRC);
            });
        });
    }
    var SETTINGS = {
        navBarTravelling: false,
        navBarTravelDirection: "",
        navBarTravelDistance: 150
    }
    var colours = {
        0: "#2f90af"
    }
    document.documentElement.classList.remove("no-js");
    document.documentElement.classList.add("js");
    try {
        var pnProductNav = document.getElementById("pnProductNav");
        var pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
        var pnAdvancerRight = document.getElementById("pnAdvancerRight");
        var pnIndicator = document.getElementById("pnIndicator");
        var pnProductNavContents = document.getElementById("pnProductNavContents");
        pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
        moveIndicator(pnProductNav.querySelector("[aria-selected=\"true\"]"), colours[0]);
        var last_known_scroll_position = 0;
        var ticking = false;

        function doSomething(scroll_pos) {
            try {
                pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
            } catch (e) {}
        }
        pnProductNav.addEventListener("scroll", function() {
            last_known_scroll_position = window.scrollY;
            if (!ticking) {
                window.requestAnimationFrame(function() {
                    doSomething(last_known_scroll_position);
                    ticking = false;
                });
            }
            ticking = true;
        });
        pnAdvancerLeft.addEventListener("click", function() {
            try {
                if (SETTINGS.navBarTravelling === true) {
                    return;
                }
                if (determineOverflow(pnProductNavContents, pnProductNav) === "left" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
                    var availableScrollLeft = pnProductNav.scrollLeft;
                    if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
                        pnProductNavContents.style.transform = "translateX(" + availableScrollLeft + "px)";
                    } else {
                        pnProductNavContents.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
                    }
                    pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
                    SETTINGS.navBarTravelDirection = "left";
                    SETTINGS.navBarTravelling = true;
                }
                pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
            } catch (e) {}
        });
        pnAdvancerRight.addEventListener("click", function() {
            try {
                if (SETTINGS.navBarTravelling === true) {
                    return;
                }
                if (determineOverflow(pnProductNavContents, pnProductNav) === "right" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
                    var navBarRightEdge = pnProductNavContents.getBoundingClientRect().right;
                    var navBarScrollerRightEdge = pnProductNav.getBoundingClientRect().right;
                    var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
                    if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
                        pnProductNavContents.style.transform = "translateX(-" + availableScrollRight + "px)";
                    } else {
                        pnProductNavContents.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
                    }
                    pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
                    SETTINGS.navBarTravelDirection = "right";
                    SETTINGS.navBarTravelling = true;
                }
                pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
            } catch (e) {}
        });
        pnProductNavContents.addEventListener("transitionend", function() {
            var styleOfTransform = window.getComputedStyle(pnProductNavContents, null);
            var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
            var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
            pnProductNavContents.style.transform = "none";
            pnProductNavContents.classList.add("pn-ProductNav_Contents-no-transition");
            if (SETTINGS.navBarTravelDirection === "left") {
                pnProductNav.scrollLeft = pnProductNav.scrollLeft - amount;
            } else {
                pnProductNav.scrollLeft = pnProductNav.scrollLeft + amount;
            }
            SETTINGS.navBarTravelling = false;
        }, false);
        pnProductNavContents.addEventListener("click", function(e) {
            var links = [].slice.call(document.querySelectorAll(".pn-ProductNav_Link"));
            links.forEach(function(item) {
                item.setAttribute("aria-selected", "false");
                item.style.borderBottom = 'none';
            })
            e.target.setAttribute("aria-selected", "true");
            moveIndicator(e.target, colours[links.indexOf(e.target)]);
        });

        function moveIndicator(item, color) {
            item.style.borderBottom = '6px solid rgb(47, 144, 175)';
        }

        function determineOverflow(content, container) {
            var containerMetrics = container.getBoundingClientRect();
            var containerMetricsRight = Math.floor(containerMetrics.right);
            var containerMetricsLeft = Math.floor(containerMetrics.left);
            var contentMetrics = content.getBoundingClientRect();
            var contentMetricsRight = Math.floor(contentMetrics.right);
            var contentMetricsLeft = Math.floor(contentMetrics.left);
            if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
                return "both";
            } else if (contentMetricsLeft < containerMetricsLeft) {
                return "left";
            } else if (contentMetricsRight > containerMetricsRight) {
                return "right";
            } else {
                return "none";
            }
        }
    } catch (e) {}
    (function(root, factory) {
        if (typeof define === 'function' && define.amd) {
            define(['exports'], factory);
        } else if (typeof exports !== 'undefined') {
            factory(exports);
        } else {
            factory((root.dragscroll = {}));
        }
    }(this, function(exports) {
        var _window = window;
        var _document = document;
        var mousemove = 'mousemove';
        var mouseup = 'mouseup';
        var mousedown = 'mousedown';
        var EventListener = 'EventListener';
        var addEventListener = 'add' + EventListener;
        var removeEventListener = 'remove' + EventListener;
        var newScrollX, newScrollY;
        var dragged = [];
        var reset = function(i, el) {
            for (i = 0; i < dragged.length;) {
                el = dragged[i++];
                el = el.container || el;
                el[removeEventListener](mousedown, el.md, 0);
                _window[removeEventListener](mouseup, el.mu, 0);
                _window[removeEventListener](mousemove, el.mm, 0);
            }
            dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
            for (i = 0; i < dragged.length;) {
                (function(el, lastClientX, lastClientY, pushed, scroller, cont) {
                    (cont = el.container || el)[addEventListener](mousedown, cont.md = function(e) {
                        if (!el.hasAttribute('nochilddrag') || _document.elementFromPoint(e.pageX, e.pageY) == cont) {
                            pushed = 1;
                            lastClientX = e.clientX;
                            lastClientY = e.clientY;
                            e.preventDefault();
                        }
                    }, 0);
                    _window[addEventListener](mouseup, cont.mu = function() {
                        pushed = 0;
                    }, 0);
                    _window[addEventListener](mousemove, cont.mm = function(e) {
                        if (pushed) {
                            (scroller = el.scroller || el).scrollLeft -= newScrollX = (-lastClientX + (lastClientX = e.clientX));
                            scroller.scrollTop -= newScrollY = (-lastClientY + (lastClientY = e.clientY));
                            if (el == _document.body) {
                                (scroller = _document.documentElement).scrollLeft -= newScrollX;
                                scroller.scrollTop -= newScrollY;
                            }
                        }
                    }, 0);
                })(dragged[i++]);
            }
        }
        if (_document.readyState == 'complete') {
            reset();
        } else {
            _window[addEventListener]('load', reset, 0);
        }
        exports.reset = reset;
    }));
});;
jQuery(document).ready(function($) {
    $(".dcom-embed textarea").click(function() {
        this.focus();
        this.select();
        document.execCommand('copy');
        $(".copied").hide().toggleClass("animated bounceIn")
        $(".copied").show().toggleClass("animated bounceIn").fadeOut(1600)
    });
});
jQuery(document).ready(function($) {
    function applyJsHome1() {
        $('#homeResults1').hide();
        $("#home_price1").ForceNumericOnly();
        $("#home_downpay1").ForceNumericOnly();
        $("#home_term1").ForceNumbersOnly();
        $("#home_rate1").ForceNumericOnly();
        $("#home_tax1").ForceNumericOnly();
        $("#home_ins1").ForceNumericOnly();
        $('#dolsaved1').empty();
        $('#timesaved1').empty();
        if ($("#langswitchEN2").val() == "English") {
            $("#homeform1").validate({
                rules: {
                    home_price1: {
                        required: true,
                        currencywithcommasSP: true
                    },
                    home_term1: {
                        required: true
                    },
                    home_downpay1: {
                        required: true,
                        max: 99
                    },
                    home_rate1: {
                        required: true,
                        max: 99
                    },
                    home_tax1: {
                        currencywithcommasSP: true
                    },
                    home_ins1: {
                        currencywithcommasSP: true
                    }
                },
                messages: {
                    home_price1: {
                        required: "El balance original del prÃ©stamo debe ser mayor a 0"
                    },
                    home_term1: "El perÃ­odo del prÃ©stamo debe ser mayor a 0",
                    home_downpay1: {
                        required: "La pago inicial debe ser mayor a 0",
                        max: "Tasa mÃ¡xima de 99"
                    },
                    home_rate1: {
                        required: "La tasa de interÃ©s debe ser mayor a 0",
                        max: "Tasa mÃ¡xima de 99"
                    }
                }
            });
        } else {
            $("#homeform1").validate({
                rules: {
                    home_price1: {
                        required: true,
                        currencywithcommas: true
                    },
                    home_term1: {
                        required: true
                    },
                    home_downpay1: {
                        required: true,
                        max: 99
                    },
                    home_rate1: {
                        required: true,
                        max: 99
                    },
                    home_tax1: {
                        currencywithcommas: true
                    },
                    home_ins1: {
                        currencywithcommas: true
                    }
                },
                messages: {
                    home_price1: {
                        required: "Original Loan Balance must be greater than 0"
                    },
                    home_downpay1: {
                        required: "Interest Rate must be greater than 0",
                        max: "Maximum rate of 99"
                    },
                    home_term1: "Loan Term must be greater than 0",
                    home_rate1: {
                        required: "Interest Rate must be greater than 0",
                        max: "Maximum rate of 99"
                    }
                }
            });
        }
        $(".reqFieldHome1").each(function() {
            $(this).keyup(function() {
                $("#submithome1").prop("disabled", CheckInputs());
            });
        });

        function CheckInputs() {
            var valid = false;
            $(".reqFieldHome1").each(function() {
                if (valid) {
                    return valid;
                }
                var input = $.trim($(this).val());
                valid = !input;
            });
            return valid;
        }
        $('#submithome1').click(function() {
            if ($("#homeform1").valid()) {
                $('#dwnpayper1').empty();
                $('#dwnpaydol1').empty();
                $('#totloanamt1').empty();
                $('#MoPnI1').empty();
                $('#MoTaxIns1').empty();
                $('#MoPayment1').empty();
                var home_price1 = getNumber($('#home_price1').val());
                var home_downpay1 = getNumber($('#home_downpay1').val());
                var home_term1 = getNumber($('#home_term1').val());
                var home_rate1 = getNumber($('#home_rate1').val());
                var home_tax1 = getNumber($('#home_tax1').val());
                var home_ins1 = getNumber($('#home_ins1').val());
                var monthly_interest = home_rate1 / 100 / 12;
                var termMo = home_term1 * 12;
                var dwnPayDol = home_price1 * (home_downpay1 / 100);
                var loanAmt = home_price1 - dwnPayDol;
                var monthlyPayment = monthly_interest * loanAmt;
                var monthlyPayment = Math.pow(1 + monthly_interest, termMo) * 100;
                var monthlyPayment = (Math.pow(1 + monthly_interest, termMo) - 1) / 100;
                var monthlyPayment = ((monthly_interest * loanAmt) * Math.pow(1 + monthly_interest, termMo) * 100) / (Math.pow(1 + monthly_interest, termMo) - 1) / 100;
                var monthlyPayment = Math.round(((monthly_interest * loanAmt) * Math.pow(1 + monthly_interest, termMo) * 100) / (Math.pow(1 + monthly_interest, termMo) - 1)) / 100;
                var monthlyPayment = getNumber(Math.round(((monthly_interest * loanAmt) * Math.pow(1 + monthly_interest, termMo) * 100) / (Math.pow(1 + monthly_interest, termMo) - 1)) / 100);
                var MoTax = home_tax1 / 12;
                var MoIns = home_ins1 / 12;
                $('#dwnpayper1').append(home_downpay1 + '%');
                $('#dwnpaydol1').append('$' + getFormattedCurrency(dwnPayDol));
                $('#totloanamt1').append('$' + getFormattedCurrency(loanAmt));
                $('#MoPnI1').append('$' + getFormattedCurrency(monthlyPayment));
                $('#MoTaxIns1').append('$' + getFormattedCurrency(MoTax + MoIns));
                $('#MoPayment1').append('$' + getFormattedCurrency(monthlyPayment + MoTax + MoIns));
                $('#homeResults1').show();
            }
        });
    }

    function applyJsHome2() {
        $('#homeResults2').hide();
        $("#home_grosswages2").ForceNumericOnly();
        $("#home_otherincome2").ForceNumericOnly();
        $("#home_moRentPmnt2").ForceNumericOnly();
        $("#home_moAutoPmnt2").ForceNumericOnly();
        $("#home_moCcPmnt2").ForceNumericOnly();
        $("#home_moOtherPmnt2").ForceNumericOnly();
        $("#home_interest_rate2").ForceNumericOnly();
        $("#home_downpay2").ForceNumericOnly();
        $("#home_term2").ForceNumbersOnly();
        $("#home_tax2").ForceNumericOnly();
        $("#home_ins2").ForceNumericOnly();
        if ($("#langswitchEN2").val() == "English") {
            $("#homeform2").validate({
                rules: {
                    home_grosswages2: {
                        required: true,
                        currencywithcommasSP: true
                    },
                    home_otherincome2: {
                        currencywithcommasSP: true
                    },
                    home_moRentPmnt2: {
                        currencywithcommasSP: true
                    },
                    home_moAutoPmnt2: {
                        currencywithcommasSP: true
                    },
                    home_moCcPmnt2: {
                        currencywithcommasSP: true
                    },
                    home_moCcPmnt2: {
                        currencywithcommasSP: true
                    },
                    home_moOtherPmnt2: {
                        currencywithcommasSP: true
                    },
                    home_interest_rate2: {
                        required: true,
                        max: 99
                    },
                    home_downpay2: {
                        required: true,
                        max: 99
                    },
                    home_term2: {
                        required: true
                    },
                    home_tax2: {
                        currencywithcommasSP: true
                    },
                    home_ins2: {
                        currencywithcommasSP: true
                    }
                },
                messages: {
                    home_grosswages2: {
                        required: "El sueldo bruto no puede estar blanco"
                    },
                    home_downpay2: {
                        required: "La pago inicial debe ser mayor a 0",
                        max: "Tasa mÃ¡xima de 99"
                    },
                    home_interest_rate2: {
                        required: "La tasa de interÃ©s debe ser mayor a 0",
                        max: "Tasa mÃ¡xima de 99"
                    },
                    home_term2: "El perÃ­odo del prÃ©stamo debe ser mayor a 0",
                }
            });
        } else {
            $("#homeform2").validate({
                rules: {
                    home_grosswages2: {
                        required: true,
                        currencywithcommas: true
                    },
                    home_otherincome2: {
                        currencywithcommas: true
                    },
                    home_moRentPmnt2: {
                        currencywithcommas: true
                    },
                    home_moAutoPmnt2: {
                        currencywithcommas: true
                    },
                    home_moCcPmnt2: {
                        currencywithcommas: true
                    },
                    home_moCcPmnt2: {
                        currencywithcommas: true
                    },
                    home_moOtherPmnt2: {
                        currencywithcommas: true
                    },
                    home_interest_rate2: {
                        required: true,
                        max: 99
                    },
                    home_downpay2: {
                        required: true,
                        max: 99
                    },
                    home_term2: {
                        required: true
                    },
                    home_tax2: {
                        currencywithcommas: true
                    },
                    home_ins2: {
                        currencywithcommas: true
                    }
                },
                messages: {
                    home_grosswages2: {
                        required: "Gross Wages must not be blank"
                    },
                    home_downpay2: {
                        required: "Rate must be greater than 0",
                        max: "Maximum rate of 99"
                    },
                    home_interest_rate2: {
                        required: "Interest Rate must be greater than 0",
                        max: "Maximum rate of 99"
                    },
                    home_term2: "Loan Term must be greater than 0",
                }
            });
        }
        $(".reqFieldHome2").each(function() {
            $(this).keyup(function() {
                $("#submithome2").prop("disabled", CheckInputs(".reqFieldHome2"));
            });
        });

        function CheckInputs(reqfldclass) {
            var valid = false;
            $(".reqFieldHome2").each(function() {
                if (valid) {
                    return valid;
                }
                var input = $.trim($(this).val());
                valid = !input;
            });
            return valid;
        }
        $('#submithome2').click(function() {
            if ($("#homeform2").valid()) {
                var formatrow2 = "";
                $('#populatehome2').empty();
                $('#dwnpayper2').empty();
                $('#dwnpaydol2').empty();
                $('#totloanamt2').empty();
                $('#MoPnI2').empty();
                $('#MoTaxIns2').empty();
                $('#MoPayment2').empty();
                $('#LoanAmt2').empty();
                $('#TotHomePrice2').empty();
                var moGross = getNumber($("#home_grosswages2").val());
                var oIncome = getNumber($("#home_otherincome2").val());
                var moHouse2 = getNumber($("#home_moRentPmnt2").val());
                var moAuto2 = getNumber($("#home_moAutoPmnt2").val());
                var moCC2 = getNumber($("#home_moCcPmnt2").val());
                var moOth2 = getNumber($("#home_moOtherPmnt2").val());
                var rate2 = getNumber($("#home_interest_rate2").val());
                var downpay2 = getNumber($("#home_downpay2").val());
                var term2 = getNumber($("#home_term2").val());
                var tax2 = getNumber($("#home_tax2").val());
                var ins2 = getNumber($("#home_ins2").val());
                var totGross = moGross + oIncome;
                var debt = moAuto2 + moCC2 + moOth2;
                var DTI = Math.round(((moHouse2 + debt) / (totGross) * 100)) / 100;
                var maxDTI = .43;
                var maxMoDebt = totGross * maxDTI;
                var maxPITI = maxMoDebt - debt;
                term2 = term2 * 12;
                var moRate = rate2 / 100 / 12;
                var guess = maxPITI * 100;
                var monthlyPI = 0;
                var defTax = 0.011;
                var defIns = 0.011;
                var tmpPITI = 0;
                var moTax = 0;
                var moIns = 0;
                while ((maxPITI - tmpPITI) > 1) {
                    monthlyPI = ((moRate * guess) * Math.pow(1 + moRate, term2)) / (Math.pow((1 + moRate), term2) - 1);
                    if (tax2 == 0) {
                        moTax = (guess * defTax) / 12
                    } else {
                        moTax = tax2 / 12
                    }
                    if (ins2 == 0) {
                        moIns = (guess * defIns) / 12
                    } else {
                        moIns = ins2 / 12
                    }
                    tmpPITI = monthlyPI + moTax + moIns;
                    if (tmpPITI < maxPITI) {
                        guess += 1;
                    } else {
                        guess -= 1;
                    }
                }
                var maxHouse = guess;
                var downPayDol = maxHouse * (Math.round(downpay2 * 100) / 100) / 100;
                var taxandIns = moTax + moIns;
                var totPmnt = monthlyPI + taxandIns;
                var LoanAmt = maxHouse - downPayDol;
                var maxHomeDol = monthlyPI;
                $('#dwnpayper2').append(downpay2);
                $('#dwnpaydol2').append(getFormattedCurrency(downPayDol));
                $('#MoPnI2').append(getFormattedCurrency(monthlyPI));
                $('#MoTaxIns2').append(getFormattedCurrency(taxandIns));
                $('#MoPayment2').append(getFormattedCurrency(totPmnt));
                $('#LoanAmt2').append(getFormattedCurrency(LoanAmt));
                $('#TotHomePrice2').append(getFormattedCurrency(maxHouse));
                var principalPaid = 0;
                var interestPaid = 0;
                var payment = 0;
                var totPaymnt = 0;
                var totalPayment = 0;
                var totalInterestP = 0;
                var balance = maxHouse;
                for (p = 1; p < term2 + 1; p++) {
                    interestPaid = balance * moRate;
                    totalInterestP += interestPaid;
                    principalPaid = monthlyPI - interestPaid;
                    if (balance < principalPaid) {
                        principalPaid = balance;
                        payment = principalPaid + interestPaid;
                    }
                    balance -= principalPaid;
                    balance = Math.round(balance * 100) / 100;
                    totalPayment += payment;
                    totalPayment = Math.round(totalPayment * 100) / 100;
                    totalInterestP = Math.round(totalInterestP * 100) / 100;
                    principalPaid = Number(principalPaid.toFixed(2));
                    interestPaid = Number(interestPaid.toFixed(2));
                    taxandIns = Number(taxandIns.toFixed(2));
                    totPaymnt = principalPaid + interestPaid + taxandIns;
                    totPaymnt = Number(totPaymnt.toFixed(2));
                    if (p == term2) {
                        totPaymnt += balance;
                        balance = 0;
                    }
                    formatrow2 += "<div class='row amort'>" + "<div class='col-md-2 col-sm-2 col-2'>" + "<span>" + p + "</span>" + "</div><div class='col-md-2 col-sm-2 col-2 text-right'>" + "<span>" + getFormattedCurrency(principalPaid) + "</span>" + "</div><div class='col-md-2 col-sm-2 col-2 text-right'>" + "<span>" + getFormattedCurrency(interestPaid) + "</span>" + "</div><div class='col-md-2 col-sm-2 col-2 text-right'>" + "<span>" + getFormattedCurrency(taxandIns) + "</span>" + "</div><div class='col-md-2 col-sm-2 col-2 text-right'>" + "<span>" + getFormattedCurrency(totPaymnt) + "</span>" + "</div><div class='col-md-2 col-sm-2 col-2 text-right'>" + "<span>" + getFormattedCurrency(balance) + "</span>" + "</div>" + "</div>";
                    if (balance == 0) {
                        break;
                    }
                }
                $('#populatehome2').append(formatrow2);
                $('#homeResults2').show();
            }
        });
    }

    function applyJsHome3() {
        $('#homeResults3').hide();
        $("#home_balance3").ForceNumericOnly();
        $("#home_apr3").ForceNumericOnly();
        $("#home_minPayment3").ForceNumericOnly();
        $("#home_tax3").ForceNumericOnly();
        $("#home_ins3").ForceNumericOnly();
        $("#home_xtraPayment3").ForceNumericOnly();
        if ($("#langswitchEN2").val() == "English") {
            $("#homeform3").validate({
                rules: {
                    home_balance3: {
                        required: true,
                        currencywithcommasSP: true
                    },
                    home_apr3: {
                        required: true,
                        max: 99
                    },
                    home_minPayment3: {
                        required: true,
                        currencywithcommasSP: true,
                        NotEnough: true
                    },
                    home_tax3: {
                        currencywithcommasSP: true
                    },
                    home_ins3: {
                        currencywithcommasSP: true
                    },
                    home_xtraPayment3: {
                        required: true,
                        min: 1,
                        currencywithcommasSP: true
                    }
                },
                messages: {
                    home_balance3: {
                        required: "El balance original del prÃ©stamo debe ser mayor a 0"
                    },
                    home_apr3: {
                        required: "La tasa de interÃ©s debe ser mayor a 0",
                        max: "Tasa mÃ¡xima de 99"
                    },
                    home_minPayment3: {
                        required: "El pago actual debe ser mayor a 0",
                        NotEnough: "Pago mÃ­nimo debe ser mayor que el interÃ©s contra saldo"
                    },
                    home_xtraPayment3: {
                        required: "El balance original del prÃ©stamo debe ser mayor a 0 ",
                        min: "El balance original del prÃ©stamo debe ser mayor a 1 "
                    }
                }
            });
        } else {
            $("#homeform3").validate({
                rules: {
                    home_balance3: {
                        required: true,
                        currencywithcommas: true
                    },
                    home_apr3: {
                        required: true,
                        max: 99
                    },
                    home_minPayment3: {
                        required: true,
                        currencywithcommas: true,
                        NotEnough: true
                    },
                    home_tax3: {
                        currencywithcommas: true
                    },
                    home_ins3: {
                        currencywithcommas: true
                    },
                    home_xtraPayment3: {
                        required: true,
                        min: 1,
                        currencywithcommas: true
                    }
                },
                messages: {
                    home_balance3: {
                        required: "Original Loan Balance must be greater than 0"
                    },
                    home_apr3: {
                        required: "Interest Rate must be greater than 0",
                        max: "Maximum rate of 99"
                    },
                    home_minPayment3: {
                        required: "Current Payment must be greater than 0"
                    },
                    home_xtraPayment3: {
                        required: "Extra Monthly Payment must be greater than 0",
                        min: "Extra Monthly Payment must be greater than 1"
                    }
                }
            });
        }
        $(".reqFieldHome3").each(function() {
            $(this).keyup(function() {
                $("#submithome3").prop("disabled", CheckInputs());
            });
        });

        function CheckInputs() {
            var valid = false;
            $(".reqFieldHome3").each(function() {
                if (valid) {
                    return valid;
                }
                var input = $.trim($(this).val());
                valid = !input;
            });
            return valid;
        }
        $('#submithome3').click(function() {
            if ($("#homeform3").valid()) {
                $('#xtraTimetopay3').empty();
                $('#curTimetopay3').empty();
                $('#xtraTotMopay3').empty();
                $('#curTotMopay3').empty();
                $('#xtraTotIntpay3').empty();
                $('#curTotIntpay3').empty();
                $('#dolsaved3').empty();
                $('#timesaved3').empty();
                var hm_balance3 = getNumber($('#home_balance3').val());
                var hm_apr3 = getNumber($('#home_apr3').val());
                var minPayment3 = getNumber($('#home_minPayment3').val());
                var home_tax3 = getNumber($('#home_tax3').val());
                var home_ins3 = getNumber($('#home_ins3').val());
                var xtraPayment3 = getNumber($('#home_xtraPayment3').val());
                var monthly_interest = hm_apr3 / 100 / 12;
                var principlePort = 0;
                var accruedInt = 0;
                var interestPort = 0;
                var balance_temp = hm_balance3;
                var term = 0;
                while (balance_temp > 0) {
                    interestPort = balance_temp * monthly_interest;
                    principlePort = minPayment3 - interestPort;
                    balance_temp -= principlePort;
                    accruedInt += interestPort;
                    term++;
                }
                var curTerm = term;
                var curTotPaid = hm_balance3 + accruedInt;
                var curTotInt = accruedInt;
                accruedInt = 0;
                interestPort = 0;
                balance_temp = hm_balance3;
                term = 0;
                var Xpayment = minPayment3 + xtraPayment3;
                while (balance_temp > 0) {
                    interestPort = balance_temp * monthly_interest;
                    principlePort = Xpayment - interestPort;
                    balance_temp -= principlePort;
                    accruedInt += interestPort;
                    term++;
                }
                xTerm = term;
                xTotPaid = hm_balance3 + accruedInt;
                xTotInt = accruedInt;
                var curYYmm = {};
                var xYYmm = {};
                curYYmm = getYearstoRepay(curTerm, 12);
                xYYmm = getYearstoRepay(xTerm, 12);
                var MoTax = home_tax3 / 12;
                var MoIns = home_ins3 / 12;
                $('#xtraTimetopay3').append(formatRepayTime(xYYmm));
                $('#curTimetopay3').append(formatRepayTime(curYYmm));
                $('#xtraTotMopay3').append('$' + getFormattedCurrency(Xpayment + MoTax + MoIns));
                $('#curTotMopay3').append('$' + getFormattedCurrency(minPayment3 + MoTax + MoIns));
                $('#xtraTotIntpay3').append('$' + getFormattedCurrency(xTotInt));
                $('#curTotIntpay3').append('$' + getFormattedCurrency(curTotInt));
                dolSaved = curTotPaid - xTotPaid;
                timeSaved = getYearstoRepay(curTerm - xTerm, 12);
                $('#dolsaved3').append('$' + getFormattedCurrency(dolSaved));
                $('#timesaved3').append(formatRepayTime(timeSaved));
                $('#homeResults3').show();
            }
        });
    }

    function getNumber(text) {
        var num = 0;
        num = parseFloat(text) || 0;
        return num;
    }

    function getYearstoRepay(nbrOfByweeklyPayment, terms) {
        var numObj = {};
        var years = 0;
        var months = 0;
        if (terms == 12) {
            years = Math.floor(nbrOfByweeklyPayment / terms);
            months = Number(nbrOfByweeklyPayment % terms);
        } else {
            years = Math.floor(nbrOfByweeklyPayment / terms);
            months = Number(nbrOfByweeklyPayment % terms);
            if (months == 0) {
                months = "";
            } else if (months == 13) {
                months = 6;
            } else if (months < 13 && months > 7) {
                months = Math.floor(months - 1);
            } else {
                months = Math.floor((months - 1) / 2);
            }
        }
        numObj = {
            years: years,
            months: months
        };
        return numObj;
    }

    function formatRepayTime(repay) {
        if ($("#langswitchEN2").val() == "English") {
            var format = repay.years <= 0 && repay.months > 0 ? repay.months + " meses " : repay.years > 0 && repay.months > 0 ? repay.years + " aÃ±os " + repay.months + " meses" : repay.years > 0 && (repay.months < 0 || repay.months == "") ? repay.years + " aÃ±os" : "";
            if (repay.years <= 1) {
                format = format.replace("aÃ±os", "aÃ±o");
            }
            if (repay.months <= 1) {
                format = format.replace("meses", "mes");
            }
        } else {
            var format = repay.years <= 0 && repay.months > 0 ? repay.months + " months " : repay.years > 0 && repay.months > 0 ? repay.years + " years " + repay.months + " months" : repay.years > 0 && (repay.months < 0 || repay.months == "") ? repay.years + " years" : "";
            if (repay.years <= 1) {
                format = format.replace("years", "year");
            }
            if (repay.months <= 1) {
                format = format.replace("months", "month");
            }
        }
        return format;
    }

    function getFormattedCurrency(num) {
        num = num.toFixed(2);
        var cents = (num - Math.floor(num)).toFixed(2);
        return Math.floor(num).toLocaleString() + '.' + cents.split('.')[1];
    }
    applyJsHome1();
});
jQuery(document).ready(function($) {
    function applyJsAuto1() {
        $('#autoresults').hide();
        $("#purchaseprice").ForceNumericOnly();
        $("#loanterm").ForceNumbersOnly();
        $("#interestrate").ForceNumericOnly();
        $("#vehicledownpayment").ForceNumericOnly();
        $("#autopayment-form1").removeData("validator").removeData("unobtrusiveValidation");
        if ($("#langswitchEN2").val() == "English") {
            $("#autopayment-form1").validate({
                rules: {
                    purchaseprice: {
                        required: true,
                        currencywithcommasSP: true
                    },
                    vehicledownpayment: {
                        currencywithcommasSP: true
                    },
                    loanterm: {
                        required: true
                    },
                    interestrate: {
                        required: true,
                        max: 99
                    }
                },
                messages: {
                    purchaseprice: {
                        required: "El precio de compra debe ser mayor a 0"
                    },
                    loanterm: "El perÃ­odo del prÃ©stamo debe ser mayor a 0",
                    interestrate: {
                        required: "La tasa de interÃ©s debe ser mayor a 0",
                        max: "Tasa mÃ¡xima de 99"
                    }
                }
            });
        } else {
            $("#autopayment-form1").validate({
                rules: {
                    purchaseprice: {
                        required: true,
                        currencywithcommas: true
                    },
                    vehicledownpayment: {
                        currencywithcommas: true
                    },
                    loanterm: {
                        required: true
                    },
                    interestrate: {
                        required: true,
                        max: 99
                    }
                },
                messages: {
                    purchaseprice: {
                        required: "Purchase Price must be greater than 0"
                    },
                    loanterm: {
                        required: "Loan Term must be greater than 0"
                    },
                    interestrate: {
                        required: "Interest Rate must be greater than 0",
                        max: "Maximum rate of 99"
                    }
                }
            });
        }
        $(".reqFieldAuto1").each(function() {
            $(this).keyup(function() {
                $("#submitAuto").prop("disabled", CheckInputs(".reqFieldAuto1"));
            });
        });

        function CheckInputs(reqfldclass) {
            var disable = false;
            $(".reqFieldAuto1").each(function() {
                if (disable) {
                    return disable;
                }
                var input = $.trim($(this).val());
                disable = !input;
            });
            return disable;
        }
        $('#submitAuto').click(function() {
            if ($("#autopayment-form1").valid()) {
                $('#mpayment').empty();
                var pprice = getNumber($('#purchaseprice').val());
                var dpayment = getNumber($('#vehicledownpayment').val());
                var apr = getNumber($('#interestrate').val());
                var terms = getNumber($('#loanterm').val());
                var mPayment = calculateLoan(pprice, dpayment, apr, terms);
                $('#mpayment').append('<span> $' + getFormattedCurrency(mPayment) + '</span>');
                var totalAutoInt = getInterestForXtraPayment(pprice - dpayment, apr, 0, mPayment, 0, 0, terms, 12);
                $('#apr-results').val(getFormattedCurrency(totalAutoInt));
                $('#ddpayment-results').val(getFormattedCurrency(dpayment));
                $('#pprice-results').val(getFormattedCurrency(pprice));
                $('#autoresults').show();
            }
        });
    }

    function accelerateLoan(lamount, lterms, irate) {
        var monthlyInterest = calculateInterest(irate, 12);
        var biweeklyInterest = calculateInterest(irate, 26);
        var monthlyPayment = calculateLoan(lamount, 0, irate, lterms);
        var biweeklyPayment = Math.round(monthlyPayment * 100 / 2) / 100;
        $('.resultAmount').empty();
        $('.resultApr').empty();
        $('.resultbiweeklypmt').empty();
        $('.resultmonthlypmt').empty();
        $('.resultTotalaprBi').empty();
        $('.resultTotalaprMonth').empty();
        $('.resultbiYears').empty();
        $('.resultmonthYears').empty();
        $('#populatepayments').empty();
        var totalInterestPaidMonthly = 0;
        var totalInterestpaidBiweekly = 0;;
        var nbrmonthlyPmt = 0;
        var nbrweeklyPmt = 0;
        var totalmonthly = lamount;
        var totalbiweekly = lamount;
        var totalaprmonthly = 0;
        var totalaprbiweekly = 0;
        var totalBiweekPymtArr = [];
        var totalBiweekArr = [];
        var totalMonthArr = [];
        var totalMonthPymntArr = [];
        var monthlyloop = {};
        var weeklyloop = {};
        var formatpayment = "";
        for (var x = 0; x < lterms / 12; x++) {
            var year = x + 1;
            var totalmonthlyPmt = 0;
            monthlyloop = loopForPymnt(totalmonthly, monthlyInterest, totalInterestPaidMonthly, monthlyPayment, totalmonthlyPmt, nbrmonthlyPmt, 12);
            if (monthlyloop.total > totalmonthly) {
                monthlyPayment = monthlyPayment + 1;
                biweeklyPayment = Math.round(monthlyPayment * 100 / 2) / 100;
                monthlyloop = {};
                nbrmonthlyPmt = 0;
                monthlyloop = loopForPymnt(totalmonthly, monthlyInterest, totalInterestPaidMonthly, monthlyPayment, totalmonthlyPmt, nbrmonthlyPmt, 12);
            }
            totalmonthly = monthlyloop.total;
            totalInterestPaidMonthly = monthlyloop.totalInterest;
            nbrmonthlyPmt = monthlyloop.numberOfPayment;
            var totalbiweeklyPmt = 0;
            weeklyloop = loopForPymnt(totalbiweekly, biweeklyInterest, totalInterestpaidBiweekly, biweeklyPayment, totalbiweeklyPmt, nbrweeklyPmt, 26);
            totalbiweekly = weeklyloop.total;
            totalInterestpaidBiweekly = weeklyloop.totalInterest;
            nbrweeklyPmt = weeklyloop.numberOfPayment;
            totalBiweekArr.push(weeklyloop.total);
            totalBiweekPymtArr.push(weeklyloop.totalPayment);
            totalMonthArr.push(monthlyloop.total);
            totalMonthPymntArr.push(monthlyloop.totalPayment);
            formatpayment += "<div class='row'>" + "<div class='col-md-1'>" + "<span>" + year + "</span>" + "</div><div class='col-md-2'>" + "<span>" + getFormattedCurrency(weeklyloop.totalPayment) + "</span>" + "</div><div class='col-md-3'>" + "<span>" + getFormattedCurrency(weeklyloop.total) + "</span>" + "</div><div class='col-md-2'>" + "<span>" + getFormattedCurrency(monthlyloop.totalPayment) + "</span>" + "</div><div class='col-md-3'>" + "<span>" + getFormattedCurrency(monthlyloop.total) + "</span>" + "</div>" + "</div>";
        }
        var repayTime = {};
        var monthlyrepaytime = {};
        repayTime = getYearstoRepay(weeklyloop.numberOfPayment, 26);
        monthlyrepaytime = getYearstoRepay(monthlyloop.numberOfPayment, 12);
        var str = formatRepayTime(repayTime);
        var strMonth = formatRepayTime(monthlyrepaytime);
        $('.resultAmount').append('$' + getFormattedCurrency(lamount));
        $('.resultApr').append(irate + '%');
        $('.resultbiweeklypmt').append('$' + getFormattedCurrency(biweeklyPayment));
        $('.resultmonthlypmt').append('$' + getFormattedCurrency(monthlyPayment));
        $('.resultTotalaprBi').append('$' + getFormattedCurrency(weeklyloop.totalInterest));
        $('.resultTotalaprMonth').append('$' + getFormattedCurrency(monthlyloop.totalInterest));
        $('.resultbiYears').append(str);
        $('.resultmonthYears').append(strMonth);
        $('#populatepayments').append(formatpayment);
    }

    function formatRepayTime(repay) {
        if ($("#langswitchEN2").val() == "English") {
            var format = repay.years <= 0 && repay.months > 0 ? repay.months + " meses " : repay.years > 0 && repay.months > 0 ? repay.years + " aÃ±os " + repay.months + " meses" : repay.years > 0 && (repay.months < 0 || repay.months == "") ? repay.years + " aÃ±os" : "";
            if (repay.years <= 1) {
                format = format.replace("aÃ±os", "aÃ±o");
            }
            if (repay.months <= 1) {
                format = format.replace("meses", "mes");
            }
        } else {
            var format = repay.years <= 0 && repay.months > 0 ? repay.months + " months " : repay.years > 0 && repay.months > 0 ? repay.years + " years " + repay.months + " months" : repay.years > 0 && (repay.months < 0 || repay.months == "") ? repay.years + " years" : "";
            if (repay.years <= 1) {
                format = format.replace("years", "year");
            }
            if (repay.months <= 1) {
                format = format.replace("months", "month");
            }
        }
        return format;
    }
    applyJsAuto1();
});

function getInterestForXtraPayment(loanAmount, interest, totalInterestP, payment, totalPayment, nbrPayment, months, terms) {
    var xloan = {};
    var apr = calculateInterest(interest, 12);
    for (var x = 0; x < months / 12; x++) {
        xloan = loopForPymnt(loanAmount, apr, 0, payment, 0, 0, 12);
        if (xloan.total > loanAmount) {
            payment = payment + 1;
            xloan = {};
            xloan = loopForPymnt(loanAmount, apr, 0, payment, 0, 0, 12);
        }
        loanAmount = xloan.total;
        totalInterestP += xloan.totalInterest;
    }
    return roundValTen(totalInterestP);
}

function roundValTen(value) {
    return (Math.round(value * 100) / 100).toFixed(2);
}

function getFormattedCurrency(num) {
    console.log(num);
    num = parseFloat(num).toFixed(2);
    var cents = (num - Math.floor(num)).toFixed(2);
    return Math.floor(num).toLocaleString() + '.' + cents.split('.')[1];
}

function calculateLoan(loanAmount, downPayment, apr, terms) {
    var monthlyInterest = calculateInterest(apr, 12);
    var newprincipal = loanAmount - downPayment;
    var camonthlyPayment = Math.round(((monthlyInterest * newprincipal) * Math.pow(1 + monthlyInterest, terms) * 100) / (Math.pow(1 + monthlyInterest, terms) - 1)) / 100;
    camonthlyPayment = getNumber(camonthlyPayment);
    return camonthlyPayment;
}

function loopForPymnt(total, interest, totalInterestP, payment, totalPayment, nbrPayment, terms) {
    var principalPaid = 0;
    var interestPaid = 0;
    var loop = {};
    for (var y = 1; y <= terms; y++) {
        if (total <= 0) {
            break;
        }
        interestPaid = total * interest;
        totalInterestP += interestPaid;
        principalPaid = payment - interestPaid;
        if (total < principalPaid) {
            principalPaid = total;
            payment = principalPaid + interestPaid;
        }
        total -= principalPaid;
        total = total * 100 / 100;
        totalPayment += payment;
        totalPayment = totalPayment * 100 / 100;
        nbrPayment++;
    }
    loop = {
        total: total,
        totalPayment: totalPayment,
        numberOfPayment: nbrPayment,
        totalInterest: (Math.round(totalInterestP * 100) / 100)
    };
    return loop;
}

function calculateInterest(interest, monthOrBiweekly) {
    var newapr = interest / 100 / monthOrBiweekly;
    return newapr;
}

function getNumber(text) {
    var num = 0;
    num = parseFloat(text) || 0;
    return num;
}

function getYearstoRepay(nbrOfByweeklyPayment, terms) {
    var numObj = {};
    var years = 0;
    var months = 0;
    if (terms == 12) {
        years = Math.floor(nbrOfByweeklyPayment / terms);
        months = Number(nbrOfByweeklyPayment % terms);
    } else {
        years = Math.floor(nbrOfByweeklyPayment / terms);
        months = Number(nbrOfByweeklyPayment % terms);
        if (months == 0) {
            months = "";
        } else if (months == 13) {
            months = 6;
        } else if (months < 13 && months > 7) {
            months = Math.floor(months - 1);
        } else {
            months = Math.floor((months - 1) / 2);
        }
    }
    numObj = {
        years: years,
        months: months
    };
    return numObj;
};
jQuery(document).ready(function($) {
    function applyJsSav1() {
        $('#savresults1').hide();
        $("#sav_amount1").ForceNumericOnly();
        $("#sav_deposit1").ForceNumericOnly();
        $("#sav_intrate1").ForceNumericOnly();
        $("#sav_years1").ForceNumbersOnly();
        if ($("#langswitchEN2").val() == "English") {
            $("#savings-form1").validate({
                rules: {
                    sav_amount1: {
                        required: true,
                        currencywithcommasSP: true
                    },
                    sav_deposit1: {
                        required: true,
                        currencywithcommasSP: true
                    },
                    sav_intrate1: {
                        required: true,
                        max: 99
                    },
                    sav_years1: {
                        required: true,
                        max: 99
                    }
                },
                messages: {
                    sav_amount1: {
                        required: "Monto inicial no puede estar estar blanco"
                    },
                    sav_deposit1: {
                        required: "El depÃ³sito mensual debe ser mayor a 0"
                    },
                    sav_intrate1: {
                        required: "La tasa de interÃ©s debe ser mayor a 0",
                        max: "Tasa mÃ¡xima de 99"
                    },
                    sav_years1: {
                        required: "El perÃ­odo del prÃ©stamo debe ser mayor a 0",
                        max: "MÃ¡xima de 99"
                    },
                }
            });
        } else {
            $("#savings-form1").validate({
                rules: {
                    sav_amount1: {
                        required: true,
                        currencywithcommas: true
                    },
                    sav_deposit1: {
                        required: true,
                        currencywithcommas: true
                    },
                    sav_intrate1: {
                        required: true,
                        max: 99
                    },
                    sav_years1: {
                        required: true,
                        max: 99
                    }
                },
                messages: {
                    sav_amount1: {
                        required: "Initial Amount must not be blank"
                    },
                    sav_deposit1: {
                        required: "Monthly Deposit must be greater than 0"
                    },
                    sav_intrate1: {
                        required: "Interest Rate must be greater than 0",
                        max: "Maximum rate of 99"
                    },
                    sav_years1: {
                        required: "Loan Term must be greater than 0",
                        max: "Maximum term of 99"
                    },
                }
            });
        }
        $(".reqFieldSav1").each(function() {
            $(this).keyup(function() {
                $("#submitSav1").prop("disabled", CheckInputs(".reqFieldSav1"));
            });
        });

        function CheckInputs(reqfldclass) {
            var valid = false;
            $(".reqFieldSav1").each(function() {
                if (valid) {
                    return valid;
                }
                var input = jQuery.trim($(this).val());
                valid = !input;
            });
            return valid;
        }
        $('#submitSav1').click(function() {
            if ($("#savings-form1").valid()) {
                var formatrow = "";
                $('#populatesavings').empty();
                $('#savsummary1').empty();
                var savamount1 = getNumber($("#sav_amount1").val());
                var savdeposit1 = getNumber($("#sav_deposit1").val());
                var savintrate1 = getNumber($("#sav_intrate1").val());
                var intfreq = getNumber($("#int_freq").val());
                var savyears1 = getNumber($("#sav_years1").val());
                var oneyr_deposit1 = 0;
                var oneyr_interest1 = 0;
                var tot_deposit1 = 0;
                var tot_interest1 = 0;
                var tot_balance1 = savamount1;
                for (var year = 1; year <= savyears1; year++) {
                    oneyr_deposit1 = savdeposit1 * 12;
                    oneyr_interest1 = calcYrInt(tot_balance1, savdeposit1, savintrate1, intfreq);
                    tot_deposit1 += oneyr_deposit1;
                    tot_interest1 += oneyr_interest1;
                    tot_balance1 += oneyr_deposit1;
                    tot_balance1 += oneyr_interest1;
                    formatrow += "<tr>" + "<td>" + year + "</td>" + "<td>" + getFormattedCurrency(oneyr_deposit1) + "</td>" + "<td>" + getFormattedCurrency(oneyr_interest1) + "</td>" + "<td>" + getFormattedCurrency(tot_deposit1) + "</td>" + "<td>" + getFormattedCurrency(tot_interest1) + "</td>" + "<td>" + getFormattedCurrency(tot_balance1) + "</td>" + "</tr>";
                }
                var savSum = "";
                if ($("#langswitchEN2").val() == "English") {
                    savSum = '<span>Su depÃ³sito mensual de $' + getFormattedCurrency(savdeposit1) + ' por ' + savyears1 + ' aÃ±o con una tasa de interÃ©s de ' + savintrate1 + '% compuesto ' + $("#int_freq option:selected").text() + 'con un saldo de partida inicial de $' + getFormattedCurrency(savamount1) + '.</span>'
                } else {
                    savSum = '<span>Your monthly deposit of $' + getFormattedCurrency(savdeposit1) + ' for ' + savyears1 + ' years with an interest rate of ' + savintrate1 + '% compounded ' + $("#int_freq option:selected").text() + ' with an initial starting balance of $' + getFormattedCurrency(savamount1) + '.</span>'
                }
                $('#savsummary1').append(savSum);
                $('#populatesavings').append(formatrow);
                $('#savresults1').show();
            }
        });
    }
    applyJsSav1();
});

function calcYrInt(balance, deposit, rate, freq) {
    var freqRate = rate / 100 / freq;
    var intdol = 0;
    var totintdol = 0;
    var freqDep = (deposit * 12) / freq;
    for (var y = 1; y <= freq; y++) {
        balance += freqDep;
        intdol = balance * freqRate;
        totintdol += intdol;
        balance += intdol;
    }
    return totintdol;
}

function getNumber(text) {
    var num = 0;
    num = parseFloat(text) || 0;
    return num;
}

function getFormattedCurrency(num) {
    num = parseFloat(num).toFixed(2);
    var cents = (num - Math.floor(num)).toFixed(2);
    return Math.floor(num).toLocaleString() + '.' + cents.split('.')[1];
};
jQuery(function() {
    $ = jQuery;
    $('input[type="text"].text').focus(function() {
        if (this.value == this.defaultValue) {
            this.select();
        }
    }).blur(function() {
        if (this.value == '') {
            this.value = this.defaultValue;
        }
        calc();
    }).mouseup(function(e) {
        e.preventDefault();
    });
    $('input[type="text"].money').blur(function() {
        this.value = '$' + (parseFloat(this.value.replace(/[^0-9.]/g, '')).formatMoney().replace('.00', ''));
    });
    $('input[type="text"].int').blur(function() {
        var val = this.value.replace(/^0+/, '');
        val = val.replace(/[^0-9.]/g, '');
        this.value = isNaN(parseInt(val)) ? 0 : parseInt(val).formatMoney().replace('.00', '');
        if ($(this).hasClass('less')) {
            var amt = parseInt($(this).closest('form').find('input.amt').val().replace(/[^0-9.]/, ''));
            amt = isNaN(amt) ? 0 : amt;
            var less = parseInt(this.value.replace(/[^0-9.]/, ''));
            less = isNaN(less) ? 0 : less;
            if (less > amt) {
                this.value = amt
            }
        }
    });
});

function calc() {
    $('form.section input.text').each(function() {
        this.blur();
    });
    var total = 0,
        cigs = 0,
        beer = 0,
        wine = 0,
        liq = 0,
        multiplier = 0;
    $('form.section').each(function() {
        var subtotal = 0,
            cost = 0,
            amt = 0,
            less = 0,
            divider = 1;
        var id = this.id.replace('calc-', '');
        switch (id) {
            case "cigs":
                multiplier = 365;
                divider = 20;
                break;
            case "beer":
                multiplier = 52;
                break;
            case "wine":
                multiplier = 52;
                break;
            case "liq":
                multiplier = 12;
                break;
        }
        cost = parseFloat($(this).find('input.cost').val().replace(/[^0-9.]/, ''));
        cost = isNaN(cost) ? 0 : cost;
        amt = parseInt($(this).find('input.amt').val().replace(/[^0-9.]/, ''));
        amt = isNaN(amt) ? 0 : amt;
        amt = amt / divider;
        less = parseInt($(this).find('input.less').val().replace(/[^0-9.]/, ''));
        less = isNaN(less) ? 0 : less;
        less = less / divider;
        var subtotal = (cost * (amt * multiplier)) - (cost * ((amt - less) * multiplier));
        $(this).find('.result .amt').text('$' + subtotal.formatMoney().replace('.00', ''));
        total += subtotal;
    });
    $('.total .amt').text('$' + total.formatMoney().replace('.00', ''));
    return false;
}
Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "")
};
(function(window) {
    'use strict';

    function extend(a, b) {
        for (var key in b) {
            if (b.hasOwnProperty(key)) {
                a[key] = b[key];
            }
        }
        return a;
    }

    function each(collection, callback) {
        for (var i = 0; i < collection.length; i++) {
            var item = collection[i];
            callback(item);
        }
    }

    function Menu(options) {
        this.options = extend({}, this.options);
        extend(this.options, options);
        this._init();
    }
    Menu.prototype.options = {
        wrapper: '#o-wrapper',
        type: 'push-right',
        menuOpenerClass: '.c-button',
        maskId: '#c-mask'
    };
    Menu.prototype._init = function() {
        this.body = document.body;
        this.wrapper = document.querySelector(this.options.wrapper);
        this.mask = document.querySelector(this.options.maskId);
        this.menu = document.querySelector('#c-menu--' + this.options.type);
        this.closeBtn = this.menu.querySelector('.c-menu__close');
        this.menuOpeners = document.querySelectorAll(this.options.menuOpenerClass);
        this._initEvents();
    };
    Menu.prototype._initEvents = function() {
        this.closeBtn.addEventListener('click', function(e) {
            e.preventDefault();
            this.close();
        }.bind(this));
        this.mask.addEventListener('click', function(e) {
            e.preventDefault();
            this.close();
        }.bind(this));
    };
    Menu.prototype.open = function() {
        this.body.classList.add('has-active-menu');
        this.wrapper.classList.add('has-' + this.options.type);
        this.menu.classList.add('is-active');
        this.mask.classList.add('is-active');
        this.disableMenuOpeners();
    };
    Menu.prototype.close = function() {
        this.body.classList.remove('has-active-menu');
        this.wrapper.classList.remove('has-' + this.options.type);
        this.menu.classList.remove('is-active');
        this.mask.classList.remove('is-active');
        this.enableMenuOpeners();
    };
    Menu.prototype.disableMenuOpeners = function() {
        each(this.menuOpeners, function(item) {
            item.disabled = true;
        });
    };
    Menu.prototype.enableMenuOpeners = function() {
        each(this.menuOpeners, function(item) {
            item.disabled = false;
        });
    };
    window.Menu = Menu;
})(window);
/* 
************ DELETE THIS! **********************
This is the latest sticky header code (homepage) we are no longer using a sticky header, get rid of it. 
*/
//var floatPanel = new McFloatPanel();
///* Float Panel v2016.10.28. Copyright www.menucool.com */
//function McFloatPanel(){var i=[],s=[],h="className",t="getElementsByClassName",d="length",l="display",C="transition",m="style",B="height",c="scrollTop",k="offsetHeight",a="fixed",e=document,b=document.documentElement,j=function(a,c,b){if(a.addEventListener)a.addEventListener(c,b,false);else a.attachEvent&&a.attachEvent("on"+c,b)},o=function(c,d){if(typeof getComputedStyle!="undefined")var b=getComputedStyle(c,null);else b=c.currentStyle;return b?b[d]:a},L=function(){var a=e.body;return Math.max(a.scrollHeight,a[k],b.clientHeight,b.scrollHeight,b[k])},O=function(a,c){var b=a[d];while(b--)if(a[b]===c)return true;return false},g=function(b,a){return O(b[h].split(" "),a)},q=function(a,b){if(!g(a,b))if(!a[h])a[h]=b;else a[h]+=" "+b},p=function(a,f){if(a[h]&&g(a,f)){for(var e="",c=a[h].split(" "),b=0,i=c[d];b<i;b++)if(c[b]!==f)e+=c[b]+" ";a[h]=e.replace(/^\s+|\s+$/g,"")}},n=function(){return window.pageYOffset||b[c]},z=function(a){return a.getBoundingClientRect().top},F=function(b){var c=n();if(c>b.oS&&!g(b,a))q(b,a);else g(b,a)&&c<b.oS&&p(b,a)},x=function(){for(var a=0;a<s[d];a++)J(s[a])},J=function(a){if(a.oS){a.fT&&clearTimeout(a.fT);a.fT=setTimeout(function(){if(a.aF)F(a);else y(a)},50)}else y(a)},w=function(d,c,b){p(d,a);c[l]="none";b.position=b.top=""},y=function(c){var j=z(c),f=c[k],e=c[m],d=c.pH[m],h=n();if(j<c.oT&&h>c.oS&&!g(c,a)&&(window.innerHeight||b.clientHeight)>f){c.tP=h+j-c.oT;var p=L();if(f>p-c.tP-f)var i=f;else i=0;d[l]="block";d[C]="none";d[B]=f+1+"px";c.pH[k];d[C]="height .3s";d[B]=i+"px";q(c,a);e.position=a;e.top=c.oT+"px";if(o(c,"position")!=a)d[l]="none"}else if(g(c,a)&&(h<c.tP||h<c.oS)){var s=o(c,"animation");if(c.oS&&c.classList&&s.indexOf("slide-down")!=-1){var r=o(c,"animationDuration");c.classList.remove(a);e.animationDirection="reverse";e.animationDuration="300ms";void c[k];c.classList.add(a);setTimeout(function(){w(c,d,e);e.animationDirection="normal";e.animationDuration=r},300)}else w(c,d,e)}},I=function(){var f=[],c,b;if(e[t]){f=e[t]("float-panel");i=e[t]("slideanim")}else{var k=e.getElementsByTagName("*");c=k[d];while(c--)g(k[c],"float-panel")&&f.push(k[c])}c=f[d];for(var h=0;h<c;h++){b=s[h]=f[h];b.oT=parseInt(b.getAttribute("data-top")||0);b.oS=parseInt(b.getAttribute("data-scroll")||0);if(b.oS>20&&o(b,"position")==a)b.aF=1;b.pH=e.createElement("div");b.pH[m].width=b.offsetWidth+"px";b.pH[m][l]="none";b.parentNode.insertBefore(b.pH,b.nextSibling)}if(s[d]){setTimeout(x,160);j(window,"scroll",x)}},f,D=200,E=0,r,u,H=function(){return window.innerWidth||b.clientWidth||e.body.clientWidth};function K(){if(!r)r=setInterval(function(){var a=e.body;if(a[c]<3)a[c]=0;else a[c]=a[c]/1.3;if(b[c]<3)b[c]=0;else b[c]=b[c]/1.3;if(!n()){clearInterval(r);r=null}},14)}function A(){clearTimeout(u);if(n()>D&&H()>E){u=setTimeout(function(){p(f,"mcOut")},60);f[m][l]="block"}else{q(f,"mcOut");u=setTimeout(function(){f[m][l]="none"},500)}}var N=function(){f=e.getElementById("backtop");if(f){var a=f.getAttribute("data-v-w");if(a){a=a.replace(/\s/g,"").split(",");D=parseInt(a[0]);if(a[d]>1)E=parseInt(a[1])}j(f,"click",K);j(window,"scroll",A);A()}},v=function(){for(var c=n(),e=c+window.innerHeight,g=i[d],b,f,a=0;a<g;a++){b=c+z(i[a]),f=b+i[a][k];if(b<e)q(i[a],"slide");else p(i[a],"slide")}},G=function(){if(i[d]){j(window,"scroll",v);v()}},M=function(){I();N();G()};j(window,"load",M);j(document,"touchstart",function(){})};
var version = "1.0";
var formToSend = {};
var _thankYouURL;
var _ip_address = "";
var _apikey = "jkjZJOwDi3ckH2CrJSGu5ymzfF5vX9gA";
var _pagesource = "";
var _opt_in = "1";
var ckm_subid_2;
var ckm_subid_3;
var ckm_subid_4;
var ckm_subid_5;

function queryStringToObject() {
    var pairs = window.location.search.substring(1).split("&"),
        obj = {},
        pair, i;
    for (i in pairs) {
        if (pairs[i] === "") continue;
        pair = pairs[i].split("=");
        obj[decodeURIComponent(pair[0].toLowerCase())] = decodeURIComponent(pair[1]);
    }
    return obj;
}

function VariableManagement() {
    _pagesource = "";
    if ((typeof Source !== 'undefined') && (Source)) {
        _pagesource = Source;
    } else {
        _pagesource = window.location.protocol + "//" + window.location.hostname + window.location.pathname;
    }
    _pagesource = _pagesource + "-" + currentScriptName + version;
    _thankYouURL = (typeof thankYou == 'undefined') ? 'https://www.debt.com/solutions/bib-thank-you' : thankYou;
    _thankYouURL_Unsold = (typeof thankYouURL_Unsold == 'undefined') ? _thankYouURL : thankYouURL_Unsold;
    qstring = queryStringToObject();
    ckm_subid_2 = (typeof qstring.s2 == 'undefined') ? (typeof s2 == 'undefined') ? "" : s2 : qstring.s2;
    ckm_subid_3 = (typeof qstring.s3 == 'undefined') ? (typeof s3 == 'undefined') ? "" : s3 : qstring.s3;
    ckm_subid_4 = (typeof qstring.s4 == 'undefined') ? (typeof s4 == 'undefined') ? "" : s4 : qstring.s4;
    ckm_subid_5 = (typeof qstring.s5 == 'undefined') ? (typeof s5 == 'undefined') ? "" : s5 : qstring.s5;
}

function getClientIpAddress() {
    var _url = "https://api.ipify.org/";
    jQuery.ajax({
        url: _url,
        jsonp: 'callback',
        async: false,
        contentType: "text/plain; charset=utf-8",
        success: function(jsonResponse) {
            _ip_address = jsonResponse;
        },
        error: function(xhr, status, error) {
            console.warn("Error getting client IP address:");
            console.warn(error);
        }
    });
}

function jsonpcallback(rtndata) {};
/*!
 * Chart.js
 * http://chartjs.org/
 * Version: 2.7.2
 *
 * Copyright 2018 Chart.js Contributors
 * Released under the MIT license
 * https://github.com/chartjs/Chart.js/blob/master/LICENSE.md
 */
(function(f) {
    if (typeof exports === "object" && typeof module !== "undefined") {
        module.exports = f()
    } else if (typeof define === "function" && define.amd) {
        define([], f)
    } else {
        var g;
        if (typeof window !== "undefined") {
            g = window
        } else if (typeof global !== "undefined") {
            g = global
        } else if (typeof self !== "undefined") {
            g = self
        } else {
            g = this
        }
        g.Chart = f()
    }
})(function() {
    var define, module, exports;
    return (function() {
        function e(t, n, r) {
            function s(o, u) {
                if (!n[o]) {
                    if (!t[o]) {
                        var a = typeof require == "function" && require;
                        if (!u && a) return a(o, !0);
                        if (i) return i(o, !0);
                        var f = new Error("Cannot find module '" + o + "'");
                        throw f.code = "MODULE_NOT_FOUND", f
                    }
                    var l = n[o] = {
                        exports: {}
                    };
                    t[o][0].call(l.exports, function(e) {
                        var n = t[o][1][e];
                        return s(n ? n : e)
                    }, l, l.exports, e, t, n, r)
                }
                return n[o].exports
            }
            var i = typeof require == "function" && require;
            for (var o = 0; o < r.length; o++) s(r[o]);
            return s
        }
        return e
    })()({
        1: [function(require, module, exports) {}, {}],
        2: [function(require, module, exports) {
            var colorNames = require(6);
            module.exports = {
                getRgba: getRgba,
                getHsla: getHsla,
                getRgb: getRgb,
                getHsl: getHsl,
                getHwb: getHwb,
                getAlpha: getAlpha,
                hexString: hexString,
                rgbString: rgbString,
                rgbaString: rgbaString,
                percentString: percentString,
                percentaString: percentaString,
                hslString: hslString,
                hslaString: hslaString,
                hwbString: hwbString,
                keyword: keyword
            }

            function getRgba(string) {
                if (!string) {
                    return;
                }
                var abbr = /^#([a-fA-F0-9]{3})$/i,
                    hex = /^#([a-fA-F0-9]{6})$/i,
                    rgba = /^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,
                    per = /^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,
                    keyword = /(\w+)/;
                var rgb = [0, 0, 0],
                    a = 1,
                    match = string.match(abbr);
                if (match) {
                    match = match[1];
                    for (var i = 0; i < rgb.length; i++) {
                        rgb[i] = parseInt(match[i] + match[i], 16);
                    }
                } else if (match = string.match(hex)) {
                    match = match[1];
                    for (var i = 0; i < rgb.length; i++) {
                        rgb[i] = parseInt(match.slice(i * 2, i * 2 + 2), 16);
                    }
                } else if (match = string.match(rgba)) {
                    for (var i = 0; i < rgb.length; i++) {
                        rgb[i] = parseInt(match[i + 1]);
                    }
                    a = parseFloat(match[4]);
                } else if (match = string.match(per)) {
                    for (var i = 0; i < rgb.length; i++) {
                        rgb[i] = Math.round(parseFloat(match[i + 1]) * 2.55);
                    }
                    a = parseFloat(match[4]);
                } else if (match = string.match(keyword)) {
                    if (match[1] == "transparent") {
                        return [0, 0, 0, 0];
                    }
                    rgb = colorNames[match[1]];
                    if (!rgb) {
                        return;
                    }
                }
                for (var i = 0; i < rgb.length; i++) {
                    rgb[i] = scale(rgb[i], 0, 255);
                }
                if (!a && a != 0) {
                    a = 1;
                } else {
                    a = scale(a, 0, 1);
                }
                rgb[3] = a;
                return rgb;
            }

            function getHsla(string) {
                if (!string) {
                    return;
                }
                var hsl = /^hsla?\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/;
                var match = string.match(hsl);
                if (match) {
                    var alpha = parseFloat(match[4]);
                    var h = scale(parseInt(match[1]), 0, 360),
                        s = scale(parseFloat(match[2]), 0, 100),
                        l = scale(parseFloat(match[3]), 0, 100),
                        a = scale(isNaN(alpha) ? 1 : alpha, 0, 1);
                    return [h, s, l, a];
                }
            }

            function getHwb(string) {
                if (!string) {
                    return;
                }
                var hwb = /^hwb\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/;
                var match = string.match(hwb);
                if (match) {
                    var alpha = parseFloat(match[4]);
                    var h = scale(parseInt(match[1]), 0, 360),
                        w = scale(parseFloat(match[2]), 0, 100),
                        b = scale(parseFloat(match[3]), 0, 100),
                        a = scale(isNaN(alpha) ? 1 : alpha, 0, 1);
                    return [h, w, b, a];
                }
            }

            function getRgb(string) {
                var rgba = getRgba(string);
                return rgba && rgba.slice(0, 3);
            }

            function getHsl(string) {
                var hsla = getHsla(string);
                return hsla && hsla.slice(0, 3);
            }

            function getAlpha(string) {
                var vals = getRgba(string);
                if (vals) {
                    return vals[3];
                } else if (vals = getHsla(string)) {
                    return vals[3];
                } else if (vals = getHwb(string)) {
                    return vals[3];
                }
            }

            function hexString(rgb) {
                return "#" + hexDouble(rgb[0]) + hexDouble(rgb[1]) + hexDouble(rgb[2]);
            }

            function rgbString(rgba, alpha) {
                if (alpha < 1 || (rgba[3] && rgba[3] < 1)) {
                    return rgbaString(rgba, alpha);
                }
                return "rgb(" + rgba[0] + ", " + rgba[1] + ", " + rgba[2] + ")";
            }

            function rgbaString(rgba, alpha) {
                if (alpha === undefined) {
                    alpha = (rgba[3] !== undefined ? rgba[3] : 1);
                }
                return "rgba(" + rgba[0] + ", " + rgba[1] + ", " + rgba[2] + ", " + alpha + ")";
            }

            function percentString(rgba, alpha) {
                if (alpha < 1 || (rgba[3] && rgba[3] < 1)) {
                    return percentaString(rgba, alpha);
                }
                var r = Math.round(rgba[0] / 255 * 100),
                    g = Math.round(rgba[1] / 255 * 100),
                    b = Math.round(rgba[2] / 255 * 100);
                return "rgb(" + r + "%, " + g + "%, " + b + "%)";
            }

            function percentaString(rgba, alpha) {
                var r = Math.round(rgba[0] / 255 * 100),
                    g = Math.round(rgba[1] / 255 * 100),
                    b = Math.round(rgba[2] / 255 * 100);
                return "rgba(" + r + "%, " + g + "%, " + b + "%, " + (alpha || rgba[3] || 1) + ")";
            }

            function hslString(hsla, alpha) {
                if (alpha < 1 || (hsla[3] && hsla[3] < 1)) {
                    return hslaString(hsla, alpha);
                }
                return "hsl(" + hsla[0] + ", " + hsla[1] + "%, " + hsla[2] + "%)";
            }

            function hslaString(hsla, alpha) {
                if (alpha === undefined) {
                    alpha = (hsla[3] !== undefined ? hsla[3] : 1);
                }
                return "hsla(" + hsla[0] + ", " + hsla[1] + "%, " + hsla[2] + "%, " + alpha + ")";
            }

            function hwbString(hwb, alpha) {
                if (alpha === undefined) {
                    alpha = (hwb[3] !== undefined ? hwb[3] : 1);
                }
                return "hwb(" + hwb[0] + ", " + hwb[1] + "%, " + hwb[2] + "%" + (alpha !== undefined && alpha !== 1 ? ", " + alpha : "") + ")";
            }

            function keyword(rgb) {
                return reverseNames[rgb.slice(0, 3)];
            }

            function scale(num, min, max) {
                return Math.min(Math.max(min, num), max);
            }

            function hexDouble(num) {
                var str = num.toString(16).toUpperCase();
                return (str.length < 2) ? "0" + str : str;
            }
            var reverseNames = {};
            for (var name in colorNames) {
                reverseNames[colorNames[name]] = name;
            }
        }, {
            "6": 6
        }],
        3: [function(require, module, exports) {
            var convert = require(5);
            var string = require(2);
            var Color = function(obj) {
                if (obj instanceof Color) {
                    return obj;
                }
                if (!(this instanceof Color)) {
                    return new Color(obj);
                }
                this.valid = false;
                this.values = {
                    rgb: [0, 0, 0],
                    hsl: [0, 0, 0],
                    hsv: [0, 0, 0],
                    hwb: [0, 0, 0],
                    cmyk: [0, 0, 0, 0],
                    alpha: 1
                };
                var vals;
                if (typeof obj === 'string') {
                    vals = string.getRgba(obj);
                    if (vals) {
                        this.setValues('rgb', vals);
                    } else if (vals = string.getHsla(obj)) {
                        this.setValues('hsl', vals);
                    } else if (vals = string.getHwb(obj)) {
                        this.setValues('hwb', vals);
                    }
                } else if (typeof obj === 'object') {
                    vals = obj;
                    if (vals.r !== undefined || vals.red !== undefined) {
                        this.setValues('rgb', vals);
                    } else if (vals.l !== undefined || vals.lightness !== undefined) {
                        this.setValues('hsl', vals);
                    } else if (vals.v !== undefined || vals.value !== undefined) {
                        this.setValues('hsv', vals);
                    } else if (vals.w !== undefined || vals.whiteness !== undefined) {
                        this.setValues('hwb', vals);
                    } else if (vals.c !== undefined || vals.cyan !== undefined) {
                        this.setValues('cmyk', vals);
                    }
                }
            };
            Color.prototype = {
                isValid: function() {
                    return this.valid;
                },
                rgb: function() {
                    return this.setSpace('rgb', arguments);
                },
                hsl: function() {
                    return this.setSpace('hsl', arguments);
                },
                hsv: function() {
                    return this.setSpace('hsv', arguments);
                },
                hwb: function() {
                    return this.setSpace('hwb', arguments);
                },
                cmyk: function() {
                    return this.setSpace('cmyk', arguments);
                },
                rgbArray: function() {
                    return this.values.rgb;
                },
                hslArray: function() {
                    return this.values.hsl;
                },
                hsvArray: function() {
                    return this.values.hsv;
                },
                hwbArray: function() {
                    var values = this.values;
                    if (values.alpha !== 1) {
                        return values.hwb.concat([values.alpha]);
                    }
                    return values.hwb;
                },
                cmykArray: function() {
                    return this.values.cmyk;
                },
                rgbaArray: function() {
                    var values = this.values;
                    return values.rgb.concat([values.alpha]);
                },
                hslaArray: function() {
                    var values = this.values;
                    return values.hsl.concat([values.alpha]);
                },
                alpha: function(val) {
                    if (val === undefined) {
                        return this.values.alpha;
                    }
                    this.setValues('alpha', val);
                    return this;
                },
                red: function(val) {
                    return this.setChannel('rgb', 0, val);
                },
                green: function(val) {
                    return this.setChannel('rgb', 1, val);
                },
                blue: function(val) {
                    return this.setChannel('rgb', 2, val);
                },
                hue: function(val) {
                    if (val) {
                        val %= 360;
                        val = val < 0 ? 360 + val : val;
                    }
                    return this.setChannel('hsl', 0, val);
                },
                saturation: function(val) {
                    return this.setChannel('hsl', 1, val);
                },
                lightness: function(val) {
                    return this.setChannel('hsl', 2, val);
                },
                saturationv: function(val) {
                    return this.setChannel('hsv', 1, val);
                },
                whiteness: function(val) {
                    return this.setChannel('hwb', 1, val);
                },
                blackness: function(val) {
                    return this.setChannel('hwb', 2, val);
                },
                value: function(val) {
                    return this.setChannel('hsv', 2, val);
                },
                cyan: function(val) {
                    return this.setChannel('cmyk', 0, val);
                },
                magenta: function(val) {
                    return this.setChannel('cmyk', 1, val);
                },
                yellow: function(val) {
                    return this.setChannel('cmyk', 2, val);
                },
                black: function(val) {
                    return this.setChannel('cmyk', 3, val);
                },
                hexString: function() {
                    return string.hexString(this.values.rgb);
                },
                rgbString: function() {
                    return string.rgbString(this.values.rgb, this.values.alpha);
                },
                rgbaString: function() {
                    return string.rgbaString(this.values.rgb, this.values.alpha);
                },
                percentString: function() {
                    return string.percentString(this.values.rgb, this.values.alpha);
                },
                hslString: function() {
                    return string.hslString(this.values.hsl, this.values.alpha);
                },
                hslaString: function() {
                    return string.hslaString(this.values.hsl, this.values.alpha);
                },
                hwbString: function() {
                    return string.hwbString(this.values.hwb, this.values.alpha);
                },
                keyword: function() {
                    return string.keyword(this.values.rgb, this.values.alpha);
                },
                rgbNumber: function() {
                    var rgb = this.values.rgb;
                    return (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
                },
                luminosity: function() {
                    var rgb = this.values.rgb;
                    var lum = [];
                    for (var i = 0; i < rgb.length; i++) {
                        var chan = rgb[i] / 255;
                        lum[i] = (chan <= 0.03928) ? chan / 12.92 : Math.pow(((chan + 0.055) / 1.055), 2.4);
                    }
                    return 0.2126 * lum[0] + 0.7152 * lum[1] + 0.0722 * lum[2];
                },
                contrast: function(color2) {
                    var lum1 = this.luminosity();
                    var lum2 = color2.luminosity();
                    if (lum1 > lum2) {
                        return (lum1 + 0.05) / (lum2 + 0.05);
                    }
                    return (lum2 + 0.05) / (lum1 + 0.05);
                },
                level: function(color2) {
                    var contrastRatio = this.contrast(color2);
                    if (contrastRatio >= 7.1) {
                        return 'AAA';
                    }
                    return (contrastRatio >= 4.5) ? 'AA' : '';
                },
                dark: function() {
                    var rgb = this.values.rgb;
                    var yiq = (rgb[0] * 299 + rgb[1] * 587 + rgb[2] * 114) / 1000;
                    return yiq < 128;
                },
                light: function() {
                    return !this.dark();
                },
                negate: function() {
                    var rgb = [];
                    for (var i = 0; i < 3; i++) {
                        rgb[i] = 255 - this.values.rgb[i];
                    }
                    this.setValues('rgb', rgb);
                    return this;
                },
                lighten: function(ratio) {
                    var hsl = this.values.hsl;
                    hsl[2] += hsl[2] * ratio;
                    this.setValues('hsl', hsl);
                    return this;
                },
                darken: function(ratio) {
                    var hsl = this.values.hsl;
                    hsl[2] -= hsl[2] * ratio;
                    this.setValues('hsl', hsl);
                    return this;
                },
                saturate: function(ratio) {
                    var hsl = this.values.hsl;
                    hsl[1] += hsl[1] * ratio;
                    this.setValues('hsl', hsl);
                    return this;
                },
                desaturate: function(ratio) {
                    var hsl = this.values.hsl;
                    hsl[1] -= hsl[1] * ratio;
                    this.setValues('hsl', hsl);
                    return this;
                },
                whiten: function(ratio) {
                    var hwb = this.values.hwb;
                    hwb[1] += hwb[1] * ratio;
                    this.setValues('hwb', hwb);
                    return this;
                },
                blacken: function(ratio) {
                    var hwb = this.values.hwb;
                    hwb[2] += hwb[2] * ratio;
                    this.setValues('hwb', hwb);
                    return this;
                },
                greyscale: function() {
                    var rgb = this.values.rgb;
                    var val = rgb[0] * 0.3 + rgb[1] * 0.59 + rgb[2] * 0.11;
                    this.setValues('rgb', [val, val, val]);
                    return this;
                },
                clearer: function(ratio) {
                    var alpha = this.values.alpha;
                    this.setValues('alpha', alpha - (alpha * ratio));
                    return this;
                },
                opaquer: function(ratio) {
                    var alpha = this.values.alpha;
                    this.setValues('alpha', alpha + (alpha * ratio));
                    return this;
                },
                rotate: function(degrees) {
                    var hsl = this.values.hsl;
                    var hue = (hsl[0] + degrees) % 360;
                    hsl[0] = hue < 0 ? 360 + hue : hue;
                    this.setValues('hsl', hsl);
                    return this;
                },
                mix: function(mixinColor, weight) {
                    var color1 = this;
                    var color2 = mixinColor;
                    var p = weight === undefined ? 0.5 : weight;
                    var w = 2 * p - 1;
                    var a = color1.alpha() - color2.alpha();
                    var w1 = (((w * a === -1) ? w : (w + a) / (1 + w * a)) + 1) / 2.0;
                    var w2 = 1 - w1;
                    return this.rgb(w1 * color1.red() + w2 * color2.red(), w1 * color1.green() + w2 * color2.green(), w1 * color1.blue() + w2 * color2.blue()).alpha(color1.alpha() * p + color2.alpha() * (1 - p));
                },
                toJSON: function() {
                    return this.rgb();
                },
                clone: function() {
                    var result = new Color();
                    var source = this.values;
                    var target = result.values;
                    var value, type;
                    for (var prop in source) {
                        if (source.hasOwnProperty(prop)) {
                            value = source[prop];
                            type = ({}).toString.call(value);
                            if (type === '[object Array]') {
                                target[prop] = value.slice(0);
                            } else if (type === '[object Number]') {
                                target[prop] = value;
                            } else {
                                console.error('unexpected color value:', value);
                            }
                        }
                    }
                    return result;
                }
            };
            Color.prototype.spaces = {
                rgb: ['red', 'green', 'blue'],
                hsl: ['hue', 'saturation', 'lightness'],
                hsv: ['hue', 'saturation', 'value'],
                hwb: ['hue', 'whiteness', 'blackness'],
                cmyk: ['cyan', 'magenta', 'yellow', 'black']
            };
            Color.prototype.maxes = {
                rgb: [255, 255, 255],
                hsl: [360, 100, 100],
                hsv: [360, 100, 100],
                hwb: [360, 100, 100],
                cmyk: [100, 100, 100, 100]
            };
            Color.prototype.getValues = function(space) {
                var values = this.values;
                var vals = {};
                for (var i = 0; i < space.length; i++) {
                    vals[space.charAt(i)] = values[space][i];
                }
                if (values.alpha !== 1) {
                    vals.a = values.alpha;
                }
                return vals;
            };
            Color.prototype.setValues = function(space, vals) {
                var values = this.values;
                var spaces = this.spaces;
                var maxes = this.maxes;
                var alpha = 1;
                var i;
                this.valid = true;
                if (space === 'alpha') {
                    alpha = vals;
                } else if (vals.length) {
                    values[space] = vals.slice(0, space.length);
                    alpha = vals[space.length];
                } else if (vals[space.charAt(0)] !== undefined) {
                    for (i = 0; i < space.length; i++) {
                        values[space][i] = vals[space.charAt(i)];
                    }
                    alpha = vals.a;
                } else if (vals[spaces[space][0]] !== undefined) {
                    var chans = spaces[space];
                    for (i = 0; i < space.length; i++) {
                        values[space][i] = vals[chans[i]];
                    }
                    alpha = vals.alpha;
                }
                values.alpha = Math.max(0, Math.min(1, (alpha === undefined ? values.alpha : alpha)));
                if (space === 'alpha') {
                    return false;
                }
                var capped;
                for (i = 0; i < space.length; i++) {
                    capped = Math.max(0, Math.min(maxes[space][i], values[space][i]));
                    values[space][i] = Math.round(capped);
                }
                for (var sname in spaces) {
                    if (sname !== space) {
                        values[sname] = convert[space][sname](values[space]);
                    }
                }
                return true;
            };
            Color.prototype.setSpace = function(space, args) {
                var vals = args[0];
                if (vals === undefined) {
                    return this.getValues(space);
                }
                if (typeof vals === 'number') {
                    vals = Array.prototype.slice.call(args);
                }
                this.setValues(space, vals);
                return this;
            };
            Color.prototype.setChannel = function(space, index, val) {
                var svalues = this.values[space];
                if (val === undefined) {
                    return svalues[index];
                } else if (val === svalues[index]) {
                    return this;
                }
                svalues[index] = val;
                this.setValues(space, svalues);
                return this;
            };
            if (typeof window !== 'undefined') {
                window.Color = Color;
            }
            module.exports = Color;
        }, {
            "2": 2,
            "5": 5
        }],
        4: [function(require, module, exports) {
            module.exports = {
                rgb2hsl: rgb2hsl,
                rgb2hsv: rgb2hsv,
                rgb2hwb: rgb2hwb,
                rgb2cmyk: rgb2cmyk,
                rgb2keyword: rgb2keyword,
                rgb2xyz: rgb2xyz,
                rgb2lab: rgb2lab,
                rgb2lch: rgb2lch,
                hsl2rgb: hsl2rgb,
                hsl2hsv: hsl2hsv,
                hsl2hwb: hsl2hwb,
                hsl2cmyk: hsl2cmyk,
                hsl2keyword: hsl2keyword,
                hsv2rgb: hsv2rgb,
                hsv2hsl: hsv2hsl,
                hsv2hwb: hsv2hwb,
                hsv2cmyk: hsv2cmyk,
                hsv2keyword: hsv2keyword,
                hwb2rgb: hwb2rgb,
                hwb2hsl: hwb2hsl,
                hwb2hsv: hwb2hsv,
                hwb2cmyk: hwb2cmyk,
                hwb2keyword: hwb2keyword,
                cmyk2rgb: cmyk2rgb,
                cmyk2hsl: cmyk2hsl,
                cmyk2hsv: cmyk2hsv,
                cmyk2hwb: cmyk2hwb,
                cmyk2keyword: cmyk2keyword,
                keyword2rgb: keyword2rgb,
                keyword2hsl: keyword2hsl,
                keyword2hsv: keyword2hsv,
                keyword2hwb: keyword2hwb,
                keyword2cmyk: keyword2cmyk,
                keyword2lab: keyword2lab,
                keyword2xyz: keyword2xyz,
                xyz2rgb: xyz2rgb,
                xyz2lab: xyz2lab,
                xyz2lch: xyz2lch,
                lab2xyz: lab2xyz,
                lab2rgb: lab2rgb,
                lab2lch: lab2lch,
                lch2lab: lch2lab,
                lch2xyz: lch2xyz,
                lch2rgb: lch2rgb
            }

            function rgb2hsl(rgb) {
                var r = rgb[0] / 255,
                    g = rgb[1] / 255,
                    b = rgb[2] / 255,
                    min = Math.min(r, g, b),
                    max = Math.max(r, g, b),
                    delta = max - min,
                    h, s, l;
                if (max == min)
                    h = 0;
                else if (r == max)
                    h = (g - b) / delta;
                else if (g == max)
                    h = 2 + (b - r) / delta;
                else if (b == max)
                    h = 4 + (r - g) / delta;
                h = Math.min(h * 60, 360);
                if (h < 0)
                    h += 360;
                l = (min + max) / 2;
                if (max == min)
                    s = 0;
                else if (l <= 0.5)
                    s = delta / (max + min);
                else
                    s = delta / (2 - max - min);
                return [h, s * 100, l * 100];
            }

            function rgb2hsv(rgb) {
                var r = rgb[0],
                    g = rgb[1],
                    b = rgb[2],
                    min = Math.min(r, g, b),
                    max = Math.max(r, g, b),
                    delta = max - min,
                    h, s, v;
                if (max == 0)
                    s = 0;
                else
                    s = (delta / max * 1000) / 10;
                if (max == min)
                    h = 0;
                else if (r == max)
                    h = (g - b) / delta;
                else if (g == max)
                    h = 2 + (b - r) / delta;
                else if (b == max)
                    h = 4 + (r - g) / delta;
                h = Math.min(h * 60, 360);
                if (h < 0)
                    h += 360;
                v = ((max / 255) * 1000) / 10;
                return [h, s, v];
            }

            function rgb2hwb(rgb) {
                var r = rgb[0],
                    g = rgb[1],
                    b = rgb[2],
                    h = rgb2hsl(rgb)[0],
                    w = 1 / 255 * Math.min(r, Math.min(g, b)),
                    b = 1 - 1 / 255 * Math.max(r, Math.max(g, b));
                return [h, w * 100, b * 100];
            }

            function rgb2cmyk(rgb) {
                var r = rgb[0] / 255,
                    g = rgb[1] / 255,
                    b = rgb[2] / 255,
                    c, m, y, k;
                k = Math.min(1 - r, 1 - g, 1 - b);
                c = (1 - r - k) / (1 - k) || 0;
                m = (1 - g - k) / (1 - k) || 0;
                y = (1 - b - k) / (1 - k) || 0;
                return [c * 100, m * 100, y * 100, k * 100];
            }

            function rgb2keyword(rgb) {
                return reverseKeywords[JSON.stringify(rgb)];
            }

            function rgb2xyz(rgb) {
                var r = rgb[0] / 255,
                    g = rgb[1] / 255,
                    b = rgb[2] / 255;
                r = r > 0.04045 ? Math.pow(((r + 0.055) / 1.055), 2.4) : (r / 12.92);
                g = g > 0.04045 ? Math.pow(((g + 0.055) / 1.055), 2.4) : (g / 12.92);
                b = b > 0.04045 ? Math.pow(((b + 0.055) / 1.055), 2.4) : (b / 12.92);
                var x = (r * 0.4124) + (g * 0.3576) + (b * 0.1805);
                var y = (r * 0.2126) + (g * 0.7152) + (b * 0.0722);
                var z = (r * 0.0193) + (g * 0.1192) + (b * 0.9505);
                return [x * 100, y * 100, z * 100];
            }

            function rgb2lab(rgb) {
                var xyz = rgb2xyz(rgb),
                    x = xyz[0],
                    y = xyz[1],
                    z = xyz[2],
                    l, a, b;
                x /= 95.047;
                y /= 100;
                z /= 108.883;
                x = x > 0.008856 ? Math.pow(x, 1 / 3) : (7.787 * x) + (16 / 116);
                y = y > 0.008856 ? Math.pow(y, 1 / 3) : (7.787 * y) + (16 / 116);
                z = z > 0.008856 ? Math.pow(z, 1 / 3) : (7.787 * z) + (16 / 116);
                l = (116 * y) - 16;
                a = 500 * (x - y);
                b = 200 * (y - z);
                return [l, a, b];
            }

            function rgb2lch(args) {
                return lab2lch(rgb2lab(args));
            }

            function hsl2rgb(hsl) {
                var h = hsl[0] / 360,
                    s = hsl[1] / 100,
                    l = hsl[2] / 100,
                    t1, t2, t3, rgb, val;
                if (s == 0) {
                    val = l * 255;
                    return [val, val, val];
                }
                if (l < 0.5)
                    t2 = l * (1 + s);
                else
                    t2 = l + s - l * s;
                t1 = 2 * l - t2;
                rgb = [0, 0, 0];
                for (var i = 0; i < 3; i++) {
                    t3 = h + 1 / 3 * -(i - 1);
                    t3 < 0 && t3++;
                    t3 > 1 && t3--;
                    if (6 * t3 < 1)
                        val = t1 + (t2 - t1) * 6 * t3;
                    else if (2 * t3 < 1)
                        val = t2;
                    else if (3 * t3 < 2)
                        val = t1 + (t2 - t1) * (2 / 3 - t3) * 6;
                    else
                        val = t1;
                    rgb[i] = val * 255;
                }
                return rgb;
            }

            function hsl2hsv(hsl) {
                var h = hsl[0],
                    s = hsl[1] / 100,
                    l = hsl[2] / 100,
                    sv, v;
                if (l === 0) {
                    return [0, 0, 0];
                }
                l *= 2;
                s *= (l <= 1) ? l : 2 - l;
                v = (l + s) / 2;
                sv = (2 * s) / (l + s);
                return [h, sv * 100, v * 100];
            }

            function hsl2hwb(args) {
                return rgb2hwb(hsl2rgb(args));
            }

            function hsl2cmyk(args) {
                return rgb2cmyk(hsl2rgb(args));
            }

            function hsl2keyword(args) {
                return rgb2keyword(hsl2rgb(args));
            }

            function hsv2rgb(hsv) {
                var h = hsv[0] / 60,
                    s = hsv[1] / 100,
                    v = hsv[2] / 100,
                    hi = Math.floor(h) % 6;
                var f = h - Math.floor(h),
                    p = 255 * v * (1 - s),
                    q = 255 * v * (1 - (s * f)),
                    t = 255 * v * (1 - (s * (1 - f))),
                    v = 255 * v;
                switch (hi) {
                    case 0:
                        return [v, t, p];
                    case 1:
                        return [q, v, p];
                    case 2:
                        return [p, v, t];
                    case 3:
                        return [p, q, v];
                    case 4:
                        return [t, p, v];
                    case 5:
                        return [v, p, q];
                }
            }

            function hsv2hsl(hsv) {
                var h = hsv[0],
                    s = hsv[1] / 100,
                    v = hsv[2] / 100,
                    sl, l;
                l = (2 - s) * v;
                sl = s * v;
                sl /= (l <= 1) ? l : 2 - l;
                sl = sl || 0;
                l /= 2;
                return [h, sl * 100, l * 100];
            }

            function hsv2hwb(args) {
                return rgb2hwb(hsv2rgb(args))
            }

            function hsv2cmyk(args) {
                return rgb2cmyk(hsv2rgb(args));
            }

            function hsv2keyword(args) {
                return rgb2keyword(hsv2rgb(args));
            }

            function hwb2rgb(hwb) {
                var h = hwb[0] / 360,
                    wh = hwb[1] / 100,
                    bl = hwb[2] / 100,
                    ratio = wh + bl,
                    i, v, f, n;
                if (ratio > 1) {
                    wh /= ratio;
                    bl /= ratio;
                }
                i = Math.floor(6 * h);
                v = 1 - bl;
                f = 6 * h - i;
                if ((i & 0x01) != 0) {
                    f = 1 - f;
                }
                n = wh + f * (v - wh);
                switch (i) {
                    default:
                        case 6:
                        case 0:
                        r = v;g = n;b = wh;
                    break;
                    case 1:
                            r = n;g = v;b = wh;
                        break;
                    case 2:
                            r = wh;g = v;b = n;
                        break;
                    case 3:
                            r = wh;g = n;b = v;
                        break;
                    case 4:
                            r = n;g = wh;b = v;
                        break;
                    case 5:
                            r = v;g = wh;b = n;
                        break;
                }
                return [r * 255, g * 255, b * 255];
            }

            function hwb2hsl(args) {
                return rgb2hsl(hwb2rgb(args));
            }

            function hwb2hsv(args) {
                return rgb2hsv(hwb2rgb(args));
            }

            function hwb2cmyk(args) {
                return rgb2cmyk(hwb2rgb(args));
            }

            function hwb2keyword(args) {
                return rgb2keyword(hwb2rgb(args));
            }

            function cmyk2rgb(cmyk) {
                var c = cmyk[0] / 100,
                    m = cmyk[1] / 100,
                    y = cmyk[2] / 100,
                    k = cmyk[3] / 100,
                    r, g, b;
                r = 1 - Math.min(1, c * (1 - k) + k);
                g = 1 - Math.min(1, m * (1 - k) + k);
                b = 1 - Math.min(1, y * (1 - k) + k);
                return [r * 255, g * 255, b * 255];
            }

            function cmyk2hsl(args) {
                return rgb2hsl(cmyk2rgb(args));
            }

            function cmyk2hsv(args) {
                return rgb2hsv(cmyk2rgb(args));
            }

            function cmyk2hwb(args) {
                return rgb2hwb(cmyk2rgb(args));
            }

            function cmyk2keyword(args) {
                return rgb2keyword(cmyk2rgb(args));
            }

            function xyz2rgb(xyz) {
                var x = xyz[0] / 100,
                    y = xyz[1] / 100,
                    z = xyz[2] / 100,
                    r, g, b;
                r = (x * 3.2406) + (y * -1.5372) + (z * -0.4986);
                g = (x * -0.9689) + (y * 1.8758) + (z * 0.0415);
                b = (x * 0.0557) + (y * -0.2040) + (z * 1.0570);
                r = r > 0.0031308 ? ((1.055 * Math.pow(r, 1.0 / 2.4)) - 0.055) : r = (r * 12.92);
                g = g > 0.0031308 ? ((1.055 * Math.pow(g, 1.0 / 2.4)) - 0.055) : g = (g * 12.92);
                b = b > 0.0031308 ? ((1.055 * Math.pow(b, 1.0 / 2.4)) - 0.055) : b = (b * 12.92);
                r = Math.min(Math.max(0, r), 1);
                g = Math.min(Math.max(0, g), 1);
                b = Math.min(Math.max(0, b), 1);
                return [r * 255, g * 255, b * 255];
            }

            function xyz2lab(xyz) {
                var x = xyz[0],
                    y = xyz[1],
                    z = xyz[2],
                    l, a, b;
                x /= 95.047;
                y /= 100;
                z /= 108.883;
                x = x > 0.008856 ? Math.pow(x, 1 / 3) : (7.787 * x) + (16 / 116);
                y = y > 0.008856 ? Math.pow(y, 1 / 3) : (7.787 * y) + (16 / 116);
                z = z > 0.008856 ? Math.pow(z, 1 / 3) : (7.787 * z) + (16 / 116);
                l = (116 * y) - 16;
                a = 500 * (x - y);
                b = 200 * (y - z);
                return [l, a, b];
            }

            function xyz2lch(args) {
                return lab2lch(xyz2lab(args));
            }

            function lab2xyz(lab) {
                var l = lab[0],
                    a = lab[1],
                    b = lab[2],
                    x, y, z, y2;
                if (l <= 8) {
                    y = (l * 100) / 903.3;
                    y2 = (7.787 * (y / 100)) + (16 / 116);
                } else {
                    y = 100 * Math.pow((l + 16) / 116, 3);
                    y2 = Math.pow(y / 100, 1 / 3);
                }
                x = x / 95.047 <= 0.008856 ? x = (95.047 * ((a / 500) + y2 - (16 / 116))) / 7.787 : 95.047 * Math.pow((a / 500) + y2, 3);
                z = z / 108.883 <= 0.008859 ? z = (108.883 * (y2 - (b / 200) - (16 / 116))) / 7.787 : 108.883 * Math.pow(y2 - (b / 200), 3);
                return [x, y, z];
            }

            function lab2lch(lab) {
                var l = lab[0],
                    a = lab[1],
                    b = lab[2],
                    hr, h, c;
                hr = Math.atan2(b, a);
                h = hr * 360 / 2 / Math.PI;
                if (h < 0) {
                    h += 360;
                }
                c = Math.sqrt(a * a + b * b);
                return [l, c, h];
            }

            function lab2rgb(args) {
                return xyz2rgb(lab2xyz(args));
            }

            function lch2lab(lch) {
                var l = lch[0],
                    c = lch[1],
                    h = lch[2],
                    a, b, hr;
                hr = h / 360 * 2 * Math.PI;
                a = c * Math.cos(hr);
                b = c * Math.sin(hr);
                return [l, a, b];
            }

            function lch2xyz(args) {
                return lab2xyz(lch2lab(args));
            }

            function lch2rgb(args) {
                return lab2rgb(lch2lab(args));
            }

            function keyword2rgb(keyword) {
                return cssKeywords[keyword];
            }

            function keyword2hsl(args) {
                return rgb2hsl(keyword2rgb(args));
            }

            function keyword2hsv(args) {
                return rgb2hsv(keyword2rgb(args));
            }

            function keyword2hwb(args) {
                return rgb2hwb(keyword2rgb(args));
            }

            function keyword2cmyk(args) {
                return rgb2cmyk(keyword2rgb(args));
            }

            function keyword2lab(args) {
                return rgb2lab(keyword2rgb(args));
            }

            function keyword2xyz(args) {
                return rgb2xyz(keyword2rgb(args));
            }
            var cssKeywords = {
                aliceblue: [240, 248, 255],
                antiquewhite: [250, 235, 215],
                aqua: [0, 255, 255],
                aquamarine: [127, 255, 212],
                azure: [240, 255, 255],
                beige: [245, 245, 220],
                bisque: [255, 228, 196],
                black: [0, 0, 0],
                blanchedalmond: [255, 235, 205],
                blue: [0, 0, 255],
                blueviolet: [138, 43, 226],
                brown: [165, 42, 42],
                burlywood: [222, 184, 135],
                cadetblue: [95, 158, 160],
                chartreuse: [127, 255, 0],
                chocolate: [210, 105, 30],
                coral: [255, 127, 80],
                cornflowerblue: [100, 149, 237],
                cornsilk: [255, 248, 220],
                crimson: [220, 20, 60],
                cyan: [0, 255, 255],
                darkblue: [0, 0, 139],
                darkcyan: [0, 139, 139],
                darkgoldenrod: [184, 134, 11],
                darkgray: [169, 169, 169],
                darkgreen: [0, 100, 0],
                darkgrey: [169, 169, 169],
                darkkhaki: [189, 183, 107],
                darkmagenta: [139, 0, 139],
                darkolivegreen: [85, 107, 47],
                darkorange: [255, 140, 0],
                darkorchid: [153, 50, 204],
                darkred: [139, 0, 0],
                darksalmon: [233, 150, 122],
                darkseagreen: [143, 188, 143],
                darkslateblue: [72, 61, 139],
                darkslategray: [47, 79, 79],
                darkslategrey: [47, 79, 79],
                darkturquoise: [0, 206, 209],
                darkviolet: [148, 0, 211],
                deeppink: [255, 20, 147],
                deepskyblue: [0, 191, 255],
                dimgray: [105, 105, 105],
                dimgrey: [105, 105, 105],
                dodgerblue: [30, 144, 255],
                firebrick: [178, 34, 34],
                floralwhite: [255, 250, 240],
                forestgreen: [34, 139, 34],
                fuchsia: [255, 0, 255],
                gainsboro: [220, 220, 220],
                ghostwhite: [248, 248, 255],
                gold: [255, 215, 0],
                goldenrod: [218, 165, 32],
                gray: [128, 128, 128],
                green: [0, 128, 0],
                greenyellow: [173, 255, 47],
                grey: [128, 128, 128],
                honeydew: [240, 255, 240],
                hotpink: [255, 105, 180],
                indianred: [205, 92, 92],
                indigo: [75, 0, 130],
                ivory: [255, 255, 240],
                khaki: [240, 230, 140],
                lavender: [230, 230, 250],
                lavenderblush: [255, 240, 245],
                lawngreen: [124, 252, 0],
                lemonchiffon: [255, 250, 205],
                lightblue: [173, 216, 230],
                lightcoral: [240, 128, 128],
                lightcyan: [224, 255, 255],
                lightgoldenrodyellow: [250, 250, 210],
                lightgray: [211, 211, 211],
                lightgreen: [144, 238, 144],
                lightgrey: [211, 211, 211],
                lightpink: [255, 182, 193],
                lightsalmon: [255, 160, 122],
                lightseagreen: [32, 178, 170],
                lightskyblue: [135, 206, 250],
                lightslategray: [119, 136, 153],
                lightslategrey: [119, 136, 153],
                lightsteelblue: [176, 196, 222],
                lightyellow: [255, 255, 224],
                lime: [0, 255, 0],
                limegreen: [50, 205, 50],
                linen: [250, 240, 230],
                magenta: [255, 0, 255],
                maroon: [128, 0, 0],
                mediumaquamarine: [102, 205, 170],
                mediumblue: [0, 0, 205],
                mediumorchid: [186, 85, 211],
                mediumpurple: [147, 112, 219],
                mediumseagreen: [60, 179, 113],
                mediumslateblue: [123, 104, 238],
                mediumspringgreen: [0, 250, 154],
                mediumturquoise: [72, 209, 204],
                mediumvioletred: [199, 21, 133],
                midnightblue: [25, 25, 112],
                mintcream: [245, 255, 250],
                mistyrose: [255, 228, 225],
                moccasin: [255, 228, 181],
                navajowhite: [255, 222, 173],
                navy: [0, 0, 128],
                oldlace: [253, 245, 230],
                olive: [128, 128, 0],
                olivedrab: [107, 142, 35],
                orange: [255, 165, 0],
                orangered: [255, 69, 0],
                orchid: [218, 112, 214],
                palegoldenrod: [238, 232, 170],
                palegreen: [152, 251, 152],
                paleturquoise: [175, 238, 238],
                palevioletred: [219, 112, 147],
                papayawhip: [255, 239, 213],
                peachpuff: [255, 218, 185],
                peru: [205, 133, 63],
                pink: [255, 192, 203],
                plum: [221, 160, 221],
                powderblue: [176, 224, 230],
                purple: [128, 0, 128],
                rebeccapurple: [102, 51, 153],
                red: [255, 0, 0],
                rosybrown: [188, 143, 143],
                royalblue: [65, 105, 225],
                saddlebrown: [139, 69, 19],
                salmon: [250, 128, 114],
                sandybrown: [244, 164, 96],
                seagreen: [46, 139, 87],
                seashell: [255, 245, 238],
                sienna: [160, 82, 45],
                silver: [192, 192, 192],
                skyblue: [135, 206, 235],
                slateblue: [106, 90, 205],
                slategray: [112, 128, 144],
                slategrey: [112, 128, 144],
                snow: [255, 250, 250],
                springgreen: [0, 255, 127],
                steelblue: [70, 130, 180],
                tan: [210, 180, 140],
                teal: [0, 128, 128],
                thistle: [216, 191, 216],
                tomato: [255, 99, 71],
                turquoise: [64, 224, 208],
                violet: [238, 130, 238],
                wheat: [245, 222, 179],
                white: [255, 255, 255],
                whitesmoke: [245, 245, 245],
                yellow: [255, 255, 0],
                yellowgreen: [154, 205, 50]
            };
            var reverseKeywords = {};
            for (var key in cssKeywords) {
                reverseKeywords[JSON.stringify(cssKeywords[key])] = key;
            }
        }, {}],
        5: [function(require, module, exports) {
            var conversions = require(4);
            var convert = function() {
                return new Converter();
            }
            for (var func in conversions) {
                convert[func + "Raw"] = (function(func) {
                    return function(arg) {
                        if (typeof arg == "number")
                            arg = Array.prototype.slice.call(arguments);
                        return conversions[func](arg);
                    }
                })(func);
                var pair = /(\w+)2(\w+)/.exec(func),
                    from = pair[1],
                    to = pair[2];
                convert[from] = convert[from] || {};
                convert[from][to] = convert[func] = (function(func) {
                    return function(arg) {
                        if (typeof arg == "number")
                            arg = Array.prototype.slice.call(arguments);
                        var val = conversions[func](arg);
                        if (typeof val == "string" || val === undefined)
                            return val;
                        for (var i = 0; i < val.length; i++)
                            val[i] = Math.round(val[i]);
                        return val;
                    }
                })(func);
            }
            var Converter = function() {
                this.convs = {};
            };
            Converter.prototype.routeSpace = function(space, args) {
                var values = args[0];
                if (values === undefined) {
                    return this.getValues(space);
                }
                if (typeof values == "number") {
                    values = Array.prototype.slice.call(args);
                }
                return this.setValues(space, values);
            };
            Converter.prototype.setValues = function(space, values) {
                this.space = space;
                this.convs = {};
                this.convs[space] = values;
                return this;
            };
            Converter.prototype.getValues = function(space) {
                var vals = this.convs[space];
                if (!vals) {
                    var fspace = this.space,
                        from = this.convs[fspace];
                    vals = convert[fspace][space](from);
                    this.convs[space] = vals;
                }
                return vals;
            };
            ["rgb", "hsl", "hsv", "cmyk", "keyword"].forEach(function(space) {
                Converter.prototype[space] = function(vals) {
                    return this.routeSpace(space, arguments);
                }
            });
            module.exports = convert;
        }, {
            "4": 4
        }],
        6: [function(require, module, exports) {
            'use strict'
            module.exports = {
                "aliceblue": [240, 248, 255],
                "antiquewhite": [250, 235, 215],
                "aqua": [0, 255, 255],
                "aquamarine": [127, 255, 212],
                "azure": [240, 255, 255],
                "beige": [245, 245, 220],
                "bisque": [255, 228, 196],
                "black": [0, 0, 0],
                "blanchedalmond": [255, 235, 205],
                "blue": [0, 0, 255],
                "blueviolet": [138, 43, 226],
                "brown": [165, 42, 42],
                "burlywood": [222, 184, 135],
                "cadetblue": [95, 158, 160],
                "chartreuse": [127, 255, 0],
                "chocolate": [210, 105, 30],
                "coral": [255, 127, 80],
                "cornflowerblue": [100, 149, 237],
                "cornsilk": [255, 248, 220],
                "crimson": [220, 20, 60],
                "cyan": [0, 255, 255],
                "darkblue": [0, 0, 139],
                "darkcyan": [0, 139, 139],
                "darkgoldenrod": [184, 134, 11],
                "darkgray": [169, 169, 169],
                "darkgreen": [0, 100, 0],
                "darkgrey": [169, 169, 169],
                "darkkhaki": [189, 183, 107],
                "darkmagenta": [139, 0, 139],
                "darkolivegreen": [85, 107, 47],
                "darkorange": [255, 140, 0],
                "darkorchid": [153, 50, 204],
                "darkred": [139, 0, 0],
                "darksalmon": [233, 150, 122],
                "darkseagreen": [143, 188, 143],
                "darkslateblue": [72, 61, 139],
                "darkslategray": [47, 79, 79],
                "darkslategrey": [47, 79, 79],
                "darkturquoise": [0, 206, 209],
                "darkviolet": [148, 0, 211],
                "deeppink": [255, 20, 147],
                "deepskyblue": [0, 191, 255],
                "dimgray": [105, 105, 105],
                "dimgrey": [105, 105, 105],
                "dodgerblue": [30, 144, 255],
                "firebrick": [178, 34, 34],
                "floralwhite": [255, 250, 240],
                "forestgreen": [34, 139, 34],
                "fuchsia": [255, 0, 255],
                "gainsboro": [220, 220, 220],
                "ghostwhite": [248, 248, 255],
                "gold": [255, 215, 0],
                "goldenrod": [218, 165, 32],
                "gray": [128, 128, 128],
                "green": [0, 128, 0],
                "greenyellow": [173, 255, 47],
                "grey": [128, 128, 128],
                "honeydew": [240, 255, 240],
                "hotpink": [255, 105, 180],
                "indianred": [205, 92, 92],
                "indigo": [75, 0, 130],
                "ivory": [255, 255, 240],
                "khaki": [240, 230, 140],
                "lavender": [230, 230, 250],
                "lavenderblush": [255, 240, 245],
                "lawngreen": [124, 252, 0],
                "lemonchiffon": [255, 250, 205],
                "lightblue": [173, 216, 230],
                "lightcoral": [240, 128, 128],
                "lightcyan": [224, 255, 255],
                "lightgoldenrodyellow": [250, 250, 210],
                "lightgray": [211, 211, 211],
                "lightgreen": [144, 238, 144],
                "lightgrey": [211, 211, 211],
                "lightpink": [255, 182, 193],
                "lightsalmon": [255, 160, 122],
                "lightseagreen": [32, 178, 170],
                "lightskyblue": [135, 206, 250],
                "lightslategray": [119, 136, 153],
                "lightslategrey": [119, 136, 153],
                "lightsteelblue": [176, 196, 222],
                "lightyellow": [255, 255, 224],
                "lime": [0, 255, 0],
                "limegreen": [50, 205, 50],
                "linen": [250, 240, 230],
                "magenta": [255, 0, 255],
                "maroon": [128, 0, 0],
                "mediumaquamarine": [102, 205, 170],
                "mediumblue": [0, 0, 205],
                "mediumorchid": [186, 85, 211],
                "mediumpurple": [147, 112, 219],
                "mediumseagreen": [60, 179, 113],
                "mediumslateblue": [123, 104, 238],
                "mediumspringgreen": [0, 250, 154],
                "mediumturquoise": [72, 209, 204],
                "mediumvioletred": [199, 21, 133],
                "midnightblue": [25, 25, 112],
                "mintcream": [245, 255, 250],
                "mistyrose": [255, 228, 225],
                "moccasin": [255, 228, 181],
                "navajowhite": [255, 222, 173],
                "navy": [0, 0, 128],
                "oldlace": [253, 245, 230],
                "olive": [128, 128, 0],
                "olivedrab": [107, 142, 35],
                "orange": [255, 165, 0],
                "orangered": [255, 69, 0],
                "orchid": [218, 112, 214],
                "palegoldenrod": [238, 232, 170],
                "palegreen": [152, 251, 152],
                "paleturquoise": [175, 238, 238],
                "palevioletred": [219, 112, 147],
                "papayawhip": [255, 239, 213],
                "peachpuff": [255, 218, 185],
                "peru": [205, 133, 63],
                "pink": [255, 192, 203],
                "plum": [221, 160, 221],
                "powderblue": [176, 224, 230],
                "purple": [128, 0, 128],
                "rebeccapurple": [102, 51, 153],
                "red": [255, 0, 0],
                "rosybrown": [188, 143, 143],
                "royalblue": [65, 105, 225],
                "saddlebrown": [139, 69, 19],
                "salmon": [250, 128, 114],
                "sandybrown": [244, 164, 96],
                "seagreen": [46, 139, 87],
                "seashell": [255, 245, 238],
                "sienna": [160, 82, 45],
                "silver": [192, 192, 192],
                "skyblue": [135, 206, 235],
                "slateblue": [106, 90, 205],
                "slategray": [112, 128, 144],
                "slategrey": [112, 128, 144],
                "snow": [255, 250, 250],
                "springgreen": [0, 255, 127],
                "steelblue": [70, 130, 180],
                "tan": [210, 180, 140],
                "teal": [0, 128, 128],
                "thistle": [216, 191, 216],
                "tomato": [255, 99, 71],
                "turquoise": [64, 224, 208],
                "violet": [238, 130, 238],
                "wheat": [245, 222, 179],
                "white": [255, 255, 255],
                "whitesmoke": [245, 245, 245],
                "yellow": [255, 255, 0],
                "yellowgreen": [154, 205, 50]
            };
        }, {}],
        7: [function(require, module, exports) {
            var Chart = require(29)();
            Chart.helpers = require(45);
            require(27)(Chart);
            Chart.defaults = require(25);
            Chart.Element = require(26);
            Chart.elements = require(40);
            Chart.Interaction = require(28);
            Chart.layouts = require(30);
            Chart.platform = require(48);
            Chart.plugins = require(31);
            Chart.Ticks = require(34);
            require(22)(Chart);
            require(23)(Chart);
            require(24)(Chart);
            require(33)(Chart);
            require(32)(Chart);
            require(35)(Chart);
            require(55)(Chart);
            require(53)(Chart);
            require(54)(Chart);
            require(56)(Chart);
            require(57)(Chart);
            require(58)(Chart);
            require(15)(Chart);
            require(16)(Chart);
            require(17)(Chart);
            require(18)(Chart);
            require(19)(Chart);
            require(20)(Chart);
            require(21)(Chart);
            require(8)(Chart);
            require(9)(Chart);
            require(10)(Chart);
            require(11)(Chart);
            require(12)(Chart);
            require(13)(Chart);
            require(14)(Chart);
            var plugins = require(49);
            for (var k in plugins) {
                if (plugins.hasOwnProperty(k)) {
                    Chart.plugins.register(plugins[k]);
                }
            }
            Chart.platform.initialize();
            module.exports = Chart;
            if (typeof window !== 'undefined') {
                window.Chart = Chart;
            }
            Chart.Legend = plugins.legend._element;
            Chart.Title = plugins.title._element;
            Chart.pluginService = Chart.plugins;
            Chart.PluginBase = Chart.Element.extend({});
            Chart.canvasHelpers = Chart.helpers.canvas;
            Chart.layoutService = Chart.layouts;
        }, {
            "10": 10,
            "11": 11,
            "12": 12,
            "13": 13,
            "14": 14,
            "15": 15,
            "16": 16,
            "17": 17,
            "18": 18,
            "19": 19,
            "20": 20,
            "21": 21,
            "22": 22,
            "23": 23,
            "24": 24,
            "25": 25,
            "26": 26,
            "27": 27,
            "28": 28,
            "29": 29,
            "30": 30,
            "31": 31,
            "32": 32,
            "33": 33,
            "34": 34,
            "35": 35,
            "40": 40,
            "45": 45,
            "48": 48,
            "49": 49,
            "53": 53,
            "54": 54,
            "55": 55,
            "56": 56,
            "57": 57,
            "58": 58,
            "8": 8,
            "9": 9
        }],
        8: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                Chart.Bar = function(context, config) {
                    config.type = 'bar';
                    return new Chart(context, config);
                };
            };
        }, {}],
        9: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                Chart.Bubble = function(context, config) {
                    config.type = 'bubble';
                    return new Chart(context, config);
                };
            };
        }, {}],
        10: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                Chart.Doughnut = function(context, config) {
                    config.type = 'doughnut';
                    return new Chart(context, config);
                };
            };
        }, {}],
        11: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                Chart.Line = function(context, config) {
                    config.type = 'line';
                    return new Chart(context, config);
                };
            };
        }, {}],
        12: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                Chart.PolarArea = function(context, config) {
                    config.type = 'polarArea';
                    return new Chart(context, config);
                };
            };
        }, {}],
        13: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                Chart.Radar = function(context, config) {
                    config.type = 'radar';
                    return new Chart(context, config);
                };
            };
        }, {}],
        14: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                Chart.Scatter = function(context, config) {
                    config.type = 'scatter';
                    return new Chart(context, config);
                };
            };
        }, {}],
        15: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var elements = require(40);
            var helpers = require(45);
            defaults._set('bar', {
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        type: 'category',
                        categoryPercentage: 0.8,
                        barPercentage: 0.9,
                        offset: true,
                        gridLines: {
                            offsetGridLines: true
                        }
                    }],
                    yAxes: [{
                        type: 'linear'
                    }]
                }
            });
            defaults._set('horizontalBar', {
                hover: {
                    mode: 'index',
                    axis: 'y'
                },
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom'
                    }],
                    yAxes: [{
                        position: 'left',
                        type: 'category',
                        categoryPercentage: 0.8,
                        barPercentage: 0.9,
                        offset: true,
                        gridLines: {
                            offsetGridLines: true
                        }
                    }]
                },
                elements: {
                    rectangle: {
                        borderSkipped: 'left'
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function(item, data) {
                            var title = '';
                            if (item.length > 0) {
                                if (item[0].yLabel) {
                                    title = item[0].yLabel;
                                } else if (data.labels.length > 0 && item[0].index < data.labels.length) {
                                    title = data.labels[item[0].index];
                                }
                            }
                            return title;
                        },
                        label: function(item, data) {
                            var datasetLabel = data.datasets[item.datasetIndex].label || '';
                            return datasetLabel + ': ' + item.xLabel;
                        }
                    },
                    mode: 'index',
                    axis: 'y'
                }
            });

            function computeMinSampleSize(scale, pixels) {
                var min = scale.isHorizontal() ? scale.width : scale.height;
                var ticks = scale.getTicks();
                var prev, curr, i, ilen;
                for (i = 1, ilen = pixels.length; i < ilen; ++i) {
                    min = Math.min(min, pixels[i] - pixels[i - 1]);
                }
                for (i = 0, ilen = ticks.length; i < ilen; ++i) {
                    curr = scale.getPixelForTick(i);
                    min = i > 0 ? Math.min(min, curr - prev) : min;
                    prev = curr;
                }
                return min;
            }

            function computeFitCategoryTraits(index, ruler, options) {
                var thickness = options.barThickness;
                var count = ruler.stackCount;
                var curr = ruler.pixels[index];
                var size, ratio;
                if (helpers.isNullOrUndef(thickness)) {
                    size = ruler.min * options.categoryPercentage;
                    ratio = options.barPercentage;
                } else {
                    size = thickness * count;
                    ratio = 1;
                }
                return {
                    chunk: size / count,
                    ratio: ratio,
                    start: curr - (size / 2)
                };
            }

            function computeFlexCategoryTraits(index, ruler, options) {
                var pixels = ruler.pixels;
                var curr = pixels[index];
                var prev = index > 0 ? pixels[index - 1] : null;
                var next = index < pixels.length - 1 ? pixels[index + 1] : null;
                var percent = options.categoryPercentage;
                var start, size;
                if (prev === null) {
                    prev = curr - (next === null ? ruler.end - curr : next - curr);
                }
                if (next === null) {
                    next = curr + curr - prev;
                }
                start = curr - ((curr - prev) / 2) * percent;
                size = ((next - prev) / 2) * percent;
                return {
                    chunk: size / ruler.stackCount,
                    ratio: options.barPercentage,
                    start: start
                };
            }
            module.exports = function(Chart) {
                Chart.controllers.bar = Chart.DatasetController.extend({
                    dataElementType: elements.Rectangle,
                    initialize: function() {
                        var me = this;
                        var meta;
                        Chart.DatasetController.prototype.initialize.apply(me, arguments);
                        meta = me.getMeta();
                        meta.stack = me.getDataset().stack;
                        meta.bar = true;
                    },
                    update: function(reset) {
                        var me = this;
                        var rects = me.getMeta().data;
                        var i, ilen;
                        me._ruler = me.getRuler();
                        for (i = 0, ilen = rects.length; i < ilen; ++i) {
                            me.updateElement(rects[i], i, reset);
                        }
                    },
                    updateElement: function(rectangle, index, reset) {
                        var me = this;
                        var chart = me.chart;
                        var meta = me.getMeta();
                        var dataset = me.getDataset();
                        var custom = rectangle.custom || {};
                        var rectangleOptions = chart.options.elements.rectangle;
                        rectangle._xScale = me.getScaleForId(meta.xAxisID);
                        rectangle._yScale = me.getScaleForId(meta.yAxisID);
                        rectangle._datasetIndex = me.index;
                        rectangle._index = index;
                        rectangle._model = {
                            datasetLabel: dataset.label,
                            label: chart.data.labels[index],
                            borderSkipped: custom.borderSkipped ? custom.borderSkipped : rectangleOptions.borderSkipped,
                            backgroundColor: custom.backgroundColor ? custom.backgroundColor : helpers.valueAtIndexOrDefault(dataset.backgroundColor, index, rectangleOptions.backgroundColor),
                            borderColor: custom.borderColor ? custom.borderColor : helpers.valueAtIndexOrDefault(dataset.borderColor, index, rectangleOptions.borderColor),
                            borderWidth: custom.borderWidth ? custom.borderWidth : helpers.valueAtIndexOrDefault(dataset.borderWidth, index, rectangleOptions.borderWidth)
                        };
                        me.updateElementGeometry(rectangle, index, reset);
                        rectangle.pivot();
                    },
                    updateElementGeometry: function(rectangle, index, reset) {
                        var me = this;
                        var model = rectangle._model;
                        var vscale = me.getValueScale();
                        var base = vscale.getBasePixel();
                        var horizontal = vscale.isHorizontal();
                        var ruler = me._ruler || me.getRuler();
                        var vpixels = me.calculateBarValuePixels(me.index, index);
                        var ipixels = me.calculateBarIndexPixels(me.index, index, ruler);
                        model.horizontal = horizontal;
                        model.base = reset ? base : vpixels.base;
                        model.x = horizontal ? reset ? base : vpixels.head : ipixels.center;
                        model.y = horizontal ? ipixels.center : reset ? base : vpixels.head;
                        model.height = horizontal ? ipixels.size : undefined;
                        model.width = horizontal ? undefined : ipixels.size;
                    },
                    getValueScaleId: function() {
                        return this.getMeta().yAxisID;
                    },
                    getIndexScaleId: function() {
                        return this.getMeta().xAxisID;
                    },
                    getValueScale: function() {
                        return this.getScaleForId(this.getValueScaleId());
                    },
                    getIndexScale: function() {
                        return this.getScaleForId(this.getIndexScaleId());
                    },
                    _getStacks: function(last) {
                        var me = this;
                        var chart = me.chart;
                        var scale = me.getIndexScale();
                        var stacked = scale.options.stacked;
                        var ilen = last === undefined ? chart.data.datasets.length : last + 1;
                        var stacks = [];
                        var i, meta;
                        for (i = 0; i < ilen; ++i) {
                            meta = chart.getDatasetMeta(i);
                            if (meta.bar && chart.isDatasetVisible(i) && (stacked === false || (stacked === true && stacks.indexOf(meta.stack) === -1) || (stacked === undefined && (meta.stack === undefined || stacks.indexOf(meta.stack) === -1)))) {
                                stacks.push(meta.stack);
                            }
                        }
                        return stacks;
                    },
                    getStackCount: function() {
                        return this._getStacks().length;
                    },
                    getStackIndex: function(datasetIndex, name) {
                        var stacks = this._getStacks(datasetIndex);
                        var index = (name !== undefined) ? stacks.indexOf(name) : -1;
                        return (index === -1) ? stacks.length - 1 : index;
                    },
                    getRuler: function() {
                        var me = this;
                        var scale = me.getIndexScale();
                        var stackCount = me.getStackCount();
                        var datasetIndex = me.index;
                        var isHorizontal = scale.isHorizontal();
                        var start = isHorizontal ? scale.left : scale.top;
                        var end = start + (isHorizontal ? scale.width : scale.height);
                        var pixels = [];
                        var i, ilen, min;
                        for (i = 0, ilen = me.getMeta().data.length; i < ilen; ++i) {
                            pixels.push(scale.getPixelForValue(null, i, datasetIndex));
                        }
                        min = helpers.isNullOrUndef(scale.options.barThickness) ? computeMinSampleSize(scale, pixels) : -1;
                        return {
                            min: min,
                            pixels: pixels,
                            start: start,
                            end: end,
                            stackCount: stackCount,
                            scale: scale
                        };
                    },
                    calculateBarValuePixels: function(datasetIndex, index) {
                        var me = this;
                        var chart = me.chart;
                        var meta = me.getMeta();
                        var scale = me.getValueScale();
                        var datasets = chart.data.datasets;
                        var value = scale.getRightValue(datasets[datasetIndex].data[index]);
                        var stacked = scale.options.stacked;
                        var stack = meta.stack;
                        var start = 0;
                        var i, imeta, ivalue, base, head, size;
                        if (stacked || (stacked === undefined && stack !== undefined)) {
                            for (i = 0; i < datasetIndex; ++i) {
                                imeta = chart.getDatasetMeta(i);
                                if (imeta.bar && imeta.stack === stack && imeta.controller.getValueScaleId() === scale.id && chart.isDatasetVisible(i)) {
                                    ivalue = scale.getRightValue(datasets[i].data[index]);
                                    if ((value < 0 && ivalue < 0) || (value >= 0 && ivalue > 0)) {
                                        start += ivalue;
                                    }
                                }
                            }
                        }
                        base = scale.getPixelForValue(start);
                        head = scale.getPixelForValue(start + value);
                        size = (head - base) / 2;
                        return {
                            size: size,
                            base: base,
                            head: head,
                            center: head + size / 2
                        };
                    },
                    calculateBarIndexPixels: function(datasetIndex, index, ruler) {
                        var me = this;
                        var options = ruler.scale.options;
                        var range = options.barThickness === 'flex' ? computeFlexCategoryTraits(index, ruler, options) : computeFitCategoryTraits(index, ruler, options);
                        var stackIndex = me.getStackIndex(datasetIndex, me.getMeta().stack);
                        var center = range.start + (range.chunk * stackIndex) + (range.chunk / 2);
                        var size = Math.min(helpers.valueOrDefault(options.maxBarThickness, Infinity), range.chunk * range.ratio);
                        return {
                            base: center - size / 2,
                            head: center + size / 2,
                            center: center,
                            size: size
                        };
                    },
                    draw: function() {
                        var me = this;
                        var chart = me.chart;
                        var scale = me.getValueScale();
                        var rects = me.getMeta().data;
                        var dataset = me.getDataset();
                        var ilen = rects.length;
                        var i = 0;
                        helpers.canvas.clipArea(chart.ctx, chart.chartArea);
                        for (; i < ilen; ++i) {
                            if (!isNaN(scale.getRightValue(dataset.data[i]))) {
                                rects[i].draw();
                            }
                        }
                        helpers.canvas.unclipArea(chart.ctx);
                    },
                    setHoverStyle: function(rectangle) {
                        var dataset = this.chart.data.datasets[rectangle._datasetIndex];
                        var index = rectangle._index;
                        var custom = rectangle.custom || {};
                        var model = rectangle._model;
                        model.backgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : helpers.valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, helpers.getHoverColor(model.backgroundColor));
                        model.borderColor = custom.hoverBorderColor ? custom.hoverBorderColor : helpers.valueAtIndexOrDefault(dataset.hoverBorderColor, index, helpers.getHoverColor(model.borderColor));
                        model.borderWidth = custom.hoverBorderWidth ? custom.hoverBorderWidth : helpers.valueAtIndexOrDefault(dataset.hoverBorderWidth, index, model.borderWidth);
                    },
                    removeHoverStyle: function(rectangle) {
                        var dataset = this.chart.data.datasets[rectangle._datasetIndex];
                        var index = rectangle._index;
                        var custom = rectangle.custom || {};
                        var model = rectangle._model;
                        var rectangleElementOptions = this.chart.options.elements.rectangle;
                        model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : helpers.valueAtIndexOrDefault(dataset.backgroundColor, index, rectangleElementOptions.backgroundColor);
                        model.borderColor = custom.borderColor ? custom.borderColor : helpers.valueAtIndexOrDefault(dataset.borderColor, index, rectangleElementOptions.borderColor);
                        model.borderWidth = custom.borderWidth ? custom.borderWidth : helpers.valueAtIndexOrDefault(dataset.borderWidth, index, rectangleElementOptions.borderWidth);
                    }
                });
                Chart.controllers.horizontalBar = Chart.controllers.bar.extend({
                    getValueScaleId: function() {
                        return this.getMeta().xAxisID;
                    },
                    getIndexScaleId: function() {
                        return this.getMeta().yAxisID;
                    }
                });
            };
        }, {
            "25": 25,
            "40": 40,
            "45": 45
        }],
        16: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var elements = require(40);
            var helpers = require(45);
            defaults._set('bubble', {
                hover: {
                    mode: 'single'
                },
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom',
                        id: 'x-axis-0'
                    }],
                    yAxes: [{
                        type: 'linear',
                        position: 'left',
                        id: 'y-axis-0'
                    }]
                },
                tooltips: {
                    callbacks: {
                        title: function() {
                            return '';
                        },
                        label: function(item, data) {
                            var datasetLabel = data.datasets[item.datasetIndex].label || '';
                            var dataPoint = data.datasets[item.datasetIndex].data[item.index];
                            return datasetLabel + ': (' + item.xLabel + ', ' + item.yLabel + ', ' + dataPoint.r + ')';
                        }
                    }
                }
            });
            module.exports = function(Chart) {
                Chart.controllers.bubble = Chart.DatasetController.extend({
                    dataElementType: elements.Point,
                    update: function(reset) {
                        var me = this;
                        var meta = me.getMeta();
                        var points = meta.data;
                        helpers.each(points, function(point, index) {
                            me.updateElement(point, index, reset);
                        });
                    },
                    updateElement: function(point, index, reset) {
                        var me = this;
                        var meta = me.getMeta();
                        var custom = point.custom || {};
                        var xScale = me.getScaleForId(meta.xAxisID);
                        var yScale = me.getScaleForId(meta.yAxisID);
                        var options = me._resolveElementOptions(point, index);
                        var data = me.getDataset().data[index];
                        var dsIndex = me.index;
                        var x = reset ? xScale.getPixelForDecimal(0.5) : xScale.getPixelForValue(typeof data === 'object' ? data : NaN, index, dsIndex);
                        var y = reset ? yScale.getBasePixel() : yScale.getPixelForValue(data, index, dsIndex);
                        point._xScale = xScale;
                        point._yScale = yScale;
                        point._options = options;
                        point._datasetIndex = dsIndex;
                        point._index = index;
                        point._model = {
                            backgroundColor: options.backgroundColor,
                            borderColor: options.borderColor,
                            borderWidth: options.borderWidth,
                            hitRadius: options.hitRadius,
                            pointStyle: options.pointStyle,
                            radius: reset ? 0 : options.radius,
                            skip: custom.skip || isNaN(x) || isNaN(y),
                            x: x,
                            y: y,
                        };
                        point.pivot();
                    },
                    setHoverStyle: function(point) {
                        var model = point._model;
                        var options = point._options;
                        model.backgroundColor = helpers.valueOrDefault(options.hoverBackgroundColor, helpers.getHoverColor(options.backgroundColor));
                        model.borderColor = helpers.valueOrDefault(options.hoverBorderColor, helpers.getHoverColor(options.borderColor));
                        model.borderWidth = helpers.valueOrDefault(options.hoverBorderWidth, options.borderWidth);
                        model.radius = options.radius + options.hoverRadius;
                    },
                    removeHoverStyle: function(point) {
                        var model = point._model;
                        var options = point._options;
                        model.backgroundColor = options.backgroundColor;
                        model.borderColor = options.borderColor;
                        model.borderWidth = options.borderWidth;
                        model.radius = options.radius;
                    },
                    _resolveElementOptions: function(point, index) {
                        var me = this;
                        var chart = me.chart;
                        var datasets = chart.data.datasets;
                        var dataset = datasets[me.index];
                        var custom = point.custom || {};
                        var options = chart.options.elements.point;
                        var resolve = helpers.options.resolve;
                        var data = dataset.data[index];
                        var values = {};
                        var i, ilen, key;
                        var context = {
                            chart: chart,
                            dataIndex: index,
                            dataset: dataset,
                            datasetIndex: me.index
                        };
                        var keys = ['backgroundColor', 'borderColor', 'borderWidth', 'hoverBackgroundColor', 'hoverBorderColor', 'hoverBorderWidth', 'hoverRadius', 'hitRadius', 'pointStyle'];
                        for (i = 0, ilen = keys.length; i < ilen; ++i) {
                            key = keys[i];
                            values[key] = resolve([custom[key], dataset[key], options[key]], context, index);
                        }
                        values.radius = resolve([custom.radius, data ? data.r : undefined, dataset.radius, options.radius], context, index);
                        return values;
                    }
                });
            };
        }, {
            "25": 25,
            "40": 40,
            "45": 45
        }],
        17: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var elements = require(40);
            var helpers = require(45);
            defaults._set('doughnut', {
                animation: {
                    animateRotate: true,
                    animateScale: false
                },
                hover: {
                    mode: 'single'
                },
                legendCallback: function(chart) {
                    var text = [];
                    text.push('<ul class="' + chart.id + '-legend">');
                    var data = chart.data;
                    var datasets = data.datasets;
                    var labels = data.labels;
                    if (datasets.length) {
                        for (var i = 0; i < datasets[0].data.length; ++i) {
                            text.push('<li><span style="background-color:' + datasets[0].backgroundColor[i] + '"></span>');
                            if (labels[i]) {
                                text.push(labels[i]);
                            }
                            text.push('</li>');
                        }
                    }
                    text.push('</ul>');
                    return text.join('');
                },
                legend: {
                    labels: {
                        generateLabels: function(chart) {
                            var data = chart.data;
                            if (data.labels.length && data.datasets.length) {
                                return data.labels.map(function(label, i) {
                                    var meta = chart.getDatasetMeta(0);
                                    var ds = data.datasets[0];
                                    var arc = meta.data[i];
                                    var custom = arc && arc.custom || {};
                                    var valueAtIndexOrDefault = helpers.valueAtIndexOrDefault;
                                    var arcOpts = chart.options.elements.arc;
                                    var fill = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                                    var stroke = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                                    var bw = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
                                    return {
                                        text: label,
                                        fillStyle: fill,
                                        strokeStyle: stroke,
                                        lineWidth: bw,
                                        hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                                        index: i
                                    };
                                });
                            }
                            return [];
                        }
                    },
                    onClick: function(e, legendItem) {
                        var index = legendItem.index;
                        var chart = this.chart;
                        var i, ilen, meta;
                        for (i = 0, ilen = (chart.data.datasets || []).length; i < ilen; ++i) {
                            meta = chart.getDatasetMeta(i);
                            if (meta.data[index]) {
                                meta.data[index].hidden = !meta.data[index].hidden;
                            }
                        }
                        chart.update();
                    }
                },
                cutoutPercentage: 50,
                rotation: Math.PI * -0.5,
                circumference: Math.PI * 2.0,
                tooltips: {
                    callbacks: {
                        title: function() {
                            return '';
                        },
                        label: function(tooltipItem, data) {
                            var dataLabel = data.labels[tooltipItem.index];
                            var value = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            if (helpers.isArray(dataLabel)) {
                                dataLabel = dataLabel.slice();
                                dataLabel[0] += value;
                            } else {
                                dataLabel += value;
                            }
                            return dataLabel;
                        }
                    }
                }
            });
            defaults._set('pie', helpers.clone(defaults.doughnut));
            defaults._set('pie', {
                cutoutPercentage: 0
            });
            module.exports = function(Chart) {
                Chart.controllers.doughnut = Chart.controllers.pie = Chart.DatasetController.extend({
                    dataElementType: elements.Arc,
                    linkScales: helpers.noop,
                    getRingIndex: function(datasetIndex) {
                        var ringIndex = 0;
                        for (var j = 0; j < datasetIndex; ++j) {
                            if (this.chart.isDatasetVisible(j)) {
                                ++ringIndex;
                            }
                        }
                        return ringIndex;
                    },
                    update: function(reset) {
                        var me = this;
                        var chart = me.chart;
                        var chartArea = chart.chartArea;
                        var opts = chart.options;
                        var arcOpts = opts.elements.arc;
                        var availableWidth = chartArea.right - chartArea.left - arcOpts.borderWidth;
                        var availableHeight = chartArea.bottom - chartArea.top - arcOpts.borderWidth;
                        var minSize = Math.min(availableWidth, availableHeight);
                        var offset = {
                            x: 0,
                            y: 0
                        };
                        var meta = me.getMeta();
                        var cutoutPercentage = opts.cutoutPercentage;
                        var circumference = opts.circumference;
                        if (circumference < Math.PI * 2.0) {
                            var startAngle = opts.rotation % (Math.PI * 2.0);
                            startAngle += Math.PI * 2.0 * (startAngle >= Math.PI ? -1 : startAngle < -Math.PI ? 1 : 0);
                            var endAngle = startAngle + circumference;
                            var start = {
                                x: Math.cos(startAngle),
                                y: Math.sin(startAngle)
                            };
                            var end = {
                                x: Math.cos(endAngle),
                                y: Math.sin(endAngle)
                            };
                            var contains0 = (startAngle <= 0 && endAngle >= 0) || (startAngle <= Math.PI * 2.0 && Math.PI * 2.0 <= endAngle);
                            var contains90 = (startAngle <= Math.PI * 0.5 && Math.PI * 0.5 <= endAngle) || (startAngle <= Math.PI * 2.5 && Math.PI * 2.5 <= endAngle);
                            var contains180 = (startAngle <= -Math.PI && -Math.PI <= endAngle) || (startAngle <= Math.PI && Math.PI <= endAngle);
                            var contains270 = (startAngle <= -Math.PI * 0.5 && -Math.PI * 0.5 <= endAngle) || (startAngle <= Math.PI * 1.5 && Math.PI * 1.5 <= endAngle);
                            var cutout = cutoutPercentage / 100.0;
                            var min = {
                                x: contains180 ? -1 : Math.min(start.x * (start.x < 0 ? 1 : cutout), end.x * (end.x < 0 ? 1 : cutout)),
                                y: contains270 ? -1 : Math.min(start.y * (start.y < 0 ? 1 : cutout), end.y * (end.y < 0 ? 1 : cutout))
                            };
                            var max = {
                                x: contains0 ? 1 : Math.max(start.x * (start.x > 0 ? 1 : cutout), end.x * (end.x > 0 ? 1 : cutout)),
                                y: contains90 ? 1 : Math.max(start.y * (start.y > 0 ? 1 : cutout), end.y * (end.y > 0 ? 1 : cutout))
                            };
                            var size = {
                                width: (max.x - min.x) * 0.5,
                                height: (max.y - min.y) * 0.5
                            };
                            minSize = Math.min(availableWidth / size.width, availableHeight / size.height);
                            offset = {
                                x: (max.x + min.x) * -0.5,
                                y: (max.y + min.y) * -0.5
                            };
                        }
                        chart.borderWidth = me.getMaxBorderWidth(meta.data);
                        chart.outerRadius = Math.max((minSize - chart.borderWidth) / 2, 0);
                        chart.innerRadius = Math.max(cutoutPercentage ? (chart.outerRadius / 100) * (cutoutPercentage) : 0, 0);
                        chart.radiusLength = (chart.outerRadius - chart.innerRadius) / chart.getVisibleDatasetCount();
                        chart.offsetX = offset.x * chart.outerRadius;
                        chart.offsetY = offset.y * chart.outerRadius;
                        meta.total = me.calculateTotal();
                        me.outerRadius = chart.outerRadius - (chart.radiusLength * me.getRingIndex(me.index));
                        me.innerRadius = Math.max(me.outerRadius - chart.radiusLength, 0);
                        helpers.each(meta.data, function(arc, index) {
                            me.updateElement(arc, index, reset);
                        });
                    },
                    updateElement: function(arc, index, reset) {
                        var me = this;
                        var chart = me.chart;
                        var chartArea = chart.chartArea;
                        var opts = chart.options;
                        var animationOpts = opts.animation;
                        var centerX = (chartArea.left + chartArea.right) / 2;
                        var centerY = (chartArea.top + chartArea.bottom) / 2;
                        var startAngle = opts.rotation;
                        var endAngle = opts.rotation;
                        var dataset = me.getDataset();
                        var circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : me.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI));
                        var innerRadius = reset && animationOpts.animateScale ? 0 : me.innerRadius;
                        var outerRadius = reset && animationOpts.animateScale ? 0 : me.outerRadius;
                        var valueAtIndexOrDefault = helpers.valueAtIndexOrDefault;
                        helpers.extend(arc, {
                            _datasetIndex: me.index,
                            _index: index,
                            _model: {
                                x: centerX + chart.offsetX,
                                y: centerY + chart.offsetY,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                circumference: circumference,
                                outerRadius: outerRadius,
                                innerRadius: innerRadius,
                                label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
                            }
                        });
                        var model = arc._model;
                        this.removeHoverStyle(arc);
                        if (!reset || !animationOpts.animateRotate) {
                            if (index === 0) {
                                model.startAngle = opts.rotation;
                            } else {
                                model.startAngle = me.getMeta().data[index - 1]._model.endAngle;
                            }
                            model.endAngle = model.startAngle + model.circumference;
                        }
                        arc.pivot();
                    },
                    removeHoverStyle: function(arc) {
                        Chart.DatasetController.prototype.removeHoverStyle.call(this, arc, this.chart.options.elements.arc);
                    },
                    calculateTotal: function() {
                        var dataset = this.getDataset();
                        var meta = this.getMeta();
                        var total = 0;
                        var value;
                        helpers.each(meta.data, function(element, index) {
                            value = dataset.data[index];
                            if (!isNaN(value) && !element.hidden) {
                                total += Math.abs(value);
                            }
                        });
                        return total;
                    },
                    calculateCircumference: function(value) {
                        var total = this.getMeta().total;
                        if (total > 0 && !isNaN(value)) {
                            return (Math.PI * 2.0) * (Math.abs(value) / total);
                        }
                        return 0;
                    },
                    getMaxBorderWidth: function(arcs) {
                        var max = 0;
                        var index = this.index;
                        var length = arcs.length;
                        var borderWidth;
                        var hoverWidth;
                        for (var i = 0; i < length; i++) {
                            borderWidth = arcs[i]._model ? arcs[i]._model.borderWidth : 0;
                            hoverWidth = arcs[i]._chart ? arcs[i]._chart.config.data.datasets[index].hoverBorderWidth : 0;
                            max = borderWidth > max ? borderWidth : max;
                            max = hoverWidth > max ? hoverWidth : max;
                        }
                        return max;
                    }
                });
            };
        }, {
            "25": 25,
            "40": 40,
            "45": 45
        }],
        18: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var elements = require(40);
            var helpers = require(45);
            defaults._set('line', {
                showLines: true,
                spanGaps: false,
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        type: 'category',
                        id: 'x-axis-0'
                    }],
                    yAxes: [{
                        type: 'linear',
                        id: 'y-axis-0'
                    }]
                }
            });
            module.exports = function(Chart) {
                function lineEnabled(dataset, options) {
                    return helpers.valueOrDefault(dataset.showLine, options.showLines);
                }
                Chart.controllers.line = Chart.DatasetController.extend({
                    datasetElementType: elements.Line,
                    dataElementType: elements.Point,
                    update: function(reset) {
                        var me = this;
                        var meta = me.getMeta();
                        var line = meta.dataset;
                        var points = meta.data || [];
                        var options = me.chart.options;
                        var lineElementOptions = options.elements.line;
                        var scale = me.getScaleForId(meta.yAxisID);
                        var i, ilen, custom;
                        var dataset = me.getDataset();
                        var showLine = lineEnabled(dataset, options);
                        if (showLine) {
                            custom = line.custom || {};
                            if ((dataset.tension !== undefined) && (dataset.lineTension === undefined)) {
                                dataset.lineTension = dataset.tension;
                            }
                            line._scale = scale;
                            line._datasetIndex = me.index;
                            line._children = points;
                            line._model = {
                                spanGaps: dataset.spanGaps ? dataset.spanGaps : options.spanGaps,
                                tension: custom.tension ? custom.tension : helpers.valueOrDefault(dataset.lineTension, lineElementOptions.tension),
                                backgroundColor: custom.backgroundColor ? custom.backgroundColor : (dataset.backgroundColor || lineElementOptions.backgroundColor),
                                borderWidth: custom.borderWidth ? custom.borderWidth : (dataset.borderWidth || lineElementOptions.borderWidth),
                                borderColor: custom.borderColor ? custom.borderColor : (dataset.borderColor || lineElementOptions.borderColor),
                                borderCapStyle: custom.borderCapStyle ? custom.borderCapStyle : (dataset.borderCapStyle || lineElementOptions.borderCapStyle),
                                borderDash: custom.borderDash ? custom.borderDash : (dataset.borderDash || lineElementOptions.borderDash),
                                borderDashOffset: custom.borderDashOffset ? custom.borderDashOffset : (dataset.borderDashOffset || lineElementOptions.borderDashOffset),
                                borderJoinStyle: custom.borderJoinStyle ? custom.borderJoinStyle : (dataset.borderJoinStyle || lineElementOptions.borderJoinStyle),
                                fill: custom.fill ? custom.fill : (dataset.fill !== undefined ? dataset.fill : lineElementOptions.fill),
                                steppedLine: custom.steppedLine ? custom.steppedLine : helpers.valueOrDefault(dataset.steppedLine, lineElementOptions.stepped),
                                cubicInterpolationMode: custom.cubicInterpolationMode ? custom.cubicInterpolationMode : helpers.valueOrDefault(dataset.cubicInterpolationMode, lineElementOptions.cubicInterpolationMode),
                            };
                            line.pivot();
                        }
                        for (i = 0, ilen = points.length; i < ilen; ++i) {
                            me.updateElement(points[i], i, reset);
                        }
                        if (showLine && line._model.tension !== 0) {
                            me.updateBezierControlPoints();
                        }
                        for (i = 0, ilen = points.length; i < ilen; ++i) {
                            points[i].pivot();
                        }
                    },
                    getPointBackgroundColor: function(point, index) {
                        var backgroundColor = this.chart.options.elements.point.backgroundColor;
                        var dataset = this.getDataset();
                        var custom = point.custom || {};
                        if (custom.backgroundColor) {
                            backgroundColor = custom.backgroundColor;
                        } else if (dataset.pointBackgroundColor) {
                            backgroundColor = helpers.valueAtIndexOrDefault(dataset.pointBackgroundColor, index, backgroundColor);
                        } else if (dataset.backgroundColor) {
                            backgroundColor = dataset.backgroundColor;
                        }
                        return backgroundColor;
                    },
                    getPointBorderColor: function(point, index) {
                        var borderColor = this.chart.options.elements.point.borderColor;
                        var dataset = this.getDataset();
                        var custom = point.custom || {};
                        if (custom.borderColor) {
                            borderColor = custom.borderColor;
                        } else if (dataset.pointBorderColor) {
                            borderColor = helpers.valueAtIndexOrDefault(dataset.pointBorderColor, index, borderColor);
                        } else if (dataset.borderColor) {
                            borderColor = dataset.borderColor;
                        }
                        return borderColor;
                    },
                    getPointBorderWidth: function(point, index) {
                        var borderWidth = this.chart.options.elements.point.borderWidth;
                        var dataset = this.getDataset();
                        var custom = point.custom || {};
                        if (!isNaN(custom.borderWidth)) {
                            borderWidth = custom.borderWidth;
                        } else if (!isNaN(dataset.pointBorderWidth) || helpers.isArray(dataset.pointBorderWidth)) {
                            borderWidth = helpers.valueAtIndexOrDefault(dataset.pointBorderWidth, index, borderWidth);
                        } else if (!isNaN(dataset.borderWidth)) {
                            borderWidth = dataset.borderWidth;
                        }
                        return borderWidth;
                    },
                    updateElement: function(point, index, reset) {
                        var me = this;
                        var meta = me.getMeta();
                        var custom = point.custom || {};
                        var dataset = me.getDataset();
                        var datasetIndex = me.index;
                        var value = dataset.data[index];
                        var yScale = me.getScaleForId(meta.yAxisID);
                        var xScale = me.getScaleForId(meta.xAxisID);
                        var pointOptions = me.chart.options.elements.point;
                        var x, y;
                        if ((dataset.radius !== undefined) && (dataset.pointRadius === undefined)) {
                            dataset.pointRadius = dataset.radius;
                        }
                        if ((dataset.hitRadius !== undefined) && (dataset.pointHitRadius === undefined)) {
                            dataset.pointHitRadius = dataset.hitRadius;
                        }
                        x = xScale.getPixelForValue(typeof value === 'object' ? value : NaN, index, datasetIndex);
                        y = reset ? yScale.getBasePixel() : me.calculatePointY(value, index, datasetIndex);
                        point._xScale = xScale;
                        point._yScale = yScale;
                        point._datasetIndex = datasetIndex;
                        point._index = index;
                        point._model = {
                            x: x,
                            y: y,
                            skip: custom.skip || isNaN(x) || isNaN(y),
                            radius: custom.radius || helpers.valueAtIndexOrDefault(dataset.pointRadius, index, pointOptions.radius),
                            pointStyle: custom.pointStyle || helpers.valueAtIndexOrDefault(dataset.pointStyle, index, pointOptions.pointStyle),
                            backgroundColor: me.getPointBackgroundColor(point, index),
                            borderColor: me.getPointBorderColor(point, index),
                            borderWidth: me.getPointBorderWidth(point, index),
                            tension: meta.dataset._model ? meta.dataset._model.tension : 0,
                            steppedLine: meta.dataset._model ? meta.dataset._model.steppedLine : false,
                            hitRadius: custom.hitRadius || helpers.valueAtIndexOrDefault(dataset.pointHitRadius, index, pointOptions.hitRadius)
                        };
                    },
                    calculatePointY: function(value, index, datasetIndex) {
                        var me = this;
                        var chart = me.chart;
                        var meta = me.getMeta();
                        var yScale = me.getScaleForId(meta.yAxisID);
                        var sumPos = 0;
                        var sumNeg = 0;
                        var i, ds, dsMeta;
                        if (yScale.options.stacked) {
                            for (i = 0; i < datasetIndex; i++) {
                                ds = chart.data.datasets[i];
                                dsMeta = chart.getDatasetMeta(i);
                                if (dsMeta.type === 'line' && dsMeta.yAxisID === yScale.id && chart.isDatasetVisible(i)) {
                                    var stackedRightValue = Number(yScale.getRightValue(ds.data[index]));
                                    if (stackedRightValue < 0) {
                                        sumNeg += stackedRightValue || 0;
                                    } else {
                                        sumPos += stackedRightValue || 0;
                                    }
                                }
                            }
                            var rightValue = Number(yScale.getRightValue(value));
                            if (rightValue < 0) {
                                return yScale.getPixelForValue(sumNeg + rightValue);
                            }
                            return yScale.getPixelForValue(sumPos + rightValue);
                        }
                        return yScale.getPixelForValue(value);
                    },
                    updateBezierControlPoints: function() {
                        var me = this;
                        var meta = me.getMeta();
                        var area = me.chart.chartArea;
                        var points = (meta.data || []);
                        var i, ilen, point, model, controlPoints;
                        if (meta.dataset._model.spanGaps) {
                            points = points.filter(function(pt) {
                                return !pt._model.skip;
                            });
                        }

                        function capControlPoint(pt, min, max) {
                            return Math.max(Math.min(pt, max), min);
                        }
                        if (meta.dataset._model.cubicInterpolationMode === 'monotone') {
                            helpers.splineCurveMonotone(points);
                        } else {
                            for (i = 0, ilen = points.length; i < ilen; ++i) {
                                point = points[i];
                                model = point._model;
                                controlPoints = helpers.splineCurve(helpers.previousItem(points, i)._model, model, helpers.nextItem(points, i)._model, meta.dataset._model.tension);
                                model.controlPointPreviousX = controlPoints.previous.x;
                                model.controlPointPreviousY = controlPoints.previous.y;
                                model.controlPointNextX = controlPoints.next.x;
                                model.controlPointNextY = controlPoints.next.y;
                            }
                        }
                        if (me.chart.options.elements.line.capBezierPoints) {
                            for (i = 0, ilen = points.length; i < ilen; ++i) {
                                model = points[i]._model;
                                model.controlPointPreviousX = capControlPoint(model.controlPointPreviousX, area.left, area.right);
                                model.controlPointPreviousY = capControlPoint(model.controlPointPreviousY, area.top, area.bottom);
                                model.controlPointNextX = capControlPoint(model.controlPointNextX, area.left, area.right);
                                model.controlPointNextY = capControlPoint(model.controlPointNextY, area.top, area.bottom);
                            }
                        }
                    },
                    draw: function() {
                        var me = this;
                        var chart = me.chart;
                        var meta = me.getMeta();
                        var points = meta.data || [];
                        var area = chart.chartArea;
                        var ilen = points.length;
                        var i = 0;
                        helpers.canvas.clipArea(chart.ctx, area);
                        if (lineEnabled(me.getDataset(), chart.options)) {
                            meta.dataset.draw();
                        }
                        helpers.canvas.unclipArea(chart.ctx);
                        for (; i < ilen; ++i) {
                            points[i].draw(area);
                        }
                    },
                    setHoverStyle: function(point) {
                        var dataset = this.chart.data.datasets[point._datasetIndex];
                        var index = point._index;
                        var custom = point.custom || {};
                        var model = point._model;
                        model.radius = custom.hoverRadius || helpers.valueAtIndexOrDefault(dataset.pointHoverRadius, index, this.chart.options.elements.point.hoverRadius);
                        model.backgroundColor = custom.hoverBackgroundColor || helpers.valueAtIndexOrDefault(dataset.pointHoverBackgroundColor, index, helpers.getHoverColor(model.backgroundColor));
                        model.borderColor = custom.hoverBorderColor || helpers.valueAtIndexOrDefault(dataset.pointHoverBorderColor, index, helpers.getHoverColor(model.borderColor));
                        model.borderWidth = custom.hoverBorderWidth || helpers.valueAtIndexOrDefault(dataset.pointHoverBorderWidth, index, model.borderWidth);
                    },
                    removeHoverStyle: function(point) {
                        var me = this;
                        var dataset = me.chart.data.datasets[point._datasetIndex];
                        var index = point._index;
                        var custom = point.custom || {};
                        var model = point._model;
                        if ((dataset.radius !== undefined) && (dataset.pointRadius === undefined)) {
                            dataset.pointRadius = dataset.radius;
                        }
                        model.radius = custom.radius || helpers.valueAtIndexOrDefault(dataset.pointRadius, index, me.chart.options.elements.point.radius);
                        model.backgroundColor = me.getPointBackgroundColor(point, index);
                        model.borderColor = me.getPointBorderColor(point, index);
                        model.borderWidth = me.getPointBorderWidth(point, index);
                    }
                });
            };
        }, {
            "25": 25,
            "40": 40,
            "45": 45
        }],
        19: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var elements = require(40);
            var helpers = require(45);
            defaults._set('polarArea', {
                scale: {
                    type: 'radialLinear',
                    angleLines: {
                        display: false
                    },
                    gridLines: {
                        circular: true
                    },
                    pointLabels: {
                        display: false
                    },
                    ticks: {
                        beginAtZero: true
                    }
                },
                animation: {
                    animateRotate: true,
                    animateScale: true
                },
                startAngle: -0.5 * Math.PI,
                legendCallback: function(chart) {
                    var text = [];
                    text.push('<ul class="' + chart.id + '-legend">');
                    var data = chart.data;
                    var datasets = data.datasets;
                    var labels = data.labels;
                    if (datasets.length) {
                        for (var i = 0; i < datasets[0].data.length; ++i) {
                            text.push('<li><span style="background-color:' + datasets[0].backgroundColor[i] + '"></span>');
                            if (labels[i]) {
                                text.push(labels[i]);
                            }
                            text.push('</li>');
                        }
                    }
                    text.push('</ul>');
                    return text.join('');
                },
                legend: {
                    labels: {
                        generateLabels: function(chart) {
                            var data = chart.data;
                            if (data.labels.length && data.datasets.length) {
                                return data.labels.map(function(label, i) {
                                    var meta = chart.getDatasetMeta(0);
                                    var ds = data.datasets[0];
                                    var arc = meta.data[i];
                                    var custom = arc.custom || {};
                                    var valueAtIndexOrDefault = helpers.valueAtIndexOrDefault;
                                    var arcOpts = chart.options.elements.arc;
                                    var fill = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                                    var stroke = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                                    var bw = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
                                    return {
                                        text: label,
                                        fillStyle: fill,
                                        strokeStyle: stroke,
                                        lineWidth: bw,
                                        hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                                        index: i
                                    };
                                });
                            }
                            return [];
                        }
                    },
                    onClick: function(e, legendItem) {
                        var index = legendItem.index;
                        var chart = this.chart;
                        var i, ilen, meta;
                        for (i = 0, ilen = (chart.data.datasets || []).length; i < ilen; ++i) {
                            meta = chart.getDatasetMeta(i);
                            meta.data[index].hidden = !meta.data[index].hidden;
                        }
                        chart.update();
                    }
                },
                tooltips: {
                    callbacks: {
                        title: function() {
                            return '';
                        },
                        label: function(item, data) {
                            return data.labels[item.index] + ': ' + item.yLabel;
                        }
                    }
                }
            });
            module.exports = function(Chart) {
                Chart.controllers.polarArea = Chart.DatasetController.extend({
                    dataElementType: elements.Arc,
                    linkScales: helpers.noop,
                    update: function(reset) {
                        var me = this;
                        var chart = me.chart;
                        var chartArea = chart.chartArea;
                        var meta = me.getMeta();
                        var opts = chart.options;
                        var arcOpts = opts.elements.arc;
                        var minSize = Math.min(chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
                        chart.outerRadius = Math.max((minSize - arcOpts.borderWidth / 2) / 2, 0);
                        chart.innerRadius = Math.max(opts.cutoutPercentage ? (chart.outerRadius / 100) * (opts.cutoutPercentage) : 1, 0);
                        chart.radiusLength = (chart.outerRadius - chart.innerRadius) / chart.getVisibleDatasetCount();
                        me.outerRadius = chart.outerRadius - (chart.radiusLength * me.index);
                        me.innerRadius = me.outerRadius - chart.radiusLength;
                        meta.count = me.countVisibleElements();
                        helpers.each(meta.data, function(arc, index) {
                            me.updateElement(arc, index, reset);
                        });
                    },
                    updateElement: function(arc, index, reset) {
                        var me = this;
                        var chart = me.chart;
                        var dataset = me.getDataset();
                        var opts = chart.options;
                        var animationOpts = opts.animation;
                        var scale = chart.scale;
                        var labels = chart.data.labels;
                        var circumference = me.calculateCircumference(dataset.data[index]);
                        var centerX = scale.xCenter;
                        var centerY = scale.yCenter;
                        var visibleCount = 0;
                        var meta = me.getMeta();
                        for (var i = 0; i < index; ++i) {
                            if (!isNaN(dataset.data[i]) && !meta.data[i].hidden) {
                                ++visibleCount;
                            }
                        }
                        var datasetStartAngle = opts.startAngle;
                        var distance = arc.hidden ? 0 : scale.getDistanceFromCenterForValue(dataset.data[index]);
                        var startAngle = datasetStartAngle + (circumference * visibleCount);
                        var endAngle = startAngle + (arc.hidden ? 0 : circumference);
                        var resetRadius = animationOpts.animateScale ? 0 : scale.getDistanceFromCenterForValue(dataset.data[index]);
                        helpers.extend(arc, {
                            _datasetIndex: me.index,
                            _index: index,
                            _scale: scale,
                            _model: {
                                x: centerX,
                                y: centerY,
                                innerRadius: 0,
                                outerRadius: reset ? resetRadius : distance,
                                startAngle: reset && animationOpts.animateRotate ? datasetStartAngle : startAngle,
                                endAngle: reset && animationOpts.animateRotate ? datasetStartAngle : endAngle,
                                label: helpers.valueAtIndexOrDefault(labels, index, labels[index])
                            }
                        });
                        me.removeHoverStyle(arc);
                        arc.pivot();
                    },
                    removeHoverStyle: function(arc) {
                        Chart.DatasetController.prototype.removeHoverStyle.call(this, arc, this.chart.options.elements.arc);
                    },
                    countVisibleElements: function() {
                        var dataset = this.getDataset();
                        var meta = this.getMeta();
                        var count = 0;
                        helpers.each(meta.data, function(element, index) {
                            if (!isNaN(dataset.data[index]) && !element.hidden) {
                                count++;
                            }
                        });
                        return count;
                    },
                    calculateCircumference: function(value) {
                        var count = this.getMeta().count;
                        if (count > 0 && !isNaN(value)) {
                            return (2 * Math.PI) / count;
                        }
                        return 0;
                    }
                });
            };
        }, {
            "25": 25,
            "40": 40,
            "45": 45
        }],
        20: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var elements = require(40);
            var helpers = require(45);
            defaults._set('radar', {
                scale: {
                    type: 'radialLinear'
                },
                elements: {
                    line: {
                        tension: 0
                    }
                }
            });
            module.exports = function(Chart) {
                Chart.controllers.radar = Chart.DatasetController.extend({
                    datasetElementType: elements.Line,
                    dataElementType: elements.Point,
                    linkScales: helpers.noop,
                    update: function(reset) {
                        var me = this;
                        var meta = me.getMeta();
                        var line = meta.dataset;
                        var points = meta.data;
                        var custom = line.custom || {};
                        var dataset = me.getDataset();
                        var lineElementOptions = me.chart.options.elements.line;
                        var scale = me.chart.scale;
                        if ((dataset.tension !== undefined) && (dataset.lineTension === undefined)) {
                            dataset.lineTension = dataset.tension;
                        }
                        helpers.extend(meta.dataset, {
                            _datasetIndex: me.index,
                            _scale: scale,
                            _children: points,
                            _loop: true,
                            _model: {
                                tension: custom.tension ? custom.tension : helpers.valueOrDefault(dataset.lineTension, lineElementOptions.tension),
                                backgroundColor: custom.backgroundColor ? custom.backgroundColor : (dataset.backgroundColor || lineElementOptions.backgroundColor),
                                borderWidth: custom.borderWidth ? custom.borderWidth : (dataset.borderWidth || lineElementOptions.borderWidth),
                                borderColor: custom.borderColor ? custom.borderColor : (dataset.borderColor || lineElementOptions.borderColor),
                                fill: custom.fill ? custom.fill : (dataset.fill !== undefined ? dataset.fill : lineElementOptions.fill),
                                borderCapStyle: custom.borderCapStyle ? custom.borderCapStyle : (dataset.borderCapStyle || lineElementOptions.borderCapStyle),
                                borderDash: custom.borderDash ? custom.borderDash : (dataset.borderDash || lineElementOptions.borderDash),
                                borderDashOffset: custom.borderDashOffset ? custom.borderDashOffset : (dataset.borderDashOffset || lineElementOptions.borderDashOffset),
                                borderJoinStyle: custom.borderJoinStyle ? custom.borderJoinStyle : (dataset.borderJoinStyle || lineElementOptions.borderJoinStyle),
                            }
                        });
                        meta.dataset.pivot();
                        helpers.each(points, function(point, index) {
                            me.updateElement(point, index, reset);
                        }, me);
                        me.updateBezierControlPoints();
                    },
                    updateElement: function(point, index, reset) {
                        var me = this;
                        var custom = point.custom || {};
                        var dataset = me.getDataset();
                        var scale = me.chart.scale;
                        var pointElementOptions = me.chart.options.elements.point;
                        var pointPosition = scale.getPointPositionForValue(index, dataset.data[index]);
                        if ((dataset.radius !== undefined) && (dataset.pointRadius === undefined)) {
                            dataset.pointRadius = dataset.radius;
                        }
                        if ((dataset.hitRadius !== undefined) && (dataset.pointHitRadius === undefined)) {
                            dataset.pointHitRadius = dataset.hitRadius;
                        }
                        helpers.extend(point, {
                            _datasetIndex: me.index,
                            _index: index,
                            _scale: scale,
                            _model: {
                                x: reset ? scale.xCenter : pointPosition.x,
                                y: reset ? scale.yCenter : pointPosition.y,
                                tension: custom.tension ? custom.tension : helpers.valueOrDefault(dataset.lineTension, me.chart.options.elements.line.tension),
                                radius: custom.radius ? custom.radius : helpers.valueAtIndexOrDefault(dataset.pointRadius, index, pointElementOptions.radius),
                                backgroundColor: custom.backgroundColor ? custom.backgroundColor : helpers.valueAtIndexOrDefault(dataset.pointBackgroundColor, index, pointElementOptions.backgroundColor),
                                borderColor: custom.borderColor ? custom.borderColor : helpers.valueAtIndexOrDefault(dataset.pointBorderColor, index, pointElementOptions.borderColor),
                                borderWidth: custom.borderWidth ? custom.borderWidth : helpers.valueAtIndexOrDefault(dataset.pointBorderWidth, index, pointElementOptions.borderWidth),
                                pointStyle: custom.pointStyle ? custom.pointStyle : helpers.valueAtIndexOrDefault(dataset.pointStyle, index, pointElementOptions.pointStyle),
                                hitRadius: custom.hitRadius ? custom.hitRadius : helpers.valueAtIndexOrDefault(dataset.pointHitRadius, index, pointElementOptions.hitRadius)
                            }
                        });
                        point._model.skip = custom.skip ? custom.skip : (isNaN(point._model.x) || isNaN(point._model.y));
                    },
                    updateBezierControlPoints: function() {
                        var chartArea = this.chart.chartArea;
                        var meta = this.getMeta();
                        helpers.each(meta.data, function(point, index) {
                            var model = point._model;
                            var controlPoints = helpers.splineCurve(helpers.previousItem(meta.data, index, true)._model, model, helpers.nextItem(meta.data, index, true)._model, model.tension);
                            model.controlPointPreviousX = Math.max(Math.min(controlPoints.previous.x, chartArea.right), chartArea.left);
                            model.controlPointPreviousY = Math.max(Math.min(controlPoints.previous.y, chartArea.bottom), chartArea.top);
                            model.controlPointNextX = Math.max(Math.min(controlPoints.next.x, chartArea.right), chartArea.left);
                            model.controlPointNextY = Math.max(Math.min(controlPoints.next.y, chartArea.bottom), chartArea.top);
                            point.pivot();
                        });
                    },
                    setHoverStyle: function(point) {
                        var dataset = this.chart.data.datasets[point._datasetIndex];
                        var custom = point.custom || {};
                        var index = point._index;
                        var model = point._model;
                        model.radius = custom.hoverRadius ? custom.hoverRadius : helpers.valueAtIndexOrDefault(dataset.pointHoverRadius, index, this.chart.options.elements.point.hoverRadius);
                        model.backgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : helpers.valueAtIndexOrDefault(dataset.pointHoverBackgroundColor, index, helpers.getHoverColor(model.backgroundColor));
                        model.borderColor = custom.hoverBorderColor ? custom.hoverBorderColor : helpers.valueAtIndexOrDefault(dataset.pointHoverBorderColor, index, helpers.getHoverColor(model.borderColor));
                        model.borderWidth = custom.hoverBorderWidth ? custom.hoverBorderWidth : helpers.valueAtIndexOrDefault(dataset.pointHoverBorderWidth, index, model.borderWidth);
                    },
                    removeHoverStyle: function(point) {
                        var dataset = this.chart.data.datasets[point._datasetIndex];
                        var custom = point.custom || {};
                        var index = point._index;
                        var model = point._model;
                        var pointElementOptions = this.chart.options.elements.point;
                        model.radius = custom.radius ? custom.radius : helpers.valueAtIndexOrDefault(dataset.pointRadius, index, pointElementOptions.radius);
                        model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : helpers.valueAtIndexOrDefault(dataset.pointBackgroundColor, index, pointElementOptions.backgroundColor);
                        model.borderColor = custom.borderColor ? custom.borderColor : helpers.valueAtIndexOrDefault(dataset.pointBorderColor, index, pointElementOptions.borderColor);
                        model.borderWidth = custom.borderWidth ? custom.borderWidth : helpers.valueAtIndexOrDefault(dataset.pointBorderWidth, index, pointElementOptions.borderWidth);
                    }
                });
            };
        }, {
            "25": 25,
            "40": 40,
            "45": 45
        }],
        21: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            defaults._set('scatter', {
                hover: {
                    mode: 'single'
                },
                scales: {
                    xAxes: [{
                        id: 'x-axis-1',
                        type: 'linear',
                        position: 'bottom'
                    }],
                    yAxes: [{
                        id: 'y-axis-1',
                        type: 'linear',
                        position: 'left'
                    }]
                },
                showLines: false,
                tooltips: {
                    callbacks: {
                        title: function() {
                            return '';
                        },
                        label: function(item) {
                            return '(' + item.xLabel + ', ' + item.yLabel + ')';
                        }
                    }
                }
            });
            module.exports = function(Chart) {
                Chart.controllers.scatter = Chart.controllers.line;
            };
        }, {
            "25": 25
        }],
        22: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            defaults._set('global', {
                animation: {
                    duration: 1000,
                    easing: 'easeOutQuart',
                    onProgress: helpers.noop,
                    onComplete: helpers.noop
                }
            });
            module.exports = function(Chart) {
                Chart.Animation = Element.extend({
                    chart: null,
                    currentStep: 0,
                    numSteps: 60,
                    easing: '',
                    render: null,
                    onAnimationProgress: null,
                    onAnimationComplete: null,
                });
                Chart.animationService = {
                    frameDuration: 17,
                    animations: [],
                    dropFrames: 0,
                    request: null,
                    addAnimation: function(chart, animation, duration, lazy) {
                        var animations = this.animations;
                        var i, ilen;
                        animation.chart = chart;
                        if (!lazy) {
                            chart.animating = true;
                        }
                        for (i = 0, ilen = animations.length; i < ilen; ++i) {
                            if (animations[i].chart === chart) {
                                animations[i] = animation;
                                return;
                            }
                        }
                        animations.push(animation);
                        if (animations.length === 1) {
                            this.requestAnimationFrame();
                        }
                    },
                    cancelAnimation: function(chart) {
                        var index = helpers.findIndex(this.animations, function(animation) {
                            return animation.chart === chart;
                        });
                        if (index !== -1) {
                            this.animations.splice(index, 1);
                            chart.animating = false;
                        }
                    },
                    requestAnimationFrame: function() {
                        var me = this;
                        if (me.request === null) {
                            me.request = helpers.requestAnimFrame.call(window, function() {
                                me.request = null;
                                me.startDigest();
                            });
                        }
                    },
                    startDigest: function() {
                        var me = this;
                        var startTime = Date.now();
                        var framesToDrop = 0;
                        if (me.dropFrames > 1) {
                            framesToDrop = Math.floor(me.dropFrames);
                            me.dropFrames = me.dropFrames % 1;
                        }
                        me.advance(1 + framesToDrop);
                        var endTime = Date.now();
                        me.dropFrames += (endTime - startTime) / me.frameDuration;
                        if (me.animations.length > 0) {
                            me.requestAnimationFrame();
                        }
                    },
                    advance: function(count) {
                        var animations = this.animations;
                        var animation, chart;
                        var i = 0;
                        while (i < animations.length) {
                            animation = animations[i];
                            chart = animation.chart;
                            animation.currentStep = (animation.currentStep || 0) + count;
                            animation.currentStep = Math.min(animation.currentStep, animation.numSteps);
                            helpers.callback(animation.render, [chart, animation], chart);
                            helpers.callback(animation.onAnimationProgress, [animation], chart);
                            if (animation.currentStep >= animation.numSteps) {
                                helpers.callback(animation.onAnimationComplete, [animation], chart);
                                chart.animating = false;
                                animations.splice(i, 1);
                            } else {
                                ++i;
                            }
                        }
                    }
                };
                Object.defineProperty(Chart.Animation.prototype, 'animationObject', {
                    get: function() {
                        return this;
                    }
                });
                Object.defineProperty(Chart.Animation.prototype, 'chartInstance', {
                    get: function() {
                        return this.chart;
                    },
                    set: function(value) {
                        this.chart = value;
                    }
                });
            };
        }, {
            "25": 25,
            "26": 26,
            "45": 45
        }],
        23: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var helpers = require(45);
            var Interaction = require(28);
            var layouts = require(30);
            var platform = require(48);
            var plugins = require(31);
            module.exports = function(Chart) {
                Chart.types = {};
                Chart.instances = {};
                Chart.controllers = {};

                function initConfig(config) {
                    config = config || {};
                    var data = config.data = config.data || {};
                    data.datasets = data.datasets || [];
                    data.labels = data.labels || [];
                    config.options = helpers.configMerge(defaults.global, defaults[config.type], config.options || {});
                    return config;
                }

                function updateConfig(chart) {
                    var newOptions = chart.options;
                    helpers.each(chart.scales, function(scale) {
                        layouts.removeBox(chart, scale);
                    });
                    newOptions = helpers.configMerge(Chart.defaults.global, Chart.defaults[chart.config.type], newOptions);
                    chart.options = chart.config.options = newOptions;
                    chart.ensureScalesHaveIDs();
                    chart.buildOrUpdateScales();
                    chart.tooltip._options = newOptions.tooltips;
                    chart.tooltip.initialize();
                }

                function positionIsHorizontal(position) {
                    return position === 'top' || position === 'bottom';
                }
                helpers.extend(Chart.prototype, {
                    construct: function(item, config) {
                        var me = this;
                        config = initConfig(config);
                        var context = platform.acquireContext(item, config);
                        var canvas = context && context.canvas;
                        var height = canvas && canvas.height;
                        var width = canvas && canvas.width;
                        me.id = helpers.uid();
                        me.ctx = context;
                        me.canvas = canvas;
                        me.config = config;
                        me.width = width;
                        me.height = height;
                        me.aspectRatio = height ? width / height : null;
                        me.options = config.options;
                        me._bufferedRender = false;
                        me.chart = me;
                        me.controller = me;
                        Chart.instances[me.id] = me;
                        Object.defineProperty(me, 'data', {
                            get: function() {
                                return me.config.data;
                            },
                            set: function(value) {
                                me.config.data = value;
                            }
                        });
                        if (!context || !canvas) {
                            console.error("Failed to create chart: can't acquire context from the given item");
                            return;
                        }
                        me.initialize();
                        me.update();
                    },
                    initialize: function() {
                        var me = this;
                        plugins.notify(me, 'beforeInit');
                        helpers.retinaScale(me, me.options.devicePixelRatio);
                        me.bindEvents();
                        if (me.options.responsive) {
                            me.resize(true);
                        }
                        me.ensureScalesHaveIDs();
                        me.buildOrUpdateScales();
                        me.initToolTip();
                        plugins.notify(me, 'afterInit');
                        return me;
                    },
                    clear: function() {
                        helpers.canvas.clear(this);
                        return this;
                    },
                    stop: function() {
                        Chart.animationService.cancelAnimation(this);
                        return this;
                    },
                    resize: function(silent) {
                        var me = this;
                        var options = me.options;
                        var canvas = me.canvas;
                        var aspectRatio = (options.maintainAspectRatio && me.aspectRatio) || null;
                        var newWidth = Math.max(0, Math.floor(helpers.getMaximumWidth(canvas)));
                        var newHeight = Math.max(0, Math.floor(aspectRatio ? newWidth / aspectRatio : helpers.getMaximumHeight(canvas)));
                        if (me.width === newWidth && me.height === newHeight) {
                            return;
                        }
                        canvas.width = me.width = newWidth;
                        canvas.height = me.height = newHeight;
                        canvas.style.width = newWidth + 'px';
                        canvas.style.height = newHeight + 'px';
                        helpers.retinaScale(me, options.devicePixelRatio);
                        if (!silent) {
                            var newSize = {
                                width: newWidth,
                                height: newHeight
                            };
                            plugins.notify(me, 'resize', [newSize]);
                            if (me.options.onResize) {
                                me.options.onResize(me, newSize);
                            }
                            me.stop();
                            me.update(me.options.responsiveAnimationDuration);
                        }
                    },
                    ensureScalesHaveIDs: function() {
                        var options = this.options;
                        var scalesOptions = options.scales || {};
                        var scaleOptions = options.scale;
                        helpers.each(scalesOptions.xAxes, function(xAxisOptions, index) {
                            xAxisOptions.id = xAxisOptions.id || ('x-axis-' + index);
                        });
                        helpers.each(scalesOptions.yAxes, function(yAxisOptions, index) {
                            yAxisOptions.id = yAxisOptions.id || ('y-axis-' + index);
                        });
                        if (scaleOptions) {
                            scaleOptions.id = scaleOptions.id || 'scale';
                        }
                    },
                    buildOrUpdateScales: function() {
                        var me = this;
                        var options = me.options;
                        var scales = me.scales || {};
                        var items = [];
                        var updated = Object.keys(scales).reduce(function(obj, id) {
                            obj[id] = false;
                            return obj;
                        }, {});
                        if (options.scales) {
                            items = items.concat((options.scales.xAxes || []).map(function(xAxisOptions) {
                                return {
                                    options: xAxisOptions,
                                    dtype: 'category',
                                    dposition: 'bottom'
                                };
                            }), (options.scales.yAxes || []).map(function(yAxisOptions) {
                                return {
                                    options: yAxisOptions,
                                    dtype: 'linear',
                                    dposition: 'left'
                                };
                            }));
                        }
                        if (options.scale) {
                            items.push({
                                options: options.scale,
                                dtype: 'radialLinear',
                                isDefault: true,
                                dposition: 'chartArea'
                            });
                        }
                        helpers.each(items, function(item) {
                            var scaleOptions = item.options;
                            var id = scaleOptions.id;
                            var scaleType = helpers.valueOrDefault(scaleOptions.type, item.dtype);
                            if (positionIsHorizontal(scaleOptions.position) !== positionIsHorizontal(item.dposition)) {
                                scaleOptions.position = item.dposition;
                            }
                            updated[id] = true;
                            var scale = null;
                            if (id in scales && scales[id].type === scaleType) {
                                scale = scales[id];
                                scale.options = scaleOptions;
                                scale.ctx = me.ctx;
                                scale.chart = me;
                            } else {
                                var scaleClass = Chart.scaleService.getScaleConstructor(scaleType);
                                if (!scaleClass) {
                                    return;
                                }
                                scale = new scaleClass({
                                    id: id,
                                    type: scaleType,
                                    options: scaleOptions,
                                    ctx: me.ctx,
                                    chart: me
                                });
                                scales[scale.id] = scale;
                            }
                            scale.mergeTicksOptions();
                            if (item.isDefault) {
                                me.scale = scale;
                            }
                        });
                        helpers.each(updated, function(hasUpdated, id) {
                            if (!hasUpdated) {
                                delete scales[id];
                            }
                        });
                        me.scales = scales;
                        Chart.scaleService.addScalesToLayout(this);
                    },
                    buildOrUpdateControllers: function() {
                        var me = this;
                        var types = [];
                        var newControllers = [];
                        helpers.each(me.data.datasets, function(dataset, datasetIndex) {
                            var meta = me.getDatasetMeta(datasetIndex);
                            var type = dataset.type || me.config.type;
                            if (meta.type && meta.type !== type) {
                                me.destroyDatasetMeta(datasetIndex);
                                meta = me.getDatasetMeta(datasetIndex);
                            }
                            meta.type = type;
                            types.push(meta.type);
                            if (meta.controller) {
                                meta.controller.updateIndex(datasetIndex);
                                meta.controller.linkScales();
                            } else {
                                var ControllerClass = Chart.controllers[meta.type];
                                if (ControllerClass === undefined) {
                                    throw new Error('"' + meta.type + '" is not a chart type.');
                                }
                                meta.controller = new ControllerClass(me, datasetIndex);
                                newControllers.push(meta.controller);
                            }
                        }, me);
                        return newControllers;
                    },
                    resetElements: function() {
                        var me = this;
                        helpers.each(me.data.datasets, function(dataset, datasetIndex) {
                            me.getDatasetMeta(datasetIndex).controller.reset();
                        }, me);
                    },
                    reset: function() {
                        this.resetElements();
                        this.tooltip.initialize();
                    },
                    update: function(config) {
                        var me = this;
                        if (!config || typeof config !== 'object') {
                            config = {
                                duration: config,
                                lazy: arguments[1]
                            };
                        }
                        updateConfig(me);
                        plugins._invalidate(me);
                        if (plugins.notify(me, 'beforeUpdate') === false) {
                            return;
                        }
                        me.tooltip._data = me.data;
                        var newControllers = me.buildOrUpdateControllers();
                        helpers.each(me.data.datasets, function(dataset, datasetIndex) {
                            me.getDatasetMeta(datasetIndex).controller.buildOrUpdateElements();
                        }, me);
                        me.updateLayout();
                        if (me.options.animation && me.options.animation.duration) {
                            helpers.each(newControllers, function(controller) {
                                controller.reset();
                            });
                        }
                        me.updateDatasets();
                        me.tooltip.initialize();
                        me.lastActive = [];
                        plugins.notify(me, 'afterUpdate');
                        if (me._bufferedRender) {
                            me._bufferedRequest = {
                                duration: config.duration,
                                easing: config.easing,
                                lazy: config.lazy
                            };
                        } else {
                            me.render(config);
                        }
                    },
                    updateLayout: function() {
                        var me = this;
                        if (plugins.notify(me, 'beforeLayout') === false) {
                            return;
                        }
                        layouts.update(this, this.width, this.height);
                        plugins.notify(me, 'afterScaleUpdate');
                        plugins.notify(me, 'afterLayout');
                    },
                    updateDatasets: function() {
                        var me = this;
                        if (plugins.notify(me, 'beforeDatasetsUpdate') === false) {
                            return;
                        }
                        for (var i = 0, ilen = me.data.datasets.length; i < ilen; ++i) {
                            me.updateDataset(i);
                        }
                        plugins.notify(me, 'afterDatasetsUpdate');
                    },
                    updateDataset: function(index) {
                        var me = this;
                        var meta = me.getDatasetMeta(index);
                        var args = {
                            meta: meta,
                            index: index
                        };
                        if (plugins.notify(me, 'beforeDatasetUpdate', [args]) === false) {
                            return;
                        }
                        meta.controller.update();
                        plugins.notify(me, 'afterDatasetUpdate', [args]);
                    },
                    render: function(config) {
                        var me = this;
                        if (!config || typeof config !== 'object') {
                            config = {
                                duration: config,
                                lazy: arguments[1]
                            };
                        }
                        var duration = config.duration;
                        var lazy = config.lazy;
                        if (plugins.notify(me, 'beforeRender') === false) {
                            return;
                        }
                        var animationOptions = me.options.animation;
                        var onComplete = function(animation) {
                            plugins.notify(me, 'afterRender');
                            helpers.callback(animationOptions && animationOptions.onComplete, [animation], me);
                        };
                        if (animationOptions && ((typeof duration !== 'undefined' && duration !== 0) || (typeof duration === 'undefined' && animationOptions.duration !== 0))) {
                            var animation = new Chart.Animation({
                                numSteps: (duration || animationOptions.duration) / 16.66,
                                easing: config.easing || animationOptions.easing,
                                render: function(chart, animationObject) {
                                    var easingFunction = helpers.easing.effects[animationObject.easing];
                                    var currentStep = animationObject.currentStep;
                                    var stepDecimal = currentStep / animationObject.numSteps;
                                    chart.draw(easingFunction(stepDecimal), stepDecimal, currentStep);
                                },
                                onAnimationProgress: animationOptions.onProgress,
                                onAnimationComplete: onComplete
                            });
                            Chart.animationService.addAnimation(me, animation, duration, lazy);
                        } else {
                            me.draw();
                            onComplete(new Chart.Animation({
                                numSteps: 0,
                                chart: me
                            }));
                        }
                        return me;
                    },
                    draw: function(easingValue) {
                        var me = this;
                        me.clear();
                        if (helpers.isNullOrUndef(easingValue)) {
                            easingValue = 1;
                        }
                        me.transition(easingValue);
                        if (plugins.notify(me, 'beforeDraw', [easingValue]) === false) {
                            return;
                        }
                        helpers.each(me.boxes, function(box) {
                            box.draw(me.chartArea);
                        }, me);
                        if (me.scale) {
                            me.scale.draw();
                        }
                        me.drawDatasets(easingValue);
                        me._drawTooltip(easingValue);
                        plugins.notify(me, 'afterDraw', [easingValue]);
                    },
                    transition: function(easingValue) {
                        var me = this;
                        for (var i = 0, ilen = (me.data.datasets || []).length; i < ilen; ++i) {
                            if (me.isDatasetVisible(i)) {
                                me.getDatasetMeta(i).controller.transition(easingValue);
                            }
                        }
                        me.tooltip.transition(easingValue);
                    },
                    drawDatasets: function(easingValue) {
                        var me = this;
                        if (plugins.notify(me, 'beforeDatasetsDraw', [easingValue]) === false) {
                            return;
                        }
                        for (var i = (me.data.datasets || []).length - 1; i >= 0; --i) {
                            if (me.isDatasetVisible(i)) {
                                me.drawDataset(i, easingValue);
                            }
                        }
                        plugins.notify(me, 'afterDatasetsDraw', [easingValue]);
                    },
                    drawDataset: function(index, easingValue) {
                        var me = this;
                        var meta = me.getDatasetMeta(index);
                        var args = {
                            meta: meta,
                            index: index,
                            easingValue: easingValue
                        };
                        if (plugins.notify(me, 'beforeDatasetDraw', [args]) === false) {
                            return;
                        }
                        meta.controller.draw(easingValue);
                        plugins.notify(me, 'afterDatasetDraw', [args]);
                    },
                    _drawTooltip: function(easingValue) {
                        var me = this;
                        var tooltip = me.tooltip;
                        var args = {
                            tooltip: tooltip,
                            easingValue: easingValue
                        };
                        if (plugins.notify(me, 'beforeTooltipDraw', [args]) === false) {
                            return;
                        }
                        tooltip.draw();
                        plugins.notify(me, 'afterTooltipDraw', [args]);
                    },
                    getElementAtEvent: function(e) {
                        return Interaction.modes.single(this, e);
                    },
                    getElementsAtEvent: function(e) {
                        return Interaction.modes.label(this, e, {
                            intersect: true
                        });
                    },
                    getElementsAtXAxis: function(e) {
                        return Interaction.modes['x-axis'](this, e, {
                            intersect: true
                        });
                    },
                    getElementsAtEventForMode: function(e, mode, options) {
                        var method = Interaction.modes[mode];
                        if (typeof method === 'function') {
                            return method(this, e, options);
                        }
                        return [];
                    },
                    getDatasetAtEvent: function(e) {
                        return Interaction.modes.dataset(this, e, {
                            intersect: true
                        });
                    },
                    getDatasetMeta: function(datasetIndex) {
                        var me = this;
                        var dataset = me.data.datasets[datasetIndex];
                        if (!dataset._meta) {
                            dataset._meta = {};
                        }
                        var meta = dataset._meta[me.id];
                        if (!meta) {
                            meta = dataset._meta[me.id] = {
                                type: null,
                                data: [],
                                dataset: null,
                                controller: null,
                                hidden: null,
                                xAxisID: null,
                                yAxisID: null
                            };
                        }
                        return meta;
                    },
                    getVisibleDatasetCount: function() {
                        var count = 0;
                        for (var i = 0, ilen = this.data.datasets.length; i < ilen; ++i) {
                            if (this.isDatasetVisible(i)) {
                                count++;
                            }
                        }
                        return count;
                    },
                    isDatasetVisible: function(datasetIndex) {
                        var meta = this.getDatasetMeta(datasetIndex);
                        return typeof meta.hidden === 'boolean' ? !meta.hidden : !this.data.datasets[datasetIndex].hidden;
                    },
                    generateLegend: function() {
                        return this.options.legendCallback(this);
                    },
                    destroyDatasetMeta: function(datasetIndex) {
                        var id = this.id;
                        var dataset = this.data.datasets[datasetIndex];
                        var meta = dataset._meta && dataset._meta[id];
                        if (meta) {
                            meta.controller.destroy();
                            delete dataset._meta[id];
                        }
                    },
                    destroy: function() {
                        var me = this;
                        var canvas = me.canvas;
                        var i, ilen;
                        me.stop();
                        for (i = 0, ilen = me.data.datasets.length; i < ilen; ++i) {
                            me.destroyDatasetMeta(i);
                        }
                        if (canvas) {
                            me.unbindEvents();
                            helpers.canvas.clear(me);
                            platform.releaseContext(me.ctx);
                            me.canvas = null;
                            me.ctx = null;
                        }
                        plugins.notify(me, 'destroy');
                        delete Chart.instances[me.id];
                    },
                    toBase64Image: function() {
                        return this.canvas.toDataURL.apply(this.canvas, arguments);
                    },
                    initToolTip: function() {
                        var me = this;
                        me.tooltip = new Chart.Tooltip({
                            _chart: me,
                            _chartInstance: me,
                            _data: me.data,
                            _options: me.options.tooltips
                        }, me);
                    },
                    bindEvents: function() {
                        var me = this;
                        var listeners = me._listeners = {};
                        var listener = function() {
                            me.eventHandler.apply(me, arguments);
                        };
                        helpers.each(me.options.events, function(type) {
                            platform.addEventListener(me, type, listener);
                            listeners[type] = listener;
                        });
                        if (me.options.responsive) {
                            listener = function() {
                                me.resize();
                            };
                            platform.addEventListener(me, 'resize', listener);
                            listeners.resize = listener;
                        }
                    },
                    unbindEvents: function() {
                        var me = this;
                        var listeners = me._listeners;
                        if (!listeners) {
                            return;
                        }
                        delete me._listeners;
                        helpers.each(listeners, function(listener, type) {
                            platform.removeEventListener(me, type, listener);
                        });
                    },
                    updateHoverStyle: function(elements, mode, enabled) {
                        var method = enabled ? 'setHoverStyle' : 'removeHoverStyle';
                        var element, i, ilen;
                        for (i = 0, ilen = elements.length; i < ilen; ++i) {
                            element = elements[i];
                            if (element) {
                                this.getDatasetMeta(element._datasetIndex).controller[method](element);
                            }
                        }
                    },
                    eventHandler: function(e) {
                        var me = this;
                        var tooltip = me.tooltip;
                        if (plugins.notify(me, 'beforeEvent', [e]) === false) {
                            return;
                        }
                        me._bufferedRender = true;
                        me._bufferedRequest = null;
                        var changed = me.handleEvent(e);
                        if (tooltip) {
                            changed = tooltip._start ? tooltip.handleEvent(e) : changed | tooltip.handleEvent(e);
                        }
                        plugins.notify(me, 'afterEvent', [e]);
                        var bufferedRequest = me._bufferedRequest;
                        if (bufferedRequest) {
                            me.render(bufferedRequest);
                        } else if (changed && !me.animating) {
                            me.stop();
                            me.render(me.options.hover.animationDuration, true);
                        }
                        me._bufferedRender = false;
                        me._bufferedRequest = null;
                        return me;
                    },
                    handleEvent: function(e) {
                        var me = this;
                        var options = me.options || {};
                        var hoverOptions = options.hover;
                        var changed = false;
                        me.lastActive = me.lastActive || [];
                        if (e.type === 'mouseout') {
                            me.active = [];
                        } else {
                            me.active = me.getElementsAtEventForMode(e, hoverOptions.mode, hoverOptions);
                        }
                        helpers.callback(options.onHover || options.hover.onHover, [e.native, me.active], me);
                        if (e.type === 'mouseup' || e.type === 'click') {
                            if (options.onClick) {
                                options.onClick.call(me, e.native, me.active);
                            }
                        }
                        if (me.lastActive.length) {
                            me.updateHoverStyle(me.lastActive, hoverOptions.mode, false);
                        }
                        if (me.active.length && hoverOptions.mode) {
                            me.updateHoverStyle(me.active, hoverOptions.mode, true);
                        }
                        changed = !helpers.arrayEquals(me.active, me.lastActive);
                        me.lastActive = me.active;
                        return changed;
                    }
                });
                Chart.Controller = Chart;
            };
        }, {
            "25": 25,
            "28": 28,
            "30": 30,
            "31": 31,
            "45": 45,
            "48": 48
        }],
        24: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);
            module.exports = function(Chart) {
                var arrayEvents = ['push', 'pop', 'shift', 'splice', 'unshift'];

                function listenArrayEvents(array, listener) {
                    if (array._chartjs) {
                        array._chartjs.listeners.push(listener);
                        return;
                    }
                    Object.defineProperty(array, '_chartjs', {
                        configurable: true,
                        enumerable: false,
                        value: {
                            listeners: [listener]
                        }
                    });
                    arrayEvents.forEach(function(key) {
                        var method = 'onData' + key.charAt(0).toUpperCase() + key.slice(1);
                        var base = array[key];
                        Object.defineProperty(array, key, {
                            configurable: true,
                            enumerable: false,
                            value: function() {
                                var args = Array.prototype.slice.call(arguments);
                                var res = base.apply(this, args);
                                helpers.each(array._chartjs.listeners, function(object) {
                                    if (typeof object[method] === 'function') {
                                        object[method].apply(object, args);
                                    }
                                });
                                return res;
                            }
                        });
                    });
                }

                function unlistenArrayEvents(array, listener) {
                    var stub = array._chartjs;
                    if (!stub) {
                        return;
                    }
                    var listeners = stub.listeners;
                    var index = listeners.indexOf(listener);
                    if (index !== -1) {
                        listeners.splice(index, 1);
                    }
                    if (listeners.length > 0) {
                        return;
                    }
                    arrayEvents.forEach(function(key) {
                        delete array[key];
                    });
                    delete array._chartjs;
                }
                Chart.DatasetController = function(chart, datasetIndex) {
                    this.initialize(chart, datasetIndex);
                };
                helpers.extend(Chart.DatasetController.prototype, {
                    datasetElementType: null,
                    dataElementType: null,
                    initialize: function(chart, datasetIndex) {
                        var me = this;
                        me.chart = chart;
                        me.index = datasetIndex;
                        me.linkScales();
                        me.addElements();
                    },
                    updateIndex: function(datasetIndex) {
                        this.index = datasetIndex;
                    },
                    linkScales: function() {
                        var me = this;
                        var meta = me.getMeta();
                        var dataset = me.getDataset();
                        if (meta.xAxisID === null || !(meta.xAxisID in me.chart.scales)) {
                            meta.xAxisID = dataset.xAxisID || me.chart.options.scales.xAxes[0].id;
                        }
                        if (meta.yAxisID === null || !(meta.yAxisID in me.chart.scales)) {
                            meta.yAxisID = dataset.yAxisID || me.chart.options.scales.yAxes[0].id;
                        }
                    },
                    getDataset: function() {
                        return this.chart.data.datasets[this.index];
                    },
                    getMeta: function() {
                        return this.chart.getDatasetMeta(this.index);
                    },
                    getScaleForId: function(scaleID) {
                        return this.chart.scales[scaleID];
                    },
                    reset: function() {
                        this.update(true);
                    },
                    destroy: function() {
                        if (this._data) {
                            unlistenArrayEvents(this._data, this);
                        }
                    },
                    createMetaDataset: function() {
                        var me = this;
                        var type = me.datasetElementType;
                        return type && new type({
                            _chart: me.chart,
                            _datasetIndex: me.index
                        });
                    },
                    createMetaData: function(index) {
                        var me = this;
                        var type = me.dataElementType;
                        return type && new type({
                            _chart: me.chart,
                            _datasetIndex: me.index,
                            _index: index
                        });
                    },
                    addElements: function() {
                        var me = this;
                        var meta = me.getMeta();
                        var data = me.getDataset().data || [];
                        var metaData = meta.data;
                        var i, ilen;
                        for (i = 0, ilen = data.length; i < ilen; ++i) {
                            metaData[i] = metaData[i] || me.createMetaData(i);
                        }
                        meta.dataset = meta.dataset || me.createMetaDataset();
                    },
                    addElementAndReset: function(index) {
                        var element = this.createMetaData(index);
                        this.getMeta().data.splice(index, 0, element);
                        this.updateElement(element, index, true);
                    },
                    buildOrUpdateElements: function() {
                        var me = this;
                        var dataset = me.getDataset();
                        var data = dataset.data || (dataset.data = []);
                        if (me._data !== data) {
                            if (me._data) {
                                unlistenArrayEvents(me._data, me);
                            }
                            listenArrayEvents(data, me);
                            me._data = data;
                        }
                        me.resyncElements();
                    },
                    update: helpers.noop,
                    transition: function(easingValue) {
                        var meta = this.getMeta();
                        var elements = meta.data || [];
                        var ilen = elements.length;
                        var i = 0;
                        for (; i < ilen; ++i) {
                            elements[i].transition(easingValue);
                        }
                        if (meta.dataset) {
                            meta.dataset.transition(easingValue);
                        }
                    },
                    draw: function() {
                        var meta = this.getMeta();
                        var elements = meta.data || [];
                        var ilen = elements.length;
                        var i = 0;
                        if (meta.dataset) {
                            meta.dataset.draw();
                        }
                        for (; i < ilen; ++i) {
                            elements[i].draw();
                        }
                    },
                    removeHoverStyle: function(element, elementOpts) {
                        var dataset = this.chart.data.datasets[element._datasetIndex];
                        var index = element._index;
                        var custom = element.custom || {};
                        var valueOrDefault = helpers.valueAtIndexOrDefault;
                        var model = element._model;
                        model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueOrDefault(dataset.backgroundColor, index, elementOpts.backgroundColor);
                        model.borderColor = custom.borderColor ? custom.borderColor : valueOrDefault(dataset.borderColor, index, elementOpts.borderColor);
                        model.borderWidth = custom.borderWidth ? custom.borderWidth : valueOrDefault(dataset.borderWidth, index, elementOpts.borderWidth);
                    },
                    setHoverStyle: function(element) {
                        var dataset = this.chart.data.datasets[element._datasetIndex];
                        var index = element._index;
                        var custom = element.custom || {};
                        var valueOrDefault = helpers.valueAtIndexOrDefault;
                        var getHoverColor = helpers.getHoverColor;
                        var model = element._model;
                        model.backgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueOrDefault(dataset.hoverBackgroundColor, index, getHoverColor(model.backgroundColor));
                        model.borderColor = custom.hoverBorderColor ? custom.hoverBorderColor : valueOrDefault(dataset.hoverBorderColor, index, getHoverColor(model.borderColor));
                        model.borderWidth = custom.hoverBorderWidth ? custom.hoverBorderWidth : valueOrDefault(dataset.hoverBorderWidth, index, model.borderWidth);
                    },
                    resyncElements: function() {
                        var me = this;
                        var meta = me.getMeta();
                        var data = me.getDataset().data;
                        var numMeta = meta.data.length;
                        var numData = data.length;
                        if (numData < numMeta) {
                            meta.data.splice(numData, numMeta - numData);
                        } else if (numData > numMeta) {
                            me.insertElements(numMeta, numData - numMeta);
                        }
                    },
                    insertElements: function(start, count) {
                        for (var i = 0; i < count; ++i) {
                            this.addElementAndReset(start + i);
                        }
                    },
                    onDataPush: function() {
                        this.insertElements(this.getDataset().data.length - 1, arguments.length);
                    },
                    onDataPop: function() {
                        this.getMeta().data.pop();
                    },
                    onDataShift: function() {
                        this.getMeta().data.shift();
                    },
                    onDataSplice: function(start, count) {
                        this.getMeta().data.splice(start, count);
                        this.insertElements(start, arguments.length - 2);
                    },
                    onDataUnshift: function() {
                        this.insertElements(0, arguments.length);
                    }
                });
                Chart.DatasetController.extend = helpers.inherits;
            };
        }, {
            "45": 45
        }],
        25: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);
            module.exports = {
                _set: function(scope, values) {
                    return helpers.merge(this[scope] || (this[scope] = {}), values);
                }
            };
        }, {
            "45": 45
        }],
        26: [function(require, module, exports) {
            'use strict';
            var color = require(3);
            var helpers = require(45);

            function interpolate(start, view, model, ease) {
                var keys = Object.keys(model);
                var i, ilen, key, actual, origin, target, type, c0, c1;
                for (i = 0, ilen = keys.length; i < ilen; ++i) {
                    key = keys[i];
                    target = model[key];
                    if (!view.hasOwnProperty(key)) {
                        view[key] = target;
                    }
                    actual = view[key];
                    if (actual === target || key[0] === '_') {
                        continue;
                    }
                    if (!start.hasOwnProperty(key)) {
                        start[key] = actual;
                    }
                    origin = start[key];
                    type = typeof target;
                    if (type === typeof origin) {
                        if (type === 'string') {
                            c0 = color(origin);
                            if (c0.valid) {
                                c1 = color(target);
                                if (c1.valid) {
                                    view[key] = c1.mix(c0, ease).rgbString();
                                    continue;
                                }
                            }
                        } else if (type === 'number' && isFinite(origin) && isFinite(target)) {
                            view[key] = origin + (target - origin) * ease;
                            continue;
                        }
                    }
                    view[key] = target;
                }
            }
            var Element = function(configuration) {
                helpers.extend(this, configuration);
                this.initialize.apply(this, arguments);
            };
            helpers.extend(Element.prototype, {
                initialize: function() {
                    this.hidden = false;
                },
                pivot: function() {
                    var me = this;
                    if (!me._view) {
                        me._view = helpers.clone(me._model);
                    }
                    me._start = {};
                    return me;
                },
                transition: function(ease) {
                    var me = this;
                    var model = me._model;
                    var start = me._start;
                    var view = me._view;
                    if (!model || ease === 1) {
                        me._view = model;
                        me._start = null;
                        return me;
                    }
                    if (!view) {
                        view = me._view = {};
                    }
                    if (!start) {
                        start = me._start = {};
                    }
                    interpolate(start, view, model, ease);
                    return me;
                },
                tooltipPosition: function() {
                    return {
                        x: this._model.x,
                        y: this._model.y
                    };
                },
                hasValue: function() {
                    return helpers.isNumber(this._model.x) && helpers.isNumber(this._model.y);
                }
            });
            Element.extend = helpers.inherits;
            module.exports = Element;
        }, {
            "3": 3,
            "45": 45
        }],
        27: [function(require, module, exports) {
            'use strict';
            var color = require(3);
            var defaults = require(25);
            var helpers = require(45);
            module.exports = function(Chart) {
                helpers.configMerge = function() {
                    return helpers.merge(helpers.clone(arguments[0]), [].slice.call(arguments, 1), {
                        merger: function(key, target, source, options) {
                            var tval = target[key] || {};
                            var sval = source[key];
                            if (key === 'scales') {
                                target[key] = helpers.scaleMerge(tval, sval);
                            } else if (key === 'scale') {
                                target[key] = helpers.merge(tval, [Chart.scaleService.getScaleDefaults(sval.type), sval]);
                            } else {
                                helpers._merger(key, target, source, options);
                            }
                        }
                    });
                };
                helpers.scaleMerge = function() {
                    return helpers.merge(helpers.clone(arguments[0]), [].slice.call(arguments, 1), {
                        merger: function(key, target, source, options) {
                            if (key === 'xAxes' || key === 'yAxes') {
                                var slen = source[key].length;
                                var i, type, scale;
                                if (!target[key]) {
                                    target[key] = [];
                                }
                                for (i = 0; i < slen; ++i) {
                                    scale = source[key][i];
                                    type = helpers.valueOrDefault(scale.type, key === 'xAxes' ? 'category' : 'linear');
                                    if (i >= target[key].length) {
                                        target[key].push({});
                                    }
                                    if (!target[key][i].type || (scale.type && scale.type !== target[key][i].type)) {
                                        helpers.merge(target[key][i], [Chart.scaleService.getScaleDefaults(type), scale]);
                                    } else {
                                        helpers.merge(target[key][i], scale);
                                    }
                                }
                            } else {
                                helpers._merger(key, target, source, options);
                            }
                        }
                    });
                };
                helpers.where = function(collection, filterCallback) {
                    if (helpers.isArray(collection) && Array.prototype.filter) {
                        return collection.filter(filterCallback);
                    }
                    var filtered = [];
                    helpers.each(collection, function(item) {
                        if (filterCallback(item)) {
                            filtered.push(item);
                        }
                    });
                    return filtered;
                };
                helpers.findIndex = Array.prototype.findIndex ? function(array, callback, scope) {
                    return array.findIndex(callback, scope);
                } : function(array, callback, scope) {
                    scope = scope === undefined ? array : scope;
                    for (var i = 0, ilen = array.length; i < ilen; ++i) {
                        if (callback.call(scope, array[i], i, array)) {
                            return i;
                        }
                    }
                    return -1;
                };
                helpers.findNextWhere = function(arrayToSearch, filterCallback, startIndex) {
                    if (helpers.isNullOrUndef(startIndex)) {
                        startIndex = -1;
                    }
                    for (var i = startIndex + 1; i < arrayToSearch.length; i++) {
                        var currentItem = arrayToSearch[i];
                        if (filterCallback(currentItem)) {
                            return currentItem;
                        }
                    }
                };
                helpers.findPreviousWhere = function(arrayToSearch, filterCallback, startIndex) {
                    if (helpers.isNullOrUndef(startIndex)) {
                        startIndex = arrayToSearch.length;
                    }
                    for (var i = startIndex - 1; i >= 0; i--) {
                        var currentItem = arrayToSearch[i];
                        if (filterCallback(currentItem)) {
                            return currentItem;
                        }
                    }
                };
                helpers.isNumber = function(n) {
                    return !isNaN(parseFloat(n)) && isFinite(n);
                };
                helpers.almostEquals = function(x, y, epsilon) {
                    return Math.abs(x - y) < epsilon;
                };
                helpers.almostWhole = function(x, epsilon) {
                    var rounded = Math.round(x);
                    return (((rounded - epsilon) < x) && ((rounded + epsilon) > x));
                };
                helpers.max = function(array) {
                    return array.reduce(function(max, value) {
                        if (!isNaN(value)) {
                            return Math.max(max, value);
                        }
                        return max;
                    }, Number.NEGATIVE_INFINITY);
                };
                helpers.min = function(array) {
                    return array.reduce(function(min, value) {
                        if (!isNaN(value)) {
                            return Math.min(min, value);
                        }
                        return min;
                    }, Number.POSITIVE_INFINITY);
                };
                helpers.sign = Math.sign ? function(x) {
                    return Math.sign(x);
                } : function(x) {
                    x = +x;
                    if (x === 0 || isNaN(x)) {
                        return x;
                    }
                    return x > 0 ? 1 : -1;
                };
                helpers.log10 = Math.log10 ? function(x) {
                    return Math.log10(x);
                } : function(x) {
                    var exponent = Math.log(x) * Math.LOG10E;
                    var powerOf10 = Math.round(exponent);
                    var isPowerOf10 = x === Math.pow(10, powerOf10);
                    return isPowerOf10 ? powerOf10 : exponent;
                };
                helpers.toRadians = function(degrees) {
                    return degrees * (Math.PI / 180);
                };
                helpers.toDegrees = function(radians) {
                    return radians * (180 / Math.PI);
                };
                helpers.getAngleFromPoint = function(centrePoint, anglePoint) {
                    var distanceFromXCenter = anglePoint.x - centrePoint.x;
                    var distanceFromYCenter = anglePoint.y - centrePoint.y;
                    var radialDistanceFromCenter = Math.sqrt(distanceFromXCenter * distanceFromXCenter + distanceFromYCenter * distanceFromYCenter);
                    var angle = Math.atan2(distanceFromYCenter, distanceFromXCenter);
                    if (angle < (-0.5 * Math.PI)) {
                        angle += 2.0 * Math.PI;
                    }
                    return {
                        angle: angle,
                        distance: radialDistanceFromCenter
                    };
                };
                helpers.distanceBetweenPoints = function(pt1, pt2) {
                    return Math.sqrt(Math.pow(pt2.x - pt1.x, 2) + Math.pow(pt2.y - pt1.y, 2));
                };
                helpers.aliasPixel = function(pixelWidth) {
                    return (pixelWidth % 2 === 0) ? 0 : 0.5;
                };
                helpers.splineCurve = function(firstPoint, middlePoint, afterPoint, t) {
                    var previous = firstPoint.skip ? middlePoint : firstPoint;
                    var current = middlePoint;
                    var next = afterPoint.skip ? middlePoint : afterPoint;
                    var d01 = Math.sqrt(Math.pow(current.x - previous.x, 2) + Math.pow(current.y - previous.y, 2));
                    var d12 = Math.sqrt(Math.pow(next.x - current.x, 2) + Math.pow(next.y - current.y, 2));
                    var s01 = d01 / (d01 + d12);
                    var s12 = d12 / (d01 + d12);
                    s01 = isNaN(s01) ? 0 : s01;
                    s12 = isNaN(s12) ? 0 : s12;
                    var fa = t * s01;
                    var fb = t * s12;
                    return {
                        previous: {
                            x: current.x - fa * (next.x - previous.x),
                            y: current.y - fa * (next.y - previous.y)
                        },
                        next: {
                            x: current.x + fb * (next.x - previous.x),
                            y: current.y + fb * (next.y - previous.y)
                        }
                    };
                };
                helpers.EPSILON = Number.EPSILON || 1e-14;
                helpers.splineCurveMonotone = function(points) {
                    var pointsWithTangents = (points || []).map(function(point) {
                        return {
                            model: point._model,
                            deltaK: 0,
                            mK: 0
                        };
                    });
                    var pointsLen = pointsWithTangents.length;
                    var i, pointBefore, pointCurrent, pointAfter;
                    for (i = 0; i < pointsLen; ++i) {
                        pointCurrent = pointsWithTangents[i];
                        if (pointCurrent.model.skip) {
                            continue;
                        }
                        pointBefore = i > 0 ? pointsWithTangents[i - 1] : null;
                        pointAfter = i < pointsLen - 1 ? pointsWithTangents[i + 1] : null;
                        if (pointAfter && !pointAfter.model.skip) {
                            var slopeDeltaX = (pointAfter.model.x - pointCurrent.model.x);
                            pointCurrent.deltaK = slopeDeltaX !== 0 ? (pointAfter.model.y - pointCurrent.model.y) / slopeDeltaX : 0;
                        }
                        if (!pointBefore || pointBefore.model.skip) {
                            pointCurrent.mK = pointCurrent.deltaK;
                        } else if (!pointAfter || pointAfter.model.skip) {
                            pointCurrent.mK = pointBefore.deltaK;
                        } else if (this.sign(pointBefore.deltaK) !== this.sign(pointCurrent.deltaK)) {
                            pointCurrent.mK = 0;
                        } else {
                            pointCurrent.mK = (pointBefore.deltaK + pointCurrent.deltaK) / 2;
                        }
                    }
                    var alphaK, betaK, tauK, squaredMagnitude;
                    for (i = 0; i < pointsLen - 1; ++i) {
                        pointCurrent = pointsWithTangents[i];
                        pointAfter = pointsWithTangents[i + 1];
                        if (pointCurrent.model.skip || pointAfter.model.skip) {
                            continue;
                        }
                        if (helpers.almostEquals(pointCurrent.deltaK, 0, this.EPSILON)) {
                            pointCurrent.mK = pointAfter.mK = 0;
                            continue;
                        }
                        alphaK = pointCurrent.mK / pointCurrent.deltaK;
                        betaK = pointAfter.mK / pointCurrent.deltaK;
                        squaredMagnitude = Math.pow(alphaK, 2) + Math.pow(betaK, 2);
                        if (squaredMagnitude <= 9) {
                            continue;
                        }
                        tauK = 3 / Math.sqrt(squaredMagnitude);
                        pointCurrent.mK = alphaK * tauK * pointCurrent.deltaK;
                        pointAfter.mK = betaK * tauK * pointCurrent.deltaK;
                    }
                    var deltaX;
                    for (i = 0; i < pointsLen; ++i) {
                        pointCurrent = pointsWithTangents[i];
                        if (pointCurrent.model.skip) {
                            continue;
                        }
                        pointBefore = i > 0 ? pointsWithTangents[i - 1] : null;
                        pointAfter = i < pointsLen - 1 ? pointsWithTangents[i + 1] : null;
                        if (pointBefore && !pointBefore.model.skip) {
                            deltaX = (pointCurrent.model.x - pointBefore.model.x) / 3;
                            pointCurrent.model.controlPointPreviousX = pointCurrent.model.x - deltaX;
                            pointCurrent.model.controlPointPreviousY = pointCurrent.model.y - deltaX * pointCurrent.mK;
                        }
                        if (pointAfter && !pointAfter.model.skip) {
                            deltaX = (pointAfter.model.x - pointCurrent.model.x) / 3;
                            pointCurrent.model.controlPointNextX = pointCurrent.model.x + deltaX;
                            pointCurrent.model.controlPointNextY = pointCurrent.model.y + deltaX * pointCurrent.mK;
                        }
                    }
                };
                helpers.nextItem = function(collection, index, loop) {
                    if (loop) {
                        return index >= collection.length - 1 ? collection[0] : collection[index + 1];
                    }
                    return index >= collection.length - 1 ? collection[collection.length - 1] : collection[index + 1];
                };
                helpers.previousItem = function(collection, index, loop) {
                    if (loop) {
                        return index <= 0 ? collection[collection.length - 1] : collection[index - 1];
                    }
                    return index <= 0 ? collection[0] : collection[index - 1];
                };
                helpers.niceNum = function(range, round) {
                    var exponent = Math.floor(helpers.log10(range));
                    var fraction = range / Math.pow(10, exponent);
                    var niceFraction;
                    if (round) {
                        if (fraction < 1.5) {
                            niceFraction = 1;
                        } else if (fraction < 3) {
                            niceFraction = 2;
                        } else if (fraction < 7) {
                            niceFraction = 5;
                        } else {
                            niceFraction = 10;
                        }
                    } else if (fraction <= 1.0) {
                        niceFraction = 1;
                    } else if (fraction <= 2) {
                        niceFraction = 2;
                    } else if (fraction <= 5) {
                        niceFraction = 5;
                    } else {
                        niceFraction = 10;
                    }
                    return niceFraction * Math.pow(10, exponent);
                };
                helpers.requestAnimFrame = (function() {
                    if (typeof window === 'undefined') {
                        return function(callback) {
                            callback();
                        };
                    }
                    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
                        return window.setTimeout(callback, 1000 / 60);
                    };
                }());
                helpers.getRelativePosition = function(evt, chart) {
                    var mouseX, mouseY;
                    var e = evt.originalEvent || evt;
                    var canvas = evt.currentTarget || evt.srcElement;
                    var boundingRect = canvas.getBoundingClientRect();
                    var touches = e.touches;
                    if (touches && touches.length > 0) {
                        mouseX = touches[0].clientX;
                        mouseY = touches[0].clientY;
                    } else {
                        mouseX = e.clientX;
                        mouseY = e.clientY;
                    }
                    var paddingLeft = parseFloat(helpers.getStyle(canvas, 'padding-left'));
                    var paddingTop = parseFloat(helpers.getStyle(canvas, 'padding-top'));
                    var paddingRight = parseFloat(helpers.getStyle(canvas, 'padding-right'));
                    var paddingBottom = parseFloat(helpers.getStyle(canvas, 'padding-bottom'));
                    var width = boundingRect.right - boundingRect.left - paddingLeft - paddingRight;
                    var height = boundingRect.bottom - boundingRect.top - paddingTop - paddingBottom;
                    mouseX = Math.round((mouseX - boundingRect.left - paddingLeft) / (width) * canvas.width / chart.currentDevicePixelRatio);
                    mouseY = Math.round((mouseY - boundingRect.top - paddingTop) / (height) * canvas.height / chart.currentDevicePixelRatio);
                    return {
                        x: mouseX,
                        y: mouseY
                    };
                };

                function parseMaxStyle(styleValue, node, parentProperty) {
                    var valueInPixels;
                    if (typeof styleValue === 'string') {
                        valueInPixels = parseInt(styleValue, 10);
                        if (styleValue.indexOf('%') !== -1) {
                            valueInPixels = valueInPixels / 100 * node.parentNode[parentProperty];
                        }
                    } else {
                        valueInPixels = styleValue;
                    }
                    return valueInPixels;
                }

                function isConstrainedValue(value) {
                    return value !== undefined && value !== null && value !== 'none';
                }

                function getConstraintDimension(domNode, maxStyle, percentageProperty) {
                    var view = document.defaultView;
                    var parentNode = domNode.parentNode;
                    var constrainedNode = view.getComputedStyle(domNode)[maxStyle];
                    var constrainedContainer = view.getComputedStyle(parentNode)[maxStyle];
                    var hasCNode = isConstrainedValue(constrainedNode);
                    var hasCContainer = isConstrainedValue(constrainedContainer);
                    var infinity = Number.POSITIVE_INFINITY;
                    if (hasCNode || hasCContainer) {
                        return Math.min(hasCNode ? parseMaxStyle(constrainedNode, domNode, percentageProperty) : infinity, hasCContainer ? parseMaxStyle(constrainedContainer, parentNode, percentageProperty) : infinity);
                    }
                    return 'none';
                }
                helpers.getConstraintWidth = function(domNode) {
                    return getConstraintDimension(domNode, 'max-width', 'clientWidth');
                };
                helpers.getConstraintHeight = function(domNode) {
                    return getConstraintDimension(domNode, 'max-height', 'clientHeight');
                };
                helpers.getMaximumWidth = function(domNode) {
                    var container = domNode.parentNode;
                    if (!container) {
                        return domNode.clientWidth;
                    }
                    var paddingLeft = parseInt(helpers.getStyle(container, 'padding-left'), 10);
                    var paddingRight = parseInt(helpers.getStyle(container, 'padding-right'), 10);
                    var w = container.clientWidth - paddingLeft - paddingRight;
                    var cw = helpers.getConstraintWidth(domNode);
                    return isNaN(cw) ? w : Math.min(w, cw);
                };
                helpers.getMaximumHeight = function(domNode) {
                    var container = domNode.parentNode;
                    if (!container) {
                        return domNode.clientHeight;
                    }
                    var paddingTop = parseInt(helpers.getStyle(container, 'padding-top'), 10);
                    var paddingBottom = parseInt(helpers.getStyle(container, 'padding-bottom'), 10);
                    var h = container.clientHeight - paddingTop - paddingBottom;
                    var ch = helpers.getConstraintHeight(domNode);
                    return isNaN(ch) ? h : Math.min(h, ch);
                };
                helpers.getStyle = function(el, property) {
                    return el.currentStyle ? el.currentStyle[property] : document.defaultView.getComputedStyle(el, null).getPropertyValue(property);
                };
                helpers.retinaScale = function(chart, forceRatio) {
                    var pixelRatio = chart.currentDevicePixelRatio = forceRatio || window.devicePixelRatio || 1;
                    if (pixelRatio === 1) {
                        return;
                    }
                    var canvas = chart.canvas;
                    var height = chart.height;
                    var width = chart.width;
                    canvas.height = height * pixelRatio;
                    canvas.width = width * pixelRatio;
                    chart.ctx.scale(pixelRatio, pixelRatio);
                    if (!canvas.style.height && !canvas.style.width) {
                        canvas.style.height = height + 'px';
                        canvas.style.width = width + 'px';
                    }
                };
                helpers.fontString = function(pixelSize, fontStyle, fontFamily) {
                    return fontStyle + ' ' + pixelSize + 'px ' + fontFamily;
                };
                helpers.longestText = function(ctx, font, arrayOfThings, cache) {
                    cache = cache || {};
                    var data = cache.data = cache.data || {};
                    var gc = cache.garbageCollect = cache.garbageCollect || [];
                    if (cache.font !== font) {
                        data = cache.data = {};
                        gc = cache.garbageCollect = [];
                        cache.font = font;
                    }
                    ctx.font = font;
                    var longest = 0;
                    helpers.each(arrayOfThings, function(thing) {
                        if (thing !== undefined && thing !== null && helpers.isArray(thing) !== true) {
                            longest = helpers.measureText(ctx, data, gc, longest, thing);
                        } else if (helpers.isArray(thing)) {
                            helpers.each(thing, function(nestedThing) {
                                if (nestedThing !== undefined && nestedThing !== null && !helpers.isArray(nestedThing)) {
                                    longest = helpers.measureText(ctx, data, gc, longest, nestedThing);
                                }
                            });
                        }
                    });
                    var gcLen = gc.length / 2;
                    if (gcLen > arrayOfThings.length) {
                        for (var i = 0; i < gcLen; i++) {
                            delete data[gc[i]];
                        }
                        gc.splice(0, gcLen);
                    }
                    return longest;
                };
                helpers.measureText = function(ctx, data, gc, longest, string) {
                    var textWidth = data[string];
                    if (!textWidth) {
                        textWidth = data[string] = ctx.measureText(string).width;
                        gc.push(string);
                    }
                    if (textWidth > longest) {
                        longest = textWidth;
                    }
                    return longest;
                };
                helpers.numberOfLabelLines = function(arrayOfThings) {
                    var numberOfLines = 1;
                    helpers.each(arrayOfThings, function(thing) {
                        if (helpers.isArray(thing)) {
                            if (thing.length > numberOfLines) {
                                numberOfLines = thing.length;
                            }
                        }
                    });
                    return numberOfLines;
                };
                helpers.color = !color ? function(value) {
                    console.error('Color.js not found!');
                    return value;
                } : function(value) {
                    if (value instanceof CanvasGradient) {
                        value = defaults.global.defaultColor;
                    }
                    return color(value);
                };
                helpers.getHoverColor = function(colorValue) {
                    return (colorValue instanceof CanvasPattern) ? colorValue : helpers.color(colorValue).saturate(0.5).darken(0.1).rgbString();
                };
            };
        }, {
            "25": 25,
            "3": 3,
            "45": 45
        }],
        28: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);

            function getRelativePosition(e, chart) {
                if (e.native) {
                    return {
                        x: e.x,
                        y: e.y
                    };
                }
                return helpers.getRelativePosition(e, chart);
            }

            function parseVisibleItems(chart, handler) {
                var datasets = chart.data.datasets;
                var meta, i, j, ilen, jlen;
                for (i = 0, ilen = datasets.length; i < ilen; ++i) {
                    if (!chart.isDatasetVisible(i)) {
                        continue;
                    }
                    meta = chart.getDatasetMeta(i);
                    for (j = 0, jlen = meta.data.length; j < jlen; ++j) {
                        var element = meta.data[j];
                        if (!element._view.skip) {
                            handler(element);
                        }
                    }
                }
            }

            function getIntersectItems(chart, position) {
                var elements = [];
                parseVisibleItems(chart, function(element) {
                    if (element.inRange(position.x, position.y)) {
                        elements.push(element);
                    }
                });
                return elements;
            }

            function getNearestItems(chart, position, intersect, distanceMetric) {
                var minDistance = Number.POSITIVE_INFINITY;
                var nearestItems = [];
                parseVisibleItems(chart, function(element) {
                    if (intersect && !element.inRange(position.x, position.y)) {
                        return;
                    }
                    var center = element.getCenterPoint();
                    var distance = distanceMetric(position, center);
                    if (distance < minDistance) {
                        nearestItems = [element];
                        minDistance = distance;
                    } else if (distance === minDistance) {
                        nearestItems.push(element);
                    }
                });
                return nearestItems;
            }

            function getDistanceMetricForAxis(axis) {
                var useX = axis.indexOf('x') !== -1;
                var useY = axis.indexOf('y') !== -1;
                return function(pt1, pt2) {
                    var deltaX = useX ? Math.abs(pt1.x - pt2.x) : 0;
                    var deltaY = useY ? Math.abs(pt1.y - pt2.y) : 0;
                    return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
                };
            }

            function indexMode(chart, e, options) {
                var position = getRelativePosition(e, chart);
                options.axis = options.axis || 'x';
                var distanceMetric = getDistanceMetricForAxis(options.axis);
                var items = options.intersect ? getIntersectItems(chart, position) : getNearestItems(chart, position, false, distanceMetric);
                var elements = [];
                if (!items.length) {
                    return [];
                }
                chart.data.datasets.forEach(function(dataset, datasetIndex) {
                    if (chart.isDatasetVisible(datasetIndex)) {
                        var meta = chart.getDatasetMeta(datasetIndex);
                        var element = meta.data[items[0]._index];
                        if (element && !element._view.skip) {
                            elements.push(element);
                        }
                    }
                });
                return elements;
            }
            module.exports = {
                modes: {
                    single: function(chart, e) {
                        var position = getRelativePosition(e, chart);
                        var elements = [];
                        parseVisibleItems(chart, function(element) {
                            if (element.inRange(position.x, position.y)) {
                                elements.push(element);
                                return elements;
                            }
                        });
                        return elements.slice(0, 1);
                    },
                    label: indexMode,
                    index: indexMode,
                    dataset: function(chart, e, options) {
                        var position = getRelativePosition(e, chart);
                        options.axis = options.axis || 'xy';
                        var distanceMetric = getDistanceMetricForAxis(options.axis);
                        var items = options.intersect ? getIntersectItems(chart, position) : getNearestItems(chart, position, false, distanceMetric);
                        if (items.length > 0) {
                            items = chart.getDatasetMeta(items[0]._datasetIndex).data;
                        }
                        return items;
                    },
                    'x-axis': function(chart, e) {
                        return indexMode(chart, e, {
                            intersect: false
                        });
                    },
                    point: function(chart, e) {
                        var position = getRelativePosition(e, chart);
                        return getIntersectItems(chart, position);
                    },
                    nearest: function(chart, e, options) {
                        var position = getRelativePosition(e, chart);
                        options.axis = options.axis || 'xy';
                        var distanceMetric = getDistanceMetricForAxis(options.axis);
                        var nearestItems = getNearestItems(chart, position, options.intersect, distanceMetric);
                        if (nearestItems.length > 1) {
                            nearestItems.sort(function(a, b) {
                                var sizeA = a.getArea();
                                var sizeB = b.getArea();
                                var ret = sizeA - sizeB;
                                if (ret === 0) {
                                    ret = a._datasetIndex - b._datasetIndex;
                                }
                                return ret;
                            });
                        }
                        return nearestItems.slice(0, 1);
                    },
                    x: function(chart, e, options) {
                        var position = getRelativePosition(e, chart);
                        var items = [];
                        var intersectsItem = false;
                        parseVisibleItems(chart, function(element) {
                            if (element.inXRange(position.x)) {
                                items.push(element);
                            }
                            if (element.inRange(position.x, position.y)) {
                                intersectsItem = true;
                            }
                        });
                        if (options.intersect && !intersectsItem) {
                            items = [];
                        }
                        return items;
                    },
                    y: function(chart, e, options) {
                        var position = getRelativePosition(e, chart);
                        var items = [];
                        var intersectsItem = false;
                        parseVisibleItems(chart, function(element) {
                            if (element.inYRange(position.y)) {
                                items.push(element);
                            }
                            if (element.inRange(position.x, position.y)) {
                                intersectsItem = true;
                            }
                        });
                        if (options.intersect && !intersectsItem) {
                            items = [];
                        }
                        return items;
                    }
                }
            };
        }, {
            "45": 45
        }],
        29: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            defaults._set('global', {
                responsive: true,
                responsiveAnimationDuration: 0,
                maintainAspectRatio: true,
                events: ['mousemove', 'mouseout', 'click', 'touchstart', 'touchmove'],
                hover: {
                    onHover: null,
                    mode: 'nearest',
                    intersect: true,
                    animationDuration: 400
                },
                onClick: null,
                defaultColor: 'rgba(0,0,0,0.1)',
                defaultFontColor: '#666',
                defaultFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                defaultFontSize: 12,
                defaultFontStyle: 'normal',
                showLines: true,
                elements: {},
                layout: {
                    padding: {
                        top: 0,
                        right: 0,
                        bottom: 0,
                        left: 0
                    }
                }
            });
            module.exports = function() {
                var Chart = function(item, config) {
                    this.construct(item, config);
                    return this;
                };
                Chart.Chart = Chart;
                return Chart;
            };
        }, {
            "25": 25
        }],
        30: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);

            function filterByPosition(array, position) {
                return helpers.where(array, function(v) {
                    return v.position === position;
                });
            }

            function sortByWeight(array, reverse) {
                array.forEach(function(v, i) {
                    v._tmpIndex_ = i;
                    return v;
                });
                array.sort(function(a, b) {
                    var v0 = reverse ? b : a;
                    var v1 = reverse ? a : b;
                    return v0.weight === v1.weight ? v0._tmpIndex_ - v1._tmpIndex_ : v0.weight - v1.weight;
                });
                array.forEach(function(v) {
                    delete v._tmpIndex_;
                });
            }
            module.exports = {
                defaults: {},
                addBox: function(chart, item) {
                    if (!chart.boxes) {
                        chart.boxes = [];
                    }
                    item.fullWidth = item.fullWidth || false;
                    item.position = item.position || 'top';
                    item.weight = item.weight || 0;
                    chart.boxes.push(item);
                },
                removeBox: function(chart, layoutItem) {
                    var index = chart.boxes ? chart.boxes.indexOf(layoutItem) : -1;
                    if (index !== -1) {
                        chart.boxes.splice(index, 1);
                    }
                },
                configure: function(chart, item, options) {
                    var props = ['fullWidth', 'position', 'weight'];
                    var ilen = props.length;
                    var i = 0;
                    var prop;
                    for (; i < ilen; ++i) {
                        prop = props[i];
                        if (options.hasOwnProperty(prop)) {
                            item[prop] = options[prop];
                        }
                    }
                },
                update: function(chart, width, height) {
                    if (!chart) {
                        return;
                    }
                    var layoutOptions = chart.options.layout || {};
                    var padding = helpers.options.toPadding(layoutOptions.padding);
                    var leftPadding = padding.left;
                    var rightPadding = padding.right;
                    var topPadding = padding.top;
                    var bottomPadding = padding.bottom;
                    var leftBoxes = filterByPosition(chart.boxes, 'left');
                    var rightBoxes = filterByPosition(chart.boxes, 'right');
                    var topBoxes = filterByPosition(chart.boxes, 'top');
                    var bottomBoxes = filterByPosition(chart.boxes, 'bottom');
                    var chartAreaBoxes = filterByPosition(chart.boxes, 'chartArea');
                    sortByWeight(leftBoxes, true);
                    sortByWeight(rightBoxes, false);
                    sortByWeight(topBoxes, true);
                    sortByWeight(bottomBoxes, false);
                    var chartWidth = width - leftPadding - rightPadding;
                    var chartHeight = height - topPadding - bottomPadding;
                    var chartAreaWidth = chartWidth / 2;
                    var chartAreaHeight = chartHeight / 2;
                    var verticalBoxWidth = (width - chartAreaWidth) / (leftBoxes.length + rightBoxes.length);
                    var horizontalBoxHeight = (height - chartAreaHeight) / (topBoxes.length + bottomBoxes.length);
                    var maxChartAreaWidth = chartWidth;
                    var maxChartAreaHeight = chartHeight;
                    var minBoxSizes = [];

                    function getMinimumBoxSize(box) {
                        var minSize;
                        var isHorizontal = box.isHorizontal();
                        if (isHorizontal) {
                            minSize = box.update(box.fullWidth ? chartWidth : maxChartAreaWidth, horizontalBoxHeight);
                            maxChartAreaHeight -= minSize.height;
                        } else {
                            minSize = box.update(verticalBoxWidth, maxChartAreaHeight);
                            maxChartAreaWidth -= minSize.width;
                        }
                        minBoxSizes.push({
                            horizontal: isHorizontal,
                            minSize: minSize,
                            box: box,
                        });
                    }
                    helpers.each(leftBoxes.concat(rightBoxes, topBoxes, bottomBoxes), getMinimumBoxSize);
                    var maxHorizontalLeftPadding = 0;
                    var maxHorizontalRightPadding = 0;
                    var maxVerticalTopPadding = 0;
                    var maxVerticalBottomPadding = 0;
                    helpers.each(topBoxes.concat(bottomBoxes), function(horizontalBox) {
                        if (horizontalBox.getPadding) {
                            var boxPadding = horizontalBox.getPadding();
                            maxHorizontalLeftPadding = Math.max(maxHorizontalLeftPadding, boxPadding.left);
                            maxHorizontalRightPadding = Math.max(maxHorizontalRightPadding, boxPadding.right);
                        }
                    });
                    helpers.each(leftBoxes.concat(rightBoxes), function(verticalBox) {
                        if (verticalBox.getPadding) {
                            var boxPadding = verticalBox.getPadding();
                            maxVerticalTopPadding = Math.max(maxVerticalTopPadding, boxPadding.top);
                            maxVerticalBottomPadding = Math.max(maxVerticalBottomPadding, boxPadding.bottom);
                        }
                    });
                    var totalLeftBoxesWidth = leftPadding;
                    var totalRightBoxesWidth = rightPadding;
                    var totalTopBoxesHeight = topPadding;
                    var totalBottomBoxesHeight = bottomPadding;

                    function fitBox(box) {
                        var minBoxSize = helpers.findNextWhere(minBoxSizes, function(minBox) {
                            return minBox.box === box;
                        });
                        if (minBoxSize) {
                            if (box.isHorizontal()) {
                                var scaleMargin = {
                                    left: Math.max(totalLeftBoxesWidth, maxHorizontalLeftPadding),
                                    right: Math.max(totalRightBoxesWidth, maxHorizontalRightPadding),
                                    top: 0,
                                    bottom: 0
                                };
                                box.update(box.fullWidth ? chartWidth : maxChartAreaWidth, chartHeight / 2, scaleMargin);
                            } else {
                                box.update(minBoxSize.minSize.width, maxChartAreaHeight);
                            }
                        }
                    }
                    helpers.each(leftBoxes.concat(rightBoxes), fitBox);
                    helpers.each(leftBoxes, function(box) {
                        totalLeftBoxesWidth += box.width;
                    });
                    helpers.each(rightBoxes, function(box) {
                        totalRightBoxesWidth += box.width;
                    });
                    helpers.each(topBoxes.concat(bottomBoxes), fitBox);
                    helpers.each(topBoxes, function(box) {
                        totalTopBoxesHeight += box.height;
                    });
                    helpers.each(bottomBoxes, function(box) {
                        totalBottomBoxesHeight += box.height;
                    });

                    function finalFitVerticalBox(box) {
                        var minBoxSize = helpers.findNextWhere(minBoxSizes, function(minSize) {
                            return minSize.box === box;
                        });
                        var scaleMargin = {
                            left: 0,
                            right: 0,
                            top: totalTopBoxesHeight,
                            bottom: totalBottomBoxesHeight
                        };
                        if (minBoxSize) {
                            box.update(minBoxSize.minSize.width, maxChartAreaHeight, scaleMargin);
                        }
                    }
                    helpers.each(leftBoxes.concat(rightBoxes), finalFitVerticalBox);
                    totalLeftBoxesWidth = leftPadding;
                    totalRightBoxesWidth = rightPadding;
                    totalTopBoxesHeight = topPadding;
                    totalBottomBoxesHeight = bottomPadding;
                    helpers.each(leftBoxes, function(box) {
                        totalLeftBoxesWidth += box.width;
                    });
                    helpers.each(rightBoxes, function(box) {
                        totalRightBoxesWidth += box.width;
                    });
                    helpers.each(topBoxes, function(box) {
                        totalTopBoxesHeight += box.height;
                    });
                    helpers.each(bottomBoxes, function(box) {
                        totalBottomBoxesHeight += box.height;
                    });
                    var leftPaddingAddition = Math.max(maxHorizontalLeftPadding - totalLeftBoxesWidth, 0);
                    totalLeftBoxesWidth += leftPaddingAddition;
                    totalRightBoxesWidth += Math.max(maxHorizontalRightPadding - totalRightBoxesWidth, 0);
                    var topPaddingAddition = Math.max(maxVerticalTopPadding - totalTopBoxesHeight, 0);
                    totalTopBoxesHeight += topPaddingAddition;
                    totalBottomBoxesHeight += Math.max(maxVerticalBottomPadding - totalBottomBoxesHeight, 0);
                    var newMaxChartAreaHeight = height - totalTopBoxesHeight - totalBottomBoxesHeight;
                    var newMaxChartAreaWidth = width - totalLeftBoxesWidth - totalRightBoxesWidth;
                    if (newMaxChartAreaWidth !== maxChartAreaWidth || newMaxChartAreaHeight !== maxChartAreaHeight) {
                        helpers.each(leftBoxes, function(box) {
                            box.height = newMaxChartAreaHeight;
                        });
                        helpers.each(rightBoxes, function(box) {
                            box.height = newMaxChartAreaHeight;
                        });
                        helpers.each(topBoxes, function(box) {
                            if (!box.fullWidth) {
                                box.width = newMaxChartAreaWidth;
                            }
                        });
                        helpers.each(bottomBoxes, function(box) {
                            if (!box.fullWidth) {
                                box.width = newMaxChartAreaWidth;
                            }
                        });
                        maxChartAreaHeight = newMaxChartAreaHeight;
                        maxChartAreaWidth = newMaxChartAreaWidth;
                    }
                    var left = leftPadding + leftPaddingAddition;
                    var top = topPadding + topPaddingAddition;

                    function placeBox(box) {
                        if (box.isHorizontal()) {
                            box.left = box.fullWidth ? leftPadding : totalLeftBoxesWidth;
                            box.right = box.fullWidth ? width - rightPadding : totalLeftBoxesWidth + maxChartAreaWidth;
                            box.top = top;
                            box.bottom = top + box.height;
                            top = box.bottom;
                        } else {
                            box.left = left;
                            box.right = left + box.width;
                            box.top = totalTopBoxesHeight;
                            box.bottom = totalTopBoxesHeight + maxChartAreaHeight;
                            left = box.right;
                        }
                    }
                    helpers.each(leftBoxes.concat(topBoxes), placeBox);
                    left += maxChartAreaWidth;
                    top += maxChartAreaHeight;
                    helpers.each(rightBoxes, placeBox);
                    helpers.each(bottomBoxes, placeBox);
                    chart.chartArea = {
                        left: totalLeftBoxesWidth,
                        top: totalTopBoxesHeight,
                        right: totalLeftBoxesWidth + maxChartAreaWidth,
                        bottom: totalTopBoxesHeight + maxChartAreaHeight
                    };
                    helpers.each(chartAreaBoxes, function(box) {
                        box.left = chart.chartArea.left;
                        box.top = chart.chartArea.top;
                        box.right = chart.chartArea.right;
                        box.bottom = chart.chartArea.bottom;
                        box.update(maxChartAreaWidth, maxChartAreaHeight);
                    });
                }
            };
        }, {
            "45": 45
        }],
        31: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var helpers = require(45);
            defaults._set('global', {
                plugins: {}
            });
            module.exports = {
                _plugins: [],
                _cacheId: 0,
                register: function(plugins) {
                    var p = this._plugins;
                    ([]).concat(plugins).forEach(function(plugin) {
                        if (p.indexOf(plugin) === -1) {
                            p.push(plugin);
                        }
                    });
                    this._cacheId++;
                },
                unregister: function(plugins) {
                    var p = this._plugins;
                    ([]).concat(plugins).forEach(function(plugin) {
                        var idx = p.indexOf(plugin);
                        if (idx !== -1) {
                            p.splice(idx, 1);
                        }
                    });
                    this._cacheId++;
                },
                clear: function() {
                    this._plugins = [];
                    this._cacheId++;
                },
                count: function() {
                    return this._plugins.length;
                },
                getAll: function() {
                    return this._plugins;
                },
                notify: function(chart, hook, args) {
                    var descriptors = this.descriptors(chart);
                    var ilen = descriptors.length;
                    var i, descriptor, plugin, params, method;
                    for (i = 0; i < ilen; ++i) {
                        descriptor = descriptors[i];
                        plugin = descriptor.plugin;
                        method = plugin[hook];
                        if (typeof method === 'function') {
                            params = [chart].concat(args || []);
                            params.push(descriptor.options);
                            if (method.apply(plugin, params) === false) {
                                return false;
                            }
                        }
                    }
                    return true;
                },
                descriptors: function(chart) {
                    var cache = chart.$plugins || (chart.$plugins = {});
                    if (cache.id === this._cacheId) {
                        return cache.descriptors;
                    }
                    var plugins = [];
                    var descriptors = [];
                    var config = (chart && chart.config) || {};
                    var options = (config.options && config.options.plugins) || {};
                    this._plugins.concat(config.plugins || []).forEach(function(plugin) {
                        var idx = plugins.indexOf(plugin);
                        if (idx !== -1) {
                            return;
                        }
                        var id = plugin.id;
                        var opts = options[id];
                        if (opts === false) {
                            return;
                        }
                        if (opts === true) {
                            opts = helpers.clone(defaults.global.plugins[id]);
                        }
                        plugins.push(plugin);
                        descriptors.push({
                            plugin: plugin,
                            options: opts || {}
                        });
                    });
                    cache.descriptors = descriptors;
                    cache.id = this._cacheId;
                    return descriptors;
                },
                _invalidate: function(chart) {
                    delete chart.$plugins;
                }
            };
        }, {
            "25": 25,
            "45": 45
        }],
        32: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            var Ticks = require(34);
            defaults._set('scale', {
                display: true,
                position: 'left',
                offset: false,
                gridLines: {
                    display: true,
                    color: 'rgba(0, 0, 0, 0.1)',
                    lineWidth: 1,
                    drawBorder: true,
                    drawOnChartArea: true,
                    drawTicks: true,
                    tickMarkLength: 10,
                    zeroLineWidth: 1,
                    zeroLineColor: 'rgba(0,0,0,0.25)',
                    zeroLineBorderDash: [],
                    zeroLineBorderDashOffset: 0.0,
                    offsetGridLines: false,
                    borderDash: [],
                    borderDashOffset: 0.0
                },
                scaleLabel: {
                    display: false,
                    labelString: '',
                    lineHeight: 1.2,
                    padding: {
                        top: 4,
                        bottom: 4
                    }
                },
                ticks: {
                    beginAtZero: false,
                    minRotation: 0,
                    maxRotation: 50,
                    mirror: false,
                    padding: 0,
                    reverse: false,
                    display: true,
                    autoSkip: true,
                    autoSkipPadding: 0,
                    labelOffset: 0,
                    callback: Ticks.formatters.values,
                    minor: {},
                    major: {}
                }
            });

            function labelsFromTicks(ticks) {
                var labels = [];
                var i, ilen;
                for (i = 0, ilen = ticks.length; i < ilen; ++i) {
                    labels.push(ticks[i].label);
                }
                return labels;
            }

            function getLineValue(scale, index, offsetGridLines) {
                var lineValue = scale.getPixelForTick(index);
                if (offsetGridLines) {
                    if (index === 0) {
                        lineValue -= (scale.getPixelForTick(1) - lineValue) / 2;
                    } else {
                        lineValue -= (lineValue - scale.getPixelForTick(index - 1)) / 2;
                    }
                }
                return lineValue;
            }
            module.exports = function(Chart) {
                function computeTextSize(context, tick, font) {
                    return helpers.isArray(tick) ? helpers.longestText(context, font, tick) : context.measureText(tick).width;
                }

                function parseFontOptions(options) {
                    var valueOrDefault = helpers.valueOrDefault;
                    var globalDefaults = defaults.global;
                    var size = valueOrDefault(options.fontSize, globalDefaults.defaultFontSize);
                    var style = valueOrDefault(options.fontStyle, globalDefaults.defaultFontStyle);
                    var family = valueOrDefault(options.fontFamily, globalDefaults.defaultFontFamily);
                    return {
                        size: size,
                        style: style,
                        family: family,
                        font: helpers.fontString(size, style, family)
                    };
                }

                function parseLineHeight(options) {
                    return helpers.options.toLineHeight(helpers.valueOrDefault(options.lineHeight, 1.2), helpers.valueOrDefault(options.fontSize, defaults.global.defaultFontSize));
                }
                Chart.Scale = Element.extend({
                    getPadding: function() {
                        var me = this;
                        return {
                            left: me.paddingLeft || 0,
                            top: me.paddingTop || 0,
                            right: me.paddingRight || 0,
                            bottom: me.paddingBottom || 0
                        };
                    },
                    getTicks: function() {
                        return this._ticks;
                    },
                    mergeTicksOptions: function() {
                        var ticks = this.options.ticks;
                        if (ticks.minor === false) {
                            ticks.minor = {
                                display: false
                            };
                        }
                        if (ticks.major === false) {
                            ticks.major = {
                                display: false
                            };
                        }
                        for (var key in ticks) {
                            if (key !== 'major' && key !== 'minor') {
                                if (typeof ticks.minor[key] === 'undefined') {
                                    ticks.minor[key] = ticks[key];
                                }
                                if (typeof ticks.major[key] === 'undefined') {
                                    ticks.major[key] = ticks[key];
                                }
                            }
                        }
                    },
                    beforeUpdate: function() {
                        helpers.callback(this.options.beforeUpdate, [this]);
                    },
                    update: function(maxWidth, maxHeight, margins) {
                        var me = this;
                        var i, ilen, labels, label, ticks, tick;
                        me.beforeUpdate();
                        me.maxWidth = maxWidth;
                        me.maxHeight = maxHeight;
                        me.margins = helpers.extend({
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }, margins);
                        me.longestTextCache = me.longestTextCache || {};
                        me.beforeSetDimensions();
                        me.setDimensions();
                        me.afterSetDimensions();
                        me.beforeDataLimits();
                        me.determineDataLimits();
                        me.afterDataLimits();
                        me.beforeBuildTicks();
                        ticks = me.buildTicks() || [];
                        me.afterBuildTicks();
                        me.beforeTickToLabelConversion();
                        labels = me.convertTicksToLabels(ticks) || me.ticks;
                        me.afterTickToLabelConversion();
                        me.ticks = labels;
                        for (i = 0, ilen = labels.length; i < ilen; ++i) {
                            label = labels[i];
                            tick = ticks[i];
                            if (!tick) {
                                ticks.push(tick = {
                                    label: label,
                                    major: false
                                });
                            } else {
                                tick.label = label;
                            }
                        }
                        me._ticks = ticks;
                        me.beforeCalculateTickRotation();
                        me.calculateTickRotation();
                        me.afterCalculateTickRotation();
                        me.beforeFit();
                        me.fit();
                        me.afterFit();
                        me.afterUpdate();
                        return me.minSize;
                    },
                    afterUpdate: function() {
                        helpers.callback(this.options.afterUpdate, [this]);
                    },
                    beforeSetDimensions: function() {
                        helpers.callback(this.options.beforeSetDimensions, [this]);
                    },
                    setDimensions: function() {
                        var me = this;
                        if (me.isHorizontal()) {
                            me.width = me.maxWidth;
                            me.left = 0;
                            me.right = me.width;
                        } else {
                            me.height = me.maxHeight;
                            me.top = 0;
                            me.bottom = me.height;
                        }
                        me.paddingLeft = 0;
                        me.paddingTop = 0;
                        me.paddingRight = 0;
                        me.paddingBottom = 0;
                    },
                    afterSetDimensions: function() {
                        helpers.callback(this.options.afterSetDimensions, [this]);
                    },
                    beforeDataLimits: function() {
                        helpers.callback(this.options.beforeDataLimits, [this]);
                    },
                    determineDataLimits: helpers.noop,
                    afterDataLimits: function() {
                        helpers.callback(this.options.afterDataLimits, [this]);
                    },
                    beforeBuildTicks: function() {
                        helpers.callback(this.options.beforeBuildTicks, [this]);
                    },
                    buildTicks: helpers.noop,
                    afterBuildTicks: function() {
                        helpers.callback(this.options.afterBuildTicks, [this]);
                    },
                    beforeTickToLabelConversion: function() {
                        helpers.callback(this.options.beforeTickToLabelConversion, [this]);
                    },
                    convertTicksToLabels: function() {
                        var me = this;
                        var tickOpts = me.options.ticks;
                        me.ticks = me.ticks.map(tickOpts.userCallback || tickOpts.callback, this);
                    },
                    afterTickToLabelConversion: function() {
                        helpers.callback(this.options.afterTickToLabelConversion, [this]);
                    },
                    beforeCalculateTickRotation: function() {
                        helpers.callback(this.options.beforeCalculateTickRotation, [this]);
                    },
                    calculateTickRotation: function() {
                        var me = this;
                        var context = me.ctx;
                        var tickOpts = me.options.ticks;
                        var labels = labelsFromTicks(me._ticks);
                        var tickFont = parseFontOptions(tickOpts);
                        context.font = tickFont.font;
                        var labelRotation = tickOpts.minRotation || 0;
                        if (labels.length && me.options.display && me.isHorizontal()) {
                            var originalLabelWidth = helpers.longestText(context, tickFont.font, labels, me.longestTextCache);
                            var labelWidth = originalLabelWidth;
                            var cosRotation, sinRotation;
                            var tickWidth = me.getPixelForTick(1) - me.getPixelForTick(0) - 6;
                            while (labelWidth > tickWidth && labelRotation < tickOpts.maxRotation) {
                                var angleRadians = helpers.toRadians(labelRotation);
                                cosRotation = Math.cos(angleRadians);
                                sinRotation = Math.sin(angleRadians);
                                if (sinRotation * originalLabelWidth > me.maxHeight) {
                                    labelRotation--;
                                    break;
                                }
                                labelRotation++;
                                labelWidth = cosRotation * originalLabelWidth;
                            }
                        }
                        me.labelRotation = labelRotation;
                    },
                    afterCalculateTickRotation: function() {
                        helpers.callback(this.options.afterCalculateTickRotation, [this]);
                    },
                    beforeFit: function() {
                        helpers.callback(this.options.beforeFit, [this]);
                    },
                    fit: function() {
                        var me = this;
                        var minSize = me.minSize = {
                            width: 0,
                            height: 0
                        };
                        var labels = labelsFromTicks(me._ticks);
                        var opts = me.options;
                        var tickOpts = opts.ticks;
                        var scaleLabelOpts = opts.scaleLabel;
                        var gridLineOpts = opts.gridLines;
                        var display = opts.display;
                        var isHorizontal = me.isHorizontal();
                        var tickFont = parseFontOptions(tickOpts);
                        var tickMarkLength = opts.gridLines.tickMarkLength;
                        if (isHorizontal) {
                            minSize.width = me.isFullWidth() ? me.maxWidth - me.margins.left - me.margins.right : me.maxWidth;
                        } else {
                            minSize.width = display && gridLineOpts.drawTicks ? tickMarkLength : 0;
                        }
                        if (isHorizontal) {
                            minSize.height = display && gridLineOpts.drawTicks ? tickMarkLength : 0;
                        } else {
                            minSize.height = me.maxHeight;
                        }
                        if (scaleLabelOpts.display && display) {
                            var scaleLabelLineHeight = parseLineHeight(scaleLabelOpts);
                            var scaleLabelPadding = helpers.options.toPadding(scaleLabelOpts.padding);
                            var deltaHeight = scaleLabelLineHeight + scaleLabelPadding.height;
                            if (isHorizontal) {
                                minSize.height += deltaHeight;
                            } else {
                                minSize.width += deltaHeight;
                            }
                        }
                        if (tickOpts.display && display) {
                            var largestTextWidth = helpers.longestText(me.ctx, tickFont.font, labels, me.longestTextCache);
                            var tallestLabelHeightInLines = helpers.numberOfLabelLines(labels);
                            var lineSpace = tickFont.size * 0.5;
                            var tickPadding = me.options.ticks.padding;
                            if (isHorizontal) {
                                me.longestLabelWidth = largestTextWidth;
                                var angleRadians = helpers.toRadians(me.labelRotation);
                                var cosRotation = Math.cos(angleRadians);
                                var sinRotation = Math.sin(angleRadians);
                                var labelHeight = (sinRotation * largestTextWidth) + (tickFont.size * tallestLabelHeightInLines) + (lineSpace * (tallestLabelHeightInLines - 1)) + lineSpace;
                                minSize.height = Math.min(me.maxHeight, minSize.height + labelHeight + tickPadding);
                                me.ctx.font = tickFont.font;
                                var firstLabelWidth = computeTextSize(me.ctx, labels[0], tickFont.font);
                                var lastLabelWidth = computeTextSize(me.ctx, labels[labels.length - 1], tickFont.font);
                                if (me.labelRotation !== 0) {
                                    me.paddingLeft = opts.position === 'bottom' ? (cosRotation * firstLabelWidth) + 3 : (cosRotation * lineSpace) + 3;
                                    me.paddingRight = opts.position === 'bottom' ? (cosRotation * lineSpace) + 3 : (cosRotation * lastLabelWidth) + 3;
                                } else {
                                    me.paddingLeft = firstLabelWidth / 2 + 3;
                                    me.paddingRight = lastLabelWidth / 2 + 3;
                                }
                            } else {
                                if (tickOpts.mirror) {
                                    largestTextWidth = 0;
                                } else {
                                    largestTextWidth += tickPadding + lineSpace;
                                }
                                minSize.width = Math.min(me.maxWidth, minSize.width + largestTextWidth);
                                me.paddingTop = tickFont.size / 2;
                                me.paddingBottom = tickFont.size / 2;
                            }
                        }
                        me.handleMargins();
                        me.width = minSize.width;
                        me.height = minSize.height;
                    },
                    handleMargins: function() {
                        var me = this;
                        if (me.margins) {
                            me.paddingLeft = Math.max(me.paddingLeft - me.margins.left, 0);
                            me.paddingTop = Math.max(me.paddingTop - me.margins.top, 0);
                            me.paddingRight = Math.max(me.paddingRight - me.margins.right, 0);
                            me.paddingBottom = Math.max(me.paddingBottom - me.margins.bottom, 0);
                        }
                    },
                    afterFit: function() {
                        helpers.callback(this.options.afterFit, [this]);
                    },
                    isHorizontal: function() {
                        return this.options.position === 'top' || this.options.position === 'bottom';
                    },
                    isFullWidth: function() {
                        return (this.options.fullWidth);
                    },
                    getRightValue: function(rawValue) {
                        if (helpers.isNullOrUndef(rawValue)) {
                            return NaN;
                        }
                        if (typeof rawValue === 'number' && !isFinite(rawValue)) {
                            return NaN;
                        }
                        if (rawValue) {
                            if (this.isHorizontal()) {
                                if (rawValue.x !== undefined) {
                                    return this.getRightValue(rawValue.x);
                                }
                            } else if (rawValue.y !== undefined) {
                                return this.getRightValue(rawValue.y);
                            }
                        }
                        return rawValue;
                    },
                    getLabelForIndex: helpers.noop,
                    getPixelForValue: helpers.noop,
                    getValueForPixel: helpers.noop,
                    getPixelForTick: function(index) {
                        var me = this;
                        var offset = me.options.offset;
                        if (me.isHorizontal()) {
                            var innerWidth = me.width - (me.paddingLeft + me.paddingRight);
                            var tickWidth = innerWidth / Math.max((me._ticks.length - (offset ? 0 : 1)), 1);
                            var pixel = (tickWidth * index) + me.paddingLeft;
                            if (offset) {
                                pixel += tickWidth / 2;
                            }
                            var finalVal = me.left + Math.round(pixel);
                            finalVal += me.isFullWidth() ? me.margins.left : 0;
                            return finalVal;
                        }
                        var innerHeight = me.height - (me.paddingTop + me.paddingBottom);
                        return me.top + (index * (innerHeight / (me._ticks.length - 1)));
                    },
                    getPixelForDecimal: function(decimal) {
                        var me = this;
                        if (me.isHorizontal()) {
                            var innerWidth = me.width - (me.paddingLeft + me.paddingRight);
                            var valueOffset = (innerWidth * decimal) + me.paddingLeft;
                            var finalVal = me.left + Math.round(valueOffset);
                            finalVal += me.isFullWidth() ? me.margins.left : 0;
                            return finalVal;
                        }
                        return me.top + (decimal * me.height);
                    },
                    getBasePixel: function() {
                        return this.getPixelForValue(this.getBaseValue());
                    },
                    getBaseValue: function() {
                        var me = this;
                        var min = me.min;
                        var max = me.max;
                        return me.beginAtZero ? 0 : min < 0 && max < 0 ? max : min > 0 && max > 0 ? min : 0;
                    },
                    _autoSkip: function(ticks) {
                        var skipRatio;
                        var me = this;
                        var isHorizontal = me.isHorizontal();
                        var optionTicks = me.options.ticks.minor;
                        var tickCount = ticks.length;
                        var labelRotationRadians = helpers.toRadians(me.labelRotation);
                        var cosRotation = Math.cos(labelRotationRadians);
                        var longestRotatedLabel = me.longestLabelWidth * cosRotation;
                        var result = [];
                        var i, tick, shouldSkip;
                        var maxTicks;
                        if (optionTicks.maxTicksLimit) {
                            maxTicks = optionTicks.maxTicksLimit;
                        }
                        if (isHorizontal) {
                            skipRatio = false;
                            if ((longestRotatedLabel + optionTicks.autoSkipPadding) * tickCount > (me.width - (me.paddingLeft + me.paddingRight))) {
                                skipRatio = 1 + Math.floor(((longestRotatedLabel + optionTicks.autoSkipPadding) * tickCount) / (me.width - (me.paddingLeft + me.paddingRight)));
                            }
                            if (maxTicks && tickCount > maxTicks) {
                                skipRatio = Math.max(skipRatio, Math.floor(tickCount / maxTicks));
                            }
                        }
                        for (i = 0; i < tickCount; i++) {
                            tick = ticks[i];
                            shouldSkip = (skipRatio > 1 && i % skipRatio > 0) || (i % skipRatio === 0 && i + skipRatio >= tickCount);
                            if (shouldSkip && i !== tickCount - 1) {
                                delete tick.label;
                            }
                            result.push(tick);
                        }
                        return result;
                    },
                    draw: function(chartArea) {
                        var me = this;
                        var options = me.options;
                        if (!options.display) {
                            return;
                        }
                        var context = me.ctx;
                        var globalDefaults = defaults.global;
                        var optionTicks = options.ticks.minor;
                        var optionMajorTicks = options.ticks.major || optionTicks;
                        var gridLines = options.gridLines;
                        var scaleLabel = options.scaleLabel;
                        var isRotated = me.labelRotation !== 0;
                        var isHorizontal = me.isHorizontal();
                        var ticks = optionTicks.autoSkip ? me._autoSkip(me.getTicks()) : me.getTicks();
                        var tickFontColor = helpers.valueOrDefault(optionTicks.fontColor, globalDefaults.defaultFontColor);
                        var tickFont = parseFontOptions(optionTicks);
                        var majorTickFontColor = helpers.valueOrDefault(optionMajorTicks.fontColor, globalDefaults.defaultFontColor);
                        var majorTickFont = parseFontOptions(optionMajorTicks);
                        var tl = gridLines.drawTicks ? gridLines.tickMarkLength : 0;
                        var scaleLabelFontColor = helpers.valueOrDefault(scaleLabel.fontColor, globalDefaults.defaultFontColor);
                        var scaleLabelFont = parseFontOptions(scaleLabel);
                        var scaleLabelPadding = helpers.options.toPadding(scaleLabel.padding);
                        var labelRotationRadians = helpers.toRadians(me.labelRotation);
                        var itemsToDraw = [];
                        var axisWidth = me.options.gridLines.lineWidth;
                        var xTickStart = options.position === 'right' ? me.right : me.right - axisWidth - tl;
                        var xTickEnd = options.position === 'right' ? me.right + tl : me.right;
                        var yTickStart = options.position === 'bottom' ? me.top + axisWidth : me.bottom - tl - axisWidth;
                        var yTickEnd = options.position === 'bottom' ? me.top + axisWidth + tl : me.bottom + axisWidth;
                        helpers.each(ticks, function(tick, index) {
                            if (helpers.isNullOrUndef(tick.label)) {
                                return;
                            }
                            var label = tick.label;
                            var lineWidth, lineColor, borderDash, borderDashOffset;
                            if (index === me.zeroLineIndex && options.offset === gridLines.offsetGridLines) {
                                lineWidth = gridLines.zeroLineWidth;
                                lineColor = gridLines.zeroLineColor;
                                borderDash = gridLines.zeroLineBorderDash;
                                borderDashOffset = gridLines.zeroLineBorderDashOffset;
                            } else {
                                lineWidth = helpers.valueAtIndexOrDefault(gridLines.lineWidth, index);
                                lineColor = helpers.valueAtIndexOrDefault(gridLines.color, index);
                                borderDash = helpers.valueOrDefault(gridLines.borderDash, globalDefaults.borderDash);
                                borderDashOffset = helpers.valueOrDefault(gridLines.borderDashOffset, globalDefaults.borderDashOffset);
                            }
                            var tx1, ty1, tx2, ty2, x1, y1, x2, y2, labelX, labelY;
                            var textAlign = 'middle';
                            var textBaseline = 'middle';
                            var tickPadding = optionTicks.padding;
                            if (isHorizontal) {
                                var labelYOffset = tl + tickPadding;
                                if (options.position === 'bottom') {
                                    textBaseline = !isRotated ? 'top' : 'middle';
                                    textAlign = !isRotated ? 'center' : 'right';
                                    labelY = me.top + labelYOffset;
                                } else {
                                    textBaseline = !isRotated ? 'bottom' : 'middle';
                                    textAlign = !isRotated ? 'center' : 'left';
                                    labelY = me.bottom - labelYOffset;
                                }
                                var xLineValue = getLineValue(me, index, gridLines.offsetGridLines && ticks.length > 1);
                                if (xLineValue < me.left) {
                                    lineColor = 'rgba(0,0,0,0)';
                                }
                                xLineValue += helpers.aliasPixel(lineWidth);
                                labelX = me.getPixelForTick(index) + optionTicks.labelOffset;
                                tx1 = tx2 = x1 = x2 = xLineValue;
                                ty1 = yTickStart;
                                ty2 = yTickEnd;
                                y1 = chartArea.top;
                                y2 = chartArea.bottom + axisWidth;
                            } else {
                                var isLeft = options.position === 'left';
                                var labelXOffset;
                                if (optionTicks.mirror) {
                                    textAlign = isLeft ? 'left' : 'right';
                                    labelXOffset = tickPadding;
                                } else {
                                    textAlign = isLeft ? 'right' : 'left';
                                    labelXOffset = tl + tickPadding;
                                }
                                labelX = isLeft ? me.right - labelXOffset : me.left + labelXOffset;
                                var yLineValue = getLineValue(me, index, gridLines.offsetGridLines && ticks.length > 1);
                                if (yLineValue < me.top) {
                                    lineColor = 'rgba(0,0,0,0)';
                                }
                                yLineValue += helpers.aliasPixel(lineWidth);
                                labelY = me.getPixelForTick(index) + optionTicks.labelOffset;
                                tx1 = xTickStart;
                                tx2 = xTickEnd;
                                x1 = chartArea.left;
                                x2 = chartArea.right + axisWidth;
                                ty1 = ty2 = y1 = y2 = yLineValue;
                            }
                            itemsToDraw.push({
                                tx1: tx1,
                                ty1: ty1,
                                tx2: tx2,
                                ty2: ty2,
                                x1: x1,
                                y1: y1,
                                x2: x2,
                                y2: y2,
                                labelX: labelX,
                                labelY: labelY,
                                glWidth: lineWidth,
                                glColor: lineColor,
                                glBorderDash: borderDash,
                                glBorderDashOffset: borderDashOffset,
                                rotation: -1 * labelRotationRadians,
                                label: label,
                                major: tick.major,
                                textBaseline: textBaseline,
                                textAlign: textAlign
                            });
                        });
                        helpers.each(itemsToDraw, function(itemToDraw) {
                            if (gridLines.display) {
                                context.save();
                                context.lineWidth = itemToDraw.glWidth;
                                context.strokeStyle = itemToDraw.glColor;
                                if (context.setLineDash) {
                                    context.setLineDash(itemToDraw.glBorderDash);
                                    context.lineDashOffset = itemToDraw.glBorderDashOffset;
                                }
                                context.beginPath();
                                if (gridLines.drawTicks) {
                                    context.moveTo(itemToDraw.tx1, itemToDraw.ty1);
                                    context.lineTo(itemToDraw.tx2, itemToDraw.ty2);
                                }
                                if (gridLines.drawOnChartArea) {
                                    context.moveTo(itemToDraw.x1, itemToDraw.y1);
                                    context.lineTo(itemToDraw.x2, itemToDraw.y2);
                                }
                                context.stroke();
                                context.restore();
                            }
                            if (optionTicks.display) {
                                context.save();
                                context.translate(itemToDraw.labelX, itemToDraw.labelY);
                                context.rotate(itemToDraw.rotation);
                                context.font = itemToDraw.major ? majorTickFont.font : tickFont.font;
                                context.fillStyle = itemToDraw.major ? majorTickFontColor : tickFontColor;
                                context.textBaseline = itemToDraw.textBaseline;
                                context.textAlign = itemToDraw.textAlign;
                                var label = itemToDraw.label;
                                if (helpers.isArray(label)) {
                                    var lineCount = label.length;
                                    var lineHeight = tickFont.size * 1.5;
                                    var y = me.isHorizontal() ? 0 : -lineHeight * (lineCount - 1) / 2;
                                    for (var i = 0; i < lineCount; ++i) {
                                        context.fillText('' + label[i], 0, y);
                                        y += lineHeight;
                                    }
                                } else {
                                    context.fillText(label, 0, 0);
                                }
                                context.restore();
                            }
                        });
                        if (scaleLabel.display) {
                            var scaleLabelX;
                            var scaleLabelY;
                            var rotation = 0;
                            var halfLineHeight = parseLineHeight(scaleLabel) / 2;
                            if (isHorizontal) {
                                scaleLabelX = me.left + ((me.right - me.left) / 2);
                                scaleLabelY = options.position === 'bottom' ? me.bottom - halfLineHeight - scaleLabelPadding.bottom : me.top + halfLineHeight + scaleLabelPadding.top;
                            } else {
                                var isLeft = options.position === 'left';
                                scaleLabelX = isLeft ? me.left + halfLineHeight + scaleLabelPadding.top : me.right - halfLineHeight - scaleLabelPadding.top;
                                scaleLabelY = me.top + ((me.bottom - me.top) / 2);
                                rotation = isLeft ? -0.5 * Math.PI : 0.5 * Math.PI;
                            }
                            context.save();
                            context.translate(scaleLabelX, scaleLabelY);
                            context.rotate(rotation);
                            context.textAlign = 'center';
                            context.textBaseline = 'middle';
                            context.fillStyle = scaleLabelFontColor;
                            context.font = scaleLabelFont.font;
                            context.fillText(scaleLabel.labelString, 0, 0);
                            context.restore();
                        }
                        if (gridLines.drawBorder) {
                            context.lineWidth = helpers.valueAtIndexOrDefault(gridLines.lineWidth, 0);
                            context.strokeStyle = helpers.valueAtIndexOrDefault(gridLines.color, 0);
                            var x1 = me.left;
                            var x2 = me.right + axisWidth;
                            var y1 = me.top;
                            var y2 = me.bottom + axisWidth;
                            var aliasPixel = helpers.aliasPixel(context.lineWidth);
                            if (isHorizontal) {
                                y1 = y2 = options.position === 'top' ? me.bottom : me.top;
                                y1 += aliasPixel;
                                y2 += aliasPixel;
                            } else {
                                x1 = x2 = options.position === 'left' ? me.right : me.left;
                                x1 += aliasPixel;
                                x2 += aliasPixel;
                            }
                            context.beginPath();
                            context.moveTo(x1, y1);
                            context.lineTo(x2, y2);
                            context.stroke();
                        }
                    }
                });
            };
        }, {
            "25": 25,
            "26": 26,
            "34": 34,
            "45": 45
        }],
        33: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var helpers = require(45);
            var layouts = require(30);
            module.exports = function(Chart) {
                Chart.scaleService = {
                    constructors: {},
                    defaults: {},
                    registerScaleType: function(type, scaleConstructor, scaleDefaults) {
                        this.constructors[type] = scaleConstructor;
                        this.defaults[type] = helpers.clone(scaleDefaults);
                    },
                    getScaleConstructor: function(type) {
                        return this.constructors.hasOwnProperty(type) ? this.constructors[type] : undefined;
                    },
                    getScaleDefaults: function(type) {
                        return this.defaults.hasOwnProperty(type) ? helpers.merge({}, [defaults.scale, this.defaults[type]]) : {};
                    },
                    updateScaleDefaults: function(type, additions) {
                        var me = this;
                        if (me.defaults.hasOwnProperty(type)) {
                            me.defaults[type] = helpers.extend(me.defaults[type], additions);
                        }
                    },
                    addScalesToLayout: function(chart) {
                        helpers.each(chart.scales, function(scale) {
                            scale.fullWidth = scale.options.fullWidth;
                            scale.position = scale.options.position;
                            scale.weight = scale.options.weight;
                            layouts.addBox(chart, scale);
                        });
                    }
                };
            };
        }, {
            "25": 25,
            "30": 30,
            "45": 45
        }],
        34: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);
            module.exports = {
                formatters: {
                    values: function(value) {
                        return helpers.isArray(value) ? value : '' + value;
                    },
                    linear: function(tickValue, index, ticks) {
                        var delta = ticks.length > 3 ? ticks[2] - ticks[1] : ticks[1] - ticks[0];
                        if (Math.abs(delta) > 1) {
                            if (tickValue !== Math.floor(tickValue)) {
                                delta = tickValue - Math.floor(tickValue);
                            }
                        }
                        var logDelta = helpers.log10(Math.abs(delta));
                        var tickString = '';
                        if (tickValue !== 0) {
                            var numDecimal = -1 * Math.floor(logDelta);
                            numDecimal = Math.max(Math.min(numDecimal, 20), 0);
                            tickString = tickValue.toFixed(numDecimal);
                        } else {
                            tickString = '0';
                        }
                        return tickString;
                    },
                    logarithmic: function(tickValue, index, ticks) {
                        var remain = tickValue / (Math.pow(10, Math.floor(helpers.log10(tickValue))));
                        if (tickValue === 0) {
                            return '0';
                        } else if (remain === 1 || remain === 2 || remain === 5 || index === 0 || index === ticks.length - 1) {
                            return tickValue.toExponential();
                        }
                        return '';
                    }
                }
            };
        }, {
            "45": 45
        }],
        35: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            defaults._set('global', {
                tooltips: {
                    enabled: true,
                    custom: null,
                    mode: 'nearest',
                    position: 'average',
                    intersect: true,
                    backgroundColor: 'rgba(0,0,0,0.8)',
                    titleFontStyle: 'bold',
                    titleSpacing: 2,
                    titleMarginBottom: 6,
                    titleFontColor: '#fff',
                    titleAlign: 'left',
                    bodySpacing: 2,
                    bodyFontColor: '#fff',
                    bodyAlign: 'left',
                    footerFontStyle: 'bold',
                    footerSpacing: 2,
                    footerMarginTop: 6,
                    footerFontColor: '#fff',
                    footerAlign: 'left',
                    yPadding: 6,
                    xPadding: 6,
                    caretPadding: 2,
                    caretSize: 5,
                    cornerRadius: 6,
                    multiKeyBackground: '#fff',
                    displayColors: true,
                    borderColor: 'rgba(0,0,0,0)',
                    borderWidth: 0,
                    callbacks: {
                        beforeTitle: helpers.noop,
                        title: function(tooltipItems, data) {
                            var title = '';
                            var labels = data.labels;
                            var labelCount = labels ? labels.length : 0;
                            if (tooltipItems.length > 0) {
                                var item = tooltipItems[0];
                                if (item.xLabel) {
                                    title = item.xLabel;
                                } else if (labelCount > 0 && item.index < labelCount) {
                                    title = labels[item.index];
                                }
                            }
                            return title;
                        },
                        afterTitle: helpers.noop,
                        beforeBody: helpers.noop,
                        beforeLabel: helpers.noop,
                        label: function(tooltipItem, data) {
                            var label = data.datasets[tooltipItem.datasetIndex].label || '';
                            if (label) {
                                label += ': ';
                            }
                            label += tooltipItem.yLabel;
                            return label;
                        },
                        labelColor: function(tooltipItem, chart) {
                            var meta = chart.getDatasetMeta(tooltipItem.datasetIndex);
                            var activeElement = meta.data[tooltipItem.index];
                            var view = activeElement._view;
                            return {
                                borderColor: view.borderColor,
                                backgroundColor: view.backgroundColor
                            };
                        },
                        labelTextColor: function() {
                            return this._options.bodyFontColor;
                        },
                        afterLabel: helpers.noop,
                        afterBody: helpers.noop,
                        beforeFooter: helpers.noop,
                        footer: helpers.noop,
                        afterFooter: helpers.noop
                    }
                }
            });
            module.exports = function(Chart) {
                function mergeOpacity(colorString, opacity) {
                    var color = helpers.color(colorString);
                    return color.alpha(opacity * color.alpha()).rgbaString();
                }

                function pushOrConcat(base, toPush) {
                    if (toPush) {
                        if (helpers.isArray(toPush)) {
                            Array.prototype.push.apply(base, toPush);
                        } else {
                            base.push(toPush);
                        }
                    }
                    return base;
                }

                function createTooltipItem(element) {
                    var xScale = element._xScale;
                    var yScale = element._yScale || element._scale;
                    var index = element._index;
                    var datasetIndex = element._datasetIndex;
                    return {
                        xLabel: xScale ? xScale.getLabelForIndex(index, datasetIndex) : '',
                        yLabel: yScale ? yScale.getLabelForIndex(index, datasetIndex) : '',
                        index: index,
                        datasetIndex: datasetIndex,
                        x: element._model.x,
                        y: element._model.y
                    };
                }

                function getBaseModel(tooltipOpts) {
                    var globalDefaults = defaults.global;
                    var valueOrDefault = helpers.valueOrDefault;
                    return {
                        xPadding: tooltipOpts.xPadding,
                        yPadding: tooltipOpts.yPadding,
                        xAlign: tooltipOpts.xAlign,
                        yAlign: tooltipOpts.yAlign,
                        bodyFontColor: tooltipOpts.bodyFontColor,
                        _bodyFontFamily: valueOrDefault(tooltipOpts.bodyFontFamily, globalDefaults.defaultFontFamily),
                        _bodyFontStyle: valueOrDefault(tooltipOpts.bodyFontStyle, globalDefaults.defaultFontStyle),
                        _bodyAlign: tooltipOpts.bodyAlign,
                        bodyFontSize: valueOrDefault(tooltipOpts.bodyFontSize, globalDefaults.defaultFontSize),
                        bodySpacing: tooltipOpts.bodySpacing,
                        titleFontColor: tooltipOpts.titleFontColor,
                        _titleFontFamily: valueOrDefault(tooltipOpts.titleFontFamily, globalDefaults.defaultFontFamily),
                        _titleFontStyle: valueOrDefault(tooltipOpts.titleFontStyle, globalDefaults.defaultFontStyle),
                        titleFontSize: valueOrDefault(tooltipOpts.titleFontSize, globalDefaults.defaultFontSize),
                        _titleAlign: tooltipOpts.titleAlign,
                        titleSpacing: tooltipOpts.titleSpacing,
                        titleMarginBottom: tooltipOpts.titleMarginBottom,
                        footerFontColor: tooltipOpts.footerFontColor,
                        _footerFontFamily: valueOrDefault(tooltipOpts.footerFontFamily, globalDefaults.defaultFontFamily),
                        _footerFontStyle: valueOrDefault(tooltipOpts.footerFontStyle, globalDefaults.defaultFontStyle),
                        footerFontSize: valueOrDefault(tooltipOpts.footerFontSize, globalDefaults.defaultFontSize),
                        _footerAlign: tooltipOpts.footerAlign,
                        footerSpacing: tooltipOpts.footerSpacing,
                        footerMarginTop: tooltipOpts.footerMarginTop,
                        caretSize: tooltipOpts.caretSize,
                        cornerRadius: tooltipOpts.cornerRadius,
                        backgroundColor: tooltipOpts.backgroundColor,
                        opacity: 0,
                        legendColorBackground: tooltipOpts.multiKeyBackground,
                        displayColors: tooltipOpts.displayColors,
                        borderColor: tooltipOpts.borderColor,
                        borderWidth: tooltipOpts.borderWidth
                    };
                }

                function getTooltipSize(tooltip, model) {
                    var ctx = tooltip._chart.ctx;
                    var height = model.yPadding * 2;
                    var width = 0;
                    var body = model.body;
                    var combinedBodyLength = body.reduce(function(count, bodyItem) {
                        return count + bodyItem.before.length + bodyItem.lines.length + bodyItem.after.length;
                    }, 0);
                    combinedBodyLength += model.beforeBody.length + model.afterBody.length;
                    var titleLineCount = model.title.length;
                    var footerLineCount = model.footer.length;
                    var titleFontSize = model.titleFontSize;
                    var bodyFontSize = model.bodyFontSize;
                    var footerFontSize = model.footerFontSize;
                    height += titleLineCount * titleFontSize;
                    height += titleLineCount ? (titleLineCount - 1) * model.titleSpacing : 0;
                    height += titleLineCount ? model.titleMarginBottom : 0;
                    height += combinedBodyLength * bodyFontSize;
                    height += combinedBodyLength ? (combinedBodyLength - 1) * model.bodySpacing : 0;
                    height += footerLineCount ? model.footerMarginTop : 0;
                    height += footerLineCount * (footerFontSize);
                    height += footerLineCount ? (footerLineCount - 1) * model.footerSpacing : 0;
                    var widthPadding = 0;
                    var maxLineWidth = function(line) {
                        width = Math.max(width, ctx.measureText(line).width + widthPadding);
                    };
                    ctx.font = helpers.fontString(titleFontSize, model._titleFontStyle, model._titleFontFamily);
                    helpers.each(model.title, maxLineWidth);
                    ctx.font = helpers.fontString(bodyFontSize, model._bodyFontStyle, model._bodyFontFamily);
                    helpers.each(model.beforeBody.concat(model.afterBody), maxLineWidth);
                    widthPadding = model.displayColors ? (bodyFontSize + 2) : 0;
                    helpers.each(body, function(bodyItem) {
                        helpers.each(bodyItem.before, maxLineWidth);
                        helpers.each(bodyItem.lines, maxLineWidth);
                        helpers.each(bodyItem.after, maxLineWidth);
                    });
                    widthPadding = 0;
                    ctx.font = helpers.fontString(footerFontSize, model._footerFontStyle, model._footerFontFamily);
                    helpers.each(model.footer, maxLineWidth);
                    width += 2 * model.xPadding;
                    return {
                        width: width,
                        height: height
                    };
                }

                function determineAlignment(tooltip, size) {
                    var model = tooltip._model;
                    var chart = tooltip._chart;
                    var chartArea = tooltip._chart.chartArea;
                    var xAlign = 'center';
                    var yAlign = 'center';
                    if (model.y < size.height) {
                        yAlign = 'top';
                    } else if (model.y > (chart.height - size.height)) {
                        yAlign = 'bottom';
                    }
                    var lf, rf;
                    var olf, orf;
                    var yf;
                    var midX = (chartArea.left + chartArea.right) / 2;
                    var midY = (chartArea.top + chartArea.bottom) / 2;
                    if (yAlign === 'center') {
                        lf = function(x) {
                            return x <= midX;
                        };
                        rf = function(x) {
                            return x > midX;
                        };
                    } else {
                        lf = function(x) {
                            return x <= (size.width / 2);
                        };
                        rf = function(x) {
                            return x >= (chart.width - (size.width / 2));
                        };
                    }
                    olf = function(x) {
                        return x + size.width + model.caretSize + model.caretPadding > chart.width;
                    };
                    orf = function(x) {
                        return x - size.width - model.caretSize - model.caretPadding < 0;
                    };
                    yf = function(y) {
                        return y <= midY ? 'top' : 'bottom';
                    };
                    if (lf(model.x)) {
                        xAlign = 'left';
                        if (olf(model.x)) {
                            xAlign = 'center';
                            yAlign = yf(model.y);
                        }
                    } else if (rf(model.x)) {
                        xAlign = 'right';
                        if (orf(model.x)) {
                            xAlign = 'center';
                            yAlign = yf(model.y);
                        }
                    }
                    var opts = tooltip._options;
                    return {
                        xAlign: opts.xAlign ? opts.xAlign : xAlign,
                        yAlign: opts.yAlign ? opts.yAlign : yAlign
                    };
                }

                function getBackgroundPoint(vm, size, alignment, chart) {
                    var x = vm.x;
                    var y = vm.y;
                    var caretSize = vm.caretSize;
                    var caretPadding = vm.caretPadding;
                    var cornerRadius = vm.cornerRadius;
                    var xAlign = alignment.xAlign;
                    var yAlign = alignment.yAlign;
                    var paddingAndSize = caretSize + caretPadding;
                    var radiusAndPadding = cornerRadius + caretPadding;
                    if (xAlign === 'right') {
                        x -= size.width;
                    } else if (xAlign === 'center') {
                        x -= (size.width / 2);
                        if (x + size.width > chart.width) {
                            x = chart.width - size.width;
                        }
                        if (x < 0) {
                            x = 0;
                        }
                    }
                    if (yAlign === 'top') {
                        y += paddingAndSize;
                    } else if (yAlign === 'bottom') {
                        y -= size.height + paddingAndSize;
                    } else {
                        y -= (size.height / 2);
                    }
                    if (yAlign === 'center') {
                        if (xAlign === 'left') {
                            x += paddingAndSize;
                        } else if (xAlign === 'right') {
                            x -= paddingAndSize;
                        }
                    } else if (xAlign === 'left') {
                        x -= radiusAndPadding;
                    } else if (xAlign === 'right') {
                        x += radiusAndPadding;
                    }
                    return {
                        x: x,
                        y: y
                    };
                }
                Chart.Tooltip = Element.extend({
                    initialize: function() {
                        this._model = getBaseModel(this._options);
                        this._lastActive = [];
                    },
                    getTitle: function() {
                        var me = this;
                        var opts = me._options;
                        var callbacks = opts.callbacks;
                        var beforeTitle = callbacks.beforeTitle.apply(me, arguments);
                        var title = callbacks.title.apply(me, arguments);
                        var afterTitle = callbacks.afterTitle.apply(me, arguments);
                        var lines = [];
                        lines = pushOrConcat(lines, beforeTitle);
                        lines = pushOrConcat(lines, title);
                        lines = pushOrConcat(lines, afterTitle);
                        return lines;
                    },
                    getBeforeBody: function() {
                        var lines = this._options.callbacks.beforeBody.apply(this, arguments);
                        return helpers.isArray(lines) ? lines : lines !== undefined ? [lines] : [];
                    },
                    getBody: function(tooltipItems, data) {
                        var me = this;
                        var callbacks = me._options.callbacks;
                        var bodyItems = [];
                        helpers.each(tooltipItems, function(tooltipItem) {
                            var bodyItem = {
                                before: [],
                                lines: [],
                                after: []
                            };
                            pushOrConcat(bodyItem.before, callbacks.beforeLabel.call(me, tooltipItem, data));
                            pushOrConcat(bodyItem.lines, callbacks.label.call(me, tooltipItem, data));
                            pushOrConcat(bodyItem.after, callbacks.afterLabel.call(me, tooltipItem, data));
                            bodyItems.push(bodyItem);
                        });
                        return bodyItems;
                    },
                    getAfterBody: function() {
                        var lines = this._options.callbacks.afterBody.apply(this, arguments);
                        return helpers.isArray(lines) ? lines : lines !== undefined ? [lines] : [];
                    },
                    getFooter: function() {
                        var me = this;
                        var callbacks = me._options.callbacks;
                        var beforeFooter = callbacks.beforeFooter.apply(me, arguments);
                        var footer = callbacks.footer.apply(me, arguments);
                        var afterFooter = callbacks.afterFooter.apply(me, arguments);
                        var lines = [];
                        lines = pushOrConcat(lines, beforeFooter);
                        lines = pushOrConcat(lines, footer);
                        lines = pushOrConcat(lines, afterFooter);
                        return lines;
                    },
                    update: function(changed) {
                        var me = this;
                        var opts = me._options;
                        var existingModel = me._model;
                        var model = me._model = getBaseModel(opts);
                        var active = me._active;
                        var data = me._data;
                        var alignment = {
                            xAlign: existingModel.xAlign,
                            yAlign: existingModel.yAlign
                        };
                        var backgroundPoint = {
                            x: existingModel.x,
                            y: existingModel.y
                        };
                        var tooltipSize = {
                            width: existingModel.width,
                            height: existingModel.height
                        };
                        var tooltipPosition = {
                            x: existingModel.caretX,
                            y: existingModel.caretY
                        };
                        var i, len;
                        if (active.length) {
                            model.opacity = 1;
                            var labelColors = [];
                            var labelTextColors = [];
                            tooltipPosition = Chart.Tooltip.positioners[opts.position].call(me, active, me._eventPosition);
                            var tooltipItems = [];
                            for (i = 0, len = active.length; i < len; ++i) {
                                tooltipItems.push(createTooltipItem(active[i]));
                            }
                            if (opts.filter) {
                                tooltipItems = tooltipItems.filter(function(a) {
                                    return opts.filter(a, data);
                                });
                            }
                            if (opts.itemSort) {
                                tooltipItems = tooltipItems.sort(function(a, b) {
                                    return opts.itemSort(a, b, data);
                                });
                            }
                            helpers.each(tooltipItems, function(tooltipItem) {
                                labelColors.push(opts.callbacks.labelColor.call(me, tooltipItem, me._chart));
                                labelTextColors.push(opts.callbacks.labelTextColor.call(me, tooltipItem, me._chart));
                            });
                            model.title = me.getTitle(tooltipItems, data);
                            model.beforeBody = me.getBeforeBody(tooltipItems, data);
                            model.body = me.getBody(tooltipItems, data);
                            model.afterBody = me.getAfterBody(tooltipItems, data);
                            model.footer = me.getFooter(tooltipItems, data);
                            model.x = Math.round(tooltipPosition.x);
                            model.y = Math.round(tooltipPosition.y);
                            model.caretPadding = opts.caretPadding;
                            model.labelColors = labelColors;
                            model.labelTextColors = labelTextColors;
                            model.dataPoints = tooltipItems;
                            tooltipSize = getTooltipSize(this, model);
                            alignment = determineAlignment(this, tooltipSize);
                            backgroundPoint = getBackgroundPoint(model, tooltipSize, alignment, me._chart);
                        } else {
                            model.opacity = 0;
                        }
                        model.xAlign = alignment.xAlign;
                        model.yAlign = alignment.yAlign;
                        model.x = backgroundPoint.x;
                        model.y = backgroundPoint.y;
                        model.width = tooltipSize.width;
                        model.height = tooltipSize.height;
                        model.caretX = tooltipPosition.x;
                        model.caretY = tooltipPosition.y;
                        me._model = model;
                        if (changed && opts.custom) {
                            opts.custom.call(me, model);
                        }
                        return me;
                    },
                    drawCaret: function(tooltipPoint, size) {
                        var ctx = this._chart.ctx;
                        var vm = this._view;
                        var caretPosition = this.getCaretPosition(tooltipPoint, size, vm);
                        ctx.lineTo(caretPosition.x1, caretPosition.y1);
                        ctx.lineTo(caretPosition.x2, caretPosition.y2);
                        ctx.lineTo(caretPosition.x3, caretPosition.y3);
                    },
                    getCaretPosition: function(tooltipPoint, size, vm) {
                        var x1, x2, x3, y1, y2, y3;
                        var caretSize = vm.caretSize;
                        var cornerRadius = vm.cornerRadius;
                        var xAlign = vm.xAlign;
                        var yAlign = vm.yAlign;
                        var ptX = tooltipPoint.x;
                        var ptY = tooltipPoint.y;
                        var width = size.width;
                        var height = size.height;
                        if (yAlign === 'center') {
                            y2 = ptY + (height / 2);
                            if (xAlign === 'left') {
                                x1 = ptX;
                                x2 = x1 - caretSize;
                                x3 = x1;
                                y1 = y2 + caretSize;
                                y3 = y2 - caretSize;
                            } else {
                                x1 = ptX + width;
                                x2 = x1 + caretSize;
                                x3 = x1;
                                y1 = y2 - caretSize;
                                y3 = y2 + caretSize;
                            }
                        } else {
                            if (xAlign === 'left') {
                                x2 = ptX + cornerRadius + (caretSize);
                                x1 = x2 - caretSize;
                                x3 = x2 + caretSize;
                            } else if (xAlign === 'right') {
                                x2 = ptX + width - cornerRadius - caretSize;
                                x1 = x2 - caretSize;
                                x3 = x2 + caretSize;
                            } else {
                                x2 = vm.caretX;
                                x1 = x2 - caretSize;
                                x3 = x2 + caretSize;
                            }
                            if (yAlign === 'top') {
                                y1 = ptY;
                                y2 = y1 - caretSize;
                                y3 = y1;
                            } else {
                                y1 = ptY + height;
                                y2 = y1 + caretSize;
                                y3 = y1;
                                var tmp = x3;
                                x3 = x1;
                                x1 = tmp;
                            }
                        }
                        return {
                            x1: x1,
                            x2: x2,
                            x3: x3,
                            y1: y1,
                            y2: y2,
                            y3: y3
                        };
                    },
                    drawTitle: function(pt, vm, ctx, opacity) {
                        var title = vm.title;
                        if (title.length) {
                            ctx.textAlign = vm._titleAlign;
                            ctx.textBaseline = 'top';
                            var titleFontSize = vm.titleFontSize;
                            var titleSpacing = vm.titleSpacing;
                            ctx.fillStyle = mergeOpacity(vm.titleFontColor, opacity);
                            ctx.font = helpers.fontString(titleFontSize, vm._titleFontStyle, vm._titleFontFamily);
                            var i, len;
                            for (i = 0, len = title.length; i < len; ++i) {
                                ctx.fillText(title[i], pt.x, pt.y);
                                pt.y += titleFontSize + titleSpacing;
                                if (i + 1 === title.length) {
                                    pt.y += vm.titleMarginBottom - titleSpacing;
                                }
                            }
                        }
                    },
                    drawBody: function(pt, vm, ctx, opacity) {
                        var bodyFontSize = vm.bodyFontSize;
                        var bodySpacing = vm.bodySpacing;
                        var body = vm.body;
                        ctx.textAlign = vm._bodyAlign;
                        ctx.textBaseline = 'top';
                        ctx.font = helpers.fontString(bodyFontSize, vm._bodyFontStyle, vm._bodyFontFamily);
                        var xLinePadding = 0;
                        var fillLineOfText = function(line) {
                            ctx.fillText(line, pt.x + xLinePadding, pt.y);
                            pt.y += bodyFontSize + bodySpacing;
                        };
                        ctx.fillStyle = mergeOpacity(vm.bodyFontColor, opacity);
                        helpers.each(vm.beforeBody, fillLineOfText);
                        var drawColorBoxes = vm.displayColors;
                        xLinePadding = drawColorBoxes ? (bodyFontSize + 2) : 0;
                        helpers.each(body, function(bodyItem, i) {
                            var textColor = mergeOpacity(vm.labelTextColors[i], opacity);
                            ctx.fillStyle = textColor;
                            helpers.each(bodyItem.before, fillLineOfText);
                            helpers.each(bodyItem.lines, function(line) {
                                if (drawColorBoxes) {
                                    ctx.fillStyle = mergeOpacity(vm.legendColorBackground, opacity);
                                    ctx.fillRect(pt.x, pt.y, bodyFontSize, bodyFontSize);
                                    ctx.lineWidth = 1;
                                    ctx.strokeStyle = mergeOpacity(vm.labelColors[i].borderColor, opacity);
                                    ctx.strokeRect(pt.x, pt.y, bodyFontSize, bodyFontSize);
                                    ctx.fillStyle = mergeOpacity(vm.labelColors[i].backgroundColor, opacity);
                                    ctx.fillRect(pt.x + 1, pt.y + 1, bodyFontSize - 2, bodyFontSize - 2);
                                    ctx.fillStyle = textColor;
                                }
                                fillLineOfText(line);
                            });
                            helpers.each(bodyItem.after, fillLineOfText);
                        });
                        xLinePadding = 0;
                        helpers.each(vm.afterBody, fillLineOfText);
                        pt.y -= bodySpacing;
                    },
                    drawFooter: function(pt, vm, ctx, opacity) {
                        var footer = vm.footer;
                        if (footer.length) {
                            pt.y += vm.footerMarginTop;
                            ctx.textAlign = vm._footerAlign;
                            ctx.textBaseline = 'top';
                            ctx.fillStyle = mergeOpacity(vm.footerFontColor, opacity);
                            ctx.font = helpers.fontString(vm.footerFontSize, vm._footerFontStyle, vm._footerFontFamily);
                            helpers.each(footer, function(line) {
                                ctx.fillText(line, pt.x, pt.y);
                                pt.y += vm.footerFontSize + vm.footerSpacing;
                            });
                        }
                    },
                    drawBackground: function(pt, vm, ctx, tooltipSize, opacity) {
                        ctx.fillStyle = mergeOpacity(vm.backgroundColor, opacity);
                        ctx.strokeStyle = mergeOpacity(vm.borderColor, opacity);
                        ctx.lineWidth = vm.borderWidth;
                        var xAlign = vm.xAlign;
                        var yAlign = vm.yAlign;
                        var x = pt.x;
                        var y = pt.y;
                        var width = tooltipSize.width;
                        var height = tooltipSize.height;
                        var radius = vm.cornerRadius;
                        ctx.beginPath();
                        ctx.moveTo(x + radius, y);
                        if (yAlign === 'top') {
                            this.drawCaret(pt, tooltipSize);
                        }
                        ctx.lineTo(x + width - radius, y);
                        ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
                        if (yAlign === 'center' && xAlign === 'right') {
                            this.drawCaret(pt, tooltipSize);
                        }
                        ctx.lineTo(x + width, y + height - radius);
                        ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                        if (yAlign === 'bottom') {
                            this.drawCaret(pt, tooltipSize);
                        }
                        ctx.lineTo(x + radius, y + height);
                        ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
                        if (yAlign === 'center' && xAlign === 'left') {
                            this.drawCaret(pt, tooltipSize);
                        }
                        ctx.lineTo(x, y + radius);
                        ctx.quadraticCurveTo(x, y, x + radius, y);
                        ctx.closePath();
                        ctx.fill();
                        if (vm.borderWidth > 0) {
                            ctx.stroke();
                        }
                    },
                    draw: function() {
                        var ctx = this._chart.ctx;
                        var vm = this._view;
                        if (vm.opacity === 0) {
                            return;
                        }
                        var tooltipSize = {
                            width: vm.width,
                            height: vm.height
                        };
                        var pt = {
                            x: vm.x,
                            y: vm.y
                        };
                        var opacity = Math.abs(vm.opacity < 1e-3) ? 0 : vm.opacity;
                        var hasTooltipContent = vm.title.length || vm.beforeBody.length || vm.body.length || vm.afterBody.length || vm.footer.length;
                        if (this._options.enabled && hasTooltipContent) {
                            this.drawBackground(pt, vm, ctx, tooltipSize, opacity);
                            pt.x += vm.xPadding;
                            pt.y += vm.yPadding;
                            this.drawTitle(pt, vm, ctx, opacity);
                            this.drawBody(pt, vm, ctx, opacity);
                            this.drawFooter(pt, vm, ctx, opacity);
                        }
                    },
                    handleEvent: function(e) {
                        var me = this;
                        var options = me._options;
                        var changed = false;
                        me._lastActive = me._lastActive || [];
                        if (e.type === 'mouseout') {
                            me._active = [];
                        } else {
                            me._active = me._chart.getElementsAtEventForMode(e, options.mode, options);
                        }
                        changed = !helpers.arrayEquals(me._active, me._lastActive);
                        if (changed) {
                            me._lastActive = me._active;
                            if (options.enabled || options.custom) {
                                me._eventPosition = {
                                    x: e.x,
                                    y: e.y
                                };
                                me.update(true);
                                me.pivot();
                            }
                        }
                        return changed;
                    }
                });
                Chart.Tooltip.positioners = {
                    average: function(elements) {
                        if (!elements.length) {
                            return false;
                        }
                        var i, len;
                        var x = 0;
                        var y = 0;
                        var count = 0;
                        for (i = 0, len = elements.length; i < len; ++i) {
                            var el = elements[i];
                            if (el && el.hasValue()) {
                                var pos = el.tooltipPosition();
                                x += pos.x;
                                y += pos.y;
                                ++count;
                            }
                        }
                        return {
                            x: Math.round(x / count),
                            y: Math.round(y / count)
                        };
                    },
                    nearest: function(elements, eventPosition) {
                        var x = eventPosition.x;
                        var y = eventPosition.y;
                        var minDistance = Number.POSITIVE_INFINITY;
                        var i, len, nearestElement;
                        for (i = 0, len = elements.length; i < len; ++i) {
                            var el = elements[i];
                            if (el && el.hasValue()) {
                                var center = el.getCenterPoint();
                                var d = helpers.distanceBetweenPoints(eventPosition, center);
                                if (d < minDistance) {
                                    minDistance = d;
                                    nearestElement = el;
                                }
                            }
                        }
                        if (nearestElement) {
                            var tp = nearestElement.tooltipPosition();
                            x = tp.x;
                            y = tp.y;
                        }
                        return {
                            x: x,
                            y: y
                        };
                    }
                };
            };
        }, {
            "25": 25,
            "26": 26,
            "45": 45
        }],
        36: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            defaults._set('global', {
                elements: {
                    arc: {
                        backgroundColor: defaults.global.defaultColor,
                        borderColor: '#fff',
                        borderWidth: 2
                    }
                }
            });
            module.exports = Element.extend({
                inLabelRange: function(mouseX) {
                    var vm = this._view;
                    if (vm) {
                        return (Math.pow(mouseX - vm.x, 2) < Math.pow(vm.radius + vm.hoverRadius, 2));
                    }
                    return false;
                },
                inRange: function(chartX, chartY) {
                    var vm = this._view;
                    if (vm) {
                        var pointRelativePosition = helpers.getAngleFromPoint(vm, {
                            x: chartX,
                            y: chartY
                        });
                        var angle = pointRelativePosition.angle;
                        var distance = pointRelativePosition.distance;
                        var startAngle = vm.startAngle;
                        var endAngle = vm.endAngle;
                        while (endAngle < startAngle) {
                            endAngle += 2.0 * Math.PI;
                        }
                        while (angle > endAngle) {
                            angle -= 2.0 * Math.PI;
                        }
                        while (angle < startAngle) {
                            angle += 2.0 * Math.PI;
                        }
                        var betweenAngles = (angle >= startAngle && angle <= endAngle);
                        var withinRadius = (distance >= vm.innerRadius && distance <= vm.outerRadius);
                        return (betweenAngles && withinRadius);
                    }
                    return false;
                },
                getCenterPoint: function() {
                    var vm = this._view;
                    var halfAngle = (vm.startAngle + vm.endAngle) / 2;
                    var halfRadius = (vm.innerRadius + vm.outerRadius) / 2;
                    return {
                        x: vm.x + Math.cos(halfAngle) * halfRadius,
                        y: vm.y + Math.sin(halfAngle) * halfRadius
                    };
                },
                getArea: function() {
                    var vm = this._view;
                    return Math.PI * ((vm.endAngle - vm.startAngle) / (2 * Math.PI)) * (Math.pow(vm.outerRadius, 2) - Math.pow(vm.innerRadius, 2));
                },
                tooltipPosition: function() {
                    var vm = this._view;
                    var centreAngle = vm.startAngle + ((vm.endAngle - vm.startAngle) / 2);
                    var rangeFromCentre = (vm.outerRadius - vm.innerRadius) / 2 + vm.innerRadius;
                    return {
                        x: vm.x + (Math.cos(centreAngle) * rangeFromCentre),
                        y: vm.y + (Math.sin(centreAngle) * rangeFromCentre)
                    };
                },
                draw: function() {
                    var ctx = this._chart.ctx;
                    var vm = this._view;
                    var sA = vm.startAngle;
                    var eA = vm.endAngle;
                    ctx.beginPath();
                    ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
                    ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
                    ctx.closePath();
                    ctx.strokeStyle = vm.borderColor;
                    ctx.lineWidth = vm.borderWidth;
                    ctx.fillStyle = vm.backgroundColor;
                    ctx.fill();
                    ctx.lineJoin = 'bevel';
                    if (vm.borderWidth) {
                        ctx.stroke();
                    }
                }
            });
        }, {
            "25": 25,
            "26": 26,
            "45": 45
        }],
        37: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            var globalDefaults = defaults.global;
            defaults._set('global', {
                elements: {
                    line: {
                        tension: 0.4,
                        backgroundColor: globalDefaults.defaultColor,
                        borderWidth: 3,
                        borderColor: globalDefaults.defaultColor,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        capBezierPoints: true,
                        fill: true,
                    }
                }
            });
            module.exports = Element.extend({
                draw: function() {
                    var me = this;
                    var vm = me._view;
                    var ctx = me._chart.ctx;
                    var spanGaps = vm.spanGaps;
                    var points = me._children.slice();
                    var globalOptionLineElements = globalDefaults.elements.line;
                    var lastDrawnIndex = -1;
                    var index, current, previous, currentVM;
                    if (me._loop && points.length) {
                        points.push(points[0]);
                    }
                    ctx.save();
                    ctx.lineCap = vm.borderCapStyle || globalOptionLineElements.borderCapStyle;
                    if (ctx.setLineDash) {
                        ctx.setLineDash(vm.borderDash || globalOptionLineElements.borderDash);
                    }
                    ctx.lineDashOffset = vm.borderDashOffset || globalOptionLineElements.borderDashOffset;
                    ctx.lineJoin = vm.borderJoinStyle || globalOptionLineElements.borderJoinStyle;
                    ctx.lineWidth = vm.borderWidth || globalOptionLineElements.borderWidth;
                    ctx.strokeStyle = vm.borderColor || globalDefaults.defaultColor;
                    ctx.beginPath();
                    lastDrawnIndex = -1;
                    for (index = 0; index < points.length; ++index) {
                        current = points[index];
                        previous = helpers.previousItem(points, index);
                        currentVM = current._view;
                        if (index === 0) {
                            if (!currentVM.skip) {
                                ctx.moveTo(currentVM.x, currentVM.y);
                                lastDrawnIndex = index;
                            }
                        } else {
                            previous = lastDrawnIndex === -1 ? previous : points[lastDrawnIndex];
                            if (!currentVM.skip) {
                                if ((lastDrawnIndex !== (index - 1) && !spanGaps) || lastDrawnIndex === -1) {
                                    ctx.moveTo(currentVM.x, currentVM.y);
                                } else {
                                    helpers.canvas.lineTo(ctx, previous._view, current._view);
                                }
                                lastDrawnIndex = index;
                            }
                        }
                    }
                    ctx.stroke();
                    ctx.restore();
                }
            });
        }, {
            "25": 25,
            "26": 26,
            "45": 45
        }],
        38: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            var defaultColor = defaults.global.defaultColor;
            defaults._set('global', {
                elements: {
                    point: {
                        radius: 3,
                        pointStyle: 'circle',
                        backgroundColor: defaultColor,
                        borderColor: defaultColor,
                        borderWidth: 1,
                        hitRadius: 1,
                        hoverRadius: 4,
                        hoverBorderWidth: 1
                    }
                }
            });

            function xRange(mouseX) {
                var vm = this._view;
                return vm ? (Math.abs(mouseX - vm.x) < vm.radius + vm.hitRadius) : false;
            }

            function yRange(mouseY) {
                var vm = this._view;
                return vm ? (Math.abs(mouseY - vm.y) < vm.radius + vm.hitRadius) : false;
            }
            module.exports = Element.extend({
                inRange: function(mouseX, mouseY) {
                    var vm = this._view;
                    return vm ? ((Math.pow(mouseX - vm.x, 2) + Math.pow(mouseY - vm.y, 2)) < Math.pow(vm.hitRadius + vm.radius, 2)) : false;
                },
                inLabelRange: xRange,
                inXRange: xRange,
                inYRange: yRange,
                getCenterPoint: function() {
                    var vm = this._view;
                    return {
                        x: vm.x,
                        y: vm.y
                    };
                },
                getArea: function() {
                    return Math.PI * Math.pow(this._view.radius, 2);
                },
                tooltipPosition: function() {
                    var vm = this._view;
                    return {
                        x: vm.x,
                        y: vm.y,
                        padding: vm.radius + vm.borderWidth
                    };
                },
                draw: function(chartArea) {
                    var vm = this._view;
                    var model = this._model;
                    var ctx = this._chart.ctx;
                    var pointStyle = vm.pointStyle;
                    var radius = vm.radius;
                    var x = vm.x;
                    var y = vm.y;
                    var color = helpers.color;
                    var errMargin = 1.01;
                    var ratio = 0;
                    if (vm.skip) {
                        return;
                    }
                    ctx.strokeStyle = vm.borderColor || defaultColor;
                    ctx.lineWidth = helpers.valueOrDefault(vm.borderWidth, defaults.global.elements.point.borderWidth);
                    ctx.fillStyle = vm.backgroundColor || defaultColor;
                    if ((chartArea !== undefined) && ((model.x < chartArea.left) || (chartArea.right * errMargin < model.x) || (model.y < chartArea.top) || (chartArea.bottom * errMargin < model.y))) {
                        if (model.x < chartArea.left) {
                            ratio = (x - model.x) / (chartArea.left - model.x);
                        } else if (chartArea.right * errMargin < model.x) {
                            ratio = (model.x - x) / (model.x - chartArea.right);
                        } else if (model.y < chartArea.top) {
                            ratio = (y - model.y) / (chartArea.top - model.y);
                        } else if (chartArea.bottom * errMargin < model.y) {
                            ratio = (model.y - y) / (model.y - chartArea.bottom);
                        }
                        ratio = Math.round(ratio * 100) / 100;
                        ctx.strokeStyle = color(ctx.strokeStyle).alpha(ratio).rgbString();
                        ctx.fillStyle = color(ctx.fillStyle).alpha(ratio).rgbString();
                    }
                    helpers.canvas.drawPoint(ctx, pointStyle, radius, x, y);
                }
            });
        }, {
            "25": 25,
            "26": 26,
            "45": 45
        }],
        39: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            defaults._set('global', {
                elements: {
                    rectangle: {
                        backgroundColor: defaults.global.defaultColor,
                        borderColor: defaults.global.defaultColor,
                        borderSkipped: 'bottom',
                        borderWidth: 0
                    }
                }
            });

            function isVertical(bar) {
                return bar._view.width !== undefined;
            }

            function getBarBounds(bar) {
                var vm = bar._view;
                var x1, x2, y1, y2;
                if (isVertical(bar)) {
                    var halfWidth = vm.width / 2;
                    x1 = vm.x - halfWidth;
                    x2 = vm.x + halfWidth;
                    y1 = Math.min(vm.y, vm.base);
                    y2 = Math.max(vm.y, vm.base);
                } else {
                    var halfHeight = vm.height / 2;
                    x1 = Math.min(vm.x, vm.base);
                    x2 = Math.max(vm.x, vm.base);
                    y1 = vm.y - halfHeight;
                    y2 = vm.y + halfHeight;
                }
                return {
                    left: x1,
                    top: y1,
                    right: x2,
                    bottom: y2
                };
            }
            module.exports = Element.extend({
                draw: function() {
                    var ctx = this._chart.ctx;
                    var vm = this._view;
                    var left, right, top, bottom, signX, signY, borderSkipped;
                    var borderWidth = vm.borderWidth;
                    if (!vm.horizontal) {
                        left = vm.x - vm.width / 2;
                        right = vm.x + vm.width / 2;
                        top = vm.y;
                        bottom = vm.base;
                        signX = 1;
                        signY = bottom > top ? 1 : -1;
                        borderSkipped = vm.borderSkipped || 'bottom';
                    } else {
                        left = vm.base;
                        right = vm.x;
                        top = vm.y - vm.height / 2;
                        bottom = vm.y + vm.height / 2;
                        signX = right > left ? 1 : -1;
                        signY = 1;
                        borderSkipped = vm.borderSkipped || 'left';
                    }
                    if (borderWidth) {
                        var barSize = Math.min(Math.abs(left - right), Math.abs(top - bottom));
                        borderWidth = borderWidth > barSize ? barSize : borderWidth;
                        var halfStroke = borderWidth / 2;
                        var borderLeft = left + (borderSkipped !== 'left' ? halfStroke * signX : 0);
                        var borderRight = right + (borderSkipped !== 'right' ? -halfStroke * signX : 0);
                        var borderTop = top + (borderSkipped !== 'top' ? halfStroke * signY : 0);
                        var borderBottom = bottom + (borderSkipped !== 'bottom' ? -halfStroke * signY : 0);
                        if (borderLeft !== borderRight) {
                            top = borderTop;
                            bottom = borderBottom;
                        }
                        if (borderTop !== borderBottom) {
                            left = borderLeft;
                            right = borderRight;
                        }
                    }
                    ctx.beginPath();
                    ctx.fillStyle = vm.backgroundColor;
                    ctx.strokeStyle = vm.borderColor;
                    ctx.lineWidth = borderWidth;
                    var corners = [
                        [left, bottom],
                        [left, top],
                        [right, top],
                        [right, bottom]
                    ];
                    var borders = ['bottom', 'left', 'top', 'right'];
                    var startCorner = borders.indexOf(borderSkipped, 0);
                    if (startCorner === -1) {
                        startCorner = 0;
                    }

                    function cornerAt(index) {
                        return corners[(startCorner + index) % 4];
                    }
                    var corner = cornerAt(0);
                    ctx.moveTo(corner[0], corner[1]);
                    for (var i = 1; i < 4; i++) {
                        corner = cornerAt(i);
                        ctx.lineTo(corner[0], corner[1]);
                    }
                    ctx.fill();
                    if (borderWidth) {
                        ctx.stroke();
                    }
                },
                height: function() {
                    var vm = this._view;
                    return vm.base - vm.y;
                },
                inRange: function(mouseX, mouseY) {
                    var inRange = false;
                    if (this._view) {
                        var bounds = getBarBounds(this);
                        inRange = mouseX >= bounds.left && mouseX <= bounds.right && mouseY >= bounds.top && mouseY <= bounds.bottom;
                    }
                    return inRange;
                },
                inLabelRange: function(mouseX, mouseY) {
                    var me = this;
                    if (!me._view) {
                        return false;
                    }
                    var inRange = false;
                    var bounds = getBarBounds(me);
                    if (isVertical(me)) {
                        inRange = mouseX >= bounds.left && mouseX <= bounds.right;
                    } else {
                        inRange = mouseY >= bounds.top && mouseY <= bounds.bottom;
                    }
                    return inRange;
                },
                inXRange: function(mouseX) {
                    var bounds = getBarBounds(this);
                    return mouseX >= bounds.left && mouseX <= bounds.right;
                },
                inYRange: function(mouseY) {
                    var bounds = getBarBounds(this);
                    return mouseY >= bounds.top && mouseY <= bounds.bottom;
                },
                getCenterPoint: function() {
                    var vm = this._view;
                    var x, y;
                    if (isVertical(this)) {
                        x = vm.x;
                        y = (vm.y + vm.base) / 2;
                    } else {
                        x = (vm.x + vm.base) / 2;
                        y = vm.y;
                    }
                    return {
                        x: x,
                        y: y
                    };
                },
                getArea: function() {
                    var vm = this._view;
                    return vm.width * Math.abs(vm.y - vm.base);
                },
                tooltipPosition: function() {
                    var vm = this._view;
                    return {
                        x: vm.x,
                        y: vm.y
                    };
                }
            });
        }, {
            "25": 25,
            "26": 26
        }],
        40: [function(require, module, exports) {
            'use strict';
            module.exports = {};
            module.exports.Arc = require(36);
            module.exports.Line = require(37);
            module.exports.Point = require(38);
            module.exports.Rectangle = require(39);
        }, {
            "36": 36,
            "37": 37,
            "38": 38,
            "39": 39
        }],
        41: [function(require, module, exports) {
            'use strict';
            var helpers = require(42);
            var exports = module.exports = {
                clear: function(chart) {
                    chart.ctx.clearRect(0, 0, chart.width, chart.height);
                },
                roundedRect: function(ctx, x, y, width, height, radius) {
                    if (radius) {
                        var rx = Math.min(radius, width / 2);
                        var ry = Math.min(radius, height / 2);
                        ctx.moveTo(x + rx, y);
                        ctx.lineTo(x + width - rx, y);
                        ctx.quadraticCurveTo(x + width, y, x + width, y + ry);
                        ctx.lineTo(x + width, y + height - ry);
                        ctx.quadraticCurveTo(x + width, y + height, x + width - rx, y + height);
                        ctx.lineTo(x + rx, y + height);
                        ctx.quadraticCurveTo(x, y + height, x, y + height - ry);
                        ctx.lineTo(x, y + ry);
                        ctx.quadraticCurveTo(x, y, x + rx, y);
                    } else {
                        ctx.rect(x, y, width, height);
                    }
                },
                drawPoint: function(ctx, style, radius, x, y) {
                    var type, edgeLength, xOffset, yOffset, height, size;
                    if (style && typeof style === 'object') {
                        type = style.toString();
                        if (type === '[object HTMLImageElement]' || type === '[object HTMLCanvasElement]') {
                            ctx.drawImage(style, x - style.width / 2, y - style.height / 2, style.width, style.height);
                            return;
                        }
                    }
                    if (isNaN(radius) || radius <= 0) {
                        return;
                    }
                    switch (style) {
                        default: ctx.beginPath();ctx.arc(x, y, radius, 0, Math.PI * 2);ctx.closePath();ctx.fill();
                        break;
                        case 'triangle':
                                ctx.beginPath();edgeLength = 3 * radius / Math.sqrt(3);height = edgeLength * Math.sqrt(3) / 2;ctx.moveTo(x - edgeLength / 2, y + height / 3);ctx.lineTo(x + edgeLength / 2, y + height / 3);ctx.lineTo(x, y - 2 * height / 3);ctx.closePath();ctx.fill();
                            break;
                        case 'rect':
                                size = 1 / Math.SQRT2 * radius;ctx.beginPath();ctx.fillRect(x - size, y - size, 2 * size, 2 * size);ctx.strokeRect(x - size, y - size, 2 * size, 2 * size);
                            break;
                        case 'rectRounded':
                                var offset = radius / Math.SQRT2;
                            var leftX = x - offset;
                            var topY = y - offset;
                            var sideSize = Math.SQRT2 * radius;ctx.beginPath();this.roundedRect(ctx, leftX, topY, sideSize, sideSize, radius / 2);ctx.closePath();ctx.fill();
                            break;
                        case 'rectRot':
                                size = 1 / Math.SQRT2 * radius;ctx.beginPath();ctx.moveTo(x - size, y);ctx.lineTo(x, y + size);ctx.lineTo(x + size, y);ctx.lineTo(x, y - size);ctx.closePath();ctx.fill();
                            break;
                        case 'cross':
                                ctx.beginPath();ctx.moveTo(x, y + radius);ctx.lineTo(x, y - radius);ctx.moveTo(x - radius, y);ctx.lineTo(x + radius, y);ctx.closePath();
                            break;
                        case 'crossRot':
                                ctx.beginPath();xOffset = Math.cos(Math.PI / 4) * radius;yOffset = Math.sin(Math.PI / 4) * radius;ctx.moveTo(x - xOffset, y - yOffset);ctx.lineTo(x + xOffset, y + yOffset);ctx.moveTo(x - xOffset, y + yOffset);ctx.lineTo(x + xOffset, y - yOffset);ctx.closePath();
                            break;
                        case 'star':
                                ctx.beginPath();ctx.moveTo(x, y + radius);ctx.lineTo(x, y - radius);ctx.moveTo(x - radius, y);ctx.lineTo(x + radius, y);xOffset = Math.cos(Math.PI / 4) * radius;yOffset = Math.sin(Math.PI / 4) * radius;ctx.moveTo(x - xOffset, y - yOffset);ctx.lineTo(x + xOffset, y + yOffset);ctx.moveTo(x - xOffset, y + yOffset);ctx.lineTo(x + xOffset, y - yOffset);ctx.closePath();
                            break;
                        case 'line':
                                ctx.beginPath();ctx.moveTo(x - radius, y);ctx.lineTo(x + radius, y);ctx.closePath();
                            break;
                        case 'dash':
                                ctx.beginPath();ctx.moveTo(x, y);ctx.lineTo(x + radius, y);ctx.closePath();
                            break;
                    }
                    ctx.stroke();
                },
                clipArea: function(ctx, area) {
                    ctx.save();
                    ctx.beginPath();
                    ctx.rect(area.left, area.top, area.right - area.left, area.bottom - area.top);
                    ctx.clip();
                },
                unclipArea: function(ctx) {
                    ctx.restore();
                },
                lineTo: function(ctx, previous, target, flip) {
                    if (target.steppedLine) {
                        if ((target.steppedLine === 'after' && !flip) || (target.steppedLine !== 'after' && flip)) {
                            ctx.lineTo(previous.x, target.y);
                        } else {
                            ctx.lineTo(target.x, previous.y);
                        }
                        ctx.lineTo(target.x, target.y);
                        return;
                    }
                    if (!target.tension) {
                        ctx.lineTo(target.x, target.y);
                        return;
                    }
                    ctx.bezierCurveTo(flip ? previous.controlPointPreviousX : previous.controlPointNextX, flip ? previous.controlPointPreviousY : previous.controlPointNextY, flip ? target.controlPointNextX : target.controlPointPreviousX, flip ? target.controlPointNextY : target.controlPointPreviousY, target.x, target.y);
                }
            };
            helpers.clear = exports.clear;
            helpers.drawRoundedRectangle = function(ctx) {
                ctx.beginPath();
                exports.roundedRect.apply(exports, arguments);
                ctx.closePath();
            };
        }, {
            "42": 42
        }],
        42: [function(require, module, exports) {
            'use strict';
            var helpers = {
                noop: function() {},
                uid: (function() {
                    var id = 0;
                    return function() {
                        return id++;
                    };
                }()),
                isNullOrUndef: function(value) {
                    return value === null || typeof value === 'undefined';
                },
                isArray: Array.isArray ? Array.isArray : function(value) {
                    return Object.prototype.toString.call(value) === '[object Array]';
                },
                isObject: function(value) {
                    return value !== null && Object.prototype.toString.call(value) === '[object Object]';
                },
                valueOrDefault: function(value, defaultValue) {
                    return typeof value === 'undefined' ? defaultValue : value;
                },
                valueAtIndexOrDefault: function(value, index, defaultValue) {
                    return helpers.valueOrDefault(helpers.isArray(value) ? value[index] : value, defaultValue);
                },
                callback: function(fn, args, thisArg) {
                    if (fn && typeof fn.call === 'function') {
                        return fn.apply(thisArg, args);
                    }
                },
                each: function(loopable, fn, thisArg, reverse) {
                    var i, len, keys;
                    if (helpers.isArray(loopable)) {
                        len = loopable.length;
                        if (reverse) {
                            for (i = len - 1; i >= 0; i--) {
                                fn.call(thisArg, loopable[i], i);
                            }
                        } else {
                            for (i = 0; i < len; i++) {
                                fn.call(thisArg, loopable[i], i);
                            }
                        }
                    } else if (helpers.isObject(loopable)) {
                        keys = Object.keys(loopable);
                        len = keys.length;
                        for (i = 0; i < len; i++) {
                            fn.call(thisArg, loopable[keys[i]], keys[i]);
                        }
                    }
                },
                arrayEquals: function(a0, a1) {
                    var i, ilen, v0, v1;
                    if (!a0 || !a1 || a0.length !== a1.length) {
                        return false;
                    }
                    for (i = 0, ilen = a0.length; i < ilen; ++i) {
                        v0 = a0[i];
                        v1 = a1[i];
                        if (v0 instanceof Array && v1 instanceof Array) {
                            if (!helpers.arrayEquals(v0, v1)) {
                                return false;
                            }
                        } else if (v0 !== v1) {
                            return false;
                        }
                    }
                    return true;
                },
                clone: function(source) {
                    if (helpers.isArray(source)) {
                        return source.map(helpers.clone);
                    }
                    if (helpers.isObject(source)) {
                        var target = {};
                        var keys = Object.keys(source);
                        var klen = keys.length;
                        var k = 0;
                        for (; k < klen; ++k) {
                            target[keys[k]] = helpers.clone(source[keys[k]]);
                        }
                        return target;
                    }
                    return source;
                },
                _merger: function(key, target, source, options) {
                    var tval = target[key];
                    var sval = source[key];
                    if (helpers.isObject(tval) && helpers.isObject(sval)) {
                        helpers.merge(tval, sval, options);
                    } else {
                        target[key] = helpers.clone(sval);
                    }
                },
                _mergerIf: function(key, target, source) {
                    var tval = target[key];
                    var sval = source[key];
                    if (helpers.isObject(tval) && helpers.isObject(sval)) {
                        helpers.mergeIf(tval, sval);
                    } else if (!target.hasOwnProperty(key)) {
                        target[key] = helpers.clone(sval);
                    }
                },
                merge: function(target, source, options) {
                    var sources = helpers.isArray(source) ? source : [source];
                    var ilen = sources.length;
                    var merge, i, keys, klen, k;
                    if (!helpers.isObject(target)) {
                        return target;
                    }
                    options = options || {};
                    merge = options.merger || helpers._merger;
                    for (i = 0; i < ilen; ++i) {
                        source = sources[i];
                        if (!helpers.isObject(source)) {
                            continue;
                        }
                        keys = Object.keys(source);
                        for (k = 0, klen = keys.length; k < klen; ++k) {
                            merge(keys[k], target, source, options);
                        }
                    }
                    return target;
                },
                mergeIf: function(target, source) {
                    return helpers.merge(target, source, {
                        merger: helpers._mergerIf
                    });
                },
                extend: function(target) {
                    var setFn = function(value, key) {
                        target[key] = value;
                    };
                    for (var i = 1, ilen = arguments.length; i < ilen; ++i) {
                        helpers.each(arguments[i], setFn);
                    }
                    return target;
                },
                inherits: function(extensions) {
                    var me = this;
                    var ChartElement = (extensions && extensions.hasOwnProperty('constructor')) ? extensions.constructor : function() {
                        return me.apply(this, arguments);
                    };
                    var Surrogate = function() {
                        this.constructor = ChartElement;
                    };
                    Surrogate.prototype = me.prototype;
                    ChartElement.prototype = new Surrogate();
                    ChartElement.extend = helpers.inherits;
                    if (extensions) {
                        helpers.extend(ChartElement.prototype, extensions);
                    }
                    ChartElement.__super__ = me.prototype;
                    return ChartElement;
                }
            };
            module.exports = helpers;
            helpers.callCallback = helpers.callback;
            helpers.indexOf = function(array, item, fromIndex) {
                return Array.prototype.indexOf.call(array, item, fromIndex);
            };
            helpers.getValueOrDefault = helpers.valueOrDefault;
            helpers.getValueAtIndexOrDefault = helpers.valueAtIndexOrDefault;
        }, {}],
        43: [function(require, module, exports) {
            'use strict';
            var helpers = require(42);
            var effects = {
                linear: function(t) {
                    return t;
                },
                easeInQuad: function(t) {
                    return t * t;
                },
                easeOutQuad: function(t) {
                    return -t * (t - 2);
                },
                easeInOutQuad: function(t) {
                    if ((t /= 0.5) < 1) {
                        return 0.5 * t * t;
                    }
                    return -0.5 * ((--t) * (t - 2) - 1);
                },
                easeInCubic: function(t) {
                    return t * t * t;
                },
                easeOutCubic: function(t) {
                    return (t = t - 1) * t * t + 1;
                },
                easeInOutCubic: function(t) {
                    if ((t /= 0.5) < 1) {
                        return 0.5 * t * t * t;
                    }
                    return 0.5 * ((t -= 2) * t * t + 2);
                },
                easeInQuart: function(t) {
                    return t * t * t * t;
                },
                easeOutQuart: function(t) {
                    return -((t = t - 1) * t * t * t - 1);
                },
                easeInOutQuart: function(t) {
                    if ((t /= 0.5) < 1) {
                        return 0.5 * t * t * t * t;
                    }
                    return -0.5 * ((t -= 2) * t * t * t - 2);
                },
                easeInQuint: function(t) {
                    return t * t * t * t * t;
                },
                easeOutQuint: function(t) {
                    return (t = t - 1) * t * t * t * t + 1;
                },
                easeInOutQuint: function(t) {
                    if ((t /= 0.5) < 1) {
                        return 0.5 * t * t * t * t * t;
                    }
                    return 0.5 * ((t -= 2) * t * t * t * t + 2);
                },
                easeInSine: function(t) {
                    return -Math.cos(t * (Math.PI / 2)) + 1;
                },
                easeOutSine: function(t) {
                    return Math.sin(t * (Math.PI / 2));
                },
                easeInOutSine: function(t) {
                    return -0.5 * (Math.cos(Math.PI * t) - 1);
                },
                easeInExpo: function(t) {
                    return (t === 0) ? 0 : Math.pow(2, 10 * (t - 1));
                },
                easeOutExpo: function(t) {
                    return (t === 1) ? 1 : -Math.pow(2, -10 * t) + 1;
                },
                easeInOutExpo: function(t) {
                    if (t === 0) {
                        return 0;
                    }
                    if (t === 1) {
                        return 1;
                    }
                    if ((t /= 0.5) < 1) {
                        return 0.5 * Math.pow(2, 10 * (t - 1));
                    }
                    return 0.5 * (-Math.pow(2, -10 * --t) + 2);
                },
                easeInCirc: function(t) {
                    if (t >= 1) {
                        return t;
                    }
                    return -(Math.sqrt(1 - t * t) - 1);
                },
                easeOutCirc: function(t) {
                    return Math.sqrt(1 - (t = t - 1) * t);
                },
                easeInOutCirc: function(t) {
                    if ((t /= 0.5) < 1) {
                        return -0.5 * (Math.sqrt(1 - t * t) - 1);
                    }
                    return 0.5 * (Math.sqrt(1 - (t -= 2) * t) + 1);
                },
                easeInElastic: function(t) {
                    var s = 1.70158;
                    var p = 0;
                    var a = 1;
                    if (t === 0) {
                        return 0;
                    }
                    if (t === 1) {
                        return 1;
                    }
                    if (!p) {
                        p = 0.3;
                    }
                    if (a < 1) {
                        a = 1;
                        s = p / 4;
                    } else {
                        s = p / (2 * Math.PI) * Math.asin(1 / a);
                    }
                    return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - s) * (2 * Math.PI) / p));
                },
                easeOutElastic: function(t) {
                    var s = 1.70158;
                    var p = 0;
                    var a = 1;
                    if (t === 0) {
                        return 0;
                    }
                    if (t === 1) {
                        return 1;
                    }
                    if (!p) {
                        p = 0.3;
                    }
                    if (a < 1) {
                        a = 1;
                        s = p / 4;
                    } else {
                        s = p / (2 * Math.PI) * Math.asin(1 / a);
                    }
                    return a * Math.pow(2, -10 * t) * Math.sin((t - s) * (2 * Math.PI) / p) + 1;
                },
                easeInOutElastic: function(t) {
                    var s = 1.70158;
                    var p = 0;
                    var a = 1;
                    if (t === 0) {
                        return 0;
                    }
                    if ((t /= 0.5) === 2) {
                        return 1;
                    }
                    if (!p) {
                        p = 0.45;
                    }
                    if (a < 1) {
                        a = 1;
                        s = p / 4;
                    } else {
                        s = p / (2 * Math.PI) * Math.asin(1 / a);
                    }
                    if (t < 1) {
                        return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin((t - s) * (2 * Math.PI) / p));
                    }
                    return a * Math.pow(2, -10 * (t -= 1)) * Math.sin((t - s) * (2 * Math.PI) / p) * 0.5 + 1;
                },
                easeInBack: function(t) {
                    var s = 1.70158;
                    return t * t * ((s + 1) * t - s);
                },
                easeOutBack: function(t) {
                    var s = 1.70158;
                    return (t = t - 1) * t * ((s + 1) * t + s) + 1;
                },
                easeInOutBack: function(t) {
                    var s = 1.70158;
                    if ((t /= 0.5) < 1) {
                        return 0.5 * (t * t * (((s *= (1.525)) + 1) * t - s));
                    }
                    return 0.5 * ((t -= 2) * t * (((s *= (1.525)) + 1) * t + s) + 2);
                },
                easeInBounce: function(t) {
                    return 1 - effects.easeOutBounce(1 - t);
                },
                easeOutBounce: function(t) {
                    if (t < (1 / 2.75)) {
                        return 7.5625 * t * t;
                    }
                    if (t < (2 / 2.75)) {
                        return 7.5625 * (t -= (1.5 / 2.75)) * t + 0.75;
                    }
                    if (t < (2.5 / 2.75)) {
                        return 7.5625 * (t -= (2.25 / 2.75)) * t + 0.9375;
                    }
                    return 7.5625 * (t -= (2.625 / 2.75)) * t + 0.984375;
                },
                easeInOutBounce: function(t) {
                    if (t < 0.5) {
                        return effects.easeInBounce(t * 2) * 0.5;
                    }
                    return effects.easeOutBounce(t * 2 - 1) * 0.5 + 0.5;
                }
            };
            module.exports = {
                effects: effects
            };
            helpers.easingEffects = effects;
        }, {
            "42": 42
        }],
        44: [function(require, module, exports) {
            'use strict';
            var helpers = require(42);
            module.exports = {
                toLineHeight: function(value, size) {
                    var matches = ('' + value).match(/^(normal|(\d+(?:\.\d+)?)(px|em|%)?)$/);
                    if (!matches || matches[1] === 'normal') {
                        return size * 1.2;
                    }
                    value = +matches[2];
                    switch (matches[3]) {
                        case 'px':
                            return value;
                        case '%':
                            value /= 100;
                            break;
                        default:
                            break;
                    }
                    return size * value;
                },
                toPadding: function(value) {
                    var t, r, b, l;
                    if (helpers.isObject(value)) {
                        t = +value.top || 0;
                        r = +value.right || 0;
                        b = +value.bottom || 0;
                        l = +value.left || 0;
                    } else {
                        t = r = b = l = +value || 0;
                    }
                    return {
                        top: t,
                        right: r,
                        bottom: b,
                        left: l,
                        height: t + b,
                        width: l + r
                    };
                },
                resolve: function(inputs, context, index) {
                    var i, ilen, value;
                    for (i = 0, ilen = inputs.length; i < ilen; ++i) {
                        value = inputs[i];
                        if (value === undefined) {
                            continue;
                        }
                        if (context !== undefined && typeof value === 'function') {
                            value = value(context);
                        }
                        if (index !== undefined && helpers.isArray(value)) {
                            value = value[index];
                        }
                        if (value !== undefined) {
                            return value;
                        }
                    }
                }
            };
        }, {
            "42": 42
        }],
        45: [function(require, module, exports) {
            'use strict';
            module.exports = require(42);
            module.exports.easing = require(43);
            module.exports.canvas = require(41);
            module.exports.options = require(44);
        }, {
            "41": 41,
            "42": 42,
            "43": 43,
            "44": 44
        }],
        46: [function(require, module, exports) {
            module.exports = {
                acquireContext: function(item) {
                    if (item && item.canvas) {
                        item = item.canvas;
                    }
                    return item && item.getContext('2d') || null;
                }
            };
        }, {}],
        47: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);
            var EXPANDO_KEY = '$chartjs';
            var CSS_PREFIX = 'chartjs-';
            var CSS_RENDER_MONITOR = CSS_PREFIX + 'render-monitor';
            var CSS_RENDER_ANIMATION = CSS_PREFIX + 'render-animation';
            var ANIMATION_START_EVENTS = ['animationstart', 'webkitAnimationStart'];
            var EVENT_TYPES = {
                touchstart: 'mousedown',
                touchmove: 'mousemove',
                touchend: 'mouseup',
                pointerenter: 'mouseenter',
                pointerdown: 'mousedown',
                pointermove: 'mousemove',
                pointerup: 'mouseup',
                pointerleave: 'mouseout',
                pointerout: 'mouseout'
            };

            function readUsedSize(element, property) {
                var value = helpers.getStyle(element, property);
                var matches = value && value.match(/^(\d+)(\.\d+)?px$/);
                return matches ? Number(matches[1]) : undefined;
            }

            function initCanvas(canvas, config) {
                var style = canvas.style;
                var renderHeight = canvas.getAttribute('height');
                var renderWidth = canvas.getAttribute('width');
                canvas[EXPANDO_KEY] = {
                    initial: {
                        height: renderHeight,
                        width: renderWidth,
                        style: {
                            display: style.display,
                            height: style.height,
                            width: style.width
                        }
                    }
                };
                style.display = style.display || 'block';
                if (renderWidth === null || renderWidth === '') {
                    var displayWidth = readUsedSize(canvas, 'width');
                    if (displayWidth !== undefined) {
                        canvas.width = displayWidth;
                    }
                }
                if (renderHeight === null || renderHeight === '') {
                    if (canvas.style.height === '') {
                        canvas.height = canvas.width / (config.options.aspectRatio || 2);
                    } else {
                        var displayHeight = readUsedSize(canvas, 'height');
                        if (displayWidth !== undefined) {
                            canvas.height = displayHeight;
                        }
                    }
                }
                return canvas;
            }
            var supportsEventListenerOptions = (function() {
                var supports = false;
                try {
                    var options = Object.defineProperty({}, 'passive', {
                        get: function() {
                            supports = true;
                        }
                    });
                    window.addEventListener('e', null, options);
                } catch (e) {}
                return supports;
            }());
            var eventListenerOptions = supportsEventListenerOptions ? {
                passive: true
            } : false;

            function addEventListener(node, type, listener) {
                node.addEventListener(type, listener, eventListenerOptions);
            }

            function removeEventListener(node, type, listener) {
                node.removeEventListener(type, listener, eventListenerOptions);
            }

            function createEvent(type, chart, x, y, nativeEvent) {
                return {
                    type: type,
                    chart: chart,
                    native: nativeEvent || null,
                    x: x !== undefined ? x : null,
                    y: y !== undefined ? y : null,
                };
            }

            function fromNativeEvent(event, chart) {
                var type = EVENT_TYPES[event.type] || event.type;
                var pos = helpers.getRelativePosition(event, chart);
                return createEvent(type, chart, pos.x, pos.y, event);
            }

            function throttled(fn, thisArg) {
                var ticking = false;
                var args = [];
                return function() {
                    args = Array.prototype.slice.call(arguments);
                    thisArg = thisArg || this;
                    if (!ticking) {
                        ticking = true;
                        helpers.requestAnimFrame.call(window, function() {
                            ticking = false;
                            fn.apply(thisArg, args);
                        });
                    }
                };
            }

            function createResizer(handler) {
                var resizer = document.createElement('div');
                var cls = CSS_PREFIX + 'size-monitor';
                var maxSize = 1000000;
                var style = 'position:absolute;' + 'left:0;' + 'top:0;' + 'right:0;' + 'bottom:0;' + 'overflow:hidden;' + 'pointer-events:none;' + 'visibility:hidden;' + 'z-index:-1;';
                resizer.style.cssText = style;
                resizer.className = cls;
                resizer.innerHTML = '<div class="' + cls + '-expand" style="' + style + '">' + '<div style="' + 'position:absolute;' + 'width:' + maxSize + 'px;' + 'height:' + maxSize + 'px;' + 'left:0;' + 'top:0">' + '</div>' + '</div>' + '<div class="' + cls + '-shrink" style="' + style + '">' + '<div style="' + 'position:absolute;' + 'width:200%;' + 'height:200%;' + 'left:0; ' + 'top:0">' + '</div>' + '</div>';
                var expand = resizer.childNodes[0];
                var shrink = resizer.childNodes[1];
                resizer._reset = function() {
                    expand.scrollLeft = maxSize;
                    expand.scrollTop = maxSize;
                    shrink.scrollLeft = maxSize;
                    shrink.scrollTop = maxSize;
                };
                var onScroll = function() {
                    resizer._reset();
                    handler();
                };
                addEventListener(expand, 'scroll', onScroll.bind(expand, 'expand'));
                addEventListener(shrink, 'scroll', onScroll.bind(shrink, 'shrink'));
                return resizer;
            }

            function watchForRender(node, handler) {
                var expando = node[EXPANDO_KEY] || (node[EXPANDO_KEY] = {});
                var proxy = expando.renderProxy = function(e) {
                    if (e.animationName === CSS_RENDER_ANIMATION) {
                        handler();
                    }
                };
                helpers.each(ANIMATION_START_EVENTS, function(type) {
                    addEventListener(node, type, proxy);
                });
                expando.reflow = !!node.offsetParent;
                node.classList.add(CSS_RENDER_MONITOR);
            }

            function unwatchForRender(node) {
                var expando = node[EXPANDO_KEY] || {};
                var proxy = expando.renderProxy;
                if (proxy) {
                    helpers.each(ANIMATION_START_EVENTS, function(type) {
                        removeEventListener(node, type, proxy);
                    });
                    delete expando.renderProxy;
                }
                node.classList.remove(CSS_RENDER_MONITOR);
            }

            function addResizeListener(node, listener, chart) {
                var expando = node[EXPANDO_KEY] || (node[EXPANDO_KEY] = {});
                var resizer = expando.resizer = createResizer(throttled(function() {
                    if (expando.resizer) {
                        return listener(createEvent('resize', chart));
                    }
                }));
                watchForRender(node, function() {
                    if (expando.resizer) {
                        var container = node.parentNode;
                        if (container && container !== resizer.parentNode) {
                            container.insertBefore(resizer, container.firstChild);
                        }
                        resizer._reset();
                    }
                });
            }

            function removeResizeListener(node) {
                var expando = node[EXPANDO_KEY] || {};
                var resizer = expando.resizer;
                delete expando.resizer;
                unwatchForRender(node);
                if (resizer && resizer.parentNode) {
                    resizer.parentNode.removeChild(resizer);
                }
            }

            function injectCSS(platform, css) {
                var style = platform._style || document.createElement('style');
                if (!platform._style) {
                    platform._style = style;
                    css = '/* Chart.js */\n' + css;
                    style.setAttribute('type', 'text/css');
                    document.getElementsByTagName('head')[0].appendChild(style);
                }
                style.appendChild(document.createTextNode(css));
            }
            module.exports = {
                _enabled: typeof window !== 'undefined' && typeof document !== 'undefined',
                initialize: function() {
                    var keyframes = 'from{opacity:0.99}to{opacity:1}';
                    injectCSS(this, '@-webkit-keyframes ' + CSS_RENDER_ANIMATION + '{' + keyframes + '}' + '@keyframes ' + CSS_RENDER_ANIMATION + '{' + keyframes + '}' + '.' + CSS_RENDER_MONITOR + '{' + '-webkit-animation:' + CSS_RENDER_ANIMATION + ' 0.001s;' + 'animation:' + CSS_RENDER_ANIMATION + ' 0.001s;' + '}');
                },
                acquireContext: function(item, config) {
                    if (typeof item === 'string') {
                        item = document.getElementById(item);
                    } else if (item.length) {
                        item = item[0];
                    }
                    if (item && item.canvas) {
                        item = item.canvas;
                    }
                    var context = item && item.getContext && item.getContext('2d');
                    if (context && context.canvas === item) {
                        initCanvas(item, config);
                        return context;
                    }
                    return null;
                },
                releaseContext: function(context) {
                    var canvas = context.canvas;
                    if (!canvas[EXPANDO_KEY]) {
                        return;
                    }
                    var initial = canvas[EXPANDO_KEY].initial;
                    ['height', 'width'].forEach(function(prop) {
                        var value = initial[prop];
                        if (helpers.isNullOrUndef(value)) {
                            canvas.removeAttribute(prop);
                        } else {
                            canvas.setAttribute(prop, value);
                        }
                    });
                    helpers.each(initial.style || {}, function(value, key) {
                        canvas.style[key] = value;
                    });
                    canvas.width = canvas.width;
                    delete canvas[EXPANDO_KEY];
                },
                addEventListener: function(chart, type, listener) {
                    var canvas = chart.canvas;
                    if (type === 'resize') {
                        addResizeListener(canvas, listener, chart);
                        return;
                    }
                    var expando = listener[EXPANDO_KEY] || (listener[EXPANDO_KEY] = {});
                    var proxies = expando.proxies || (expando.proxies = {});
                    var proxy = proxies[chart.id + '_' + type] = function(event) {
                        listener(fromNativeEvent(event, chart));
                    };
                    addEventListener(canvas, type, proxy);
                },
                removeEventListener: function(chart, type, listener) {
                    var canvas = chart.canvas;
                    if (type === 'resize') {
                        removeResizeListener(canvas, listener);
                        return;
                    }
                    var expando = listener[EXPANDO_KEY] || {};
                    var proxies = expando.proxies || {};
                    var proxy = proxies[chart.id + '_' + type];
                    if (!proxy) {
                        return;
                    }
                    removeEventListener(canvas, type, proxy);
                }
            };
            helpers.addEvent = addEventListener;
            helpers.removeEvent = removeEventListener;
        }, {
            "45": 45
        }],
        48: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);
            var basic = require(46);
            var dom = require(47);
            var implementation = dom._enabled ? dom : basic;
            module.exports = helpers.extend({
                initialize: function() {},
                acquireContext: function() {},
                releaseContext: function() {},
                addEventListener: function() {},
                removeEventListener: function() {}
            }, implementation);
        }, {
            "45": 45,
            "46": 46,
            "47": 47
        }],
        49: [function(require, module, exports) {
            'use strict';
            module.exports = {};
            module.exports.filler = require(50);
            module.exports.legend = require(51);
            module.exports.title = require(52);
        }, {
            "50": 50,
            "51": 51,
            "52": 52
        }],
        50: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var elements = require(40);
            var helpers = require(45);
            defaults._set('global', {
                plugins: {
                    filler: {
                        propagate: true
                    }
                }
            });
            var mappers = {
                dataset: function(source) {
                    var index = source.fill;
                    var chart = source.chart;
                    var meta = chart.getDatasetMeta(index);
                    var visible = meta && chart.isDatasetVisible(index);
                    var points = (visible && meta.dataset._children) || [];
                    var length = points.length || 0;
                    return !length ? null : function(point, i) {
                        return (i < length && points[i]._view) || null;
                    };
                },
                boundary: function(source) {
                    var boundary = source.boundary;
                    var x = boundary ? boundary.x : null;
                    var y = boundary ? boundary.y : null;
                    return function(point) {
                        return {
                            x: x === null ? point.x : x,
                            y: y === null ? point.y : y,
                        };
                    };
                }
            };

            function decodeFill(el, index, count) {
                var model = el._model || {};
                var fill = model.fill;
                var target;
                if (fill === undefined) {
                    fill = !!model.backgroundColor;
                }
                if (fill === false || fill === null) {
                    return false;
                }
                if (fill === true) {
                    return 'origin';
                }
                target = parseFloat(fill, 10);
                if (isFinite(target) && Math.floor(target) === target) {
                    if (fill[0] === '-' || fill[0] === '+') {
                        target = index + target;
                    }
                    if (target === index || target < 0 || target >= count) {
                        return false;
                    }
                    return target;
                }
                switch (fill) {
                    case 'bottom':
                        return 'start';
                    case 'top':
                        return 'end';
                    case 'zero':
                        return 'origin';
                    case 'origin':
                    case 'start':
                    case 'end':
                        return fill;
                    default:
                        return false;
                }
            }

            function computeBoundary(source) {
                var model = source.el._model || {};
                var scale = source.el._scale || {};
                var fill = source.fill;
                var target = null;
                var horizontal;
                if (isFinite(fill)) {
                    return null;
                }
                if (fill === 'start') {
                    target = model.scaleBottom === undefined ? scale.bottom : model.scaleBottom;
                } else if (fill === 'end') {
                    target = model.scaleTop === undefined ? scale.top : model.scaleTop;
                } else if (model.scaleZero !== undefined) {
                    target = model.scaleZero;
                } else if (scale.getBasePosition) {
                    target = scale.getBasePosition();
                } else if (scale.getBasePixel) {
                    target = scale.getBasePixel();
                }
                if (target !== undefined && target !== null) {
                    if (target.x !== undefined && target.y !== undefined) {
                        return target;
                    }
                    if (typeof target === 'number' && isFinite(target)) {
                        horizontal = scale.isHorizontal();
                        return {
                            x: horizontal ? target : null,
                            y: horizontal ? null : target
                        };
                    }
                }
                return null;
            }

            function resolveTarget(sources, index, propagate) {
                var source = sources[index];
                var fill = source.fill;
                var visited = [index];
                var target;
                if (!propagate) {
                    return fill;
                }
                while (fill !== false && visited.indexOf(fill) === -1) {
                    if (!isFinite(fill)) {
                        return fill;
                    }
                    target = sources[fill];
                    if (!target) {
                        return false;
                    }
                    if (target.visible) {
                        return fill;
                    }
                    visited.push(fill);
                    fill = target.fill;
                }
                return false;
            }

            function createMapper(source) {
                var fill = source.fill;
                var type = 'dataset';
                if (fill === false) {
                    return null;
                }
                if (!isFinite(fill)) {
                    type = 'boundary';
                }
                return mappers[type](source);
            }

            function isDrawable(point) {
                return point && !point.skip;
            }

            function drawArea(ctx, curve0, curve1, len0, len1) {
                var i;
                if (!len0 || !len1) {
                    return;
                }
                ctx.moveTo(curve0[0].x, curve0[0].y);
                for (i = 1; i < len0; ++i) {
                    helpers.canvas.lineTo(ctx, curve0[i - 1], curve0[i]);
                }
                ctx.lineTo(curve1[len1 - 1].x, curve1[len1 - 1].y);
                for (i = len1 - 1; i > 0; --i) {
                    helpers.canvas.lineTo(ctx, curve1[i], curve1[i - 1], true);
                }
            }

            function doFill(ctx, points, mapper, view, color, loop) {
                var count = points.length;
                var span = view.spanGaps;
                var curve0 = [];
                var curve1 = [];
                var len0 = 0;
                var len1 = 0;
                var i, ilen, index, p0, p1, d0, d1;
                ctx.beginPath();
                for (i = 0, ilen = (count + !!loop); i < ilen; ++i) {
                    index = i % count;
                    p0 = points[index]._view;
                    p1 = mapper(p0, index, view);
                    d0 = isDrawable(p0);
                    d1 = isDrawable(p1);
                    if (d0 && d1) {
                        len0 = curve0.push(p0);
                        len1 = curve1.push(p1);
                    } else if (len0 && len1) {
                        if (!span) {
                            drawArea(ctx, curve0, curve1, len0, len1);
                            len0 = len1 = 0;
                            curve0 = [];
                            curve1 = [];
                        } else {
                            if (d0) {
                                curve0.push(p0);
                            }
                            if (d1) {
                                curve1.push(p1);
                            }
                        }
                    }
                }
                drawArea(ctx, curve0, curve1, len0, len1);
                ctx.closePath();
                ctx.fillStyle = color;
                ctx.fill();
            }
            module.exports = {
                id: 'filler',
                afterDatasetsUpdate: function(chart, options) {
                    var count = (chart.data.datasets || []).length;
                    var propagate = options.propagate;
                    var sources = [];
                    var meta, i, el, source;
                    for (i = 0; i < count; ++i) {
                        meta = chart.getDatasetMeta(i);
                        el = meta.dataset;
                        source = null;
                        if (el && el._model && el instanceof elements.Line) {
                            source = {
                                visible: chart.isDatasetVisible(i),
                                fill: decodeFill(el, i, count),
                                chart: chart,
                                el: el
                            };
                        }
                        meta.$filler = source;
                        sources.push(source);
                    }
                    for (i = 0; i < count; ++i) {
                        source = sources[i];
                        if (!source) {
                            continue;
                        }
                        source.fill = resolveTarget(sources, i, propagate);
                        source.boundary = computeBoundary(source);
                        source.mapper = createMapper(source);
                    }
                },
                beforeDatasetDraw: function(chart, args) {
                    var meta = args.meta.$filler;
                    if (!meta) {
                        return;
                    }
                    var ctx = chart.ctx;
                    var el = meta.el;
                    var view = el._view;
                    var points = el._children || [];
                    var mapper = meta.mapper;
                    var color = view.backgroundColor || defaults.global.defaultColor;
                    if (mapper && color && points.length) {
                        helpers.canvas.clipArea(ctx, chart.chartArea);
                        doFill(ctx, points, mapper, view, color, el._loop);
                        helpers.canvas.unclipArea(ctx);
                    }
                }
            };
        }, {
            "25": 25,
            "40": 40,
            "45": 45
        }],
        51: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            var layouts = require(30);
            var noop = helpers.noop;
            defaults._set('global', {
                legend: {
                    display: true,
                    position: 'top',
                    fullWidth: true,
                    reverse: false,
                    weight: 1000,
                    onClick: function(e, legendItem) {
                        var index = legendItem.datasetIndex;
                        var ci = this.chart;
                        var meta = ci.getDatasetMeta(index);
                        meta.hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null;
                        ci.update();
                    },
                    onHover: null,
                    labels: {
                        boxWidth: 40,
                        padding: 10,
                        generateLabels: function(chart) {
                            var data = chart.data;
                            return helpers.isArray(data.datasets) ? data.datasets.map(function(dataset, i) {
                                return {
                                    text: dataset.label,
                                    fillStyle: (!helpers.isArray(dataset.backgroundColor) ? dataset.backgroundColor : dataset.backgroundColor[0]),
                                    hidden: !chart.isDatasetVisible(i),
                                    lineCap: dataset.borderCapStyle,
                                    lineDash: dataset.borderDash,
                                    lineDashOffset: dataset.borderDashOffset,
                                    lineJoin: dataset.borderJoinStyle,
                                    lineWidth: dataset.borderWidth,
                                    strokeStyle: dataset.borderColor,
                                    pointStyle: dataset.pointStyle,
                                    datasetIndex: i
                                };
                            }, this) : [];
                        }
                    }
                },
                legendCallback: function(chart) {
                    var text = [];
                    text.push('<ul class="' + chart.id + '-legend">');
                    for (var i = 0; i < chart.data.datasets.length; i++) {
                        text.push('<li><span style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
                        if (chart.data.datasets[i].label) {
                            text.push(chart.data.datasets[i].label);
                        }
                        text.push('</li>');
                    }
                    text.push('</ul>');
                    return text.join('');
                }
            });

            function getBoxWidth(labelOpts, fontSize) {
                return labelOpts.usePointStyle ? fontSize * Math.SQRT2 : labelOpts.boxWidth;
            }
            var Legend = Element.extend({
                initialize: function(config) {
                    helpers.extend(this, config);
                    this.legendHitBoxes = [];
                    this.doughnutMode = false;
                },
                beforeUpdate: noop,
                update: function(maxWidth, maxHeight, margins) {
                    var me = this;
                    me.beforeUpdate();
                    me.maxWidth = maxWidth;
                    me.maxHeight = maxHeight;
                    me.margins = margins;
                    me.beforeSetDimensions();
                    me.setDimensions();
                    me.afterSetDimensions();
                    me.beforeBuildLabels();
                    me.buildLabels();
                    me.afterBuildLabels();
                    me.beforeFit();
                    me.fit();
                    me.afterFit();
                    me.afterUpdate();
                    return me.minSize;
                },
                afterUpdate: noop,
                beforeSetDimensions: noop,
                setDimensions: function() {
                    var me = this;
                    if (me.isHorizontal()) {
                        me.width = me.maxWidth;
                        me.left = 0;
                        me.right = me.width;
                    } else {
                        me.height = me.maxHeight;
                        me.top = 0;
                        me.bottom = me.height;
                    }
                    me.paddingLeft = 0;
                    me.paddingTop = 0;
                    me.paddingRight = 0;
                    me.paddingBottom = 0;
                    me.minSize = {
                        width: 0,
                        height: 0
                    };
                },
                afterSetDimensions: noop,
                beforeBuildLabels: noop,
                buildLabels: function() {
                    var me = this;
                    var labelOpts = me.options.labels || {};
                    var legendItems = helpers.callback(labelOpts.generateLabels, [me.chart], me) || [];
                    if (labelOpts.filter) {
                        legendItems = legendItems.filter(function(item) {
                            return labelOpts.filter(item, me.chart.data);
                        });
                    }
                    if (me.options.reverse) {
                        legendItems.reverse();
                    }
                    me.legendItems = legendItems;
                },
                afterBuildLabels: noop,
                beforeFit: noop,
                fit: function() {
                    var me = this;
                    var opts = me.options;
                    var labelOpts = opts.labels;
                    var display = opts.display;
                    var ctx = me.ctx;
                    var globalDefault = defaults.global;
                    var valueOrDefault = helpers.valueOrDefault;
                    var fontSize = valueOrDefault(labelOpts.fontSize, globalDefault.defaultFontSize);
                    var fontStyle = valueOrDefault(labelOpts.fontStyle, globalDefault.defaultFontStyle);
                    var fontFamily = valueOrDefault(labelOpts.fontFamily, globalDefault.defaultFontFamily);
                    var labelFont = helpers.fontString(fontSize, fontStyle, fontFamily);
                    var hitboxes = me.legendHitBoxes = [];
                    var minSize = me.minSize;
                    var isHorizontal = me.isHorizontal();
                    if (isHorizontal) {
                        minSize.width = me.maxWidth;
                        minSize.height = display ? 10 : 0;
                    } else {
                        minSize.width = display ? 10 : 0;
                        minSize.height = me.maxHeight;
                    }
                    if (display) {
                        ctx.font = labelFont;
                        if (isHorizontal) {
                            var lineWidths = me.lineWidths = [0];
                            var totalHeight = me.legendItems.length ? fontSize + (labelOpts.padding) : 0;
                            ctx.textAlign = 'left';
                            ctx.textBaseline = 'top';
                            helpers.each(me.legendItems, function(legendItem, i) {
                                var boxWidth = getBoxWidth(labelOpts, fontSize);
                                var width = boxWidth + (fontSize / 2) + ctx.measureText(legendItem.text).width;
                                if (lineWidths[lineWidths.length - 1] + width + labelOpts.padding >= me.width) {
                                    totalHeight += fontSize + (labelOpts.padding);
                                    lineWidths[lineWidths.length] = me.left;
                                }
                                hitboxes[i] = {
                                    left: 0,
                                    top: 0,
                                    width: width,
                                    height: fontSize
                                };
                                lineWidths[lineWidths.length - 1] += width + labelOpts.padding;
                            });
                            minSize.height += totalHeight;
                        } else {
                            var vPadding = labelOpts.padding;
                            var columnWidths = me.columnWidths = [];
                            var totalWidth = labelOpts.padding;
                            var currentColWidth = 0;
                            var currentColHeight = 0;
                            var itemHeight = fontSize + vPadding;
                            helpers.each(me.legendItems, function(legendItem, i) {
                                var boxWidth = getBoxWidth(labelOpts, fontSize);
                                var itemWidth = boxWidth + (fontSize / 2) + ctx.measureText(legendItem.text).width;
                                if (currentColHeight + itemHeight > minSize.height) {
                                    totalWidth += currentColWidth + labelOpts.padding;
                                    columnWidths.push(currentColWidth);
                                    currentColWidth = 0;
                                    currentColHeight = 0;
                                }
                                currentColWidth = Math.max(currentColWidth, itemWidth);
                                currentColHeight += itemHeight;
                                hitboxes[i] = {
                                    left: 0,
                                    top: 0,
                                    width: itemWidth,
                                    height: fontSize
                                };
                            });
                            totalWidth += currentColWidth;
                            columnWidths.push(currentColWidth);
                            minSize.width += totalWidth;
                        }
                    }
                    me.width = minSize.width;
                    me.height = minSize.height;
                },
                afterFit: noop,
                isHorizontal: function() {
                    return this.options.position === 'top' || this.options.position === 'bottom';
                },
                draw: function() {
                    var me = this;
                    var opts = me.options;
                    var labelOpts = opts.labels;
                    var globalDefault = defaults.global;
                    var lineDefault = globalDefault.elements.line;
                    var legendWidth = me.width;
                    var lineWidths = me.lineWidths;
                    if (opts.display) {
                        var ctx = me.ctx;
                        var valueOrDefault = helpers.valueOrDefault;
                        var fontColor = valueOrDefault(labelOpts.fontColor, globalDefault.defaultFontColor);
                        var fontSize = valueOrDefault(labelOpts.fontSize, globalDefault.defaultFontSize);
                        var fontStyle = valueOrDefault(labelOpts.fontStyle, globalDefault.defaultFontStyle);
                        var fontFamily = valueOrDefault(labelOpts.fontFamily, globalDefault.defaultFontFamily);
                        var labelFont = helpers.fontString(fontSize, fontStyle, fontFamily);
                        var cursor;
                        ctx.textAlign = 'left';
                        ctx.textBaseline = 'middle';
                        ctx.lineWidth = 0.5;
                        ctx.strokeStyle = fontColor;
                        ctx.fillStyle = fontColor;
                        ctx.font = labelFont;
                        var boxWidth = getBoxWidth(labelOpts, fontSize);
                        var hitboxes = me.legendHitBoxes;
                        var drawLegendBox = function(x, y, legendItem) {
                            if (isNaN(boxWidth) || boxWidth <= 0) {
                                return;
                            }
                            ctx.save();
                            ctx.fillStyle = valueOrDefault(legendItem.fillStyle, globalDefault.defaultColor);
                            ctx.lineCap = valueOrDefault(legendItem.lineCap, lineDefault.borderCapStyle);
                            ctx.lineDashOffset = valueOrDefault(legendItem.lineDashOffset, lineDefault.borderDashOffset);
                            ctx.lineJoin = valueOrDefault(legendItem.lineJoin, lineDefault.borderJoinStyle);
                            ctx.lineWidth = valueOrDefault(legendItem.lineWidth, lineDefault.borderWidth);
                            ctx.strokeStyle = valueOrDefault(legendItem.strokeStyle, globalDefault.defaultColor);
                            var isLineWidthZero = (valueOrDefault(legendItem.lineWidth, lineDefault.borderWidth) === 0);
                            if (ctx.setLineDash) {
                                ctx.setLineDash(valueOrDefault(legendItem.lineDash, lineDefault.borderDash));
                            }
                            if (opts.labels && opts.labels.usePointStyle) {
                                var radius = fontSize * Math.SQRT2 / 2;
                                var offSet = radius / Math.SQRT2;
                                var centerX = x + offSet;
                                var centerY = y + offSet;
                                helpers.canvas.drawPoint(ctx, legendItem.pointStyle, radius, centerX, centerY);
                            } else {
                                if (!isLineWidthZero) {
                                    ctx.strokeRect(x, y, boxWidth, fontSize);
                                }
                                ctx.fillRect(x, y, boxWidth, fontSize);
                            }
                            ctx.restore();
                        };
                        var fillText = function(x, y, legendItem, textWidth) {
                            var halfFontSize = fontSize / 2;
                            var xLeft = boxWidth + halfFontSize + x;
                            var yMiddle = y + halfFontSize;
                            ctx.fillText(legendItem.text, xLeft, yMiddle);
                            if (legendItem.hidden) {
                                ctx.beginPath();
                                ctx.lineWidth = 2;
                                ctx.moveTo(xLeft, yMiddle);
                                ctx.lineTo(xLeft + textWidth, yMiddle);
                                ctx.stroke();
                            }
                        };
                        var isHorizontal = me.isHorizontal();
                        if (isHorizontal) {
                            cursor = {
                                x: me.left + ((legendWidth - lineWidths[0]) / 2),
                                y: me.top + labelOpts.padding,
                                line: 0
                            };
                        } else {
                            cursor = {
                                x: me.left + labelOpts.padding,
                                y: me.top + labelOpts.padding,
                                line: 0
                            };
                        }
                        var itemHeight = fontSize + labelOpts.padding;
                        helpers.each(me.legendItems, function(legendItem, i) {
                            var textWidth = ctx.measureText(legendItem.text).width;
                            var width = boxWidth + (fontSize / 2) + textWidth;
                            var x = cursor.x;
                            var y = cursor.y;
                            if (isHorizontal) {
                                if (x + width >= legendWidth) {
                                    y = cursor.y += itemHeight;
                                    cursor.line++;
                                    x = cursor.x = me.left + ((legendWidth - lineWidths[cursor.line]) / 2);
                                }
                            } else if (y + itemHeight > me.bottom) {
                                x = cursor.x = x + me.columnWidths[cursor.line] + labelOpts.padding;
                                y = cursor.y = me.top + labelOpts.padding;
                                cursor.line++;
                            }
                            drawLegendBox(x, y, legendItem);
                            hitboxes[i].left = x;
                            hitboxes[i].top = y;
                            fillText(x, y, legendItem, textWidth);
                            if (isHorizontal) {
                                cursor.x += width + (labelOpts.padding);
                            } else {
                                cursor.y += itemHeight;
                            }
                        });
                    }
                },
                handleEvent: function(e) {
                    var me = this;
                    var opts = me.options;
                    var type = e.type === 'mouseup' ? 'click' : e.type;
                    var changed = false;
                    if (type === 'mousemove') {
                        if (!opts.onHover) {
                            return;
                        }
                    } else if (type === 'click') {
                        if (!opts.onClick) {
                            return;
                        }
                    } else {
                        return;
                    }
                    var x = e.x;
                    var y = e.y;
                    if (x >= me.left && x <= me.right && y >= me.top && y <= me.bottom) {
                        var lh = me.legendHitBoxes;
                        for (var i = 0; i < lh.length; ++i) {
                            var hitBox = lh[i];
                            if (x >= hitBox.left && x <= hitBox.left + hitBox.width && y >= hitBox.top && y <= hitBox.top + hitBox.height) {
                                if (type === 'click') {
                                    opts.onClick.call(me, e.native, me.legendItems[i]);
                                    changed = true;
                                    break;
                                } else if (type === 'mousemove') {
                                    opts.onHover.call(me, e.native, me.legendItems[i]);
                                    changed = true;
                                    break;
                                }
                            }
                        }
                    }
                    return changed;
                }
            });

            function createNewLegendAndAttach(chart, legendOpts) {
                var legend = new Legend({
                    ctx: chart.ctx,
                    options: legendOpts,
                    chart: chart
                });
                layouts.configure(chart, legend, legendOpts);
                layouts.addBox(chart, legend);
                chart.legend = legend;
            }
            module.exports = {
                id: 'legend',
                _element: Legend,
                beforeInit: function(chart) {
                    var legendOpts = chart.options.legend;
                    if (legendOpts) {
                        createNewLegendAndAttach(chart, legendOpts);
                    }
                },
                beforeUpdate: function(chart) {
                    var legendOpts = chart.options.legend;
                    var legend = chart.legend;
                    if (legendOpts) {
                        helpers.mergeIf(legendOpts, defaults.global.legend);
                        if (legend) {
                            layouts.configure(chart, legend, legendOpts);
                            legend.options = legendOpts;
                        } else {
                            createNewLegendAndAttach(chart, legendOpts);
                        }
                    } else if (legend) {
                        layouts.removeBox(chart, legend);
                        delete chart.legend;
                    }
                },
                afterEvent: function(chart, e) {
                    var legend = chart.legend;
                    if (legend) {
                        legend.handleEvent(e);
                    }
                }
            };
        }, {
            "25": 25,
            "26": 26,
            "30": 30,
            "45": 45
        }],
        52: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var Element = require(26);
            var helpers = require(45);
            var layouts = require(30);
            var noop = helpers.noop;
            defaults._set('global', {
                title: {
                    display: false,
                    fontStyle: 'bold',
                    fullWidth: true,
                    lineHeight: 1.2,
                    padding: 10,
                    position: 'top',
                    text: '',
                    weight: 2000
                }
            });
            var Title = Element.extend({
                initialize: function(config) {
                    var me = this;
                    helpers.extend(me, config);
                    me.legendHitBoxes = [];
                },
                beforeUpdate: noop,
                update: function(maxWidth, maxHeight, margins) {
                    var me = this;
                    me.beforeUpdate();
                    me.maxWidth = maxWidth;
                    me.maxHeight = maxHeight;
                    me.margins = margins;
                    me.beforeSetDimensions();
                    me.setDimensions();
                    me.afterSetDimensions();
                    me.beforeBuildLabels();
                    me.buildLabels();
                    me.afterBuildLabels();
                    me.beforeFit();
                    me.fit();
                    me.afterFit();
                    me.afterUpdate();
                    return me.minSize;
                },
                afterUpdate: noop,
                beforeSetDimensions: noop,
                setDimensions: function() {
                    var me = this;
                    if (me.isHorizontal()) {
                        me.width = me.maxWidth;
                        me.left = 0;
                        me.right = me.width;
                    } else {
                        me.height = me.maxHeight;
                        me.top = 0;
                        me.bottom = me.height;
                    }
                    me.paddingLeft = 0;
                    me.paddingTop = 0;
                    me.paddingRight = 0;
                    me.paddingBottom = 0;
                    me.minSize = {
                        width: 0,
                        height: 0
                    };
                },
                afterSetDimensions: noop,
                beforeBuildLabels: noop,
                buildLabels: noop,
                afterBuildLabels: noop,
                beforeFit: noop,
                fit: function() {
                    var me = this;
                    var valueOrDefault = helpers.valueOrDefault;
                    var opts = me.options;
                    var display = opts.display;
                    var fontSize = valueOrDefault(opts.fontSize, defaults.global.defaultFontSize);
                    var minSize = me.minSize;
                    var lineCount = helpers.isArray(opts.text) ? opts.text.length : 1;
                    var lineHeight = helpers.options.toLineHeight(opts.lineHeight, fontSize);
                    var textSize = display ? (lineCount * lineHeight) + (opts.padding * 2) : 0;
                    if (me.isHorizontal()) {
                        minSize.width = me.maxWidth;
                        minSize.height = textSize;
                    } else {
                        minSize.width = textSize;
                        minSize.height = me.maxHeight;
                    }
                    me.width = minSize.width;
                    me.height = minSize.height;
                },
                afterFit: noop,
                isHorizontal: function() {
                    var pos = this.options.position;
                    return pos === 'top' || pos === 'bottom';
                },
                draw: function() {
                    var me = this;
                    var ctx = me.ctx;
                    var valueOrDefault = helpers.valueOrDefault;
                    var opts = me.options;
                    var globalDefaults = defaults.global;
                    if (opts.display) {
                        var fontSize = valueOrDefault(opts.fontSize, globalDefaults.defaultFontSize);
                        var fontStyle = valueOrDefault(opts.fontStyle, globalDefaults.defaultFontStyle);
                        var fontFamily = valueOrDefault(opts.fontFamily, globalDefaults.defaultFontFamily);
                        var titleFont = helpers.fontString(fontSize, fontStyle, fontFamily);
                        var lineHeight = helpers.options.toLineHeight(opts.lineHeight, fontSize);
                        var offset = lineHeight / 2 + opts.padding;
                        var rotation = 0;
                        var top = me.top;
                        var left = me.left;
                        var bottom = me.bottom;
                        var right = me.right;
                        var maxWidth, titleX, titleY;
                        ctx.fillStyle = valueOrDefault(opts.fontColor, globalDefaults.defaultFontColor);
                        ctx.font = titleFont;
                        if (me.isHorizontal()) {
                            titleX = left + ((right - left) / 2);
                            titleY = top + offset;
                            maxWidth = right - left;
                        } else {
                            titleX = opts.position === 'left' ? left + offset : right - offset;
                            titleY = top + ((bottom - top) / 2);
                            maxWidth = bottom - top;
                            rotation = Math.PI * (opts.position === 'left' ? -0.5 : 0.5);
                        }
                        ctx.save();
                        ctx.translate(titleX, titleY);
                        ctx.rotate(rotation);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        var text = opts.text;
                        if (helpers.isArray(text)) {
                            var y = 0;
                            for (var i = 0; i < text.length; ++i) {
                                ctx.fillText(text[i], 0, y, maxWidth);
                                y += lineHeight;
                            }
                        } else {
                            ctx.fillText(text, 0, 0, maxWidth);
                        }
                        ctx.restore();
                    }
                }
            });

            function createNewTitleBlockAndAttach(chart, titleOpts) {
                var title = new Title({
                    ctx: chart.ctx,
                    options: titleOpts,
                    chart: chart
                });
                layouts.configure(chart, title, titleOpts);
                layouts.addBox(chart, title);
                chart.titleBlock = title;
            }
            module.exports = {
                id: 'title',
                _element: Title,
                beforeInit: function(chart) {
                    var titleOpts = chart.options.title;
                    if (titleOpts) {
                        createNewTitleBlockAndAttach(chart, titleOpts);
                    }
                },
                beforeUpdate: function(chart) {
                    var titleOpts = chart.options.title;
                    var titleBlock = chart.titleBlock;
                    if (titleOpts) {
                        helpers.mergeIf(titleOpts, defaults.global.title);
                        if (titleBlock) {
                            layouts.configure(chart, titleBlock, titleOpts);
                            titleBlock.options = titleOpts;
                        } else {
                            createNewTitleBlockAndAttach(chart, titleOpts);
                        }
                    } else if (titleBlock) {
                        layouts.removeBox(chart, titleBlock);
                        delete chart.titleBlock;
                    }
                }
            };
        }, {
            "25": 25,
            "26": 26,
            "30": 30,
            "45": 45
        }],
        53: [function(require, module, exports) {
            'use strict';
            module.exports = function(Chart) {
                var defaultConfig = {
                    position: 'bottom'
                };
                var DatasetScale = Chart.Scale.extend({
                    getLabels: function() {
                        var data = this.chart.data;
                        return this.options.labels || (this.isHorizontal() ? data.xLabels : data.yLabels) || data.labels;
                    },
                    determineDataLimits: function() {
                        var me = this;
                        var labels = me.getLabels();
                        me.minIndex = 0;
                        me.maxIndex = labels.length - 1;
                        var findIndex;
                        if (me.options.ticks.min !== undefined) {
                            findIndex = labels.indexOf(me.options.ticks.min);
                            me.minIndex = findIndex !== -1 ? findIndex : me.minIndex;
                        }
                        if (me.options.ticks.max !== undefined) {
                            findIndex = labels.indexOf(me.options.ticks.max);
                            me.maxIndex = findIndex !== -1 ? findIndex : me.maxIndex;
                        }
                        me.min = labels[me.minIndex];
                        me.max = labels[me.maxIndex];
                    },
                    buildTicks: function() {
                        var me = this;
                        var labels = me.getLabels();
                        me.ticks = (me.minIndex === 0 && me.maxIndex === labels.length - 1) ? labels : labels.slice(me.minIndex, me.maxIndex + 1);
                    },
                    getLabelForIndex: function(index, datasetIndex) {
                        var me = this;
                        var data = me.chart.data;
                        var isHorizontal = me.isHorizontal();
                        if (data.yLabels && !isHorizontal) {
                            return me.getRightValue(data.datasets[datasetIndex].data[index]);
                        }
                        return me.ticks[index - me.minIndex];
                    },
                    getPixelForValue: function(value, index) {
                        var me = this;
                        var offset = me.options.offset;
                        var offsetAmt = Math.max((me.maxIndex + 1 - me.minIndex - (offset ? 0 : 1)), 1);
                        var valueCategory;
                        if (value !== undefined && value !== null) {
                            valueCategory = me.isHorizontal() ? value.x : value.y;
                        }
                        if (valueCategory !== undefined || (value !== undefined && isNaN(index))) {
                            var labels = me.getLabels();
                            value = valueCategory || value;
                            var idx = labels.indexOf(value);
                            index = idx !== -1 ? idx : index;
                        }
                        if (me.isHorizontal()) {
                            var valueWidth = me.width / offsetAmt;
                            var widthOffset = (valueWidth * (index - me.minIndex));
                            if (offset) {
                                widthOffset += (valueWidth / 2);
                            }
                            return me.left + Math.round(widthOffset);
                        }
                        var valueHeight = me.height / offsetAmt;
                        var heightOffset = (valueHeight * (index - me.minIndex));
                        if (offset) {
                            heightOffset += (valueHeight / 2);
                        }
                        return me.top + Math.round(heightOffset);
                    },
                    getPixelForTick: function(index) {
                        return this.getPixelForValue(this.ticks[index], index + this.minIndex, null);
                    },
                    getValueForPixel: function(pixel) {
                        var me = this;
                        var offset = me.options.offset;
                        var value;
                        var offsetAmt = Math.max((me._ticks.length - (offset ? 0 : 1)), 1);
                        var horz = me.isHorizontal();
                        var valueDimension = (horz ? me.width : me.height) / offsetAmt;
                        pixel -= horz ? me.left : me.top;
                        if (offset) {
                            pixel -= (valueDimension / 2);
                        }
                        if (pixel <= 0) {
                            value = 0;
                        } else {
                            value = Math.round(pixel / valueDimension);
                        }
                        return value + me.minIndex;
                    },
                    getBasePixel: function() {
                        return this.bottom;
                    }
                });
                Chart.scaleService.registerScaleType('category', DatasetScale, defaultConfig);
            };
        }, {}],
        54: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var helpers = require(45);
            var Ticks = require(34);
            module.exports = function(Chart) {
                var defaultConfig = {
                    position: 'left',
                    ticks: {
                        callback: Ticks.formatters.linear
                    }
                };
                var LinearScale = Chart.LinearScaleBase.extend({
                    determineDataLimits: function() {
                        var me = this;
                        var opts = me.options;
                        var chart = me.chart;
                        var data = chart.data;
                        var datasets = data.datasets;
                        var isHorizontal = me.isHorizontal();
                        var DEFAULT_MIN = 0;
                        var DEFAULT_MAX = 1;

                        function IDMatches(meta) {
                            return isHorizontal ? meta.xAxisID === me.id : meta.yAxisID === me.id;
                        }
                        me.min = null;
                        me.max = null;
                        var hasStacks = opts.stacked;
                        if (hasStacks === undefined) {
                            helpers.each(datasets, function(dataset, datasetIndex) {
                                if (hasStacks) {
                                    return;
                                }
                                var meta = chart.getDatasetMeta(datasetIndex);
                                if (chart.isDatasetVisible(datasetIndex) && IDMatches(meta) && meta.stack !== undefined) {
                                    hasStacks = true;
                                }
                            });
                        }
                        if (opts.stacked || hasStacks) {
                            var valuesPerStack = {};
                            helpers.each(datasets, function(dataset, datasetIndex) {
                                var meta = chart.getDatasetMeta(datasetIndex);
                                var key = [meta.type, ((opts.stacked === undefined && meta.stack === undefined) ? datasetIndex : ''), meta.stack].join('.');
                                if (valuesPerStack[key] === undefined) {
                                    valuesPerStack[key] = {
                                        positiveValues: [],
                                        negativeValues: []
                                    };
                                }
                                var positiveValues = valuesPerStack[key].positiveValues;
                                var negativeValues = valuesPerStack[key].negativeValues;
                                if (chart.isDatasetVisible(datasetIndex) && IDMatches(meta)) {
                                    helpers.each(dataset.data, function(rawValue, index) {
                                        var value = +me.getRightValue(rawValue);
                                        if (isNaN(value) || meta.data[index].hidden) {
                                            return;
                                        }
                                        positiveValues[index] = positiveValues[index] || 0;
                                        negativeValues[index] = negativeValues[index] || 0;
                                        if (opts.relativePoints) {
                                            positiveValues[index] = 100;
                                        } else if (value < 0) {
                                            negativeValues[index] += value;
                                        } else {
                                            positiveValues[index] += value;
                                        }
                                    });
                                }
                            });
                            helpers.each(valuesPerStack, function(valuesForType) {
                                var values = valuesForType.positiveValues.concat(valuesForType.negativeValues);
                                var minVal = helpers.min(values);
                                var maxVal = helpers.max(values);
                                me.min = me.min === null ? minVal : Math.min(me.min, minVal);
                                me.max = me.max === null ? maxVal : Math.max(me.max, maxVal);
                            });
                        } else {
                            helpers.each(datasets, function(dataset, datasetIndex) {
                                var meta = chart.getDatasetMeta(datasetIndex);
                                if (chart.isDatasetVisible(datasetIndex) && IDMatches(meta)) {
                                    helpers.each(dataset.data, function(rawValue, index) {
                                        var value = +me.getRightValue(rawValue);
                                        if (isNaN(value) || meta.data[index].hidden) {
                                            return;
                                        }
                                        if (me.min === null) {
                                            me.min = value;
                                        } else if (value < me.min) {
                                            me.min = value;
                                        }
                                        if (me.max === null) {
                                            me.max = value;
                                        } else if (value > me.max) {
                                            me.max = value;
                                        }
                                    });
                                }
                            });
                        }
                        me.min = isFinite(me.min) && !isNaN(me.min) ? me.min : DEFAULT_MIN;
                        me.max = isFinite(me.max) && !isNaN(me.max) ? me.max : DEFAULT_MAX;
                        this.handleTickRangeOptions();
                    },
                    getTickLimit: function() {
                        var maxTicks;
                        var me = this;
                        var tickOpts = me.options.ticks;
                        if (me.isHorizontal()) {
                            maxTicks = Math.min(tickOpts.maxTicksLimit ? tickOpts.maxTicksLimit : 11, Math.ceil(me.width / 50));
                        } else {
                            var tickFontSize = helpers.valueOrDefault(tickOpts.fontSize, defaults.global.defaultFontSize);
                            maxTicks = Math.min(tickOpts.maxTicksLimit ? tickOpts.maxTicksLimit : 11, Math.ceil(me.height / (2 * tickFontSize)));
                        }
                        return maxTicks;
                    },
                    handleDirectionalChanges: function() {
                        if (!this.isHorizontal()) {
                            this.ticks.reverse();
                        }
                    },
                    getLabelForIndex: function(index, datasetIndex) {
                        return +this.getRightValue(this.chart.data.datasets[datasetIndex].data[index]);
                    },
                    getPixelForValue: function(value) {
                        var me = this;
                        var start = me.start;
                        var rightValue = +me.getRightValue(value);
                        var pixel;
                        var range = me.end - start;
                        if (me.isHorizontal()) {
                            pixel = me.left + (me.width / range * (rightValue - start));
                        } else {
                            pixel = me.bottom - (me.height / range * (rightValue - start));
                        }
                        return pixel;
                    },
                    getValueForPixel: function(pixel) {
                        var me = this;
                        var isHorizontal = me.isHorizontal();
                        var innerDimension = isHorizontal ? me.width : me.height;
                        var offset = (isHorizontal ? pixel - me.left : me.bottom - pixel) / innerDimension;
                        return me.start + ((me.end - me.start) * offset);
                    },
                    getPixelForTick: function(index) {
                        return this.getPixelForValue(this.ticksAsNumbers[index]);
                    }
                });
                Chart.scaleService.registerScaleType('linear', LinearScale, defaultConfig);
            };
        }, {
            "25": 25,
            "34": 34,
            "45": 45
        }],
        55: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);

            function generateTicks(generationOptions, dataRange) {
                var ticks = [];
                var spacing;
                if (generationOptions.stepSize && generationOptions.stepSize > 0) {
                    spacing = generationOptions.stepSize;
                } else {
                    var niceRange = helpers.niceNum(dataRange.max - dataRange.min, false);
                    spacing = helpers.niceNum(niceRange / (generationOptions.maxTicks - 1), true);
                }
                var niceMin = Math.floor(dataRange.min / spacing) * spacing;
                var niceMax = Math.ceil(dataRange.max / spacing) * spacing;
                if (generationOptions.min && generationOptions.max && generationOptions.stepSize) {
                    if (helpers.almostWhole((generationOptions.max - generationOptions.min) / generationOptions.stepSize, spacing / 1000)) {
                        niceMin = generationOptions.min;
                        niceMax = generationOptions.max;
                    }
                }
                var numSpaces = (niceMax - niceMin) / spacing;
                if (helpers.almostEquals(numSpaces, Math.round(numSpaces), spacing / 1000)) {
                    numSpaces = Math.round(numSpaces);
                } else {
                    numSpaces = Math.ceil(numSpaces);
                }
                var precision = 1;
                if (spacing < 1) {
                    precision = Math.pow(10, spacing.toString().length - 2);
                    niceMin = Math.round(niceMin * precision) / precision;
                    niceMax = Math.round(niceMax * precision) / precision;
                }
                ticks.push(generationOptions.min !== undefined ? generationOptions.min : niceMin);
                for (var j = 1; j < numSpaces; ++j) {
                    ticks.push(Math.round((niceMin + j * spacing) * precision) / precision);
                }
                ticks.push(generationOptions.max !== undefined ? generationOptions.max : niceMax);
                return ticks;
            }
            module.exports = function(Chart) {
                var noop = helpers.noop;
                Chart.LinearScaleBase = Chart.Scale.extend({
                    getRightValue: function(value) {
                        if (typeof value === 'string') {
                            return +value;
                        }
                        return Chart.Scale.prototype.getRightValue.call(this, value);
                    },
                    handleTickRangeOptions: function() {
                        var me = this;
                        var opts = me.options;
                        var tickOpts = opts.ticks;
                        if (tickOpts.beginAtZero) {
                            var minSign = helpers.sign(me.min);
                            var maxSign = helpers.sign(me.max);
                            if (minSign < 0 && maxSign < 0) {
                                me.max = 0;
                            } else if (minSign > 0 && maxSign > 0) {
                                me.min = 0;
                            }
                        }
                        var setMin = tickOpts.min !== undefined || tickOpts.suggestedMin !== undefined;
                        var setMax = tickOpts.max !== undefined || tickOpts.suggestedMax !== undefined;
                        if (tickOpts.min !== undefined) {
                            me.min = tickOpts.min;
                        } else if (tickOpts.suggestedMin !== undefined) {
                            if (me.min === null) {
                                me.min = tickOpts.suggestedMin;
                            } else {
                                me.min = Math.min(me.min, tickOpts.suggestedMin);
                            }
                        }
                        if (tickOpts.max !== undefined) {
                            me.max = tickOpts.max;
                        } else if (tickOpts.suggestedMax !== undefined) {
                            if (me.max === null) {
                                me.max = tickOpts.suggestedMax;
                            } else {
                                me.max = Math.max(me.max, tickOpts.suggestedMax);
                            }
                        }
                        if (setMin !== setMax) {
                            if (me.min >= me.max) {
                                if (setMin) {
                                    me.max = me.min + 1;
                                } else {
                                    me.min = me.max - 1;
                                }
                            }
                        }
                        if (me.min === me.max) {
                            me.max++;
                            if (!tickOpts.beginAtZero) {
                                me.min--;
                            }
                        }
                    },
                    getTickLimit: noop,
                    handleDirectionalChanges: noop,
                    buildTicks: function() {
                        var me = this;
                        var opts = me.options;
                        var tickOpts = opts.ticks;
                        var maxTicks = me.getTickLimit();
                        maxTicks = Math.max(2, maxTicks);
                        var numericGeneratorOptions = {
                            maxTicks: maxTicks,
                            min: tickOpts.min,
                            max: tickOpts.max,
                            stepSize: helpers.valueOrDefault(tickOpts.fixedStepSize, tickOpts.stepSize)
                        };
                        var ticks = me.ticks = generateTicks(numericGeneratorOptions, me);
                        me.handleDirectionalChanges();
                        me.max = helpers.max(ticks);
                        me.min = helpers.min(ticks);
                        if (tickOpts.reverse) {
                            ticks.reverse();
                            me.start = me.max;
                            me.end = me.min;
                        } else {
                            me.start = me.min;
                            me.end = me.max;
                        }
                    },
                    convertTicksToLabels: function() {
                        var me = this;
                        me.ticksAsNumbers = me.ticks.slice();
                        me.zeroLineIndex = me.ticks.indexOf(0);
                        Chart.Scale.prototype.convertTicksToLabels.call(me);
                    }
                });
            };
        }, {
            "45": 45
        }],
        56: [function(require, module, exports) {
            'use strict';
            var helpers = require(45);
            var Ticks = require(34);

            function generateTicks(generationOptions, dataRange) {
                var ticks = [];
                var valueOrDefault = helpers.valueOrDefault;
                var tickVal = valueOrDefault(generationOptions.min, Math.pow(10, Math.floor(helpers.log10(dataRange.min))));
                var endExp = Math.floor(helpers.log10(dataRange.max));
                var endSignificand = Math.ceil(dataRange.max / Math.pow(10, endExp));
                var exp, significand;
                if (tickVal === 0) {
                    exp = Math.floor(helpers.log10(dataRange.minNotZero));
                    significand = Math.floor(dataRange.minNotZero / Math.pow(10, exp));
                    ticks.push(tickVal);
                    tickVal = significand * Math.pow(10, exp);
                } else {
                    exp = Math.floor(helpers.log10(tickVal));
                    significand = Math.floor(tickVal / Math.pow(10, exp));
                }
                var precision = exp < 0 ? Math.pow(10, Math.abs(exp)) : 1;
                do {
                    ticks.push(tickVal);
                    ++significand;
                    if (significand === 10) {
                        significand = 1;
                        ++exp;
                        precision = exp >= 0 ? 1 : precision;
                    }
                    tickVal = Math.round(significand * Math.pow(10, exp) * precision) / precision;
                } while (exp < endExp || (exp === endExp && significand < endSignificand));
                var lastTick = valueOrDefault(generationOptions.max, tickVal);
                ticks.push(lastTick);
                return ticks;
            }
            module.exports = function(Chart) {
                var defaultConfig = {
                    position: 'left',
                    ticks: {
                        callback: Ticks.formatters.logarithmic
                    }
                };
                var LogarithmicScale = Chart.Scale.extend({
                    determineDataLimits: function() {
                        var me = this;
                        var opts = me.options;
                        var chart = me.chart;
                        var data = chart.data;
                        var datasets = data.datasets;
                        var isHorizontal = me.isHorizontal();

                        function IDMatches(meta) {
                            return isHorizontal ? meta.xAxisID === me.id : meta.yAxisID === me.id;
                        }
                        me.min = null;
                        me.max = null;
                        me.minNotZero = null;
                        var hasStacks = opts.stacked;
                        if (hasStacks === undefined) {
                            helpers.each(datasets, function(dataset, datasetIndex) {
                                if (hasStacks) {
                                    return;
                                }
                                var meta = chart.getDatasetMeta(datasetIndex);
                                if (chart.isDatasetVisible(datasetIndex) && IDMatches(meta) && meta.stack !== undefined) {
                                    hasStacks = true;
                                }
                            });
                        }
                        if (opts.stacked || hasStacks) {
                            var valuesPerStack = {};
                            helpers.each(datasets, function(dataset, datasetIndex) {
                                var meta = chart.getDatasetMeta(datasetIndex);
                                var key = [meta.type, ((opts.stacked === undefined && meta.stack === undefined) ? datasetIndex : ''), meta.stack].join('.');
                                if (chart.isDatasetVisible(datasetIndex) && IDMatches(meta)) {
                                    if (valuesPerStack[key] === undefined) {
                                        valuesPerStack[key] = [];
                                    }
                                    helpers.each(dataset.data, function(rawValue, index) {
                                        var values = valuesPerStack[key];
                                        var value = +me.getRightValue(rawValue);
                                        if (isNaN(value) || meta.data[index].hidden || value < 0) {
                                            return;
                                        }
                                        values[index] = values[index] || 0;
                                        values[index] += value;
                                    });
                                }
                            });
                            helpers.each(valuesPerStack, function(valuesForType) {
                                if (valuesForType.length > 0) {
                                    var minVal = helpers.min(valuesForType);
                                    var maxVal = helpers.max(valuesForType);
                                    me.min = me.min === null ? minVal : Math.min(me.min, minVal);
                                    me.max = me.max === null ? maxVal : Math.max(me.max, maxVal);
                                }
                            });
                        } else {
                            helpers.each(datasets, function(dataset, datasetIndex) {
                                var meta = chart.getDatasetMeta(datasetIndex);
                                if (chart.isDatasetVisible(datasetIndex) && IDMatches(meta)) {
                                    helpers.each(dataset.data, function(rawValue, index) {
                                        var value = +me.getRightValue(rawValue);
                                        if (isNaN(value) || meta.data[index].hidden || value < 0) {
                                            return;
                                        }
                                        if (me.min === null) {
                                            me.min = value;
                                        } else if (value < me.min) {
                                            me.min = value;
                                        }
                                        if (me.max === null) {
                                            me.max = value;
                                        } else if (value > me.max) {
                                            me.max = value;
                                        }
                                        if (value !== 0 && (me.minNotZero === null || value < me.minNotZero)) {
                                            me.minNotZero = value;
                                        }
                                    });
                                }
                            });
                        }
                        this.handleTickRangeOptions();
                    },
                    handleTickRangeOptions: function() {
                        var me = this;
                        var opts = me.options;
                        var tickOpts = opts.ticks;
                        var valueOrDefault = helpers.valueOrDefault;
                        var DEFAULT_MIN = 1;
                        var DEFAULT_MAX = 10;
                        me.min = valueOrDefault(tickOpts.min, me.min);
                        me.max = valueOrDefault(tickOpts.max, me.max);
                        if (me.min === me.max) {
                            if (me.min !== 0 && me.min !== null) {
                                me.min = Math.pow(10, Math.floor(helpers.log10(me.min)) - 1);
                                me.max = Math.pow(10, Math.floor(helpers.log10(me.max)) + 1);
                            } else {
                                me.min = DEFAULT_MIN;
                                me.max = DEFAULT_MAX;
                            }
                        }
                        if (me.min === null) {
                            me.min = Math.pow(10, Math.floor(helpers.log10(me.max)) - 1);
                        }
                        if (me.max === null) {
                            me.max = me.min !== 0 ? Math.pow(10, Math.floor(helpers.log10(me.min)) + 1) : DEFAULT_MAX;
                        }
                        if (me.minNotZero === null) {
                            if (me.min > 0) {
                                me.minNotZero = me.min;
                            } else if (me.max < 1) {
                                me.minNotZero = Math.pow(10, Math.floor(helpers.log10(me.max)));
                            } else {
                                me.minNotZero = DEFAULT_MIN;
                            }
                        }
                    },
                    buildTicks: function() {
                        var me = this;
                        var opts = me.options;
                        var tickOpts = opts.ticks;
                        var reverse = !me.isHorizontal();
                        var generationOptions = {
                            min: tickOpts.min,
                            max: tickOpts.max
                        };
                        var ticks = me.ticks = generateTicks(generationOptions, me);
                        me.max = helpers.max(ticks);
                        me.min = helpers.min(ticks);
                        if (tickOpts.reverse) {
                            reverse = !reverse;
                            me.start = me.max;
                            me.end = me.min;
                        } else {
                            me.start = me.min;
                            me.end = me.max;
                        }
                        if (reverse) {
                            ticks.reverse();
                        }
                    },
                    convertTicksToLabels: function() {
                        this.tickValues = this.ticks.slice();
                        Chart.Scale.prototype.convertTicksToLabels.call(this);
                    },
                    getLabelForIndex: function(index, datasetIndex) {
                        return +this.getRightValue(this.chart.data.datasets[datasetIndex].data[index]);
                    },
                    getPixelForTick: function(index) {
                        return this.getPixelForValue(this.tickValues[index]);
                    },
                    _getFirstTickValue: function(value) {
                        var exp = Math.floor(helpers.log10(value));
                        var significand = Math.floor(value / Math.pow(10, exp));
                        return significand * Math.pow(10, exp);
                    },
                    getPixelForValue: function(value) {
                        var me = this;
                        var reverse = me.options.ticks.reverse;
                        var log10 = helpers.log10;
                        var firstTickValue = me._getFirstTickValue(me.minNotZero);
                        var offset = 0;
                        var innerDimension, pixel, start, end, sign;
                        value = +me.getRightValue(value);
                        if (reverse) {
                            start = me.end;
                            end = me.start;
                            sign = -1;
                        } else {
                            start = me.start;
                            end = me.end;
                            sign = 1;
                        }
                        if (me.isHorizontal()) {
                            innerDimension = me.width;
                            pixel = reverse ? me.right : me.left;
                        } else {
                            innerDimension = me.height;
                            sign *= -1;
                            pixel = reverse ? me.top : me.bottom;
                        }
                        if (value !== start) {
                            if (start === 0) {
                                offset = helpers.getValueOrDefault(me.options.ticks.fontSize, Chart.defaults.global.defaultFontSize);
                                innerDimension -= offset;
                                start = firstTickValue;
                            }
                            if (value !== 0) {
                                offset += innerDimension / (log10(end) - log10(start)) * (log10(value) - log10(start));
                            }
                            pixel += sign * offset;
                        }
                        return pixel;
                    },
                    getValueForPixel: function(pixel) {
                        var me = this;
                        var reverse = me.options.ticks.reverse;
                        var log10 = helpers.log10;
                        var firstTickValue = me._getFirstTickValue(me.minNotZero);
                        var innerDimension, start, end, value;
                        if (reverse) {
                            start = me.end;
                            end = me.start;
                        } else {
                            start = me.start;
                            end = me.end;
                        }
                        if (me.isHorizontal()) {
                            innerDimension = me.width;
                            value = reverse ? me.right - pixel : pixel - me.left;
                        } else {
                            innerDimension = me.height;
                            value = reverse ? pixel - me.top : me.bottom - pixel;
                        }
                        if (value !== start) {
                            if (start === 0) {
                                var offset = helpers.getValueOrDefault(me.options.ticks.fontSize, Chart.defaults.global.defaultFontSize);
                                value -= offset;
                                innerDimension -= offset;
                                start = firstTickValue;
                            }
                            value *= log10(end) - log10(start);
                            value /= innerDimension;
                            value = Math.pow(10, log10(start) + value);
                        }
                        return value;
                    }
                });
                Chart.scaleService.registerScaleType('logarithmic', LogarithmicScale, defaultConfig);
            };
        }, {
            "34": 34,
            "45": 45
        }],
        57: [function(require, module, exports) {
            'use strict';
            var defaults = require(25);
            var helpers = require(45);
            var Ticks = require(34);
            module.exports = function(Chart) {
                var globalDefaults = defaults.global;
                var defaultConfig = {
                    display: true,
                    animate: true,
                    position: 'chartArea',
                    angleLines: {
                        display: true,
                        color: 'rgba(0, 0, 0, 0.1)',
                        lineWidth: 1
                    },
                    gridLines: {
                        circular: false
                    },
                    ticks: {
                        showLabelBackdrop: true,
                        backdropColor: 'rgba(255,255,255,0.75)',
                        backdropPaddingY: 2,
                        backdropPaddingX: 2,
                        callback: Ticks.formatters.linear
                    },
                    pointLabels: {
                        display: true,
                        fontSize: 10,
                        callback: function(label) {
                            return label;
                        }
                    }
                };

                function getValueCount(scale) {
                    var opts = scale.options;
                    return opts.angleLines.display || opts.pointLabels.display ? scale.chart.data.labels.length : 0;
                }

                function getPointLabelFontOptions(scale) {
                    var pointLabelOptions = scale.options.pointLabels;
                    var fontSize = helpers.valueOrDefault(pointLabelOptions.fontSize, globalDefaults.defaultFontSize);
                    var fontStyle = helpers.valueOrDefault(pointLabelOptions.fontStyle, globalDefaults.defaultFontStyle);
                    var fontFamily = helpers.valueOrDefault(pointLabelOptions.fontFamily, globalDefaults.defaultFontFamily);
                    var font = helpers.fontString(fontSize, fontStyle, fontFamily);
                    return {
                        size: fontSize,
                        style: fontStyle,
                        family: fontFamily,
                        font: font
                    };
                }

                function measureLabelSize(ctx, fontSize, label) {
                    if (helpers.isArray(label)) {
                        return {
                            w: helpers.longestText(ctx, ctx.font, label),
                            h: (label.length * fontSize) + ((label.length - 1) * 1.5 * fontSize)
                        };
                    }
                    return {
                        w: ctx.measureText(label).width,
                        h: fontSize
                    };
                }

                function determineLimits(angle, pos, size, min, max) {
                    if (angle === min || angle === max) {
                        return {
                            start: pos - (size / 2),
                            end: pos + (size / 2)
                        };
                    } else if (angle < min || angle > max) {
                        return {
                            start: pos - size - 5,
                            end: pos
                        };
                    }
                    return {
                        start: pos,
                        end: pos + size + 5
                    };
                }

                function fitWithPointLabels(scale) {
                    var plFont = getPointLabelFontOptions(scale);
                    var largestPossibleRadius = Math.min(scale.height / 2, scale.width / 2);
                    var furthestLimits = {
                        r: scale.width,
                        l: 0,
                        t: scale.height,
                        b: 0
                    };
                    var furthestAngles = {};
                    var i, textSize, pointPosition;
                    scale.ctx.font = plFont.font;
                    scale._pointLabelSizes = [];
                    var valueCount = getValueCount(scale);
                    for (i = 0; i < valueCount; i++) {
                        pointPosition = scale.getPointPosition(i, largestPossibleRadius);
                        textSize = measureLabelSize(scale.ctx, plFont.size, scale.pointLabels[i] || '');
                        scale._pointLabelSizes[i] = textSize;
                        var angleRadians = scale.getIndexAngle(i);
                        var angle = helpers.toDegrees(angleRadians) % 360;
                        var hLimits = determineLimits(angle, pointPosition.x, textSize.w, 0, 180);
                        var vLimits = determineLimits(angle, pointPosition.y, textSize.h, 90, 270);
                        if (hLimits.start < furthestLimits.l) {
                            furthestLimits.l = hLimits.start;
                            furthestAngles.l = angleRadians;
                        }
                        if (hLimits.end > furthestLimits.r) {
                            furthestLimits.r = hLimits.end;
                            furthestAngles.r = angleRadians;
                        }
                        if (vLimits.start < furthestLimits.t) {
                            furthestLimits.t = vLimits.start;
                            furthestAngles.t = angleRadians;
                        }
                        if (vLimits.end > furthestLimits.b) {
                            furthestLimits.b = vLimits.end;
                            furthestAngles.b = angleRadians;
                        }
                    }
                    scale.setReductions(largestPossibleRadius, furthestLimits, furthestAngles);
                }

                function fit(scale) {
                    var largestPossibleRadius = Math.min(scale.height / 2, scale.width / 2);
                    scale.drawingArea = Math.round(largestPossibleRadius);
                    scale.setCenterPoint(0, 0, 0, 0);
                }

                function getTextAlignForAngle(angle) {
                    if (angle === 0 || angle === 180) {
                        return 'center';
                    } else if (angle < 180) {
                        return 'left';
                    }
                    return 'right';
                }

                function fillText(ctx, text, position, fontSize) {
                    if (helpers.isArray(text)) {
                        var y = position.y;
                        var spacing = 1.5 * fontSize;
                        for (var i = 0; i < text.length; ++i) {
                            ctx.fillText(text[i], position.x, y);
                            y += spacing;
                        }
                    } else {
                        ctx.fillText(text, position.x, position.y);
                    }
                }

                function adjustPointPositionForLabelHeight(angle, textSize, position) {
                    if (angle === 90 || angle === 270) {
                        position.y -= (textSize.h / 2);
                    } else if (angle > 270 || angle < 90) {
                        position.y -= textSize.h;
                    }
                }

                function drawPointLabels(scale) {
                    var ctx = scale.ctx;
                    var opts = scale.options;
                    var angleLineOpts = opts.angleLines;
                    var pointLabelOpts = opts.pointLabels;
                    ctx.lineWidth = angleLineOpts.lineWidth;
                    ctx.strokeStyle = angleLineOpts.color;
                    var outerDistance = scale.getDistanceFromCenterForValue(opts.ticks.reverse ? scale.min : scale.max);
                    var plFont = getPointLabelFontOptions(scale);
                    ctx.textBaseline = 'top';
                    for (var i = getValueCount(scale) - 1; i >= 0; i--) {
                        if (angleLineOpts.display) {
                            var outerPosition = scale.getPointPosition(i, outerDistance);
                            ctx.beginPath();
                            ctx.moveTo(scale.xCenter, scale.yCenter);
                            ctx.lineTo(outerPosition.x, outerPosition.y);
                            ctx.stroke();
                            ctx.closePath();
                        }
                        if (pointLabelOpts.display) {
                            var pointLabelPosition = scale.getPointPosition(i, outerDistance + 5);
                            var pointLabelFontColor = helpers.valueAtIndexOrDefault(pointLabelOpts.fontColor, i, globalDefaults.defaultFontColor);
                            ctx.font = plFont.font;
                            ctx.fillStyle = pointLabelFontColor;
                            var angleRadians = scale.getIndexAngle(i);
                            var angle = helpers.toDegrees(angleRadians);
                            ctx.textAlign = getTextAlignForAngle(angle);
                            adjustPointPositionForLabelHeight(angle, scale._pointLabelSizes[i], pointLabelPosition);
                            fillText(ctx, scale.pointLabels[i] || '', pointLabelPosition, plFont.size);
                        }
                    }
                }

                function drawRadiusLine(scale, gridLineOpts, radius, index) {
                    var ctx = scale.ctx;
                    ctx.strokeStyle = helpers.valueAtIndexOrDefault(gridLineOpts.color, index - 1);
                    ctx.lineWidth = helpers.valueAtIndexOrDefault(gridLineOpts.lineWidth, index - 1);
                    if (scale.options.gridLines.circular) {
                        ctx.beginPath();
                        ctx.arc(scale.xCenter, scale.yCenter, radius, 0, Math.PI * 2);
                        ctx.closePath();
                        ctx.stroke();
                    } else {
                        var valueCount = getValueCount(scale);
                        if (valueCount === 0) {
                            return;
                        }
                        ctx.beginPath();
                        var pointPosition = scale.getPointPosition(0, radius);
                        ctx.moveTo(pointPosition.x, pointPosition.y);
                        for (var i = 1; i < valueCount; i++) {
                            pointPosition = scale.getPointPosition(i, radius);
                            ctx.lineTo(pointPosition.x, pointPosition.y);
                        }
                        ctx.closePath();
                        ctx.stroke();
                    }
                }

                function numberOrZero(param) {
                    return helpers.isNumber(param) ? param : 0;
                }
                var LinearRadialScale = Chart.LinearScaleBase.extend({
                    setDimensions: function() {
                        var me = this;
                        var opts = me.options;
                        var tickOpts = opts.ticks;
                        me.width = me.maxWidth;
                        me.height = me.maxHeight;
                        me.xCenter = Math.round(me.width / 2);
                        me.yCenter = Math.round(me.height / 2);
                        var minSize = helpers.min([me.height, me.width]);
                        var tickFontSize = helpers.valueOrDefault(tickOpts.fontSize, globalDefaults.defaultFontSize);
                        me.drawingArea = opts.display ? (minSize / 2) - (tickFontSize / 2 + tickOpts.backdropPaddingY) : (minSize / 2);
                    },
                    determineDataLimits: function() {
                        var me = this;
                        var chart = me.chart;
                        var min = Number.POSITIVE_INFINITY;
                        var max = Number.NEGATIVE_INFINITY;
                        helpers.each(chart.data.datasets, function(dataset, datasetIndex) {
                            if (chart.isDatasetVisible(datasetIndex)) {
                                var meta = chart.getDatasetMeta(datasetIndex);
                                helpers.each(dataset.data, function(rawValue, index) {
                                    var value = +me.getRightValue(rawValue);
                                    if (isNaN(value) || meta.data[index].hidden) {
                                        return;
                                    }
                                    min = Math.min(value, min);
                                    max = Math.max(value, max);
                                });
                            }
                        });
                        me.min = (min === Number.POSITIVE_INFINITY ? 0 : min);
                        me.max = (max === Number.NEGATIVE_INFINITY ? 0 : max);
                        me.handleTickRangeOptions();
                    },
                    getTickLimit: function() {
                        var tickOpts = this.options.ticks;
                        var tickFontSize = helpers.valueOrDefault(tickOpts.fontSize, globalDefaults.defaultFontSize);
                        return Math.min(tickOpts.maxTicksLimit ? tickOpts.maxTicksLimit : 11, Math.ceil(this.drawingArea / (1.5 * tickFontSize)));
                    },
                    convertTicksToLabels: function() {
                        var me = this;
                        Chart.LinearScaleBase.prototype.convertTicksToLabels.call(me);
                        me.pointLabels = me.chart.data.labels.map(me.options.pointLabels.callback, me);
                    },
                    getLabelForIndex: function(index, datasetIndex) {
                        return +this.getRightValue(this.chart.data.datasets[datasetIndex].data[index]);
                    },
                    fit: function() {
                        if (this.options.pointLabels.display) {
                            fitWithPointLabels(this);
                        } else {
                            fit(this);
                        }
                    },
                    setReductions: function(largestPossibleRadius, furthestLimits, furthestAngles) {
                        var me = this;
                        var radiusReductionLeft = furthestLimits.l / Math.sin(furthestAngles.l);
                        var radiusReductionRight = Math.max(furthestLimits.r - me.width, 0) / Math.sin(furthestAngles.r);
                        var radiusReductionTop = -furthestLimits.t / Math.cos(furthestAngles.t);
                        var radiusReductionBottom = -Math.max(furthestLimits.b - me.height, 0) / Math.cos(furthestAngles.b);
                        radiusReductionLeft = numberOrZero(radiusReductionLeft);
                        radiusReductionRight = numberOrZero(radiusReductionRight);
                        radiusReductionTop = numberOrZero(radiusReductionTop);
                        radiusReductionBottom = numberOrZero(radiusReductionBottom);
                        me.drawingArea = Math.min(Math.round(largestPossibleRadius - (radiusReductionLeft + radiusReductionRight) / 2), Math.round(largestPossibleRadius - (radiusReductionTop + radiusReductionBottom) / 2));
                        me.setCenterPoint(radiusReductionLeft, radiusReductionRight, radiusReductionTop, radiusReductionBottom);
                    },
                    setCenterPoint: function(leftMovement, rightMovement, topMovement, bottomMovement) {
                        var me = this;
                        var maxRight = me.width - rightMovement - me.drawingArea;
                        var maxLeft = leftMovement + me.drawingArea;
                        var maxTop = topMovement + me.drawingArea;
                        var maxBottom = me.height - bottomMovement - me.drawingArea;
                        me.xCenter = Math.round(((maxLeft + maxRight) / 2) + me.left);
                        me.yCenter = Math.round(((maxTop + maxBottom) / 2) + me.top);
                    },
                    getIndexAngle: function(index) {
                        var angleMultiplier = (Math.PI * 2) / getValueCount(this);
                        var startAngle = this.chart.options && this.chart.options.startAngle ? this.chart.options.startAngle : 0;
                        var startAngleRadians = startAngle * Math.PI * 2 / 360;
                        return index * angleMultiplier + startAngleRadians;
                    },
                    getDistanceFromCenterForValue: function(value) {
                        var me = this;
                        if (value === null) {
                            return 0;
                        }
                        var scalingFactor = me.drawingArea / (me.max - me.min);
                        if (me.options.ticks.reverse) {
                            return (me.max - value) * scalingFactor;
                        }
                        return (value - me.min) * scalingFactor;
                    },
                    getPointPosition: function(index, distanceFromCenter) {
                        var me = this;
                        var thisAngle = me.getIndexAngle(index) - (Math.PI / 2);
                        return {
                            x: Math.round(Math.cos(thisAngle) * distanceFromCenter) + me.xCenter,
                            y: Math.round(Math.sin(thisAngle) * distanceFromCenter) + me.yCenter
                        };
                    },
                    getPointPositionForValue: function(index, value) {
                        return this.getPointPosition(index, this.getDistanceFromCenterForValue(value));
                    },
                    getBasePosition: function() {
                        var me = this;
                        var min = me.min;
                        var max = me.max;
                        return me.getPointPositionForValue(0, me.beginAtZero ? 0 : min < 0 && max < 0 ? max : min > 0 && max > 0 ? min : 0);
                    },
                    draw: function() {
                        var me = this;
                        var opts = me.options;
                        var gridLineOpts = opts.gridLines;
                        var tickOpts = opts.ticks;
                        var valueOrDefault = helpers.valueOrDefault;
                        if (opts.display) {
                            var ctx = me.ctx;
                            var startAngle = this.getIndexAngle(0);
                            var tickFontSize = valueOrDefault(tickOpts.fontSize, globalDefaults.defaultFontSize);
                            var tickFontStyle = valueOrDefault(tickOpts.fontStyle, globalDefaults.defaultFontStyle);
                            var tickFontFamily = valueOrDefault(tickOpts.fontFamily, globalDefaults.defaultFontFamily);
                            var tickLabelFont = helpers.fontString(tickFontSize, tickFontStyle, tickFontFamily);
                            helpers.each(me.ticks, function(label, index) {
                                if (index > 0 || tickOpts.reverse) {
                                    var yCenterOffset = me.getDistanceFromCenterForValue(me.ticksAsNumbers[index]);
                                    if (gridLineOpts.display && index !== 0) {
                                        drawRadiusLine(me, gridLineOpts, yCenterOffset, index);
                                    }
                                    if (tickOpts.display) {
                                        var tickFontColor = valueOrDefault(tickOpts.fontColor, globalDefaults.defaultFontColor);
                                        ctx.font = tickLabelFont;
                                        ctx.save();
                                        ctx.translate(me.xCenter, me.yCenter);
                                        ctx.rotate(startAngle);
                                        if (tickOpts.showLabelBackdrop) {
                                            var labelWidth = ctx.measureText(label).width;
                                            ctx.fillStyle = tickOpts.backdropColor;
                                            ctx.fillRect(-labelWidth / 2 - tickOpts.backdropPaddingX, -yCenterOffset - tickFontSize / 2 - tickOpts.backdropPaddingY, labelWidth + tickOpts.backdropPaddingX * 2, tickFontSize + tickOpts.backdropPaddingY * 2);
                                        }
                                        ctx.textAlign = 'center';
                                        ctx.textBaseline = 'middle';
                                        ctx.fillStyle = tickFontColor;
                                        ctx.fillText(label, 0, -yCenterOffset);
                                        ctx.restore();
                                    }
                                }
                            });
                            if (opts.angleLines.display || opts.pointLabels.display) {
                                drawPointLabels(me);
                            }
                        }
                    }
                });
                Chart.scaleService.registerScaleType('radialLinear', LinearRadialScale, defaultConfig);
            };
        }, {
            "25": 25,
            "34": 34,
            "45": 45
        }],
        58: [function(require, module, exports) {
            'use strict';
            var moment = require(1);
            moment = typeof moment === 'function' ? moment : window.moment;
            var defaults = require(25);
            var helpers = require(45);
            var MIN_INTEGER = Number.MIN_SAFE_INTEGER || -9007199254740991;
            var MAX_INTEGER = Number.MAX_SAFE_INTEGER || 9007199254740991;
            var INTERVALS = {
                millisecond: {
                    common: true,
                    size: 1,
                    steps: [1, 2, 5, 10, 20, 50, 100, 250, 500]
                },
                second: {
                    common: true,
                    size: 1000,
                    steps: [1, 2, 5, 10, 30]
                },
                minute: {
                    common: true,
                    size: 60000,
                    steps: [1, 2, 5, 10, 30]
                },
                hour: {
                    common: true,
                    size: 3600000,
                    steps: [1, 2, 3, 6, 12]
                },
                day: {
                    common: true,
                    size: 86400000,
                    steps: [1, 2, 5]
                },
                week: {
                    common: false,
                    size: 604800000,
                    steps: [1, 2, 3, 4]
                },
                month: {
                    common: true,
                    size: 2.628e9,
                    steps: [1, 2, 3]
                },
                quarter: {
                    common: false,
                    size: 7.884e9,
                    steps: [1, 2, 3, 4]
                },
                year: {
                    common: true,
                    size: 3.154e10
                }
            };
            var UNITS = Object.keys(INTERVALS);

            function sorter(a, b) {
                return a - b;
            }

            function arrayUnique(items) {
                var hash = {};
                var out = [];
                var i, ilen, item;
                for (i = 0, ilen = items.length; i < ilen; ++i) {
                    item = items[i];
                    if (!hash[item]) {
                        hash[item] = true;
                        out.push(item);
                    }
                }
                return out;
            }

            function buildLookupTable(timestamps, min, max, distribution) {
                if (distribution === 'linear' || !timestamps.length) {
                    return [{
                        time: min,
                        pos: 0
                    }, {
                        time: max,
                        pos: 1
                    }];
                }
                var table = [];
                var items = [min];
                var i, ilen, prev, curr, next;
                for (i = 0, ilen = timestamps.length; i < ilen; ++i) {
                    curr = timestamps[i];
                    if (curr > min && curr < max) {
                        items.push(curr);
                    }
                }
                items.push(max);
                for (i = 0, ilen = items.length; i < ilen; ++i) {
                    next = items[i + 1];
                    prev = items[i - 1];
                    curr = items[i];
                    if (prev === undefined || next === undefined || Math.round((next + prev) / 2) !== curr) {
                        table.push({
                            time: curr,
                            pos: i / (ilen - 1)
                        });
                    }
                }
                return table;
            }

            function lookup(table, key, value) {
                var lo = 0;
                var hi = table.length - 1;
                var mid, i0, i1;
                while (lo >= 0 && lo <= hi) {
                    mid = (lo + hi) >> 1;
                    i0 = table[mid - 1] || null;
                    i1 = table[mid];
                    if (!i0) {
                        return {
                            lo: null,
                            hi: i1
                        };
                    } else if (i1[key] < value) {
                        lo = mid + 1;
                    } else if (i0[key] > value) {
                        hi = mid - 1;
                    } else {
                        return {
                            lo: i0,
                            hi: i1
                        };
                    }
                }
                return {
                    lo: i1,
                    hi: null
                };
            }

            function interpolate(table, skey, sval, tkey) {
                var range = lookup(table, skey, sval);
                var prev = !range.lo ? table[0] : !range.hi ? table[table.length - 2] : range.lo;
                var next = !range.lo ? table[1] : !range.hi ? table[table.length - 1] : range.hi;
                var span = next[skey] - prev[skey];
                var ratio = span ? (sval - prev[skey]) / span : 0;
                var offset = (next[tkey] - prev[tkey]) * ratio;
                return prev[tkey] + offset;
            }

            function momentify(value, options) {
                var parser = options.parser;
                var format = options.parser || options.format;
                if (typeof parser === 'function') {
                    return parser(value);
                }
                if (typeof value === 'string' && typeof format === 'string') {
                    return moment(value, format);
                }
                if (!(value instanceof moment)) {
                    value = moment(value);
                }
                if (value.isValid()) {
                    return value;
                }
                if (typeof format === 'function') {
                    return format(value);
                }
                return value;
            }

            function parse(input, scale) {
                if (helpers.isNullOrUndef(input)) {
                    return null;
                }
                var options = scale.options.time;
                var value = momentify(scale.getRightValue(input), options);
                if (!value.isValid()) {
                    return null;
                }
                if (options.round) {
                    value.startOf(options.round);
                }
                return value.valueOf();
            }

            function determineStepSize(min, max, unit, capacity) {
                var range = max - min;
                var interval = INTERVALS[unit];
                var milliseconds = interval.size;
                var steps = interval.steps;
                var i, ilen, factor;
                if (!steps) {
                    return Math.ceil(range / (capacity * milliseconds));
                }
                for (i = 0, ilen = steps.length; i < ilen; ++i) {
                    factor = steps[i];
                    if (Math.ceil(range / (milliseconds * factor)) <= capacity) {
                        break;
                    }
                }
                return factor;
            }

            function determineUnitForAutoTicks(minUnit, min, max, capacity) {
                var ilen = UNITS.length;
                var i, interval, factor;
                for (i = UNITS.indexOf(minUnit); i < ilen - 1; ++i) {
                    interval = INTERVALS[UNITS[i]];
                    factor = interval.steps ? interval.steps[interval.steps.length - 1] : MAX_INTEGER;
                    if (interval.common && Math.ceil((max - min) / (factor * interval.size)) <= capacity) {
                        return UNITS[i];
                    }
                }
                return UNITS[ilen - 1];
            }

            function determineUnitForFormatting(ticks, minUnit, min, max) {
                var duration = moment.duration(moment(max).diff(moment(min)));
                var ilen = UNITS.length;
                var i, unit;
                for (i = ilen - 1; i >= UNITS.indexOf(minUnit); i--) {
                    unit = UNITS[i];
                    if (INTERVALS[unit].common && duration.as(unit) >= ticks.length) {
                        return unit;
                    }
                }
                return UNITS[minUnit ? UNITS.indexOf(minUnit) : 0];
            }

            function determineMajorUnit(unit) {
                for (var i = UNITS.indexOf(unit) + 1, ilen = UNITS.length; i < ilen; ++i) {
                    if (INTERVALS[UNITS[i]].common) {
                        return UNITS[i];
                    }
                }
            }

            function generate(min, max, capacity, options) {
                var timeOpts = options.time;
                var minor = timeOpts.unit || determineUnitForAutoTicks(timeOpts.minUnit, min, max, capacity);
                var major = determineMajorUnit(minor);
                var stepSize = helpers.valueOrDefault(timeOpts.stepSize, timeOpts.unitStepSize);
                var weekday = minor === 'week' ? timeOpts.isoWeekday : false;
                var majorTicksEnabled = options.ticks.major.enabled;
                var interval = INTERVALS[minor];
                var first = moment(min);
                var last = moment(max);
                var ticks = [];
                var time;
                if (!stepSize) {
                    stepSize = determineStepSize(min, max, minor, capacity);
                }
                if (weekday) {
                    first = first.isoWeekday(weekday);
                    last = last.isoWeekday(weekday);
                }
                first = first.startOf(weekday ? 'day' : minor);
                last = last.startOf(weekday ? 'day' : minor);
                if (last < max) {
                    last.add(1, minor);
                }
                time = moment(first);
                if (majorTicksEnabled && major && !weekday && !timeOpts.round) {
                    time.startOf(major);
                    time.add(~~((first - time) / (interval.size * stepSize)) * stepSize, minor);
                }
                for (; time < last; time.add(stepSize, minor)) {
                    ticks.push(+time);
                }
                ticks.push(+time);
                return ticks;
            }

            function computeOffsets(table, ticks, min, max, options) {
                var left = 0;
                var right = 0;
                var upper, lower;
                if (options.offset && ticks.length) {
                    if (!options.time.min) {
                        upper = ticks.length > 1 ? ticks[1] : max;
                        lower = ticks[0];
                        left = (interpolate(table, 'time', upper, 'pos') -
                            interpolate(table, 'time', lower, 'pos')) / 2;
                    }
                    if (!options.time.max) {
                        upper = ticks[ticks.length - 1];
                        lower = ticks.length > 1 ? ticks[ticks.length - 2] : min;
                        right = (interpolate(table, 'time', upper, 'pos') -
                            interpolate(table, 'time', lower, 'pos')) / 2;
                    }
                }
                return {
                    left: left,
                    right: right
                };
            }

            function ticksFromTimestamps(values, majorUnit) {
                var ticks = [];
                var i, ilen, value, major;
                for (i = 0, ilen = values.length; i < ilen; ++i) {
                    value = values[i];
                    major = majorUnit ? value === +moment(value).startOf(majorUnit) : false;
                    ticks.push({
                        value: value,
                        major: major
                    });
                }
                return ticks;
            }

            function determineLabelFormat(data, timeOpts) {
                var i, momentDate, hasTime;
                var ilen = data.length;
                for (i = 0; i < ilen; i++) {
                    momentDate = momentify(data[i], timeOpts);
                    if (momentDate.millisecond() !== 0) {
                        return 'MMM D, YYYY h:mm:ss.SSS a';
                    }
                    if (momentDate.second() !== 0 || momentDate.minute() !== 0 || momentDate.hour() !== 0) {
                        hasTime = true;
                    }
                }
                if (hasTime) {
                    return 'MMM D, YYYY h:mm:ss a';
                }
                return 'MMM D, YYYY';
            }
            module.exports = function(Chart) {
                var defaultConfig = {
                    position: 'bottom',
                    distribution: 'linear',
                    bounds: 'data',
                    time: {
                        parser: false,
                        format: false,
                        unit: false,
                        round: false,
                        displayFormat: false,
                        isoWeekday: false,
                        minUnit: 'millisecond',
                        displayFormats: {
                            millisecond: 'h:mm:ss.SSS a',
                            second: 'h:mm:ss a',
                            minute: 'h:mm a',
                            hour: 'hA',
                            day: 'MMM D',
                            week: 'll',
                            month: 'MMM YYYY',
                            quarter: '[Q]Q - YYYY',
                            year: 'YYYY'
                        },
                    },
                    ticks: {
                        autoSkip: false,
                        source: 'auto',
                        major: {
                            enabled: false
                        }
                    }
                };
                var TimeScale = Chart.Scale.extend({
                    initialize: function() {
                        if (!moment) {
                            throw new Error('Chart.js - Moment.js could not be found! You must include it before Chart.js to use the time scale. Download at https://momentjs.com');
                        }
                        this.mergeTicksOptions();
                        Chart.Scale.prototype.initialize.call(this);
                    },
                    update: function() {
                        var me = this;
                        var options = me.options;
                        if (options.time && options.time.format) {
                            console.warn('options.time.format is deprecated and replaced by options.time.parser.');
                        }
                        return Chart.Scale.prototype.update.apply(me, arguments);
                    },
                    getRightValue: function(rawValue) {
                        if (rawValue && rawValue.t !== undefined) {
                            rawValue = rawValue.t;
                        }
                        return Chart.Scale.prototype.getRightValue.call(this, rawValue);
                    },
                    determineDataLimits: function() {
                        var me = this;
                        var chart = me.chart;
                        var timeOpts = me.options.time;
                        var unit = timeOpts.unit || 'day';
                        var min = MAX_INTEGER;
                        var max = MIN_INTEGER;
                        var timestamps = [];
                        var datasets = [];
                        var labels = [];
                        var i, j, ilen, jlen, data, timestamp;
                        for (i = 0, ilen = chart.data.labels.length; i < ilen; ++i) {
                            labels.push(parse(chart.data.labels[i], me));
                        }
                        for (i = 0, ilen = (chart.data.datasets || []).length; i < ilen; ++i) {
                            if (chart.isDatasetVisible(i)) {
                                data = chart.data.datasets[i].data;
                                if (helpers.isObject(data[0])) {
                                    datasets[i] = [];
                                    for (j = 0, jlen = data.length; j < jlen; ++j) {
                                        timestamp = parse(data[j], me);
                                        timestamps.push(timestamp);
                                        datasets[i][j] = timestamp;
                                    }
                                } else {
                                    timestamps.push.apply(timestamps, labels);
                                    datasets[i] = labels.slice(0);
                                }
                            } else {
                                datasets[i] = [];
                            }
                        }
                        if (labels.length) {
                            labels = arrayUnique(labels).sort(sorter);
                            min = Math.min(min, labels[0]);
                            max = Math.max(max, labels[labels.length - 1]);
                        }
                        if (timestamps.length) {
                            timestamps = arrayUnique(timestamps).sort(sorter);
                            min = Math.min(min, timestamps[0]);
                            max = Math.max(max, timestamps[timestamps.length - 1]);
                        }
                        min = parse(timeOpts.min, me) || min;
                        max = parse(timeOpts.max, me) || max;
                        min = min === MAX_INTEGER ? +moment().startOf(unit) : min;
                        max = max === MIN_INTEGER ? +moment().endOf(unit) + 1 : max;
                        me.min = Math.min(min, max);
                        me.max = Math.max(min + 1, max);
                        me._horizontal = me.isHorizontal();
                        me._table = [];
                        me._timestamps = {
                            data: timestamps,
                            datasets: datasets,
                            labels: labels
                        };
                    },
                    buildTicks: function() {
                        var me = this;
                        var min = me.min;
                        var max = me.max;
                        var options = me.options;
                        var timeOpts = options.time;
                        var timestamps = [];
                        var ticks = [];
                        var i, ilen, timestamp;
                        switch (options.ticks.source) {
                            case 'data':
                                timestamps = me._timestamps.data;
                                break;
                            case 'labels':
                                timestamps = me._timestamps.labels;
                                break;
                            case 'auto':
                            default:
                                timestamps = generate(min, max, me.getLabelCapacity(min), options);
                        }
                        if (options.bounds === 'ticks' && timestamps.length) {
                            min = timestamps[0];
                            max = timestamps[timestamps.length - 1];
                        }
                        min = parse(timeOpts.min, me) || min;
                        max = parse(timeOpts.max, me) || max;
                        for (i = 0, ilen = timestamps.length; i < ilen; ++i) {
                            timestamp = timestamps[i];
                            if (timestamp >= min && timestamp <= max) {
                                ticks.push(timestamp);
                            }
                        }
                        me.min = min;
                        me.max = max;
                        me._unit = timeOpts.unit || determineUnitForFormatting(ticks, timeOpts.minUnit, me.min, me.max);
                        me._majorUnit = determineMajorUnit(me._unit);
                        me._table = buildLookupTable(me._timestamps.data, min, max, options.distribution);
                        me._offsets = computeOffsets(me._table, ticks, min, max, options);
                        me._labelFormat = determineLabelFormat(me._timestamps.data, timeOpts);
                        return ticksFromTimestamps(ticks, me._majorUnit);
                    },
                    getLabelForIndex: function(index, datasetIndex) {
                        var me = this;
                        var data = me.chart.data;
                        var timeOpts = me.options.time;
                        var label = data.labels && index < data.labels.length ? data.labels[index] : '';
                        var value = data.datasets[datasetIndex].data[index];
                        if (helpers.isObject(value)) {
                            label = me.getRightValue(value);
                        }
                        if (timeOpts.tooltipFormat) {
                            return momentify(label, timeOpts).format(timeOpts.tooltipFormat);
                        }
                        if (typeof label === 'string') {
                            return label;
                        }
                        return momentify(label, timeOpts).format(me._labelFormat);
                    },
                    tickFormatFunction: function(tick, index, ticks, formatOverride) {
                        var me = this;
                        var options = me.options;
                        var time = tick.valueOf();
                        var formats = options.time.displayFormats;
                        var minorFormat = formats[me._unit];
                        var majorUnit = me._majorUnit;
                        var majorFormat = formats[majorUnit];
                        var majorTime = tick.clone().startOf(majorUnit).valueOf();
                        var majorTickOpts = options.ticks.major;
                        var major = majorTickOpts.enabled && majorUnit && majorFormat && time === majorTime;
                        var label = tick.format(formatOverride ? formatOverride : major ? majorFormat : minorFormat);
                        var tickOpts = major ? majorTickOpts : options.ticks.minor;
                        var formatter = helpers.valueOrDefault(tickOpts.callback, tickOpts.userCallback);
                        return formatter ? formatter(label, index, ticks) : label;
                    },
                    convertTicksToLabels: function(ticks) {
                        var labels = [];
                        var i, ilen;
                        for (i = 0, ilen = ticks.length; i < ilen; ++i) {
                            labels.push(this.tickFormatFunction(moment(ticks[i].value), i, ticks));
                        }
                        return labels;
                    },
                    getPixelForOffset: function(time) {
                        var me = this;
                        var size = me._horizontal ? me.width : me.height;
                        var start = me._horizontal ? me.left : me.top;
                        var pos = interpolate(me._table, 'time', time, 'pos');
                        return start + size * (me._offsets.left + pos) / (me._offsets.left + 1 + me._offsets.right);
                    },
                    getPixelForValue: function(value, index, datasetIndex) {
                        var me = this;
                        var time = null;
                        if (index !== undefined && datasetIndex !== undefined) {
                            time = me._timestamps.datasets[datasetIndex][index];
                        }
                        if (time === null) {
                            time = parse(value, me);
                        }
                        if (time !== null) {
                            return me.getPixelForOffset(time);
                        }
                    },
                    getPixelForTick: function(index) {
                        var ticks = this.getTicks();
                        return index >= 0 && index < ticks.length ? this.getPixelForOffset(ticks[index].value) : null;
                    },
                    getValueForPixel: function(pixel) {
                        var me = this;
                        var size = me._horizontal ? me.width : me.height;
                        var start = me._horizontal ? me.left : me.top;
                        var pos = (size ? (pixel - start) / size : 0) * (me._offsets.left + 1 + me._offsets.left) - me._offsets.right;
                        var time = interpolate(me._table, 'pos', pos, 'time');
                        return moment(time);
                    },
                    getLabelWidth: function(label) {
                        var me = this;
                        var ticksOpts = me.options.ticks;
                        var tickLabelWidth = me.ctx.measureText(label).width;
                        var angle = helpers.toRadians(ticksOpts.maxRotation);
                        var cosRotation = Math.cos(angle);
                        var sinRotation = Math.sin(angle);
                        var tickFontSize = helpers.valueOrDefault(ticksOpts.fontSize, defaults.global.defaultFontSize);
                        return (tickLabelWidth * cosRotation) + (tickFontSize * sinRotation);
                    },
                    getLabelCapacity: function(exampleTime) {
                        var me = this;
                        var formatOverride = me.options.time.displayFormats.millisecond;
                        var exampleLabel = me.tickFormatFunction(moment(exampleTime), 0, [], formatOverride);
                        var tickLabelWidth = me.getLabelWidth(exampleLabel);
                        var innerWidth = me.isHorizontal() ? me.width : me.height;
                        var capacity = Math.floor(innerWidth / tickLabelWidth);
                        return capacity > 0 ? capacity : 1;
                    }
                });
                Chart.scaleService.registerScaleType('time', TimeScale, defaultConfig);
            };
        }, {
            "1": 1,
            "25": 25,
            "45": 45
        }]
    }, {}, [7])(7)
});

(function() {
    function d() {
        this.drawDataset = this.drawDataset.bind(this)
    }
    "undefined" === typeof Chart ? console.warn("Can not find Chart object.") : (d.prototype.beforeDatasetsUpdate = function(a) {
            if (this.parseOptions(a) && "outside" === this.position) {
                var b = 1.5 * this.fontSize + this.outsidePadding;
                a.chartArea.top += b;
                a.chartArea.bottom -= b
            }
        }, d.prototype.afterDatasetsDraw = function(a) {
            this.parseOptions(a) && (this.labelBounds = [], a.config.data.datasets.forEach(this.drawDataset))
        }, d.prototype.drawDataset = function(a) {
            for (var b =
                    this.ctx, f = this.chartInstance, q = a._meta[Object.keys(a._meta)[0]], k = 0, g = 0; g < q.data.length; g++) {
                var h = q.data[g],
                    c = h._view;
                if (0 !== c.circumference || this.showZero) {
                    switch (this.render) {
                        case "value":
                            var e = a.data[g];
                            this.format && (e = this.format(e));
                            e = e.toString();
                            break;
                        case "label":
                            e = f.config.data.labels[g];
                            break;
                        case "image":
                            e = this.images[g] ? this.loadImage(this.images[g]) : "";
                            break;
                        default:
                            var p = c.circumference / this.options.circumference * 100;
                            p = parseFloat(p.toFixed(this.precision));
                            this.showActualPercentages ||
                                (k += p, 100 < k && (p -= k - 100, p = parseFloat(p.toFixed(this.precision))));
                            e = p + "%"
                    }
                    "function" === typeof this.render && (e = this.render({
                        label: f.config.data.labels[g],
                        value: a.data[g],
                        percentage: p,
                        dataset: a,
                        index: g
                    }), "object" === typeof e && (e = this.loadImage(e)));
                    if (e) {
                        b.save();
                        b.beginPath();
                        b.font = Chart.helpers.fontString(this.fontSize, this.fontStyle, this.fontFamily);
                        if ("outside" === this.position || "border" === this.position) {
                            var l = c.outerRadius / 2;
                            var d, m = this.fontSize + 2;
                            var n = c.startAngle + (c.endAngle - c.startAngle) / 2;
                            "border" === this.position ? d = (c.outerRadius - l) / 2 + l : "outside" === this.position && (d = c.outerRadius - l + l + m);
                            n = {
                                x: c.x + Math.cos(n) * d,
                                y: c.y + Math.sin(n) * d
                            };
                            if ("outside" === this.position) {
                                n.x = n.x < c.x ? n.x - m : n.x + m;
                                var r = c.outerRadius + m
                            }
                        } else l = c.innerRadius, n = h.tooltipPosition();
                        m = this.fontColor;
                        "function" === typeof m ? m = m({
                            label: f.config.data.labels[g],
                            value: a.data[g],
                            percentage: p,
                            text: e,
                            backgroundColor: a.backgroundColor[g],
                            dataset: a,
                            index: g
                        }) : "string" !== typeof m && (m = m[g] || this.options.defaultFontColor);
                        if (this.arc) r ||
                            (r = (l + c.outerRadius) / 2), b.fillStyle = m, b.textBaseline = "middle", this.drawArcText(e, r, c, this.overlap);
                        else {
                            l = this.measureText(e);
                            c = n.x - l.width / 2;
                            l = n.x + l.width / 2;
                            var t = n.y - this.fontSize / 2,
                                u = n.y + this.fontSize / 2;
                            (this.overlap || ("outside" === this.position ? this.checkTextBound(c, l, t, u) : h.inRange(c, t) && h.inRange(c, u) && h.inRange(l, t) && h.inRange(l, u))) && this.fillText(e, n, m)
                        }
                        b.restore()
                    }
                }
            }
        }, d.prototype.parseOptions = function(a) {
            var b = a.options.pieceLabel;
            return b ? (this.chartInstance = a, this.ctx = a.chart.ctx, this.options =
                a.config.options, this.render = b.render || b.mode, this.position = b.position || "default", this.arc = b.arc, this.format = b.format, this.precision = b.precision || 0, this.fontSize = b.fontSize || this.options.defaultFontSize, this.fontColor = b.fontColor || this.options.defaultFontColor, this.fontStyle = b.fontStyle || this.options.defaultFontStyle, this.fontFamily = b.fontFamily || this.options.defaultFontFamily, this.hasTooltip = a.tooltip._active && a.tooltip._active.length, this.showZero = b.showZero, this.overlap = b.overlap, this.images = b.images || [], this.outsidePadding = b.outsidePadding || 2, this.showActualPercentages = b.showActualPercentages || !1, !0) : !1
        }, d.prototype.checkTextBound = function(a, b, f, q) {
            for (var k = this.labelBounds, g = 0; g < k.length; ++g) {
                for (var h = k[g], c = [
                        [a, f],
                        [a, q],
                        [b, f],
                        [b, q]
                    ], e = 0; e < c.length; ++e) {
                    var d = c[e][0],
                        l = c[e][1];
                    if (d >= h.left && d <= h.right && l >= h.top && l <= h.bottom) return !1
                }
                c = [
                    [h.left, h.top],
                    [h.left, h.bottom],
                    [h.right, h.top],
                    [h.right, h.bottom]
                ];
                for (e = 0; e < c.length; ++e)
                    if (d = c[e][0], l = c[e][1], d >= a && d <= b && l >= f && l <= q) return !1
            }
            k.push({
                left: a,
                right: b,
                top: f,
                bottom: q
            });
            return !0
        }, d.prototype.measureText = function(a) {
            return "object" === typeof a ? {
                width: a.width,
                height: a.height
            } : this.ctx.measureText(a)
        }, d.prototype.fillText = function(a, b, d) {
            var f = this.ctx;
            "object" === typeof a ? f.drawImage(a, b.x - a.width / 2, b.y - a.height / 2, a.width, a.height) : (f.fillStyle = d, f.textBaseline = "top", f.textAlign = "center", f.fillText(a, b.x, b.y - this.fontSize / 2))
        }, d.prototype.loadImage = function(a) {
            var b = new Image;
            b.src = a.src;
            b.width = a.width;
            b.height = a.height;
            return b
        }, d.prototype.drawArcText =
        function(a, b, f, d) {
            var k = this.ctx,
                g = f.x,
                h = f.y,
                c = f.startAngle;
            f = f.endAngle;
            k.save();
            k.translate(g, h);
            h = f - c;
            c += Math.PI / 2;
            f += Math.PI / 2;
            var e = c;
            g = this.measureText(a);
            c += (f - (g.width / b + c)) / 2;
            if (d || !(f - c > h))
                if ("string" === typeof a)
                    for (k.rotate(c), d = 0; d < a.length; d++) c = a.charAt(d), g = k.measureText(c), k.save(), k.translate(0, -1 * b), k.fillText(c, 0, 0), k.restore(), k.rotate(g.width / b);
                else k.rotate((e + f) / 2), k.translate(0, -1 * b), this.fillText(a, {
                    x: 0,
                    y: 0
                });
            k.restore()
        }, Chart.pluginService.register({
            beforeInit: function(a) {
                a.pieceLabel =
                    new d
            },
            beforeDatasetsUpdate: function(a) {
                a.pieceLabel.beforeDatasetsUpdate(a)
            },
            afterDatasetsDraw: function(a) {
                a.pieceLabel.afterDatasetsDraw(a)
            }
        }))
})();
var enArray = new Array();
var esArray = new Array();
var resourses = new Array();
enArray['month'] = 'Month';
esArray['month'] = 'Meses';
enArray['minimumPayment'] = 'Minimum Payment';
esArray['minimumPayment'] = 'Pago MÃ­nimo ';
enArray['principalPaid'] = 'Principal Paid';
esArray['principalPaid'] = 'Capital Pagado';
enArray['interestPaid'] = 'Interest Paid';
esArray['interestPaid'] = 'InterÃ©s Pagado';
enArray['remainingBalance'] = 'Remaining Balance';
esArray['remainingBalance'] = 'Balance Restante';
enArray['totalInterest'] = 'Total Interest';
esArray['totalInterest'] = 'InterÃ©s Total';
resourses['en'] = enArray;
resourses['es'] = esArray;
var path = window.location.pathname;
path = path.split('/');
var lang = path[1];
if (lang !== 'es') {
    lang = 'en';
}

function clearTable(tableId) {
    jQuery(tableId).remove();
};

function scrollMobile() {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
        jQuery('html').animate({
            scrollTop: jQuery("#debtcom-calculator-v3 #message").offset().top - 100
        }, 'slow');
    } else {
        jQuery('html, body').animate({
            scrollTop: jQuery("#debtcom-calculator-v3 #message").offset().top - 300
        }, 'slow');
    }
};
jQuery(document).ready(function($) {
    $('#debtcom-calculator-v3 #message,#debtcom-calculator-v3 #table').hide();
    $("#minimum_payment_form label[for='minimum_dollar_amount']").hide();

    function setMinimunPayment() {
        var mPP = parseFloat(jQuery("#current_balance").val()) || 0;
        var mPPP = parseFloat(jQuery("#percentage_of_balance").val()) / 100;
        var mP2 = ((parseFloat(jQuery("#interest_rate").val() || 18) / 12) / 100) * parseFloat(jQuery("#current_balance").val());
        $('#minimum_payment_form label').attr('for', 'minimum_dollar_amount').fadeIn();
        if ($("#percentage_of_balance").val() == "1") {
            var mPPP = parseFloat(1) / 100;
            var mP2 = ((parseFloat(jQuery("#interest_rate").val() || 18) / 12) / 100) * parseFloat(jQuery("#current_balance").val());
            var test2 = Math.floor(((mPP * mPPP) + mP2));
            return populateMinimum(test2);
        } else {
            var test1 = mPPP;
            return miniPaymentPopulate(mPP, test1);
        }
    }
    $('#minimum_payment_form select').on('change', function() {
        setMinimunPayment();
    });
    $('#minimum_payment_form input').on('keyup', function() {
        setMinimunPayment();
    });
});

function hideAll() {
    jQuery('#debtcom-calculator-v3 #message').hide();
    jQuery('#debtcom-calculator-v3 #table').hide();
};

function toComma(x) {
    var tempX = parseFloat(x);
    return tempX.toLocaleString();
}

function hideMe(xs) {
    var tempX = xs
    return jQuery('tempX').hide();
};

function createFutureDate(lastRow) {
    var currentDate = new Date;
    currentDate.setMonth(currentDate.getMonth() + lastRow.month);
    var futureDate = currentDate.toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'long'
    });
    return futureDate;
}

function writeMessage(month, balance, interest, totalInterest, totalTotal, year, yearRemainder, futureDate, intrestPercentage, principlePercentage, sixtyprinciple) {
    jQuery('.js-month').text(toComma(month));
    jQuery('.js-total-balance').text('$' + toComma(balance));
    jQuery('.js-apr').text(interest + '%');
    jQuery('.js-total-interest').text('$' + toComma(totalInterest));
    jQuery('.js-total-total').text('$' + toComma(totalTotal));
    jQuery('.js-total-years').text(toComma(year.toFixed(0)));
    jQuery('.js-months-remainder').text(yearRemainder + ' months');
    jQuery('.js-completion-year').text(futureDate);
    jQuery('.js-principle-percentage').text(principlePercentage + '%');
    jQuery('.js-principle-percentage-width').width(principlePercentage + "%");
    jQuery('.js-interest-percentage').text(intrestPercentage + '%');
    jQuery('.js-interest-percentage-width').width(intrestPercentage + "%");
    jQuery('.js-sixty-percent').text(toComma(sixtyprinciple));
};

function populateMinimum(xf) {
    jQuery('#minimum_payment').text('$' + toComma(xf)).fadeIn();
    jQuery("#minimum_payment").addClass("number-populated");
}

function miniPaymentPopulate(mPP, mPPP) {
    var test2 = Math.floor((mPP * mPPP));
    return populateMinimum(test2);
};

function getSuccessOutput() {
    hideAll();
    var validator = jQuery("#minimum_payment_form").validate({
        debug: false,
        errorClass: "is-invalid",
        errorElement: "div",
        validClass: "is-valid",
        onkeyup: function(element) {
            return jQuery(element).valid();
        },
        errorPlacement: function(error, element) {
            jQuery(element).parent().append(error.addClass("invalid-feedback"));
        }
    });
    validator.element("#current_balance");
    validator.element("#interest_rate");
    var currentBalance = jQuery("#current_balance").val(),
        interestRate = jQuery("#interest_rate").val(),
        minumumDolarAmount = jQuery("#minimum_dollar_amount").val(),
        percentageOfBalance = jQuery("#percentage_of_balance").val();
    if (validator.valid() && currentBalance && interestRate) {
        jQuery('#output_error').text('Loading....');
        jQuery('#output_error').show();
        validator.resetForm();
        validator.reset();
        var url = 'https://calculator-api-bk.debt.com/api/v1/getPayments/' + currentBalance + '/' + interestRate + '/' + percentageOfBalance + '/' + minumumDolarAmount;
        jQuery.ajax({
            method: 'GET',
            url: url,
            success: function(response) {},
            complete: function(response) {
                if (response.status === 200) {
                    jQuery('#output_error').text('');
                    var mydata = eval(response.responseJSON);
                    var lastRow = mydata[mydata.length - 1];
                    var futureDate = createFutureDate(lastRow);
                    var firstRow = mydata[0];
                    var year = parseFloat(lastRow.month) / 12;
                    var yearRemainder = parseFloat(lastRow.month) % 12;
                    var minimumPayment = parseFloat(firstRow.minimumPayment);
                    var totalTotal = parseFloat(parseFloat(currentBalance) + parseFloat(lastRow.totalInterest)).toFixed(2);
                    var interestPercentage = ((parseFloat(lastRow.totalInterest).toFixed(2) / totalTotal) * 100).toFixed(0);
                    var principlePercentage = ((parseFloat(parseFloat(currentBalance)) / totalTotal) * 100).toFixed(0);
                    var sixtyprinciple = (parseFloat(currentBalance) * .60).toFixed(0);
                    writeMessage(lastRow.month, currentBalance, interestRate, lastRow.totalInterest, totalTotal, year, yearRemainder, futureDate, interestPercentage, principlePercentage, sixtyprinciple);
                    populateMinimum(minimumPayment);
                    clearTable('#payment-history');
                    jQuery.makeTable = function(mydata) {
                        var table = jQuery('<table id=\"payment-history\" class=\"table table-bordered table-hover\">');
                        var tblHeader = "<thead class=\"thead-dark\"><tr>";
                        for (var k in mydata[0]) tblHeader += "<th>" + k + "</th>";
                        tblHeader += "</tr></thead>";
                        jQuery(tblHeader).appendTo(table);
                        jQuery.each(mydata, function(index, value) {
                            var TableRow = "<tr>";
                            jQuery.each(value, function(key, val) {
                                TableRow += "<td>" + toComma(val) + "</td>";
                            });
                            TableRow += "</tr>";
                            jQuery(table).append(TableRow);
                        });
                        return (jQuery(table));
                    };
                    var table = jQuery.makeTable(mydata);
                    jQuery(table).appendTo("#table div div");
                    var x = document.getElementById("payment-history").rows[0].cells;
                    x[0].innerHTML = "<div class=\"fixedTop\">" + resourses[lang || 'en']['month'] + "  </div>";
                    x[1].innerHTML = "<div class=\"fixedTop\"> " + resourses[lang || 'en']['minimumPayment'] + " </div>";
                    x[2].innerHTML = "<div class=\"fixedTop\"> " + resourses[lang || 'en']['principalPaid'] + " </div>";
                    x[3].innerHTML = "<div class=\"fixedTop\"> " + resourses[lang || 'en']['interestPaid'] + "</div>";
                    x[4].innerHTML = "<div class=\"fixedTop\"> " + resourses[lang || 'en']['remainingBalance'] + " </div>";
                    x[5].innerHTML = "<div class=\"fixedTop\"> " + resourses[lang || 'en']['totalInterest'] + " </div>";
                    jQuery('#message').fadeIn("slow");
                    jQuery('#table').fadeIn("slow");
                    jQuery('#myChart').fadeIn("slow");
                    scrollMobile();

                    function fixWidth() {
                        var myTbodyW = jQuery("#payment-history tbody tr");
                        var myTbodyC = jQuery("#payment-history tbody tr td");
                        var fixer = myTbodyW.width()
                        jQuery("#payment-history thead").css({
                            "width": fixer + "px",
                        });
                        jQuery("#payment-history thead th").css({
                            "myTbodyC": fixer + "px",
                        });
                    };
                    jQuery(document).ready(function() {
                        jQuery('#payment-history tbody').scroll(function(e) {
                            jQuery('#payment-history thead').css("left", -jQuery("tbody").scrollLeft());
                        });
                        fixWidth();
                        jQuery(window, "#payment-history").on('resize', function() {
                            fixWidth();
                        });
                    });
                }
            },
            error: function(err) {
                var text = 'There was an error, please try again';
                if (!err.responseText.includes('RangeError')) {
                    text = err.responseText;
                }
                jQuery('#output_error').html(text);
                jQuery('#output_error').show();
            },
        });
        return false;
    } else {
        return false;
    }
}
var version = "1.0";
var currentScriptName = 'debtForm.js';

function showThankYou(url) {
    jQuery("#debtform-app").remove();
    var thankyoupage = document.createElement("iframe");
    thankyoupage.setAttribute("class", "bib-thank-you");
    thankyoupage.setAttribute("src", url);
    thankyoupage.setAttribute("security", "restricted");
    thankyoupage.setAttribute("sandbox", "allow-scripts allow-same-origin");
    thankyoupage.setAttribute("height", "0");
    thankyoupage.setAttribute("width", "0");
    thankyoupage.setAttribute("display", "none");
    document.getElementById("bib-wrapper").appendChild(thankyoupage);
    jQuery("#bib-wrapper .success-message").css("display", "block");
}

function buildDebtTypeDropDown() {
    var op_items = '<option selected="selected">Select...</option>';
    jQuery.each(pids, function(index, setting) {
        var _label = "";
        switch (index) {
            case '106':
                _label = "Credit Card Debt";
                break;
            case '109':
                _label = "Student Loan Debt";
                break;
            case '110':
                _label = "Tax Debt";
                break;
            case '108':
                _label = "Credit Correction";
                break;
            case '115':
                _label = "Bankruptcy";
                break;
            case '111':
                _label = "Collector Harassmet";
                break;
        }
        op_items = op_items + ' <option value="' + index + '" label="' + _label + '" >' + _label + '</option> ';
    });
    jQuery("#vt-form-debt-type").html(op_items);
}

/*function getFormValues() {
    jQuery("#debtform-app *").each(function() {
        if (jQuery(this).attr("data-var-name") != "undefined") {
            if (jQuery(this).is("input")) {
                if ((jQuery(this).attr("type") == "text" || jQuery(this).attr("type") == "email" || jQuery(this).attr("type") == "tel" || jQuery(this).attr("type") == "number") && jQuery(this).val() != "" && jQuery(this).val() != null) {
                    formToSend[jQuery(this).attr("data-var-name")] = jQuery(this).val().replace(/ /g, "+");
                } else if ((jQuery(this).attr("type") == "hidden") && jQuery(this).val() != "" && jQuery(this).val() != null) {
                    formToSend[jQuery(this).attr("name")] = jQuery(this).val().replace(/ /g, "+");
                    if (jQuery(this).attr("data-var-name") == "pid") {
                        formToSend["ckm_subid"] = jQuery(this).val().replace(/ /g, "+");
                    } else if (jQuery(this).attr("data-var-name") == "vid-pid") {
                        var var_name = jQuery(this).attr("data-var-name");
                        var value = jQuery(this).val();
                        var slider_value = jQuery("#debtform-app .debt-slider").val() + "000";
                        if (var_name == "vid-pid") {
                            formToSend["vid"] = value;
                            formToSend["pid"] = pids[value];
                            formToSend["ckm_subid"] = pids[value];
                            if (formToSend["vid"] === "106") formToSend["cc_debt_amount"] = slider_value;
                            else if (formToSend["vid"] === "109") formToSend["slc_debt_amount"] = slider_value;
                            else if (formToSend["vid"] === "110") formToSend["tax_debt_amount"] = slider_value;
                            else if (formToSend["vid"] === "115") formToSend["bk_total_debt"] = slider_value;
                        }
                    } else if (jQuery(this).attr("data-var-name") == "vid") {
                        var slider_value = jQuery("#debtform-app .debt-slider").val() + "000";
                        if (jQuery(this).val() === "106") formToSend["cc_debt_amount"] = slider_value;
                        else if (jQuery(this).val() === "109") formToSend["slc_debt_amount"] = slider_value;
                        else if (jQuery(this).val() === "110") formToSend["tax_debt_amount"] = slider_value;
                        else if (jQuery(this).val() === "115") formToSend["bk_total_debt"] = slider_value;
                    }
                } else if (jQuery(this).attr("type") == "radio" && jQuery(this).is(":checked")) {
                    var var_name = jQuery(this).attr("data-var-name");
                    var value = jQuery(this).val();
                    var slider_value = jQuery("#debtform-app .debt-slider").val() + "000";
                    if (var_name == "vid-pid") {
                        formToSend["vid"] = value;
                        formToSend["pid"] = pids[value];
                        formToSend["ckm_subid"] = pids[value];
                        if (formToSend["vid"] === "106") formToSend["cc_debt_amount"] = slider_value;
                        else if (formToSend["vid"] === "109") formToSend["slc_debt_amount"] = slider_value;
                        else if (formToSend["vid"] === "110") formToSend["tax_debt_amount"] = slider_value;
                        else if (formToSend["vid"] === "115") formToSend["bk_total_debt"] = slider_value;
                    }
                }
                if (jQuery(this).attr("type") == "checkbox" && jQuery(this).is(":checked")) {
                    formToSend[jQuery(this).attr("data-var-name")] = 1;
                }
            } else if (jQuery(this).is("textarea")) {
                if (jQuery(this).val() != "" && jQuery(this).val() != null) {
                    formToSend[jQuery(this).attr("data-var-name")] = jQuery(this).val().replace(/ /g, "+");
                }
            } else if (jQuery(this).is("select")) {
                var var_name = jQuery(this).attr("data-var-name");
                if (jQuery(this).children().is(":selected")) {
                    var value = jQuery(this).val();
                    if (var_name == "vid-pid") {
                        formToSend["vid"] = value;
                        formToSend["pid"] = pids[value];
                        formToSend["ckm_subid"] = pids[value];
                    } else if (var_name == "debtFieldName") {
                        if (formToSend["vid"] === "106") formToSend["cc_debt_amount"] = value;
                        else if (formToSend["vid"] === "109") formToSend["slc_debt_amount"] = value;
                        else if (formToSend["vid"] === "110") formToSend["tax_debt_amount"] = value;
                        else if (formToSend["vid"] === "115") formToSend["bk_total_debt"] = value;
                    } else if (value != null && value != "")
                        formToSend[var_name] = value.replace(/ /g, "+");
                }
            }
        }
    });
    getClientIpAddress();
    formToSend["ip_address"] = _ip_address;
    formToSend["apikey"] = _apikey;
    formToSend["source"] = _pagesource.replace(/ /g, "+");
    formToSend["opt_in"] = _opt_in;
    if (ckm_subid_2 != null && ckm_subid_2 != '')
        formToSend["ckm_subid_2"] = ckm_subid_2;
    if (ckm_subid_3 != null && ckm_subid_3 != '')
        formToSend["ckm_subid_3"] = ckm_subid_3;
    if (ckm_subid_4 != null && ckm_subid_4 != '')
        formToSend["ckm_subid_4"] = ckm_subid_4;
    if (ckm_subid_5 != null && ckm_subid_5 != '')
        formToSend["ckm_subid_5"] = ckm_subid_5;
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName, i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var utm_campaign = getUrlParameter('utm_campaign');
    var utm_medium = getUrlParameter('utm_medium');
    var utm_source = getUrlParameter('utm_source');
    if (utm_campaign != null && utm_campaign != '')
        formToSend["utm_campaign"] = utm_campaign;
    if (utm_medium != null && utm_medium != '')
        formToSend["utm_medium"] = utm_medium;
    if (utm_source != null && utm_source != '')
        formToSend["utm_source"] = utm_source;
}*/

function submitFormToMarketing() {
    jQuery(".js-name").text(formToSend["first_name"]);
    window.dataLayer = window.dataLayer || [];
    var resultUrl = _thankYouURL_Unsold;
    var form = formToSend;
    formToSend.primary_phone = formToSend.primary_phone.replace(/[^\d]/g, '');
    var data = {};
    var _data = "";
    for (var prop in form) {
        if (form.hasOwnProperty(prop)) {
            data[prop] = form[prop];
            _data = _data + "&" + prop + "=" + form[prop];
        }
    }
    if (_data != "") {
        _data = "?" + _data.substring(1, _data.length);
    }
    var _url = "https://leadapi.debt.com/createlead.aspx";
    jQuery("#vt-bib-button-2 span").show();
    alert(_data);
    jQuery.ajax({
        url: _url + _data,
        type: "GET",
        jsonp: 'callback',
        jsonpCallback: "jsonpcallback",
        dataType: "jsonp",
        contentType: "application/json; charset=utf-8",
        success: function(json) {
            var data = JSON.parse(json);
            //console.log(data);
            if (data.Status === "Success") {
                console.log(data.Status);
                var success = JSON.parse(data.Cake);
                var buyerId = success.BuyerId;
                var buyerContractId = success.BuyerContractId;
                var mapi_lead_id = data.LeadId;
                var cake_lead_id = success.CakeLeadId;
                var cake_lead_guid = data.LeadGUID;
                jQuery("#res_email").val(formToSend["email"]);
                jQuery("#res_leadguid").val(data.LeadGUID);
                resultUrl = _thankYouURL;
                console.log(resultUrl);
                dataLayer.push({
                    'event': 'LandingPageSubmissionSuccess',
                    'frm.status': data.Status
                });
                if (typeof PassBuyerInfo != 'undefined') {
                    if (PassBuyerInfo) {
                        if (resultUrl.indexOf('?') == -1) {
                            resultUrl = resultUrl + "?BuyerId=" + buyerId + "&BuyerContractId=" + buyerContractId + "&LeadId=" + mapi_lead_id + "&CakeId=" + cake_lead_id;
                        } else {
                            resultUrl = resultUrl + "&BuyerId=" + buyerId + "&BuyerContractId=" + buyerContractId + "&LeadId=" + mapi_lead_id + "&CakeId=" + cake_lead_id;
                        }
                    }
                }
            }
            showThankYou(resultUrl);
        },
        error: function(xhr, status, error) {
            dataLayer.push({
                'event': 'LandingPageSubmissionFailure'
            });
            console.log(resultUrl);
            showThankYou(resultUrl);
            console.warn("Error saving the data:");
            console.warn(error);
        }
    });
}
jQuery(document).ready(function($) {
    if (typeof _getLangCode != 'undefined' && _getLangCode != "es") {
        primary_phone_text_error = "Your 10 digit phone number is required";
        first_name_text_error = "Your first name is required";
        last_name_text_error = "Your last name is required";
        email_text_error = "Your email address is required";
        email_text_error_missing = "Please use this email format: name@domain.com";
        minlength_error = "Please enter at least 2 characters";
    } else {
        primary_phone_text_error = "Su nÃºmero de telÃ©fono debe tener 10 dÃ­gitos";
        first_name_text_error = "Su nombre es obligatorio";
        last_name_text_error = "Su apellido es obligatorio";
        email_text_error = "Su correo electrÃ³nico es obligatorio";
        email_text_error_missing = "Por favor use este formato de correo electrÃ³nico: nombre@dominio.com";
        minlength_error = "Por favor, no escribas menos de 2 caracteres.";
    }
    var msform = $("#msform");
    msform.validate({
        onfocusout: function(element) {
            this.element(element);
        },
        rules: {
            primary_phone: {
                minlength: 14
            },
            radio_debt: {
                required: true
            },
            terms: {
                required: true
            }
        },
        messages: {
            primary_phone: primary_phone_text_error,
            first_name: {
                required: first_name_text_error,
                minlength: minlength_error,
            },
            last_name: {
                required: last_name_text_error,
                minlength: minlength_error,
            },
            email: {
                required: email_text_error,
                email: email_text_error_missing
            },
        },
        errorClass: 'error',
        errorPlacement: function(error, element) {
            if (element.parent('.form-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element.parent());
            }
            if (element.attr('name') == 'number[]') {
                error.insertAfter('#checkboxGroup');
            } else {
                error.appendTo(element.parent());
            }
        },
        submitHandler: function(form) {}
    });

    function is_int(value) {
        if ((parseFloat(value) == parseInt(value)) && !isNaN(value)) {
            return true;
        } else {
            return false;
        }
    }
    $('input.required').keyup(function() {
        var empty = $(this).val() == "";
        if ($(this).val() == "") {
            $(this).next().toggleClass("hideError", !empty).toggleClass("showError", empty);
        }
    });
    $('#msform #zip').keyup(function() {
        var el = $(this);
        if ((el.val().length == 5) && (is_int(el.val()))) {
            $.ajax({
                url: "https://zip.getziptastic.com/v2/US/" + el.val(),
                cache: false,
                dataType: "json",
                type: "GET",
                timeout: 2000,
                success: function(result, success) {
                    $(".zip-error, .instructions").slideUp(200);
                    $(".js-city").text(result.city);
                    $('.zip-code .next').prop('disabled', false);
                },
                error: function(result, success) {
                    if (success === 'timeout') {
                        $(".zip-error, .instructions").slideUp(200);
                        $(".js-city").text('');
                        $('.zip-code .next').prop('disabled', false);
                    } else {
                        $('.zip-error').slideDown(300);
                        $('.zip-code').removeClass("required");
                        $('.showerror').hide();
                        $('#spanEmail').hide();
                        $('.zip-code .next').prop('disabled', true);
                    }
                }
            });
        } else if ((el.val().length < 5) || (el.val().length > 5)) {
            $(".zip-error").slideDown(200);
            $('.zip-code').removeClass("required");
            $(".showerror").hide();
            $("#spanEmail").hide();
            $('.zip-code .next').prop('disabled', true);
        }
    });
    $('.ng-hide').hide();
    $("#vt-bib-button-1").hide();
    $("#vt-bib-button-3").hide();
    $("#vt-form-debt-type").change(function() {
        var value_select = $(this).val();
        $(".bib-button-wrap").show();
        $("#vt-bib-button-1").show();
        $("#vt-bib-button-3").show();
        $(".step2").show();
        $(".step2 .form-group").each(function() {
            if ($(this).attr("date-value").indexOf(value_select) != -1)
                $(this).show();
            else
                $(this).hide();
        });
    });
    var buttonformpressed;
    $('#msform .btn-primary').click(function(e) {
        buttonformpressed = $(this).attr('id');
    });


    $("#contact-vtform1").submit(function(event) {
        event.preventDefault();
       // alert('Inside contact');
       var ajaxurl = "http://debt.mobilytedev.com/wp-admin/admin-ajax.php";
       var firstname = jQuery("#first_name").val();
       var lastname = jQuery("#last_name").val();
       var email = jQuery("#email").val();
       var message = jQuery("#message").val();
       // alert(lastname);

        var FirstName = jQuery("#first_name").attr("aria-invalid");
                var LastName = jQuery("#last_name").attr("aria-invalid");
                var EmailAddress = jQuery("#email").attr("aria-invalid");
                var MessageUser = jQuery("#message").attr("aria-invalid");
                if(FirstName == 'false' && LastName == 'false' && EmailAddress == 'false' && MessageUser == 'false'){
                    jQuery.ajax({
                                url : ajaxurl,
                                type : 'POST',
                                //action: 'multi_step_ajax_handler',
                                data : {'action': 'contact_form_data',firstname: firstname, lastname: lastname, email: email, message: message},
                               success: function(result){
                                    jQuery('.success-message').show();
                                    jQuery('.dc_forms').hide();dc_forms
                                //alert(result);

                               },
                               error: function(result){
                                 alert('error');
                               } 
                            });
                }


    });



    $("#msform").submit(function(event) {
        event.preventDefault();
        if ($(document.activeElement).attr('id') == 'vt-bib-button-1') {
            $(".clientInfo").css("display", "flex");
            $("#vt-bib-button-2").show().removeAttr('disabled');
            $("#debtform-app .form-group").hide();
            $("#vt-bib-button-1").hide();
            $("#div-lead").show();
            $("#form-disclaimer").show();
            $("#form-disclaimer .form-group").each(function() {
                $(this).show();
                if ($('select[name=debt_type]').val() != 108) {
                    $(this).find(".general-disclaimer").show();
                    $(this).find(".credit-correction-disclaimer").hide();
                } else {
                    $(this).find(".general-disclaimer").hide();
                    $(this).find(".credit-correction-disclaimer").show();
                }
            });
            $(".clientInfo .form-group").each(function() {
                $(this).show();
            });
            return false;
        } else if (buttonformpressed == 'vt-bib-button-2') {
            if (msform.valid() === true) {

                //var debt_types = jQuery(".debt-types").attr(radio);
                var radioval = jQuery('input[name=radio]:checked').val();
                //var points = jQuery('.debt-slider').val();
                var late_payments = jQuery("#late-payments").val();
                var bankruptcy = jQuery("#bankruptcy").val();
                var charge_offs = jQuery("#charge-offs").val();               
                var collections = jQuery("#collections").val();
                var other = jQuery("#other").val();
                var js_city = jQuery(".js-city").text();
                var points = jQuery("#asRange-tip").text();
                var payment_status = jQuery('input[name=radiodebt]:checked').val();
                var student_loan = jQuery('input[name=radio-sl]:checked').val();
                var currently_enrolled = jQuery('input[name=radio-tax]:checked').val();
                var first_name = jQuery("#first_name").val();
                var last_name = jQuery("#last_name").val();
                var email = jQuery("#email").val();
                var primary_phone = jQuery("#primary_phone").val();
                var ajaxurl = "http://debt.mobilytedev.com/wp-admin/admin-ajax.php";
                console.log(radioval);                
                console.log(points);                
                console.log(js_city);
                console.log(first_name);
                console.log(last_name);
                console.log(email);
                console.log(primary_phone);
                console.log(payment_status);
                console.log(student_loan);
                console.log(currently_enrolled);

                jQuery.ajax({
                    url : ajaxurl,
                    type : 'POST',
                    //action: 'multi_step_ajax_handler',
                    data : {'action': 'multi_step_ajax_handler',radioval : radioval, points: points, js_city: js_city, first_name: first_name, last_name: last_name, email:email, primary_phone: primary_phone, payment_status: payment_status,student_loan:student_loan,currently_enrolled: currently_enrolled },
                   success: function(result){

                    jQuery('.success-message').show();
                    jQuery('.js-name').html(first_name);
                    jQuery('fieldset.step-5').hide();


                   },
                   error: function(result){
                   // alert('error');
                   } 
                })
                //alert('form is here');
                //getFormValues();
                //console.log(getval);
                //submitFormToMarketing();
                //console.log(getformval);
                return false;
            }
        }
    });
    VariableManagement();
    if (typeof defaultVid != 'undefined' && defaultVid != "") {
        $("#vt-form-debt-type").val(defaultVid);
        $("#vt-form-debt-type").trigger("change");
        if (defaultVid == '108') {
            $("#vt-bib-button-1").show();
            $(".bib-button-wrap").show();
            $(".step2").show();
        }
    }
});
/*!
 * jQuery throttle / debounce - v1.1 - 3/7/2010
 * http://benalman.com/projects/jquery-throttle-debounce-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
var socialWarfarePlugin = socialWarfarePlugin || {};
(function(c, d) {
    var b;
    var a = c.socialWarfarePlugin;
    a.throttle = b = function(g, f, k, h) {
        var i, e = 0;
        if (typeof f !== "boolean") {
            h = k;
            k = f;
            f = d
        }

        function j() {
            var p = this,
                n = +new Date() - e,
                o = arguments;

            function m() {
                e = +new Date();
                k.apply(p, o)
            }

            function l() {
                i = d
            }
            if (h && !i) {
                m()
            }
            i && clearTimeout(i);
            if (h === d && n > g) {
                m()
            } else {
                if (f !== true) {
                    i = setTimeout(h ? l : m, h === d ? g - n : g)
                }
            }
        }
        if (a.guid) {
            j.guid = k.guid = k.guid || a.guid++
        }
        return j
    };
    a.debounce = function(e, f, g) {
        return g === d ? b(e, f, false) : b(e, g, f !== false)
    }
})(this);
(function(h, g, d) {
    var i = h.socialWarfarePlugin;

    function j(n) {
        return parseInt(n, 10)
    }

    function a(o) {
        var n = g.Event(o);
        g(h).trigger(n)
    }
    var l = {};
    socialWarfarePlugin.fetchShares = function() {
        g.when(g.get("https://graph.facebook.com/?fields=og_object{likes.summary(true).limit(0)},share&id=" + swp_post_url), (swp_post_recovery_url ? g.get("https://graph.facebook.com/?fields=og_object{likes.summary(true).limit(0)},share&id=" + swp_post_recovery_url) : "")).then(function(w, v) {
            if ("undefined" !== typeof w[0].share) {
                console.log(w);
                var u = j(w[0].share.share_count);
                var t = j(w[0].share.comment_count);
                if ("undefined" !== typeof w[0].og_object) {
                    var s = j(w[0].og_object.likes.summary.total_count)
                } else {
                    var s = 0
                }
                var n = u + t + s;
                if (swp_post_recovery_url) {
                    console.log(v);
                    if (typeof v[0].share !== "undefined") {
                        var r = j(v[0].share.share_count);
                        var q = j(v[0].share.comment_count)
                    } else {
                        var r = 0,
                            q = 0
                    }
                    if (typeof v[0].og_object !== "undefined") {
                        var o = j(v[0].og_object.likes.summary.total_count)
                    } else {
                        var o = 0
                    }
                    var p = r + q + o;
                    if (n !== p) {
                        n = n + p
                    }
                }
                l = {
                    action: "swp_facebook_shares_update",
                    post_id: swp_post_id,
                    activity: n
                };
                g.post(swp_admin_ajax, l, function(x) {
                    console.log("Facebook Shares Response: " + n)
                })
            }
        })
    };
    i.activateHoverStates = function() {
        a("pre_activate_buttons");
        g(".nc_socialPanel:not(.nc_socialPanelSide) .nc_tweetContainer").on("mouseenter", function() {
            if (g(this).hasClass("swp_nohover")) {} else {
                console.log("fired");
                k();
                var p = g(this).find(".swp_share").outerWidth();
                var o = g(this).find("i.sw").outerWidth();
                var q = g(this).width();
                var n = 1 + ((p + 35) / q);
                g(this).find(".iconFiller").width(p + o + 25 + "px");
                g(this).css({
                    flex: n + " 1 0%"
                })
            }
        });
        g(".nc_socialPanel:not(.nc_socialPanelSide)").on("mouseleave", function() {
            k()
        })
    };

    function k() {
        g(".nc_socialPanel:not(.nc_socialPanelSide) .nc_tweetContainer:not(.swp_nohover) .iconFiller").removeAttr("style");
        g(".nc_socialPanel:not(.nc_socialPanelSide) .nc_tweetContainer:not(.swp_nohover)").removeAttr("style")
    }

    function f() {
        if (g(".nc_wrapper").length) {
            g(".nc_wrapper").remove()
        }
        var q = g(".nc_socialPanel").not('[data-float="float_ignore"]').first();
        var p = g(".nc_socialPanel").index(q);
        var t = q.attr("data-float");
        var u = q.attr("data-align");
        if (t) {
            var o = g(".nc_socialPanel").attr("data-floatColor");
            g('<div class="nc_wrapper" style="background-color:' + o + '"></div>').appendTo("body");
            var n = q.attr("data-float");
            q.clone().appendTo(".nc_wrapper");
            g(".nc_wrapper").hide().addClass((n == "floatLeft" ? "floatBottom" : n));
            var r = q.outerWidth(true);
            var s = q.offset();
            g(".nc_socialPanel").last().addClass("nc_floater").css({
                width: r,
                left: (u == "center" ? 0 : s.left)
            });
            g(".nc_socialPanel .swp_count").css({
                transition: "padding .1s linear"
            });
            g(".nc_socialPanel").eq(0).addClass("swp_one");
            g(".nc_socialPanel").eq(2).addClass("swp_two");
            g(".nc_socialPanel").eq(1).addClass("swp_three")
        }
    }

    function e() {
        var u = g(".nc_socialPanel");
        var C = u.not('[data-float="float_ignore"]').eq(0).attr("data-float");
        var y = g(h);
        var n = y.height();
        var w = g(".nc_wrapper");
        var r = g(".nc_socialPanelSide").filter(":not(.mobile)");
        var s = g(".nc_socialPanel").attr("data-position");
        var o = r.attr("data-screen-width");
        var A = u.eq(0).offset();
        var p = y.scrollTop();
        var B = g(h).scrollTop();
        if (typeof h.swpOffsets == "undefined") {
            h.swpOffsets = {}
        }
        var q = false;
        if (C == "floatLeft") {
            var x = g(".nc_socialPanelSide").attr("data-mobileFloat");
            if (g(".nc_socialPanel").not(".nc_socialPanelSide").length) {
                g(".nc_socialPanel").not(".nc_socialPanelSide, .nc_floater").each(function() {
                    var D = g(this).offset();
                    var E = g(this).height();
                    if (D.top + E > p && D.top < p + n) {
                        q = true
                    }
                });
                if (A.left < 100 || g(h).width() < o) {
                    q = true;
                    if (x == "bottom") {
                        C = "floatBottom"
                    }
                } else {
                    if (q) {
                        q == true
                    } else {
                        q = false
                    }
                }
            } else {
                if (g(h).width() > o) {
                    q = false
                } else {
                    q = true;
                    if (x == "bottom") {
                        C = "floatBottom"
                    }
                }
            }
            var v = r.attr("data-transition");
            if (v == "slide") {
                if (q == true) {
                    r.css({
                        left: "-100px"
                    }, 200)
                } else {
                    r.css({
                        left: "5px"
                    })
                }
            } else {
                if (v == "fade") {
                    if (q == true) {
                        r.fadeOut(200)
                    } else {
                        r.fadeIn(200)
                    }
                }
            }
        }
        if (C == "floatBottom" || C == "floatTop") {
            q = false;
            g(".nc_socialPanel").not(".nc_socialPanelSide, .nc_floater").each(function() {
                var D = g(this).offset();
                var E = g(this).height();
                if (D.top + E > p && D.top < p + n) {
                    q = true
                }
            });
            if (q) {
                w.hide();
                if (C == "floatBottom") {
                    g("body").animate({
                        "padding-bottom": h.bodyPaddingBottom + "px"
                    }, 0)
                } else {
                    if (C == "floatTop") {
                        g("body").animate({
                            "padding-top": h.bodyPaddingTop + "px"
                        }, 0)
                    }
                }
            } else {
                var z, t;
                w.show();
                a("floating_bar_revealed");
                if (C == "floatBottom") {
                    z = h.bodyPaddingBottom + 50;
                    g("body").animate({
                        "padding-bottom": z + "px"
                    }, 0)
                } else {
                    if (C == "floatTop") {
                        t = g(".nc_socialPanel").not(".nc_socialPanelSide, .nc_wrapper .nc_socialPanel").first().offset();
                        if (t.top > p + n) {
                            z = h.bodyPaddingTop + 50;
                            g("body").animate({
                                "padding-top": z + "px"
                            }, 0)
                        }
                    }
                }
            }
        }
    }

    function m() {
        if (0 !== g(".nc_socialPanel").length) {
            f();
            i.activateHoverStates();
            c();
            g(h).scrollTop();
            g(h).scroll(i.throttle(50, function() {
                e()
            }));
            g(h).trigger("scroll");
            g(".nc_socialPanel").css({
                opacity: 1
            })
        }
    }

    function b() {
        var o = {
            wrap: '<div class="sw-pinit" />',
            pageURL: document.URL
        };
        var n = g.extend(o, n);
        g(".swp-content-locator").parent().find("img").each(function() {
            var r = g(this);
            if (r.outerHeight() < swpPinIt.minHeight || r.outerWidth() < swpPinIt.minWidth) {
                return
            }
            var s = false;
            if ("undefined" !== typeof swpPinIt.image_source) {
                s = swpPinIt.image_source
            } else {
                if (r.data("media")) {
                    s = r.data("media")
                } else {
                    if (g(this).attr("data-lazy-src")) {
                        s = g(this).attr("data-lazy-src")
                    } else {
                        if (r[0].src) {
                            s = r[0].src
                        }
                    }
                }
            }
            if (false === s) {
                return
            }
            if (r.hasClass("no_pin")) {
                return
            }
            var u = "";
            if ("undefined" !== typeof swpPinIt.image_description) {
                u = swpPinIt.image_description
            } else {
                if (r.attr("title")) {
                    u = r.attr("title")
                } else {
                    if (r.attr("alt")) {
                        u = r.attr("alt")
                    }
                }
            }
            var q = "http://pinterest.com/pin/create/bookmarklet/?media=" + encodeURI(s) + "&url=" + encodeURI(n.pageURL) + "&is_video=false&description=" + encodeURIComponent(u);
            var t = r.attr("class");
            var p = r.attr("style");
            r.removeClass().attr("style", "").wrap(n.wrap);
            r.after('<a href="' + q + '" class="sw-pinit-button sw-pinit-' + swpPinIt.vLocation + " sw-pinit-" + swpPinIt.hLocation + '">Save</a>');
            r.parent(".sw-pinit").addClass(t).attr("style", p);
            g(".sw-pinit .sw-pinit-button").on("click", function() {
                h.open(g(this).attr("href"), "Pinterest", "width=632,height=253,status=0,toolbar=0,menubar=0,location=1,scrollbars=1");
                if (typeof ga == "function" && true === swpClickTracking) {
                    var v = "pin_image";
                    console.log(v + " Button Clicked");
                    ga("send", "event", "social_media", "swp_" + v + "_share")
                }
                return false
            })
        })
    }

    function c() {
        g(".nc_tweet, a.swp_CTT").off("click");
        g(".nc_tweet, a.swp_CTT").on("click", function(s) {
            if (g(this).hasClass("noPop")) {
                return false
            }
            console.log(g(this));
            if (g(this).attr("data-link")) {
                s.preventDefault ? s.preventDefault() : (s.returnValue = false);
                var p = g(this).attr("data-link");
                console.log(p);
                var o, q, n;
                p = p.replace("â€™", "'");
                if (g(this).hasClass("pinterest") || g(this).hasClass("buffer_link") || g(this).hasClass("flipboard")) {
                    o = 550;
                    q = 775
                } else {
                    o = 270;
                    q = 500
                }
                n = h.open(p, "_blank", "height=" + o + ",width=" + q);
                if (typeof ga == "function" && true === swpClickTracking) {
                    if (g(this).hasClass("nc_tweet")) {
                        var r = g(this).parents(".nc_tweetContainer").attr("data-network")
                    } else {
                        if (g(this).hasClass("swp_CTT")) {
                            var r = "ctt"
                        }
                    }
                    console.log(r + " Button Clicked");
                    ga("send", "event", "social_media", "swp_" + r + "_share")
                }
                return false
            }
        })
    }
    g(h).on("load", function() {
        if ("undefined" !== typeof swpPinIt && swpPinIt.enabled) {
            b()
        }
    });
    g(document).ready(function() {
        c();
        m();
        h.bodyPaddingTop = j(g("body").css("padding-top").replace("px", ""));
        h.bodyPaddingBottom = j(g("body").css("padding-bottom").replace("px", ""));
        var p = false;
        g(".nc_socialPanel").hover(function() {
            p = true
        }, function() {
            p = false
        });
        g(h).resize(i.debounce(250, function() {
            if (g(".nc_socialPanel").length && false !== p) {} else {
                h.swpAdjust = 1;
                m()
            }
        }));
        g(document.body).on("post-load", function() {
            m()
        });
        if (0 !== g(".nc_socialPanelSide").length) {
            var n = g(".nc_socialPanelSide").height();
            var q = g(h).height();
            var o = j((q / 2) - (n / 2));
            setTimeout(function() {
                g(".nc_socialPanelSide").animate({
                    top: o
                }, 0)
            }, 105)
        }
        if (1 === g(".swp-content-locator").parent().children().length) {
            g(".swp-content-locator").parent().hide()
        }
    })
})(this, jQuery);
(function(k) {
    var b = /iPhone/i,
        m = /iPod/i,
        j = /iPad/i,
        o = /(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,
        p = /Android/i,
        l = /(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i,
        h = /(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i,
        g = /Windows Phone/i,
        q = /(?=.*\bWindows\b)(?=.*\bARM\b)/i,
        n = /BlackBerry/i,
        a = /BB10/i,
        c = /Opera Mini/i,
        d = /(CriOS|Chrome)(?=.*\bMobile\b)/i,
        s = /(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,
        i = new RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)", "i");
    var f = function(t, u) {
        return t.test(u)
    };
    var e = function(v) {
        var u = v || navigator.userAgent;
        var t = u.split("[FBAN");
        if (typeof t[1] !== "undefined") {
            u = t[0]
        }
        t = u.split("Twitter");
        if (typeof t[1] !== "undefined") {
            u = t[0]
        }
        this.apple = {
            phone: f(b, u),
            ipod: f(m, u),
            tablet: !f(b, u) && f(j, u),
            device: f(b, u) || f(m, u) || f(j, u)
        };
        this.amazon = {
            phone: f(l, u),
            tablet: !f(l, u) && f(h, u),
            device: f(l, u) || f(h, u)
        };
        this.android = {
            phone: f(l, u) || f(o, u),
            tablet: !f(l, u) && !f(o, u) && (f(h, u) || f(p, u)),
            device: f(l, u) || f(h, u) || f(o, u) || f(p, u)
        };
        this.windows = {
            phone: f(g, u),
            tablet: f(q, u),
            device: f(g, u) || f(q, u)
        };
        this.other = {
            blackberry: f(n, u),
            blackberry10: f(a, u),
            opera: f(c, u),
            firefox: f(s, u),
            chrome: f(d, u),
            device: f(n, u) || f(a, u) || f(c, u) || f(s, u) || f(d, u)
        };
        this.seven_inch = f(i, u);
        this.any = this.apple.device || this.android.device || this.windows.device || this.other.device || this.seven_inch;
        this.phone = this.apple.phone || this.android.phone || this.windows.phone;
        this.tablet = this.apple.tablet || this.android.tablet || this.windows.tablet;
        if (typeof window === "undefined") {
            return this
        }
    };
    var r = function() {
        var t = new e();
        t.Class = e;
        return t
    };
    if (typeof module !== "undefined" && module.exports && typeof window === "undefined") {
        module.exports = e
    } else {
        if (typeof module !== "undefined" && module.exports && typeof window !== "undefined") {
            module.exports = r()
        } else {
            if (typeof define === "function" && define.amd) {
                define("swp_isMobile", [], k.swp_isMobile = r())
            } else {
                k.swp_isMobile = r()
            }
        }
    }
})(this);
! function(a, b) {
    "use strict";

    function c() {
        if (!e) {
            e = !0;
            var a, c, d, f, g = -1 !== navigator.appVersion.indexOf("MSIE 10"),
                h = !!navigator.userAgent.match(/Trident.*rv:11\./),
                i = b.querySelectorAll("iframe.wp-embedded-content");
            for (c = 0; c < i.length; c++) {
                if (d = i[c], !d.getAttribute("data-secret")) f = Math.random().toString(36).substr(2, 10), d.src += "#?secret=" + f, d.setAttribute("data-secret", f);
                if (g || h) a = d.cloneNode(!0), a.removeAttribute("security"), d.parentNode.replaceChild(a, d)
            }
        }
    }
    var d = !1,
        e = !1;
    if (b.querySelector)
        if (a.addEventListener) d = !0;
    if (a.wp = a.wp || {}, !a.wp.receiveEmbedMessage)
        if (a.wp.receiveEmbedMessage = function(c) {
                var d = c.data;
                if (d.secret || d.message || d.value)
                    if (!/[^a-zA-Z0-9]/.test(d.secret)) {
                        var e, f, g, h, i, j = b.querySelectorAll('iframe[data-secret="' + d.secret + '"]'),
                            k = b.querySelectorAll('blockquote[data-secret="' + d.secret + '"]');
                        for (e = 0; e < k.length; e++) k[e].style.display = "none";
                        for (e = 0; e < j.length; e++)
                            if (f = j[e], c.source === f.contentWindow) {
                                if (f.removeAttribute("style"), "height" === d.message) {
                                    if (g = parseInt(d.value, 10), g > 1e3) g = 1e3;
                                    else if (~~g < 200) g = 200;
                                    f.height = g
                                }
                                if ("link" === d.message)
                                    if (h = b.createElement("a"), i = b.createElement("a"), h.href = f.getAttribute("src"), i.href = d.value, i.host === h.host)
                                        if (b.activeElement === f) a.top.location.href = d.value
                            } else;
                    }
            }, d) a.addEventListener("message", a.wp.receiveEmbedMessage, !1), b.addEventListener("DOMContentLoaded", c, !1), a.addEventListener("load", c, !1)
}(window, document);