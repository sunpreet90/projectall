<footer id="colophon" class="site-footer" role="contentinfo">
  <div id="footer-organization-wrapper">
    <div class="footer-main">
      <div class="container-fluid">
        <div class="row">
          <!-- COL LG 4 START HERE -->
          <div class="col-lg-4">
            <h3>Connect</h3><hr>
            <!-- <p><a class="tracking-phone-click" href="tel:+18559969795">
              <span itemprop="telephone" class="tracking-phone">(855) 996-9795</span></a>
            </p>
            <p property="address" typeof="PostalAddress"> 
              <span property="streetAddress">5769 W. Sunrise Blvd.</span><br>
               <span property="addressLocality">Plantation</span>, 
               <span property="addressRegion">FL</span> 
               <span property="postalCode">33313</span>
             </p> -->
            <div class="social-icons">
              <ul>
                <li> 
                  <a class="sm-icon fb" href="#" target="_blank" property="sameAs"> 
                    <span class="icon fb"><i aria-hidden="true" class="fab fa-facebook" title="Facebook Icon"></i></span> 
                    <span class="sr-only">Facebook Icon linking to Facebook Page</span> </a>
                  </li>
                  <li> 
                    <a class="sm-icon twitter" href="#" target="_blank" property="sameAs">
                     <span class="icon twitter"><i aria-hidden="true" class="fab fa-twitter" title="Twitter Icon"></i></span> 
                     <span class="sr-only">Twitter Icon linking to Twitter Page</span> </a>
                  </li>
                    <li> 
                      <a class="sm-icon youtube" href="#" target="_blank" property="sameAs"> <span class="icon youtube"><i aria-hidden="true" class="fab fa-youtube" title="Youtube Icon"></i></span>
                       <span class="sr-only">Youtube Icon linking to Youtube Page</span> </a>
                     </li>
                       <li> 
                        <a class="sm-icon google" href="#" target="_blank" property="sameAs"> 
                          <span class="icon google"><i aria-hidden="true" class="fab fa-google-plus" title="Google Plus Icon"></i></span> 
                          <span class="sr-only">Google Plus Icon linking to Google Plus Page</span> </a>
                      </li>
                        <li> 
                          <a class="sm-icon flipboard" href="#" target="_blank" property="sameAs"> <span class="icon flipboard">
                            <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/icons/flipboard-icon-white.jpg" alt="Flipboard Icon"></span> 
                            <span class="sr-only">Flipboard Icon linking to Flipboard Page</span> </a>
                          </li>
                        <li>
                             <a href="#" target="_blank" property="sameAs" class="sm-icon in"> 
                              <span class="icon in"><i aria-hidden="true" title="Instagram Icon" class="fab fa-instagram"></i></span> 
                              <span class="sr-only">Instagram Icon linking to Instagram Page</span> </a>
                        </li>
                            <li> 
                              <a class="sm-icon pinterest" href="#" target="_blank" property="sameAs"> 
                                <span class="icon pinterest"><i aria-hidden="true" class="fab fa-pinterest" title="Pinterest Icon"></i></span> 
                                <span class="sr-only">Pinterest Icon linking to Pinterest Page</span> </a>
                              </li>
                              <li> 
                                <a class="sm-icon rss" href="/feed/" target="_blank" property="sameAs"> 
                                  <span class="icon rss"><i aria-hidden="true" class="fas fa-rss" title="RSS Icon"></i></span> 
                                  <span class="sr-only">RSS Icon linking to RSS Feed</span> </a>
                                </li>
                              </ul>
                            </div> 
                            <script>// var _depids = 96960;</script>
                             <div id="newsletter-signup" class="newsletter-signup" data-form="main-div">
                              <h3>Sign Up for our Newsletter</h3>
                              <form class="form-inline dc_forms" id="email-vtform" novalidate="novalidate">
                                <div class="form-group"> 
                                  <label for="newsletterEmail" class="sr-only">Email Address</label> 
                                  <input type="email" class="form-control" id="newsletterEmail1" name="newsletterEmail" placeholder="Email Address" data-var-name="email" required="" aria-required="true"> 
                                  <input type="hidden" name="pid" value="96960" data-var-name="pid"></div> 
                                  <button id="email-button" class="btn btn-primary">Subscribe<span class="ng-hide" style="display: none;">&nbsp;<img src="https://www.debt.com/ang/images/ajax-loader.gif" alt="ajax loader"></span>
                                  </button>
                                  <input type="hidden" name="xxTrustedFormToken" id="xxTrustedFormToken_2" value="https://cert.trustedform.com/acef9a398adbcbce6d7a2fe352ab18626d9d58db">
                                  <input type="hidden" name="xxTrustedFormCertUrl" id="xxTrustedFormCertUrl_2" value="https://cert.trustedform.com/acef9a398adbcbce6d7a2fe352ab18626d9d58db"></form> 
                                  <span class="success-message" style="display: none;">Thank you for signing up for the newsletter!</span>
                                  <p class="newsletter-disclaimer"> Sign up for our newsletter to get the latest articles, financial tips, tools, giveaways and advice delivered right to your inbox. <a href="#">Privacy Policy</a></p>
                                </div>  
                                <script>var _getLangCode = 'en';</script> 
                              </div>
                              <!-- COL LG 4 END HERE -->
                              <div class="col-lg-8">
                                <h3>Explore</h3><hr>
                                <div class="nav-column">
                                  <h4 class="blue">Company</h4>
                                  <div class="menu-company-container">
                                    <ul id="menu-company" class="footer-menu">
                                      <li id="menu-item-53582" class="menu-item">
                                        <a href="http://debt.mobilytedev.com/contact-page/">Contact Us</a>
                                      </li>
                                      <li id="menu-item-53583" class="menu-item"><a title="Press Center &amp; Media Contact" href="#">Press Center</a>
                                      </li>
                                      <li id="menu-item-53584" class="menu-item">
                                        <a title="The Scholarship For Aggressive Scholarship Applicants" href="#">Scholarship</a>
                                      </li>
                                      <li id="menu-item-67265" class="menu-item">
                                        <a href="#">Affiliates</a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                <div class="nav-column">
                                  <h4 class="green">Debt Relief</h4>
                                  <div class="menu-debt-relief-container">
                                    <ul id="menu-debt-relief" class="footer-menu">
                                      <li id="menu-item-53714" class="menu-item">
                                        <a href="#">Credit Card Debt Help</a>
                                      </li><li id="menu-item-53587" class="menu-item">
                                        <a href="#">Debt Management</a>
                                      </li>
                                      <li id="menu-item-53591" class="menu-item">
                                        <a href="#">Student Loan Debt Help</a></li>
                                        <li id="menu-item-53595" class="menu-item">
                                          <a href="#">Tax Debt Relief</a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="nav-column">
                                      <h4 class="red">Financial Education</h4>
                                      <div class="menu-financial-education-container">
                                        <ul id="menu-financial-education" class="footer-menu">
                                          <li id="menu-item-55722" class="menu-item">
                                            <a href="#">Credit Card Debt</a></li>
                                            <li id="menu-item-55723" class="menu-item">
                                              <a href="#">Student Loan Debt</a>
                                            </li>
                                              <li id="menu-item-55725" class="menu-item">
                                                <a href="#">Tax Debt</a>
                                              </li>
                                                <li id="menu-item-55726" class="menu-item"><a href="#">Bankruptcy</a></li>
                                              </ul>
                                            </div>
                                          </div>
                                          <div class="nav-column">
                                            <h4 class="blue">News</h4>
                                            <div class="menu-debt-com-news-container">
                                              <ul id="menu-debt-com-news" class="footer-menu">
                                                  <li id="menu-item-53603" class="menu-item">
                                                    <a href="#">Editors and Authors</a>
                                                  </li>
                                                  <li id="menu-item-55721" class="menu-item">
                                                    <a title="Credit &amp; Debt News" href="#">Credit &amp; Debt</a>
                                                  </li>
                                                  <li id="menu-item-55720" class="menu-item">
                                                    <a href="#">Ask the Expert</a></li>
                                                </ul>
                                              </div>
                                            </div>
                                            <div class="credibility-logos"> 
                                              <a href="#" target="_blank"> 
                                                <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/bbb-aplus-logo.png" class="bbb-logo" alt="BBB Logo"> 
                                              </a>
                                                 <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/United-Way-logo.png" alt="United Way">
                                               </div>
                                             </div>
                                           </div>
                                         </div>
                                       </div>
                                       <div class="footer-bottom">
                                        <div class="container-fluid">
                                          <div class="row">
                                            <div class="col-lg-6"> © 2018 <span property="legalName">
                                              <span property="name">Demo</span>®, LLC.</span> All Rights Reserved.</div>
                                            <!-- <div class="col-lg-6 legal-links">
                                              <div class="menu-legal-menu-container">
                                                <ul id="menu-legal-menu" class="footer-menu">
                                                  <li id="menu-item-55727" class="menu-item">
                                                    <a title="About" href="#">About Us</a>
                                                  </li><li id="menu-item-53526" class="menu-item">
                                                    <a href="#">Privacy Policy</a>
                                                  </li>
                                                  <li id="menu-item-53527" class="menu-item">
                                                    <a href="#">Terms &amp; Conditions</a></li>
                                                    <li id="menu-item-75862" class="menu-item">
                                                      <a href="#">Sitemap</a>
                                                    </li>
                                                      <li id="menu-item-53528" class="menu-item">
                                                        <a href="#">Advertising Disclosures</a>
                                                      </li>
                                                      <li id="menu-item-56155" class="menu-item">
                                                        <a href="#">Accessibility</a>
                                                      </li>
                                                      <li id="menu-item-70643" class="menu-item">
                                                        <a href="#">GDPR Compliance</a>
                                                      </li>
                                                    </ul>
                                                  </div>
                                                </div> -->
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </footer><!--/#footer-->

<script type="text/javascript">
    /*jQuery(document).ready(function() {
        jQuery(".next.action-button").click(function(){
            alert('click on next');
            if (jQuery('.step-2.type-of-debt').is(":visible")){
                        current_fs = jQuery('.step-2.type-of-debt');
                        next_fs = jQuery('.step-1.amount-owed');
                    }else if(jQuery('.step-1.amount-owed').is(":visible")){
                        current_fs = jQuery('.step-1.amount-owed');
                        next_fs = jQuery('.step-2.type-of-debt');
                    }
                    
                    next_fs.show(); 
                    current_fs.hide();
    });
    });*/
</script>
<!--     <script src="<?php //echo get_template_directory_uri(); ?>/js/jquery.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/mousescroll.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/smoothscroll.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/jquery.isotope.min.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/jquery.inview.min.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/main.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/plugins.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
    <script src="<?php //echo get_template_directory_uri(); ?>/js/custom.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script>
        jQuery(document).ready(function() {
        jQuery("#example").DataTable();
        } );
    </script> -->
 


<?php wp_footer(); ?>
</body>
</html>