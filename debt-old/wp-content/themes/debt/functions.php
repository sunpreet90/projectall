<?php 

//  To register a logo

$inc_path = TEMPLATEPATH . '/inc/';

function theme_prefix_setup() {
	
	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-width' => true,
	) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

//  To register nav-menus in Wordpress 
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'top-menu' => __( 'Top Menu' ),
      'footer-menu' => (' Footer Menu ')
    )
  );
}
add_action( 'init', 'register_my_menus' );

// To register a widget in wordpress
add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'theme-slug' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 2', 'theme-slug' ),
        'id' => 'sidebar-2', 
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Sidebar 3', 'theme-slug' ),
        'id' => 'sidebar-3',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Recent Sidebar', 'theme-slug' ),
        'id' => 'sidebar-4',
        'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}  
//  End register custom widgets in Wordpress
//   For register  feature image in Wordpress 
add_theme_support( 'post-thumbnails' ); 
// End function here





// For Sub Menu
add_action('wp_enqueue_scripts', 'cssmenumaker_scripts_styles' );

function cssmenumaker_scripts_styles() {  
   wp_enqueue_style( 'cssmenu-styles', get_template_directory_uri() . '/css/styles-submenu.css');
}

class CSS_Menu_Maker_Walker extends Walker {

  var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul>\n";
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $class_names = $value = '';        
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    /* Add active class */
    if(in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
      unset($classes['current-menu-item']);
    }

    /* Check for children */
    $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
    if (!empty($children)) {
      $classes[] = 'has-sub';
    }

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'><span>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</span></a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= "</li>\n";
  }
}

add_action("wp_ajax_multi_step_ajax_handler", "multi_step_ajax_handler");
add_action("wp_ajax_nopriv_multi_step_ajax_handler", "multi_step_ajax_handler");

function multi_step_ajax_handler(){
  global $wpdb;
//print_r($_POST); die;
  $radioval = $_POST['radioval'];
  $points = $_POST['points'];
  //$points = $point; 
  $js_city = $_POST['js_city'];
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $email = $_POST['email'];
  //$email = $_POST['email'];
  $primary_phone = $_POST['primary_phone'];
  $payment_status = $_POST['payment_status'];
  $student_loan = $_POST['student_loan'];
  $currently_enrolled = $_POST['currently_enrolled'];


  $insert_array = array(
        'post_title'    => $first_name,
        'post_type' => 'multistepajax',
        'post_content'=>'Multistep Form Data',
        'post_status' => 'publish',
        'help' => $radioval,
        'zip' => $js_city,
        'first_name' => $first_name, 
        'last_name'=>$last_name,
        'email' => $email,
        'primary_phone' => $primary_phone,            
        'points' => $points,
        //'zip' => $zip,
        'payment_status' => $payment_status,
        'student_loan' => $student_loan,
        'currently_enrolled' => $currently_enrolled,
        'status' => 'pending'

        
         );
    $post_id = wp_insert_post($insert_array);
    $post = get_post($post_id);

    update_post_meta($post_id,'help',$radioval);
    update_post_meta($post_id,'zip',$js_city);
    update_post_meta($post_id,'first_name',$first_name);
    update_post_meta($post_id,'last_name',$last_name);
    update_post_meta($post_id,'email',$email);
    update_post_meta($post_id,'primary_phone',$primary_phone);
    update_post_meta($post_id,'points',$points);
    update_post_meta($post_id,'payment_status',$payment_status);
    update_post_meta($post_id,'student_loan',$student_loan);
    update_post_meta($post_id,'currently_enrolled',$currently_enrolled);
    //echo "<pre>"; print_r($post); die;
   // $table_name = $wpdb->prefix."multistep";

    /*$insert_id = "INSERT INTO `wp_multistep`( `help`, `first_name`, `last_name`, `email`, `primary_phone`, `points`, `zip`,`payment_status`) VALUES ('$radioval','$first_name','$last_name','$email','$primary_phone','$points','$js_city','$payment_status')";
    $wpdb->query($insert_id);*/
    //echo "<pre>"; print_r($insert_id); echo "</pre>"; die; 
   // $insert_id = "INSERT INTO `as_service_request`( `user_id`, `first_name`, `last_name`, `email`, `phone_number`, `address`, `user_type`, `description`, `services`, `status`) VALUES ('$user_id','$first_name','$last_name','$email','$phone','$address','$user_type','$description','avaliable','pending' )";
    //$wpdb-〉query($insert_id);
   
    if($post_id)
    {
       return true;
       //die('True');
    }
    else
    {
        return false;
        //echo "False";
    }
   
    //return true;
    die();





}

add_action("wp_ajax_contact_form_data", "contact_form_data");
add_action("wp_ajax_nopriv_contact_form_data", "contact_form_data");

function contact_form_data(){

    global $wpdb;
    $firstname = $_POST['firstname'];
    $last_name = $_POST['lastname'];
    $email = $_POST['email'];
    $message = $_POST['message'];
  // echo "<pre>";print_r($firstname);
  // die('stop');
    $insert_array = array(
        'post_title'    => $firstname.' '.$last_name,
        'post_type' => 'contact',
        'post_content'=>'',
        'post_status' => 'publish',
        'firstname' => $firstname,
        'lastname' => $last_name,
        'email' => $email, 
        'message'=>$message
         );
    $post_id = wp_insert_post($insert_array);
    $post = get_post($post_id);

    update_post_meta($post_id,'firstname',$firstname);
    update_post_meta($post_id,'lastname',$last_name);
    update_post_meta($post_id,'email',$email);
    update_post_meta($post_id,'message',$message);


     if($post_id)
    {
       
       die('True');
    }
    else
    {
       // return false;
         die("False");
    }


}

?>
