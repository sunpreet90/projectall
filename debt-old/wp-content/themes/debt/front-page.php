<?php 
get_header(); ?>

<style type="text/css">
    .second-range{
        display: none;
    }
</style>
 <div id="content" class="site-content">
  <div class="homepage-hero">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 hero-form">
            <div class="hero-content clearfix"><h1>Overcome Debt, Achieve Good Credit</h1>
            <h2>Don’t let debt and credit problems hold you back from living the life you want!</h2>

            <div id="bib-wrapper" class="form-wrapper clearfix   form-w-next-btn">
                <div class="success-message" style="display: none;">
                    <h2>Congratulations, <span class="js-name"></span>!</h2>
                    <p>We’ve received your request and have matched you with a Trusted Provider that specializes in <span class="js-vertical"></span>. You should receive a call within the next few minutes so you can get connected.  If you are unavailable, a confirmation text will be sent, so connecting at your convenience is quick and easy.  We look forward to assisting you!</p>
                    <input name="res_email" id="res_email" value="" type="hidden">
                    <input name="res_leadguid" id="res_leadguid" value="" type="hidden">
                </div>
                    
        <!------------------------------------------ Fieldset Form Start Here ------------------------------------------------->        
       <div id="debtform-app">
    <form role="form" id="msform" class="vt-form hp-bt lead-form" name="formItem" novalidate="novalidate" autocomplete="off">
        <fieldset class="step-2 type-of-debt required" aria-required="true" style="display: block;">
            <h3>What can we help you with?</h3>
            <div class="debt-types" data-next="1">
                <label>
                    <input type="radio" name="radio" data-var-name="vid-pid" value="106">
                    <div class="credit-cards box"> <span>Credit Card Debt</span></div>
                </label>
                <label>
                    <input type="radio" name="radio" data-var-name="vid-pid" value="109">
                    <div class="student-loans box"> <span>Student Loan Debt</span></div>
                </label>
                <label>
                    <input type="radio" name="radio" data-var-name="vid-pid" value="110">
                    <div class="taxes box"> <span>Back Taxes</span></div>
                </label>
                <label>
                    <input type="radio" name="radio" data-var-name="vid-pid" value="108">
                    <div class="credit-repair box"> <span>Fix My Credit</span></div>
                </label>
            </div> <span class="showerror" style="display: none;">Please select an option</span>
            <input type="button" name="next" class="next action-button btn btn-primary" value="Get Started">
        </fieldset>

        <fieldset class="step-1 amount-owed creditrq" style="display: none;">
            <div class="how-much-you-owe" data-next="1" style="display: none;">
                <h3>Tell us how much you owe.</h3>
                <input class="debt-slider" type="text" min="1,000" max="100,000" value="15" name="points" step="1" data-var-name="debtFieldName" style="display: none;">
                <div class="asRange second-range">
                    <div class="asRange-bar"></div>
                    <div class="asRange-pointer asRange-pointer-1" tabindex="0" style="left: 14.1414%;"><span class="asRange-tip" id="asRange-tip">$15,000</span></div><span class="asRange-selected" style="left: 0px; width: 14.1414%;"></span>
                    <div class="asRange-scale">
                        <ul class="asRange-scale-lines">
                            <li class="asRange-scale-grid" style="left: 0%;"></li>
                            <li style="left: 5%;"></li>
                            <li style="left: 10%;"></li>
                            <li style="left: 15%;"></li>
                            <li style="left: 20%;"></li>
                            <li class="asRange-scale-inlineGrid" style="left: 25%;"></li>
                            <li style="left: 30%;"></li>
                            <li style="left: 35%;"></li>
                            <li style="left: 40%;"></li>
                            <li style="left: 45%;"></li>
                            <li class="asRange-scale-grid" style="left: 50%;"></li>
                            <li style="left: 55%;"></li>
                            <li style="left: 60%;"></li>
                            <li style="left: 65%;"></li>
                            <li style="left: 70%;"></li>
                            <li class="asRange-scale-inlineGrid" style="left: 75%;"></li>
                            <li style="left: 80%;"></li>
                            <li style="left: 85%;"></li>
                            <li style="left: 90%;"></li>
                            <li style="left: 95%;"></li>
                            <li class="asRange-scale-grid" style="left: 100%;"></li>
                        </ul>
                        <ul class="asRange-scale-values">
                            <li style="left: 0%;"><span>1</span></li>
                            <li style="left: 50%;"><span>49.5</span></li>
                            <li style="left: 100%;"><span>100</span></li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="asRange">
                    <div class="asRange-bar"></div>
                    <div class="asRange-pointer asRange-pointer-1" tabindex="0" style="left: 14.1414%;"><span class="asRange-tip">$15,000</span></div><span class="asRange-selected" style="left: 0px; width: 14.1414%;"></span>
                    <div class="asRange-scale">
                        <ul class="asRange-scale-lines">
                            <li class="asRange-scale-grid" style="left: 0%;"></li>
                            <li style="left: 5%;"></li>
                            <li style="left: 10%;"></li>
                            <li style="left: 15%;"></li>
                            <li style="left: 20%;"></li>
                            <li class="asRange-scale-inlineGrid" style="left: 25%;"></li>
                            <li style="left: 30%;"></li>
                            <li style="left: 35%;"></li>
                            <li style="left: 40%;"></li>
                            <li style="left: 45%;"></li>
                            <li class="asRange-scale-grid" style="left: 50%;"></li>
                            <li style="left: 55%;"></li>
                            <li style="left: 60%;"></li>
                            <li style="left: 65%;"></li>
                            <li style="left: 70%;"></li>
                            <li class="asRange-scale-inlineGrid" style="left: 75%;"></li>
                            <li style="left: 80%;"></li>
                            <li style="left: 85%;"></li>
                            <li style="left: 90%;"></li>
                            <li style="left: 95%;"></li>
                            <li class="asRange-scale-grid" style="left: 100%;"></li>
                        </ul>
                        <ul class="asRange-scale-values">
                            <li style="left: 0%;"><span>1</span></li>
                            <li style="left: 50%;"><span>49.5</span></li>
                            <li style="left: 100%;"><span>100</span></li>
                        </ul>
                    </div>
                </div> -->
            </div>
            <div class="credit-repair-question" data-next="3" style="display: none;">
                <h3>What problems are you having with your credit report?</h3>
                <div class="checkbox-group">
                    <div class="check-container">
                        <input data-var-name="cr_late_payments" type="checkbox" id="late-payments" name="late-payments" value="">
                        <label for="late-payments"> <span></span>Late Payments </label>
                    </div>
                    <div class="check-container">
                        <input data-var-name="cr_bankruptcy" type="checkbox" id="bankruptcy" name="bankruptcy" value="">
                        <label for="bankruptcy"> <span></span>Bankruptcy </label>
                    </div>
                    <div class="check-container">
                        <input data-var-name="cr_charge_off" type="checkbox" id="charge-offs" name="charge-offs" value="">
                        <label for="charge-offs"> <span></span>Charge offs </label>
                    </div>
                    <div class="check-container">
                        <input data-var-name="cr_collections" type="checkbox" id="collections" name="collections" value="">
                        <label for="collections"> <span></span>Collections </label>
                    </div>
                    <div class="check-container">
                        <input data-var-name="cr_other" type="checkbox" id="other" name="other" value="">
                        <label for="other"> <span></span>Other </label>
                    </div>
                </div>
            </div> 
			<span class="showerror" style="display: none;">Please select an option</span>
            <!-- <input type="button" name="next" class="next action-button btn btn-primary" value="Get Started"> -->
            <input type="button" name="next" class="next action-button btn btn-primary" value="Next">
        </fieldset>
        <fieldset class="step-3 qualifying-questions-1 required" aria-required="true" style="display: none;">
            <div class="cc-debt-questions" data-next="2" style="display: none;">
                <h3>What is the status of your payments?</h3>
                <div class="tax-debt-types">
                    <label>
                        <input type="radio" name="radiodebt" data-var-name="payment_status" value="Current">
                        <div class="current-cc box"> <span>Current and/or struggling</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radiodebt" data-var-name="payment_status" value="Behind">
                        <div class="behind-less-cc box"> <span>Behind less than 6 months</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radiodebt" data-var-name="payment_status" value="Collections">
                        <div class="behind-more-cc box"> <span>More than 6 months behind</span></div>
                    </label>
                </div>
            </div>
            <div class="tax-debt-questions" data-next="1" style="display: none;">
                <h3>What type of tax debt do you have?</h3>
                <div class="tax-debt-types">
                    <label>
                        <input type="radio" name="radio-tax" data-var-name="tax_debt_type" value="Federal">
                        <div class="federal-tax box"> <span>Federal</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radio-tax" data-var-name="tax_debt_type" value="State">
                        <div class="state-tax box"> <span>State</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radio-tax" data-var-name="tax_debt_type" value="Both">
                        <div class="both-tax box"> <span>Both</span></div>
                    </label>
                </div>
            </div>
            <div class="student-loan-questions" data-next="1" style="display: block;">
                <h3>What type of student loans do you have?</h3>
                <div class="student-loan-types">
                    <label>
                        <input type="radio" name="radio-sl" data-var-name="student_loan_status" value="Federal">
                        <div class="federal-sl box"> <span>Federal</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radio-sl" data-var-name="student_loan_status" value="Private">
                        <div class="private-sl box"> <span>Private</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radio-sl" data-var-name="student_loan_status" value="Both">
                        <div class="both-sl box"> <span>Both</span></div>
                    </label>
                </div>
            </div> <span class="showerror" style="display: none;">Please select an option</span>
            <input type="reset" name="reset" class="reset action-button btn btn-light" value="Start Over">
            <input type="button" name="next" class="next action-button btn btn-primary" value="Next ">
        </fieldset>
        <fieldset class="step-3 qualifying-questions-2 required" aria-required="true" style="display: none;">
            <div class="tax-debt-questions-2" data-next="1" style="display: none;">
                <h3>Are you currently enrolled in a payment program with the IRS?</h3>
                <div class="tax-debt-enrollment">
                    <label>
                        <input type="radio" name="radio-tax" data-var-name="tax_irs_enrolled" value="yes">
                        <div class="yes-tax box"> <span>Yes</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radio-tax" data-var-name="tax_irs_enrolled" value="no">
                        <div class="no-tax box"> <span>No</span></div>
                    </label>
                </div>
            </div>
            <div class="student-loan-questions-2" data-next="1" style="display: block;">
                <h3>What is the status of your loans?</h3>
                <div class="student-loan-status">
                    <label>
                        <input type="radio" name="radio-sl" data-var-name="student_loan_status" value="Current">
                        <div class="current-sl box"> <span>Current</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radio-sl" data-var-name="student_loan_status" value="Behind">
                        <div class="behind-sl box"> <span>Behind</span></div>
                    </label>
                    <label>
                        <input type="radio" name="radio-sl" data-var-name="student_loan_status" value="Deferred">
                        <div class="deferred-sl box"> <span>Deferred</span></div>
                    </label>
                </div>
            </div> <span class="showerror" style="display: none;">Please select an option</span>
            <input type="reset" name="reset" class="reset action-button btn btn-light" value="Start Over">
            <input type="button" name="next" class="next action-button btn btn-primary" value="Next ">
        </fieldset>
        <fieldset class="step-4 zip-code required" aria-required="true" style="display: none;">
            <h3>Enter your zip code.</h3>
            <div class="form-group" data-next="1" style="display: block;">
                <label for="zip" class="sr-only">Zip Code</label>
                <input id="zip" name="zip" data-var-name="zip" class="form-control required" placeholder="Zip Code" required="required" type="text" maxlength="5" autocomplete="nope" aria-required="true"> <span class="showerror" style="display: none;">Please Enter a valid Zip Code</span> <span id="spanEmail">Please Enter a valid Zip Code</span>
                <p class="zip-error">Not a real zip code</p>
            </div>
            <input type="reset" name="reset" class="reset action-button btn btn-light" value="Start Over">
            <input type="button" name="next" class="next action-button btn btn-primary" value="Next">
        </fieldset>
        <fieldset class="processing-step">
            <div class="form-group" data-next="1">
                <h3>Searching...</h3>
                <h4>Searching for availability in <span class="js-city"></span></h4>
                <div class="spinner"><img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/loading.gif" alt="Loading..."></div>
                <h4>Please wait while we find the best solution for you .</h4></div>
        </fieldset>
        <fieldset class="step-5">
            <h3>Good news! We can help!</h3>
            <h4 class="thank-you-variable credit-card" style="display: none;">Provide a few details about yourself so we can set you up with a debt specialist.</h4>
            <h4 class="thank-you-variable tax-debt" style="display: none;">Provide a few details about yourself so we can set you up with a tax specialist.</h4>
            <h4 class="thank-you-variable student-loans" style="display: none;">Provide a few details about yourself so we can set you up with a student loan specialist.</h4>
            <h4 class="thank-you-variable credit-repair" style="display: block;">Provide a few details about yourself so we can set you up with a credit repair specialist.</h4>
            <div class="form-row clientInfo">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name" class="sr-only">First Name</label>
                        <input id="first_name" name="first_name" data-var-name="first_name" class="form-control" placeholder="First Name" required="" type="text" minlength="2" data-ng-pattern="/^[A-Za-z]+$/" maxlength="50" aria-required="true">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="last_name" class="sr-only">Last Name</label>
                        <input id="last_name" name="last_name" data-var-name="last_name" class="form-control" placeholder="Last Name" required="" type="text" minlength="2" aria-required="true">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email" class="sr-only">Email</label>
                        <input id="email" name="email" data-var-name="email" class="form-control" placeholder="Email Address" required="" type="email" aria-required="true">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="primary_phone" class="sr-only">Phone Number</label>
                        <input id="primary_phone" name="primary_phone" data-var-name="primary_phone" class="form-control phone" data-mask="(999) 999-9999" data-pattern="^(\+1)?1?[ \-\.]*\(?([2-9][0-8][0-9])\)?[ \-\.]*([2-9][0-9]{2})[ \-\.]*([0-9]{4})$" placeholder="Phone Number(+91 84525145263)" required="" type="tel" maxlength="14" autocomplete="off" aria-required="true">
                    </div>
                </div>
            </div>
            <input type="reset" name="reset" class="reset action-button btn btn-light" value="Start Over">
            <button type="submit" class="submit action-button btn btn-primary" id="vt-bib-button-2">Contact Me<span>&nbsp;<i class="far fa-spinner fa-spin"></i></span></button>
            <div class="form-disclaimer" id="form-disclaimer">
                <br>
                <p class="general-disclaimer" style="display: none;">By clicking on the "Contact Me" button above, you consent, acknowledge, and agree to the following: Our <a href="https://www.debt.com/terms-conditions/" target="_blank">Terms of Use</a> and <a href="https://www.debt.com/privacy-policy/" target="_blank">Privacy Policy</a>. That you are providing express "written" consent for  or appropriate <a href="https://www.debt.com/privacy-policy/partners/" target="_blank">service provider(s)</a> to call you (autodialing, text and pre-recorded messaging for convenience) via telephone, mobile device (including SMS and MMS - charges may apply depending on your carrier, not by us), even if your telephone number is currently listed on any internal, corporate, state or federal Do-Not-Call list. We take your privacy seriously and you may receive electronic communications, including periodic emails with important news, financial tips, tools and more. You can always unsubscribe at any time. Consent is not required as a condition to utilize services and you are under no obligation to purchase anything.</p>
                <p class="credit-correction-disclaimer" style="display: block;">By clicking on the “Contact me” button above, you consent, acknowledge, and agree to the following: (1)That you are providing express “written” consent for Lexington Law Firm, or appropriate <a href="/privacy-policy/partners/" target="_blank">service provider(s)</a> to call you (including through automated means; e.g. autodialing, text and pre-recorded messaging) via telephone, mobile device (including SMS and MMS – charges may apply), or dialed manually, at my residential or cellular number, even if your telephone number is currently listed on any internal, corporate, state or federal Do-Not-Call list; and (2)Lexington Law’s <a href="https://www.lexingtonlaw.com/info/privacy-policy" target="_blank">Privacy Policy</a> and <a href="https://www.lexingtonlaw.com/info/terms" target="_blank">Terms of Use</a> and <a href="/terms-conditions/" target="_blank">Terms of Use</a>Terms of Use and <a href="/privacy-policy/" target="_blank">Privacy Policy</a>. We take your privacy seriously and you may receive electronic communications, including periodic emails with important news, financial tips, tools and more. You can always unsubscribe at any time. Consent is not required as a condition to utilize Lexington Law or services and you are under no obligation to purchase anything.</p>
            </div>
            <input type="hidden" name="utm_content" id="utm_content" data-var-name="utm_content" value="HomepageNextButtonConcept">
        </fieldset>
        <input type="hidden" name="xxTrustedFormToken" id="xxTrustedFormToken_1" value="https://cert.trustedform.com/db0186b1f9c2393026750ed93308321f29bb5b12">
        <input type="hidden" name="xxTrustedFormCertUrl" id="xxTrustedFormCertUrl_1" value="https://cert.trustedform.com/db0186b1f9c2393026750ed93308321f29bb5b12">
    </form>
    <!-- <div class="credibility-logos clearfix">
        <a href="https://www.bbb.org/south-east-florida/business-reviews/credit-monitoring-protection/debtcom-in-plantation-fl-90080459" target="_blank"> <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/bbb-aplus-logo.png" class="bbb-logo" alt="BBB Logo"> </a> <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/United-Way-logo.png" alt="United Way">
        <a target="_blank" href="https://seal.godaddy.com/verifySeal?sealID=VttdGalb6RNnHGweH1ZPwYmmYt3XSbYeXM2Jk90v88Poa2JxPmkvASdRAOnA"> <img class="security-logo" src="https://www.debt.com/ang/images/godaddySecure.gif" alt="Go Daddy Secure" border="0"> </a>
    </div> -->
</div>

        <!------------------------------------------ Fieldset Form End Here ------------------------------------------------->        


            </div>
            
            </div>
      </div>
    </div>
  </div>
</div>
</div>

   


<?php get_footer();
?>