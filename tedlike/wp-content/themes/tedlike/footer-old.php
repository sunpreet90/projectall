<div class="sticky-footer" style="background-color: pink; margin-top: 20%;">
<footer class="footer-bs">
        <div class="container">
        
        <div class="row">
            <div class="col-md-7 footer-brand animated fadeInLeft">
            <img src="<?php echo home_url(); ?>/wp-content/uploads/2018/06/logo-ted.png" class="img-fluid" alt="LOGO">
               
               
                <div class="col-md-12">
                  
                        <ul id="menu-footer-nav" class="menu pages"><li id="menu-item-364" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-364"><a href="http://tedxtoronto.com/about/">About</a></li>
<li id="menu-item-367" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-367"><a href="http://tedxtoronto.com/news-ideas/">News &amp; Ideas</a></li>
<li id="menu-item-368" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-206 current_page_item menu-item-368"><a href="http://tedxtoronto.com/past-talks/">Past Talks</a></li>
<li id="menu-item-370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-370"><a href="http://tedxtoronto.com/faq/">FAQ</a></li>
<li id="menu-item-371" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-371"><a href="http://tedxtoronto.com/contact/">Contact</a></li>
<li id="menu-item-1401" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1401"><a href="http://tedxtoronto.com/volunteer/">Volunteer</a></li>
</ul>
                  
                </div>
                </div>
            
            <div class="col-md-5 footer-ns animated fadeInRight">
                <h4>Newsletter</h4>
                <p>Sign up to hear about new talks, meetups, and the latest TEDxToronto news.</p>
                <p>
                    </p><div class="input-group">
                      <input type="text" class="form-control" placeholder="Search for...">
                      <span class="input-group-btn">
                        <button class="btn btn-default newsletterbtn" type="button"> Submit</button>
                      </span>
                    </div><!-- /input-group -->
                 <p></p>
            </div>
        </div>
        
            <div class="row"> 
            
            <ul class="social-icons">
    <li>
    <i class="fab fa-facebook-f"></i>
    </li>
    

    <li>
    <i class="fab fa-youtube"></i>
    </li>
    
    
    

    <li>
    <i class="fab fa-twitter"></i>
    </li>
    

    
    <li>
<i class="fab fa-instagram"></i>
    </li>
    
    </ul>
            
            </div>
        
        <div class="row"> 
        <div class="copyright"> 
    <div class="footer-last">
        <div class="footer-copyright">Copyright © TEDxToronto 2018. All Rights Reserved.</div>
        <!-- <div class="footer-twg float-right">Designed and built by <a href="http://www.twg.io" target="_blank" rel="noopener"><img src="http://tedxtoronto.com/wp-content/themes/twg/images/twg.svg" alt="TWG" class="footer-twgLogo"></a></div> -->
      </div>
        
        </div>
        </div>
        
        </div>
    </footer>

</div>


    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/mousescroll.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/smoothscroll.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.isotope.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.inview.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/video.popup.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
$(function(){
    jQuery("#video").videoPopup();
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = $("#cartoonVideo").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#myModal").on('hide.bs.modal', function(){
        $("#cartoonVideo").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#myModal").on('show.bs.modal', function(){
        $("#cartoonVideo").attr('src', url);
    });
});
</script>


    <script>
        jQuery(document).ready(function() {
        jQuery("#example").DataTable();
        } );
    </script>
    <script type="text/javascript">
    
        $(window).load(function() {
        
            $("#flexiselDemo3").flexisel({
                visibleItems: 6,
                itemsToScroll: 1,         
                autoPlay: {
                    enable: true,
                    interval: 5000,
                    pauseOnHover: true
                }        
            });

            $("#flexiselDemo4").flexisel({
                infinite: false     
            });    

        });
</script>
<script type="text/javascript">

    var worksgrid   = $('#works-grid'),
            worksgrid_mode;

        if (worksgrid.hasClass('works-grid-masonry')) {
            worksgrid_mode = 'masonry';
        } else {
            worksgrid_mode = 'fitRows';
        }

        worksgrid.imagesLoaded(function() {
            worksgrid.isotope({
                layoutMode: worksgrid_mode,
                itemSelector: '.work-item'
            });
        });
    $('#filters a').click(function() {
        
            $('#filters .current').removeClass('current');
            $(this).addClass('current');
            var selector = $(this).attr('data-filter');

            worksgrid.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            return false;
        });

</script>

<script> 
$(document).ready(function(){
$( "li.menu-item" ).hover(function() {  // mouse enter
    $( this ).find( " > .sub-menu" ).show(); // display immediate child

}, function(){ // mouse leave
    if ( !$(this).hasClass("current_page_item") ) {  // check if current page
        $( this ).find( ".sub-menu" ).hide(); // hide if not current page
    }
});
});
</script>

<?php wp_footer(); ?>
</body>
</html>