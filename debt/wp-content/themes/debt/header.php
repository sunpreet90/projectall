<?php
$url =  admin_url('admin-ajax.php');
?>
<!DOCTYPE html>
<html>
<head>
<head><meta charset="UTF-8">
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/page-api.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#44accd">
  <meta name="apple-mobile-web-app-title" content="Debt.com">
  <meta name="application-name" content="Debt.com">
  <meta name="theme-color" content="#ffffff">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Cache-Control" content="max-age=0, public">
  <meta http-equiv="Expires" content="-1">
  <link rel="dns-prefetch" href="//static.ads-twitter.com" pr="1.0">
  <link rel="dns-prefetch" href="//tracking.aimediagroup.com" pr="1.0">
  <link rel="dns-prefetch" href="//dist.routingapi.com" pr="1.0">
  <link rel="dns-prefetch" href="//api.routingapi.com" pr="1.0">
  <link rel="dns-prefetch" href="//cdn.taboola.com" pr="1.0">
  <link rel="dns-prefetch" href="//trc.taboola.com" pr="1.0">
  <link rel="dns-prefetch" href="//stats.g.doubleclick.net" pr="1.0">
  <link rel="dns-prefetch" href="//js-agent.newrelic.com" pr="1.0">
  <link rel="dns-prefetch" href="//builder-assets.unbounce.com" pr="1.0">
  <link rel="dns-prefetch" href="//bat.bing.com" pr="1.0">
  <link rel="dns-prefetch" href="//abc84c1452a446328be5a8141b812b7f.events.ubembed.com" pr="1.0">
  <link rel="dns-prefetch" href="//c.liadm.com" pr="1.0">
  <link rel="dns-prefetch" href="//bam.nr-data.net" pr="1.0">
  <link rel="dns-prefetch" href="//us-u.openx.net" pr="1.0">
  <link rel="dns-prefetch" href="//usage.trackjs.com" pr="1.0">
  <link rel="dns-prefetch" href="//tpc.googlesyndication.com" pr="1.0">
  <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" pr="1.0">
  <link rel="preconnect" href="https://api.routingapi.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://dist.routingapi.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://tracking.aimediagroup.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://cdn.taboola.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://trc.taboola.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://stats.g.doubleclick.net" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://abc84c1452a446328be5a8141b812b7f.events.ubembed.com" crossorigin="crossorigin" pr="1.0"><link rel="preconnect" href="https://js-agent.newrelic.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://c.liadm.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://bam.nr-data.net" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://us-u.openx.net" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://usage.trackjs.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://tpc.googlesyndication.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://pagead2.googlesyndication.com" crossorigin="crossorigin" pr="1.0">
  <link rel="preconnect" href="https://builder-assets.unbounce.com" crossorigin="crossorigin" pr="1.0">
  <meta name="msvalidate.01" content="8FE8084725595B2F93F49B41AA21F570">
  <meta name="google-site-verification" content="rF5JYoBa0BTq8Sk-b-oLpV-rC2rvGuOY3SARwB3_V9M"><script type="application/javascript">/* Setup our tracking queues */
    window.retQ = window.retQ || { q: {}, tags : function( args ) { for (var tag in args) { this.q[tag] = args[tag]; } }, empty: function() { return Object.keys(this.q).length === 0; }, to_tags: function() { var ret = this.q; this.q = {}; return ret; }};
    window.dataLayer = window.dataLayer || [];</script> <script type="application/javascript" data-cfasync="false" id="vt-bib-settings">var aid = 1124;
    var phone_pid = 96326;
    var verticalSettings = [{ vertical: 1, pid: 95124 }, { vertical: 2, pid: 95125 }, { vertical: 3, pid: 95126 }, { vertical: 4, pid: 95127 }, { vertical: 5, pid: 97429 }, { vertical: 6, pid: 97430 }];
    var default_pids = { 106: 95124, 109: 95125, 110: 95126, 108: 95127, 115: 97429, 111: 97430 };
    var thankYou='/thankyou/debt-com-sidebar-thank/';
    if (typeof pids === 'undefined') {
        var pids = default_pids;
    } else {
        for (var vid in default_pids) {
            if (default_pids.hasOwnProperty(vid) && !pids.hasOwnProperty(vid)) {
                pids[vid] = default_pids[vid];
            }
        }
    }
    /* Add B Values if present - move this from Enligh only to both once setup */
  var bib_slug = (typeof location.search.split("b=")[1] !== 'undefined') ? location.search.split("b=")[1].split("&")[0] : '';
  bib_slug = ((bib_slug == '') && (typeof document.cookie.split("b=")[1] !== 'undefined')) ? document.cookie.split("b=")[1].split(";")[0] : bib_slug;
  if ((typeof location.search.split("b=")[1] === 'undefined') && ((typeof location.search.split("s2=")[1] !== 'undefined') || (typeof location.search.split("s3=")[1] !== 'undefined') || (typeof location.search.split("s4=")[1] !== 'undefined') || (typeof location.search.split("s5=")[1] !== 'undefined'))) { bib_slug = ''; }
    if (bib_slug != '') {
        var bloader = document.createElement('script');
        bloader.src = 'https://www.debt.com'+'/bib/'+bib_slug+'/?brand=1';
        bloader.type = "application/javascript";
        bloader.async = true;
        bloader.defer = true;
        document.head.appendChild(bloader);
    }
    retQ.tags({ 'aid': aid, 'publisher_id': aid, 'pid': phone_pid, 'sub_id': phone_pid });
  </script> 
    
<title>Debtsite</title>
<link rel="alternate" href="https://www.edebt.com/" hreflang="x-default">
<link rel="alternate" hreflang="en" href="https://www.edebt.com/">
<link rel="alternate" hreflang="es" href="https://www.edebt.com/es/"> 
<style>
@font-face {font-family: "sw-icon-font";src:url("https://keycdn.debt.com/wp-content/plugins/social-warfare/fonts/sw-icon-font.eot?ver=2.3.5");src:url("https://keycdn.debt.com/wp-content/plugins/social-warfare/fonts/sw-icon-font.eot?ver=2.3.5#iefix") format("embedded-opentype"),url("https://keycdn.debt.com/wp-content/plugins/social-warfare/fonts/sw-icon-font.woff?ver=2.3.5") format("woff"), url("https://keycdn.debt.com/wp-content/plugins/social-warfare/fonts/sw-icon-font.ttf?ver=2.3.5") format("truetype"),url("https://keycdn.debt.com/wp-content/plugins/social-warfare/fonts/sw-icon-font.svg?ver=2.3.5#1445203416") format("svg");font-weight: normal;font-style: normal;}
</style>
<meta name="description" content="Struggling to get out of edebt? Don’t let edebt and credit problems hold you back from living the life you want! Find debt relief today."><link rel="canonical" href="https://www.debt.com/"> 
<link rel="publisher" href="https://plus.google.com/+DebtDotCom/">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:title" content="edebt.com: Do-It-Yourself Guidance plus Professional edebt Help">
<meta property="og:description" content="Struggling to get out of edebt? Don’t let edebt and credit problems hold you back from living the life you want! Find edebt relief today.">
<meta property="og:url" content="https://www.edebt.com/">
<meta property="og:site_name" content="eDebt.com">
<meta property="fb:app_id" content="336281840103306">
<meta property="og:image" content="<?php echo site_url(); ?>/wp-content/themes/debt/images/banner.jpg">
<meta property="og:image:secure_url" content="<?php echo site_url(); ?>/wp-content/themes/debt/images/banner.jpg">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="628">
<meta property="og:image:alt" content="eDebt.com Brand Collage">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:description" content="Struggling to get out of edebt? Don’t let edebt and credit problems hold you back from living the life you want! Find debt relief today.">
<meta name="twitter:title" content="eDebt.com: Do-It-Yourself Guidance plus Professional eDebt Help">
<meta name="twitter:site" content="@debtcom">
<meta name="twitter:image" content="<?php echo site_url(); ?>/wp-content/themes/debt/images/banner.jpg">
<meta name="twitter:creator" content="@debtcom">
<meta name="msvalidate.01" content="8FE8084725595B2F93F49B41AA21F570">
<meta name="google-site-verification" content="rF5JYoBa0BTq8Sk-b-oLpV-rC2rvGuOY3SARwB3_V9M">
<link rel="dns-prefetch" href="//www.edebt.com">
<link rel="dns-prefetch" href="//pro.fontawesome.com">
<link rel="dns-prefetch" href="//s.w.org">
<link rel="alternate" type="application/rss+xml" title="Debt.com » Feed" href="https://www.debt.com/feed/">
<link rel="alternate" type="application/rss+xml" title="Debt.com » Comments Feed" href="https://www.debt.com/comments/feed/">
<style id="ez-toc-inline-css" type="text/css">
div#ez-toc-container p.ez-toc-title {font-size: 120%;}
div#ez-toc-container p.ez-toc-title {font-weight: 500;}
div#ez-toc-container ul li {font-size: 95%;}
div#ez-toc-container {background: #fff;border: 1px solid #ffffff;}
div#ez-toc-container p.ez-toc-title {color: #464646;}
div#ez-toc-container ul.ez-toc-list a {color: #26748b;}
div#ez-toc-container ul.ez-toc-list a:hover {color: #2d8ca8;}
div#ez-toc-container ul.ez-toc-list a:visited {color: #26748b;}
</style>
<link rel="stylesheet" id="fontawesome-css" href="https://pro.fontawesome.com/releases/v5.4.1/css/all.css?ver=5.4.1" type="text/css" media="all"> 
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles.css" type="text/css"> 
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom-animation.css" type="text/css"> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script> 
<script type="application/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.min.js" defer="defer"></script> 
<script type="application/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-migrate.min.js" defer="defer"></script> 
<script type="application/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.unobtrusive-ajax.min.js" defer="defer"></script>
<script type="application/javascript" src="https://keycdn.debt.com/wp-content/themes/vt_parent/js/bootstrap.min.js?ver=4.0.0" defer="defer"></script> 
<script type="application/javascript" src="https://keycdn.debt.com/wp-content/themes/Dcom/js/debt-js/jquery-asRange.js?ver=4.19.1" defer="defer"></script>
 <link rel="https://api.w.org/" href="https://www.debt.com/wp-json/"><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.debt.com/xmlrpc.php?rsd">
 <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://keycdn.debt.com/wp-includes/wlwmanifest.xml"><meta name="generator" content="WordPress 4.9.8">
 <link rel="shortlink" href="https://www.debt.com/">
 <link rel="alternate" type="application/json+oembed" href="https://www.debt.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.debt.com%2F">
 <link rel="alternate" type="text/xml+oembed" href="https://www.debt.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.debt.com%2F&amp;format=xml">
 <meta name="generator" content="WPML ver:3.9.3 stt:1,2;">
 <link rel="alternate" type="application/json" title="JSON Feed" href="https://www.debt.com/feed/json/">
 <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
 <script> 
 var spPicTest = document.createElement( "picture" );
 if(!window.HTMLPictureElement && document.addEventListener) {window.addEventListener("DOMContentLoaded", function() {
  var scriptTag = document.createElement("script");
  scriptTag.src = "https://keycdn.debt.com/wp-content/plugins/shortpixel-image-optimiser/res/js/picturefill.min.js";document.body.appendChild(scriptTag);});} 
  </script> 
  
  <meta id="focus-keyword" name="focus_keyword" content="debt"> 
 <!--  <script type="text/javascript" src="https://keycdn.debt.com/wp-content/uploads/resources/js/aggregated_c94d0442f372f4daedbc269d2392e83b.js">
  </script> -->
  <style type="text/css">
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script type="text/javascript">
    //var ajaxurl = "<?php //echo admin_url('admin-ajax.php'); ?>";
    jQuery(document).ready(function(){
      jQuery('#primary_phone').mask('(000) 000-0000');
      jQuery('#phone_1').mask('(000) 000-0000');
      jQuery('#phone_2').mask('(000) 000-0000');
      jQuery('#phone_3').mask('(000) 000-0000');
      jQuery('#phone_4').mask('(000) 000-0000');
    });
</script>
<style type="text/css">
  .logo h1 {
    margin-left: 60px;
    color: #000;
    text-decoration: none;

}
.bxc.bx-brand-9693 .bx-row-line-logo > :first-child
{
  background-image: url('../wp-content/themes/debt/images/logo.png') !important;
  height:55px !important;
}
.zip-error, .thank-you-variable.credit-card,.thank-you-variable.student-loans,.thank-you-variable.tax-debt,.thank-you-variable.credit-repair,.credit-repair.form-group >h3,.form-group > h4, .success-message h2, .success-message p{
  color:#fff !important;
  font-weight: bold !important;
}
#bx-campaign-871065, #bx-campaign-871064
{
  display:none !important;
}
.col-md-offset-6.logo
{
  text-align:center;
}
.next .action-button
{
  font-size:25px !important;
}
</style>



<?php wp_head(); ?>  
</head>

<body class="home page-template page-template-front-page page-template-front-page-php page page-id-2 lang-en group-blog" cz-shortcut-listen="true">

<!-- add code for default popup -->


<!-- Modal -->
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="myModal" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body bd-example-modal-lg">
            <div class="text-center logo"><img src="/wp-content/uploads/2019/02/logo-1-1.png" /></div>
            <div id="newform-app">
               <form role="form" id="msform" class="vt-form hp-bt lead-form" name="formItem"  autocomplete="off">
                  <div class="text-center bar" style="display: none;"> <i>25% Complete</i></div>
                  <div class="progress" style="display: none;">
                     <div class="progress-bar progress-bar-success" role="progressbar" data-valuenow="0">
                     </div>
                  </div>
                  <fieldset class="type-of-debt main-debt-menus" id="debt-credit-type" aria-required="true" style="display: block;">
                     <h3>What do you need help with?</h3>

                     <div class="debt-types" data-next="1">
                        <label>
                           <input type="radio" name="card_details"   value="106">
                           <div its-value='106' target-class=".credit-card-debt-steps" class="credit-cards box debt_options"> <span>Credit Card debt</span></div>
                        </label>
                        <label>
                           <input type="radio" name="card_details"   value="109">
                           <div its-value='109' target-class=".student-loans-debt-steps" class="student-loans box debt_options"> <span>Student Loan debt</span></div>
                        </label>
                        <label>
                           <input type="radio" name="card_details"   value="110">
                           <div its-value='110' target-class=".taxes-debt-steps" class="taxes box debt_options"> <span>Back Taxes</span></div>
                        </label>
                        <label>
                           <input type="radio" name="card_details"   value="108">
                           <div its-value='108' target-class=".credit-repair-debt-steps" class="credit-repair box debt_options"> <span>Fix My Credit</span></div>
                        </label>
                     </div>
                     <div class="text-center hide_type-of-debt">
                   <p> No thanks, I don't need help with debt today.</p>
                     </div>
                     <!-- <span class="showerror" style="display: none;">Please select an option</span>
                        <input type="button" name="next" class="next action-button btn btn-primary" value="Get Started"> -->
                  </fieldset>
                  <fieldset class="credit-card-debt-steps" data-step="1" data-progress="25" style="display: none;">
                     <div class="how-much-you-owe" data-next="1">
                        <div class="vw_100 text-center range-text">
                           <span>
                              To Get Started,
                              <h3>Tell us how much you owe.</h3>
                              and payment status 
                           </span>
                        </div>
                        <div class="vw_50 bx-select">
                           <select class="select_cont bx-selectelem bx-input" id="bx-element_1" name="debt_amount" aria-required="true">
                              <option value="">Debt Amount</option>
                              <option value="2500">$1000 - $5000</option>
                              <option value="6500">$5000 - $8000</option>
                              <option value="9000">$8000 - $10000</option>
                              <option value="12500">$10000 - $15000</option>
                              <option value="16500">$15000 - $20000</option>
                              <option value="25000">$20000 - $30000</option>
                              <option value="35000">$30000 - $40000</option>
                              <option value="45000">$40000 - $50000</option>
                              <option value="50000">$50000+</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_50 bx-select">
                           <select class="select_cont bx-selectelem bx-input" id="bx-payment" name="payment_status" aria-required="true">
                              <option value="">Payment Status</option>
                              <option value="Current">Current and/or just falling behind</option>
                              <option value="Behind">Behind less than 6 months</option>
                              <option value="Collections">More than 6 months behind</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_100 btn_cont">
                           <input type="button" class="next2 action-button2 btn btn_green" id="click_submit" value="Get Started">
                           <span class="showerror" style="display: none;">Please select an option</span>
                           <input type="button" name="next" class="hide_type-of-debt btn btn_boder" value="No thnaks, I'm not ready to get started">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="credit-card-debt-steps" data-step="2" data-progress="70" style="display: none;">
                     <div class="how-much-you-owe" data-next="2">
                        <div class="vw_100 text-center">
                           <span>
                              To Get Started,
                              <h3>Tell us how much you owe.</h3>
                              and payment status 
                           </span>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="fname_1" name="firstname" placeholder ="First Name" required>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="lname_1" name="lastname" placeholder="Last Name" required>
                        </div>
                        <div class="vw_50">
                           <input type="email" class="btn_greenboder" id="email_1" name="email" placeholder="Email" required>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="zip_code_1" name="zip-code" placeholder="Zip Code" required>
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green" value="Next Step">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="credit-card-debt-steps" data-step="3" data-progress="99" style="display: none;">
                     <div class="how-much-you-owe" data-next="3">
                        <div class="vw_100 text-center">
                           <span>
                              Last Step!,
                              <h3><strong>Let Us Know How To Reach You</strong></h3>
                              Our team will reach out for 24 hours. 
                           </span>
                        </div>
                        <div class="vw_100">
                           <input type="tel" class="btn_greenboder" id="phone_1" name="phone-number" data-mask="(000) 000-0000" data-pattern="^(\+1)?1?[ \-\.]*\(?([2-9][0-8][0-9])\)?[ \-\.]*([2-9][0-9]{2})[ \-\.]*([0-9]{4})$" placeholder="Phone Number">
                        </div>
                        <div class="vw_100">
                           <input type="submit" name="next"  id="submit" class="form_class next2 action-button2 btn btn_green" value="Get Connected">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="credit-card-debt-steps" data-step="4" data-progress="100" style="display: none;">
                     <div class="how-much-you-owe" data-next="4">
                        <div class="vw_100 text-center">
                           <span>
                              <i>Look like we already have your information!</i>
                              <h3>Call Us Directly At</h3>
                              to talk with a specialist for free.
                           </span>
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green final" value="Continue to site">
                        </div>
                     </div>
                  </fieldset>
                  <!----------------------------START STUDENT LOAN---------------------------->
                  <fieldset class="student-loans-debt-steps" data-step="1" data-progress="25" style="display: none;">
                     <div class="how-much-you-owe" data-next="1">
                        <div class="vw_100 text-center range-text">
                           <span>
                              To Get Started,
                              <h3>Tell us your Loan Type.</h3>
                              and payment amout & status 
                           </span>
                        </div>
                        <div class="vw_50 bx-select">
                           <select class="select_cont bx-selectelem bx-input" id="bx-loan" name="loan_type" aria-required="true">
                              <option value="">Type of Loan</option>
                              <option value="Federal Loans">Federal Loans</option>
                              <option value="Private Loans">Private Loans</option>
                              <option value="Both">Both</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_50 bx-select">
                           <select class="select_cont bx-selectelem bx-input bx-element" id="bx-element_2" name="debt_amount" aria-required="true">
                              <option value="">Debt Amount</option>
                              <option value="2500">$1000 - $5000</option>
                              <option value="6500">$5000 - $8000</option>
                              <option value="9000">$8000 - $10000</option>
                              <option value="12500">$10000 - $15000</option>
                              <option value="16500">$15000 - $20000</option>
                              <option value="25000">$20000 - $30000</option>
                              <option value="35000">$30000 - $40000</option>
                              <option value="45000">$40000 - $50000</option>
                              <option value="50000">$50000+</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_100 bx-select">
                           <select class="select_cont bx-selectelem bx-input" id="bx-status" name="status_of_payment" aria-required="true">
                              <option value="">Status of Payment</option>
                              <option value="Current">Current</option>
                              <option value="Behind">Behind</option>
                              <option value="Deferred">Deferred</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_100 btn_cont">
                           <input type="button" class="next2 action-button2 btn btn_green" id="click_submit" value="Get Started">
                           <span class="showerror" style="display: none;">Please select an option</span>
                           <input type="button" name="next" class="hide_type-of-debt btn btn_boder" value="No thnaks, I'm not ready to get started">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="student-loans-debt-steps" data-step="2" data-progress="70" style="display: none;">
                     <div class="how-much-you-owe" data-next="2">
                        <div class="vw_100 text-center">
                           <h3>Tell us more about you</h3>
                           <span>We'll use this to connect you with a specialist for free </span>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="fname_2" name="firstname" placeholder ="First Name">
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="lname_2" name="lastname" placeholder="Last Name">
                        </div>
                        <div class="vw_50">
                           <input type="email" class="btn_greenboder" id="email_2" name="email" placeholder="Email">
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="zip_code_2" name="zip-code" placeholder="Zip Code">
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green" value="Next Step">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="student-loans-debt-steps" data-step="2" data-progress="99" style="display: none;">
                     <div class="how-much-you-owe" data-next="3">
                        <div class="vw_100 text-center">
                           <span>
                              Last Step!,
                              <h3><strong>Let Us Know How To Reach You</strong></h3>
                              Our team will reach out for 24 hours. 
                           </span>
                        </div>
                        <div class="vw_100">
                           <input type="tel" class="btn_greenboder" id='phone_2' name="phone-number" data-mask="(000) 000-0000" data-pattern="^(\+1)?1?[ \-\.]*\(?([2-9][0-8][0-9])\)?[ \-\.]*([2-9][0-9]{2})[ \-\.]*([0-9]{4})$" placeholder="Phone Number">
                        </div>
                        <div class="vw_100">
                           <input type="submit" name="next"  id="submit" class="form_class next2 action-button2 btn btn_green" value="Get Connected">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="student-loans-debt-steps" data-step="4" data-progress="100" style="display: none;">
                     <div class="how-much-you-owe" data-next="4">
                        <div class="vw_100 text-center">
                           <span>
                              <i>Look like we already have your information!</i><br>Call Us Directly At
                              <h3>(855) 712-6472</h3>
                              to talk with a specialist free.
                           </span>
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green final" value="Continue to site">
                        </div>
                     </div>
                  </fieldset>
                  <!----------------------------END STUDENT LOAN---------------------------->
                  <!----------------------------START taxes LOAN---------------------------->
                  <fieldset class="taxes-debt-steps" data-step="2" data-progress="70" style="display: none;">
                     <div class="how-much-you-owe" data-next="2">
                        <div class="vw_100 text-center">
                           <span>Get free professional help to
                           <h3>Tell us more about you</h3>
                           <span>and end problems with back taxes</span>
                        </div>
                        <div class="vw_50">
                           <input type="email" class="btn_greenboder" id="email_3" name="email" placeholder="Email" required>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="zip_code_3" name="zip-code" placeholder="Zip Code">
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green" value="Get Out Of Debt">
                           <input type="button" name="hide" class="hide_type-of-debt btn btn_boder" value="I'm not ready to get started">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="taxes-debt-steps" data-step="3" data-progress="99" style="display: none;">
                     <div class="how-much-you-owe" data-next="3">
                        <div class="vw_100 text-center">
                           <h3>Tell us more about you</h3>
                           <span>We'll provide these details to your debt specialist</span>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="fname_3" name="firstname" placeholder ="First Name">
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="lname_3" name="lastname" placeholder="Last Name">
                        </div>
                        <div class="vw_50 bx-select">
                           <select class="select_cont bx-selectelem bx-input" id="bx-debt-tax" name="debt-tax" aria-required="true">
                              <option value="">Type of Tax Debt</option>
                              <option value="Federal">Federal</option>
                              <option value="State">State</option>
                              <option value="Both">Both</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_50 bx-select">
                           <select class="select_cont bx-selectelem bx-input bx-element" id="bx-element_3" name="debt_amount" aria-required="true">
                              <option value="">Debt Amount</option>
                              <option value="2500">$1000 - $5000</option>
                              <option value="6500">$5000 - $8000</option>
                              <option value="9000">$8000 - $10000</option>
                              <option value="12500">$10000 - $15000</option>
                              <option value="16500">$15000 - $20000</option>
                              <option value="25000">$20000 - $30000</option>
                              <option value="35000">$30000 - $40000</option>
                              <option value="45000">$40000 - $50000</option>
                              <option value="50000">$50000+</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_100">
                           <input type="tel" class="btn_greenboder" id="phone_3" name="phone-number" data-mask="(000) 000-0000" data-pattern="^(\+1)?1?[ \-\.]*\(?([2-9][0-8][0-9])\)?[ \-\.]*([2-9][0-9]{2})[ \-\.]*([0-9]{4})$" placeholder="Phone Number">
                        </div>
                        <div class="vw_100">
                           <input type="submit" name="next" id="submit" class="form_class next2 action-button2 btn btn_green" value="Get Connected">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="taxes-debt-steps" data-step="4" data-progress="100" style="display: none;">
                     <div class="how-much-you-owe" data-next="4">
                        <div class="vw_100 text-center">
                           <span>
                              <i>Look like we already have your information!</i>
                              <h3>Call Us Directly At</h3>
                              to talk with a specialist for free.
                           </span>
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green final" value="Continue to site">
                        </div>
                     </div>
                  </fieldset>
                  <!----------------------------END TAX LOAN---------------------------->
                  <!----------------------------START FIX CREIDT---------------------------->
                  <fieldset class="credit-repair-debt-steps" data-step="2" data-progress="70" style="display: none;">
                     <div class="how-much-you-owe" data-next="2">
                        <div class="vw_100 text-center">
                           <span>Get free professional help to
                           <h3>Tell us more about you</h3>
                           <span>and end problems with back taxes</span>
                        </div>
                        <div class="vw_50">
                           <input type="email" class="btn_greenboder" id="email_4" name="email" placeholder="Email" required>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="zip_code_4" name="zip-code" placeholder="Zip Code">
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green" value="Build Your Score">
                           <input type="button" name="next" class="hide_type-of-debt btn btn_boder" value="Idon't want to build my credit score">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="credit-repair-debt-steps" data-step="3" data-progress="99" style="display: none;">
                     <div class="how-much-you-owe" data-next="3">
                        <div class="vw_100 text-center">
                           <h3>Tell us more about you</h3>
                           <span>We'll provide these details to your debt specialist</span>
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="fname_4" name="firstname" placeholder ="First Name">
                        </div>
                        <div class="vw_50">
                           <input type="text" class="btn_greenboder" id="lname_4" name="lastname" placeholder="Last Name">
                        </div>
                        <div class="vw_100 bx-select">
                           <select class="select_cont bx-selectelem bx-input" id="bx-credit" name="debt-tax" aria-required="true">
                              <option value="">Select what problem you've been facing</option>
                              <option value="Late Payments">Late Payments</option>
                              <option value="Bankruptcy">Bankruptcy</option>
                              <option value="Charge Offs">Charge Offs</option>
                              <option value="Collections">Collections</option>
                              <option value="Others">Others</option>
                           </select>
                           <label for="bx-element" class="bx-ally-label" aria-hidden="true"></label>
                        </div>
                        <div class="vw_100">
                           <input type="tel" class="btn_greenboder" id="phone_4" name="phone-number" data-mask="(000) 000-0000" data-pattern="^(\+1)?1?[ \-\.]*\(?([2-9][0-8][0-9])\)?[ \-\.]*([2-9][0-9]{2})[ \-\.]*([0-9]{4})$" placeholder="Phone Number">
                        </div>
                        <div class="vw_100">

                            <input type="hidden" name="selected_debt_option" id="selected_debt_option">

                            <input type="submit" name="next" id="submit" class="form_class next2 action-button2 btn btn_green" value="Get Connected">
                        </div>
                     </div>
                  </fieldset>
                  <fieldset class="taxes-debt-steps" data-step="4" data-progress="100" style="display: none;">
                     <div class="how-much-you-owe" data-next="4">
                        <div class="vw_100 text-center">
                           <span>
                              <i>Look like we already have your information!</i>
                              <h3>Call Us Directly At</h3>
                              to talk with a specialist for free.
                           </span>
                        </div>
                        <div class="vw_100">
                           <input type="button" name="next" class="next2 action-button2 btn btn_green final" value="Continue to site">
                        </div>
                     </div>
                  </fieldset>
                  <!----------------------------END FIX CREIDT---------------------------->
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- end code  -->
<!-- end code  -->




    <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>  <noscript>&lt;iframe src="//www.googletagmanager.com/ns.html?id=GTM-WHTB53"
height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript> 
<script>(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push({
'gtm.start':
new Date().getTime(), event: 'gtm.js'
}); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-WHTB53');</script>

<div id="o-wrapper" class="o-wrapper">

  <div id="page" class="site">
    <div class="float-panel">


<header id="masthead" class="site-header" role="banner">
<div class="top-color-lines container-fluid">
<div class="row">
  <div class="col-4 color-1"></div>
  <div class="col-4 color-2"></div>
  <div class="col-4 color-3"></div>
</div>
</div>
<div class="header-main">
<div class="container-fluid">
<div class="row">
<div class="col-6 col-md-3 logo"> 
  <a href="<?php echo site_url(); ?>"> 
    <h1>
      <?php
          $custom_logo_id = get_theme_mod( 'custom_logo' );
        $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
        echo '<img src="' . esc_url( $custom_logo_url ) . '" alt="">';?></h1> 
  <!-- <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/logo4.png" alt="Debt.com" class="img-responsive with-tagline"> <img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/debt-logo-notag.png" alt="Debt.com" class="img-responsive no-tagline"> --> 
</a>
</div>
<div class="col-6 col-md-9 header-ui">
 <div class="desktop-text">
<a class="tracking-phone-click" href="#"><i class="fas fa-phone" aria-hidden="true"></i><span>(800) 555-5555</span></a>
</div> 
<div class="credibility-logos"> 
<a href="https://www.bbb.org/south-east-florida/business-reviews/credit-monitoring-protection/debtcom-in-plantation-fl-90080459" target="_blank"> 
<img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/bbb-aplus-logo.png" class="bbb-logo" alt="BBB Logo"> </a> 
<img src="https://keycdn.debt.com/wp-content/themes/Dcom/images/United-Way-logo.png" alt="United Way">
</div> 
<span class="mobile-phone">
<span class="phone-txt"></span>
<a class="tracking-phone-click" href="tel:+18559969795">
<span itemprop="telephone" class="header-ui-icons">
<i class="fas fa-phone" aria-hidden="true"></i>
<span class="sr-only tracking-phone ">(855) 996-9795</span></span>
</a>
</span> 
 <script type="application/javascript" data-cfasync="false">// console.log("HEaaREr")
                                // // document.getElementsByClassName("tracking-phone").style.display = "block!important";
                                // var element = document.getElementById("tracking-phone-retreaver");
                                // // element.forEach(function(item){
                                // //     console.log("asdasd"+item);
                                // // });
                                // console.log(element);
                                // element.classList.remove("d-none");</script>
                    <button id="c-button--push-right" class="c-button"> 
                      <i class="fal fa-bars" aria-hidden="true"></i> 
                      <span class="sr-only">Navigation Menu</span> </button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="header-nav" class="header-nav">
                <div class="menu-menu-1-container">
                  <?php 
                   wp_nav_menu(array(
                    'theme_location' => 'primary',
                    'menu' => 'main-menu',
                    'container' => 'ul',
                    'menu_class' => 'c-menu__items',
                    'echo' => true,
                    'before' => '',
                    'after' => '',
                    'link_before' => '',
                    'link_after' => '',
                    'depth' => 0
                    )
                  );
                ?>
                 
                  </div>
                </div>
              </header>
</div>
<div id="admin_ajax_url" style="display: none;" ><?php echo $url; ?></div>


