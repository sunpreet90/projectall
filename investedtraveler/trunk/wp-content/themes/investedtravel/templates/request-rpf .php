<?php
/**
 * Template Name:Request RPF Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */ 
get_header();

 
if(isset($_POST['fname'])){
    
    $rfp_data = '';
    
    $rfp_data .= '1. What are you looking to do?</br> Ans:'.$_POST['is_company'].'</hr>';
    $rfp_data .= '2.List of Other.Not Other: '.$_POST['list of other'].'</br>';
    $rfp_data .= '3. Tell us more: '.$_POST['tell_us_more'].'</hr>';
    $rfp_data .= '4. What’s you budget and time frame?</br>Ans:'.$_POST['budget_timeframe'].'</hr>';
    $rfp_data .= '5. Best way to reach you?</br>Ans:'.$_POST['fname'].','.$_POST['lname'].','.$_POST['email'].','.$_POST['pnumber'].','.$_POST['cname'].','.$_POST['message'].',,</hr>';
   

    $id = wp_insert_post(
      array(
      'post_title'  =>  'RFP Requests', 
      'post_type'   =>  'rfp_requests', 
      'post_content'  =>  $rfp_data,
      'post_status' =>  'publish'
    )
  );

    //echo "<pre>"; print_r($_POST); echo "</pre>"; die();

	//$x = wp_mail('nitika@mobilyte.com', 'Subject',$message);
  update_post_meta( $id, $meta_key = 'rfp_user_looking_to_do', $meta_value = $_POST['is_company']);
  update_post_meta( $id, $meta_key = 'rfp_list_of_other_not_other', $meta_value = $_POST['list of other']);
  update_post_meta( $id, $meta_key = 'rfp_tell_us_more', $meta_value = $_POST['tell_us_more']);
  update_post_meta( $id, $meta_key = 'rfp_budget_timeframe', $meta_value = $_POST['budget_timeframe']);
  update_post_meta( $id, $meta_key = 'rfp_first_name', $meta_value = $_POST['fname']);
  update_post_meta( $id, $meta_key = 'rfp_last_name', $meta_value = $_POST['lname']);
  update_post_meta( $id, $meta_key = 'rfp_pnumber', $meta_value = $_POST['email']);
  update_post_meta( $id, $meta_key = 'rfp_pnumber', $meta_value = $_POST['pnumber']);
  update_post_meta( $id, $meta_key = 'rfp_company_name', $meta_value = $_POST['cname']);
  update_post_meta( $id, $meta_key = 'rfp_message', $meta_value = $_POST['message']);
 

}
?>


<div class="form-wrapper" style="background-image: url('<?php the_field('rfp_background_image') ?>');">
      <div class="container">
      <div class="row">
      <div class="col-md-12">
      <h2><?php the_field('rfp_title')?></h2>
      <hr class="bordershadow">
      </div>
      <div class="col-md-12">
       <form id="regForm" method="post" action="">
    <!-- One "tab" for each step in the form: -->
    <div class="tab">
     <h3>1. WHAT SERVICES DO YOU NEED?</h3>
      <div class="forminnerd">
      <label class="radio">Journey Model of Workforce Engagement
      <input name="is_company" value="Journey Model of Workforce Engagement" type="radio" oninput="this.className = ''">
      <span class="checkround"></span>
      </label>
      <label class="radio">Affinity and Group Travel
      <input name="is_company" value="Affinity and Group Travel" type="radio" oninput="this.className = ''">
      <span class="checkround"></span>
      </label>
      
        <label class="radio">Event Design and Production
      <input name="is_company" value="Event Design and Production" type="radio" oninput="this.className = ''">
      <span class="checkround"></span>
      </label>
      <label class="radio">Meeting and Event Placement Services
      <input name="is_company" value="Meeting and Event Placement Services" type="radio" oninput="this.className = ''">
      <span class="checkround"></span>
      </label>
      <label class="radio">Hotel Services
      <input name="is_company" type="radio" value="Hotel Services" oninput="this.className = ''">
      <span class="checkround"></span>
      </label>
      <label class="radio">You're Going Place Products and Wearables
      <input name="is_company" type="radio" value="You're Going Place Products and Wearables" oninput="this.className = ''">
      <span class="checkround"></span>
      </label>
      <label  id="flipddd" class="radio">Other's
      <input class="dfd" name="is_company" value="Other" type="radio" oninput="this.className = ''">
      <span class="checkround"></span>
      </label>
          <div id="paneldd">
      <input type="text" name='user_other_options' placeholder="Enter the text">
      </div>
          </div>
    </div>
    <div class="tab">
    <h3>2. List as Other, Not Other's</h3>
    <div class="forminnerd"> 
    <input name='list of other' placeholder="List of others,Not other's...." oninput="this.className = ''">
      </div>
    </div>


    <div class="tab">
    <h3>3. Tell us more</h3>
    <div class="forminnerd"> 
    <input name='tell_us_more' placeholder="Tell us more...." oninput="this.className = ''">
      </div>
    </div>

    <div class="tab">
    <h3>4. What’s you budget and time frame?</h3>
    <div class="forminnerd">
    <input name="budget_timeframe" placeholder="What’s you budget..." oninput="this.className = ''">
     </div> 
    </div>

    <div class="tab">
    <h3>5. Best way to reach you?</h3>
    <div class="forminnerd">
    <input placeholder="First Name" name="fname" oninput="this.className = ''">
    <input placeholder="Last Name" name="lname" oninput="this.className = ''">
     <input placeholder="Email" name="email" oninput="this.className = ''">
      <input placeholder="Phone Number" name="pnumber" oninput="this.className = ''">
         <input name="cname" placeholder="Company Name" oninput="this.className = ''">
        <input name="message" placeholder="Message" oninput="this.className = ''">
      </div> 
    </div>

    <div class="tab last-tab">
      <div class="forminnerd">
        <img class="oksubmit" src="<?php the_field('thanku_image');?>">
        <h4><?php the_field('thanku_content');?> </h4>
      </div>
    </div>

      <div class="formnextprv">
        <button  type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
        <button type="button" id="nextBtn" onclick="nextPrev(1)">Continue</button>
      </div>


    <!-- Circles which indicates the steps of the form: -->
    <div style="text-align:center;margin-top:40px;display:none">
      <span class="step"></span>
      <span class="step"></span>
      <span class="step"></span>
      <span class="step"></span>
      <span class="step"></span>

    </div>

    </form> 

    			</div>
    			</div>
    			</div>
    			</div>

			<!-- End Footer Area -->

			
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.2.0/js/all.js" integrity="sha384-4oV5EgaV02iISL2ban6c/RmotsABqE4yZxZLcYMAdG7FAPsyHYAPpywE9PJo+Khy" crossorigin="anonymous"></script>		

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {

    var element = document.getElementById("nextBtn");
    element.classList.add("rfp-form-submit-btn");

    document.getElementById("nextBtn").innerHTML = "Submit";
    
  } else {
    document.getElementById("nextBtn").innerHTML = "Continue";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {

    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>
<script> 
jQuery(document).ready(function($){
  
	$("#flipddd").click(function(){
    $("#paneldd").toggle();
}); 
});
</script>
<script>
  jQuery(document).ready(function($) {

      $('#nextBtn').click(function () {

        var name = $('#nextBtn').attr("data_id");
        var id = $('#nextBtn').attr("uid");
        var data = 'id='+ id  & 'name='+ name; // this where i add multiple data using  ' & '

      //   $.ajax({
      //     type:"GET",
      //     cache:false,
      //     url:"request-rpf.php",
      //     data:data,    // multiple data sent using ajax
      //     success: function (html) {

      //       $('#nextBtn').val('data sent sent');
      //       $('#msg').html(html);
      //     }
      //   });
      //   return false;
      // });


      $('#regForm').submit(function(e){
  		e.preventDefault();
  		alert('xx');
      });

    });

  });


  /* ##### ##### */

  jQuery(document).on('click', '.rfp-form-submit-btn', function() {

    //jQuery('.rfp-form-submit-btn').hide();

  })
  
  /* ##### / ##### */


</script>
<?php get_footer('pages'); ?>

