<?php
/**
 * Template Name:Who We Serve Page
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */ 
get_header(); ?>


			<div class="banner_who" style="background-image: url('<?php the_field('we-serve-banner-image'); ?>');">
				<div class="container">
					<div class="banner_content  banner_who-content text-center">
						<h1><?php the_field('we-serve-title'); ?></h1>
						<p><?php the_field('we-serve-content'); ?> </p>						
					</div>
				</div>
			</div>
			<!-- End Banner Area -->
		

				<!-- start section Area -->
			<section class="who-we-sec">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="who-we-content">
								<h3><?php the_field('serve_title'); ?></h3>
								<div class="hr"></div>
								<div class="hr2"></div>
								<h1 class="jumps"><?php the_field('serve_tagline'); ?></h1>
								<p><?php the_field('serve_content'); ?></p>
								<h2 class="company-heading"><?php the_field('serve_heading_ul'); ?></h2>

								<?php if( have_rows('company_serve') ): 
                                  while( have_rows('company_serve') ): the_row(); ?>
								<div class="tick_lines">
									<div class="tick_heading">
										<img src="<?php the_sub_field('tick_image'); ?>" alt="#">
									</div>
									<div class="content tick_content">
										<p><?php the_sub_field('tick_content'); ?></p>
									</div>
									<!-- <div class="tick_heading">
										<img src="<?php echo get_template_directory_uri(); ?>/images/tick.jpg" alt="#">
									</div>
									<div class="content tick_content">
										<p>Consectetur Lorem ipsum dolor adipiscing Consectetur adipiscing</p>
									</div>
									<div class="tick_heading">
										<img src="<?php echo get_template_directory_uri(); ?>/images/tick.jpg" alt="#">
									</div>
									<div class="content tick_content">
										<p>Lorem ipsum dolor adipiscing Consectetur adipiscing sed dapibus leo</p>
									</div>
									<div class="tick_heading">
										<img src="<?php echo get_template_directory_uri(); ?>/images/tick.jpg" alt="#">
									</div>
									<div class="content tick_content">
										<p>Sed Lorem ipsum dolor adipiscing Consectetur adipiscing sed dapibus</p>
									</div>
									<div class="tick_heading">
										<img src="<?php echo get_template_directory_uri(); ?>/images/tick.jpg" alt="#">
									</div>
									<div class="content tick_content">
										<p>Lorem ipsum dolor adipiscing Consectetur adipiscing sed dapibus leo</p>
									</div>
									<div class="tick_heading">
										<img src="<?php echo get_template_directory_uri(); ?>/images/tick.jpg" alt="#">
									</div>
									<div class="content tick_content">
										<p>Lorem Lorem ipsum dolor adipiscing Consectetur adipiscing sed dapibus</p>
									</div> -->
								</div>
								<?php endwhile; 
                                 endif; ?>
                                 <div class="content-cta"><p><?php the_field('cta_content'); ?></p>
                                 	<button type="button" class="btn btn-light btn-req-pro"><?php the_field('serve_button'); ?></button></div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<div class="boxes">
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
										<?php if( have_rows('serve_boxes') ): 
                                         while( have_rows('serve_boxes') ): the_row(); ?>
										<div class="d-box">
											<div class="box">
												<img src="<?php the_sub_field('serve_boxes_image'); ?>" alt="#">
												<h1><?php the_sub_field('serve_box_content'); ?></h1>
											</div>
										</div>
										<?php endwhile; 
                                          endif; ?>
										<!-- <div class="d-box">
											<div class="box">
												<img src="<?php echo get_template_directory_uri(); ?>/images/box3.jpg" alt="#">
												<h1>Meeting Planners</h1>
											</div>
										</div> -->
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
										<?php if( have_rows('serve_boxes_2') ): 
                                         while( have_rows('serve_boxes_2') ): the_row(); ?>
										<div class="d-box">
											<div class="box">
												<img src="<?php the_sub_field('serve_boxes_image_1'); ?>" alt="#">
												<h1><?php the_sub_field('serve_boxes_title_1'); ?></h1>
											</div>
										</div>
										<?php endwhile; 
                                          endif; ?>
										<!-- <div class="d-box">
											<div class="box">
												<img src="<?php echo get_template_directory_uri(); ?>/images/box4.jpg" alt="#">
												<h1>Hotels</h1>
											</div>
										</div> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Start our Donation Area -->
				
				<!-- <section class="invest-donation">
				<div class="container">
				<h2>Testinomials </h2>
					<div class="hr"></div>
					<div class="hr2"></div>
					<h3></h3>
					<ul class=" donation-text1">				
					<li class="">
						<p>This was my first client appreciation event I have attended. I was told before going that I would be impressed, but that was a huge understatement.  What an event!  I just had to tell you that the flower wall was absolutely brilliant.  My few clients who attended would not stop raving about it.  One of the clients I invited is a wholesale flower company and he was very impressed with the quality of flowers you had available for our clients.
					    <span>What a fantastic job!! –</span></p>
					
						</li>	
									<li class="">
					
					<p>I spoke with a number of employees enjoying the holiday celebration and you seemed to have earned the respect and support from all.  I must say that that kind of feedback speaks very highly to the reputation of your organization. You have certainly set an early “high bar” for all holiday parties to come.  – David, California<p>			
						</li>	
					
					<li class="">
					<p>The event was absolutely beautiful last night!  My colleague and I had the pleasure of attending and I’m remiss that I didn’t have the chance to introduce myself to you.  The area that your team utilized in the lobby was clean and everything seemed to run smoothly.  Thank you for your attention to detail. Again, the event was great; we especially loved the flower wall. Amber, San Diego<p>
				
						</li>							
				</ul>
				
				
				</div>
				</section> -->
<!-- Start investedtraveler Area -->
			
			
			
			   <!-- End section Area -->
<?php get_footer('pages'); ?>