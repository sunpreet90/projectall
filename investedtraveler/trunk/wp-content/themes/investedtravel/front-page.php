<?php get_header(); ?>

<div class="banner_block" style="background-image: url('<?php the_field('banner_image','5'); ?>');">
				<div class="container">
					<div class="banner_content text-center">								
						<p class="text-uppercase"><?php the_field('banner_title'); ?></p>						
						<a href="<?php the_field('banner_link');?>"><button type="button" class="btn btn-light"><?php the_field('banner_button'); ?></button></a>
					</div>
				</div>
			</div>
			<!-- End Banner Area -->

<!-- Start About Area -->
			<section class="about_area">
				<div class="container">
					<div class="row">					
						<div class="col-lg-12 text-center">
							<h2><?php the_field('about_title','5'); ?></h2>
							<p><?php the_field('about_content','5'); ?></p>
						</div>						
					</div>
				</div>
			</section>
			<!-- End About Area -->

			<!-- Start business Area -->
			<section class="business_area">
				<div class="container">
					<div class="row">					
						<div class="col-lg-12 text-center">
							<h2><?php the_field('business_title','5'); ?></h2>
							<p><?php the_field('business_content','5'); ?></p>
						</div>						
					</div>
				</div>
			</section>
			<!-- End business Area -->


			<!-- Start Services Area -->
			<section id="services" class="services_area" style="background-image: url('<?php the_field('agency_background_image','5'); ?>');">
				<div class="container">
					<div class="row">
						
						<div class="col-lg-12">
						    <div class="service_title text-center">	
								<h2><?php the_field('agency_title','5'); ?></h2>
								<p><?php the_field('agency_tagline','5'); ?></p>
							</div>	
						</div>
						<?php if( have_rows('services_cards','5') ): ?>
                       <?php while ( have_rows('services_cards','5') ) : the_row(); ?>
						<div class="col-md-6 col-lg-4">
                            <div class="service_cart">
								<div class="card">								  
								  <div class="card-body">
									<div class="cart_img text-center"><img src="<?php the_sub_field('services_card_image'); ?>" alt="Service Image"></div>  <h5 class="card-title text-center"><?php the_sub_field('services_card_title'); ?></h5>
									<p class="card-text"><?php the_sub_field('services_card_content'); ?> </p>
									<a href="<?php the_sub_field('services_card_link'); ?>"><?php the_sub_field('services_card_button'); ?></a>
								  </div>
								</div>
							</div>
						</div><!--./col-lg-4./service_cart-->
						<?php  endwhile; ?>
                         <?php endif; ?>
						
						<!-- <div class="col-md-6 col-lg-4">
                            <div class="service_cart">
								<div class="card">								  
								  <div class="card-body">
									<div class="cart_img text-center"><img src="<?php echo get_template_directory_uri(); ?>/images/icon2.png" alt="Service Image"></div>  
									<h5 class="card-title text-center">Event Design and Production</h5>
									<p class="card-text">From non-profit galas to golf tournaments and everything in between, we provide full service event design, production and staffing that help you deepen engagement with your gueets and stakeholders.</p>
									<a href="#">Identify the benefits</a>
								  </div>
								</div>
							</div>
						</div><!--./col-lg-4./service_cart-->	
						
					<!-- 	<div class="col-md-6 col-lg-4">
                            <div class="service_cart">
								<div class="card">								  
								  <div class="card-body">
									<div class="cart_img text-center"><img src="<?php echo get_template_directory_uri(); ?>/images/icon3.png" alt="Service Image"></div>  
									<h5 class="card-title text-center">Meeting and Event Placement Services</h5>
									<p class="card-text">Helping meeting planners and government agencies source, negotiate and place meetings and events at hotels and other establishments to save you time and money. <a href="#">Any additional benefits? </a> </p>
									<a href="#">Learn more</a>
								  </div>
								</div>
							</div>
						</div> --><!--./col-lg-4./service_cart-->
						
						<!-- <div class="col-md-6 col-lg-4">
                            <div class="service_cart">
								<div class="card">								  
								  <div class="card-body">
									<div class="cart_img text-center"><img src="<?php echo get_template_directory_uri(); ?>/images/icon4.png" alt="Service Image"></div>  
									<h5 class="card-title text-center">Affinity and Group Travel</h5>
									<p class="card-text">Where your passions are, is where Invested Traveler can take you. Whether that's the U.S. Open, a wine experience through Tuscany or yoga retreat in Cabo San Lucas, Invested Traveler has relationships with local DMCs in top destinations to peak your interest for all of your passions.</p>									
								  </div>
								</div>
							</div>
						</div> --><!--./col-lg-4./service_cart-->	
						
					<!-- 	<div class="col-md-6 col-lg-4">
                            <div class="service_cart">
								<div class="card">								  
								  <div class="card-body">
									<div class="cart_img text-center"><img src="<?php echo get_template_directory_uri(); ?>/images/icon5.png" alt="Service Image"></div>  
									<h5 class="card-title text-center">Hotel Services</h5>
									<p class="card-text">Sales and marketing services that help you position your team for sales and revenue growth, offer new merchandising products that appeal to business and leisure traveler and financial services advisory services when you want to acquire or refinance debt. Link to hotel services page</p>									
								  </div>
								</div>
							</div>
						</div> --><!--./col-lg-4./service_cart-->
						
						<!-- <div class="col-md-6 col-lg-4">
                            <div class="service_cart">
								<div class="card">								  
								  <div class="card-body">
									<div class="cart_img text-center"><img src="<?php echo get_template_directory_uri(); ?>/images/icon6.png" alt="Service Image"></div>  
									<h5 class="card-title text-center">You're Going Place Products and Wearables</h5>
									<p class="card-text">Travel accessories in luxury catergory that are suitable for high end hotel and cruiseline gift shops or branded for your company events. See our collection.</p>					
								  </div>
								</div>
							</div>
						</div --><!--./col-lg-4./service_cart-->							
						
					</div>
				</div>
			</section>
			<!-- End investedtraveler Area -->


<?php get_footer('pages'); ?>