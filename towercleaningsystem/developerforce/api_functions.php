<?php
// v2014.07.22

require_once(dirname(__FILE__).'/soapclient/SforcePartnerClient.php');



/*
 * The centralized sfConnection function.
 * 
 * By default, returns the connection object needed to interact with Salesforce.
 *
 * Arguments:
 * If boolean true is passed as the parameter, it will return both the connection
 *    AND the connection login result, in an array of respective order.
 */
function sfConnection($return_both = false){
	$sf_credentials = sfConnectionCredentials();
	extract($sf_credentials);
	
	if(empty($sf_username) or empty($sf_password) or empty($wsdl_file_name)){
		trigger_error('Error: api_functions.php did not receive a full list of credentials!', E_USER_WARNING);
		return false;
	}
	
	try{
		$sfConnection = new SforcePartnerClient();
		$sfConnection->createConnection(dirname(__FILE__).'/soapclient/'.$wsdl_file_name);
		$sfConnection_login_result = $sfConnection->login($sf_username, $sf_password);
	} catch(Exception $e){
		trigger_error($e, E_USER_WARNING);
		if($return_both) return array(false, false);
		else return false;
	}
	
	if($return_both) return array($sfConnection, $sfConnection_login_result);
	else return $sfConnection;
}


/*
 * Returns the current timestamp from Salesforce.
 *
 * Arguments:
 * Can be passed a currently connected SforcePartnerClient as an argument. Will create a new one otherwise.
 */
function sfTimestamp($con=''){
	if(empty($con)) $con = sfConnection();
	
	if(empty($con)){
		return false;
	}
	
	$result = $con->getServerTimestamp();
	return $result->timestamp;
}


/*
 * Returns the metadata for a Salesforce object.
 *
 * Arguments:
 * The object's API name must be specified as first argument $q.
 * 
 * An existing SforcePartnerClient object can optionally be passed as a second argument.
 */
function sfDescribe($q, $con='', $type='object'){
	if(empty($con)) $con = sfConnection();
	
	if(empty($con)){
		return false;
	}
	
	$res = $con->describeSObject($q);
	$fields = $res->fields;
	
	if ($type=='object'){
		return $fields;
	}
	else{
		trigger_error('Error: api.php does not support non-object type', E_USER_WARNING);
		return false;
	}
}


/*
 * Performs a SOQL query on Salesforce records and returns the results.
 *
 * Arguments:
 * SOQL query most be passed as first argument $q.
 * 
 * Optionally, an existing SforcePartnerClient object can be passed as second argument $con.
 * 
 * Optionally, a return $type may be specified as the third argument. Acceptable values are:
 *    "object" (default, returns the results object unfiltered)
 *    "array" an array of arrays. Each array is one result/row, a flattened array combining
 *       the properties ->Id, ->queryResult, plus all available values of the ->fields property.
 */
function sfQuery($q, $con='', $type='object'){
	if(empty($con)) $con = sfConnection();
	
	if(empty($con)){
		return false;
	}
	
	$res = $con->query($q);
	
	$done = false;
	$records = array();
	
	while(!$done) {
		foreach($res->records as $a_record){
			$records[] = $a_record;
		}
		if($res->done != true){
			$res = $con->queryMore($res->queryLocator);
		}
		else{
			$done = true;
		}
	}
	
	unset($res);
	if($type=='object')
		return $records;
	else if($type=='array'){
		$a = array();
		foreach($records as $record){
			if(isset($record->fields)) $temp_array = get_object_vars($record->fields);
			else $temp_array = array();
			if(isset($record->Id)) $temp_array['Id'] = $record->Id;
			if(isset($record->queryResult)){
				$temp_array['queryResult'] = $record->queryResult;
			}
			$a[] = $temp_array;
		}
		unset($record);
		return $a;
	}
	else{
		return $records;
	}
}


/*
 * Takes an array of one or more SObjects, and performs updates on Salesforce records based on them.
 *
 * The record updates must be valid SObjects that include the necessary Id property.
 *
 * Arguments:
 * An array of SObjects must be passed as first argument $sobj.
 *
 * Optionally, an existing SforcePartnerClient object can be passed as second argument $con.
 *
 * Optionally, a return $type may be specified as the third argument. Acceptable values are:
 *    "object" (default, returns the update results object unfiltered)
 *    "array" an array of arrays. Each array is one result/row, containing the properties of the
 *       original result objects.
 */
function sfUpdate($sobj, $con='', $type='object'){
	if(empty($con)) $con = sfConnection();
	
	if(empty($con)){
		return false;
	}
	
	try{
		$results = $con->update($sobj);
	} catch(Exception $e){
		trigger_error($e, E_USER_WARNING);
		return false;
	}
	if($type=='object')
		return $results;
	else if($type=='array'){
		$a = array();
		foreach($results as $result){
			$a[] = get_object_vars($result);
		}
		unset($record);
		return $a;
	}
	else{
		return $results;
	}
}


/*
 * Inserts one or more new records in to Salesforce.
 *
 * The new records must be in the form of valid SObjects.
 *
 * Arguments:
 * An array of SObjects must be passed as first argument $sobjs.
 * Optionally, an existing SforcePartnerClient object can be passed as second argument $con.
 *
 * Optionally, a return $type may be specified as the third argument.
 *    "object" (default, returns the results object unfiltered) is the only acceptable one at present
 *
 * Optionally, an EmailHeader instance may be specified as the fourth argument. Must be passed as an
 *    array, mapping respectively to the triggerAutoResponseEmail, triggerOtherEmail, and
 *    triggerUserEmail arguments of the class. More details at
 *    http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_header_emailheader.htm
 *
 * Optionally, $assignment_rule_header may be specified as boolean true for a fifth argument.
 *    This will submit a useDefaultRule assignment rule header as specified at
 *    https://www.salesforce.com/us/developer/docs/api/Content/sforce_api_header_assignmentruleheader.htm
 *    Specifying specific rules by ID, although supported by Salesforce, is not currently supported
 *    by this function.
 */
function sfCreate($sobjs, $con='', $type='object', $email_header = false, $assignment_rule_header = ''){
	if(empty($con)) $con = sfConnection();
	
	if(empty($con)){
		return false;
	}
	
	if(empty($type)) $type = 'object';
	
	if($email_header === true) $email_header = array(true, true, true);
	
	if($email_header){
		if(!is_array($email_header) or count($email_header) != 3){
			trigger_error('Error, invalid email header specified!', E_USER_WARNING);
			return false;
		}
		else{
			$email_header = new EmailHeader($email_header[0], $email_header[1], $email_header[2]);
			$con->setEmailHeader($email_header);
		}
	}
	
	if(!empty($assignment_rule_header)){
		if($assignment_rule_header === true){
			$rule_header = new AssignmentRuleHeader('', true);
			$con->setAssignmentRuleHeader($rule_header);
		}
		else{
			trigger_error('Error, specific assignment rules not yet supported!', E_USER_WARNING);
			return false;
		}
	}
	
	try{
		$results = $con->create($sobjs);
	} catch(Exception $e){
		trigger_error($e, E_USER_WARNING);
		return false;
	}
	
	if($type=='object'){
		return $results;
	}
	else{
		trigger_error("sfCreate does not yet support " . $type . " return type", E_USER_WARNING);
		return true;
	}
}

?>