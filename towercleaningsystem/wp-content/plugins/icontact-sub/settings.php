<?php

function ucp_iContact_getDefaultSettings($return_all = true){
	//Settings available to change in WP Settings page
	$default_settings = array(
		'debug' => false,   // false, true, or 'verbose'
		
		'submission_good_for' => 48,   // how long to keep unconfirmed signups around in the database, in hours
		'data_lifespan' => 240,   // how long to keep other data like metadata, email logs, etc. cached, in hours
		'metadata_refresh_time' => 24,   // how often to poll Salesforce for new metadata
		'live_metadata' => false,
		
		'url_for_shortcode_icontact_signup' => '',
		'url_for_shortcode_icontact_signup_verification' => '',
		'url_for_shortcode_icontact_sub_edit' => ''
	);
	
	//Settings that can only be altered here
	$hidden_settings = array(
		/* Test settings */
		'sqluser' => 'towercleaningsystems',
		'sqlpassword' => 'W2r7SJYFbUfGfAhY',
		'sqldb' => 'towercleaningsystems',
		
		/* Production settings 
		'sqluser' => 'icontact',
		'sqlpassword' => '9GHm93j,BdyGaC',
		'sqldb' => 'ucporg382_icontact', */
		
		'subscriptions_email_address' => 'subscriptions@ucp.org',
		'subscriptions_email_password' => '9PhRc1%VaCf6AN9!c&;=',
		'subscriptions_email_name' => 'UCP Subscriptions'
	);
	
	if($return_all) return array_merge($default_settings, $hidden_settings);
	else return $default_settings;
}


function ucp_iContact_initSettings(){
	register_setting('ucp_iContact', 'ucp_iContact', 'ucp_iContact_sanitizeSettingsSubmission');
	
	add_settings_section('ucp_iContact_newsletters', 'Newsletters', 'ucp_iContact_renderNewslettersSection', 'ucp_iContact');
	add_settings_field('ucp_iContact_newsletters_primary_list', 'Primary Newsletters', 'ucp_iContact_renderPrimaryNewslettersField', 'ucp_iContact', 'ucp_iContact_newsletters');
	
	add_settings_section('ucp_iContact_urls', 'Page URLs', 'ucp_iContact_renderURLsSection', 'ucp_iContact');
	add_settings_field('ucp_iContact_url_for_shortcode_icontact_signup', 'Signup', 'ucp_iContact_renderURLSignupField', 'ucp_iContact', 'ucp_iContact_urls');
	add_settings_field('ucp_iContact_url_for_shortcode_icontact_signup_verification', 'Address Verification', 'ucp_iContact_renderURLVerificationField', 'ucp_iContact', 'ucp_iContact_urls');
	add_settings_field('ucp_iContact_url_for_shortcode_icontact_sub_edit', 'Edit Subscription', 'ucp_iContact_renderURLSubEditField', 'ucp_iContact', 'ucp_iContact_urls');
	
	add_settings_section('ucp_iContact_database', 'Database', 'ucp_iContact_renderDatabaseSection', 'ucp_iContact');
	add_settings_field('ucp_iContact_database_submissions_good_for', 'Signup and Address Change Lifespan', 'ucp_iContact_renderSubmissionGoodForField', 'ucp_iContact', 'ucp_iContact_database');
	add_settings_field('ucp_iContact_database_data_lifespan', 'Log Lifespan', 'ucp_iContact_renderDataLifespanField', 'ucp_iContact', 'ucp_iContact_database');
	add_settings_field('ucp_iContact_database_metadata_refresh_time', 'Metadata Lifespan', 'ucp_iContact_renderMetadataRefreshTimeField', 'ucp_iContact', 'ucp_iContact_database');
	add_settings_field('ucp_iContact_database_live_metadata', 'Live Metadata', 'ucp_iContact_renderLiveMetadataField', 'ucp_iContact', 'ucp_iContact_database');
	
	add_settings_section('ucp_iContact_debug', 'Debug', 'ucp_iContact_renderDebugSection', 'ucp_iContact');
	add_settings_field('ucp_iContact_debug_setting', 'Debug', 'ucp_iContact_renderDebugField', 'ucp_iContact', 'ucp_iContact_debug');
}
if(function_exists('add_action')){
	add_action('admin_init', 'ucp_iContact_initSettings');
}


function ucp_iContact_initSettingsMenu(){
	add_options_page('UCP Newsletters Settings', 'UCP Newsletters', 'manage_options', 'ucp_iContact', 'ucp_iContact_renderSettingsPage');
}
if(function_exists('add_action')){
	add_action('admin_menu', 'ucp_iContact_initSettingsMenu');
}


function ucp_iContact_renderSettingsPage(){
	?>
	<div class="wrap">
		<h2>UCP Newsletters</h2>
		<form method="post" action="options.php">
		<?php settings_fields( 'ucp_iContact' ); ?>
		<?php do_settings_sections('ucp_iContact'); ?>
		
		<input name="Submit" id="submit" class="button button-primary" type="submit" value="Save Changes" />
		</form>
	</div><!-- .wrap -->
	<?php
}


function ucp_iContact_renderNewslettersSection(){
	
}


function ucp_iContact_renderPrimaryNewslettersField($args){
	$options = get_option('ucp_iContact');
	wp_enqueue_script('jquery'); ?>
	
	<script type="text/javascript">
		jQuery(function(){
			function ucp_iContact_moveDown() {
				var current_down_button = jQuery(this);
				var current_div = jQuery(current_down_button).parent();
				var current_up_button = jQuery(current_div).children('.ucp_iContact_primnewsletter_toggleup');
				var current_order_field = jQuery(current_div).children('.ucp_iContact_primnewsletter_order');
				var next_div = jQuery(current_div).next();
				
				if(typeof jQuery(next_div).val() !== 'undefined'){
					var next_down_button = jQuery(next_div).children('.ucp_iContact_primnewsletter_toggledown');
					var next_order_field = jQuery(next_div).children('.ucp_iContact_primnewsletter_order');
					var next_next_div = jQuery(next_div).next();
					
					var low_number = jQuery(current_order_field).val();
					var high_number = jQuery(next_order_field).val();
					
					jQuery(current_order_field).val(high_number);
					jQuery(next_order_field).val(low_number);
					
					jQuery(next_down_button).prop('disabled', false);
					jQuery(current_up_button).prop('disabled', false);
					if(typeof jQuery(next_next_div).val() === 'undefined') jQuery(current_down_button).prop('disabled', true);
					if(typeof jQuery(current_div).prev().val() === 'undefined') jQuery(next_div).children('.ucp_iContact_primnewsletter_toggleup').prop('disabled', true);
					
					jQuery(next_div).insertBefore(current_div);
				}
				else{
					jQuery(current_down_button).prop('disabled', true);
				}
			}
			
			function ucp_iContact_moveUp() {
				var current_up_button = jQuery(this);
				var current_div = jQuery(current_up_button).parent();
				var current_down_button = jQuery(current_div).children('.ucp_iContact_primnewsletter_toggledown');
				var current_order_field = jQuery(current_div).children('.ucp_iContact_primnewsletter_order');
				var prev_div = jQuery(current_div).prev();
				
				if(typeof jQuery(prev_div).val() !== 'undefined'){
					var prev_up_button = jQuery(prev_div).children('.ucp_iContact_primnewsletter_toggleup');
					var prev_order_field = jQuery(prev_div).children('.ucp_iContact_primnewsletter_order');
					var prev_prev_div = jQuery(prev_div).prev();
					
					var low_number = jQuery(prev_order_field).val();
					var high_number = jQuery(current_order_field).val();
					
					jQuery(current_order_field).val(low_number);
					jQuery(prev_order_field).val(high_number);
					
					jQuery(prev_up_button).prop('disabled', false);
					jQuery(current_down_button).prop('disabled', false);
					if(typeof jQuery(prev_prev_div).val() === 'undefined') jQuery(current_up_button).prop('disabled', true);
					if(typeof jQuery(current_div).next().val() === 'undefined') jQuery(prev_div).children('.ucp_iContact_primnewsletter_toggledown').prop('disabled', true);
					
					jQuery(current_div).insertBefore(prev_div);
				}
				else{
					jQuery(current_up_button).prop('disabled', true);
				}
			}
			
			function ucp_iContact_indieToggle() {
				var indie_checkbox = jQuery(this);
				var current_div = jQuery(indie_checkbox).parent().parent();
				var indielink_div = jQuery(current_div).children('.ucp_iContact_primnewsletter_indieurldiv');
				jQuery(indielink_div).toggle();
			}
			
			jQuery('.ucp_iContact_primnewsletter_toggledown').click(ucp_iContact_moveDown);
			jQuery('.ucp_iContact_primnewsletter_toggleup').click(ucp_iContact_moveUp);
			jQuery('.ucp_iContact_primnewsletter_indie').change(ucp_iContact_indieToggle);
		});
	</script>
	
	<style type="text/css">
		div.ucp_iContact_primary_newsletter_entry{
			border: 1px solid black;
			margin: 5px 0;
			padding: 3px;
			width: 95%;
		}
		div.ucp_iContact_primary_newsletter_entry label{
			font-weight: bold;
			margin-left: 2px;
		}
		input.ucp_iContact_primary_newsletter_name{
			min-width: 300px;
			width: 33%;
		}
		input.ucp_iContact_primnewsletter_toggledown, input.ucp_iContact_primnewsletter_toggleup{
			float: right;
		}
		input.ucp_iContact_primnewsletter_desc{
			width: 90%;
			margin-top: 3px;
			margin-right: 5%;
		}
		
		.wrap input[type="url"]{
			min-width: 400px;
			width: 50%;
		}
		
		.ucp_iContact_primnewsletter_indiediv, .ucp_iContact_primnewsletter_indieurldiv {
			display: inline-block;
			width: auto;
			padding: 0 5px;
			min-height: 25px;
			border: 1px solid rgb(221, 221, 221);
		}
		
		.ucp_iContact_primnewsletter_indiediv{
			line-height: 25px;
			margin: 2px 30px 2px 2px;
		}
		
		.ucp_iContact_primnewsletter_indieurldiv input[type="url"].ucp_iContact_primnewsletter_indielink{
			min-width: 200px;
			max-width: 600px;
			width: auto;
		}
	</style>
	
	<p>Put here the name and description for newsletters that you want to show up on the signup form. (Will also always show up on the subscription editing page for people to add them later.)<br/>
	<strong>Names must <em>exactly</em> match the Salesforce field, unless it is a non-Salesforce/iContact newsletter.</strong><br />
	Erase a newsletter's name to delete it.</p>
	<div id="ucp_iContact_newsletters_primary_list">
		<?php $first_newsletter = true;
		$last_newsletter = false;
		if(!empty($options['primary_newsletters'])) $prim_newsletters = $options['primary_newsletters'];
		else $prim_newsletters = array();
		$prim_newsletters[] = array('name' => '', 'description' => '');
		foreach($prim_newsletters as $key => $a_primary_newsletter):
			if($key == count($prim_newsletters) - 1) $last_newsletter = true; ?>
		<div class="ucp_iContact_primary_newsletter_entry" id="prim_news_<?php echo $key; ?>">
			<input type="button" id="ucp_iContact_primnewsletter_toggledown<?php echo $key; ?>" class="button ucp_iContact_primnewsletter_toggledown" value="&darr;" <?php if($last_newsletter) echo 'disabled="disabled" '; ?>/>
			<input type="button" id="ucp_iContact_primnewsletter_toggleup<?php echo $key; ?>" class="button ucp_iContact_primnewsletter_toggleup" value="&uarr;" <?php if($first_newsletter){$first_newsletter = false; echo 'disabled="disabled" ';} ?>/>
			<input type="hidden" class="ucp_iContact_primnewsletter_order" name="ucp_iContact[primary_newsletters][<?php echo $key; ?>][order]" value="<?php echo $key; ?>" />
			<label for="ucp_iContact_primary_newsletters<?php echo $key; ?>_name">Name</label> <input type="text" id="ucp_iContact_primary_newsletters<?php echo $key; ?>_name" class="ucp_iContact_primary_newsletter_name" name="ucp_iContact[primary_newsletters][<?php echo $key; ?>][name]" placeholder="Enter a newsletter here by entering the name" maxlength="40" value="<?php echo htmlspecialchars($a_primary_newsletter['name']); ?>" /><br />
			<input type="text" class="ucp_iContact_primnewsletter_desc" name="ucp_iContact[primary_newsletters][<?php echo $key; ?>][description]" value="<?php echo htmlspecialchars($a_primary_newsletter['description']); ?>" placeholder="Description" maxlength="500" /><br />
			<div class="ucp_iContact_primnewsletter_indiediv"><input type="checkbox" class="ucp_iContact_primnewsletter_indie" id="ucp_iContact_primnewsletter<?php echo $key; ?>_indie" name="ucp_iContact[primary_newsletters][<?php echo $key; ?>][indie]" <?php if($a_primary_newsletter['indie']) echo 'checked="checked" '; ?>/> <label for="ucp_iContact_primnewsletter<?php echo $key; ?>_indie">Not a Salesforce/iContact newsletter</label></div>
			<div class="ucp_iContact_primnewsletter_indieurldiv"<?php if(!$a_primary_newsletter['indie']) echo ' style="display: none;"'; ?>><label for="ucp_iContact_primnewsletter<?php echo $key; ?>_indielink">Link URL</label> <input type="url" class="ucp_iContact_primnewsletter_indielink" id="ucp_iContact_primnewsletter<?php echo $key; ?>_indielink" name="ucp_iContact[primary_newsletters][<?php echo $key; ?>][indielink]" value="<?php echo $a_primary_newsletter['indielink']; ?>" /></div>
		</div>
		<?php endforeach;
		unset($key, $a_primary_newsletter); ?>
	</div><!-- #ucp_iContact_newsletters_primary_list -->
	<?php
}


function ucp_iContact_renderURLsSection(){
	?>
	<p>The plug-in will try to find these automatically by searching for the corresponding shortcodes. If the search fails &ndash; for instance, the pages are hidden from site search &ndash; or if the results of that search need to be overridden, put the address of the appropriate page(s) below.</p>
	<p>List of Shortcodes:</p>
	<ul>
		<li>[icontact_signup]</li>
		<li>[icontact_signup_verification]</li>
		<li>[icontact_sub_edit]</li>
	</ul>
	<?php
}


function ucp_iContact_renderURLSignupField($args){
	$options = ucp_iContact_getSettings(); ?>
	<input id="ucp_iContact_url_for_shortcode_icontact_signup" class="url" name="ucp_iContact[url_for_shortcode_icontact_signup]" type="url" value="<?php echo $options['url_for_shortcode_icontact_signup'];?>" />
	<?php
}


function ucp_iContact_renderURLVerificationField($args){
	$options = ucp_iContact_getSettings(); ?>
	<input id="ucp_iContact_url_for_shortcode_icontact_signup_verification" class="url" name="ucp_iContact[url_for_shortcode_icontact_signup_verification]" type="url" value="<?php echo $options['url_for_shortcode_icontact_signup_verification'];?>" />
	<?php
}


function ucp_iContact_renderURLSubEditField($args){
	$options = ucp_iContact_getSettings(); ?>
	<input id="ucp_iContact_url_for_shortcode_icontact_sub_edit" class="url" name="ucp_iContact[url_for_shortcode_icontact_sub_edit]" type="url" value="<?php echo $options['url_for_shortcode_icontact_sub_edit'];?>" />
	<?php
}


function ucp_iContact_renderDatabaseSection(){
	echo 'Control how long the local SQL database holds on to information.';
}


function ucp_iContact_renderSubmissionGoodForField($args){
	$options = ucp_iContact_getSettings(); ?>
	<input id="ucp_iContact_database_submissions_good_for" name="ucp_iContact[submission_good_for]" type="number" min="1" max="168" value="<?php echo $options['submission_good_for']; ?>" /> hours in which new subscribers or email address changes can be verified before request is tossed out
	<?php
}


function ucp_iContact_renderDataLifespanField($args){
	$options = ucp_iContact_getSettings(); ?>
	<input id="ucp_iContact_database_data_lifespan" name="ucp_iContact[data_lifespan]" type="number" min="24" max="672" value="<?php echo $options['data_lifespan']; ?>" /> hours - other data logged in the SQL database will be kept this long
	<?php
}


function ucp_iContact_renderMetadataRefreshTimeField($args){
	$options = ucp_iContact_getSettings(); ?>
	<input id="ucp_iContact_database_metadata_refresh_time" name="ucp_iContact[metadata_refresh_time]" type="number" min="1" max="168" value="<?php echo $options['metadata_refresh_time']; ?>" /> Metadata from salesforce will be refreshed after this many hours
	<?php
}


function ucp_iContact_renderLiveMetadataField($args){
	$options = ucp_iContact_getSettings(); ?>
	<input id="ucp_iContact_database_live_metadata" name="ucp_iContact[live_metadata]" type="checkbox" <?php if($options['live_metadata']) echo 'checked="checked" '; ?>/> If checked, metadata (e.g. newsletter subscription options data) will be retrieved from Salesforce every time a newsletter-related page is loaded, ignoring the above interval. Not recommended!
	<?php
}


function ucp_iContact_renderDebugSection(){
	echo '<p>Enables debug messages for whitelisted (UCP office) IP addresses.</p>';
}


function ucp_iContact_renderDebugField($args){
	$options = ucp_iContact_getSettings(); ?>
	<select id="ucp_iContact_debug_setting" name="ucp_iContact[debug]" />
		<option value="false"<?php if($options['debug'] === false or $options['debug'] === 'false') echo ' selected="selected"'; ?>>Off</option>
		<option value="true"<?php if($options['debug'] === true or $options['debug'] === 'true') echo ' selected="selected"'; ?>>On</option>
		<option value="verbose"<?php if($options['debug'] === 'verbose') echo ' selected="selected"'; ?>>On - Verbose</option>
	</select>
	<?php
}


/*
 * Accepts settings submission as array and returns validated output
 */
function ucp_iContact_sanitizeSettingsSubmission($input){
	
	/*
	 * for usorting the newsletter array
	 */
	if(!function_exists('compareNewsletters')){
		function compareNewsletters($a, $b){
			$a = intval($a['order']);
			$b = intval($b['order']);
			
			if($a == $b) return 0;
			return ($a < $b) ? -1 : 1;
		}
	}
	
	
	
	$output = array();
	$old_settings = get_option('ucp_iContact');
	$defaults = ucp_iContact_getDefaultSettings(false);
	
	$integers = array('submission_good_for', 'data_lifespan', 'metadata_refresh_time');
	foreach($integers as $an_int_field){
		if(isset($input[$an_int_field]) and ctype_digit($input[$an_int_field])) $output[$an_int_field] = intval($input[$an_int_field]);
		elseif(isset($old_settings[$an_int_field])) $output[$an_int_field] = $old_settings[$an_int_field];
		else $output[$an_int_field] = $defaults[$an_int_field];
	}
	
	$urls = array('url_for_shortcode_icontact_signup',
				  'url_for_shortcode_icontact_signup_verification',
				  'url_for_shortcode_icontact_sub_edit'
				  );
	foreach($urls as $a_url){
		if(isset($input[$a_url])){
			$input_to_test = str_replace('-', '', $input[$a_url]);   // Old versions of PHP have a bug that regards hyphens as an invalid URL, so just always allow them as workaround
			if(empty($input[$a_url])) $output[$a_url] = '';
			elseif(filter_var($input_to_test, FILTER_VALIDATE_URL)) $output[$a_url] = $input[$a_url];
			else $output[$a_url] = $defaults[$a_url];
		}
		else $output[$a_url] = $defaults[$a_url];
	}
	unset($a_url, $input_to_test);
	
	$booleans = array('live_metadata'
				);
	foreach($booleans as $a_boolean){
		if(isset($input[$a_boolean])) $output[$a_boolean] = $input[$a_boolean];
		elseif(!isset($old_settings[$a_boolean])) $output[$a_boolean] = $defaults[$a_boolean];
		else $output[$a_boolean] = false;
	}
	
	if(isset($input['debug'])){
		if($input['debug'] === true or $input['debug'] === 'true') $output['debug'] = true;
		elseif($input['debug'] === 'verbose') $output['debug'] = $input['debug'];
		else $output['debug'] = $defaults['debug'];
	}
	else $output['debug'] = $defaults['debug'];
	
	if(!empty($input['primary_newsletters'])){
		foreach($input['primary_newsletters'] as $key => $a_newsletter){
			if(!empty($a_newsletter['name'])) $prim_newsletters[$key] = $a_newsletter;
		}
		unset($key, $a_newsletter);
		
		foreach($prim_newsletters as &$a_newsletter){
			if(!empty($a_newsletter['indielink'])){
				if($a_newsletter['indie']){
					$input_to_test = str_replace('-', '', $a_newsletter['indielink']);   // Old versions of PHP have a bug that regards hyphens as an invalid URL, so just always allow them as workaround
					if(!filter_var($input_to_test, FILTER_VALIDATE_URL)) $a_newsletter['indielink'] = '';
				}
				else $a_newsletter['indielink'] = '';
			}
		}
		unset($a_newsletter, $input_to_test);
		
		usort($prim_newsletters, 'compareNewsletters');
		
		$output['primary_newsletters'] = $prim_newsletters;
	}
	else $output['primary_newsletters'] = array();
	
	
	if($output['live_metadata']){
		ucp_iContact_cleanSFMetadataCache(0);
	}
	
	
	return $output;
}


function ucp_iContact_getSettings(){
	$settings = get_option('ucp_iContact');
	if(isset($settings['primary_newsletters'])) unset($settings['primary_newsletters']);
	
	$default_settings = ucp_iContact_getDefaultSettings();
	
	foreach($default_settings as $key => $value){
		if(!isset($settings[$key])) $settings[$key] = $value;
	}
	
	if($settings['debug'] === 'true') $settings['debug'] = true;
	elseif($settings['debug'] === 'false') $settings['debug'] = false;
	
	if($settings['debug'] and !ucp_whitelist_isOfficeIP()) $settings['debug'] = false;
	
	return $settings;
}


function ucp_iContact_getPrimaryMailings() {
	$options = get_option('ucp_iContact');
	
	$primary_mailings = array();
	
	if(!empty($options['primary_newsletters'])){
		foreach($options['primary_newsletters'] as $a_primary_mailing){
			$primary_mailings[] = new ucp_MailingList($a_primary_mailing['name'], $a_primary_mailing['name'], $a_primary_mailing['description'], true, $a_primary_mailing['indie'], $a_primary_mailing['indielink']);
		}
	}
	
	return $primary_mailings;
}

?>