<?php
get_header();
/*if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		the_title();
		the_post_thumbnail('small');
		the_content();
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;*/
//get_sidebar();
//get_footer();
?>
<style>

.add{background: #333; padding: 10%; height: 300px;}

.nav-sidebar { 
    width: 100%;
    padding: 8px 0; 
    border-right: 1px solid #ddd;
}
.nav-sidebar a {
    color: #333;
    -webkit-transition: all 0.08s linear;
    -moz-transition: all 0.08s linear;
    -o-transition: all 0.08s linear;
    transition: all 0.08s linear;
}
.nav-sidebar .active a { 
    cursor: default;
    background-color: #34ca78; 
    color: #fff; 
}
.nav-sidebar .active a:hover {
    background-color: #37D980;   
}
.nav-sidebar .text-overflow a,
.nav-sidebar .text-overflow .media-body {
    white-space: nowrap;
    overflow: hidden;
    -o-text-overflow: ellipsis;
    text-overflow: ellipsis; 
}

.btn-blog {
    color: #ffffff;
    background: #035d78;
    border-color: #035d78;
    border-radius:0;
    margin-bottom:10px
    padding: 12px 45px;
    font-size: 13px;
}
.btn-blog:hover,
.btn-blog:focus,
.btn-blog:active,
.btn-blog.active,
.open .dropdown-toggle.btn-blog {
    color: white;
    background-color:#035d78;
    border-color: #035d78;
}
 h2{color:#035d78;}
 .margin10{margin-bottom:10px; margin-right:10px;}

</style>
<div class="servicedtl-wrapper">
<div class="container">
	<div class="row servicesdetail"> 
	<div class="col-xs-12 col-sm-8 col-sm-offset-2 paddingTop20">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail(); ?></a>
        </div>    
     <div class="col-xs-12 col-sm-8 col-sm-offset-2 ">
	 <div class="servicesdetailtext">
     	<?php 
     	if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>
                     <h1><?php  the_title(); ?></h1>
                     <!-- <?php the_post_thumbnail(); ?> -->
                     <article><p>
                         <?php the_content(); ?> 
                         </p>
                     </article>
       <?php endwhile;
else :  ?> 
<article><p>
                         <?php echo wpautop( 'Sorry, no posts were found' ); ?> 
                         </p>
                     </article>
                     <?php endif;
 ?> 
     </div> </div>
	</div>
</div>
</div>
<?php  get_footer(); ?>