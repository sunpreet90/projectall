<?php  
// Template Name: Home Gallery Page
get_header();
?>
<style type="text/css">
	.homegallery-inner img {
    /* min-height: 22px !important; */
    height: 201px !important;
}
</style>
<div class="homegallery-wrapper">
<div class="hmgal-heading">
<div class="container">
<div class="section-header">
			                <h1 class="section-title text-center wow fadeInDown animated animated" style="visibility: visible; animation-name: fadeInDown;">Home Gallery</h1>
										           
			               
			              
            			</div>
</div>
</div>
<section class="homegal-part">
<div class="container">
<div class="row">

<?php


$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
$args = array (
    'nopaging'               => false,
    'paged'                  => $paged,
    'posts_per_page'         => '6',
    'post_type'              => 'gallery',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {

    

    while ( $query->have_posts() ) {
        $query->the_post();
        echo '<a href="/home-for-rent-application?id='.get_the_ID().'"><div class="col-md-4">';
echo '<div class="homegallery-inner">';
?>
<?php the_post_thumbnail(); ?>
<a href="/home-for-rent-application?id=<?php echo get_the_ID(); ?>"><h2><?php  the_title(); ?> </h2></a>
<p><?php the_content(); ?></p>
<!-- <?php //echo do_shortcode('[wpep_form]'); ?> -->
</div>
</div></a><?php 
        //echo '<div class="news-item">';
          
                //echo '<h1 class="page-title screen-reader-text">' . the_title() . '</h1>';
        //echo '</div>';
    }
    if ( $query->max_num_pages > 1 ) : // if there's more than one page turn on pagination
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    //echo "<pre>";
    //print_r($query->max_num_pages);
?>
    <div class="col-md-12 text-center">
	<ul class="pagenavd">
	<?php if($paged>1){ ?><li><?php previous_posts_link( '« Prev' ); ?></li><?php } ?>
    <?php if($paged < $query->max_num_pages){ ?><li><?php next_posts_link( 'Next »', $query->max_num_pages ); ?></li><?php } ?>
    </ul>
</div>	
<?php endif;
} else {
    // no posts found
    echo '<h1 class="page-title screen-reader-text">No Posts Found</h1>';
}

// Restore original Post Data
wp_reset_postdata();
 ?>



	

</div>
<div class="page-paginations hide">
                        <nav aria-label="Pagination">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">
                                            <i class="fa fa-chevron-left"></i>
                                        </span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">
                                            <i class="fa fa-chevron-right"></i>
                                        </span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
</div>
</section>
</div>

<?php get_footer(); ?>